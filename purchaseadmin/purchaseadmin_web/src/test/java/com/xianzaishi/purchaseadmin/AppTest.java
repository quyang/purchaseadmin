package com.xianzaishi.purchaseadmin;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.caucho.hessian.client.HessianProxyFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.trade.client.OrderService;

public class AppTest {
  
  private static final String STEP_FILE = "E:\\step.txt";
  public static String[] inviteChars = new String[] { "a", "b", "c", "d", "e", "f",
    "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
    "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
    "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
    "W", "X", "Y", "Z" };
  @Autowired
  private static ItemComponent itemComponent;

  public static void main(String[] args) throws MalformedURLException {
    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
//    String url = "http://trade.xianzaishi.com/hessian/orderService";
//    HessianProxyFactory factory = new HessianProxyFactory();
//    OrderService orderService = (OrderService) factory.create(OrderService.class, url);// 创建IService接口的实例对象
//    com.xianzaishi.trade.client.Result<Boolean> updateOrderResult =
//        orderService.updateOrderStatus(101049, (short) 9);
//    System.out.println(JackSonUtil.getJson(updateOrderResult));
//    String resultString = getStepInfo();
//    for(int i = 1;i < 100; i ++){
//      System.out.println(createInviteCode((long) i));
//    }
    
//    String result = add2(10001L);
//    System.out.println(result);
//    Integer tags = 511;
//    for(int i = 1;i<=256;i=(i<<1)){
//      if((tags & i) == i){
//        if(i == 0x1){
//          System.out.println("线上满90减40元");
//        }else if(i == 0x2){
//          System.out.println("线下满40减20元");
//        }else if(i == 0x4){
//          System.out.println("0x4");
//        }else if(i == 0x10){
//          System.out.println("线下满90减40元");
//        }else if(i == 0x20){
//          System.out.println("线上满40减20元");
//        }else if(i == 0x40){
//          System.out.println("0x40");
//        }else if(i == 0x80){
//          System.out.println("0x80");
//        }else if(i == 0x100){
//          System.out.println("0x100");
//        }
//      }
//    }
//    String a1 = "abc";
//    String a2 = "abc";
//    Long couponId = 31L;
//    System.out.println("couponId > 0 : " + (couponId > 0));
    List<Long> cSkuIds = Lists.newArrayList();
    Map<Long, Long> cPSkuIdMap = Maps.newHashMap();
    cSkuIds.add(50038L);
    cPSkuIdMap.put(50038L, 500100100020155L);
    Map<Long, Integer> resultMap = itemComponent.batchQueryItemInventory(cSkuIds, cPSkuIdMap);
    System.out.println(JackSonUtil.getJson(resultMap));
  }
  
  private static String add2(Long userId) {
    String resultString = String.valueOf(userId + 2);
    return  resultString;
  }

  public static String createInviteCode(Long userId){
    StringBuffer shortBuffer = new StringBuffer();
    String uuid = UUID.randomUUID().toString().replace("-", "");
    String userIdCode = Long.toString(userId, 16);
    if(userIdCode.length() < 8){
      for (int i = 0; i < 8 - userIdCode.length(); i++) {
        String str = uuid.substring(i * 4, i * 4 + 4);
        int x = Integer.parseInt(str, 16);
        shortBuffer.append(inviteChars[x % 0x3E]);
      }
      shortBuffer.append(userIdCode);
    }else{
      for (int i = 0; i < 8; i++) {
        String str = uuid.substring(i * 4, i * 4 + 4);
        int x = Integer.parseInt(str, 16);
        shortBuffer.append(inviteChars[x % 0x3E]);
      }
    }
    
    return shortBuffer.toString();
  }
  
  
  /**
   * 读取配置文件楼层信息
   * @return
   */
  private static String getStepInfo(){
    FileReader reader = null;
    BufferedReader br = null;
    try {
      reader = new FileReader(STEP_FILE);
      br = new BufferedReader(reader);
      StringBuffer buffer = new StringBuffer();
      String line = "";
      while(null != (line = br.readLine())){
        buffer.append(line);
        buffer.append("\n");
      }
      System.out.println(buffer);
      return buffer.toString();
    } catch (FileNotFoundException e) {
      System.out.println("文件未找到：" + e);
      e.printStackTrace();
    } catch (IOException e) {
      System.out.println("IO错误：" + e);
      e.printStackTrace();
    }finally{
      if(null != br){
        try {
          br.close();//关闭流
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if(null != reader){
        try {
          reader.close();//关闭文件
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }
 
}