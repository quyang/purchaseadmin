package com.xianzaishi.purchaseadmin.sale.controller;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.sale.vo.SaleListQueryVO;
import com.xianzaishi.purchaseadmin.client.sale.vo.SaleVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.salelist.SaleListComponent;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 销售列表接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping(value = "/salelist")
public class SaleListController {

  private static final Logger LOGGER = Logger.getLogger(SaleListController.class);

  @Autowired
  private SaleListComponent saleListComponent;

  @RequestMapping(value = "/requestsalelist", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String requestSaleList(String data, HttpServletResponse response) {
    if (null != data) {
      LOGGER.info(data);
    }
    SaleListQueryVO saleListQueryVO =
        (SaleListQueryVO) JackSonUtil.jsonToObject(data, SaleListQueryVO.class);

    if (null == saleListQueryVO) {
      saleListQueryVO = new SaleListQueryVO();
    }

    LOGGER.error("参数对象=" + JackSonUtil.getJson(saleListQueryVO));

    String result =
        saleListComponent.querySaleList(saleListQueryVO.getItemKeyWord(),
            saleListQueryVO.getSkuId(), saleListQueryVO.getSkuName(),
            saleListQueryVO.getSku69Code(), saleListQueryVO.getPageSize(),
            saleListQueryVO.getPageNum());
    return result;
  }

  @RequestMapping(value = "/saleList", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String saleList(@RequestBody String request, HttpServletResponse response) {
    if (StringUtils.isEmpty(request)) {
      LOGGER.error("request=" + request);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    HttpQuery<SaleListQueryVO> httpQuery =
        BaseActionAdapter.processObjectParameter(request, SaleListQueryVO.class);

    LOGGER.error("解析后=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    SaleListQueryVO data = httpQuery.getData();

    String result =
        saleListComponent.querySaleList(data.getItemKeyWord(), data.getSkuId(), data.getSkuName(),
            data.getSku69Code(), data.getPageSize(), data.getPageNum());
    return result;
  }

  /**
   * 请求二维码信息
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/requestqrcode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String createQrCode(String data, HttpServletResponse response) {

    LOGGER.error("data=" + data);
    if (null != data) {
      LOGGER.info(data);
    } else {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    if (null == map) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "参数不是基本json数据格式"));
    }
    Long qrId = Long.valueOf(map.get("qrId"));
    Short qrType = Short.valueOf(map.get("qrType"));
    if (null == qrId || null == qrType) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = saleListComponent.createQrCode(qrId, qrType);

    return result;
  }

  /**
   * 请求二维码信息
   * 
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/getQrcode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String getQrcode(@RequestBody String request, HttpServletResponse response) {

    LOGGER.error("request=" + request);// {"qrId":"10205706","qrType":"0"}

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    HttpQuery<SaleVO> httpQuery = BaseActionAdapter.processObjectParameter(request, SaleVO.class);

    LOGGER.error("解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    SaleVO data = httpQuery.getData();

    return saleListComponent.createQrCode(data.getQrId(), data.getQrType());
  }

  /**
   * 上架
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/onshelf", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String onShelf(String data, HttpServletResponse response) {
    if (null != data) {
      LOGGER.info(data);
    }
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    if (null == map) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "参数不是基本json数据格式"));
    }
    Long skuId = Long.valueOf(map.get("skuId"));
    String result = saleListComponent.onShelf(skuId);
    return result;
  }

  /**
   * 上架 new
   * 
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/upShelf", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String upShelf(@RequestBody String request, HttpServletResponse response) {

    LOGGER.error("request" + request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    HttpQuery<SaleVO> httpQuery = BaseActionAdapter.processObjectParameter(request, SaleVO.class);

    LOGGER.error("解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    return saleListComponent.onShelf(httpQuery.getData().getSkuId());
  }

  /**
   * 下架
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/offshelf", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String offShelf(String data, HttpServletResponse response) {
    if (null != data) {
      LOGGER.info(data);
    }
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    if (null == map) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "参数不是基本json数据格式"));
    }
    Long skuId = Long.valueOf(map.get("skuId"));
    String result = saleListComponent.offShelf(skuId);
    return result;
  }


  /**
   * 下架 new
   * 
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/downShelf", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String downShelf(@RequestBody String request, HttpServletResponse response) {

    LOGGER.error("request=" + request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    HttpQuery<SaleVO> httpQuery = BaseActionAdapter.processObjectParameter(request, SaleVO.class);

    LOGGER.error("解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    return saleListComponent.offShelf(httpQuery.getData().getSkuId());
  }

  /**
   * 销售渠道修改
   * 
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/updateChannel", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String updateChannel(@RequestBody String request, HttpServletResponse response) {

    LOGGER.error("request=" + request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    // channel skuId
    HttpQuery<SaleVO> httpQuery = BaseActionAdapter.processObjectParameter(request, SaleVO.class);

    LOGGER.error("解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    return saleListComponent.updateChannel(httpQuery.getData());
  }

  /**
   * 更改商品供应商
   * 
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/changeitemsupplier", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String changeItemSupplier(@RequestBody String data, HttpServletResponse response) {
    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, Object> requestMap = query.getData();
    if (MapUtils.isEmpty(requestMap)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    Long skuId = Long.valueOf((String) requestMap.get("skuId"));
    Integer oldSupplierId = Integer.valueOf((String) requestMap.get("oldSupplierId"));
    Integer newSupplierId = Integer.valueOf((String) requestMap.get("newSupplierId"));
    Result<Boolean> updateResult =
        saleListComponent.updateItemSupplier(skuId, oldSupplierId, newSupplierId);
    return JackSonUtil.getJson(updateResult);
  }
  
  /**
   * 更改商品供应商
   * 
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/clearitem", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String clearItem(@RequestBody String data, HttpServletResponse response) {
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, Object> requestMap = query.getData();
    Long skuId = Long.valueOf((String) requestMap.get("skuId"));
    Short status = Short.valueOf((String) requestMap.get("status"));
    Result<Boolean> clearResult = saleListComponent.clearItem(skuId,status);
    return JackSonUtil.getJson(clearResult);
  }
}
