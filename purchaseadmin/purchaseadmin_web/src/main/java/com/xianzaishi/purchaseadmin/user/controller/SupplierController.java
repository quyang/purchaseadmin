package com.xianzaishi.purchaseadmin.user.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.supplier.vo.SupplierInfoQueryVO;
import com.xianzaishi.purchaseadmin.client.supplier.vo.SupplierInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.ResetPwdUserVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.user.SupplierComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

@Controller
@RequestMapping(value = "/supplier")
@SessionAttributes("lastTime")
public class SupplierController extends BaseController {

  private static final Logger LOGGER = Logger.getLogger(SupplierController.class);

  @Autowired
  private UserComponent userComponent;

  @Autowired
  private SupplierComponent supplierComponent;

  /**
   * 供应商登陆接口
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/login", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String supplierLogin(String data, HttpServletResponse response) {
    String postInfo = "post receive:" + data;
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    UserVO userVO = (UserVO) JackSonUtil.jsonToObject(data, UserVO.class);
    userVO.setUserType((short) 1);
    Result<Map<String, String>> loginResultM = supplierComponent.login(userVO);
    if (null == loginResultM) {
      LOGGER.error(postInfo + ".login failed,return null");
    } else if (!loginResultM.getSuccess()) {
      LOGGER.error(postInfo + ".login failed,return code:" + loginResultM.getResultCode() + ",msg:"
          + loginResultM.getModule());
    } else {
      LOGGER.error(postInfo + ".login success,token is:" + loginResultM.getModule().get("token"));
    }
    return JackSonUtil.getJson(loginResultM);
  }


  /**
   * 供应商退出接口
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/logout", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String supplierLogout(String data, HttpServletResponse response) {
    LOGGER.error("post receive:" + data);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    BackGroundUserDTO userDTO = getUser();
    if (userDTO == null || null == userDTO.getToken()) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "logout token error"));
    }
    UserVO userVO = (UserVO) JackSonUtil.jsonToObject(data, UserVO.class);
    if(null == userVO){
      userVO = new UserVO();
    }
    userVO.setToken(userDTO.getToken());
    return JackSonUtil.getJson(userComponent.logout(userVO));
  }

  /**
   * 用户注册接口
   * 
   * @param userRegistVO 用户注册信息，包括phone、pwd、verificationCode
   * @return
   */
  @RequestMapping(value = "/regist", method = RequestMethod.POST,
      produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String supplierRegist(String data, HttpServletResponse response) {

    return null;
  }

  /**
   * 发送验证码接口
   * 
   * @param userRegistVO 用户注册信息，包括phone、pwd、verificationCode
   * @return
   */
  @RequestMapping(value = "/sendcheckcode", method = RequestMethod.POST,
      produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String sendCheckCode(String data, HttpServletResponse response) {
    LOGGER.error("post receive:" + data);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    Map<String, String> userMap = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String userName = userMap.get("userName");
    return JackSonUtil.getJson(userComponent.resetUserPwdSendToken(userName));
  }

  /**
   * 重置密码接口
   * 
   * @param userRegistVO 用户注册信息，包括phone、pwd、verificationCode
   * @return
   */
  @RequestMapping(value = "/modifiedpwd", method = RequestMethod.POST,
      produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String modifiedPwd(String data, HttpServletResponse response) {
    LOGGER.error("post receive:" + data);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    ResetPwdUserVO userVO = (ResetPwdUserVO) JackSonUtil.jsonToObject(data, ResetPwdUserVO.class);
    return JackSonUtil.getJson(userComponent.resetPwdAndLogin(userVO));
  }

  /**
   * 修改供应商信息
   * 
   * @param userInfoVO
   * @return
   */
  @RequestMapping(value = "/updatesupplierinfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String updateUserInfo(String data, HttpServletResponse response) {

    return null;
  }

  /**
   * 获取默认前200条供应商信息
   * 
   * @param userId
   * @return
   */
  @RequestMapping(value = "/querydefaultsupplierinfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  @Authorization
  public String queryDefaultSupplierInfo(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    SupplierInfoQueryVO supplierInfoQueryVO =
        (SupplierInfoQueryVO) JackSonUtil.jsonToObject(data, SupplierInfoQueryVO.class);
    if (null == supplierInfoQueryVO) {
      supplierInfoQueryVO = new SupplierInfoQueryVO();
    }
    PagedResult<List<SupplierInfoVO>> supplierInfoResult =
        supplierComponent.querySupplierInfo(supplierInfoQueryVO);

    return JackSonUtil.getJson(supplierInfoResult);
  }


  /**
   * 获取供应商信息
   * 
   * @param userId
   * @return
   */
  @RequestMapping(value = "/querysupplierinfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  @Authorization
  public String queryUserInfo(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    if (StringUtils.isBlank(data)) {
      return queryDefaultSupplierInfo(null, response);
    }
    SupplierInfoQueryVO supplierInfoQueryVO =
        (SupplierInfoQueryVO) JackSonUtil.jsonToObject(data, SupplierInfoQueryVO.class);
    if (null == supplierInfoQueryVO) {
      return JackSonUtil.getJson(PagedResult.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数格式错误"));
    }
    PagedResult<List<SupplierInfoVO>> supplierInfoResult =
        supplierComponent.querySupplierInfo(supplierInfoQueryVO);

    return JackSonUtil.getJson(supplierInfoResult);
  }

  /**
   * 获取默认前200条供应商信息
   * 
   * @param userId
   * @return
   */
  @RequestMapping(value = "/checksessionexpire", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String checkSessionExpire(HttpServletRequest request) {
    // Long lastTime = (Long) WebUtils.getSessionAttribute(request,
    // "context_key");
    Result<Boolean> sessionExpireResult = supplierComponent.checkSessionExpire(request);
    return JackSonUtil.getJson(sessionExpireResult);
  }
}
