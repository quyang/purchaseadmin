package com.xianzaishi.purchaseadmin.activitypage.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.activity.vo.ActivityPageVO;
import com.xianzaishi.purchaseadmin.client.activity.vo.ActivityStepVO;
import com.xianzaishi.purchaseadmin.component.activitypage.ActivityPageComponent;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;

/**
 * 支持活动页面
 * 
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityController {

  private static final Logger logger = Logger.getLogger(ActivityController.class);

  @Autowired
  private ActivityPageComponent activityPageComponent;

  /**
   * 查询所有活动楼层页面
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/list", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String list(String data, HttpServletResponse response) {
//    BaseQuery query = new BaseQuery();
//    if(null != data){
//      query = (BaseQuery) JackSonUtil.jsonToObject(data, BaseQuery.class);
//    }
    Result<List<ActivityPageVO>> result = activityPageComponent.getList();
    if(null == result || !result.getSuccess()){
      logger.error("post receive:" + data);
    }
    return JackSonUtil.getJson(result);
  }
  
  /**
   * 删除楼层数据
   * @param stepId
   * @param response
   * @return
   */
  @RequestMapping(value = "/dropstep", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String dropStep(Integer stepId, HttpServletResponse response) {
    if(null == stepId){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = activityPageComponent.dropPageStep(stepId);
    return result;
  }

  /**
   * 添加一个新页面
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/init", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String initPage(String data, HttpServletResponse response) {
    ActivityPageVO pageVO = (ActivityPageVO) JackSonUtil.jsonToObject(data, ActivityPageVO.class);
    if (null == pageVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "Add page paramter change failed,recive data is:"+data));
    }

    return JackSonUtil.getJson(activityPageComponent.addPage(pageVO));
  }
  
  /**
   * 添加一个新楼层
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/addstep", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String addStep(String data, HttpServletResponse response) {
    ActivityStepVO stepVO = (ActivityStepVO) JackSonUtil.jsonToObject(data, ActivityStepVO.class);
    if (null == stepVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "Add step paramter change failed,recive data is:"+data));
    }

    return JackSonUtil.getJson(activityPageComponent.addStep(stepVO));
  }
  
  /**
   * 某个楼层回滚
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/reset", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String reset(String data, HttpServletResponse response) {
    Map<String,Integer> stepInfo = (HashMap<String,Integer>) JackSonUtil.jsonToObject(data, HashMap.class);
    if (null == stepInfo) {
      logger.error("post receive:" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query step paramter change failed,input is:"+data));
    }

    return JackSonUtil.getJson(activityPageComponent.resetStep(stepInfo.get("pageId"),stepInfo.get("stepId")));
  }
  
  /**
   * 添加一个新页面
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/querystep", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String queryStep(String data, HttpServletResponse response) {
    Map<String,Integer> stepInfo = (HashMap<String,Integer>) JackSonUtil.jsonToObject(data, HashMap.class);
    if (null == stepInfo) {
      logger.error("post receive:" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query step paramter change failed,input is:"+data));
    }

    return JackSonUtil.getJson(activityPageComponent.queryStep(stepInfo.get("pageId"),stepInfo.get("stepId")));
  }
  
  /**
   * 返回楼层类型数据
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/liststeptype", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String listStepType(HttpServletResponse response) {
//    List<String[]> stepTypeList = Lists.newArrayList();
//    String[] picStep = new String[2];
//    picStep[0] = "0";
//    picStep[1] = "多图片轮播楼层";
//    
//    String[] picItemStep = new String[2];
//    picItemStep[0] = "1";
//    picItemStep[1] = "图片+商品楼层";
//    
//    String[] itemStep = new String[2];
//    itemStep[0] = "2";
//    itemStep[1] = "无图片商品楼层";
//
//    String[] titleItemStep = new String[2];
//    titleItemStep[0] = "3";
//    titleItemStep[1] = "文字+商品列表";
//    
//    String[] OnePicStep = new String[2];
//    OnePicStep[0] = "4";
//    OnePicStep[1] = "单图楼层";
//    
//    String[] PicListStep = new String[2];
//    PicListStep[0] = "5";
//    PicListStep[1] = "图片列表楼层";
//    
//    stepTypeList.add(picStep);
//    stepTypeList.add(picItemStep);
//    stepTypeList.add(itemStep);
//    stepTypeList.add(titleItemStep);
//    stepTypeList.add(OnePicStep);
//    stepTypeList.add(PicListStep);
    String result = activityPageComponent.getStepTypeList();
    
    return result;
  }
  
  /**
   * 返回图片类型数据
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/listpictype", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String listPicType(HttpServletResponse response) {
    List<String[]> picTypeList = Lists.newArrayList();
    String[] pic = new String[2];
    pic[0] = "0";
    pic[1] = "无目标地址固定图片";
    
    String[] picSubactivitypage = new String[2];
    picSubactivitypage[0] = "1";
    picSubactivitypage[1] = "指向活动图片";
    
    String[] picItem = new String[2];
    picItem[0] = "2";
    picItem[1] = "指向商品图片";

    String[] requestUrl= new String[2];
    requestUrl[0] = "3";
    requestUrl[1] = "请求固定链接";
    
    String[] picUrl= new String[2];
    picUrl[0] = "4";
    picUrl[1] = "跳转固定链接";
    
    picTypeList.add(pic);
    picTypeList.add(picSubactivitypage);
    picTypeList.add(picItem);
    picTypeList.add(requestUrl);
    picTypeList.add(picUrl);
    
    return JackSonUtil.getJson(Result.getSuccDataResult(picTypeList));
  }
}
