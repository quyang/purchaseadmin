package com.xianzaishi.purchaseadmin.order.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.order.vo.OrderListQueryVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.order.OrderListComponent;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseOrderAuditingStatusConstants;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO.UserStatusConstants;

/**
 * 采购单列表接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping(value = "/orderlist")
public class OrderListController extends BaseController {

  private static final Logger LOGGER = Logger.getLogger(OrderListController.class);

  @Autowired
  private OrderListComponent orderListComponent;

  /**
   * 采购人员查询采购单列表
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/requestorderlistwithemployee",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String requestOrderListWithEmployee(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
    // response.addHeader("Access-Control-Allow-Origin", "*");
    // response.setHeader("Access-Control-Allow-Credentials", "true");
    LOGGER.info(data);
    OrderListQueryVO orderListQueryVO =
        (OrderListQueryVO) JackSonUtil.jsonToObject(data, OrderListQueryVO.class);


    BackGroundUserDTO userDTO = getUser();
    if (null == userDTO) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST,
          "用户已失效，请重新登陆"));
    }

    if (!UserTypeConstants.USER_EMPLOYEE.equals(userDTO.getUserType())
        && !userDTO.queryRoleList().contains(80) && !userDTO.queryRoleList().contains(81)
        && !userDTO.queryRoleList().contains(82)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "您没有权限进行该项操作"));
    }
    String result = orderListComponent.queryOrderList(orderListQueryVO, userDTO);
    return result;
  }

  /**
   * 供应商查询采购单列表
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/requestorderlistwithsupplier",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String requestOrderListWithSupplier(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
    // response.addHeader("Access-Control-Allow-Origin", "*");
    // response.setHeader("Access-Control-Allow-Credentials", "true");
    LOGGER.info(data);
    OrderListQueryVO orderListQueryVO =
        (OrderListQueryVO) JackSonUtil.jsonToObject(data, OrderListQueryVO.class);


    BackGroundUserDTO userDTO = getUser();
    if (null == userDTO) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST,
          "用户已失效，请重新登陆"));
    }

    if (!UserTypeConstants.USER_SUPPLIER.equals(userDTO.getUserType())) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "您没有权限进行该项操作"));
    }
    String result = orderListComponent.queryOrderList(orderListQueryVO, userDTO);
    return result;
  }

  @RequestMapping(value = "/requestorderDetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String requestOrderDetail(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
    // response.addHeader("Access-Control-Allow-Origin", "*");
    // response.setHeader("Access-Control-Allow-Credentials", "true");
    Map<String, Object> map = (Map<String, Object>) JackSonUtil.jsonToObject(data, Map.class);
    Integer purchaseId = (Integer) map.get("purchaseId");
    Integer pageSize = (Integer) map.get("pageSize");
    Integer pageNum = (Integer) map.get("pageNum");
    if (null == purchaseId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    if (null == pageSize) {
      pageSize = BaseQuery.DEFAULT_PAGE_SIZE;
    }
    if (null == pageNum) {
      pageNum = BaseQuery.DEFAULT_PAGE_NUM;
    }
    PagedResult<Map<String, Object>> result =
        orderListComponent.queryOrderDetail(purchaseId, pageSize, pageNum);
    return JackSonUtil.getJson(result);
  }

  /**
   * 采购主管审核
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/trialwithemployee", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String purchaseTrialWithEmployee(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
    // response.addHeader("Access-Control-Allow-Origin", "*");
    // response.setHeader("Access-Control-Allow-Credentials", "true");
    Map<String, Object> map = (Map<String, Object>) JackSonUtil.jsonToObject(data, Map.class);
    Integer purchaseId = Integer.valueOf((String) map.get("purchaseId"));
    Short auditingStatus = Short.parseShort((String) map.get("auditingStatus"));
    String remarks = (String) map.get("remarks");
    Integer userId = getUserId();
    if (null == userId) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST,
          "用户已失效，请重新登陆"));
    }
    BackGroundUserDTO userDTO = getUser();
    List<Integer> role = userDTO.queryRoleList();
    if (!role.contains(81)
        || !role.contains(82)
        || (PurchaseOrderAuditingStatusConstants.STATUS_FIRST_AUDITING_PASS.equals(auditingStatus) && !role
            .contains(81))
        || (PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS.equals(auditingStatus) && !role
            .contains(82))) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "您没有权限进行该项操作"));
    }
    Result<Boolean> result =
        orderListComponent.updatePurchaseStatus(purchaseId, auditingStatus, remarks, userDTO);
    return JackSonUtil.getJson(result);
  }

  /**
   * 供应商审核
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/trialwithsupplier", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String purchaseTrialWithSupplier(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
    // response.addHeader("Access-Control-Allow-Origin", "*");
    // response.setHeader("Access-Control-Allow-Credentials", "true");
    Map<String, Object> map = (Map<String, Object>) JackSonUtil.jsonToObject(data, Map.class);
    Integer purchaseId = Integer.valueOf((String) map.get("purchaseId"));
    Short auditingStatus = Short.parseShort((String) map.get("auditingStatus"));
    String remarks = (String) map.get("remarks");
    Integer userId = getUserId();
    if (null == userId) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST,
          "用户已失效，请重新登陆"));
    }
    BackGroundUserDTO userDTO = getUser();
    if (!UserTypeConstants.USER_SUPPLIER.equals(userDTO.getUserType())) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "您没有权限进行该项操作"));
    }
    Result<Boolean> result =
        orderListComponent.updatePurchaseStatus(purchaseId, auditingStatus, remarks, userDTO);
    return JackSonUtil.getJson(result);
  }
  
  @RequestMapping(value = "/getmail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getMaile(){
    return orderListComponent.printRecipientMail();
  }

}
