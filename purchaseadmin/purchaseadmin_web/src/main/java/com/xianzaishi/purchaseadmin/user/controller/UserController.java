package com.xianzaishi.purchaseadmin.user.controller;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.bill.vo.BillQueryVO;
import com.xianzaishi.purchaseadmin.client.bill.vo.BillUpdateVO;
import com.xianzaishi.purchaseadmin.client.bill.vo.BillsInsertVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderSVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrdersInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.ResetPwdUserVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserCouponVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserOrderVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserQueryVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierCompanyDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import java.io.File;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * erp后台登陆
 *
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/user")
public class UserController extends BaseController {

  private static final Logger logger = Logger.getLogger(UserController.class);

  @Resource
  HttpServletRequest request;

  @Autowired
  private UserComponent userComponent;

  /**
   * 用户登录
   */
  @RequestMapping(value = "/login", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String login(String data, HttpServletResponse response) {
    String postInfo = "post receive:" + data;
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    UserVO userVO = (UserVO) JackSonUtil.jsonToObject(data, UserVO.class);
    userVO.setUserType((short) 0);
    Result<Map<String, String>> loginResultM = userComponent.login(userVO);
    if (null == loginResultM) {
      logger.error(postInfo + ".login failed,return null");
    } else if (!loginResultM.getSuccess()) {
      logger.error(postInfo + ".login failed,return code:" + loginResultM.getResultCode() + ",msg:"
          + loginResultM.getModule());
    } else {
      logger.error(postInfo + ".login success,token is:" + loginResultM.getModule().get("token"));
    }
    return JackSonUtil.getJson(loginResultM);
  }

  /**
   * 用户退出
   */
  @RequestMapping(value = "/logout", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String logout(String data, HttpServletResponse response) {
    logger.error("post receive:" + data);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    BackGroundUserDTO userDTO = getUser();
    if (userDTO == null || null == userDTO.getToken()) {
      return JackSonUtil.getJson(Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "logout token error"));
    }
    UserVO userVO = (UserVO) JackSonUtil.jsonToObject(data, UserVO.class);
    userVO.setToken(userDTO.getToken());
    return JackSonUtil.getJson(userComponent.logout(userVO));
  }


  /**
   * 用户重置密码请求发短信
   */
  @RequestMapping(value = "/resetpwdsendtoken", method = RequestMethod.POST)
  @ResponseBody
  public String resetPwdSendToken(String data, HttpServletResponse response) {
    logger.error("post receive:" + data);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    Map<String, String> userMap = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String userName = userMap.get("userName");
    return JackSonUtil.getJson(userComponent.resetUserPwdSendToken(userName));
  }

  /**
   * 用户重置密码提交
   */
  @RequestMapping(value = "/resetpwdlogin", method = RequestMethod.POST)
  @ResponseBody
  public String resetPwdLogin(String data, HttpServletResponse response) {
    logger.error("post receive:" + data);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    ResetPwdUserVO userVO = (ResetPwdUserVO) JackSonUtil.jsonToObject(data, ResetPwdUserVO.class);
    return JackSonUtil.getJson(userComponent.resetPwdAndLogin(userVO));
  }

  /**
   * 初始导入所有供应商接口
   */
  @RequestMapping(value = "/addallsupplier", method = RequestMethod.POST)
  @ResponseBody
  public String addallSupplier(String data) {

    String inpukey = "M3uTYdy15";
    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
    }
    Map<String, String> userMap = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String istest = userMap.get("istest");
    String isonline = userMap.get("isonline");
    String key = userMap.get("key");

    if (StringUtils.isEmpty(istest) || StringUtils.isEmpty(isonline) || StringUtils.isEmpty(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
    }
    if (!inpukey.equals(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is error"));
    }

    userComponent.addAllSupplier(new File("/usr/works/datainit/supplier.xls"));
    return request.getRequestURI();
  }

  /**
   * 初始导入所有采购商品接口
   */
  @RequestMapping(value = "/addallemploy", method = RequestMethod.GET)
  @ResponseBody
  public String addallEmploy(String data) {

    String inpukey = "yuTYdy15";
    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
    }
    Map<String, String> userMap = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String istest = userMap.get("istest");
    String isonline = userMap.get("isonline");
    String key = userMap.get("key");

    if (StringUtils.isEmpty(istest) || StringUtils.isEmpty(isonline) || StringUtils.isEmpty(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
    }
    if (!inpukey.equals(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is error"));
    }
    boolean isTest = !"false".equals(istest);

    userComponent.addAllEmployee(isonline, isTest);
    return JackSonUtil.getJson(true);
  }

  // /**
  // * 初始导入所有采购商品接口
  // *
  // * @return
  // */
  // @RequestMapping(value = "/adduserrole", method = RequestMethod.POST)
  // @ResponseBody
  // public String addUserRole(String data) {
  // String inpukey = "yuTYdy15er";
  // if (StringUtils.isEmpty(data)) {
  // return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
  // }
  // Map<String, String> userMap = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
  // String username = userMap.get("username");
  // String rolename = userMap.get("rolename");
  // String key = userMap.get("key");
  //
  // if (StringUtils.isEmpty(username) || StringUtils.isEmpty(username) || StringUtils.isEmpty(key))
  // {
  // return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
  // }
  //
  // if (!inpukey.equals(key)) {
  // return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is error"));
  // }
  //
  // return JackSonUtil.getJson(userComponent.addUserRole(rolename, username));
  // }

  @RequestMapping(value = "/addpagerole", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String addpageRole(String page, String role, String key) {
    String inpukey = "yuTYdy15er";
    if (!inpukey.equals(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "key is wrong"));
    }
    return JackSonUtil.getJson(userComponent.addPageRequireRole(page, role));
  }

  @RequestMapping(value = "/adduserrole", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String addUserRole(String rolename, String username, String key) {
    String inpukey = "yuTYdy15er";
    if (!inpukey.equals(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "key is wrong"));
    }
    return JackSonUtil.getJson(userComponent.addUserRole(rolename, username));
  }

  /**
   * 根据用户id请求用户信息
   */
  @RequestMapping(value = "/queryuserinfo", method = RequestMethod.POST)
  @ResponseBody
  public SupplierCompanyDTO queryUserInfo(String data) {
    if (null == data) {
      return null;
    }
    Integer userId = (Integer) JackSonUtil.jsonToObject(data, Integer.class);
    SupplierDTO userInfo = userComponent.getSupplierById(userId);
    if (null == userInfo) {
      return null;
    }
    return userInfo.getSupplierCompanyDTO();
  }


  /**
   * 查询用户信息
   *
   * @param data 用户手机号码
   * @return ReponseModel 用户信息
   */
  @RequestMapping(value = "/getUserInfo", method = {RequestMethod.POST},
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getUserInfo(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    // 封装参数
    UserOrderVO userOrderVO = (UserOrderVO) JackSonUtil.jsonToObject(data, UserOrderVO.class);

    try {
      Result<UserInfoVO> result = userComponent.getBasicUserInfo(userOrderVO);
      if (result.getSuccess() && null != result.getModule()) {
        return JackSonUtil.getJson(Result.getSuccDataResult(result.getModule()));
      }

      return JackSonUtil.getJson(Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, result.getErrorMsg()));

    } catch (RuntimeException e) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    }
  }


  /**
   * 查询订单列表
   *
   * @return 订单列表
   */
  @RequestMapping(value = "/getOrderList",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getOrderList(String data, HttpServletResponse response) {
    try {
      response.setContentType("text/html;charset=UTF-8");
//      response.addHeader("Access-Control-Allow-Origin", "*");
      logger.error("data=" + data);
      
      if (StringUtils.isEmpty(data)) {
        return JackSonUtil.getJson(
            Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数data异常"));
      }

      //封装参数 userId pageSize currentPage
      UserQueryVO params = (UserQueryVO) JackSonUtil.jsonToObject(data, UserQueryVO.class);
      Result<OrdersInfoVO> result = userComponent.getOrderList(params);
      if (null != result && result.getSuccess() && null != result.getModule()) {
        return JackSonUtil.getJson(Result.getSuccDataResult(result.getModule()));
      }
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,result.getErrorMsg()));
    } catch (RuntimeException e) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    }
  }

  /**
   * 查询优惠券列表
   *
   * @param data
   * @return string
   */
  @RequestMapping(value = "/getCouponList", method = {RequestMethod.POST},
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getCouponList(String data, HttpServletResponse response) {

    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    if (StringUtils.isEmpty(data)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    try {

      //封装参数
      UserQueryVO params = (UserQueryVO) JackSonUtil.jsonToObject(data, UserQueryVO.class);

      logger.error("UserControl 用户id="+params.getUid());

      Result<List<UserCouponVO>> result = userComponent.getCouponList(params);
      if (null != result && result.getSuccess() && null != result.getModule()) {
        return JackSonUtil.getJson(result);
      }

      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询用户优惠券失败"));

    } catch (RuntimeException e) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    }
  }


  /**
   * 插入用户发票信息
   *
   * @param data string
   */
  @RequestMapping(value = "/insertBill", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String insertBill(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    BillsInsertVO queryVO = (BillsInsertVO) JackSonUtil.jsonToObject(data, BillsInsertVO.class);
    Result<Boolean> result = userComponent.insertBillInfo(queryVO);
    if (null != result && result.getSuccess() && result.getModule()) {
      return JackSonUtil.getJson(Result.getSuccDataResult(result.getModule()));
    }
    return JackSonUtil.getJson(
        Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, result.getErrorMsg()));
  }


  /**
   *某一个订单是否开过发票
   *
   * @param data
   */
  @RequestMapping(value = "/hasBilled", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String hasBilled(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    System.out.print("data"+data);
    try {

      //封装参数 orderId
      BillQueryVO params = (BillQueryVO) JackSonUtil.jsonToObject(data, BillQueryVO.class);

      Result<Boolean> result = userComponent.hasBilled(params);
      if (null != result && null != result.getModule() && result.getModule()) {
        return JackSonUtil.getJson(Result.getSuccDataResult("该订单已开过发票"));
      }

      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"该订单没有开过发票"));
    } catch (RuntimeException e) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    }
  }


  /**
   *修改发票信息
   *
   * @param data
   */
  @RequestMapping(value = "/updateBillInfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String updateBillInfo(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    System.out.print("data值是=" + data);

    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    try {

      //封装参数 phoneNumber userId billNumber billMoney contribution
      BillUpdateVO o = (BillUpdateVO) JackSonUtil.jsonToObject(data, BillUpdateVO.class);

      logger.error(
          "参数=" + o.getBillNumber() + ",=" + o.getBillMoney() + ",=" + o.getUserId() + ",=" + o
              .getContribution() + ",=" + o.getPhoneNumber());

      Result<Boolean> result = userComponent.updateBIllVO(o);
      if (null != result && null != result.getModule() && result.getModule()) {
        return JackSonUtil.getJson(Result.getSuccDataResult("修改发票信息成功"));
      }

      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "修改发票信息失败"));
    } catch (RuntimeException e) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    }
  }


  /**
   *获取订单信息
   *
   * @param data
   */
  @RequestMapping(value = "/getOrderInfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getOrderInfo(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

      //封装参数  oid
      UserQueryVO o = (UserQueryVO) JackSonUtil.jsonToObject(data, UserQueryVO.class);

      Result<OrderSVO> result = userComponent.getOrderDetailsInfo(o);

    if (null != result && result.getSuccess()) {
        return JackSonUtil.getJson(
            Result.getSuccDataResult(result).getModule());
      }

      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,result.getErrorMsg()));

  }

}
