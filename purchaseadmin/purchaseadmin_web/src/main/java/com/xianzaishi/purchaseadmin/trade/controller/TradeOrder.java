package com.xianzaishi.purchaseadmin.trade.controller;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.trade.vo.OrderStatisticsVO;
import com.xianzaishi.purchaseadmin.client.trade.vo.TradeOrderTagVo;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.trade.TradeOrderComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.StringUtils;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 前台交易订单
 * 
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/tradeorder")
public class TradeOrder extends BaseController {

  private static final Logger logger = Logger.getLogger(TradeOrder.class);

  @Resource
  HttpServletRequest request;
  
  @Autowired
  private TradeOrderComponent tradeOrderComponent;

  @Autowired
  private UserComponent userComponent;
  
  /**
   * 查询订单详细
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/getorder", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getOrder(@RequestParam String data) {
    Long id = (Long) JackSonUtil.jsonToObject(data, Long.class);

    OrderVO queryResult = tradeOrderComponent.getOrder(id);
    return JackSonUtil.getJson(Result.getSuccDataResult(queryResult));
  }
  
  /**
   * 通知后台同步订单
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/synorder", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String synOrder(@RequestParam String data,@RequestParam String paytype) {
    long id = Long.valueOf(data);
    short payway = Short.valueOf(paytype);

    OrderVO queryResult = tradeOrderComponent.synOrder(id, payway);
    if(null == queryResult){
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "同步失败请重试"));
    }
    return JackSonUtil.getJson(Result.getSuccDataResult(queryResult));
  }
  
  /**
   * 订单统计，也就是交接班时的收银对账
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/getorderstatistics", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getOrderStatistics(@RequestBody String data) {
    HttpQuery<OrderStatisticsVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, OrderStatisticsVO.class);
    OrderStatisticsVO order = queryObj.getData();
    if(order.getUserId() <=0 || order.getEnd() <= order.getStart() || order.getStart() <= 0){
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "parameter is error"));
    }
    return JackSonUtil.getJson(Result.getSuccDataResult(tradeOrderComponent.getOrderInfoSumInfoTotal(order.getUserId(), order.getStart(), order.getEnd())));
  }
  
  /**
   * 订单统计，也就是交接班时的收银对账
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/getorderstatisticlist", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getOrderStatisticList(@RequestParam String data) {
    logger.error("query para is:"+data);
    HttpQuery<OrderStatisticsVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, OrderStatisticsVO.class);
    String token = queryObj.getToken();
    if(null == userComponent.getUserByToken(token)){
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "parameter is error"));
    }
    OrderStatisticsVO order = queryObj.getData();
    return JackSonUtil.getJson(Result.getSuccDataResult(tradeOrderComponent.getOrderInfoSumInfoList(order.getUserId(), order.getStart(), order.getEnd())));
  }

  /**
   * 给订单打标
   */
  @RequestMapping(value = "/setTag", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String setTag(@RequestBody String data, HttpServletResponse response) {
    String result = null;
    try {
      logger.error("data=" + data);
      StringUtils.isEmptyString(data,"参数不能为空");
      HttpQuery<TradeOrderTagVo> httpQuery =
          BaseActionAdapter.processObjectParameter(data, TradeOrderTagVo.class);
      if (!BaseActionAdapter.isRightParameter(httpQuery)) {
        throw new BizException("参数不正确");
      }
      result =  JackSonUtil.getJson(Result.getSuccDataResult(tradeOrderComponent
          .setTag2Order(httpQuery.getData())));
    } catch (BizException e) {
      result = JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    } catch (Exception e) {
      result = JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "unknow error"));
    }
    return result;
  }


}
