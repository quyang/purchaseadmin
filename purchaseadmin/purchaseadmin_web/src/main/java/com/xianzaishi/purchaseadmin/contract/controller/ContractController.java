package com.xianzaishi.purchaseadmin.contract.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 合同详情、合同列表逻辑控制
 * 
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/contract")
public class ContractController {


//  /**
//   * 提交注册和提交更新数据接口
//   * 
//   * @param supplierInfo
//   * @return
//   */
//  @RequestMapping(value = "/registe", method = RequestMethod.POST)
//  @ResponseBody
//  public String registe(@RequestParam String supplierInfo) {
//    String url = request.getRequestURI();
//    return url;
//  }
//
//  /**
//   * 提交审核动作
//   * 
//   * @param supplierInfo
//   * @return
//   */
//  @RequestMapping(value = "/check", method = RequestMethod.POST)
//  @ResponseBody
//  public String check(@RequestParam String supplierInfo) {
//    String url = request.getRequestURI();
//    return url;
//  }
//
//  /**
//   * 查看详情。
//   * 
//   * @param id
//   * @return
//   */
//  @RequestMapping(value = "/detail", method = RequestMethod.GET)
//  @ResponseBody
//  public String detail(@RequestParam String id) {
//    String url = request.getRequestURI();
//    // TODO add current user role display page button
//    return url + "===" + id;
//  }
//
//  /**
//   * 查看list页面
//   * 
//   * @param supplierQuery
//   * @return
//   */
//  @RequestMapping(value = "/list", method = RequestMethod.GET)
//  @ResponseBody
//  public String list(@RequestParam String supplierQuery) {
//    String url = request.getRequestURI();
//    // TODO query add current user role
//    // TODO return add current user role display operator
//    return url + "===" + supplierQuery;
//  }
}
