package com.xianzaishi.purchaseadmin.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

@Controller
@RequestMapping("/test")
public class TestController {
	
	@RequestMapping("/testController")
	@ResponseBody
	public String  myTest() {
		System.out.println("测试数据");
		return JackSonUtil.getJson(Result.getSuccDataResult(null));
	}
	

}
