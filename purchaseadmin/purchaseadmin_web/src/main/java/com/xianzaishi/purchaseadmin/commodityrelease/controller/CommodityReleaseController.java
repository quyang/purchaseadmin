package com.xianzaishi.purchaseadmin.commodityrelease.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.item.ItemReleaseComponent;

/**
 * 后台商品发布接口
 * @author dongpo
 *
 */
@Controller
@RequestMapping(value = "/commodityrelease")
public class CommodityReleaseController {
  
  @Autowired
  private ItemReleaseComponent itemReleaseComponent;
  
  @RequestMapping(value = "/querycategories",produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String queryCategories(Integer parentId,HttpServletResponse response){
    String result = itemReleaseComponent.queryCategories(parentId);
    return result;
  }

}
