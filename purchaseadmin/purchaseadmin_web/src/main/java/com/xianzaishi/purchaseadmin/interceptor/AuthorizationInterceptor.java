package com.xianzaishi.purchaseadmin.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.common.SpringContextUtil;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

/**
 * 自定义拦截器，判断此次请求是否有权限
 * 
 * @see com.scienjus.authorization.annotation.Authorization
 * @author ScienJus
 * @date 2015/7/30.
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

  private static final Logger logger = Logger.getLogger(AuthorizationInterceptor.class);
  
  @Autowired
  private UserComponent userComponent;

  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    // 如果不是映射到方法直接通过
    if (!(handler instanceof HandlerMethod)) {
      return true;
    }
    Object user = request.getAttribute(BaseController.USER_KEY);
    Object roleList = request.getAttribute(BaseController.URL_REQUEST_ROLE_LIST_KEY);
    if (null == roleList) {
      return true;
    }
    if (null == user) {
      responseLogin(response);
      return false;
    } else {
      BackGroundUserDTO userInfo = (BackGroundUserDTO) user;
      List<Integer> userRoleList = userInfo.queryRoleList();

      List<RoleDTO> requireRoleList = (List<RoleDTO>) roleList;
      if (CollectionUtils.isEmpty(userRoleList)) {
        return true;
      }
      for (RoleDTO role : requireRoleList) {
        int id = role.getRoleId();
        if (userRoleList.contains(id)) {
          return true;
        }
      }
      responseAuthFailed(response);
      return false;
    }
  }

  private void responseLogin(HttpServletResponse response) throws IOException {
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/json; charset=utf-8");
    if (SpringContextUtil.isOnline()) {
      logger.error("Display online");
//      response.addHeader("Access-Control-Allow-Origin", "http://erp.xianzaishi.com");
    }else{
      logger.error("Display daily");
//      response.addHeader("Access-Control-Allow-Origin", "http://erp.xianzaishi.net");
    }
//    response.addHeader("Access-Control-Allow-Credentials", "true");
    String result = JackSonUtil.getJson(Result.getErrDataResult(401, "require login"));
    PrintWriter out = null;
    try {
      out = response.getWriter();
      out.println(result);
      logger.error("response end");
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (out != null) {
        out.close();
      }
    }
  }

  private void responseAuthFailed(HttpServletResponse response) {
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/json; charset=utf-8");
    if (SpringContextUtil.isOnline()) {
      logger.error("Display online");
    }else{
      logger.error("Display daily");
    }
    String result = JackSonUtil.getJson(Result.getErrDataResult(403, "用户权限不足，请重新登录后重试"));
    PrintWriter out = null;
    try {
      out = response.getWriter();
      out.println(result);
      logger.error("response end");
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (out != null) {
        out.close();
      }
    }
  }

}
