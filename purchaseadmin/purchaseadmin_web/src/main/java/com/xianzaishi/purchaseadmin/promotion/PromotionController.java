package com.xianzaishi.purchaseadmin.promotion;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.GsonUtil;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.promotion.vo.SetDiscountQuery;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.promotion.PromotionComponent;

/**
 * 采购单接口
 * 
 * @author dongpo
 *
 */
@Controller
@RequestMapping(value = "/promotion")
public class PromotionController extends BaseController {

  private static final Logger LOGGER = Logger.getLogger(PromotionController.class);

  @Autowired
  private PromotionComponent promotionComponent;
  
  @RequestMapping(value = "/addDiscountCoupon")
  @ResponseBody
  @Authorization
  public String addDiscountCoupon(@RequestBody String param,HttpServletResponse response){
	HttpQuery<Map> couponParam = BaseActionAdapter.processObjectParameter(param, Map.class);
	if (!BaseActionAdapter.isRightParameter(couponParam)) {
	  return JackSonUtil.getJson(Result.getErrDataResult(
	      ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
	}
	
	return JackSonUtil.getJson(
			promotionComponent.addUserCoupon(
					new BigDecimal(couponParam.getData().get("userId").toString()).longValue(), 
					new BigDecimal(couponParam.getData().get("couponId").toString()).intValue()
					)
			);  
  }
  
  @RequestMapping(value = "/detail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String getPromotionDetailBySkuId(@RequestBody String para, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    HttpQuery<Long> skuId = BaseActionAdapter.processObjectParameter(para, Long.class);
    if((!BaseActionAdapter.isRightParameter(skuId)) || (null == skuId.getData()) || (skuId.getData()<=0)){
      LOGGER.error("请求参数为：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    return GsonUtil.getJson(promotionComponent.getPromotionItemBySkuId(skuId.getData()));
  }

  @RequestMapping(value = "/set", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String setSkuPromotion(@RequestBody String para,HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    HttpQuery<SetDiscountQuery> queryPara = BaseActionAdapter.processObjectParameter(para, SetDiscountQuery.class);
    if((!BaseActionAdapter.isRightParameter(queryPara)) || (null == queryPara.getData())){
      LOGGER.error("请求参数为：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    return GsonUtil.getJson(promotionComponent.setDiscount(queryPara.getData()));
  }
}
