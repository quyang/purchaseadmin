package com.xianzaishi.purchaseadmin;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

/**
 * 定义基类
 * 
 * @author zhancang
 */
public class BaseController {

  private static final Logger logger = Logger.getLogger(BaseController.class);
  
  public static final String URL_REQUEST_ROLE_LIST_KEY = "REQUIREROLELIST";

  public static final String USER_KEY = "USER";

  @Resource
  public HttpServletRequest request;

  public List<RoleDTO> getRequireRoleList() {
    Object roleList = request.getAttribute(URL_REQUEST_ROLE_LIST_KEY);
    if (null == roleList) {
      return Collections.emptyList();
    } else {
      return (List<RoleDTO>) roleList;
    }
  }

  public BackGroundUserDTO getUser() {
    Object user = request.getAttribute(USER_KEY);
    if (null == user) {
      return null;
    } else {
      return (BackGroundUserDTO) user;
    }
  }
  
  protected int getUserId(){
    BackGroundUserDTO user = getUser();
    int userId = 1;
    if(null != user){
      userId = user.getUserId();
    }
    logger.error("Get userid is:"+userId);
    return userId;
  }
}
