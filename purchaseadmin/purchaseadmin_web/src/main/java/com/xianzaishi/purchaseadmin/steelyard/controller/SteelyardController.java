package com.xianzaishi.purchaseadmin.steelyard.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.purchaseadmin.component.steelyard.SteelyardComponent;

@Controller
@RequestMapping(value = "/steelyard")
public class SteelyardController {
  
  @Autowired
  private SteelyardComponent steelyardComponent;
  
  @RequestMapping(value = "/querycommodiesall", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String requestCommodiesAll(Integer pageNum, HttpServletResponse response){
    if(null == pageNum){
      pageNum = 0;
    }
    return JackSonUtil.getJson(steelyardComponent.queryCommodiesAll(pageNum));
  }
  
  @RequestMapping(value = "/querycommodies", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String requestCommodies(){
    return JackSonUtil.getJson(steelyardComponent.queryItemCommodies());
  }

}
