package com.xianzaishi.purchaseadmin.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain.PageType;

/**
 * 自定义拦截器，针对跨域的增加跨域逻辑
 * 
 * @see com.scienjus.authorization.annotation.Authorization
 * @author ScienJus
 * @date 2015/7/30.
 */
public class CrossdomainInterceptor extends HandlerInterceptorAdapter {

  private static final Logger logger = Logger.getLogger(CrossdomainInterceptor.class);
  
  private String env;
  
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    // 如果不是映射到方法直接通过
    if (!(handler instanceof HandlerMethod)) {
      return true;
    }
    HandlerMethod handlerMethod = (HandlerMethod) handler;
    Method method = handlerMethod.getMethod();
    if (method.getAnnotation(Crossdomain.class) != null) {
      Crossdomain cros = method.getAnnotation(Crossdomain.class);
      if(cros.targetDomain() != null){
        String pageName = null;
        if(PageType.HOME_PAGE.equals(cros.targetDomain())){
          pageName = "http://www.xianzaishi."+getNetEnvInfo();
        }else if(PageType.ERP_PAGE.equals(cros.targetDomain())){
          pageName = "http://erp.xianzaishi."+getNetEnvInfo();
        }else if(PageType.ERP_DEV.equals(cros.targetDomain())){
          pageName = "http://erp-dev.xianzaishi."+getNetEnvInfo();
        }
        if(StringUtils.isNotEmpty(pageName)){
//          response.addHeader("Access-Control-Allow-Origin", pageName);
//          response.addHeader("Access-Control-Allow-Credentials", "true");
        }
      }
    }
    return true;
  }

  private String getNetEnvInfo(){
    if(StringUtils.isNotEmpty(getEnv()) && (!"pre".equals(getEnv()))){
      return "net";
    }else{
      return "com";
    }
  }
  public String getEnv() {
    return env;
  }

  public void setEnv(String env) {
    this.env = env;
  }
}
