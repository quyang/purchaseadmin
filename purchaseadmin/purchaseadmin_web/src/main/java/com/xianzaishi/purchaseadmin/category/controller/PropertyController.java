package com.xianzaishi.purchaseadmin.category.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.category.CategoryComponent;

/**
 * 属性可见性逻辑控制 发布商品入口逻辑控制
 * 
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/property")
public class PropertyController {

  @Autowired
  private CategoryComponent categoryComponent;
  
  @Autowired
  private HttpServletRequest request;

  /**
   * 按照类目id查询类目+类目下属性。
   * 
   * @param id
   * @return
   */
  @RequestMapping(value = "/queryvalue", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String detail(@RequestParam  String idlist, HttpServletResponse response) {
    String idArray[] = idlist.split(",");
    List<Integer> idList = Lists.newArrayList();
    for(String idStr:idArray){
      if(StringUtils.isNumeric(idStr)){
        idList.add(Integer.valueOf(idStr));
      }
    }
    Map<Integer,String> valueName = categoryComponent.getValueNameByValueIdList(idList);
    if(CollectionUtils.isNotEmpty(valueName.keySet())){
      return JackSonUtil.getJson(Result.getSuccDataResult(valueName));
    }else{
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input valueid not exit"));
    }
  }
//
//  /**
//   * 提交属性可见性控制
//   * 
//   * @param supplierInfo
//   * @return
//   */
//  @RequestMapping(value = "/submit", method = RequestMethod.POST)
//  @ResponseBody
//  public String registe(@RequestParam String catDetailInfo) {
//    String url = request.getRequestURI();
//    return url;
//  }
  
//  public static void main(String args[]){
//    QrInfoVO qr = new QrInfoVO();
//    qr.setQrInfoType(QrInfoTypeConstants.MOBILE_PAGE);
//    QrDetailVO qd = new QrDetailVO();
//    qd.setData("detail");
//    Map<String,String> para = Maps.newHashMap();
//    para.put("id", "10201601");
//    qd.setQueryParameter(para);
//    qr.setQueryDetail(qd);
//    System.out.println(JackSonUtil.getJson(qr));
//    
//    QrInfoVO qr2 = new QrInfoVO();
//    qr2.setQrInfoType(QrInfoTypeConstants.URL);
//    QrDetailVO qd2 = new QrDetailVO();
//    qd2.setData("http://www.xianzaishi.com");
//    qd2.setQueryType(QueryTypeConstants.GET);
//    Map<String,String> para2 = Maps.newHashMap();
//    para2.put("id", "10201601");
//    qd2.setQueryParameter(para2);
//    qr2.setQueryDetail(qd2);
//    System.out.println(JackSonUtil.getJson(qr2));
//  }
}
