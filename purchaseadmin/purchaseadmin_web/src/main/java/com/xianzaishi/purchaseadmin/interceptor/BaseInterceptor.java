package com.xianzaishi.purchaseadmin.interceptor;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

import com.xianzaishi.itemcenter.common.MD5Util;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

public class BaseInterceptor implements WebRequestInterceptor {

  private static final Logger logger = Logger.getLogger(BaseInterceptor.class);

  private static final String TOKENKEY = "token";

  @Resource
  HttpServletRequest request;

  @Autowired
  private UserComponent userComponent;

  @Override
  public void preHandle(WebRequest request) throws Exception {
    setUserInfo(request);
    setUrlRequireRole(request);
  }

  @Override
  public void postHandle(WebRequest request, ModelMap model) throws Exception {}

  @Override
  public void afterCompletion(WebRequest request, Exception ex) throws Exception {}

  private void setUserInfo(WebRequest webRequest) {
    String token = this.getRequestToke();
    logger.error("request url:" + request.getRequestURI() + ";token:" + token);
    if (StringUtils.isNotEmpty(token)) {
      BackGroundUserDTO user = userComponent.getUserByToken(token);
      logger.error("用户为"+ JackSonUtil.getJson(user));
      if (null != user) {
        webRequest.setAttribute(BaseController.USER_KEY, user, WebRequest.SCOPE_REQUEST);
      }
    }
  }

  private void setUrlRequireRole(WebRequest webRequest) {
    String url = request.getRequestURI();
    String pageId = MD5Util.getMD5Str(url);
    logger.error("page is:"+url+",pageid"+pageId);
    List<RoleDTO> roleList = userComponent.getPageRequireRole(pageId);
    if (CollectionUtils.isNotEmpty(roleList)) {
      webRequest.setAttribute(BaseController.URL_REQUEST_ROLE_LIST_KEY, roleList,
          WebRequest.SCOPE_REQUEST);
    }
  }

  /**
   * 获取请求中的token
   * 
   * @return
   */
  private String getRequestToke() {
    // web页面通过cookie传递token
    Cookie[] cookieArray = request.getCookies();
    System.out.println(cookieArray);
    if (null != cookieArray && cookieArray.length > 0) {
      for (Cookie cookie : cookieArray) {
        if (null != cookie && TOKENKEY.equals(cookie.getName())) {
          return cookie.getValue();
        }
      }
    }

    // 无线页面通过参数传递token
    String tokenPara = request.getParameter("token");
    if (StringUtils.isNotEmpty(tokenPara)) {
      return tokenPara;
    }
    return "";
  }
}
