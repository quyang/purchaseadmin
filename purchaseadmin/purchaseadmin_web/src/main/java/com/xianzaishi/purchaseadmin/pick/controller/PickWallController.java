package com.xianzaishi.purchaseadmin.pick.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.component.pick.PickWallComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

@Controller
@RequestMapping(value = "pickwall")
public class PickWallController extends BaseController {
  
  @Autowired
  private PickWallComponent pickWallComponent;
  
  @RequestMapping(value = "/getwavebypickingbasketbarcode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getWaveVOByPickingBasketBarcode(String data, HttpServletResponse response){
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, String> requestMap = query.getData();
    String barcode = (String) requestMap.get("barcode");
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickWallComponent.getWaveVOByPickingBasketBarcode(user,barcode));
  }
  
  @RequestMapping(value = "/getpickingwallpositionbybarcode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getPickingWallPositionByBarcode(String data, HttpServletResponse response){
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, String> requestMap = query.getData();
    String barcode = (String) requestMap.get("barcode");
    BackGroundUserDTO user = getUser();
    
    return JackSonUtil.getJson(pickWallComponent.getPickingWallPositionByBarcode(user,barcode));
  }

  @RequestMapping(value = "/assignpickingwallposition", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String assignPickingWallPosition(String data, HttpServletResponse response){
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, String> requestMap = query.getData();
    Long wallPositionId = Long.valueOf((String) requestMap.get("wallPositionId"));
    Long assignmentId = Long.valueOf((String) requestMap.get("assignmentId"));
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickWallComponent.assignPickingWallPosition(user,wallPositionId,assignmentId));
  }
}
