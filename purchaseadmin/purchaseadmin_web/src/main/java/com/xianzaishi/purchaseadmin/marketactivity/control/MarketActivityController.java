package com.xianzaishi.purchaseadmin.marketactivity.control;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.CouponVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.MarketActivityQueryVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.MarketActivityVO;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.marketactivity.MarketActivityComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * erp上营销活动
 *
 * @author quyang
 */
@Controller
@RequestMapping(value = "/marketActivity")
public class MarketActivityController extends BaseController {

  private static final Logger logger = Logger.getLogger(MarketActivityController.class);

  @Autowired
  private MarketActivityComponent couponComponent;

  /**
   * 获取活动
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   *
   */
  @RequestMapping(value = "/getActivityList",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String getActivityList(@RequestBody String date, HttpServletResponse response) {
    //startTime endTime pageSize currentPage

    //活动idid，活动名称name，活动状态status，活动类型type；
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求数据不能为空");
      HttpQuery<MarketActivityQueryVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, MarketActivityQueryVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析数据失败");
      result = couponComponent.getActivityList(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

  /**
   * 获取活动列表
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/deleteActivity",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String deleteActivity(@RequestBody String date, HttpServletResponse response) {
    //id,name
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date,"请求参数为空");
      HttpQuery<MarketActivityVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, MarketActivityVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析参数不正确");
      Assert.notNull(getUser(),"登录者信息有误");
      result = couponComponent.deleteActivity(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }


  /**
   * 获取活动详情
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/getActivityDetail",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String getActivityDetail(@RequestBody String date, HttpServletResponse response) {
    //id,name
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求参数不能为空");
      HttpQuery<MarketActivityVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, MarketActivityVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析数据失败");
      //id
      result = couponComponent.getActivityDetail(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }


  /**
   * 更新活动
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/updateActivity",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String updateActivity(@RequestBody String date, HttpServletResponse response) {
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求数据不能为空");
      HttpQuery<MarketActivityQueryVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, MarketActivityQueryVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析数据失败");
      result = couponComponent.updateActivity(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

  /**
   * 添加活动
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/addActivity",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String addActivity(@RequestBody String date, HttpServletResponse response) {
    //name type(0 表示满件活动 1是限购活动) gmtStartString gmtEndString

    //满减 discount折扣  post是否包邮 discountCount满n件
    //限购 limitedTimeDiscount限时折扣 limitCount限购数
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求数据不能为空");
      HttpQuery<MarketActivityVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, MarketActivityVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析数据失败");
      BackGroundUserDTO user = getUser();
      Assert.notNull(user, "登录者信息有误");
      MarketActivityVO data = httpQuery.getData();
      data.setOperator(user.getUserId());
      result = couponComponent.addActivity(data);
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

  /**
   * 查询优惠券
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/getCoupons",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String getCoupons(@RequestBody String date, HttpServletResponse response) {
    //gmtstartString gmtendString  pageSize currentPage
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求参数不能为空");
      HttpQuery<CouponVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, CouponVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析数据失败");
      result = couponComponent.getCoupons(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

  /**
   * 查询优惠券
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/getCouponDetail",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String getCouponDetail(@RequestBody String date, HttpServletResponse response) {
    //id
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求参数不能为空");
      HttpQuery<CouponVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, CouponVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析数据失败");
      result = couponComponent.getCouponDetail(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

  /**
   * 更新优惠券状态
   * @param date the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/updateCoupons",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String updateCoupons(@RequestBody String date, HttpServletResponse response) {
    Result result = null;
    try {
      logger.error("date=" + date);
      Assert.hasLength(date, "请求数据不能为空");
      HttpQuery<CouponVO> httpQuery = BaseActionAdapter
          .processObjectParameter(date, CouponVO.class);
      Assert.isTrue(BaseActionAdapter.isRightParameter(httpQuery), "解析参数失败");
      result = couponComponent.updateCoupons(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }


  /**
   * 定时轮询活动
   * @param data the request from client
   * @throws IllegalArgumentException when the parameters is not right
   * @throws BizException customed exception containing error msg wo want to throw
   */
  @RequestMapping(value = "/activityTimer",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String activityTimer(@RequestBody String data, HttpServletResponse response) {
    Result result = null;
    try {
      result = couponComponent.timer();
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

}
