package com.xianzaishi.purchaseadmin.pick.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.pick.PickComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.tmscore.vo.PickDetailVO;

@Controller
@RequestMapping(value = "pick")
public class PickController extends BaseController{
  
  @Autowired
  private PickComponent pickComponent;

  @RequestMapping(value = "/getpickedtask", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getPickedTask(String data, HttpServletResponse response) {
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<String> queryData = BaseActionAdapter.processObjectParameter(data, String.class);
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickComponent.getPickedTask(user));
  }
  
  @RequestMapping(value = "/submitpickeddetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  @Authorization
  public String submitPickedDetail(String data, HttpServletResponse response) {
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<PickDetailVO> queryData = BaseActionAdapter.processObjectParameter(data, PickDetailVO.class);
    PickDetailVO pickDetailVO = queryData.getData();
    
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickComponent.submitPickedDetail(pickDetailVO,user));
  }
  
  @RequestMapping(value = "/getpickedtaskcount", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  @Authorization
  public String getPickTask(String data, HttpServletResponse response) {
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<String> queryData = BaseActionAdapter.processObjectParameter(data, String.class);
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickComponent.getPickedTaskCount(user));
  }
}
