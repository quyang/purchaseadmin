package com.xianzaishi.purchaseadmin.wms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchaseadmin.component.wms.WmsCallBackComponent;
import com.xianzaishi.purchaseadmin.item.controller.ItemController;

/**
 * 店内门店通过扫码枪相关逻辑
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/wms")
public class WmsController {

  private static final Logger logger = Logger.getLogger(ItemController.class);

  @Resource
  HttpServletRequest request;

  @Autowired
  private WmsCallBackComponent wmsCallBackComponent;

  /**
   * 员工扫码枪用户登录
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/updateorder", method = RequestMethod.GET,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String updateorder(@RequestParam String id, @RequestParam String key, HttpServletResponse response) {
    if(!"UGdu67o".equals(key)){
        return JackSonUtil.getJson(Result.getErrDataResult(-2, "Input para is error,input is:"+id));
    }
    logger.error("Wmscall back receive:" + id);
    Long orderId = Long.valueOf(id);
    if(null == orderId || orderId <= 0){
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input para is error,input is:"+id));
    }
    return JackSonUtil.getJson(wmsCallBackComponent.updateOrderCallBack(orderId));
  }
}
