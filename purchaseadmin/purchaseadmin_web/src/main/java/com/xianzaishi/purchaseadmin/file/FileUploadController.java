package com.xianzaishi.purchaseadmin.file;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchaseadmin.component.common.SpringContextUtil;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;

@Controller
@RequestMapping(value = "/file")
public class FileUploadController {

  @Autowired
  private UserComponent userComponent;

  @Autowired
  private ItemComponent itemComponent;

  private static final Logger logger = Logger.getLogger(ItemComponent.class);

  @RequestMapping(value = "/upload", produces = {"application/html;charset=UTF-8"})
  public ModelAndView upload(@RequestParam("img") MultipartFile file, String type,
      HttpServletRequest request, HttpServletResponse response) throws IOException,
      InterruptedException {

//    response.addHeader("Access-Control-Allow-Origin", "*"); 
  
    ModelAndView model = new ModelAndView();
    model.setViewName("/file/up.jsp");

    String filePath = null;
    if (SpringContextUtil.isLocal()) {
      logger.error("islocal");
      filePath = "D:/work/input/";
    } else {
      logger.error("isnotlocal");
      filePath = "/usr/works/datainit/";
    }
    if (SpringContextUtil.isOnline()) {
      logger.error("isonline");
      model.addObject("action", "/file/upload");
    } else {
      logger.error("isnotonline");
      model.addObject("action", "/purchaseadmin/file/upload");
    }
    boolean isonline = "/usr/works/datainit/".equals(filePath);

    if (null != file) {
      Result<File> upfile = uploadFile(file, filePath);
      String result = null;
      if (upfile.getSuccess()) {
        if ("0".equals(type)) {
          result = userComponent.addAllSupplier(upfile.getModule());
        } else if ("1".equals(type)) {
          result = itemComponent.testAllProduct(upfile.getModule(), isonline, false);
        } else if ("2".equals(type)) {
          result = itemComponent.testAllProduct(upfile.getModule(), isonline, true);
        }
      } else {
        result = "上传文件过程出现异常，联系开发";
        model.addObject("result", result);
        return model;
      }
      // response.setContentType("text/html;charset=utf8");
      // response.getWriter().write(result);
      model.addObject("result", result);
      return model;
    } else {
      // response.setContentType("text/html;charset=utf8");
      // response.getWriter().write("服务器未收到文件，请重新上传");
      model.addObject("result", "服务器未收到文件，请重新上传");
      return model;
    }
  }

  @RequestMapping(value = "/up")
  public ModelAndView up() throws IOException, InterruptedException {
    ModelAndView model = new ModelAndView();
    model.setViewName("/file/up.jsp");
    if (SpringContextUtil.isOnline()) {
      model.addObject("action", "/file/upload");
    } else {
      model.addObject("action", "/purchaseadmin/file/upload");
    }
    return model;
  }

  // 文件上传
  public Result<File> uploadFile(MultipartFile file, String path) throws IOException {
    String fileName = file.getOriginalFilename();

    File tempFile = null;
    fileName = new Date().getTime() + "_" + Math.random() + "_" + fileName;
    tempFile = new File(path, fileName);
    if (!tempFile.getParentFile().exists()) {
      tempFile.getParentFile().mkdir();
    }
    if (!tempFile.exists()) {
      tempFile.createNewFile();
    }

    file.transferTo(tempFile);
    return Result.getSuccDataResult(tempFile);
  }
}
