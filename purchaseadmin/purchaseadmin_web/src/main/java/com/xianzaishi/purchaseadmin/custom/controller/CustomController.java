package com.xianzaishi.purchaseadmin.custom.controller;

import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskStatusConstants;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.custom.vo.ApplyRefundQueryVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CommitRefoundQueryVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomTaskQueryVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.EmployeeVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.GiveCouponsVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.TaskCheckVO;
import com.xianzaishi.purchaseadmin.client.user.vo.QueryDataVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.custom.CustomComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 客服接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping(value = "/customservice")
public class CustomController extends BaseController {

  @Autowired
  private CustomComponent customComponent;

  private static final Logger logger = Logger.getLogger(CustomComponent.class);

  @RequestMapping(value = "/requesttasks", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String requestTasks(String data, HttpServletResponse response) {
    CustomTaskQueryVO customTaskQueryVO =
        (CustomTaskQueryVO) JackSonUtil.jsonToObject(data, CustomTaskQueryVO.class);
    if (null == customTaskQueryVO) {
      customTaskQueryVO = new CustomTaskQueryVO();
    }
    String result =
        customComponent.queryTasks(customTaskQueryVO.getStart(), customTaskQueryVO.getEnd(),
            customTaskQueryVO.getPageSize(), customTaskQueryVO.getPageNum(),
            customTaskQueryVO.getType(), customTaskQueryVO.getStatus());

    return result;
  }

  //{"version":1,"src":"Eclipse###8.0","data":[10884,11203,11204,11206],"appversion":"1"}¶ new
  @RequestMapping(value = "/gettasks", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String gettasks(@RequestBody String request, HttpServletResponse response) {

    logger.error("request=" + request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求数据为空"));
    }

    HttpQuery<CustomTaskQueryVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, CustomTaskQueryVO.class);

    logger.error("参数="+JackSonUtil.getJson(httpQuery));
    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(
              Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "参数错误"));
    }

    CustomTaskQueryVO customTaskQueryVO = httpQuery.getData();

//    private Short type;
//    private String start;
//    private String end;
//    private Short status;
//    private Integer pageSize;页面大小
//    private Integer pageNum;
    String result =
        customComponent.queryTasks(customTaskQueryVO.getStart(), customTaskQueryVO.getEnd(),
            customTaskQueryVO.getPageSize(), customTaskQueryVO.getPageNum(),
            customTaskQueryVO.getType(), customTaskQueryVO.getStatus());

    return result;
  }

  @RequestMapping(value = "/customdetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String customDetail(Long taskId, HttpServletResponse response){
    if(null == taskId){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    String result = customComponent.queryCustomDetail(taskId);
    return result;
  }


  //new
  @RequestMapping(value = "/getcustomdetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String getcustomdetail(@RequestBody String request, HttpServletResponse response) {
    logger.error("request=" + request);

    HttpQuery<TaskCheckVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, TaskCheckVO.class);

    logger.error("参数=" + JackSonUtil.getJson(httpQuery));
    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    TaskCheckVO httpQueryData = httpQuery.getData();
    Long taskId = httpQueryData.getTaskId();

    // long taskId
    String result = customComponent.queryCustomDetail(taskId);
    return result;
  }

  /**
   * 任务检查
   * @param taskId
   * @param orderId
   * @param skuId
   * @param count
   * @param response
   * @return
   */
  @RequestMapping(value = "/taskcheck", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String taskCheck(Long taskId,String orderId, Long skuId, Integer count, HttpServletResponse response){
    if(null == orderId || null == skuId || null == taskId){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = customComponent.taskCheck(taskId,orderId);
    return result;
  }

  /**
   * 任务检查 new
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/checkTask", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String checkTask(@RequestBody String request, HttpServletResponse response){
    logger.error("request="+request);

    HttpQuery<TaskCheckVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, TaskCheckVO.class);

    logger.error("参数异常="+JackSonUtil.getJson(httpQuery));
    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    TaskCheckVO httpQueryData = httpQuery.getData();
    String orderId = httpQueryData.getOrderId();
    Long taskId = httpQueryData.getTaskId();

    //string orderId,Long taskId
    return customComponent.taskCheck(taskId,orderId);
  }

  /**
   * 确认退款
   * @param orderId
   * @param sum
   * @param refundReason
   * @return
   */
  @RequestMapping(value = "/confirmrefund", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String confirmRefund(String orderId, Double sum, String refundReason){
    if(StringUtils.isEmpty(orderId) || null == sum){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    BackGroundUserDTO customerInfo = getUser();
    String result = customComponent.confirmRefund(orderId, sum, customerInfo, refundReason);
    return result;
  }

  /**
   * 确认退款 new
   */
  @RequestMapping(value = "/commitRefund", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String commitRefund(@RequestBody String request, HttpServletResponse response) {
    logger.error("request=" + request);

    HttpQuery<CommitRefoundQueryVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, CommitRefoundQueryVO.class);

    logger.error("参数="+JackSonUtil.getJson(httpQuery));
    //String orderId, Double sum, String refundReason

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      logger.error("参数异常");
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    CommitRefoundQueryVO httpQueryData = httpQuery.getData();
    String orderId = httpQueryData.getOrderId();
    Double sum = httpQueryData.getSum();
    String refundReason = httpQueryData.getRefundReason();

    BackGroundUserDTO customerInfo = getUser();
    logger.error("用户信息="+JackSonUtil.getJson(customerInfo));
    String result = customComponent.confirmRefund(orderId, sum, customerInfo, refundReason);
    return result;
  }
  
  /**
   * 赠送优惠券
   * @param couponId
   * @param userId
   * @return
   */
  @RequestMapping(value = "/giftcoupons", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String giftCoupons(Long couponId,Long userId){
    if(null == couponId || null == userId){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = customComponent.giftCoupons(couponId,userId,null);
    return result;
  }


  /**
   * 赠送优惠券 new
   * @param request
   * @return
   */
  @RequestMapping(value = "/givecoupons", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String givecoupons(@RequestBody String request, HttpServletResponse response){
    logger.error("request="+request);
    HttpQuery<GiveCouponsVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, GiveCouponsVO.class);

    logger.error("参数="+JackSonUtil.getJson(httpQuery));
    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    GiveCouponsVO httpQueryData = httpQuery.getData();
    Long couponId = httpQueryData.getCouponId();
    Long userId = httpQueryData.getUserId();
    Long taskId = httpQueryData.getTaskId();

    //private Long couponId;
//    private Long userId;
//    long taskId;
    String result = customComponent.giftCoupons(couponId,userId,taskId);
    return result;
  }



  /**
   * 申请售后
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/applyrefund", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String applyRefund(String data,HttpServletResponse response){
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    
    ApplyRefundQueryVO applyRefundQueryVO = (ApplyRefundQueryVO) JackSonUtil.jsonToObject(data, ApplyRefundQueryVO.class);
    String result = customComponent.insertCustomTaskInfo(applyRefundQueryVO);
    return result;
  }


  /**
   * 完成
   * @param taskId
   * @param response
   * @return
   */
  @RequestMapping(value = "/complete", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String taskComplete(Long taskId,HttpServletResponse response){
    if(null == taskId){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Boolean aBoolean = customComponent.taskComplete(taskId);
    if (null != aBoolean && aBoolean) {
      JackSonUtil.getJson(Result.getSuccDataResult(aBoolean));
    }
    return JackSonUtil
        .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"完成订单失败" ));
  }

  /**
   * 完成 new
   */
  @RequestMapping(value = "/completeTask", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String completeTask(@RequestBody String request, HttpServletResponse response) {
    logger.error("request=" + request);

    HttpQuery<TaskCheckVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, TaskCheckVO.class);

    logger.error("参数=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    TaskCheckVO taskCheckVO = httpQuery.getData();
    Long taskId = taskCheckVO.getTaskId();

    //taskId
    Boolean aBoolean = customComponent.taskComplete(taskId);
    if (null != aBoolean && aBoolean) {
      return JackSonUtil.getJson(Result.getSuccDataResult(aBoolean));
    }
    return JackSonUtil
        .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"完成订单失败" ));
  }


  /**
   * 退款任务提交审核
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/commitAudit", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String commitAudit(@RequestBody String request,HttpServletResponse response){
    logger.error("request="+request);
    if(StringUtils.isEmpty(request)){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    HttpQuery<QueryDataVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, QueryDataVO.class);

    logger.error("参数="+JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    QueryDataVO queryData = httpQuery.getData();

    //sum refoudReason taskId=id
    return customComponent.commitAudit(queryData);
  }


  /**
   * 获取审核状态task new
   */
  @RequestMapping(value = "/requestAuditTasks", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String requestAuditTasks(@RequestBody String request, HttpServletResponse response) {

    logger.error("request=" + request);

    HttpQuery<CustomTaskQueryVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, CustomTaskQueryVO.class);

    logger.error("参数+" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    CustomTaskQueryVO customTaskQueryVO = httpQuery.getData();

//    private Short type;
//    private String start;
//    private Integer pageNum;
//    private String end;
//    private Integer pageSize;

    String result =
        customComponent.queryTasks(customTaskQueryVO.getStart(), customTaskQueryVO.getEnd(),
            customTaskQueryVO.getPageSize(), customTaskQueryVO.getPageNum(),
            customTaskQueryVO.getType(), CustomerServiceTaskStatusConstants.AUDIT);

    return result;
  }

  /**
   * 判断登录用户类型
   */
  @RequestMapping(value = "/roleList", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String roleList(@RequestBody String request, HttpServletResponse response) {

    logger.error("request=" + request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求参数为空！"));
    }

    HttpQuery<EmployeeVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, EmployeeVO.class);

    logger.error("参数="+JackSonUtil.getJson(httpQuery));
    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"参数错误" ));
    }

    EmployeeVO employeeVO = httpQuery.getData();

    return customComponent.roleList(employeeVO);

  }
}
