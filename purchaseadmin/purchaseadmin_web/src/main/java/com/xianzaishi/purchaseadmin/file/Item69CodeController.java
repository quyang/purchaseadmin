package com.xianzaishi.purchaseadmin.file;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xianzaishi.purchaseadmin.component.common.SpringContextUtil;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;

@Controller
@RequestMapping(value = "/upitem")
public class Item69CodeController {

  @Autowired
  private UserComponent userComponent;

  @Autowired
  private ItemComponent itemComponent;

  private static final Logger logger = Logger.getLogger(ItemComponent.class);

  @RequestMapping(value = "/up69code", produces = {"application/html;charset=UTF-8"})
  public ModelAndView upload69code(String title, String skuId, String sku69code, String pwd,
      HttpServletRequest request, HttpServletResponse response) throws IOException,
      InterruptedException {

    long skuIdNum = 0;
    long sku69Num = 0;
    if (StringUtils.isNotEmpty(skuId)) {
      skuIdNum = Long.valueOf(skuId);
    }
    if (StringUtils.isNotEmpty(sku69code)) {
      sku69Num = Long.valueOf(sku69code);
    }

    String updateResult = itemComponent.checkUptateSkuCode(title, skuIdNum, sku69Num, pwd);

    ModelAndView model = new ModelAndView();
    model.setViewName("/file/up69code.jsp");
    model.addObject("result", updateResult);

    return model;
  }
//  
//  @RequestMapping(value = "/usercodeaction", produces = {"application/html;charset=UTF-8"})
//  public ModelAndView usercodeaction(String phone,
//      HttpServletRequest request, HttpServletResponse response) throws IOException,
//      InterruptedException {
//
//    long skuIdNum = 0;
//    long sku69Num = 0;
//    if (StringUtils.isNotEmpty(skuId)) {
//      skuIdNum = Long.valueOf(skuId);
//    }
//    if (StringUtils.isNotEmpty(sku69code)) {
//      sku69Num = Long.valueOf(sku69code);
//    }
//
//    String updateResult = itemComponent.checkUptateSkuCode(title, skuIdNum, sku69Num, pwd);
//
//    ModelAndView model = new ModelAndView();
//    model.setViewName("/file/up69code.jsp");
//    model.addObject("result", updateResult);
//
//    return model;
//  }

  @RequestMapping(value = "/up")
  public ModelAndView up() throws IOException, InterruptedException {
    ModelAndView model = new ModelAndView();
    model.setViewName("/file/up69code.jsp");
    if (SpringContextUtil.isOnline()) {
      model.addObject("update69action", "/upitem/up69code");
      model.addObject("usercodeaction", "/upitem/usercodeaction");
    } else {
      model.addObject("update69action", "/purchaseadmin/upitem/up69code");
      model.addObject("usercodeaction", "/purchaseadmin/upitem/usercodeaction");
    }
    return model;
  }
}
