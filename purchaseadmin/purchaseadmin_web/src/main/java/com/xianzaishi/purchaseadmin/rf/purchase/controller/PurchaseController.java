package com.xianzaishi.purchaseadmin.rf.purchase.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.purchase.vo.PurchaseInfoVo;
import com.xianzaishi.purchaseadmin.client.user.vo.CommRequestVo;
import com.xianzaishi.purchaseadmin.client.user.vo.PutPurchaseadminVo;
import com.xianzaishi.purchaseadmin.component.presenter.PresenterComponent;

@Controller
@RequestMapping(value="/purchase")
public class PurchaseController {
	
	 @Autowired
	 private PresenterComponent presenterComponent;
	 
	 
	 
	 /**
	  * 更新子采购单
	  * @param id
	  * @return
	  */
	@RequestMapping(value="/updatePurchaseInfo", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
	 public   Result<PurchaseInfoVo>  updatePurchaseInfo(@RequestBody String putVo){
		if (TextUtils.isEmpty(putVo)) {
			return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"服务器没有获取到任何数据!");
		}
		   PutPurchaseadminVo putPurBean = (PutPurchaseadminVo) JackSonUtil.jsonToBean(putVo, PutPurchaseadminVo.class);
		     boolean resultSt=presenterComponent.upDatePurchaseInfo(putPurBean);
		   
		   return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"服务器没有获取到任何数据!");
	}


	/**
	 * 根据采购单ID查询采购单详细
	 */
	@RequestMapping(value = "/serachPurchaseInfo", produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public Result<PurchaseInfoVo> serachPurchaseInfo(@RequestBody CommRequestVo commRequest) {
		if (commRequest == null || TextUtils.isEmpty(commRequest.getData())) {
			return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "服务器没有获取到任何数据!");
		}
		PurchaseInfoVo searchPurchaseInfoBySubId = presenterComponent
				.searchPurchaseInfoBySubId(Long.valueOf(commRequest.getData()));
		if (searchPurchaseInfoBySubId == null) {
			return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询数据为空!");

		}
		return Result.getSuccDataResult(searchPurchaseInfoBySubId);
	}
	 
	
	 /**
	  * 根据采购单ID查询采购单详细
	  * @param request
	  * @return
	  */
	 @RequestMapping(value = "/serachPurchase", produces = {"application/json;charset=UTF-8"})
	 @ResponseBody
	 public String queryPurchase(String request) {

		 if (StringUtils.isEmpty(request)) {
			 return JackSonUtil
					 .getJson(
							 Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "采购单编号不能为空"));
		 }

		 CommRequestVo commRequestVo = (CommRequestVo) JackSonUtil
				 .jsonToBean(request, CommRequestVo.class);
		 int purchaseId = 1;
		 try {
			 String data = commRequestVo.getData();
			 purchaseId = Integer.valueOf(data);
		 } catch (Exception e) {
			 return JackSonUtil
					 .getJson(
							 Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请输入正确的采购单编号"));
		 }
		 return presenterComponent.queryPresenterById(purchaseId);

	 }

}
