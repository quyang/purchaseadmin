package com.xianzaishi.purchaseadmin.maintenance.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemListVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.item.MaintenanceComponent;

/**
 * 根据title，plu码，69码搜索相关信息
 * 
 * @author wangxiao
 */
@Controller
@RequestMapping(value = "/maintenance")
public class ItemListController {
  @Autowired
  private MaintenanceComponent maintenanceComponent;

  @RequestMapping(value = "/list", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String List(String date) {
    Result<ItemListVO> List = null;
    if (StringUtils.isEmpty(date)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    if (!StringUtils.isNumeric(date)) {
      List = maintenanceComponent.getTitleList(date);
    } else if (date.length() == 5) {
      List = maintenanceComponent.getSkuList(Long.parseLong(date));
    } else {
      List = maintenanceComponent.get69List(Long.parseLong(date));
    }
    return JackSonUtil.getJson(List);
  }
}
