package com.xianzaishi.purchaseadmin.inventory.controller;


import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskTypeConstants;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomTaskQueryVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.TaskInfoVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.OrderUpdateInventoryVO;
import com.xianzaishi.purchaseadmin.component.custom.CustomComponent;
import com.xianzaishi.purchaseadmin.component.inventory.InventoryComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 库存相关接口
 * 
 * @author 展苍
 * 
 */
@Controller
@RequestMapping(value = "/inventory")
public class InventoryController extends BaseController {

  @Autowired
  private InventoryComponent inventoryComponent;

  @Autowired
  private CustomComponent customComponent;

  private static final Logger logger = Logger.getLogger(InventoryController.class);

  /**
   * 客服在订单页面调用退换货时触发，调用后由后台调取审核逻辑
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/orderUpdateInventory", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String orderUpdateInventory(@RequestBody String data) {
    Result result = null;
    try {
      HttpQuery<OrderUpdateInventoryVO> queryObj =
          BaseActionAdapter.processObjectParameter(data, OrderUpdateInventoryVO.class);
      if (!BaseActionAdapter.isRightParameter(queryObj)) {
        throw new BizException("提交参数异常");
      }
      BackGroundUserDTO user = getUser();
      if (null == user) {
        throw new BizException("不存在这样的用户");
      }
      OrderUpdateInventoryVO queryObjData = queryObj.getData();
      queryObjData.setOperate(user.getUserId());
      result = inventoryComponent.returnAndExchangeGoods(queryObjData);
    } catch (BizException e) {
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error("exception,e=" + e.getMessage());
      result = Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
              e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }

  /**
   * 获取差价
   *
   * @param data
   * @return
   */
  @RequestMapping(value = "/getPrice", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getPrice(@RequestBody String data) {
    Result result = null;
    try {
      HttpQuery<OrderUpdateInventoryVO> queryObj =
          BaseActionAdapter.processObjectParameter(data, OrderUpdateInventoryVO.class);
      if (!BaseActionAdapter.isRightParameter(queryObj)) {
        throw new BizException("提交参数异常");
      }
      BackGroundUserDTO user = getUser();
      if (null == user) {
        throw new BizException("不存在这样的用户");
      }
      OrderUpdateInventoryVO queryObjData = queryObj.getData();
      queryObjData.setOperate(user.getUserId());
      result = inventoryComponent.getPrice(queryObjData);
    } catch (BizException e) {
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error("exception,e=" + e.getMessage());
      result = Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
              e.getMessage() == null ? "unknow error" : e.getMessage());
    }
    return JackSonUtil.getJson(result);
  }



  /**
   * 查询待审核出入库记录
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/queryStorageList", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryStorageList(@RequestBody String data) {
    HttpQuery<CustomTaskQueryVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, CustomTaskQueryVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("查询待审核入库数据：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    // 查看客服平台数据，通过客服平台任务获取到符合条件的storageVOID,只需要获取任务类型和storeageId，用于详情页面展示
    // 下面只是mock调用 0 退款 1 rf枪其他原因入库 2 报损出库
    CustomTaskQueryVO customTaskQueryVO = queryObj.getData();
    //需要参数 sart end status pageSize pageNum
    return customComponent
        .queryTasks(customTaskQueryVO.getStart(), customTaskQueryVO.getEnd(),
            customTaskQueryVO.getPageSize(), customTaskQueryVO.getPageNum(),
            CustomerServiceTaskTypeConstants.TYPE_RF_OTHER, customTaskQueryVO.getStatus());
  }


  /**
   * 对入库任务进行审核
   *
   * @param data
   * @return
   */
  @RequestMapping(value = "/processStorageTask", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String audited(@RequestBody String data) {
    String result = null;
    try {
      HttpQuery<CustomTaskQueryVO> queryObj =
          BaseActionAdapter.processObjectParameter(data, CustomTaskQueryVO.class);
      if (!BaseActionAdapter.isRightParameter(queryObj)) {
        logger.error("查询待审核入库数据：" + data);
        throw new BizException("参数错误");
      }
      CustomTaskQueryVO customTaskQueryVO = queryObj.getData();
      BackGroundUserDTO user = getUser();
      if (null == user || null == user.getUserId() || user.getUserId() <= 0) {
        throw new BizException("登录者信息有误");
      }
      customTaskQueryVO.setUserId(Long.parseLong(String.valueOf(user.getUserId())));
//      customTaskQueryVO.setUserId(55L);
      //需要参数 taskId status
      result = inventoryComponent.setAudited(customTaskQueryVO);
    } catch (BizException e) {
      result = JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    } catch (Exception e) {
      result = JackSonUtil
          .getJson(
              Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage() == null ? "unknow error" : e.getMessage()));
    }
    return result;
  }

  /**
   *获取入库明细
   *
   * @param data
   * @return
   */
  @RequestMapping(value = "/queryStorageDetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getStorageDetails(@RequestBody String data) {
    String result = null;
    try {
      HttpQuery<String> queryObj =
          BaseActionAdapter.processObjectParameter(data, String.class);
      if (!BaseActionAdapter.isRightParameter(queryObj)) {
        logger.error("查询待审核入库数据：" + data);
        throw new BizException("参数错误");
      }
      String customTaskQueryVO = queryObj.getData();
      //需要参数 入库id
      result = JackSonUtil.getJson(
          Result.getSuccDataResult(inventoryComponent.getStorageDetails(customTaskQueryVO)));
    } catch (BizException e) {
      result = JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    } catch (Exception e) {
      result = JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage() == null ? "unknow error" : e.getMessage()));
    }
    return result;
  }


  /**
   * 查询待审核其它出库、报损出库记录。该类型客服任务，在设置时设置type=2、type=3
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/queryOutgoingList", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryOutgoingList(@RequestBody String data) {
    HttpQuery<CustomTaskQueryVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, CustomTaskQueryVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("查询待审核出库数据：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    CustomTaskQueryVO customTaskQueryVO = queryObj.getData();
    String resultStr =
        customComponent.queryTasks(customTaskQueryVO.getStart(), customTaskQueryVO.getEnd(),
            customTaskQueryVO.getPageSize(), customTaskQueryVO.getPageNum(),
            customTaskQueryVO.getType(), customTaskQueryVO.getStatus());
    return resultStr;
  }

  /**
   * 查询待审核入库记录详情
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/queryOutgoinDetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryOutgoinDetail(@RequestBody String data) {
    HttpQuery<Long> queryObj = BaseActionAdapter.processObjectParameter(data, Long.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("查询待审核出库详情数据：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    String resultStr = JackSonUtil.getJson(inventoryComponent.getOutgoingVO(queryObj.getData()));
    return resultStr;
  }

  /**
   * 审核报损出库，其它出库
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/processOutgoinTask", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String processOutgoinTask(@RequestBody String data) {
    HttpQuery<TaskInfoVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, TaskInfoVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("审核入库数据：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    // 根据TaskInfoVO的任务id，和设置的任务状态，如果状态设置为成功，则成功结束客服任务，
    // 并且调用rfdock IOutgoingDomainService auditOutgoing

    String resultStr =
        JackSonUtil
            .getJson(inventoryComponent.processOutgoingTask(queryObj.getData(), getUserId()));
    return resultStr;
  }

}
