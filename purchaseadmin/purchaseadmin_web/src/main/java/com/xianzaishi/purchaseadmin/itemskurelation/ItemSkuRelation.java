package com.xianzaishi.purchaseadmin.itemskurelation;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationScheduleVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationVO;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

/**
 * 商品发布页面
 * 
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/skurelation")
public class ItemSkuRelation extends BaseController {

  private static final Logger logger = Logger.getLogger(ItemSkuRelation.class);

  @Resource
  HttpServletRequest request;
  
  @Autowired
  private ItemComponent itemComponent;

  /**
   * 展示进售价维护
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/skurelation", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String skuRelation(@RequestBody String data) {
    HttpQuery<SkuRelationQueryVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, SkuRelationQueryVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("请求首页参数为：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    SkuRelationQueryVO queryVO = queryObj.getData();;
    List<SkuRelationVO> tmp =
        itemComponent.getSkuRelationVOList(itemComponent.getQueryByQueryVO(queryVO));
    int count = itemComponent.countSkuRelationByQueryVO(queryVO);
    int totalPageCount = count/queryVO.getPageSize();
    if(count % queryVO.getPageSize() > 0){
      totalPageCount++;
    }
    
    PagedResult<List<SkuRelationVO>> result = PagedResult.getSuccDataResult(tmp, count, totalPageCount, queryVO.getPageSize(), queryVO.getPageNum());
    if(isNeedCleanInfo()){
      itemComponent.cleanPurchaseInfo(result.getModule());
    }
    
    String resultStr = JackSonUtil.getJson(result);
    return resultStr;
  }

  /**
   * 展示进售价维护，销售价格
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/skurelationSchedule", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String skurelationSchedule(@RequestBody String para) {
    HttpQuery<Map> idPara = BaseActionAdapter.processObjectParameter(para, Map.class);
    if (!BaseActionAdapter.isRightParameter(idPara)) {
      logger.error("请求参数为：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    Object id = idPara.getData().get("id");
    Object type = idPara.getData().get("type");
    if (null == id || null == type || !StringUtils.isNumeric(id.toString())) {
      logger.error("请求参数为：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    List<SkuRelationScheduleVO> result =
        itemComponent.getSkuRelationScheduleList(Long.valueOf(id.toString()),Integer.valueOf(type.toString()));
    if(isNeedCleanInfo() && 0 == Integer.valueOf(type.toString())){
      itemComponent.cleanPurchaseScheduleInfo(result);
    }
    
    return JackSonUtil.getJson(Result.getSuccDataResult(result));
  }
  
  /**
   * 更新进售价维护，销售价格
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/updateskurelationSchedule", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String updateSkuRelation(@RequestBody String data) {
    HttpQuery<SkuRelationVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, SkuRelationVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("请求首页参数为：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    SkuRelationVO skuRelationVO = queryObj.getData();

    boolean isUpdateBuy = false;
    boolean isUpdateSale = true;
    return JackSonUtil.getJson(itemComponent.updateSkuRelationSchedule(skuRelationVO, getUserId(),isUpdateBuy,isUpdateSale));
  }

  /**
   * 更新进售价维护，采购价格
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/updateskurelationBuySchedule", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String updateSkuBuyRelation(@RequestBody String data) {
    HttpQuery<SkuRelationVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, SkuRelationVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("请求首页参数为：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }

    SkuRelationVO skuRelationVO = queryObj.getData();

    boolean isUpdateBuy = true;
    boolean isUpdateSale = false;
    return JackSonUtil.getJson(itemComponent.updateSkuRelationSchedule(skuRelationVO, getUserId(),isUpdateBuy,isUpdateSale));
  }
  
  /**
   * 更新进售价维护
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/emergencyupdateskurelation", method = RequestMethod.POST,produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String emergencyUpdateSkuRelation(@RequestBody String data, HttpServletResponse response) {
    HttpQuery<SkuRelationVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, SkuRelationVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("请求首页参数为：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    SkuRelationVO skuRelationVO = queryObj.getData();;
    boolean isUpdateBuy = false;
    boolean isUpdateSale = true;
    skuRelationVO.setSaleStartTime(System.currentTimeMillis());
    Result<Boolean> updateResult = itemComponent.updateSkuRelationScheduleWhenEmergencyUpdatePrice(skuRelationVO, getUserId(),isUpdateBuy,isUpdateSale);
    if(null == updateResult || !updateResult.getSuccess() || !updateResult.getModule()){
      logger.error("emergencyUpdateSkuRelation update failed:"+JackSonUtil.getJson(updateResult));
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "变价失败，请重试"));
    }
    return JackSonUtil.getJson(itemComponent.updateSkuRelationVO(skuRelationVO, true, getUserId(),isUpdateBuy,isUpdateSale));
  }
  
  /**
   * 更新进售价维护,采购价格
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/emergencyupdatebuyskurelation", method = RequestMethod.POST,produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String emergencyUpdateBuySkuRelation(@RequestBody String data, HttpServletResponse response) {
    HttpQuery<SkuRelationVO> queryObj =
        BaseActionAdapter.processObjectParameter(data, SkuRelationVO.class);
    if (!BaseActionAdapter.isRightParameter(queryObj)) {
      logger.error("请求首页参数为：" + data);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    SkuRelationVO skuRelationVO = queryObj.getData();;
    boolean isUpdateBuy = false;
    boolean isUpdateSale = true;
    skuRelationVO.setBuyStartTime(System.currentTimeMillis());
    itemComponent.updateSkuRelationSchedule(skuRelationVO, getUserId(),isUpdateBuy,false);
    return JackSonUtil.getJson(itemComponent.updateSkuRelationVO(skuRelationVO, true, getUserId(),isUpdateBuy,isUpdateSale));
  }
  
  private boolean isNeedCleanInfo(){
    Object roleList = request.getAttribute(BaseController.URL_REQUEST_ROLE_LIST_KEY);
    List<RoleDTO> requireRoleList = (List<RoleDTO>) roleList;
    if(CollectionUtils.isEmpty(requireRoleList)){
      return false;
    }
    Object user = request.getAttribute(BaseController.USER_KEY);
    if(null == user){
      return true;
    }
    BackGroundUserDTO userInfo = (BackGroundUserDTO) user;
    List<Integer> userRoleList = userInfo.queryRoleList();
    
    if(userRoleList.contains(89) && !userRoleList.contains(61)){
      return true;
    }else{
      return false;
    }
  }
}
