package com.xianzaishi.purchaseadmin.monitor;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.monitor.vo.MonitorInformationVO;
import com.xianzaishi.purchaseadmin.component.monitor.MonitorComponent;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 监控
 *
 * @author quyang
 */
@Controller
@RequestMapping(value = "/monitor")
public class MonitorController extends BaseController {

  private static final Logger logger = Logger.getLogger(MonitorController.class);


  @Autowired
  private MonitorComponent monitorComponent;


  /**
   * 插入一条监控信息
   */
  @RequestMapping(value = "/insertLog", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String insertLog(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    //name info type
    logger.error("request=" + request);

    HttpQuery<MonitorInformationVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, MonitorInformationVO.class);

    logger.error("请求解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    MonitorInformationVO data = httpQuery.getData();

    return monitorComponent.insertLog(data);

  }



  /**
   * 更新一条监控信息
   */
  @RequestMapping(value = "/updateLog", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String updateLog(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    //name  type
    logger.error("request=" + request);

    HttpQuery<MonitorInformationVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, MonitorInformationVO.class);

    logger.error("请求解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    MonitorInformationVO data = httpQuery.getData();

    return monitorComponent.updateLog(data);


  }


  /**
   * 更新一条监控配置信息
   */
  @RequestMapping(value = "/updateLogConfig", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String updateLogConfig(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    //  type      time_scope phone
    logger.error("request=" + request);

    HttpQuery<MonitorInformationVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, MonitorInformationVO.class);

    logger.error("请求解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    MonitorInformationVO data = httpQuery.getData();

    return monitorComponent.updateLogConfig(data);

  }

  /**
   * 插入一条监控配置信息
   */
  @RequestMapping(value = "/insertLogConfig", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String insertLogConfig(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    //  type time_scope phone
    logger.error("request=" + request);

    HttpQuery<MonitorInformationVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, MonitorInformationVO.class);

    logger.error("请求解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    MonitorInformationVO data = httpQuery.getData();

    return monitorComponent.insertLogConfig(data);

  }

  /**
   * 插入或更新一条监控信息
   */
  @RequestMapping(value = "/insertOrUpdateLog", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String insertOrUpdateLog(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    //type and isOnlyUpdate
    logger.error("request=" + request);

    HttpQuery<MonitorInformationVO> httpQuery = BaseActionAdapter
        .processObjectParameter(request, MonitorInformationVO.class);

    logger.error("请求解析结果=" + JackSonUtil.getJson(httpQuery));

    if (!BaseActionAdapter.isRightParameter(httpQuery)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    MonitorInformationVO data = httpQuery.getData();

    return monitorComponent.insertOrUpdateLog(data);

  }
}
