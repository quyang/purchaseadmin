package com.xianzaishi.purchaseadmin.organization.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.component.organizaion.OrganizationComponent;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;

@Controller
@RequestMapping(value = "/organization")
public class OrganizationController {
  
  @Autowired
  private OrganizationComponent organizationComponent;

  @RequestMapping(value = "/queryshopinfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String requestShopInfo(String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");

    Integer shopId = (Integer) JackSonUtil.jsonToObject(data, Integer.class);
    OrganizationDTO organizationDTO = organizationComponent.getOrganizationDTOById(shopId);
    return JackSonUtil.getJson(organizationDTO);
  }

}
