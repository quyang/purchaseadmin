package com.xianzaishi.purchaseadmin.pick.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.pick.PickBasketComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

@Controller
@RequestMapping(value = "pickbasket")
public class PickBasketController extends BaseController {
  
  @Autowired
  private PickBasketComponent pickBasketComponent;

  @RequestMapping(value = "/getpickingbasketbybarcode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String getPickingBasketByBarcode(String data, HttpServletResponse response) {
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, String> requestMap = query.getData();
    String barcode = (String) requestMap.get("barcode");
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickBasketComponent.getPickingBasketByBarcode(user,barcode));
  }
  
  @RequestMapping(value = "/recievepickingbasket", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String recievePickingBasket(String data, HttpServletResponse response) {
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    HttpQuery<Map> query = BaseActionAdapter.processObjectParameter(data, Map.class);
    Map<String, String> requestMap = query.getData();
    String barcode = (String) requestMap.get("barcode");
    BackGroundUserDTO user = getUser();
    return JackSonUtil.getJson(pickBasketComponent.recievePickingBasket(user,barcode));
  }

}
