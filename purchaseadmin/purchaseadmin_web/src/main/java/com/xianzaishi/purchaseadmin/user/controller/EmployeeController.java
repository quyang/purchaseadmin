package com.xianzaishi.purchaseadmin.user.controller;

import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.erpapp.vo.ERPQueryVO;
import com.xianzaishi.purchaseadmin.client.user.vo.EmployeeQueryVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderReturnVO;
import com.xianzaishi.purchaseadmin.client.user.vo.ResultVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserVO;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchaseadmin.item.controller.ItemController;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;





/**
 * 店内门店通过扫码枪相关逻辑
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {

  private static final Logger logger = Logger.getLogger(ItemController.class);

  @Resource
  HttpServletRequest request;

  @Autowired
  private UserComponent userComponent;

  /**
   * 员工扫码枪用户登录
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/login", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Crossdomain
  public String login(String data, HttpServletResponse response) {
    Long start = new Date().getTime();
    logger.error("CashierLog:员工登陆接口开始调用");
    String postInfo = "post receive:" + data;
    UserVO userVO = (UserVO) JackSonUtil.jsonToObject(data, UserVO.class);
    userVO.setUserType((short) 0);
    Result<BackGroundUserDTO> loginResult = userComponent.employeelogin(userVO);
    if(null == loginResult){
      logger.error(postInfo+".login failed,return null");
    }else if(!loginResult.getSuccess()){
      logger.error(postInfo+".login failed,return code:"+loginResult.getResultCode()+",msg:"+loginResult.getModule());
    }else{
      logger.error(postInfo+".login success,token:"+loginResult.getModule());
    }
    Long end = new Date().getTime();
    Long takeTime = end - start;
    logger.error("CashierLog:员工登陆接口调用结束,耗时：[" + takeTime + "]ms");
    return JackSonUtil.getJson(Result.getSuccDataResult(loginResult.getModule().getToken()));
  }

  /**
   * 员工扫码枪退出
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/logout", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String logout(String data, HttpServletResponse response) {
    logger.error("post receive:" + data);
    UserVO userVO = (UserVO) JackSonUtil.jsonToObject(data, UserVO.class);
    return JackSonUtil.getJson(userComponent.logout(userVO));
  }
  
  /**
   * 员工扫码枪用户登录
   * 
   * @param
   * @return
   */
  @RequestMapping(value = "/organizationinfo", method = RequestMethod.GET,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String organizationinfo(String token, HttpServletResponse response) {
    return JackSonUtil.getJson(userComponent.getOrganizationName(token));
  }

  /**
   *获取订单信息 erp app
   *
   * @param request
   */
  @RequestMapping(value = "/getERPOrderList", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getERPOrderList(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    logger.error("参数，request="+request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"请求参数为空"));
    }

    //线上线下
    ERPQueryVO query = (ERPQueryVO) JackSonUtil.jsonToObject(request, ERPQueryVO.class);

    Result<HashMap<String, List<OrderReturnVO>>> result = userComponent
        .getUndealedOrderList(query);
    if (null != result && result.getSuccess()) {
      return JackSonUtil.getJson(
          Result.getSuccDataResult(result.getModule()));
    }

    return JackSonUtil.getJson(
        Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,result.getErrorMsg()));

  }

  /**
   *获取订单信息 erp app   根据时间和订单id或手机号
   *
   * @param request
   */
  @RequestMapping(value = "/getOrderListByTimeOrderID", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getOrderListByTimeOrderID(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    logger.error("参数，request="+request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"请求参数为空"));
    }

    // time orderId phone
    ERPQueryVO query = (ERPQueryVO) JackSonUtil.jsonToObject(request, ERPQueryVO.class);

    Result<HashMap<String, List<OrderReturnVO>>> result = userComponent
        .getOrderListByTimeOrOrderID(query);
    if (null != result && result.getSuccess()) {
      return JackSonUtil.getJson(
          Result.getSuccDataResult(result.getModule()));
    }

    return JackSonUtil.getJson(
        Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,result.getErrorMsg()));

  }

  /**
   * 订单加工完成 oid
   */
  @RequestMapping(value = "/completedOrder", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String completedOrder(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    logger.error("参数，request=" + request);

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"请求参数为空"));
    }

    // orderId
    ERPQueryVO query = (ERPQueryVO) JackSonUtil.jsonToObject(request, ERPQueryVO.class);

    Result<Boolean> result = userComponent.updateOrder(query);
    if (null != result && result.getSuccess() && result.getModule()) {
      return JackSonUtil.getJson(
          Result.getSuccDataResult(result.getModule()));
    }

    return JackSonUtil.getJson(
        Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, result.getErrorMsg()));

  }

  /**
   * 员工erp app用户登录
   */
  @RequestMapping(value = "/loginerp", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String loginerp(String request, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
              "请求参数异常"));
    }

    logger.error("参数:" + request);

    EmployeeQueryVO userVO = (EmployeeQueryVO) JackSonUtil
        .jsonToObject(request, EmployeeQueryVO.class);
    Result<ResultVO> loginResult = userComponent.employeeloginERP(userVO);
    if (null != loginResult && loginResult.getSuccess() && null != loginResult.getModule()) {
      return JackSonUtil.getJson(Result.getSuccDataResult(loginResult.getModule()));
    }

    return JackSonUtil
        .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, loginResult.getErrorMsg()));
  }

  /**
   * 发送验证码接口
   *
   * @param  request
   * @return
   */
  @RequestMapping(value = "/sendcheckcode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String sendCheckCode(String request, HttpServletResponse response) {
    logger.error("数据:" + request);
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");

    if (StringUtils.isEmpty(request)) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
              "请求参数异常"));
    }

    EmployeeQueryVO query = (EmployeeQueryVO) JackSonUtil
        .jsonToObject(request, EmployeeQueryVO.class);

    logger.error("手机号==" + query.getData().getPhone());

    Result<Boolean> result = userComponent.sendCode(query);

    if (null != result && result.getSuccess() && null != result.getModule() && result.getModule()) {
      return JackSonUtil.getJson(Result.getSuccDataResult(true));
    }

    return JackSonUtil
        .getJson(Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
            result.getErrorMsg()));

  }
}
