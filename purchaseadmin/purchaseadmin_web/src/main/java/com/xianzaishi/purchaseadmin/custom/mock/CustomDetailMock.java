package com.xianzaishi.purchaseadmin.custom.mock;

import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;
import com.xianzaishi.customercenter.client.customerservice.dto.ProofDataDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomDetailVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomInfoVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.OrderInfoVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.TaskInfoVO;

public class CustomDetailMock {
  private static final String[] title = {"苹果2斤5个","三文鱼","哈密瓜"};
  private static final String[] price = {"21元","100元","5元"};
  private static final Double[] count = {(double) 3,(double) 1,(double) 5};
  private static final String[] coupon = {"满5减1优惠券","满100减20优惠券","满50减10优惠券"};
  
  public String queryCustomDetail(Long id){
    
    CustomInfoVO customInfoVO = getCustomInfo(id);
    TaskInfoVO taskInfoVO = getTaskInfo(id);
    List<OrderInfoVO> orderInfoVOs = getOrderInfo(id);
    List<String> coupons = getCoupon();
    CustomDetailVO customDetailVO = new CustomDetailVO();
//    customDetailVO.setCoupons(coupons);
    customDetailVO.setCustomInfo(customInfoVO);
    customDetailVO.setOrderInfo(orderInfoVOs);
    customDetailVO.setTaskInfo(taskInfoVO);
    return JackSonUtil.getJson(Result.getSuccDataResult(customDetailVO));
  }
  
  private CustomInfoVO getCustomInfo(Long id){
    CustomInfoVO customInfoVO = new CustomInfoVO();
    int radom = (int) (Math.random() * 20) ;
    Long phone = 18688888888L;
    customInfoVO.setPhone(phone + radom);
    customInfoVO.setName(String.valueOf(phone + radom));
    customInfoVO.setAddress("上海市浦东新区东方路东方红大厦1号");
    
    return customInfoVO;
  }
  
  private TaskInfoVO getTaskInfo(Long id){
    TaskInfoVO taskInfoVO = new TaskInfoVO();
    taskInfoVO.setType("交易退款");
//    taskInfoVO.setGmtCreate(new Date());
    taskInfoVO.setOrderId(id);
    ProofDataDTO proofDataDTO = new ProofDataDTO();
    proofDataDTO.setId(1);
    proofDataDTO.setSource("你好，从你们家买的葡萄坏了，有图为证：");
    proofDataDTO.setType(1);
    ProofDataDTO proofDataDTO1 = new ProofDataDTO();
    proofDataDTO1.setId(2);
    proofDataDTO1.setSource("http://img.blog.163.com/photo/LhDRPfYG4jyQ5RNsbGCbsQ==/2546785589278833838.jpg");
    proofDataDTO1.setType(2);
    List<ProofDataDTO> proofDataDTOs = Lists.newArrayList();
    proofDataDTOs.add(proofDataDTO);
    proofDataDTOs.add(proofDataDTO1);
    taskInfoVO.setTaskDescription(proofDataDTOs);
    
    return taskInfoVO;
  }
  
  private List<OrderInfoVO> getOrderInfo(Long id){
    List<OrderInfoVO> orderInfoVOs = Lists.newArrayList();
    for(int i = 0;i < 3;i++){
      OrderInfoVO orderInfoVO = new OrderInfoVO();
      orderInfoVO.setSkuId((long) (1 + i));
      orderInfoVO.setTitle(title[i]);
      orderInfoVO.setTotalPrice(price[i]);
      orderInfoVO.setCount(count[i]);
      orderInfoVOs.add(orderInfoVO);
    }
    return orderInfoVOs;
  }
  
  private List<String> getCoupon(){
    List<String> coupons = Lists.newArrayList();
    for(int i = 0; i < 3; i++){
      coupons.add(coupon[i]);
    }
    return coupons;
  }
  
  public String taskCheck(String orderId,Long skuId,Integer count){
    String result = null;
    if(skuId == 1L){
      result = "返还用户满5减1优惠券，返还用户"+count+"元";
    }else if(skuId == 2L){
      result = "返还用户满100减20优惠券,用户还需付款" + count*10 + "元";
    }else{
      result = "返还用户满5减1优惠券，返还用户4元";
    }
    
    return result;
  }

}
