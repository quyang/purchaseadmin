package com.xianzaishi.purchaseadmin.cashier.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.cashier.vo.KitchenSkuQuery;
import com.xianzaishi.purchaseadmin.client.cashier.vo.PayOrderQuery;
import com.xianzaishi.purchaseadmin.client.item.vo.CommodityItemVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserVO;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.purchaseadmin.component.organizaion.OrganizationComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

@Controller
@RequestMapping(value = "/cashier")
public class CashierController {

  private static final Logger logger = Logger.getLogger(CashierController.class);

  @Autowired
  private HttpServletRequest request;

  @Autowired
  private ItemComponent itemComponent;

  @Autowired
  private OrganizationComponent organizationComponent;

  @Autowired
  private UserComponent userComponent;

  /**
   * 收银台查询sku
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryskudetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryskudetail(@RequestBody String para) {
    long start = System.currentTimeMillis();
    HttpQuery<String> idPara = BaseActionAdapter.processObjectParameter(para, String.class);
    if (!BaseActionAdapter.isRightParameter(idPara)) {
      logger.error("收银台请求数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    Result<CommodityItemVO> commodityItemVO = itemComponent.getCommodityItemVO(idPara.getData());
    long end = System.currentTimeMillis();
    logger.error("CashierLog: start:" + start + " end:" + end + " times:" + (end - start)
        + " skuId:" + idPara.getData());

    return JackSonUtil.getJson(commodityItemVO);
  }

  /**
   * 收银机获取档口商品数据列表
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryKitchenSku", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryKitchenSkuByCat(@RequestBody String para) {
    long start = System.currentTimeMillis();
    HttpQuery<KitchenSkuQuery> kitchenPara =
        BaseActionAdapter.processObjectParameter(para, KitchenSkuQuery.class);
    if (!BaseActionAdapter.isRightParameter(kitchenPara)) {
      logger.error("收银台请求档口数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    PagedResult<List<CommodityItemVO>> commodityItemVO =
        itemComponent.getKitchenCommodityItemVO(kitchenPara.getData().getCatId(), kitchenPara
            .getData().getPageNum());
    long end = System.currentTimeMillis();
    logger.error("CashierLog: start:" + start + " end:" + end + " times:" + (end - start)
        + " para:" + JackSonUtil.getJson(kitchenPara.getData()));
    return JackSonUtil.getJson(commodityItemVO);
  }

  /**
   * 收银台查询sku
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryorginfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryorganizationinfo(@RequestBody String para) {
    HttpQuery<Integer> idPara = BaseActionAdapter.processObjectParameter(para, Integer.class);
    if (!BaseActionAdapter.isRightParameter(idPara)) {
      logger.error("收银台请求店铺数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    int shopId = idPara.getData();
    OrganizationDTO orgInfo = organizationComponent.getOrganizationDTOById(shopId);
    return JackSonUtil.getJson(Result.getSuccDataResult(orgInfo));
  }
  
  /**
   * 获取用户余额
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryuserbalance", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryUserBalance(@RequestBody String para) {
    HttpQuery<Long> idPara = BaseActionAdapter.processObjectParameter(para, Long.class);
    if (!BaseActionAdapter.isRightParameter(idPara)) {
      logger.error("收银台请求店铺数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    Long userId = idPara.getData();
    Result<Map<String, Object>> balanceResult = organizationComponent.getUserBalance(userId);
    return JackSonUtil.getJson(balanceResult);
  }

  /**
   * 员工扫码枪用户登录
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/login", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String login(@RequestBody String para, HttpServletResponse response) {
    Long start = new Date().getTime();
    HttpQuery<UserVO> userPara = BaseActionAdapter.processObjectParameter(para, UserVO.class);
    if (!BaseActionAdapter.isRightParameter(userPara)) {
      logger.error("收银台员工登录数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    UserVO userVO = userPara.getData();
    userVO.setUserType((short) 0);
    Result<BackGroundUserDTO> loginResult = userComponent.employeelogin(userVO);
    if (null == loginResult || !loginResult.getSuccess()) {
      logger.error("employ cashier login failed:" + para);
      return JackSonUtil.getJson(Result.getSuccDataResult(null));
    } else{
      logger.error("employ cashier login failed:" + para + " server return:"
          + JackSonUtil.getJson(loginResult));
      Long end = new Date().getTime();
      logger.error("CashierLog:cashier_login,times：" + (end - start) + "ms");
      return JackSonUtil.getJson(loginResult);
    }
  }
  
  /**
   * 员工扫码枪用户登录
   * 
   * @param data
   * @return
   */
  @RequestMapping(value = "/verifyPaySign", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String verifyPaySign(@RequestBody String para, HttpServletResponse response) {
    Long start = new Date().getTime();
    HttpQuery<UserVO> userPara = BaseActionAdapter.processObjectParameter(para, UserVO.class);
    if (!BaseActionAdapter.isRightParameter(userPara)) {
      logger.error("收银台员工登录数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    UserVO userVO = userPara.getData();
    userVO.setUserType((short) 0);
    Result<BackGroundUserDTO> loginResult = userComponent.employeelogin(userVO);
    if (null == loginResult || !loginResult.getSuccess()) {
      logger.error("employ cashier login failed:" + para);
      return JackSonUtil.getJson(Result.getSuccDataResult(null));
    } else{
      logger.error("employ cashier login failed:" + para + " server return:"
          + JackSonUtil.getJson(loginResult));
      Long end = new Date().getTime();
      logger.error("CashierLog:cashier_login,times：" + (end - start) + "ms");
      return JackSonUtil.getJson(loginResult);
    }
  }
  
  /**
   * 余额支付
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/paywithbalance", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String paywithbalance(@RequestBody String para) {
    HttpQuery<PayOrderQuery> queryPara = BaseActionAdapter.processObjectParameter(para, PayOrderQuery.class);
    if (!BaseActionAdapter.isRightParameter(queryPara)) {
      logger.error("收银台请求店铺数据错误，参数：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    PayOrderQuery query = queryPara.getData();
    Result<Map<String, Object>> balanceResult = organizationComponent.payWithUserBalance(query);
    return JackSonUtil.getJson(balanceResult);
  }
}
