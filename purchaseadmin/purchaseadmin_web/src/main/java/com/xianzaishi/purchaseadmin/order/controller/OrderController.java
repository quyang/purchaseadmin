package com.xianzaishi.purchaseadmin.order.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.item.vo.PurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SubPurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.order.vo.OrderVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.order.OrderComponent;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseOrderAuditingStatusConstants;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

/**
 * 采购单接口
 * 
 * @author dongpo
 *
 */
@Controller
@RequestMapping(value = "/order")
public class OrderController extends BaseController {

  private static final Logger LOGGER = Logger.getLogger(OrderController.class);

  @Autowired
  private OrderComponent orderComponent;

  @RequestMapping(value = "/createpurchasebyskuCode", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String createPurchaseBySkuId(Long startSkuCode, Long endSkuCode,
      HttpServletResponse response) {
//    String result = orderComponent.queryPurchaseBySkuId(startSkuCode, endSkuCode);

    return null;
  }

  @RequestMapping(value = "/createpurchasebykeyword", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String createPurchaseByKeyWord(String supplier, String itemKeyWord,
      HttpServletResponse response) {
    LOGGER.info(supplier + "," + itemKeyWord);
//    String result = orderComponent.queryPurchaseByKeyWord(supplier, itemKeyWord);
    return null;
  }

  /**
   * 下采购单，目前没有用
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/placeorder", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String placeOrder(String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    LOGGER.info(data);
    OrderVO orderVO = (OrderVO) JackSonUtil.jsonToObject(data, OrderVO.class);
    BackGroundUserDTO backGroundUserDTO = getUser();
    orderVO.setPurchasingAgent(backGroundUserDTO.getUserId());
    String result = orderComponent.insertOrder(orderVO);
    return result;
  }
  
  @RequestMapping(value = "/tmporder")
  public void tmporder(@RequestParam String start, @RequestParam String end,@RequestParam String userid, HttpServletResponse response) {
    response.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=utf-8");
    
    String result = orderComponent.getOrderInfo(start, end, userid);
    PrintWriter out = null;
    try {
      out = response.getWriter();
      out.println(result);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (out != null) {
        out.close();
      }
    }
  }
  
  /**
   * 更新子采购单
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/updatesuborder", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String updateSubOrder(String data, HttpServletResponse response){
    SubPurchaseItemVO purchaseItemVO = (SubPurchaseItemVO) JackSonUtil.jsonToObject(data, SubPurchaseItemVO.class);
    String result = orderComponent.updateSubOrder(purchaseItemVO);
    return result;
  }
  
  /**
   * 添加一个子采购单
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/addsuborder", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String addSubOrder(@RequestBody String data, HttpServletResponse response){
    HttpQuery<List<PurchaseItemVO>> queryData = BaseActionAdapter.processListObjectParameter(data, PurchaseItemVO.class);
    List<PurchaseItemVO> purchaseItemVOs = queryData.getData();
    if(null == getUser()){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "用户已失效，请重新登陆"));
    }
    Result<Boolean> result = orderComponent.insertSubOrder(purchaseItemVOs,getUser());
    return JackSonUtil.getJson(result);
  }
  
  /**
   * 保存采购单
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/save", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String save(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
//    response.setHeader("Access-Control-Allow-Credentials", "true");
    if(null == data){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    OrderVO orderVO = (OrderVO) JackSonUtil.jsonToObject(data, OrderVO.class);
    if(0 == getUserId()){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "用户已失效，请重新登陆"));
    }
    if(null != orderVO){
      orderVO.setAuditingStatus(PurchaseOrderAuditingStatusConstants.STATUS_DRAFT);
      orderVO.setPurchasingAgent(getUserId());
    }
    BackGroundUserDTO userDTO = getUser();
    if(null == userDTO){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "用户已失效，请重新登陆"));
    }
    if(null == userDTO.getRole() || !userDTO.getRole().contains("80")){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "您没有权限进行该项操作"));
    }
    String result = orderComponent.insertOrder(orderVO);
    return result;
  }
  
  /**
   * 采购单提交审核
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/submitaudit", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String submitAudit(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
//    response.setHeader("Access-Control-Allow-Credentials", "true");
    OrderVO orderVO = (OrderVO) JackSonUtil.jsonToObject(data, OrderVO.class);
    if(0 == getUserId()){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "用户已失效，请重新登陆"));
    }
    if(null != orderVO){
      orderVO.setAuditingStatus(PurchaseOrderAuditingStatusConstants.STATUS_FIRST_AUDITING);//设置为待审核状态
      orderVO.setPurchasingAgent(getUserId());
    }
    BackGroundUserDTO userDTO = getUser();
    
    if(null == userDTO){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "用户已失效，请重新登陆"));
    }
    
    if(null == userDTO.getRole() || !userDTO.getRole().contains("80")){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "您没有权限进行该项操作"));
    }
    String result = orderComponent.insertOrder(orderVO);
    return result;
  }
}
