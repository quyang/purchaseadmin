package com.xianzaishi.purchaseadmin.system.controller;


import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 获取系统时间
 * @author dongpo
 *
 */
@Controller
@RequestMapping(value = "/system")
public class SystemTimeController {
  private static final Logger LOGGER = Logger.getLogger(SystemTimeController.class);
  @RequestMapping(value = "/gettime", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String getSystemTime(){
    Date date = new java.util.Date();
    Long time = date.getTime();
    LOGGER.info("当前系统时间：" + time);
    return JackSonUtil.getJson(Result.getSuccDataResult(time));
  }
}
