package com.xianzaishi.purchaseadmin.category.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.component.category.CategoryComponent;

@Controller
@RequestMapping(value = "/category")
public class CategoryController {
  
  @Autowired
  private CategoryComponent categoryComponent;
  
  @RequestMapping(value = "/addcategory",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String addCategoryData(Integer parentId, String catName){
    if(null == parentId || null == catName){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    
    Result<Integer> result = categoryComponent.insertSubCategoryDate(parentId,catName);
    return JackSonUtil.getJson(result);
    
  }

}