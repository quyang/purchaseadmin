package com.xianzaishi.purchaseadmin.order.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.order.vo.SubOrderDetailVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.order.SubOrderDetailComponent;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;

/**
 * 采购单详情页接口
 * 
 * @author dongpo
 *
 */
@Controller
@RequestMapping(value = "/suborderdetail")
public class SubOrderDetailController extends BaseController {

  @Autowired
  private SubOrderDetailComponent subOrderDetailComponent;

  private static final Logger LOGGER = Logger.getLogger(SubOrderDetailController.class);
  private static final String AuditAuthority = "采购";

  @RequestMapping(value = "/orderdetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String requestOrderDetail(Integer subOrderId, HttpServletResponse response) {
    LOGGER.info(subOrderId);
    List<RoleDTO> roleDTOs = getRequireRoleList();
    Boolean displayReviewStatus = false;
    for (RoleDTO roleDTO : roleDTOs) {
      if (roleDTO.getRoleName().equals(AuditAuthority)) {
        displayReviewStatus = true;
      }
    }
    String result = subOrderDetailComponent.queryOrderDetail(subOrderId, displayReviewStatus);
    return result;

  }

  @RequestMapping(value = "/commitreview", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String commitReview(String data, HttpServletResponse response) {
    LOGGER.info(data);
    SubOrderDetailVO subOrderDetailVO =
        (SubOrderDetailVO) JackSonUtil.jsonToObject(data, SubOrderDetailVO.class);
    subOrderDetailVO.setStatusCode((short) 0);
    String result = subOrderDetailComponent.updateSubOrder(subOrderDetailVO, null);

    return result;
  }

  @RequestMapping(value = "/reviewpassstatus", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String reviewPassStatus(String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    LOGGER.info(data);
    SubOrderDetailVO subOrderDetailVO =
        (SubOrderDetailVO) JackSonUtil.jsonToObject(data, SubOrderDetailVO.class);
    String result = subOrderDetailComponent.updateSubOrder(subOrderDetailVO, null);
    return result;
  }

}
