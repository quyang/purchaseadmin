package com.xianzaishi.purchaseadmin.item.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.BaseController;
import com.xianzaishi.purchaseadmin.client.base.vo.CollectionVO;
import com.xianzaishi.purchaseadmin.client.item.vo.BoomCommodityItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.CommodityItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemListVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemTagsDetaileVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ProductItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ProductListQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ProductListVO;
import com.xianzaishi.purchaseadmin.client.item.vo.PurchaseItemQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SubPurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.CouponQueryVO;
import com.xianzaishi.purchaseadmin.component.annotation.Authorization;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain;
import com.xianzaishi.purchaseadmin.component.annotation.Crossdomain.PageType;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.wms.common.exception.BizException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 商品发布页面
 * 
 * @author zhancang
 */
@Controller
@RequestMapping(value = "/item")
public class ItemController extends BaseController {

  private static final Logger logger = Logger.getLogger(ItemController.class);

  @Autowired
  private HttpServletRequest request;

  @Autowired
  private ItemComponent itemComponent;

  /**
   * 初始导入所有采购商品接口
   * 
   * @return
   */
  @RequestMapping(value = "/addall")
  @ResponseBody
  public String addall(String data) {

    String inpukey = "M3uPYdy15";
    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
    }
    Map<String, String> userMap = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String istest = userMap.get("istest");
    String isonline = userMap.get("isonline");
    String pic = userMap.get("pic");
    String key = userMap.get("key");

    if (StringUtils.isEmpty(istest) || StringUtils.isEmpty(isonline) || StringUtils.isEmpty(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is null"));
    }
    if (!inpukey.equals(key)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "Input data is error"));
    }
    boolean isTest = !"false".equals(istest);
    boolean isOnlineEnv = !"false".equals(isonline);
    boolean isOnlyPic = "true".equals(pic);
    itemComponent.addAllProduct(isOnlineEnv, isTest, isOnlyPic);
    return request.getRequestURI();
  }

  /**
   * 按照采购商品id查询采购商品对象
   * 
   * @param productId
   * @return
   */
  @RequestMapping(value = "/queryproduct", method = RequestMethod.GET,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization(role = {Authorization.LOGIN_ROLE})
  public String queryProduct(@RequestParam long productId) {
    ProductItemVO productVO = itemComponent.queryProductItem(productId);
    return JackSonUtil.getJson(Result.getSuccDataResult(productVO));
  }

  /**
   * 查询商品列表
   * 
   * @param supplierInfo
   * @return
   */
  @RequestMapping(value = "/listproduct", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String listproduct(String data, HttpServletResponse response) {
    ProductListQueryVO query = null;
    if (!StringUtils.isEmpty(data)) {
      query = (ProductListQueryVO) JackSonUtil.jsonToObject(data, ProductListQueryVO.class);
    }
    CollectionVO<ProductListVO> productList = itemComponent.getProductList(query);
    return JackSonUtil.getJson(Result.getSuccDataResult(productList));
  }

  /**
   * 针对非加工类发布销售商品时，发布前查询采购商品属性。包括第一次发布和修改前查询
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/beforaddcommodity", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String beforAddCommodity(@RequestParam long skuId, HttpServletResponse response) {
    CommodityItemVO commodityItemVO = itemComponent.beforAddCommodityQueryProductBySku(skuId);
    if (null == commodityItemVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST,
          "Query product is null"));
    }
    logger.error(JackSonUtil.getJson(commodityItemVO));
    return JackSonUtil.getJson(Result.getSuccDataResult(commodityItemVO));
  }

  /**
   * 针对加工类发布销售商品时，发布前查询类目属性
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/beforaddboomcommodity", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String beforAddBoomCommodity(@RequestParam int catId, HttpServletResponse response) {
    BoomCommodityItemVO commodityItemVO = itemComponent.beforAddBoomCommodityQueryProperty(catId);
    logger.error(JackSonUtil.getJson(commodityItemVO));
    return JackSonUtil.getJson(Result.getSuccDataResult(commodityItemVO));
  }

  /**
   * 提交一个前台商品到后台
   * 
   * @param skuId
   * @return
   */
  @RequestMapping(value = "/addcommodity", method = RequestMethod.POST)
  @ResponseBody
  @Authorization
  @Crossdomain
  public String addCommodity(String data, HttpServletResponse response) {
    logger.error("post receive:" + data);

    if (StringUtils.isEmpty(data)) {
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "input post msg null"));
    }
    CommodityItemVO commodityItemVO = itemComponent.getItemVOByJson(data);
    Result<Long> insertResult = itemComponent.insertItemDTO(commodityItemVO, getUserId());
    return JackSonUtil.getJson(insertResult);
  }

  /**
   * 修改销售商品前，查询已经设置的商品属性
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/querycommodity", method = RequestMethod.POST,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String queryCommodity(@RequestParam long skuId, HttpServletResponse response) {
    CommodityItemVO commodityItemVO = itemComponent.queryCommodityItem(skuId);
    return JackSonUtil.getJson(Result.getSuccDataResult(commodityItemVO));
  }

  // /**
  // * 查询前后台商品sku属性，前台支持plu码或者69码，后台支持商品货号
  // *
  // * @param skuId 对应skuId
  // * @return
  // */
  // @RequestMapping(value = "/queryskuInfo", produces = {"application/json;charset=UTF-8"})
  // @ResponseBody
  // @Authorization
  // @Crossdomain
  // public String querySkuInfo(@RequestParam String skuCode, HttpServletResponse response) {
  // Result<? extends BaseItemVO> commodityItemVO = itemComponent.getItemInfoVO(skuCode);
  // return JackSonUtil.getJson(commodityItemVO);
  // }

  /**
   * 针对修改销售商品时，查询曾经的销售商品属性.包括加工类和非加工类商品
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/modifycommodity", method = RequestMethod.POST)
  @ResponseBody
  @Authorization
  @Crossdomain
  public String modifyCommodity(String data, HttpServletResponse response) {
    CommodityItemVO commodityItemVO = itemComponent.getItemVOByJson(data);
    Result<Boolean> result =
        itemComponent.updateCommodityItemInfo(commodityItemVO, super.getUserId());
    return JackSonUtil.getJson(result);
  }

  /**
   * 展示进售价维护
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/skurelation", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String skuRelation(String data, HttpServletResponse response) {
    SkuRelationQueryVO queryVO =
        (SkuRelationQueryVO) JackSonUtil.jsonToObject(data, SkuRelationQueryVO.class);
    List<SkuRelationVO> tmp =
        itemComponent.getSkuRelationVOList(itemComponent.getQueryByQueryVO(queryVO));
    int count = itemComponent.getQueryCountByQueryVO(queryVO);
    CollectionVO<SkuRelationVO> result = new CollectionVO<SkuRelationVO>();
    result.setList(tmp);
    result.setTotalPage(count);
    return JackSonUtil.getJson(Result.getSuccDataResult(result));
  }
  
//  /**
//   * 展示进售价维护
//   * 
//   * @param skurelationquery
//   * @return
//   */
//  @RequestMapping(value = "/skurelationSchedule", produces = {"application/json;charset=UTF-8"})
//  @ResponseBody
//  @Authorization
//  @Crossdomain
//  public String skurelationSchedule(@RequestBody String para, HttpServletResponse response) {
//    HttpQuery<Map> idPara = BaseActionAdapter.processObjectParameter(para, Map.class);
//    if(!BaseActionAdapter.isRightParameter(idPara)){
//      logger.error("请求参数为：" + para);
//      return JackSonUtil.getJson(Result.getErrDataResult(
//          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
//    }
//    
//    Object id = idPara.getData().get("id");
//    Object type = idPara.getData().get("type");
//    if(null == id || null == type || !StringUtils.isNumeric(id.toString()) || !StringUtils.isNumeric(type.toString()) ){
//      logger.error("请求参数为：" + para);
//      return JackSonUtil.getJson(Result.getErrDataResult(
//          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
//    }
//    
//    List<String> result = itemComponent.getSkuRelationScheduleList(Long.valueOf(id.toString()),Integer.valueOf(type.toString()));
//    return JackSonUtil.getJson(Result.getSuccDataResult(result));
//  }

  /**
   * 更新进售价维护
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/updateskurelation", method = RequestMethod.POST)
  @ResponseBody
  @Authorization
  @Crossdomain
  public String updateSkuRelation(String data, HttpServletResponse response) {
    SkuRelationVO skuRelationVO =
        (SkuRelationVO) JackSonUtil.jsonToObject(data, SkuRelationVO.class);

    skuRelationVO.setSalePrice(skuRelationVO.getSalePrice());
    return JackSonUtil.getJson(itemComponent.updateSkuRelationSchedule(skuRelationVO, getUserId(),true,true));
  }

  /**
   * 更新进售价维护
   * 
   * @param skurelationquery
   * @return
   */
  @RequestMapping(value = "/emergencyupdateskurelation", method = RequestMethod.POST)
  @ResponseBody
  @Authorization
  @Crossdomain
  public String emergencyUpdateSkuRelation(String data, HttpServletResponse response) {

    SkuRelationVO skuRelationVO =
        (SkuRelationVO) JackSonUtil.jsonToObject(data, SkuRelationVO.class);

    skuRelationVO.setSalePrice(skuRelationVO.getSalePrice());
    return JackSonUtil.getJson(itemComponent.updateSkuRelationVO(skuRelationVO, true, getUserId(),true,true));
  }

  @Crossdomain(targetDomain = PageType.HOME_PAGE)
  @RequestMapping(value = "/getdetail", method = RequestMethod.GET,
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String addCommodity(int id, HttpServletResponse response) {
    return JackSonUtil.getJson(Result.getSuccDataResult(itemComponent.getIntroJson(id)));
  }

  /**
   * 无线查询前台商品通用接口，支持plu码、69码,收银调用入口
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryskudetail", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String wirelessQuerySkuDetailFromCashDesk(@RequestParam String skuCode,
      HttpServletResponse response) {
    long start = System.currentTimeMillis();
    Result<CommodityItemVO> commodityItemVO = itemComponent.getCommodityItemVO(skuCode);
    long end = System.currentTimeMillis();
    long takeTime = end - start;
    logger.error("CashierLog logType:1 start:"+ start +" end:" + end + " times:" + takeTime + " skuId:" + skuCode);
    
    return JackSonUtil.getJson(commodityItemVO);
  }

  /**
   * 无线查询前台商品通用接口，支持plu码、69码,仓储调用入口
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryskuInfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String wirelessQuerySkuDetailFromWarehouse(@RequestParam String skuCode,
      HttpServletResponse response) {
    return wirelessQuerySkuDetailFromCashDesk(skuCode, response);
  }

  /**
   * 无线查询前台商品通用接口，支持plu码、69码,仓储调用入口
   */
  @RequestMapping(value = "/skuInfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String getSkuInfo(@RequestBody String data,
      HttpServletResponse response) {

    String flag = null;

    try {

      if (StringUtils.isEmpty(data)) {
        throw new BizException("参数对象为空");
      }

      HttpQuery<ItemListVO> httpQuery = BaseActionAdapter.processObjectParameter(data, ItemListVO.class);
      if (!BaseActionAdapter.isRightParameter(httpQuery)) {
        throw new BizException("参数异常");
      }

      ItemListVO httpQueryData = httpQuery.getData();
      flag = itemComponent.getSkuInfo(httpQueryData);

    } catch (BizException e) {
      flag = JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    } catch (Exception e) {
      flag = JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "unknow error"));
    }

    return JackSonUtil.getJson(flag);
  }

  /**
   * 无线查询前台商品通用接口，支持plu码
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryskubyidlist", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String wirelessQuerySkuDetailByIdList(@RequestParam String idlist,
      HttpServletResponse response) {
    Result<List<CommodityItemVO>> commodityItemVOList =
        itemComponent.getCommodityItemVOByIdList(idlist);
    return JackSonUtil.getJson(commodityItemVOList);
  }

  /**
   * 更新sku打标逻辑
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/addSkuTags", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String addSkuTags(@RequestParam short type, @RequestParam String idlist, @RequestParam(
      value = "tag", defaultValue = "0", required = false) int tag, @RequestParam(
      value = "addremoveflag", defaultValue = "0", required = false) int addremoveflag,
      @RequestParam(value = "channel", defaultValue = "0", required = false) short channel,
      @RequestParam(value = "tagcontent", required = false) String tagcontent, @RequestParam(
          value = "tagrule", required = false) String tagrule, HttpServletResponse response) {
    if (type == 1 && addremoveflag == 0) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "暂不支持类目查询"));
    }//请求参数：idlist（商品sku id列表）、tag（商品标）、addremoveflag（0表示查询、1表示增加、-1表示删除）、 （标渠道，0表示全场，1表示线上，2表示线下）、tagcontent（标内容） type(1表示类目 0 表示skuId)
    if ((tag == 64 || tag == 4096) && channel != 0) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "不参与优惠标或者限购标只支持全场标"));
    }
    if (tag == 128 && channel != 1) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "不允许交易标只支持线上标"));
    }
    if (tag == 4096 && !StringUtils.isNumeric(tagcontent)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "请输入整数"));
    }
    String skuIdArray[] = idlist.split(",");
    List<Long> skuIds = Lists.newArrayList();
    for (String idStr : skuIdArray) {
      if (null != idStr && itemComponent.isDecimal(idStr)) {
        skuIds.add(Long.parseLong(idStr));
      }
    }
    if (type == 1) {
      List<Integer> catIds = Lists.newArrayList();
      for (String idStr : skuIdArray) {
        if (null != idStr && itemComponent.isDecimal(idStr)) {
          catIds.add(Integer.valueOf(idStr));
        }
      }
      skuIds = itemComponent.querySkuIdsByCatId(catIds);
    }
    if (CollectionUtils.isEmpty(skuIds)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "商品数据为空"));
    }
    if (addremoveflag == 0) {
      return JackSonUtil.getJson(itemComponent.querySkuTags(skuIds));
    } else {
      return JackSonUtil.getJson(itemComponent.updateSkuTags(skuIds, tag, channel, tagcontent,
          tagrule));
    }
  }

  /**
   * 更新sku打标逻辑
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/dropSkuTags", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String dropSkuTags(ItemTagsDetaileVO itemTagsDetaileVO, HttpServletResponse response) {
    if (null == itemTagsDetaileVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Result<Boolean> result = itemComponent.deleteSkuTags(itemTagsDetaileVO);

    return JackSonUtil.getJson(result);
  }

  /**
   * 查询标种类
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/queryTagsType", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  @Crossdomain
  public String queryTagsType(String data, HttpServletResponse response) {
    Result<Map<String, Object>> tagsTypeResult = itemComponent.queryTagsType();
    return JackSonUtil.getJson(tagsTypeResult);
  }

  @RequestMapping(value = "/queryiteminventory", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryItemInventory(Long skuId) {
    if (null == skuId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    return JackSonUtil.getJson(itemComponent.queryItemInventory(skuId));
  }

  /**
   * 采购时查询前200的商品
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/querydefaultpurchaseitem",
      produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String queryDefaultPurchaseItem(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    PurchaseItemQueryVO purchaseItemQueryVO =
        (PurchaseItemQueryVO) JackSonUtil.jsonToObject(data, PurchaseItemQueryVO.class);
    if (null == purchaseItemQueryVO) {
      purchaseItemQueryVO = new PurchaseItemQueryVO();
    }
    PagedResult<List<SubPurchaseItemVO>> result = itemComponent.queryPurchaseItem(purchaseItemQueryVO);
    return JackSonUtil.getJson(result);
  }

  /**
   * 采购时查询商品
   * 
   * @param skuId 对应skuId
   * @return
   */
  @RequestMapping(value = "/querypurchaseitem", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String queryPurchaseItem(String data, HttpServletResponse response) {
    response.setContentType("text/html;charset=UTF-8");
//    response.addHeader("Access-Control-Allow-Origin", "*");
    if (StringUtils.isBlank(data)) {
      return JackSonUtil.getJson(PagedResult.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    PurchaseItemQueryVO purchaseItemQueryVO =
        (PurchaseItemQueryVO) JackSonUtil.jsonToObject(data, PurchaseItemQueryVO.class);
    if (null == purchaseItemQueryVO) {
      return JackSonUtil.getJson(PagedResult.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数格式错误"));
    }
    PagedResult<List<SubPurchaseItemVO>> result = itemComponent.queryPurchaseItem(purchaseItemQueryVO);
    return JackSonUtil.getJson(result);
  }

  @RequestMapping(value = "/test", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String test(String data, HttpServletResponse response) {
    List<Long> cSkuIds = Lists.newArrayList();
    Map<Long, Long> cPSkuIdMap = Maps.newHashMap();
    cSkuIds.add(10064L);
    cPSkuIdMap.put(10064L, 100200500010002L);
    Map<Long, Integer> resultMap = itemComponent.batchQueryItemInventory(cSkuIds, cPSkuIdMap);
    return JackSonUtil.getJson(resultMap);
  }


  /**
   * 查询商品信息
   */
  @RequestMapping(value = "/getItemsInfo", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String getItemsInfo(@RequestBody String data, HttpServletResponse response) {
    logger.error("data=" + data);
    String result = null;
    try {
      if (StringUtils.isEmpty(data)) {
        throw new BizException("参数为空");
      }
      HttpQuery<CommodityItemVO> httpQuery = BaseActionAdapter
          .processObjectParameter(data, CommodityItemVO.class);
      if (!BaseActionAdapter.isRightParameter(httpQuery)) {
        throw new BizException("参数解析失败");
      }

      CommodityItemVO commodityItemVO = httpQuery.getData();
      result = itemComponent.getItemsInfo(commodityItemVO);
    } catch (BizException e) {
      result = JackSonUtil
          .getJson(
              Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    } catch (Exception e) {
      result = JackSonUtil
          .getJson(
              Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage()));
    }

    return result;
  }

  /**
   * 增加优惠券，并将商品和优惠券关联，如果没有skuIds就只生产优惠券
   */
  @RequestMapping(value = "/addcoupon", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  @Authorization
  public String addCoupon(@RequestBody String data, HttpServletResponse response) {
    Result result = null;
    try {
      //channnelType distributeAmount(发行数) endTime startTiem title useConditon(无门槛or满n元 0 无门槛 1 满减券) limitAmount(限领数)
      //amount(优惠金额) amountLimit(最大金额)
      //skuIds 商品skuId集合
      //sendRulesType 发放类型
//      0 有就不发，没有就发
//      1 用户拥有优惠券数量不满n张就发，满就不发；
//      2 用户有效可用优惠券数量不满n张就发，满就不发；
      logger.error("date=" + data);
      if (StringUtils.isEmpty(data)) {
        throw new BizException("请求参数为空");
      }
      HttpQuery<CouponQueryVO> httpQuery = BaseActionAdapter
          .processObjectParameter(data, CouponQueryVO.class);
      if (!BaseActionAdapter.isRightParameter(httpQuery)) {
        throw new BizException("参数不正确");
      }
      result = itemComponent.addCouponAndItem(httpQuery.getData());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      result = Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "unknow error");
    }

    return JackSonUtil.getJson(result);
  }
}
