package com.xianzaishi.purchaseadmin.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;

@Controller
@RequestMapping(value = "/status")
public class Status {

  @RequestMapping(value = "/init")
  @ResponseBody
  public String inserttest() {
    return JackSonUtil.getJson("SUCCESS");
  }
}
