package com.xianzaishi.purchaseadmin.client.inventory.vo;

import com.xianzaishi.wms.track.vo.StorageVO;

/**
 * Created by quyang on 2017/5/11.
 */
public class StorageInfoVO extends StorageVO {

  /**
   * rf枪入库原因
   */
  private String OpReasonString;

  /**
   * 入库状态
   */
  private String statusString;

  public String getOpReasonString() {
    return OpReasonString;
  }

  public void setOpReasonString(String opReasonString) {
    OpReasonString = opReasonString;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }
}
