package com.xianzaishi.purchaseadmin.client.item.vo;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 采购商品列表页面对应的查询对象
 * 
 * @author zhancang
 */
public class ProductListQueryVO extends BaseQuery {

  private String supplierName;

  private String title;

  private String status;

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
