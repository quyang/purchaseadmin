package com.xianzaishi.purchaseadmin.client.custom.vo;

import com.xianzaishi.purchaseadmin.client.user.vo.QueryDataVO;

/**
 * Created by quyang on 2017/3/13.
 */
public class CustomQueryVO {

  private Integer version;

  private QueryDataVO data;


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public QueryDataVO getData() {
    return data;
  }

  public void setData(QueryDataVO data) {
    this.data = data;
  }
}
