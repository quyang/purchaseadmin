package com.xianzaishi.purchaseadmin.client.user.vo;

import java.io.Serializable;

/**
 * 用户订单信息
 * 
 * @author quyang 2017-2-10 10:27:18
 */
public class UserOrderVO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * 用户id
   */
  private Long phoneNum;

  /**
   * 订单id
   */
  private Long oid;



  public Long getOid() {
    return oid;
  }


  public void setOid(Long oid) {
    this.oid = oid;
  }


  public Long getPhoneNum() {
    return phoneNum;
  }


  public void setUid(Long phoneNum) {
    this.phoneNum = phoneNum;
  }



}
