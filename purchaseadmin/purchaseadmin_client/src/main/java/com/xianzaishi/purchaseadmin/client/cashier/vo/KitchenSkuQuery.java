package com.xianzaishi.purchaseadmin.client.cashier.vo;

/**
 * 查询档口商品接口
 * @author zhancang
 */
public class KitchenSkuQuery {

  private Integer catId;
  
  private Integer pageNum;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }
}
