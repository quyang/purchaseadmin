package com.xianzaishi.purchaseadmin.client.item.vo;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationScheduleDTO;

/**
 * sku对应关系
 * 
 * @author zhancang
 */
public class SkuRelationScheduleVO extends ItemSkuRelationScheduleDTO{
  
  private static final long serialVersionUID = -3151300172446905503L;

  private String boxCostYuan;
  
  private String avgCostYuan;
  
  private String priceYuan;
  
  private String bPriceYuan;

  public String getBoxCostYuan() {
    if(StringUtils.isNotEmpty(boxCostYuan)){
      return boxCostYuan;
    }
    if(super.getBoxCost() != null && getBoxCost() > 0){
      return new BigDecimal(getBoxCost()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString();
    }
    return "0";
  }

  public void setBoxCostYuan(String boxCostYuan) {
    this.boxCostYuan = boxCostYuan;
  }

  public String getAvgCostYuan() {
    if(StringUtils.isNotEmpty(avgCostYuan)){
      return avgCostYuan;
    }
    if(super.getAvgCost() != null && getAvgCost() > 0){
      return new BigDecimal(getAvgCost()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString();
    }
    return "0";
  }

  public void setAvgCostYuan(String avgCostYuan) {
    this.avgCostYuan = avgCostYuan;
  }

  public String getPriceYuan() {
    if(StringUtils.isNotEmpty(priceYuan)){
      return priceYuan;
    }
    if(super.getPrice() != null && getPrice() > 0){
      return new BigDecimal(getPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString();
    }
    return "0";
  }

  public void setPriceYuan(String priceYuan) {
    this.priceYuan = priceYuan;
  }

  public String getbPriceYuan() {
    if(StringUtils.isNotEmpty(bPriceYuan)){
      return bPriceYuan;
    }
    if(super.getbPrice() != null && getbPrice() > 0){
      return new BigDecimal(getbPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString();
    }
    return "0";
  }

  public void setbPriceYuan(String bPriceYuan) {
    this.bPriceYuan = bPriceYuan;
  }
}
