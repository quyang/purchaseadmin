package com.xianzaishi.purchaseadmin.client.order.vo;

import java.io.Serializable;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 采购单列表查询条件
 * @author dongpo
 *
 */
public class OrderListQueryVO extends BaseQuery implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 1872384737171887684L;

  /**
   * 采购单id
   */
  private Integer purchaseId;
  
  /**
   * 采购人
   */
  private String purchasingAgent;
  
  /**
   * 采购单审核状态
   */
  private Short auditingStatus;
  
  /**
   * 采购单状态
   */
  private Short statusCode;
  
  /**
   * 供应商名称
   */
  private String supplier;
  
  /**
   * 商品关键字
   */
  private String itemKeyWord;
  
  /**
   * 采购开始时间
   */
  private String begin;
  
  /**
   * 采购结束时间
   */
  private String end;
  
  public Integer getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }

  public String getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(String purchasingAgent) {
    this.purchasingAgent = purchasingAgent;
  }

  public Short getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Short statusCode) {
    this.statusCode = statusCode;
  }

  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }

  public String getItemKeyWord() {
    return itemKeyWord;
  }

  public void setItemKeyWord(String itemKeyWord) {
    this.itemKeyWord = itemKeyWord;
  }

  public String getBegin() {
    return begin;
  }

  public void setBegin(String begin) {
    this.begin = begin;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }

  public Short getAuditingStatus() {
    return auditingStatus;
  }

  public void setAuditingStatus(Short auditingStatus) {
    this.auditingStatus = auditingStatus;
  }

}
