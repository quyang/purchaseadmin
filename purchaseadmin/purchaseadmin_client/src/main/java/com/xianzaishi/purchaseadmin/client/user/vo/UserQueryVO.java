package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 *
 * @author quyang
 * 2017-2-15 13:11:53
 */
public class UserQueryVO {
  
  /**
   * 用户id
   */
  private Long uid;

  /**
   * 一页有多少条数据
   */
  private Integer pageSize;

  /**
   * 当前页
   */
  private Integer currentPage;

  /**
   * 订单id
   */
  private Long orderId;

  /**
   * 是否请求全部订单
   */
  private Boolean isAll;

  /**
   * 开始时间
   */
  private String gmtStart;
  /**
   * 结束时间
   */
  private String gmtEnd;

  public String getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(String gmtStart) {
    this.gmtStart = gmtStart;
  }

  public String getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(String gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Boolean getAll() {
    return isAll;
  }

  public void setAll(Boolean all) {
    isAll = all;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Long getUid() {
    return uid;
  }

  public void setUid(Long uid) {
    this.uid = uid;
  }
  
  
  
  
}
