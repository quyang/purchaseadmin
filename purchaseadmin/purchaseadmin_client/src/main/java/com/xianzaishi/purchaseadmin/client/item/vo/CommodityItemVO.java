package com.xianzaishi.purchaseadmin.client.item.vo;

import java.util.List;


/**
 * 前台商品vo对象
 * 
 * @author zhancang
 */

public class CommodityItemVO extends BaseItemVO {

  /**
   * 发布前台商品时，关联前后台sku，需要页面传值并传送后台
   */
  private long skuId;
  
  /**
   * 返回商品69码
   */
  private long sku69Id;

  /**
   * 对应商品id
   */
  private long productId;
  
  /**
   * 分为单位的价格描述,与specification对应
   */
  private int price;
  
  /**
   * 称重商品使用时替换为称重单价的价格，与unStandardSpecification对应
   */
  private int unitPrice;
  
  /**
   * 关联商品
   */
  private List<ItemDisplaySpecificationVO> relatedItems;
  
  /**
   * 成分列表
   */
  private List<String> elementList;
  
  /**
   * 商品成份
   */
  private String element;
  
  /**
   * 展示规格
   */
  private String displaySpecifications;
  
  /**
   * 展示规格列表
   */
  private List<ItemDisplaySpecificationVO> displaySpecificationList;
  
  /**
   * 商品规格
   */
  private String specification = "";
  
  /**
   * 有称重商品时包含称重规格
   */
  private String unStandardSpecification = "";

  /**
   * 温度条件
   */
  private int storageType;
  
  /**
   * 销售单位的类型，分为整箱销售(1),分组销售(2),按小件销售(3),按称重类商品销售(4)
   */
  private int saleUnitType;
  
  /**
   * 称重销售起重量
   */
  private int saleRange;
  
  /**
   * 商品的单位描述
   */
  private String saleUnitStr;
  
  /**
   * 销售单位，线下销售单位
   */
  private int skuUnit;
  
  /**
   * 销售单位，线上销售单位
   */
  private int onlineSkuUnit;
  
  /**
   * 线上销售规格数值
   */
  private int skuSpecificationNum;

  /**
   * 线上销售规格单位
   */
  private int skuSpecificationUnit;
  
  /**
   * 各种商品标
   */
  private int itemtags;
  
  /**
   * 商品标内容
   */
  private List<String> itemTagContent;
  
  /**
   * 供应商id
   */
  private int supplierId;

  /**
   * 是否是标品
   */
  private Boolean standardGoods;
  
  /**
   * 为true（一般只有临期商品才会出现）的时候，客户端同学应将这一次扫描商品结果单独展示一行，这个时候购物车的+号是失效的（不能点击+号添加该商品）
   * 
   */
  private Boolean independentDisplay = false;

  /**
   * allowrepeat=true,则允许有同样条码，如果为false，就不允许同样条码。也就是说independentDisplay 只控制是否独立一行展示商品并且有没有加减号。allowrepeat控制是否有同样条码输入
   */
  private Boolean allowrepeat = true;
  
  /**
   * 条码类型，针对线下扫条码时，代表不同条码的含义，参考 BarcodeInfoVO.BarcodeTypeConstants.*
   */
  private Short barType = 0;
  
  public Boolean getStandardGoods() {
    return standardGoods;
  }

  public void setStandardGoods(Boolean standardGoods) {
    this.standardGoods = standardGoods;
  }


  private Integer pageSize;

  private Integer currentPage;

  /**
   * skuid集合
   */
  private List<Long> skuIds;

  public List<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<Long> skuIds) {
    this.skuIds = skuIds;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public long getSkuId() {
    return skuId;
  }

  public void setSkuId(long skuId) {
    this.skuId = skuId;
  }

  public long getSku69Id() {
    return sku69Id;
  }

  public void setSku69Id(long sku69Id) {
    this.sku69Id = sku69Id;
  }

  public int getPrice() {
    return price;
  }

  public List<ItemDisplaySpecificationVO> getRelatedItems() {
    return relatedItems;
  }

  public void setRelatedItems(List<ItemDisplaySpecificationVO> relatedItems) {
    this.relatedItems = relatedItems;
  }

  public List<String> getElementList() {
    return elementList;
  }

  public void setElementList(List<String> elementList) {
    this.elementList = elementList;
  }

  public List<ItemDisplaySpecificationVO> getDisplaySpecificationList() {
    return displaySpecificationList;
  }

  public void setDisplaySpecificationList(List<ItemDisplaySpecificationVO> displaySpecificationList) {
    this.displaySpecificationList = displaySpecificationList;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public long getProductId() {
    return productId;
  }

  public void setProductId(long productId) {
    this.productId = productId;
  }
  
  public String getElement() {
    return element;
  }

  public void setElement(String element) {
    this.element = element;
  }

  public String getDisplaySpecifications() {
    return displaySpecifications;
  }

  public void setDisplaySpecifications(String displaySpecifications) {
    this.displaySpecifications = displaySpecifications;
  }

  public String getSpecification() {
    return specification;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  public int getStorageType() {
    return storageType;
  }

  public void setStorageType(int storageType) {
    this.storageType = storageType;
  }

  public int getSaleUnitType() {
    return saleUnitType;
  }

  public void setSaleUnitType(int saleUnitType) {
    this.saleUnitType = saleUnitType;
  }

  public int getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(int unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getUnStandardSpecification() {
    return unStandardSpecification;
  }

  public void setUnStandardSpecification(String unStandardSpecification) {
    this.unStandardSpecification = unStandardSpecification;
  }

  public int getSaleRange() {
    return saleRange;
  }

  public void setSaleRange(int saleRange) {
    this.saleRange = saleRange;
  }

  public String getSaleUnitStr() {
    return saleUnitStr;
  }

  public void setSaleUnitStr(String saleUnitStr) {
    this.saleUnitStr = saleUnitStr;
  }

  public int getSkuUnit() {
    return skuUnit;
  }

  public void setSkuUnit(int skuUnit) {
    this.skuUnit = skuUnit;
  }

  public int getOnlineSkuUnit() {
    return onlineSkuUnit;
  }

  public void setOnlineSkuUnit(int onlineSkuUnit) {
    this.onlineSkuUnit = onlineSkuUnit;
  }

  public int getSkuSpecificationNum() {
    return skuSpecificationNum;
  }

  public void setSkuSpecificationNum(int skuSpecificationNum) {
    this.skuSpecificationNum = skuSpecificationNum;
  }

  public int getSkuSpecificationUnit() {
    return skuSpecificationUnit;
  }

  public void setSkuSpecificationUnit(int skuSpecificationUnit) {
    this.skuSpecificationUnit = skuSpecificationUnit;
  }

  public int getItemtags() {
    return itemtags;
  }

  public void setItemtags(int itemtags) {
    this.itemtags = itemtags;
  }

  public List<String> getItemTagContent() {
    return itemTagContent;
  }

  public void setItemTagContent(List<String> itemTagContent) {
    this.itemTagContent = itemTagContent;
  }

  public int getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(int supplierId) {
    this.supplierId = supplierId;
  }

  public Boolean getIndependentDisplay() {
    return independentDisplay;
  }

  public void setIndependentDisplay(Boolean independentDisplay) {
    this.independentDisplay = independentDisplay;
  }

  public Boolean getAllowrepeat() {
    return allowrepeat;
  }

  public void setAllowrepeat(Boolean allowrepeat) {
    this.allowrepeat = allowrepeat;
  }

  public Short getBarType() {
    return barType;
  }

  public void setBarType(Short barType) {
    this.barType = barType;
  }
  
}
