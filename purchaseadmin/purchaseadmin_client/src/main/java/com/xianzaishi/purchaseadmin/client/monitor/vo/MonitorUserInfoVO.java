package com.xianzaishi.purchaseadmin.client.monitor.vo;

/**
 * Created by quyang on 2017/3/23.
 */
public class MonitorUserInfoVO {

  /**
   * 用户token
   */
  private String token;

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 用户名称
   */
  private String userName;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
