package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * Created by quyang on 2017/3/16.
 */
public class GiveCouponsVO {

  /**
   * 优惠券id
   */
  private Long couponId;

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 退款任务id
   */
  private Long taskId;

  public Long getTaskId() {
    return taskId;
  }

  public void setTaskId(Long taskId) {
    this.taskId = taskId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }
}
