package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by quyang on 2017/4/25.
 */
public class MarketActivityQueryVO {



  /**
   * 主键
   */
  private Integer id;

  /**
   * 活动名称
   */
  private String name;
  /**
   * 活动状态
   */
  private Short status;
  /**
   * 活动类型
   */
  private Short type;

  /**
   * 优惠券Id集合
   */
  private LinkedList<Long> couponIds;

  /**
   * 商品itemId集合
   */
  private LinkedList<Long> itemIds;
  /**
   * 开始时间
   */
  private Date gmtStart;

  private String gmtStartString;

  /**
   * 结束时间
   */
  private Date gmtEnd;

  private String gmtEndString;

  private Date gmtCreate;

  private String gmtCreateString;

  private Date gmtModified;

  private String gmtModifiedString;



  /**
   * 限时折扣
   */
  private String limitedTimeDiscount;

  /**
   * 限购数
   */
  private String limitCount;



  /**
   * 折扣
   */
  private String discount;

  /**
   * 是否包邮
   */
  private Boolean post;
  /**
   * 满n件
   */
  private String discountCount;



  private Integer pageSize;

  private Integer currentPage;

  /**
   * 绑定类型 0 表示关联的是商品  1 表示关联的是优惠券
   */
  private Short bindType;

  /**
   * 是否增加关联信息，true表示在原有的基础上增加，false表示在原有的基础上减去
   */
  private Boolean add;

  public Boolean getAdd() {
    return add;
  }

  public void setAdd(Boolean add) {
    this.add = add;
  }

  public Short getBindType() {
    return bindType;
  }

  public void setBindType(Short bindType) {
    this.bindType = bindType;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public String getLimitedTimeDiscount() {
    return limitedTimeDiscount;
  }

  public void setLimitedTimeDiscount(String limitedTimeDiscount) {
    this.limitedTimeDiscount = limitedTimeDiscount;
  }

  public String getLimitCount() {
    return limitCount;
  }

  public void setLimitCount(String limitCount) {
    this.limitCount = limitCount;
  }

  public String getDiscount() {
    return discount;
  }

  public void setDiscount(String discount) {
    this.discount = discount;
  }

  public Boolean getPost() {
    return post;
  }

  public void setPost(Boolean post) {
    this.post = post;
  }

  public String getDiscountCount() {
    return discountCount;
  }

  public void setDiscountCount(String discountCount) {
    this.discountCount = discountCount;
  }

  public String getGmtCreateString() {
    return gmtCreateString;
  }

  public void setGmtCreateString(String gmtCreateString) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      gmtCreate = sdf.parse(gmtCreateString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.gmtCreateString = gmtCreateString;
  }

  public String getGmtModifiedString() {
    return gmtModifiedString;
  }

  public void setGmtModifiedString(String gmtModifiedString) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      gmtModified = sdf.parse(gmtModifiedString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.gmtModifiedString = gmtModifiedString;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public LinkedList<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(LinkedList<Long> couponIds) {
    this.couponIds = couponIds;
  }

  public LinkedList<Long> getItemIds() {
    return itemIds;
  }

  public void setItemIds(LinkedList<Long> itemIds) {
    this.itemIds = itemIds;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public String getGmtStartString() {
    return gmtStartString;
  }

  public void setGmtStartString(String gmtStartString) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      gmtStart = sdf.parse(gmtStartString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.gmtStartString = gmtStartString;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public String getGmtEndString() {
    return gmtEndString;
  }

  public void setGmtEndString(String gmtEndString) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      gmtEnd = sdf.parse(gmtEndString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.gmtEndString = gmtEndString;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
}
