package com.xianzaishi.purchaseadmin.client.property.vo;

public class ValueVO {

  /**
   * 属性值id
   */
  private int valueId;

  /**
   * 输入类型时，代表输入值<br/>
   * 选择 类型时，代表属性值id对应的标签名称<br/>
   */
  private String value;
  
  /**
   * 单位后缀，某个数值型单位需要后缀展示出来
   */
  private String valueUnitStr;
  
  public int getValueId() {
    return valueId;
  }

  public void setValueId(int valueId) {
    this.valueId = valueId;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getValueUnitStr() {
    return valueUnitStr;
  }

  public void setValueUnitStr(String valueUnitStr) {
    this.valueUnitStr = valueUnitStr;
  }
}
