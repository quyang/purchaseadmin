package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * 用户登录时，登录vo
 * 
 * @author zhancang
 */
public class UserVO {

  public static final String SIGN_SPLIT = "_";
  
  /**
   * 用户id
   */
  private int userId;
  
  /**
   * 根据手机号登录
   */
  private long phone;
  
  /**
   * 用户名
   */
  private String userName;
  
  /**
   * 用户退出时的token
   */
  private String token;

  /**
   * 设备号
   */
  private String equipmentId;

  /**
   * 登录时间
   */
  private long sysTime;

  /**
   * 签名<br/>
   * MD5(MD5(password)_data_equipmentId)
   */
  private String sign;
  
  /**
   * 供应商还是普通员工
   */
  private Short userType;

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }
  
  public long getPhone() {
    return phone;
  }

  public void setPhone(long phone) {
    this.phone = phone;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getEquipmentId() {
    return equipmentId;
  }

  public void setEquipmentId(String equipmentId) {
    this.equipmentId = equipmentId;
  }

  public long getSysTime() {
    return sysTime;
  }

  public void setSysTime(long sysTime) {
    this.sysTime = sysTime;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }
  
}
