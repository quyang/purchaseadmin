package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.Date;
import java.util.List;

import com.xianzaishi.trade.client.vo.LogisticalVO;
import com.xianzaishi.trade.client.vo.OrderItemVO;

/**
 *
 * @author quyang 2017-2-14 15:28:02
 *
 */
public class OrderInformationVO {


  /**
   * 用户ID
   */
  private Long userId;

  /**
   * 账单金额
   */
  private String payAmount;

  /**
   * 实收金额
   */
  private String effeAmount;

  /**
   * 折扣金额
   */
  private String discountAmount;

  /**
   * 设备ID
   */
  private String deviceId;

  /**
   * 收银员ID
   */
  private Integer cashierId;

  /**
   * 用户地址ID
   */
  private Long userAddressId;

  /**
   * 用户收货地址
   */
  private String userAddress;

  /**
   * 属性
   */
  private String attribute;

  /**
   * 订单ID
   */
  private Long id;

  /**
   * 流水号
   */
  private Integer seq;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 付款时间
   */
  private Date gmtPay;

  /**
   * 签收时间
   */
  private Date gmtEnd;

  /**
   * 定时送达时间
   */
  private Date gmtDistribution;

  /**
   * 订单状态
   */
  private Short status;

  private String statusString ;

  /**
   * 店铺ID
   */
  private Integer shopId;

  /**
   * 使用积分
   */
  private Integer credit;

  /**
   * 使用优惠券ID
   */
  private Long couponId;

  /**
   * 订单渠道(1线上,2线下)
   */
  private Short channelType;

  /**
   * 订单类型0普通订单 1加工订单
   */
  private Short orderType;

  /**
   * 订单商品列表
   *
   * @see OrderItemVO
   */
  private List<OrderItemVO> items ;

  /**
   * 物流信息
   *
   * @see LogisticalVO
   */
  private List<LogisticalVO> logisticalInfo;
  /**
   * 退款金额
   */
  private Long refunded;
  /**
   * 该订单是否开发票
   */
  private Boolean isBilled;

  /**
   * 订单总数
   *
   */
  private Integer count;

  /**
   * 当前页
   */
  private Integer currentPage;
  /**
   * 总页数
   */
  private Integer totalPage;

  /**
   * 总消费金额
   */
  private Integer allMoney;

  /**

   * 总消费现金金额
   */
  private Integer allCash;


  /**
   * 已开发票金额
   */
  private Integer moneyHasBilled;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getPayAmount() {
    return payAmount;
  }

  public void setPayAmount(String payAmount) {
    this.payAmount = payAmount;
  }

  public String getEffeAmount() {
    return effeAmount;
  }

  public void setEffeAmount(String effeAmount) {
    this.effeAmount = effeAmount;
  }

  public String getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(String discountAmount) {
    this.discountAmount = discountAmount;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public Integer getCashierId() {
    return cashierId;
  }

  public void setCashierId(Integer cashierId) {
    this.cashierId = cashierId;
  }

  public Long getUserAddressId() {
    return userAddressId;
  }

  public void setUserAddressId(Long userAddressId) {
    this.userAddressId = userAddressId;
  }

  public String getUserAddress() {
    return userAddress;
  }

  public void setUserAddress(String userAddress) {
    this.userAddress = userAddress;
  }

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getSeq() {
    return seq;
  }

  public void setSeq(Integer seq) {
    this.seq = seq;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtPay() {
    return gmtPay;
  }

  public void setGmtPay(Date gmtPay) {
    this.gmtPay = gmtPay;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtDistribution() {
    return gmtDistribution;
  }

  public void setGmtDistribution(Date gmtDistribution) {
    this.gmtDistribution = gmtDistribution;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }

  public Integer getShopId() {
    return shopId;
  }

  public void setShopId(Integer shopId) {
    this.shopId = shopId;
  }

  public Integer getCredit() {
    return credit;
  }

  public void setCredit(Integer credit) {
    this.credit = credit;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Short getOrderType() {
    return orderType;
  }

  public void setOrderType(Short orderType) {
    this.orderType = orderType;
  }

  public List<OrderItemVO> getItems() {
    return items;
  }

  public void setItems(List<OrderItemVO> items) {
    this.items = items;
  }

  public List<LogisticalVO> getLogisticalInfo() {
    return logisticalInfo;
  }

  public void setLogisticalInfo(List<LogisticalVO> logisticalInfo) {
    this.logisticalInfo = logisticalInfo;
  }

  public Long getRefunded() {
    return refunded;
  }

  public void setRefunded(Long refunded) {
    this.refunded = refunded;
  }

  public Boolean getBilled() {
    return isBilled;
  }

  public void setBilled(Boolean billed) {
    isBilled = billed;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Integer getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(Integer totalPage) {
    this.totalPage = totalPage;
  }

  public Integer getAllMoney() {
    return allMoney;
  }

  public void setAllMoney(Integer allMoney) {
    this.allMoney = allMoney;
  }

  public Integer getAllCash() {
    return allCash;
  }

  public void setAllCash(Integer allCash) {
    this.allCash = allCash;
  }

  public Integer getMoneyHasBilled() {
    return moneyHasBilled;
  }

  public void setMoneyHasBilled(Integer moneyHasBilled) {
    this.moneyHasBilled = moneyHasBilled;
  }
}
