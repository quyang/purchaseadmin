package com.xianzaishi.purchaseadmin.client.item.vo;

public class ItemTagsDetaileVO {
  
  /**
   * 商品sku id
   */
  private Long skuId;
  
  /**
   * 商品标id
   */
  private String tagId;
  
  /**
   * 商品标种类
   */
  private String tagString;
  
  /**
   * 标渠道
   */
  private Short channel;
  
  /**
   * 标渠道
   */
  private String channelString;
  
  /**
   * 标内容
   */
  private String tagContent;
  
  /**
   * 内容类型：0表示内容为标内容，1表示内容为标类型
   */
  private Short contentType;
  
  /**
   * 标规则
   */
  private String tagRule;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getTagId() {
    return tagId;
  }

  public void setTagId(String tagId) {
    this.tagId = tagId;
  }

  public Short getChannel() {
    return channel;
  }

  public void setChannel(Short channel) {
    this.channel = channel;
  }

  public String getTagContent() {
    return tagContent;
  }

  public void setTagContent(String tagContent) {
    this.tagContent = tagContent;
  }

  public Short getContentType() {
    return contentType;
  }

  public void setContentType(Short contentType) {
    this.contentType = contentType;
  }

  public String getTagString() {
    return tagString;
  }

  public void setTagString(String tagString) {
    this.tagString = tagString;
  }

  public String getChannelString() {
    return channelString;
  }

  public void setChannelString(String channelString) {
    this.channelString = channelString;
  }

  public String getTagRule() {
    return tagRule;
  }

  public void setTagRule(String tagRule) {
    this.tagRule = tagRule;
  }
  
}
