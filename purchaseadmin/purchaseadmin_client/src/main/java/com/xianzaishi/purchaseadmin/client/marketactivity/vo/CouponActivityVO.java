package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.util.List;

/**
 * Created by quyang on 2017/5/3.
 */
public class CouponActivityVO {

  private List<Long> couponIds;

  public List<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(List<Long> couponIds) {
    this.couponIds = couponIds;
  }
}
