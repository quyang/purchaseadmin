package com.xianzaishi.purchaseadmin.client.item.vo;

/**
 * 采购商品列表页面vo
 * 
 * @author zhancang
 */
public class ProductListVO {

  private long productId;
  
  private long skuId;
  
  private long productCode;

  private String title;

  private String supplierName;

  private int supplierId;
  
  private String purchaseAgentName;

  private int purchaseAgentId;
  
  private String status;
  
  private Short statusCode;

  public long getProductId() {
    return productId;
  }

  public void setProductId(long productId) {
    this.productId = productId;
  }

  public long getSkuId() {
    return skuId;
  }

  public void setSkuId(long skuId) {
    this.skuId = skuId;
  }

  public long getProductCode() {
    return productCode;
  }

  public void setProductCode(long productCode) {
    this.productCode = productCode;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getPurchaseAgentName() {
    return purchaseAgentName;
  }

  public void setPurchaseAgentName(String purchaseAgentName) {
    this.purchaseAgentName = purchaseAgentName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Short getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Short statusCode) {
    this.statusCode = statusCode;
  }

  public int getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(int supplierId) {
    this.supplierId = supplierId;
  }

  public int getPurchaseAgentId() {
    return purchaseAgentId;
  }

  public void setPurchaseAgentId(int purchaseAgentId) {
    this.purchaseAgentId = purchaseAgentId;
  }
}
