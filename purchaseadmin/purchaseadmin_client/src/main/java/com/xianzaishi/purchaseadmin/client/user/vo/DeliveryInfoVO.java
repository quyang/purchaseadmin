package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * 
 * @author quyang 2017-2-14 11:25:37
 *
 */
public class DeliveryInfoVO {

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 用户名字
   */
  private String name;
  /**
   * 详细地址
   */
  private String address;

  /**
   * 大概地址
   */
  private String codeAddress;

  public String getCodeAddress() {
    return codeAddress;
  }

  public void setCodeAddress(String codeAddress) {
    this.codeAddress = codeAddress;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


}
