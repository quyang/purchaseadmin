package com.xianzaishi.purchaseadmin.client.bill.vo;

/**
 * Created by quyang on 2017/2/19.
 */
public class BillUpdateVO {

  /**
   * 手机号
   */
  private Long phoneNumber;

  /**
   * 用户id
   */
  private Integer userId;


  /**
   * 发票号
   */
  private Long billNumber;
  /**
   * 发票金额
   */
  private String billMoney;
  /**
   * 发票描述
   */
  private String contribution;

  public Long getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(Long phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public String getBillMoney() {
    return billMoney;
  }

  public void setBillMoney(String billMoney) {
    this.billMoney = billMoney;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }
}
