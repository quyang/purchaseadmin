package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.Date;
import java.util.List;

/**
 * Created by quyang on 2017/2/20.
 */
public class OrderSVO {

  /**
   * 用户ID
   */
  private Long userId;

  /**
   * 手机号
   */
  private String phoneNumber;

  /**
   * 账单金额
   */
  private String payAmount;

  /**
   * 实收金额
   */
  private String effeAmount;

  /**
   * 折扣金额
   */
  private String discountAmount;

  /**
   * 业务状态码
   */
  private int businessStatus;

  /**
   * 设备ID
   */
  private String deviceId;

  /**
   * 收银员ID
   */
  private Integer cashierId;

  /**
   * 用户地址ID
   */
  private Long userAddressId;

  /**
   * 用户收货地址
   */
  private String userAddress;

  /**
   * 属性
   */
  private String attribute;

  /**
   * 订单ID
   */
  private Long id;

  /**
   * 流水号
   */
  private Integer seq;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 付款时间
   */
  private Date gmtPay;

  /**
   * 签收时间
   */
  private Date gmtEnd;

  /**
   * 定时送达时间
   */
  private Date gmtDistribution;

  /**
   * 订单状态
   */
  private Short status;

  private String statusString;

  /**
   * 店铺ID
   */
  private Integer shopId;

  /**
   * 使用积分
   */
  private Integer credit;

  /**
   * 积分折扣
   */
  private String creditDiscount;

  /**
   * 使用优惠券ID
   */
  private Long couponId;

  /**
   * 订单渠道(1线上,2线下)
   */
  private Short channelType;

  /**
   * 订单类型0普通订单 1加工订单
   */
  private Short orderType;

  /**
   * 优惠券折扣
   */
  private String couponDiscount;


  /**
   * 优惠券状态
   */
  private Short couponStatus;

  /**
   * 最大优惠金额
   */
  private Double amount;


  /**
   * 配送地址
   */
  private String distributinaAddress ;

  /**
   * 配送状态  0，创建成功，1，进入等待队列，2，波次创建完成，3，送货中，4，送货完成，-1，挂起
   */
  private String distributinStatus ;
  /**
   * 开始配送时间
   */
  private Date distributinStartTime;
  /**
   * 送达时间  如果为null 的话就是立即送达
   */
  private Date distributinEndTime ;

  /**
   * 配送单上的名字
   */
  private String name;

  /**
   * 商品列表
   */
  private List<OrderItemInfoVO> itemsList;

  /**
   * 物流列表
   */
  private List<LogisticalInfoVO> logisticalInfoList;

  /**
   * 订单状态 erp app
   */
  private Integer tagStatus;

  /**
   * 退货类型
   */
  private Short refoundGoodsType;

  private String refoundGoodsTypeString;

  /**
   * 是否全部退货
   */
  private Boolean allRefound;

  /**
   * 是否退过货
   */
  private Boolean havedRefoud;


  public Short getRefoundGoodsType() {
    return refoundGoodsType;
  }

  public void setRefoundGoodsType(Short refoundGoodsType) {
    this.refoundGoodsType = refoundGoodsType;
  }

  public String getRefoundGoodsTypeString() {
    return refoundGoodsTypeString;
  }

  public void setRefoundGoodsTypeString(String refoundGoodsTypeString) {
    this.refoundGoodsTypeString = refoundGoodsTypeString;
  }

  public Integer getTagStatus() {
    return tagStatus;
  }

  public void setTagStatus(Integer tagStatus) {
    this.tagStatus = tagStatus;
  }

  public int getBusinessStatus() {
    return businessStatus;
  }

  public void setBusinessStatus(int businessStatus) {
    this.businessStatus = businessStatus;
  }

  public String getCouponDiscount() {
    return couponDiscount;
  }

  public void setCouponDiscount(String couponDiscount) {
    this.couponDiscount = couponDiscount;
  }

  public String getCreditDiscount() {
    return creditDiscount;
  }

  public void setCreditDiscount(String creditDiscount) {
    this.creditDiscount = creditDiscount;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDistributinaAddress() {
    return distributinaAddress;
  }

  public void setDistributinaAddress(String distributinaAddress) {
    this.distributinaAddress = distributinaAddress;
  }

  public String getDistributinStatus() {
    return distributinStatus;
  }

  public void setDistributinStatus(String distributinStatus) {
    this.distributinStatus = distributinStatus;
  }

  public Date getDistributinStartTime() {
    return distributinStartTime;
  }

  public void setDistributinStartTime(Date distributinStartTime) {
    this.distributinStartTime = distributinStartTime;
  }

  public Date getDistributinEndTime() {
    return distributinEndTime;
  }

  public void setDistributinEndTime(Date distributinEndTime) {
    this.distributinEndTime = distributinEndTime;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Short getCouponStatus() {
    return couponStatus;
  }

  public void setCouponStatus(Short couponStatus) {
    this.couponStatus = couponStatus;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getPayAmount() {
    return payAmount;
  }

  public void setPayAmount(String payAmount) {
    this.payAmount = payAmount;
  }

  public String getEffeAmount() {
    return effeAmount;
  }

  public void setEffeAmount(String effeAmount) {
    this.effeAmount = effeAmount;
  }

  public String getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(String discountAmount) {
    this.discountAmount = discountAmount;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public Integer getCashierId() {
    return cashierId;
  }

  public void setCashierId(Integer cashierId) {
    this.cashierId = cashierId;
  }

  public Long getUserAddressId() {
    return userAddressId;
  }

  public void setUserAddressId(Long userAddressId) {
    this.userAddressId = userAddressId;
  }

  public String getUserAddress() {
    return userAddress;
  }

  public void setUserAddress(String userAddress) {
    this.userAddress = userAddress;
  }

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getSeq() {
    return seq;
  }

  public void setSeq(Integer seq) {
    this.seq = seq;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtPay() {
    return gmtPay;
  }

  public void setGmtPay(Date gmtPay) {
    this.gmtPay = gmtPay;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtDistribution() {
    return gmtDistribution;
  }

  public void setGmtDistribution(Date gmtDistribution) {
    this.gmtDistribution = gmtDistribution;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }

  public Integer getShopId() {
    return shopId;
  }

  public void setShopId(Integer shopId) {
    this.shopId = shopId;
  }

  public Integer getCredit() {
    return credit;
  }

  public void setCredit(Integer credit) {
    this.credit = credit;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Short getOrderType() {
    return orderType;
  }

  public void setOrderType(Short orderType) {
    this.orderType = orderType;
  }

  public List<OrderItemInfoVO> getItemsList() {
    return itemsList;
  }

  public void setItemsList(
      List<OrderItemInfoVO> itemsList) {
    this.itemsList = itemsList;
  }

  public List<LogisticalInfoVO> getLogisticalInfoList() {
    return logisticalInfoList;
  }

  public void setLogisticalInfoList(
      List<LogisticalInfoVO> logisticalInfoList) {
    this.logisticalInfoList = logisticalInfoList;
  }

  public Boolean isAllRefound() {
    return allRefound;
  }

  public void setAllRefound(Boolean allRefound) {
    this.allRefound = allRefound;
  }

  public Boolean isHavedRefoud() {
    return havedRefoud;
  }

  public void setHavedRefoud(Boolean havedRefoud) {
    this.havedRefoud = havedRefoud;
  }
}
