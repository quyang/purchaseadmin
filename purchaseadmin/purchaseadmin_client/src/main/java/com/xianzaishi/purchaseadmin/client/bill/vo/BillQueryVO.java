package com.xianzaishi.purchaseadmin.client.bill.vo;

/**
 *
 * @author quyang 2017-2-15 15:20:09
 *
 */
public class BillQueryVO {

  /**
   * 用户id
   */
  private Integer userId;
  /**
   * 订单id
   */
  private Long orderId;
  /**
   * 发票号
   */
  private Long billNumber;
  /**
   * 发票金额
   */
  private String billMoney;

  /**
   * 发票描述
   */
  private String contribution;

  /**
   * 用户手机号
   */
  private Long phoneNumber;


  public Long getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(Long phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getBillMoney() {
    return billMoney;
  }

  public void setBillMoney(String billMoney) {
    this.billMoney = billMoney;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }




}
