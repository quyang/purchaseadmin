package com.xianzaishi.purchaseadmin.client.user.coupon;

import java.util.List;

/**
 * Created by quyang on 2017/2/17.
 */
public class BillsInsertVO {


  /**
   * 订单信息
   */
  private List<BillDetailsVO> orders ;

  /**
   * 发票号
   */
  private Long billNumber ;

  /**
   * 发票描述
   */
  private String contribution ;

  public List<BillDetailsVO> getOrders() {
    return orders;
  }

  public void setOrders(
      List<BillDetailsVO> orders) {
    this.orders = orders;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }
}
