package com.xianzaishi.purchaseadmin.client.item.vo;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.xianzaishi.purchaseadmin.client.property.vo.PropertyVO;

public class BaseItemVO {

  protected long id;

  protected String title;

  protected String subTitle;

  protected int catId;

  protected List<String> picList;

  protected List<IntorductionDataVO> introduction;

  /**
   * 当前类目下属性列表
   */
  protected List<PropertyVO> propertyVO;
  
  /**
   * 对应公司名称
   */
  protected String supplierName;

  /**
   * 对应采购
   */
  protected String purchasingAgentName;

  /**
   * 商品状态
   */
  protected short status;

  /**
   * 对应采购流程
   */
  protected short purchaseFlowStatus;

  protected String checkReason;

  protected String checkUserName;
  
  protected String titletags;

  /**
   * 是否展示审核人和审核理由框
   */
  boolean displayCheckInfo = false;
  
  /**
   * 是否展示审核通过不通过按钮
   */
  boolean displayCheckButton = false;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubTitle() {
    return subTitle;
  }

  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }

  public List<String> getPicList() {
    return picList;
  }

  public void setPicList(List<String> picList) {
    this.picList = picList;
  }

  public List<IntorductionDataVO> getIntroduction() {
    return introduction;
  }

  public void setIntroduction(List<IntorductionDataVO> introduction) {
    this.introduction = introduction;
  }

  public List<PropertyVO> getPropertyVO() {
    return propertyVO;
  }

  public void setPropertyVO(List<PropertyVO> propertyVO) {
    this.propertyVO = propertyVO;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getPurchasingAgentName() {
    return purchasingAgentName;
  }

  public void setPurchasingAgentName(String purchasingAgentName) {
    this.purchasingAgentName = purchasingAgentName;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

  public short getPurchaseFlowStatus() {
    return purchaseFlowStatus;
  }

  public void setPurchaseFlowStatus(short purchaseFlowStatus) {
    this.purchaseFlowStatus = purchaseFlowStatus;
  }

  public String getCheckReason() {
    return checkReason;
  }

  public void setCheckReason(String checkReason) {
    this.checkReason = checkReason;
  }

  public String getCheckUserName() {
    return checkUserName;
  }

  public void setCheckUserName(String checkUserName) {
    this.checkUserName = checkUserName;
  }

  public int getCatId() {
    return catId;
  }

  public void setCatId(int catId) {
    this.catId = catId;
  }
  
  public String getTitletags() {
    return titletags;
  }

  public void setTitletags(String titletags) {
    this.titletags = titletags;
  }

  public boolean isDisplayCheckInfo() {
    return displayCheckInfo;
  }

  public void setDisplayCheckInfo(boolean displayCheckInfo) {
    this.displayCheckInfo = displayCheckInfo;
  }

  public boolean isDisplayCheckButton() {
    return displayCheckButton;
  }

  public void setDisplayCheckButton(boolean displayCheckButton) {
    this.displayCheckButton = displayCheckButton;
  }

  public Map<Integer, PropertyVO> queryPropertyMap() {
    Map<Integer, PropertyVO> propertyVOMap = Maps.newHashMap();
    for (PropertyVO pro : this.getPropertyVO()) {
      propertyVOMap.put(pro.getPropertyId(), pro);
    }
    return propertyVOMap;
  }
}
