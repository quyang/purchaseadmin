package com.xianzaishi.purchaseadmin.client.user.vo;

import java.io.Serializable;

public class BaseRequestVo implements Serializable{

	private String token;

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
	
}
