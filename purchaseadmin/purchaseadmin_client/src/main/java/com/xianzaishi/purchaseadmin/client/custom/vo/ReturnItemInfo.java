package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * Created by quyang on 2017/3/17.
 */
public class ReturnItemInfo {


  /**
   * 商品skuId
   */
  private Long skuId;

  /**
   * 数量
   */
  private Long count;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }
}
