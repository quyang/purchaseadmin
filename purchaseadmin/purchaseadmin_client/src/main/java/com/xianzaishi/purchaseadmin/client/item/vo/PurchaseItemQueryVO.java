package com.xianzaishi.purchaseadmin.client.item.vo;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 采购商品查询类
 * @author dongpo
 *
 */
public class PurchaseItemQueryVO extends BaseQuery {
  /**
   * sku id
   */
  private Long skuId;
  
  /**
   * 商品sku标题
   */
  private String title;
  
  /**
   * 供应商编号
   */
  private Integer supplierId;
  
  /**
   * 商品sku状态
   */
  private Short status;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

}
