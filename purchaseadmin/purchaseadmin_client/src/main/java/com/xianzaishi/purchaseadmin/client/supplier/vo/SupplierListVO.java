package com.xianzaishi.purchaseadmin.client.supplier.vo;

/**
 * 供应商注册页面、供应商list页面vo
 * 
 * @author zhancang
 */
public class SupplierListVO {

  private int supplier;

  private String supplierName;

  private String companyName;

  private String purchasingAgentName;

  private short status;

  public int getSupplier() {
    return supplier;
  }

  public void setSupplier(int supplier) {
    this.supplier = supplier;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getPurchasingAgentName() {
    return purchasingAgentName;
  }

  public void setPurchasingAgentName(String purchasingAgentName) {
    this.purchasingAgentName = purchasingAgentName;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }
}
