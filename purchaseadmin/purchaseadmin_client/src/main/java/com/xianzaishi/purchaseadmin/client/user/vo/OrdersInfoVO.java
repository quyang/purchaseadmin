package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.List;

/**
 * Created by quyang on 2017/2/16.
 */
public class OrdersInfoVO {

  /**
   * 订单总数
   *
   */
  private Integer count;

  /**
   * 当前页
   */
  private Integer currentPage;
  
  /**
   * 总页数
   */
  private Integer totalPage;

  /**
   * 总消费金额
   */
  private String allMoney;

  /**

   * 总消费现金金额
   */
  private String allCash;


  /**
   * 已开发票金额
   */
  private String moneyHasBilled;

  /**
   * 用户名称
   */
  private String userName;

  /**
   * 手机号码
   */
  private String phoneNumber;

  /**
   * 订单列表
   */
  private List<OrderListResultVO> orders;

  /**
   * 订单是否退货过，true表示退货过，false表示没有
   */
  private Boolean hasRefound;


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Integer getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(Integer totalPage) {
    this.totalPage = totalPage;
  }

  public String getAllMoney() {
    return allMoney;
  }

  public void setAllMoney(String allMoney) {
    this.allMoney = allMoney;
  }

  public String getAllCash() {
    return allCash;
  }

  public void setAllCash(String allCash) {
    this.allCash = allCash;
  }

  public String getMoneyHasBilled() {
    return moneyHasBilled;
  }

  public void setMoneyHasBilled(String moneyHasBilled) {
    this.moneyHasBilled = moneyHasBilled;
  }

  public List<OrderListResultVO> getOrders() {
    return orders;
  }

  public void setOrders(
      List<OrderListResultVO> orders) {
    this.orders = orders;
  }

  public Boolean getHasRefound() {
    return hasRefound;
  }

  public void setHasRefound(Boolean hasRefound) {
    this.hasRefound = hasRefound;
  }
}
