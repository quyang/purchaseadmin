package com.xianzaishi.purchaseadmin.client.custom.vo;

import java.util.List;

/**
 * 客服详细页面
 * @author dongpo
 *
 */
public class CustomDetailVO {
  
  private CustomInfoVO customInfo;
  
  private TaskInfoVO taskInfo;
  
  private List<OrderInfoVO> orderInfo;
  
  private List<CustomCouponVO> coupons;

  public CustomInfoVO getCustomInfo() {
    return customInfo;
  }

  public void setCustomInfo(CustomInfoVO customInfo) {
    this.customInfo = customInfo;
  }

  public TaskInfoVO getTaskInfo() {
    return taskInfo;
  }

  public void setTaskInfo(TaskInfoVO taskInfo) {
    this.taskInfo = taskInfo;
  }

  public List<OrderInfoVO> getOrderInfo() {
    return orderInfo;
  }

  public void setOrderInfo(List<OrderInfoVO> orderInfo) {
    this.orderInfo = orderInfo;
  }

  public List<CustomCouponVO> getCoupons() {
    return coupons;
  }

  public void setCoupons(List<CustomCouponVO> coupons) {
    this.coupons = coupons;
  }
  
}
