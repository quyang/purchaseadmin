package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.Date;

/**
 * Created by quyang on 2017/2/20.
 */
public class LogisticalInfoVO {


  /**
   * 描述
   */
  private String description;

  /**
   * 第N步
   */
  private byte step;

  /**
   * 操作时间
   */
  private Date gmtOperation;

  /**
   * 操作者
   */
  private String operatorId;

  /**
   * 操作者信息
   */
  private String operatorInfo;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public byte getStep() {
    return step;
  }

  public void setStep(byte step) {
    this.step = step;
  }

  public Date getGmtOperation() {
    return gmtOperation;
  }

  public void setGmtOperation(Date gmtOperation) {
    this.gmtOperation = gmtOperation;
  }

  public String getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(String operatorId) {
    this.operatorId = operatorId;
  }

  public String getOperatorInfo() {
    return operatorInfo;
  }

  public void setOperatorInfo(String operatorInfo) {
    this.operatorInfo = operatorInfo;
  }
}
