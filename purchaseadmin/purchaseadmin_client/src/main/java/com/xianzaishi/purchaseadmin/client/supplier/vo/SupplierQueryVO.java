package com.xianzaishi.purchaseadmin.client.supplier.vo;

/**
 * 供应商列表页面查询参数vo
 * 
 * @author zhancang
 */
public class SupplierQueryVO {

  private String supplierName = null;

  private String companyName = null;

  private short supplierStatus = -1;

  private String purchasingAgentName = null;

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public short getSupplierStatus() {
    return supplierStatus;
  }

  public void setSupplierStatus(short supplierStatus) {
    this.supplierStatus = supplierStatus;
  }

  public String getPurchasingAgentName() {
    return purchasingAgentName;
  }

  public void setPurchasingAgentName(String purchasingAgentName) {
    this.purchasingAgentName = purchasingAgentName;
  }

}
