package com.xianzaishi.purchaseadmin.client.order.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * 保存或提交审核时子采购单数据
 * @author dongpo
 *
 */
public class SubOrderVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 7088220359315120251L;

  /**
   * 商品sku id
   */
  private Long skuId;
  
  /**
   * 商品名称
   */
  private String title;
  
  /**
   * 商品数量
   */
  private Integer count;
  
  /**
   * 采购单价
   */
  private String discountPrice;
  
  /**
   * 库位号
   */
  private String wareHouse;
  
  /**
   * 备注
   */
  private String checkInfo;
  
  /**
   * 供应商id
   */
  private Integer supplierId;
  

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "sku is null");
    Preconditions.checkArgument(skuId > 0, "sku must be greater than 0");
    this.skuId = skuId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public String getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(String discountPrice) {
    this.discountPrice = discountPrice;
  }

  public String getWareHouse() {
    return wareHouse;
  }

  public void setWareHouse(String wareHouse) {
    this.wareHouse = wareHouse;
  }

  public String getCheckInfo() {
    return checkInfo;
  }

  public void setCheckInfo(String checkInfo) {
    this.checkInfo = checkInfo;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }
  
}
