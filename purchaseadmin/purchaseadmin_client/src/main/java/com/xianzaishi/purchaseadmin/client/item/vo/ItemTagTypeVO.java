package com.xianzaishi.purchaseadmin.client.item.vo;

/**
 * 标类型
 * @author dongpo
 *
 */
public class ItemTagTypeVO {
  
  /**
   * 商品标
   */
  private Integer tags;
  
  /**
   * 商品标中文字符
   */
  private String tagsString;
  
  /**
   * 标规则
   */
  private String tagRule;

  public Integer getTags() {
    return tags;
  }

  public void setTags(Integer tags) {
    this.tags = tags;
  }

  public String getTagsString() {
    return tagsString;
  }

  public void setTagsString(String tagsString) {
    this.tagsString = tagsString;
  }

  public String getTagRule() {
    return tagRule;
  }

  public void setTagRule(String tagRule) {
    this.tagRule = tagRule;
  }
  
}
