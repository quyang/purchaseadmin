package com.xianzaishi.purchaseadmin.client.promotion.vo;

public class SetDiscountQuery {

  private long skuId;
  
  private String setPrice;
  
  private int count;
  
  private int day;

  public long getSkuId() {
    return skuId;
  }

  public void setSkuId(long skuId) {
    this.skuId = skuId;
  }

  public String getSetPrice() {
    return setPrice;
  }

  public void setSetPrice(String setPrice) {
    this.setPrice = setPrice;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }
  
}
