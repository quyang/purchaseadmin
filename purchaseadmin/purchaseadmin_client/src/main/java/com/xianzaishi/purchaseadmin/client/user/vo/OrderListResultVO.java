package com.xianzaishi.purchaseadmin.client.user.vo;

import com.xianzaishi.trade.client.vo.LogisticalVO;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import java.util.Date;
import java.util.List;

/**
 * Created by quyang on 2017/2/16.
 */
public class OrderListResultVO {


  /**
   * 用户ID
   */
  private Long userId;

  /**
   * 账单金额
   */
  private String payAmount;

  /**
   * 实收金额
   */
  private String effeAmount;

  /**
   * 折扣金额
   */
  private String discountAmount;

  /**
   * 设备ID
   */
  private String deviceId;

  /**
   * 收银员ID
   */
  private Integer cashierId;

  /**
   * 用户地址ID
   */
  private Long userAddressId;

  /**
   * 用户收货地址
   */
  private String userAddress;

  /**
   * 属性
   */
  private String attribute;

  /**
   * 订单ID
   */
  private Long id;

  /**
   * 流水号
   */
  private Integer seq;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 付款时间
   */
  private Date gmtPay;

  /**
   * 签收时间
   */
  private Date gmtEnd;

  /**
   * 定时送达时间
   */
  private Date gmtDistribution;

  /**
   * 订单状态
   */
  private Short status;

  private String statusString ;

  /**
   * 店铺ID
   */
  private Integer shopId;

  /**
   * 使用积分
   */
  private Integer credit;

  /**
   * 交易时间
   */
  private String tradeTime;


  /**
   * 积分折扣
   */
  private String creditDiscount;

  /**
   * 使用优惠券ID
   */
  private Long couponId;

  /**
   * 优惠券面值
   */
  private String amount;

  /**
   * 优惠券状态
   */
  private short couponStatus;

  /**
   * 优惠券折扣
   */
  private String couponDiscount;

  /**
   * 订单渠道(1线上,2线下)
   */
  private Short channelType;

  /**
   * 渠道类型
   */
  private String channelTypeString;

  /**
   * 订单类型0普通订单 1加工订单
   */
  private Short orderType;

  /**
   * 订单商品列表
   *
   * @see OrderItemVO
   */
  private List<OrderItemInfoVO> itemList;

  /**
   * 物流信息
   *
   * @see LogisticalVO
   */
  private List<LogisticalInfoVO> logisticalInfoList;

  /**
   * 退款金额
   */
  private String refunded = "0.00";
  /**
   * 该订单是否开发票
   */
  private Boolean isBilled;

  /**
   * 1 取消  0 正常
   */
  private int businessStatus;

  /**
   * 发票金额
   */
  private String billMoney ;

  /**
   * 该订单id所在的发票号
   */
  private Long billNumber;

  /**
   * 是否是全部退款，true表示全部退款，false表示部分；
   */
  private Boolean allRefound;

  /**
   * 否是退货过
   */
  private Boolean havedRefoud;

  /**
   * 0表示全部退货,1表示部分退货，2表示没有退过货
   */
  private Short refoundGoodsType;


  private String refoundGoodsTypeString;


  public String getRefoundGoodsTypeString() {
    return refoundGoodsTypeString;
  }

  public void setRefoundGoodsTypeString(String refoundGoodsTypeString) {
    this.refoundGoodsTypeString = refoundGoodsTypeString;
  }

  public Short getRefoundGoodsType() {
    return refoundGoodsType;
  }

  public void setRefoundGoodsType(Short refoundGoodsType) {
    this.refoundGoodsType = refoundGoodsType;
  }

  public Boolean getHavedRefoud() {
    return havedRefoud;
  }

  public void setHavedRefoud(Boolean havedRefoud) {
    this.havedRefoud = havedRefoud;
  }

  public Boolean getAllRefound() {
    return allRefound;
  }

  public void setAllRefound(Boolean allRefound) {
    this.allRefound = allRefound;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getTradeTime() {
    return tradeTime;
  }

  public void setTradeTime(String tradeTime) {
    this.tradeTime = tradeTime;
  }

  public int getBusinessStatus() {
    return businessStatus;
  }

  public void setBusinessStatus(int businessStatus) {
    this.businessStatus = businessStatus;
  }

  public String getChannelTypeString() {
    return channelTypeString;
  }

  public void setChannelTypeString(String channelTypeString) {
    this.channelTypeString = channelTypeString;
  }

  public short getCouponStatus() {
    return couponStatus;
  }

  public void setCouponStatus(short couponStatus) {
    this.couponStatus = couponStatus;
  }

  public String getCouponDiscount() {
    return couponDiscount;
  }

  public void setCouponDiscount(String couponDiscount) {
    this.couponDiscount = couponDiscount;
  }

  public String getCreditDiscount() {
    return creditDiscount;
  }

  public void setCreditDiscount(String creditDiscount) {
    this.creditDiscount = creditDiscount;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public String getBillMoney() {
    return billMoney;
  }

  public void setBillMoney(String billMoney) {
    this.billMoney = billMoney;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getPayAmount() {
    return payAmount;
  }

  public void setPayAmount(String payAmount) {
    this.payAmount = payAmount;
  }

  public String getEffeAmount() {
    return effeAmount;
  }

  public void setEffeAmount(String effeAmount) {
    this.effeAmount = effeAmount;
  }

  public String getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(String discountAmount) {
    this.discountAmount = discountAmount;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public Integer getCashierId() {
    return cashierId;
  }

  public void setCashierId(Integer cashierId) {
    this.cashierId = cashierId;
  }

  public Long getUserAddressId() {
    return userAddressId;
  }

  public void setUserAddressId(Long userAddressId) {
    this.userAddressId = userAddressId;
  }

  public String getUserAddress() {
    return userAddress;
  }

  public void setUserAddress(String userAddress) {
    this.userAddress = userAddress;
  }

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getSeq() {
    return seq;
  }

  public void setSeq(Integer seq) {
    this.seq = seq;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtPay() {
    return gmtPay;
  }

  public void setGmtPay(Date gmtPay) {
    this.gmtPay = gmtPay;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtDistribution() {
    return gmtDistribution;
  }

  public void setGmtDistribution(Date gmtDistribution) {
    this.gmtDistribution = gmtDistribution;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }

  public Integer getShopId() {
    return shopId;
  }

  public void setShopId(Integer shopId) {
    this.shopId = shopId;
  }

  public Integer getCredit() {
    return credit;
  }

  public void setCredit(Integer credit) {
    this.credit = credit;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Short getOrderType() {
    return orderType;
  }

  public void setOrderType(Short orderType) {
    this.orderType = orderType;
  }

  public List<OrderItemInfoVO> getItemList() {
    return itemList;
  }

  public void setItemList(
      List<OrderItemInfoVO> itemList) {
    this.itemList = itemList;
  }

  public List<LogisticalInfoVO> getLogisticalInfoList() {
    return logisticalInfoList;
  }

  public void setLogisticalInfoList(
      List<LogisticalInfoVO> logisticalInfoList) {
    this.logisticalInfoList = logisticalInfoList;
  }

  public String getRefunded() {
    return refunded;
  }

  public void setRefunded(String refunded) {
    this.refunded = refunded;
  }

  public Boolean getBilled() {
    return isBilled;
  }

  public void setBilled(Boolean billed) {
    isBilled = billed;
  }


  /**
   * 订单退货类型
   *
   */
  public static class RefoundGoodsType{

    /**
     * 0表示全部退货,1表示部分退货，2表示没有退过货
     */
    public static final Short REFOUND_ALL = 0;

    /**
     * 1表示部分退货，2表示没有退过货
     */
    public static final Short REFOUND_PART = 1;

    /**
     * 2表示没有退过货
     */
    public static final Short NO_REFOUND = 2;


  }
}
