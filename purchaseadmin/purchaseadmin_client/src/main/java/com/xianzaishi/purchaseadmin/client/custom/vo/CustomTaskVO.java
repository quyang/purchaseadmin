package com.xianzaishi.purchaseadmin.client.custom.vo;


/**
 * 任务列表
 *
 * @author dongpo
 */
public class CustomTaskVO {

  /**
   * 任务编号
   */
  private Long id;

  /**
   * 任务类型
   */
  private String type;

  /**
   * 任务类型码
   */
  private Short typeCode;

  /**
   * 任务标题
   */
  private String title;

  /**
   * 任务对应业务id
   */
  private String bizId;

  /**
   * 任务状态
   */
  private Short status;

  /**
   * 任务状态中文
   */
  private String statusString;

  /**
   * 创建时间
   */
  private String gmtCreate;

  /**
   * 退款金额 客服妹子填写后会传的
   */
  private Long refoudAmount;

  /**
   * 退款原因
   */
  private String refoudReason;

  /**
   * 处理方式类型
   */
  private Short processType;

  /**
   * 处理方式string
   */
  private String processTypeString;

  /**
   * rf枪入库原因
   */
  private String opReasonString;

  /**
   * rf枪入库原因
   */
  private int opReason;

  public String getProcessTypeString() {
    return processTypeString;
  }

  public void setProcessTypeString(String processTypeString) {
    this.processTypeString = processTypeString;
  }

  public Short getProcessType() {
    return processType;
  }

  public void setProcessType(Short processType) {
    this.processType = processType;
  }

  public String getRefoudReason() {
    if (null == refoudAmount) {
      refoudReason = "";
    }
    return refoudReason;
  }

  public void setRefoudReason(String refoudReason) {
    this.refoudReason = refoudReason;
  }

  public Long getRefoudAmount() {
    if (null == refoudAmount) {
      refoudAmount = 0L;
    }
    return refoudAmount;
  }

  public void setRefoudAmount(Long refoudAmount) {
    this.refoudAmount = refoudAmount;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Short getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(Short typeCode) {
    this.typeCode = typeCode;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBizId() {
    return bizId;
  }

  public void setBizId(String bizId) {
    this.bizId = bizId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }

  public String getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(String gmtCreate) {
    this.gmtCreate = gmtCreate;
  }


  public void setOpReasonString(String opReasonString) {
    this.opReasonString = opReasonString;
  }

  public String getOpReasonString() {
    return opReasonString;
  }

  public void setOpReason(int opReason) {
    this.opReason = opReason;
  }

  public int getOpReason() {
    return opReason;
  }
}
