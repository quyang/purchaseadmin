package com.xianzaishi.purchaseadmin.client.supplier.vo;

import java.util.List;

import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierCompanyDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierContactDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierFinanceDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierLicensesDTO;

/**
 * 供应商注册页面、供应商list页面vo
 * 
 * @author zhancang
 */
public class SupplierVO {

  private SupplierDTO supplier;

  private SupplierContactDTO supplierContact;

  private SupplierCompanyDTO supplierCompany;

  private SupplierFinanceDTO supplierFinance;

  private List<SupplierLicensesDTO> supplierLicensesList;

  private String checkReason;

  private String checkUserName;

  public SupplierDTO getSupplier() {
    return supplier;
  }

  public void setSupplier(SupplierDTO supplier) {
    this.supplier = supplier;
  }

  public SupplierContactDTO getSupplierContact() {
    return supplierContact;
  }

  public void setSupplierContact(SupplierContactDTO supplierContact) {
    this.supplierContact = supplierContact;
  }

  public SupplierCompanyDTO getSupplierCompany() {
    return supplierCompany;
  }

  public void setSupplierCompany(SupplierCompanyDTO supplierCompany) {
    this.supplierCompany = supplierCompany;
  }

  public SupplierFinanceDTO getSupplierFinance() {
    return supplierFinance;
  }

  public void setSupplierFinance(SupplierFinanceDTO supplierFinance) {
    this.supplierFinance = supplierFinance;
  }

  public List<SupplierLicensesDTO> getSupplierLicensesList() {
    return supplierLicensesList;
  }

  public void setSupplierLicensesList(List<SupplierLicensesDTO> supplierLicensesList) {
    this.supplierLicensesList = supplierLicensesList;
  }

  public String getCheckReason() {
    return checkReason;
  }

  public void setCheckReason(String checkReason) {
    this.checkReason = checkReason;
  }

  public String getCheckUserName() {
    return checkUserName;
  }

  public void setCheckUserName(String checkUserName) {
    this.checkUserName = checkUserName;
  }

}
