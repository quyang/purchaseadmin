package com.xianzaishi.purchaseadmin.client.property.vo;

/**
 * 规格单位描述
 * 
 * @author zhancang
 */
public class UnitVO {

  /**
   * 规格id
   */
  private int unitId;

  /**
   * 规格中文
   */
  private String unitStr;

  public int getUnitId() {
    return unitId;
  }

  public void setUnitId(int unitId) {
    this.unitId = unitId;
  }

  public String getUnitStr() {
    return unitStr;
  }

  public void setUnitStr(String unitStr) {
    this.unitStr = unitStr;
  }

}
