package com.xianzaishi.purchaseadmin.client.supplier.vo;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 供应商信息查询bean
 * @author dongpo
 *
 */
public class SupplierInfoQueryVO extends BaseQuery {
  /**
   * 供应商类型
   */
  private Short userType;
  
  /**
   * 查询条件，包括用户名、手机号、联系人
   */
  private String query;
  
  /**
   * skuId或者69码
   */
  private String skuCode;
  

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }
  
}
