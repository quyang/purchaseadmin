package com.xianzaishi.purchaseadmin.client.order.vo;

import java.io.Serializable;
import java.util.List;

import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderTypeConstants;

/**
 * 子采购单详情页数据
 * @author dongpo
 *
 */
public class SubOrderDetailVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -4164742617763425243L;

  /**
   * 采购订单id
   */
  private Integer purchaseSubId;
  
  /**
   * 采购类型
   */
  private String type;
  
  /**
   * 采购类型码
   */
  private Short typeCode = PurchaseSubOrderTypeConstants.PURCHASE_TYPE_NEW;
  
  /**
   * 采购人
   */
  private String purchasingAgent;
  
  /**
   * 采购单名称
   */
  private String title;
  
  /**
   * 采购数量
   */
  private Integer count;
  
  /**
   * 数量单位
   */
  private String skuUnit;
  
  /**
   * 采购总价
   */
  private Integer settlementPrice;
  
  /**
   * 到货截止日期
   */
  private String expectArriveDate;
  
  /**
   * 采购商信息
   */
  private Integer supplierId;
  
  /**
   * 采购商品信息
   */
  private Long skuId;
  
  /**
   * 质检信息
   */
  private List<Integer> qualityId;
  
  /**
   * 入库信息
   */
  private List<Integer> storageId;
  
  /**
   * 审核不通过原因
   */
  private String checkedFailedReason;
  
  /**
   * 审核人列表
   */
  private List<String> checkerList;
  
  /**
   * 是否显示审核状态
   */
  private Boolean displayReviewStatus = false;
  
  /**
   * 审核是否通过
   */
  private String status;
  
  /**
   * 审核状态码
   */
  private Short statusCode = PurchaseSubOrderStatusConstants.SUBORDER_STATUS_WAITCHECK;

  public Integer getPurchaseSubId() {
    return purchaseSubId;
  }

  public void setPurchaseSubId(Integer purchaseSubId) {
    this.purchaseSubId = purchaseSubId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getPurchasingAgent() {
    return purchasingAgent;
  }
  
  public Short getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(Short typeCode) {
    this.typeCode = typeCode;
  }

  public void setPurchasingAgent(String purchasingAgent) {
    this.purchasingAgent = purchasingAgent;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
  
  public String getSkuUnit() {
    return skuUnit;
  }

  public void setSkuUnit(String skuUnit) {
    this.skuUnit = skuUnit;
  }

  public Integer getSettlementPrice() {
    return settlementPrice;
  }

  public void setSettlementPrice(Integer settlementPrice) {
    this.settlementPrice = settlementPrice;
  }

  public String getExpectArriveDate() {
    return expectArriveDate;
  }

  public void setExpectArriveDate(String expectArriveDate) {
    this.expectArriveDate = expectArriveDate;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public List<Integer> getQualityId() {
    return qualityId;
  }

  public void setQualityId(List<Integer> qualityId) {
    this.qualityId = qualityId;
  }

  public List<Integer> getStorageId() {
    return storageId;
  }

  public void setStorageId(List<Integer> storageId) {
    this.storageId = storageId;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<String> getCheckerList() {
    return checkerList;
  }

  public void setCheckerList(List<String> checkerList) {
    this.checkerList = checkerList;
  }

  public Boolean getDisplayReviewStatus() {
    return displayReviewStatus;
  }

  public void setDisplayReviewStatus(Boolean displayReviewStatus) {
    this.displayReviewStatus = displayReviewStatus;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Short getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Short statusCode) {
    this.statusCode = statusCode;
  }
  
}
