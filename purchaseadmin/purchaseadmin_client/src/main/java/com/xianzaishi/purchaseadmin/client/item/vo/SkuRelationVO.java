package com.xianzaishi.purchaseadmin.client.item.vo;

import org.apache.commons.lang.StringUtils;

import com.xianzaishi.purchaseadmin.client.property.vo.UnitVO;

/**
 * sku对应关系
 * 
 * @author zhancang
 */
public class SkuRelationVO {
  
  public static final String DATE_FORMAT = "yyyy/MM/dd";

  /**
   * 属性件装规格名称
   */
  public static final String PROPERY_JIANZHUANGGUIGE = "件装规格";
  /**
   * 属性件装规格名称
   */
  public static final String PROPERY_JIANZHUANGSHU = "件装数";
  /**
   * 属性件装单位名称
   */
  public static final String PROPERY_JIANZHUANGDANWEI = "件装单位";
  /**
   * 件装规格属性
   */
  public static final String PROPERY_JIANZHUANGGUIGESHUXING = "件装规格属性";

  private long id;
  /**
   * /** 进货skuId
   */
  private long pskuId;

  /**
   * 销售skuId
   */
  private long cskuId;
  /**
   * 商品货号
   */
  private long skuCode;

  /**
   * 商品名称
   */
  private String title;

  /**
   * 整箱成本
   */
  private float packageTotalCost;
  
//  /**
//   * 采购规格，整箱采购或者整合采购等
//   */
//  private UnitVO buyPackageUnit;

  /**
   * 进货单位名称（中文）&id<br/>
   * 优先查看【件装规格】属性，如果取值为1，则取【件装单位】对应属性值<br/>
   * 如果取值大于1，则取【件装规格属性】对应属性值<br/>
   */
  private UnitVO packageUnit;

  /**
   * 进货单位箱规，箱规<br/>
   * 一个大箱多少个进货单位
   */
  private float packageUnitSize;

  /**
   * 进货单位成本<br/>
   * packageTotalCost/packageUnitSize
   */
  private float packageUnitCost;


  /**
   * 当前销售规格,默认等于进货单位
   */
  private UnitVO saleUnit = packageUnit;

  /**
   * 销售单位大小
   */
  private float saleSize;

  /**
   * 销售单位成本<br>
   * (packageUnitCost*saleSize)/procurementCommodityUnitProportion*
   */
  private float saleUnitCost;

  /**
   * 平均移动成本 TODO 需要和业务方确定公式
   */
  private float avgCost = saleUnitCost;

  /**
   * 设置采购生效开始时间
   */
  private String buyStart;

  /**
   * 设置采购生效开始时间
   */
  private Long buyStartTime;
  /**
   * 设置采购生效结束时间
   */
  private String buyEnd = "2026/09/01";
  
  /**
   * 设置采购生效结束时间
   */
  private Long buyEndTime;
  
  /**
   * 设置销售生效开始时间
   */
  private String saleStart;

  /**
   * 设置销售生效开始时间
   */
  private Long saleStartTime;
  
  /**
   * 设置销售生效结束时间
   */
  private String saleEnd = "2026/09/01";
  
  /**
   * 设置销售生效开始时间
   */
  private Long saleEndTime;

  /**
   * 销售价格
   */
  private float salePrice;
  
  /**
   * 毛利润
   */
  private float grossMargin;

//  private ProductSkuVO productSku;
//
//  private ProductSkuVO commoditySku;

  /**
//   * 查询类型<br/>
//   * true:查询优惠数据; false：查询普通数据
//   */
//  private boolean isDiscount;
  
  /**
   * 是否是采购信息变价
   */
  private boolean buyUpdate;
  
  /**
   * 是否是销售信息变价
   */
  private boolean saleUpdate;

  public long getPskuId() {
    return pskuId;
  }

  public void setPskuId(long pskuId) {
    this.pskuId = pskuId;
  }

  public long getCskuId() {
    return cskuId;
  }

  public void setCskuId(long cskuId) {
    this.cskuId = cskuId;
  }

//  public boolean getDiscount() {
//    return isDiscount;
//  }
//  
//  public void setDiscount(boolean isDiscount) {
//    this.isDiscount = isDiscount;
//  }

  public long getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(long skuCode) {
    this.skuCode = skuCode;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public float getPackageTotalCost() {
    return packageTotalCost;
  }

  public void setPackageTotalCost(float packageTotalCost) {
    this.packageTotalCost = packageTotalCost;
  }

  public UnitVO getPackageUnit() {
    return packageUnit;
  }

  public void setPackageUnit(UnitVO packageUnit) {
    this.packageUnit = packageUnit;
  }

  public float getPackageUnitSize() {
    return packageUnitSize;
  }

  public void setPackageUnitSize(float packageUnitSize) {
    this.packageUnitSize = packageUnitSize;
  }

  public float getPackageUnitCost() {
    return packageUnitCost;
  }

  public void setPackageUnitCost(float packageUnitCost) {
    this.packageUnitCost = packageUnitCost;
  }

  public UnitVO getSaleUnit() {
    return saleUnit;
  }

  public void setSaleUnit(UnitVO saleUnit) {
    this.saleUnit = saleUnit;
  }

  public float getSaleSize() {
    return saleSize;
  }

  public void setSaleSize(float saleSize) {
    this.saleSize = saleSize;
  }

  public float getSaleUnitCost() {
    return saleUnitCost;
  }

  public void setSaleUnitCost(float saleUnitCost) {
    this.saleUnitCost = saleUnitCost;
  }

  public float getAvgCost() {
    return avgCost;
  }

  public void setAvgCost(float avgCost) {
    this.avgCost = avgCost;
  }

  public float getSalePrice() {
    return salePrice;
  }

  public void setSalePrice(float salePrice) {
    this.salePrice = salePrice;
  }

  public float getGrossMargin() {
    return grossMargin;
  }

  public void setGrossMargin(float grossMargin) {
    this.grossMargin = grossMargin;
  }

  public UnitVO getBuyPackageUnit() {
    UnitVO v = new UnitVO();
    v.setUnitId(1);
    v.setUnitStr("箱");
    return v;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getBuyStart() {
    return buyStart;
  }

  public void setBuyStart(String buyStart) {
    this.buyStart = buyStart;
  }

  public String getBuyEnd() {
    return buyEnd;
  }

  public void setBuyEnd(String buyEnd) {
    if(StringUtils.isNotEmpty(buyEnd)){
      this.buyEnd = buyEnd;
    }
  }

  public String getSaleStart() {
    return saleStart;
  }

  public void setSaleStart(String saleStart) {
    this.saleStart = saleStart;
  }

  public String getSaleEnd() {
    return saleEnd;
  }

  public void setSaleEnd(String saleEnd) {
    if(StringUtils.isNotEmpty(saleEnd)){
      this.saleEnd = saleEnd;
    }
  }

  public boolean getBuyUpdate() {
    return buyUpdate;
  }

  public void setBuyUpdate(boolean buyUpdate) {
    this.buyUpdate = buyUpdate;
  }

  public boolean getSaleUpdate() {
    return saleUpdate;
  }

  public void setSaleUpdate(boolean saleUpdate) {
    this.saleUpdate = saleUpdate;
  }

  public Long getBuyStartTime() {
    return buyStartTime;
  }

  public void setBuyStartTime(Long buyStartTime) {
    this.buyStartTime = buyStartTime;
  }

  public Long getBuyEndTime() {
    return buyEndTime;
  }

  public void setBuyEndTime(Long buyEndTime) {
    this.buyEndTime = buyEndTime;
  }

  public Long getSaleStartTime() {
    return saleStartTime;
  }

  public void setSaleStartTime(Long saleStartTime) {
    this.saleStartTime = saleStartTime;
  }

  public Long getSaleEndTime() {
    return saleEndTime;
  }

  public void setSaleEndTime(Long saleEndTime) {
    this.saleEndTime = saleEndTime;
  }
}
