package com.xianzaishi.purchaseadmin.client.inventory.vo;

import java.util.List;

public class InventoryVO {

  /**
   * 全部库存数量
   */
  private String num;
  
  /**
   * 安全库存数量
   */
  private String safeNum;

  /**
   * 全部库位数量
   */
  private List<InventoryDetailVO>  availableInventory;

  /**
   * 推荐库位数量
   */
  private List<InventoryDetailVO> recommondInventory;

  public String getNum() {
    return num;
  }

  public void setNum(String num) {
    this.num = num;
  }

  public String getSafeNum() {
    return safeNum;
  }

  public void setSafeNum(String safeNum) {
    this.safeNum = safeNum;
  }

  public List<InventoryDetailVO> getAvailableInventory() {
    return availableInventory;
  }

  public void setAvailableInventory(List<InventoryDetailVO> availableInventory) {
    this.availableInventory = availableInventory;
  }

  public List<InventoryDetailVO> getRecommondInventory() {
    return recommondInventory;
  }

  public void setRecommondInventory(List<InventoryDetailVO> recommondInventory) {
    this.recommondInventory = recommondInventory;
  }

}
