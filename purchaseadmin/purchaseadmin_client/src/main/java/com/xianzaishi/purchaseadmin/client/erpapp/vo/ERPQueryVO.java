package com.xianzaishi.purchaseadmin.client.erpapp.vo;

/**
 * Created by Administrator on 2017/3/1.
 */
public class ERPQueryVO {

  /**
   * 接口版本
   */
  private String version;

  /**
   * data数据
   */
  private QueryData data;

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public QueryData getData() {
    return data;
  }

  public void setData(QueryData data) {
    this.data = data;
  }
}
