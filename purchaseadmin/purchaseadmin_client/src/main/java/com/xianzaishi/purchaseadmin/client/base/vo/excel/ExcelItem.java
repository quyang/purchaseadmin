package com.xianzaishi.purchaseadmin.client.base.vo.excel;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPicture;

import com.google.common.collect.Lists;

public class ExcelItem {

  private List<String> property;

  private List<HSSFPicture> pictureList;

  private List<HSSFPicture> introPictureList;

  public List<String> getProperty() {
    return property;
  }

  public void setProperty(List<String> property) {
    this.property = property;
  }

  public void addProperty(String property) {
    if (null == this.property) {
      this.property = Lists.newArrayList();
    }
    this.property.add(property);
  }

  public List<HSSFPicture> getPictureList() {
    return pictureList;
  }

  public void setPictureList(List<HSSFPicture> pictureList) {
    this.pictureList = pictureList;
  }

  public List<HSSFPicture> getIntroPictureList() {
    return introPictureList;
  }

  public void setIntroPictureList(List<HSSFPicture> introPictureList) {
    this.introPictureList = introPictureList;
  }
}
