package com.xianzaishi.purchaseadmin.client.item.vo;

import java.util.List;

/**
 * 展示规格格式
 * @author dongpo
 *
 */
public class ItemDisplaySpecificationVO {
  
  private String name;
  
  private List<String> values;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getValues() {
    return values;
  }

  public void setValues(List<String> values) {
    this.values = values;
  }

}
