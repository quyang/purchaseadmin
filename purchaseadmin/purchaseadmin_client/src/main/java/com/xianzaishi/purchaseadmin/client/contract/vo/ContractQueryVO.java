package com.xianzaishi.purchaseadmin.client.contract.vo;

/**
 * 供应商列表页面查询参数vo
 * 
 * @author zhancang
 */
public class ContractQueryVO {

  private String companyName = null;

  private short status = -1;

  private String purchasingAgentName = null;

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getPurchasingAgentName() {
    return purchasingAgentName;
  }

  public void setPurchasingAgentName(String purchasingAgentName) {
    this.purchasingAgentName = purchasingAgentName;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

}
