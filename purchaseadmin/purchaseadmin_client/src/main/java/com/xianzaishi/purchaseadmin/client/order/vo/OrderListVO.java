package com.xianzaishi.purchaseadmin.client.order.vo;

import java.io.Serializable;

import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;

/**
 * 销售商品列表
 * @author dongpo
 *
 */
public class OrderListVO implements Serializable {
  
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 8817427566105950500L;

  /**
   * 订单号
   */
  private Integer purchaseId;
  
  /**
   * 购货金额
   */
  private String purchaseAmount;
  
  /**
   * 供应商名称
   */
  private String supplier;
  
  /**
   * 采购日期
   */
  private String purchaseData;
  
  /**
   * 到货时间
   */
  private String arriveDate;
  
  /**
   * 付费订单状态
   */
  private String payStatus;
  
  /**
   * 付费订单状态码
   */
  private Short payStatusCode;
  
  /**
   * 入库订单状态
   */
  private String storageStatus;
  
  /**
   * 入库状态码
   */
  private Short storageStatusCode;
  
  /**
   * 备注
   */
  private String remarks;
  
  /**
   * 审核状态
   */
  private Short auditingStatus = PurchaseSubOrderStatusConstants.SUBORDER_STATUS_WAITCHECK;
  
  /**
   * 审核状态
   */
  private String auditingStatusString;
  
  /**
   * 实际金额
   */
  private String actualAmount;
  
  public Integer getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }

  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }

  public String getPurchaseAmount() {
    return purchaseAmount;
  }

  public void setPurchaseAmount(String purchaseAmount) {
    this.purchaseAmount = purchaseAmount;
  }

  public String getPurchaseData() {
    return purchaseData;
  }

  public void setPurchaseData(String purchaseData) {
    this.purchaseData = purchaseData;
  }

  public String getArriveDate() {
    return arriveDate;
  }

  public void setArriveDate(String arriveDate) {
    this.arriveDate = arriveDate;
  }

  public String getPayStatus() {
    return payStatus;
  }

  public void setPayStatus(String payStatus) {
    this.payStatus = payStatus;
  }

  public Short getPayStatusCode() {
    return payStatusCode;
  }

  public void setPayStatusCode(Short payStatusCode) {
    this.payStatusCode = payStatusCode;
  }

  public String getStorageStatus() {
    return storageStatus;
  }

  public void setStorageStatus(String storageStatus) {
    this.storageStatus = storageStatus;
  }

  public Short getStorageStatusCode() {
    return storageStatusCode;
  }

  public void setStorageStatusCode(Short storageStatusCode) {
    this.storageStatusCode = storageStatusCode;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public Short getAuditingStatus() {
    return auditingStatus;
  }

  public void setAuditingStatus(Short auditingStatus) {
    this.auditingStatus = auditingStatus;
  }

  public String getAuditingStatusString() {
    return auditingStatusString;
  }

  public void setAuditingStatusString(String auditingStatusString) {
    this.auditingStatusString = auditingStatusString;
  }

  public String getActualAmount() {
    return actualAmount;
  }

  public void setActualAmount(String actualAmount) {
    this.actualAmount = actualAmount;
  }
  
}
