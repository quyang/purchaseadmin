package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.ArrayList;

public class PutPurchaseRequestVo {
	
    private String  no;//采购单号
    private int caigouNum;//采购数量
    private ArrayList<RequestPurchaseInfoModel> list;


    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getCaigouNum() {
        return caigouNum;
    }

    public void setCaigouNum(int caigouNum) {
        this.caigouNum = caigouNum;
    }

    public ArrayList<RequestPurchaseInfoModel> getList() {
        return list;
    }

    public void setList(ArrayList<RequestPurchaseInfoModel> list) {
        this.list = list;
    }

}
