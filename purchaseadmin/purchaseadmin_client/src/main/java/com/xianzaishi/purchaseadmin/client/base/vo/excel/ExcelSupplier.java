package com.xianzaishi.purchaseadmin.client.base.vo.excel;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPicture;

import com.google.common.collect.Lists;

public class ExcelSupplier {

  private List<String> property;

  private List<HSSFPicture> yingyezhizhaoList;

  private List<HSSFPicture> zuzhijigoudaimaList;

  private List<HSSFPicture> shuiwudengjiList;

  private List<HSSFPicture> kaihuhangxukezhengList;

  private List<HSSFPicture> shengchanxukezhengList;

  private List<HSSFPicture> guojitiaomazhengList;

  private List<HSSFPicture> shipingliutongxukezhengList;

  private List<HSSFPicture> otherxukezhengList;

  public List<String> getProperty() {
    return property;
  }

  public void setProperty(List<String> property) {
    this.property = property;
  }

  public void addProperty(String property) {
    if (null == this.property) {
      this.property = Lists.newArrayList();
    }
    this.property.add(property);
  }
  
  public List<HSSFPicture> getYingyezhizhaoList() {
    return yingyezhizhaoList;
  }

  public void setYingyezhizhaoList(List<HSSFPicture> yingyezhizhaoList) {
    this.yingyezhizhaoList = yingyezhizhaoList;
  }

  public List<HSSFPicture> getZuzhijigoudaimaList() {
    return zuzhijigoudaimaList;
  }

  public void setZuzhijigoudaimaList(List<HSSFPicture> zuzhijigoudaimaList) {
    this.zuzhijigoudaimaList = zuzhijigoudaimaList;
  }

  public List<HSSFPicture> getShuiwudengjiList() {
    return shuiwudengjiList;
  }

  public void setShuiwudengjiList(List<HSSFPicture> shuiwudengjiList) {
    this.shuiwudengjiList = shuiwudengjiList;
  }

  public List<HSSFPicture> getKaihuhangxukezhengList() {
    return kaihuhangxukezhengList;
  }

  public void setKaihuhangxukezhengList(List<HSSFPicture> kaihuhangxukezhengList) {
    this.kaihuhangxukezhengList = kaihuhangxukezhengList;
  }

  public List<HSSFPicture> getShengchanxukezhengList() {
    return shengchanxukezhengList;
  }

  public void setShengchanxukezhengList(List<HSSFPicture> shengchanxukezhengList) {
    this.shengchanxukezhengList = shengchanxukezhengList;
  }

  public List<HSSFPicture> getGuojitiaomazhengList() {
    return guojitiaomazhengList;
  }

  public void setGuojitiaomazhengList(List<HSSFPicture> guojitiaomazhengList) {
    this.guojitiaomazhengList = guojitiaomazhengList;
  }

  public List<HSSFPicture> getShipingliutongxukezhengList() {
    return shipingliutongxukezhengList;
  }

  public void setShipingliutongxukezhengList(List<HSSFPicture> shipingliutongxukezhengList) {
    this.shipingliutongxukezhengList = shipingliutongxukezhengList;
  }

  public List<HSSFPicture> getOtherxukezhengList() {
    return otherxukezhengList;
  }

  public void setOtherxukezhengList(List<HSSFPicture> otherxukezhengList) {
    this.otherxukezhengList = otherxukezhengList;
  }
}
