package com.xianzaishi.purchaseadmin.client.inventory.vo;

/**
 * Created by Administrator on 2017/5/11.
 */
public class StorageDetailInfoVO {

  private Long storageId = null;

  private Long agencyId = null;

  private Long skuId = null;

  private String skuBarcode = null;

  private String skuName = null;

  private String number = null;

  private Integer numberReal = null;

  private Long positionId = null;

  private String positionCode = null;

  private String positionBarcode = null;

  private String saleUnit = null;

  private Long saleUnitId = null;

  private String specUnit = null;

  private Long specUnitId = null;

  private Integer spec = null;

  private Integer saleUnitType = null;

  private Long relationId = null;//子采购单id

  public Long getStorageId() {
    return storageId;
  }

  public void setStorageId(Long storageId) {
    this.storageId = storageId;
  }

  public Long getAgencyId() {
    return agencyId;
  }

  public void setAgencyId(Long agencyId) {
    this.agencyId = agencyId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getSkuBarcode() {
    return skuBarcode;
  }

  public void setSkuBarcode(String skuBarcode) {
    this.skuBarcode = skuBarcode;
  }

  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Integer getNumberReal() {
    return numberReal;
  }

  public void setNumberReal(Integer numberReal) {
    this.numberReal = numberReal;
  }

  public Long getPositionId() {
    return positionId;
  }

  public void setPositionId(Long positionId) {
    this.positionId = positionId;
  }

  public String getPositionCode() {
    return positionCode;
  }

  public void setPositionCode(String positionCode) {
    this.positionCode = positionCode;
  }

  public String getPositionBarcode() {
    return positionBarcode;
  }

  public void setPositionBarcode(String positionBarcode) {
    this.positionBarcode = positionBarcode;
  }

  public String getSaleUnit() {
    return saleUnit;
  }

  public void setSaleUnit(String saleUnit) {
    this.saleUnit = saleUnit;
  }

  public Long getSaleUnitId() {
    return saleUnitId;
  }

  public void setSaleUnitId(Long saleUnitId) {
    this.saleUnitId = saleUnitId;
  }

  public String getSpecUnit() {
    return specUnit;
  }

  public void setSpecUnit(String specUnit) {
    this.specUnit = specUnit;
  }

  public Long getSpecUnitId() {
    return specUnitId;
  }

  public void setSpecUnitId(Long specUnitId) {
    this.specUnitId = specUnitId;
  }

  public Integer getSpec() {
    return spec;
  }

  public void setSpec(Integer spec) {
    this.spec = spec;
  }

  public Integer getSaleUnitType() {
    return saleUnitType;
  }

  public void setSaleUnitType(Integer saleUnitType) {
    this.saleUnitType = saleUnitType;
  }

  public Long getRelationId() {
    return relationId;
  }

  public void setRelationId(Long relationId) {
    this.relationId = relationId;
  }
}
