package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.util.Date;
import java.util.List;

/**
 * Created by quyang on 2017/4/27.
 */
public class MarketActivityReturnVO {

  /**
   * 工号
   */
  private Integer operator;

  private String operatorName;

  private Integer id;

  /**
   * 活动名称
   */
  private String name;
  /**
   * 活动状态
   */
  private Short status;

  private String statusString;

  private List<String> statusList;

  /**
   * 活动类型
   */
  private Short type;

  private String typeString;

  /**
   * 开始时间
   */
  private Date gmtStart;

  private String gmtStartString;

  /**
   * 结束时间
   */
  private Date gmtEnd;

  private String gmtEndString;

  private Date gmtCreate;

  private Date gmtModified;


  private Object activityInfo;

  public List<String> getStatusList() {
    return statusList;
  }

  public void setStatusList(List<String> statusList) {
    this.statusList = statusList;
  }

  public Integer getOperator() {
    return operator;
  }

  public void setOperator(Integer operator) {
    this.operator = operator;
  }

  public String getOperatorName() {
    return operatorName;
  }

  public void setOperatorName(String operatorName) {
    this.operatorName = operatorName;
  }

  public String getTypeString() {
    return typeString;
  }

  public void setTypeString(String typeString) {
    this.typeString = typeString;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }

  public Object getActivityInfo() {
    return activityInfo;
  }

  public void setActivityInfo(Object activityInfo) {
    this.activityInfo = activityInfo;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public String getGmtStartString() {
    return gmtStartString;
  }

  public void setGmtStartString(String gmtStartString) {
    this.gmtStartString = gmtStartString;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public String getGmtEndString() {
    return gmtEndString;
  }

  public void setGmtEndString(String gmtEndString) {
    this.gmtEndString = gmtEndString;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
}
