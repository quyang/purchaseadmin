package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import com.xianzaishi.itemcenter.common.query.BaseQuery;
import java.util.Date;

/**
 * Created by quyang on 2017/4/16.
 */
public class CouponVO extends BaseQuery {

  private Long id;

  private String title;

  private String rules;

  /**
   * 设置优惠券发放规则，比较的条件包括当前券数量和有效券数量两个维度
   */
  private String sendRules;
  /**
   * 该字段不知道干嘛的，没有调用 by zhancang
   */
  private Long crowdId;

  /**
   * type=1 看样子是现金券<br/>
   * type=77 早餐券<br/>
   * type=11 新人礼包券<br/>
   */
  private Short type;

  /**
   * 优惠券开始生效时间
   */
  private Date gmtStart;

  private String gmtStartString;


  /**
   * 优惠券开始失效时间
   */
  private Date gmtEnd;

  private String gmtEndString;

  /**
   * 优惠券持续时常，天为单位<br/>
   * gmtStart 必须设置。gmtEnd和gmtDayDuration必须设置1个<br/>
   * 领券时间早于gmtStart时，券生效时间参考gmtStart。晚于gmtStart时，生效时间为当天0点<br/>
   * 领券结束时间参考gmtEnd或者券生效时间加上gmtDayDuration<br/>
   * 如果gmtEnd和gmtDayDuration同时设置时，哪个时间早使用哪个时间作为券结束时间
   */
  private Integer gmtDayDuration;

  private Date gmtCreate;

  private Date gmtModify;

  /**
   * INVALID("无效",(short)-1),
   * INIT("初始化",(short)0),
   * VALID("有效",(short)1);
   */
  private Short status;

  /**
   * 抵扣金额 by zhancang<br/>
   * 丢失各种精度，不推荐使用
   */
  private Double amount;

  /**
   * 推荐使用分为单位价格
   */
  private Integer amountCent;

  /**
   * channelType=1代表线上渠道<br/>
   * channelType=2代表线下渠道<br/>
   */
  private Short channelType;

  /**
   * 折扣使用前置价格条件 by zhancang<br/>
   * 丢失各种精度，不推荐使用
   */
  private Double amountLimit;

  /**
   * 分为单价的价格限制
   */
  private Integer amountLimitCent;


  public String getGmtStartString() {
    return gmtStartString;
  }

  public void setGmtStartString(String gmtStartString) {
    this.gmtStartString = gmtStartString;
  }

  public String getGmtEndString() {
    return gmtEndString;
  }

  public void setGmtEndString(String gmtEndString) {
    this.gmtEndString = gmtEndString;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getRules() {
    return rules;
  }

  public void setRules(String rules) {
    this.rules = rules;
  }

  public String getSendRules() {
    return sendRules;
  }

  public void setSendRules(String sendRules) {
    this.sendRules = sendRules;
  }

  public Long getCrowdId() {
    return crowdId;
  }

  public void setCrowdId(Long crowdId) {
    this.crowdId = crowdId;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Integer getGmtDayDuration() {
    return gmtDayDuration;
  }

  public void setGmtDayDuration(Integer gmtDayDuration) {
    this.gmtDayDuration = gmtDayDuration;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModify() {
    return gmtModify;
  }

  public void setGmtModify(Date gmtModify) {
    this.gmtModify = gmtModify;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Integer getAmountCent() {
    return amountCent;
  }

  public void setAmountCent(Integer amountCent) {
    this.amountCent = amountCent;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Double getAmountLimit() {
    return amountLimit;
  }

  public void setAmountLimit(Double amountLimit) {
    this.amountLimit = amountLimit;
  }

  public Integer getAmountLimitCent() {
    return amountLimitCent;
  }

  public void setAmountLimitCent(Integer amountLimitCent) {
    this.amountLimitCent = amountLimitCent;
  }
}
