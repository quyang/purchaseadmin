package com.xianzaishi.purchaseadmin.client.base.vo.excel;

import java.util.List;

import com.google.common.collect.Lists;

public class ExcelEmployee {

  private List<String> property;

  public List<String> getProperty() {
    return property;
  }

  public void setProperty(List<String> property) {
    this.property = property;
  }

  public void addProperty(String property) {
    if (null == this.property) {
      this.property = Lists.newArrayList();
    }
    this.property.add(property);
  }
}
