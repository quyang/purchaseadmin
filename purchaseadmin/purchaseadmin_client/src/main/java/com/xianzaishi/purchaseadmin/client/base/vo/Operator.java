package com.xianzaishi.purchaseadmin.client.base.vo;

/**
 * 操作动作定义
 * 
 * @author zhancang
 *
 */
public class Operator {

  /**
   * 操作目标连接 add1
   */
  private String operatorLink;

  /**
   * 操作名称
   */
  private String operatorName;

  /**
   * 1:当前页面跳转 2:新开页面跳转 3:弹窗 4:提交表单
   */
  private int operatorActionType;

  /**
   * 1:链接 2:按钮
   */
  private int iconType;

  public String getOperatorLink() {
    return operatorLink;
  }

  public void setOperatorLink(String operatorLink) {
    this.operatorLink = operatorLink;
  }

  public String getOperatorName() {
    return operatorName;
  }

  public void setOperatorName(String operatorName) {
    this.operatorName = operatorName;
  }

  public int getOperatorActionType() {
    return operatorActionType;
  }

  public void setOperatorActionType(int operatorActionType) {
    this.operatorActionType = operatorActionType;
  }

  public int getIconType() {
    return iconType;
  }

  public void setIconType(int iconType) {
    this.iconType = iconType;
  }

}
