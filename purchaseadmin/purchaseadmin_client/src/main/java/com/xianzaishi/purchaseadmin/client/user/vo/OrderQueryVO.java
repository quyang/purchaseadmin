package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 *
 * @author quyang 2017-2-20 16:44:42
 *
 */
public class OrderQueryVO  {

  /**
   * 用户id
   */
  private Long userId;
  /**
   * 订单id
   */
  private Long orderId;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }
}
