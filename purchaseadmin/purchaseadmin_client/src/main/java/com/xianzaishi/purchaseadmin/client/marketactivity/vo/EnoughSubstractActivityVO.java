package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.util.LinkedList;

/**
 *  满件活动
 * Created by quyang on 2017/4/26.
 */
public class EnoughSubstractActivityVO {



  /**
   * 折扣
   */
  private String discount;

  /**
   * 是否包邮
   */
  private Boolean post;
  /**
   * 满n件
   */
  private String discountCount;

  /**
   * 优惠券Id集合
   */
  private LinkedList<Long> couponIds ;

  /**
   * 商品skuId集合
   */
  private LinkedList<Long> skuIds ;

  public String getDiscount() {
    return discount;
  }

  public void setDiscount(String discount) {
    this.discount = discount;
  }

  public Boolean getPost() {
    return post;
  }

  public void setPost(Boolean post) {
    this.post = post;
  }

  public String getDiscountCount() {
    return discountCount;
  }

  public void setDiscountCount(String discountCount) {
    this.discountCount = discountCount;
  }

  public LinkedList<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(LinkedList<Long> couponIds) {
    this.couponIds = couponIds;
  }

  public LinkedList<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(LinkedList<Long> skuIds) {
    this.skuIds = skuIds;
  }
}
