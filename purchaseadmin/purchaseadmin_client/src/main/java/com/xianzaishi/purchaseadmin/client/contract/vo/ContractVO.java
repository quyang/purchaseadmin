package com.xianzaishi.purchaseadmin.client.contract.vo;


/**
 * 合同详情页面vo
 * 
 * @author zhancang
 */
public class ContractVO {

  private int contractId;

  private String supplierName;

  private String companyName;

  private String purchasingAgentName;

  private short status;

  private String contractPartAcompanyName;

  private String contractPartAContactName;

  private String contractPartAPhone;

  private String contractPartBcompanyName;

  private String contractPartBContactName;

  private String contractPartBPhone;

  private String mailAddress;

  public int getContractId() {
    return contractId;
  }

  public void setContractId(int contractId) {
    this.contractId = contractId;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getPurchasingAgentName() {
    return purchasingAgentName;
  }

  public void setPurchasingAgentName(String purchasingAgentName) {
    this.purchasingAgentName = purchasingAgentName;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

  public String getContractPartAcompanyName() {
    return contractPartAcompanyName;
  }

  public void setContractPartAcompanyName(String contractPartAcompanyName) {
    this.contractPartAcompanyName = contractPartAcompanyName;
  }

  public String getContractPartAContactName() {
    return contractPartAContactName;
  }

  public void setContractPartAContactName(String contractPartAContactName) {
    this.contractPartAContactName = contractPartAContactName;
  }

  public String getContractPartAPhone() {
    return contractPartAPhone;
  }

  public void setContractPartAPhone(String contractPartAPhone) {
    this.contractPartAPhone = contractPartAPhone;
  }

  public String getContractPartBcompanyName() {
    return contractPartBcompanyName;
  }

  public void setContractPartBcompanyName(String contractPartBcompanyName) {
    this.contractPartBcompanyName = contractPartBcompanyName;
  }

  public String getContractPartBContactName() {
    return contractPartBContactName;
  }

  public void setContractPartBContactName(String contractPartBContactName) {
    this.contractPartBContactName = contractPartBContactName;
  }

  public String getContractPartBPhone() {
    return contractPartBPhone;
  }

  public void setContractPartBPhone(String contractPartBPhone) {
    this.contractPartBPhone = contractPartBPhone;
  }

  public String getMailAddress() {
    return mailAddress;
  }

  public void setMailAddress(String mailAddress) {
    this.mailAddress = mailAddress;
  }

}
