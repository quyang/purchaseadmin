package com.xianzaishi.purchaseadmin.client.item.vo;

import java.util.List;

/**
 * 商品详情基本对象
 * 
 * @author zhancang
 */
public class IntorductionDataVO {

  private int id;

  private String source;

  private int type;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }
}
