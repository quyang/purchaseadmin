package com.xianzaishi.purchaseadmin.client.item.vo;

public class PurchaseItemVO extends SubPurchaseItemVO {

  private Integer purchaseId;

  public Integer getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }
  
}
