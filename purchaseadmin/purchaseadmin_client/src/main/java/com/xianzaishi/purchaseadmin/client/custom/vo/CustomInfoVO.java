package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * 客户信息
 * @author dongpo
 *
 */
public class CustomInfoVO {
  
  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 用户名
   */
  private String name;
  
  /**
   * 用户联系方式
   */
  private Long phone;
  
  /**
   * 用户详细地址
   */
  private String address;
  
  /**
   * 会员卡号
   */
  private String vipCardNO;
  
  /**
   * 会员级别
   */
  private String memberLevel;

  /**
   * 用户类型 1 表示普通用户
   */
  private Short userType;

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getVipCardNO() {
    return vipCardNO;
  }

  public void setVipCardNO(String vipCardNO) {
    this.vipCardNO = vipCardNO;
  }

  public String getMemberLevel() {
    return memberLevel;
  }

  public void setMemberLevel(String memberLevel) {
    this.memberLevel = memberLevel;
  }
  
}
