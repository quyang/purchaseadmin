package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * Created by quyang on 2017/3/16.
 */
public class CommitRefoundQueryVO {


  /**
   * 订单id
   */
  private String orderId;

  /**
   * 退款金额
   */
  private Double sum;

  /**
   * 退款原因
   */
  private String refundReason;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Double getSum() {
    return sum;
  }

  public void setSum(Double sum) {
    this.sum = sum;
  }

  public String getRefundReason() {
    return refundReason;
  }

  public void setRefundReason(String refundReason) {
    this.refundReason = refundReason;
  }
}
