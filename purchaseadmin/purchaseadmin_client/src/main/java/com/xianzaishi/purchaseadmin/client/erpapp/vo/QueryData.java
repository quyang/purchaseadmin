package com.xianzaishi.purchaseadmin.client.erpapp.vo;

import java.util.List;

/**
 * Created by quyang on 2017/3/1.
 */
public class QueryData {

  /**
   * 查询时间
   */
  private Long time;

  /**
   * 订单id
   */
  private Long orderId;

  /**
   * 手机号
   */
  private Long phone;

  /**
   * 是否线下
   */
  private Boolean offLine;

  /**
   * 类目集合
   */
  private List<Integer> catIdList;

  public Boolean getOffLine() {
    return offLine;
  }

  public void setOffLine(Boolean offLine) {
    this.offLine = offLine;
  }

  public List<Integer> getCatIdList() {
    return catIdList;
  }

  public void setCatIdList(List<Integer> catIdList) {
    this.catIdList = catIdList;
  }

  public Long getTime() {
    return time;
  }

  public void setTime(Long time) {
    this.time = time;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }
}
