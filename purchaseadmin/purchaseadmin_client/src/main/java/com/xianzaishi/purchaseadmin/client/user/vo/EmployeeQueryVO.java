package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * Created by Administrator on 2017/3/15.
 */
public class EmployeeQueryVO {
  /**
   * 版本
   */
  private Integer version;

  /**
   * 请求数据
   */
  private QueryDataVO data;

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public QueryDataVO getData() {
    return data;
  }

  public void setData(QueryDataVO data) {
    this.data = data;
  }
}
