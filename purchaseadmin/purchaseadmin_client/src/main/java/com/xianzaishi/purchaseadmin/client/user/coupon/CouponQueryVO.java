package com.xianzaishi.purchaseadmin.client.user.coupon;

import com.xianzaishi.purchaseadmin.client.bill.vo.BillQueryVO;
import java.util.List;

/**
 * Created by quyang on 2017/2/16.
 */
public class CouponQueryVO {


  /**
   * 封装要插入发票信息
   */
  List<BillQueryVO> bill;

  public List<BillQueryVO> getBill() {
    return bill;
  }

  public void setBill(List<BillQueryVO> bill) {
    this.bill = bill;
  }
}
