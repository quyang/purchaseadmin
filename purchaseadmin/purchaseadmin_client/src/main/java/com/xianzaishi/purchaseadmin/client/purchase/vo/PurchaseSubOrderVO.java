package com.xianzaishi.purchaseadmin.client.purchase.vo;

import java.util.Date;
import java.util.List;

/**
 * Created by quyang on 2017/4/21.
 */
public class PurchaseSubOrderVO {


  /**
   * 创建人
   */
  private String createName;

  /**
   * 供应商
   */
  private String supplierName;

  /**
   * 具体单笔sku采购详细id
   */
  private Integer purchaseSubId;

  /**
   * 采购单状态
   */
  private Short status;

  /**
   * 采购单流转状态
   */
  private Short flowStatus;

  /**
   * 采购单类型
   */
  private Short type;

  /**
   * skuId
   */
  private Long skuId;

  /**
   * 对应总采购单id
   */
  private Integer purchaseId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 入库编号
   */
  private Long storageId;

  /**
   * 采购员id
   */
  private Integer purchasingAgent;

  /**
   * 对应合同id
   */
  private Integer contractId;

  /**
   * 标题
   */
  private String title;

  /**
   * 采购生效时间
   */
  private Date purchaseDate;

  /**
   * 期望到货时间
   */
  private Date expectArriveDate;

  /**
   * 仓库号
   */
  private Integer wareHouse;

  /**
   * 采购数量
   */
  private Integer count;

  /**
   * 单箱成本
   */
  private Integer unitCost;

  /**
   * 真实结算金额
   */
  private Integer settlementPrice;

  /**
   * 创建人
   */
  private Integer creator;

  /**
   *
   */
  private Date gmtCreate;

  /**
   *
   */
  private Date gmtModified;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 审核人列表
   */
  private List<Integer> checkerList;

  /**
   * 审核信息
   */
  private String checkinfo;

  /**
   * 实际采购时间
   */
  private Date arriveDate;

  /**
   * 标识是否是称重商品，0标识标品，1标识称重商品
   */
  private Short steelyardSku;

  /**
   * 箱规
   */
  private Integer pcProportion;

  /**
   * 箱规单位
   */
  private String pcProportionUnit;

  /**
   * 是否采购  0 已采购 1 表示未采购
   */
  private Short partTag;

  /**
   * plu码
   */
  private Long csku_id;

  public Long getCsku_id() {
    return csku_id;
  }

  public void setCsku_id(Long csku_id) {
    this.csku_id = csku_id;
  }

  public String getCreateName() {
    return createName;
  }

  public void setCreateName(String createName) {
    this.createName = createName;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public Integer getPurchaseSubId() {
    return purchaseSubId;
  }

  public void setPurchaseSubId(Integer purchaseSubId) {
    this.purchaseSubId = purchaseSubId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getFlowStatus() {
    return flowStatus;
  }

  public void setFlowStatus(Short flowStatus) {
    this.flowStatus = flowStatus;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public Long getStorageId() {
    return storageId;
  }

  public void setStorageId(Long storageId) {
    this.storageId = storageId;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    this.purchasingAgent = purchasingAgent;
  }

  public Integer getContractId() {
    return contractId;
  }

  public void setContractId(Integer contractId) {
    this.contractId = contractId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(Date purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public Date getExpectArriveDate() {
    return expectArriveDate;
  }

  public void setExpectArriveDate(Date expectArriveDate) {
    this.expectArriveDate = expectArriveDate;
  }

  public Integer getWareHouse() {
    return wareHouse;
  }

  public void setWareHouse(Integer wareHouse) {
    this.wareHouse = wareHouse;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Integer getUnitCost() {
    return unitCost;
  }

  public void setUnitCost(Integer unitCost) {
    this.unitCost = unitCost;
  }

  public Integer getSettlementPrice() {
    return settlementPrice;
  }

  public void setSettlementPrice(Integer settlementPrice) {
    this.settlementPrice = settlementPrice;
  }

  public Integer getCreator() {
    return creator;
  }

  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<Integer> getCheckerList() {
    return checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo;
  }

  public Date getArriveDate() {
    return arriveDate;
  }

  public void setArriveDate(Date arriveDate) {
    this.arriveDate = arriveDate;
  }

  public Short getSteelyardSku() {
    return steelyardSku;
  }

  public void setSteelyardSku(Short steelyardSku) {
    this.steelyardSku = steelyardSku;
  }

  public Integer getPcProportion() {
    return pcProportion;
  }

  public void setPcProportion(Integer pcProportion) {
    this.pcProportion = pcProportion;
  }

  public String getPcProportionUnit() {
    return pcProportionUnit;
  }

  public void setPcProportionUnit(String pcProportionUnit) {
    this.pcProportionUnit = pcProportionUnit;
  }

  public Short getPartTag() {
    return partTag;
  }

  public void setPartTag(Short partTag) {
    this.partTag = partTag;
  }
}
