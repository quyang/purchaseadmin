package com.xianzaishi.purchaseadmin.client.activity.vo;

import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO;

public class ActivityStepVO extends ActivityStepDTO{

  private static final long serialVersionUID = 1L;
  
  private int pageId;
  
  public int getPageId() {
    return pageId;
  }

  public void setPageId(int pageId) {
    this.pageId = pageId;
  }
}
