package com.xianzaishi.purchaseadmin.client.inventory.vo;

import com.xianzaishi.wms.track.vo.OutgoingVO;

public class OutgointDetailAuditVO extends OutgoingVO {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -6039934857027986750L;
  /**
   * 操作原因
   */
  private String opReasonString;
  
  /**
   * 出库状态
   */
  private String statusString;

  public String getOpReasonString() {
    return opReasonString;
  }

  public void setOpReasonString(String opReasonString) {
    this.opReasonString = opReasonString;
  }

  public String getStatusString() {
    return statusString;
  }

  public void setStatusString(String statusString) {
    this.statusString = statusString;
  }
  
  
}
