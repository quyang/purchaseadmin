package com.xianzaishi.purchaseadmin.client.inventory.vo;

import java.util.List;

public class OrderUpdateInventoryVO {


  /**
   * erp操作者
   */
  private Integer operate;

  private Long orderId;
  
  private List<OrderSkuInfo> skuList;
  
  /**
   * 订单处理逻辑  0:根据退换货逻辑，订单只做补差价事情，包括调用部分退款退款和记录用户补差价信息<br/>
   * 1:订单已经退款，只调用后台做补库存相关逻辑
   */
  private Short orderProcessType;

  private OrderItemTransformDetail orderItemTransformDetail;

  public Integer getOperate() {
    return operate;
  }

  public void setOperate(Integer operate) {
    this.operate = operate;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public List<OrderSkuInfo> getSkuList() {
    return skuList;
  }

  public void setSkuList(List<OrderSkuInfo> skuList) {
    this.skuList = skuList;
  }

  public Short getOrderProcessType() {
    return orderProcessType;
  }

  public void setOrderProcessType(Short orderProcessType) {
    this.orderProcessType = orderProcessType;
  }

  public OrderItemTransformDetail getOrderItemTransformDetail() {
    return orderItemTransformDetail;
  }

  public void setOrderItemTransformDetail(OrderItemTransformDetail orderItemTransformDetail) {
    this.orderItemTransformDetail = orderItemTransformDetail;
  }


  public static class OrderProcessTypeConstant {

    /**
     * 退换货
     */
    public static final Short RETURN_EXCHANGE_TYPE = 0;

    /**
     * 订单已经退款，只调用后台做补库存相关逻辑
     */
    public static final Short REFOUND_TYPE = 1;


    public static Boolean hasType(Short type) {
      if (RETURN_EXCHANGE_TYPE.equals(type) || REFOUND_TYPE.equals(type)) {
        return true;
      }
      return false;
    }



  }
  
}
