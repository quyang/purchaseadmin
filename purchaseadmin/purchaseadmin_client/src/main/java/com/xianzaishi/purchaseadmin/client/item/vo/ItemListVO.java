package com.xianzaishi.purchaseadmin.client.item.vo;

public class ItemListVO {
  /**
   * 返回商品69码
   */
  private Long sku69Id;
  /**
   * 发布前台商品时，关联前后台sku，需要页面传值并传送后台
   */
  private Long skuId;
  /**
   * 分为单位的价格描述
   */
  private Integer price;
  /**
   * 商品名称
   */
  private String title;
  /**
   * 生产数量，代表库存
   */
  private Integer inventory;
  /**
   * 公司名称
   */
  private String companyName;
  /**
   * 销售规格
   */
  private String saleUnit;
  /**
   * 销售方式
   */
  private String saleStyle;


  private Integer pageSize;

  private Integer currentPage;

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Long getSku69Id() {
    return sku69Id;
  }
  public void setSku69Id(Long sku69Id) {
    this.sku69Id = sku69Id;
  }
  public Long getSkuId() {
    return skuId;
  }
  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }
  public Integer getPrice() {
    return price;
  }
  public void setPrice(Integer price) {
    this.price = price;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public Integer getInventory() {
    return inventory;
  }
  public void setInventory(Integer inventory) {
    this.inventory = inventory;
  }
  public String getCompanyName() {
    return companyName;
  }
  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }
  public String getSaleUnit() {
    return saleUnit;
  }
  public void setSaleUnit(String saleUnit) {
    this.saleUnit = saleUnit;
  }
  public String getSaleStyle() {
    return saleStyle;
  }
  public void setSaleStyle(String saleStyle) {
    this.saleStyle = saleStyle;
  }

 
 
}
