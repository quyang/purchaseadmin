package com.xianzaishi.purchaseadmin.client.order.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 子采购单集
 * @author dongpo
 *
 */
public class OrderCollectionVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 5412013879709830262L;
  
  /**
   * 订单列表
   */
  private List<OrderListVO> orderListVOs;
  
  public List<OrderListVO> getOrderListVOs() {
    return orderListVOs;
  }

  public void setOrderListVOs(List<OrderListVO> orderListVOs) {
    this.orderListVOs = orderListVOs;
  }
}
