package com.xianzaishi.purchaseadmin.client.supplier.vo;

public class SupplierInfoVO {
  
  /**
   * 供应商id
   */
  private Integer userId;
  
  /**
   * 供应商类型
   */
  private Short userType;
  
  /**
   * 供应商类型
   */
  private String userTypeString;
  
  /**
   * 供应商名称
   */
  private String name;
  
  /**
   * 联系人
   */
  private String contract;
  
  /**
   * 联系人手机号码
   */
  private Long phone;

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContract() {
    return contract;
  }

  public void setContract(String contract) {
    this.contract = contract;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public String getUserTypeString() {
    return userTypeString;
  }

  public void setUserTypeString(String userTypeString) {
    this.userTypeString = userTypeString;
  }
  

}
