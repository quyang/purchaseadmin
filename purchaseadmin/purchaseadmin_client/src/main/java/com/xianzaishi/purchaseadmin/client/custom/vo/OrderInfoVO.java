package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * 客服订单信息
 * @author dongpo
 *
 */
public class OrderInfoVO {
  /**
   * 商品sku id
   */
  private Long skuId;
  
  /**
   * 商品标题
   */
  private String title;
  
  /**
   * 商品总价格
   */
  private String totalPrice;
  
  /**
   * 商品购买数量
   */
  private Double count;
  
  /**
   * 数量单位
   */
  private String unit;

  /**
   * 订单里的优惠券id
   */
  private Long couponId;
  private Long orderId;


  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }


  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Long getOrderId() {
    return orderId;
  }
}
