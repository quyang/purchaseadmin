package com.xianzaishi.purchaseadmin.client.sale.vo;

public class SaleListQueryVO {
  
  /**
   * 商品关键字
   */
  String itemKeyWord;
  
  /**
   * sku id
   */
  Long skuId;
  
  /**
   * 69码
   */
  Long sku69Code;
  
  /**
   * sku 名称
   */
  String skuName;
  
  /**
   * 页面大小
   */
  Integer pageSize;
  
  /**
   * 当前页面数
   */
  Integer pageNum;

  public String getItemKeyWord() {
    return itemKeyWord;
  }

  public void setItemKeyWord(String itemKeyWord) {
    this.itemKeyWord = itemKeyWord;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Long getSku69Code() {
    return sku69Code;
  }

  public void setSku69Code(Long sku69Code) {
    this.sku69Code = sku69Code;
  }

  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }
  
}
