package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * 客服任务查询条件
 * @author dongpo
 *
 */
public class CustomTaskQueryVO {


  /**
   * 入库id
   */
  private Long storageId;

  /**
   * 任务id
   */
  private Long taskId;
  
  /**
   * 任务类型
   */
  private Short type;
  
  /**
   * 任务开始时间
   */
  private String start;
  
  /**
   * 任务结束时间
   */
  private String end;
  
  /**
   * 任务状态
   */
  private Short status;
  
  /**
   * 页面大小
   */
  private Integer pageSize;
  
  /**
   * 当前页码
   */
  private Integer pageNum;

  /**
   * 用户id
   */
  private Long userId;


  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getStorageId() {
    return storageId;
  }

  public void setStorageId(Long storageId) {
    this.storageId = storageId;
  }

  public Long getTaskId() {
    return taskId;
  }

  public void setTaskId(Long taskId) {
    this.taskId = taskId;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }
  
  

}
