package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.Date;
import java.util.List;

/**
 * Created by quyang on 2017/3/7.
 */
public class OrderReturnVO {

  /**
   * 单位元 账单金额
   */
  private String payAmount;
  /**
   * 商品列表
   */
  private List<OrderItemInfoVO> itemsList;
  /**
   * 订单类型
   */
  private Short channelType;
  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 配送单上的手机号码  1111
   */
  private String distributionPhone;
  /**
   * 配送地址 111
   */
  private String distributinaAddress;
  /**
   * 订单描述
   */
  private String attribute;
  /**
   * 订单状态 0 待处理 1 处理中 2 加工完成 -1 取消  111
   */
  private Integer tagStatus;
  /**
   * 折扣金额
   */
  private String discountAmount;
  /**
   * 订单id
   */
  private Long id;
  /**
   * 单位元 实际金额
   */
  private String effeAmount;
  /**
   * 配送时间
   */
  private Date gmtDistribution;
  /**
   * 会员号码 111
   */
  private Long phoneNumber;
  /**
   * 积分折扣 111
   */
  private String creditDiscount;
  /**
   * 流水号
   */
  private Integer seq;

  public String getPayAmount() {
    if (null == payAmount) {
      payAmount = "0.00";
    }
    return payAmount;
  }

  public void setPayAmount(String payAmount) {
    this.payAmount = payAmount;
  }

  public List<OrderItemInfoVO> getItemsList() {
    return itemsList;
  }

  public void setItemsList(
      List<OrderItemInfoVO> itemsList) {
    this.itemsList = itemsList;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public String getDistributionPhone() {
    return distributionPhone;
  }

  public void setDistributionPhone(String distributionPhone) {
    this.distributionPhone = distributionPhone;
  }

  public String getDistributinaAddress() {
    if (null == distributinaAddress) {
      distributinaAddress = "无";
    }
    return distributinaAddress;
  }

  public void setDistributinaAddress(String distributinaAddress) {
    this.distributinaAddress = distributinaAddress;
  }

  public String getAttribute() {
    if (null == attribute) {
      attribute = "无";
    }
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }

  public Integer getTagStatus() {
    return tagStatus;
  }

  public void setTagStatus(Integer tagStatus) {
    this.tagStatus = tagStatus;
  }

  public String getDiscountAmount() {
    if (null == discountAmount) {
      discountAmount = "0.00";
    }
    return discountAmount;
  }

  public void setDiscountAmount(String discountAmount) {
    this.discountAmount = discountAmount;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEffeAmount() {
    if (null == effeAmount) {
      effeAmount = "0.00";
    }
    return effeAmount;
  }

  public void setEffeAmount(String effeAmount) {
    this.effeAmount = effeAmount;
  }

  public Date getGmtDistribution() {
    return gmtDistribution;
  }

  public void setGmtDistribution(Date gmtDistribution) {
    this.gmtDistribution = gmtDistribution;
  }

  public Long getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(Long phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getCreditDiscount() {
    if (null == creditDiscount) {
      creditDiscount = "0.00";
    }
    return creditDiscount;
  }

  public void setCreditDiscount(String creditDiscount) {
    this.creditDiscount = creditDiscount;
  }

  public Integer getSeq() {
    return seq;
  }

  public void setSeq(Integer seq) {
    this.seq = seq;
  }
}
