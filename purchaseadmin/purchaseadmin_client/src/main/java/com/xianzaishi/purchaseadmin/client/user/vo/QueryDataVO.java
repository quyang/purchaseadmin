package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * Created by Administrator on 2017/3/15.
 */
public class QueryDataVO {

  private Long id;
  /**
   * 手机号
   */
  private Long phone;

  /**
   * 验证码
   */
  private String checkCode;

  /**
   * 退款金额
   */
  private String sum;

  /**
   * 退款原因
   */
  private String refoudReason;

  /**
   * 赠送的优惠券id
   */
  private Long couponId;

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public String getRefoudReason() {
    return refoudReason;
  }

  public void setRefoudReason(String refoudReason) {
    this.refoudReason = refoudReason;
  }

  public String getSum() {
    return sum;
  }

  public void setSum(String sum) {
    this.sum = sum;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCheckCode() {
    return checkCode;
  }

  public void setCheckCode(String checkCode) {
    this.checkCode = checkCode;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }
}
