package com.xianzaishi.purchaseadmin.client.custom.vo;

import java.util.List;

import com.xianzaishi.customercenter.client.customerservice.dto.ProofDataDTO;

/**
 * 客服待处理任务信息
 * @author dongpo
 *
 */
public class TaskInfoVO {

  /**
   * 退款任务id
   */
  private Long taskId;
  
  /**
   * 任务类型
   */
  private String type;
  
  /**
   * 发生时间
   */
  private String gmtCreate;
  
  /**
   * 对应订单id
   */
  private Long orderId;
  
  /**
   * 任务描述
   */
  private List<ProofDataDTO> taskDescription;

  /**
   * 退款金额
   */
  private String sum;

  /**
   * 退款原因
   */
  private String refoudReason;


  /**
   * 要赠送的优惠券id
   */
  private Long couponId;

  /**
   * 优惠券名称
   */
  private String couponTitle;

  /**
   * 是否是审核状态退款任务
   */
  private Boolean audited;

  /**
   * 退款任务是否完成
   */
  private Boolean taskCompleted;

  /**
   * 退款任务状态
   */
  private Short status;

  public Long getTaskId() {
    return taskId;
  }

  public void setTaskId(Long taskId) {
    this.taskId = taskId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getCouponTitle() {
    if (null == couponTitle) {
      couponTitle = "无";
    }
    return couponTitle;
  }

  public void setCouponTitle(String couponTitle) {
    this.couponTitle = couponTitle;
  }

  public Long getCouponId() {
    if (null == couponId) {
      couponId = 0L;
    }
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Boolean getTaskCompleted() {
    return taskCompleted;
  }

  public void setTaskCompleted(Boolean taskCompleted) {
    this.taskCompleted = taskCompleted;
  }

  public Boolean getAudited() {
    return audited;
  }

  public void setAudited(Boolean audited) {
    this.audited = audited;
  }

  public String getSum() {
    if (sum == null ) {
      sum = "0.00";
    }
    return sum;
  }

  public void setSum(String sum) {
    this.sum = sum;
  }

  public String getRefoudReason() {
    if (null == refoudReason) {
      refoudReason = "";
    }
    return refoudReason;
  }

  public void setRefoudReason(String refoudReason) {
    this.refoudReason = refoudReason;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(String gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public List<ProofDataDTO> getTaskDescription() {
    return taskDescription;
  }

  public void setTaskDescription(List<ProofDataDTO> taskDescription) {
    this.taskDescription = taskDescription;
  }
  
}
