package com.xianzaishi.purchaseadmin.client.inventory.vo;

import java.util.List;

public class OrderSkuInfo {

  private Long skuId;

  /**
   * 换货skuId集合
   */
  private List<Long> exSkuId;

  /**
   * 退货数量
   */
  private String refundCount;

  /**
   * 重新送货数量
   */
  private String newTradeCount;

  /**
   * 退货后可以入库数量
   */
  private String inventoryCount;

  /**
   * 退货后报损数量
   */
  private String inventoryLostCount;

  /**
   * 商品处理类型
   */
  private Short refundOrderSkuType;

  public List<Long> getExSkuId() {
    return exSkuId;
  }

  public void setExSkuId(List<Long> exSkuId) {
    this.exSkuId = exSkuId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getRefundCount() {
    return refundCount == null ? "0" : refundCount;
  }

  public void setRefundCount(String refundCount) {
    this.refundCount = refundCount;
  }

  public String getNewTradeCount() {
    return newTradeCount == null ? "0" : newTradeCount;
  }

  public void setNewTradeCount(String newTradeCount) {
    this.newTradeCount = newTradeCount;
  }

  public String getInventoryCount() {
    return inventoryCount == null ? "0" : inventoryCount;
  }

  public void setInventoryCount(String inventoryCount) {
    this.inventoryCount = inventoryCount;
  }

  public String getInventoryLostCount() {
    return inventoryLostCount == null ? "0" : inventoryLostCount;
  }

  public void setInventoryLostCount(String inventoryLostCount) {
    this.inventoryLostCount = inventoryLostCount;
  }

  public Short getRefundOrderSkuType() {
    return refundOrderSkuType;
  }

  public void setRefundOrderSkuType(Short refundOrderSkuType) {
    this.refundOrderSkuType = refundOrderSkuType;
  }

  public static class RefundOrderSkuType {

    /**
     * sku退货
     */
    public static final Short REFUND = 1;

    /**
     * sku送货
     */
    public static final Short EXCHANGE = 2;


    public static Boolean hasType(Short type) {
      if (REFUND.equals(type) || EXCHANGE.equals(type)) {
        return true;
      }
      return false;
    }
  }
}
