package com.xianzaishi.purchaseadmin.client.sale.vo;

import java.io.Serializable;

public class SaleListVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 3862587269206083308L;

  /**
   * sku id
   */
  private Long skuId;
  
  /**
   * 商品货号
   */
  private Long skuCode;
  
  /**
   * 商品69码
   */
  private Long sku69code;
  
  /**
   * sku名称
   */
  private String title;
  
  /**
   * 商品id
   */
  private Long itemId;
  
  /**
   * 商品规格
   */
  private String specification;
  
  /**
   * sku状态
   */
  private String status;
  
  /**
   * sku状态码
   */
  private Short statusCode;
  
  /**
   * 销售状态，默认为线上线下同步销售
   */
  private String saleStatus;
  
  /**
   * 销售状态码
   */
  private Short saleStatusCode = SaleStatusConstants.SALE_ONLINE_AND_MARKET;
  
  /**
   * 供应商id
   */
  private Integer supplierId;
  
  /**
   * 供应商名称
   */
  private String supplierName;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }
  
  public Long getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(Long skuCode) {
    this.skuCode = skuCode;
  }

  public Long getSku69code() {
    return sku69code;
  }

  public void setSku69code(Long sku69code) {
    this.sku69code = sku69code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
  
  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public String getSpecification() {
    return specification;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Short getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Short statusCode) {
    this.statusCode = statusCode;
  }

  public String getSaleStatus() {
    return saleStatus;
  }

  public void setSaleStatus(String saleStatus) {
    this.saleStatus = saleStatus;
  }

  public Short getSaleStatusCode() {
    return saleStatusCode;
  }

  public void setSaleStatusCode(Short saleStatusCode) {
    this.saleStatusCode = saleStatusCode;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }


  /**
   * 销售状态
   * @author dongpo
   *
   */
  public static class SaleStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189024053122453L;

    /**
     * 线上线下同步销售
     */
    public static final Short SALE_ONLINE_AND_MARKET = 1;

    /**
     * 线上销售
     */
    public static final Short SALE_ONLY_ONLINE = 2;

    /**
     * 线下销售
     */
    public static final Short SALE_ONLY_MARKET = 3;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == SALE_ONLINE_AND_MARKET || parameter == SALE_ONLY_ONLINE
          || parameter == SALE_ONLY_MARKET;
    }
  }

}
