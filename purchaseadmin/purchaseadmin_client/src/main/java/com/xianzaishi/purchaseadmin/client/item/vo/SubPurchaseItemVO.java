package com.xianzaishi.purchaseadmin.client.item.vo;

public class SubPurchaseItemVO {
  
  /**
   * 子采购单id
   */
  private Integer subPurchaseId;
  
  /**
   * 商品编号
   */
  private Long skuId;
  
  /**
   * 类目名称
   */
  private String catName;
  
  /**
   * 商品名称
   */
  private String title;
  
  /**
   * 商品规格
   */
  private String specification;
  
  /**
   * 商品库存
   */
  private Integer inventory;
  
  /**
   * 销售规格
   */
  private String saleUnit;
  
  /**
   * 采购规格
   */
  private String purchaseUnit;
  
  /**
   * 单价
   */
  private String discountPrice;
  
  /**
   * 仓库
   */
  private String wareHouse;
  
  /**
   * 采购数量
   */
  private Integer count;
  
  /**
   * 子采购单备注
   */
  private String checkInfo;
  
  /**
   * 供应商id
   */
  private Integer supplierId;
  
  /**
   * 供应商名称
   */
  private String supplierName;
  
  /**
   * 子采购单状态
   */
  private Short status;
  
  /**
   * 近一周线上销量
   */
  private String saleCountOnLine;
  
  /**
   * 近一周线下销量
   */
  private String saleCountOffLine;
  
  /**
   * 子采购单总价
   */
  private String subPurchaseTotalAmount;
  
  /**
   * 实际入库数量
   */
  private String actualCount; 
  
  /**
   * 子采购单实际总价（入库后）
   */
  private String subPurchaseActualAmount;
  
  /**
   * 商品69码
   */
  private Long sku69Code;
  
  public Integer getSubPurchaseId() {
    return subPurchaseId;
  }

  public void setSubPurchaseId(Integer subPurchaseId) {
    this.subPurchaseId = subPurchaseId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSpecification() {
    return specification;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  public Integer getInventory() {
    return inventory;
  }

  public void setInventory(Integer inventory) {
    this.inventory = inventory;
  }

  public String getSaleUnit() {
    return saleUnit;
  }

  public void setSaleUnit(String saleUnit) {
    this.saleUnit = saleUnit;
  }

  public String getPurchaseUnit() {
    return purchaseUnit;
  }

  public void setPurchaseUnit(String purchaseUnit) {
    this.purchaseUnit = purchaseUnit;
  }

  public String getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(String discountPrice) {
    this.discountPrice = discountPrice;
  }

  public String getWareHouse() {
    return wareHouse;
  }

  public void setWareHouse(String wareHouse) {
    this.wareHouse = wareHouse;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public String getCheckInfo() {
    return checkInfo;
  }

  public void setCheckInfo(String checkInfo) {
    this.checkInfo = checkInfo;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getSaleCountOnLine() {
    return saleCountOnLine;
  }

  public void setSaleCountOnLine(String saleCountOnLine) {
    this.saleCountOnLine = saleCountOnLine;
  }

  public String getSaleCountOffLine() {
    return saleCountOffLine;
  }

  public void setSaleCountOffLine(String saleCountOffLine) {
    this.saleCountOffLine = saleCountOffLine;
  }

  public String getSubPurchaseTotalAmount() {
    return subPurchaseTotalAmount;
  }

  public void setSubPurchaseTotalAmount(String subPurchaseTotalAmount) {
    this.subPurchaseTotalAmount = subPurchaseTotalAmount;
  }

  public String getActualCount() {
    return actualCount;
  }

  public void setActualCount(String actualCount) {
    this.actualCount = actualCount;
  }

  public String getSubPurchaseActualAmount() {
    return subPurchaseActualAmount;
  }

  public void setSubPurchaseActualAmount(String subPurchaseActualAmount) {
    this.subPurchaseActualAmount = subPurchaseActualAmount;
  }

  public Long getSku69Code() {
    return sku69Code;
  }

  public void setSku69Code(Long sku69Code) {
    this.sku69Code = sku69Code;
  }
  
}
