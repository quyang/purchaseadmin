package com.xianzaishi.purchaseadmin.client.item.vo;

import java.util.List;

import com.xianzaishi.purchaseadmin.client.promotion.vo.SkuDiscountVO;

public class PromotionItemVO extends CommodityItemVO{

  private String catName;
  
  /**
   * 优惠设置价格
   */
  private String promotionPriceStr;
  
  /**
   * 优惠前价格
   */
  private String priceStr;
  
  /**
   * 库存
   */
  private String stocNum;
  
  /**
   * 安全库存
   */
  private String safeStocNum;
  
  /**
   * 有效库位
   */
  private List<String> availableStocPosition;
  
  /**
   * 推荐库位
   */
  private List<String> recommondStocPosition;
  
  /**
   * 临期历史记录
   */
  private List<SkuDiscountVO> discountItemHistory;

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public String getStocNum() {
    return stocNum;
  }

  public void setStocNum(String stocNum) {
    this.stocNum = stocNum;
  }

  public String getSafeStocNum() {
    return safeStocNum;
  }

  public void setSafeStocNum(String safeStocNum) {
    this.safeStocNum = safeStocNum;
  }

  public List<String> getAvailableStocPosition() {
    return availableStocPosition;
  }

  public void setAvailableStocPosition(List<String> availableStocPosition) {
    this.availableStocPosition = availableStocPosition;
  }

  public List<String> getRecommondStocPosition() {
    return recommondStocPosition;
  }

  public void setRecommondStocPosition(List<String> recommondStocPosition) {
    this.recommondStocPosition = recommondStocPosition;
  }

  public List<SkuDiscountVO> getDiscountItemHistory() {
    return discountItemHistory;
  }

  public void setDiscountItemHistory(List<SkuDiscountVO> discountItemHistory) {
    this.discountItemHistory = discountItemHistory;
  }

  public String getPromotionPriceStr() {
    return promotionPriceStr;
  }

  public void setPromotionPriceStr(String promotionPriceStr) {
    this.promotionPriceStr = promotionPriceStr;
  }

  public String getPriceStr() {
    return priceStr;
  }

  public void setPriceStr(String priceStr) {
    this.priceStr = priceStr;
  }
}
