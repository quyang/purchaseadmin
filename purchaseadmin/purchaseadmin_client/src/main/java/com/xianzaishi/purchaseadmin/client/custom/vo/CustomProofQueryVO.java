package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * 客户申请售后证据
 * @author dongpo
 *
 */
public class CustomProofQueryVO {
  
  /**
   * 证据id
   */
  private Integer id;
  
  /**
   * 图片或字符串
   */
  private String source;
  
  /**
   * 证据类型
   */
  private Integer type;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
  
}
