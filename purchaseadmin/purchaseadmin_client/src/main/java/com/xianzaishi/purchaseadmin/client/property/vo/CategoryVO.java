package com.xianzaishi.purchaseadmin.client.property.vo;

import java.util.List;

/**
 * 前台使用类目属性对象
 * 
 * @author zhancang
 */
public class CategoryVO {

  /**
   * 类目id
   */
  private int catId;

  /**
   * 类目名称
   */
  private String catName;

  /**
   * 子类目列表
   */
  private List<CategoryVO> subCatList;

  /**
   * 当前类目下属性列表
   */
  private List<PropertyVO> propertyVO;

  public int getCatId() {
    return catId;
  }

  public void setCatId(int catId) {
    this.catId = catId;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public List<CategoryVO> getSubCatList() {
    return subCatList;
  }

  public void setSubCatList(List<CategoryVO> subCatList) {
    this.subCatList = subCatList;
  }

  public List<PropertyVO> getPropertyVO() {
    return propertyVO;
  }

  public void setPropertyVO(List<PropertyVO> propertyVO) {
    this.propertyVO = propertyVO;
  }
}
