package com.xianzaishi.purchaseadmin.client.item.vo;

/**
 * Created by quyang on 2017/4/16.
 */
public class ItemCommoditySkuVO {


  /**
   * 后台skuID，系统使用，唯一标识一个sku
   */
  private Long skuId;

  /**
   * 对应后台采购\销售商品id
   */
  private Long itemId;

  private Long skuPluCode;

  /**
   * 69码
   */
  private Long sku69code;

  /**
   * 商品名称
   */
  private String title;

  /**
   * 销售单位的中文描述
   */
  private String saleUnitStr;




}
