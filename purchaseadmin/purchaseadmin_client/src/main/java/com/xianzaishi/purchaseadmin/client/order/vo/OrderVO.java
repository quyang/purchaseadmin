package com.xianzaishi.purchaseadmin.client.order.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 总采购单数据
 * @author dongpo
 *
 */
public class OrderVO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 6140057548355245106L;
  
  /**
   * 采购单id
   */
  private Integer purchaseId;
  
  /**
   * 供应商编号
   */
  private Integer userId;
  
  /**
   * 采购订单列表
   */
  private List<SubOrderVO> items;
  
  /**
   * 采购人id
   */
  private Integer purchasingAgent;
  
  /**
   * 采购日期
   */
  private Long purchaseDate;
  
  /**
   * 到货日期
   */
  private Long expectArriveDate;
  
  /**
   * 备注
   */
  private String remarks;
  
  /**
   * 审核状态
   */
  private Short auditingStatus;

  
  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public List<SubOrderVO> getItems() {
    return items;
  }

  public void setItems(List<SubOrderVO> items) {
    this.items = items;
  }

  public Long getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(Long purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public Long getExpectArriveDate() {
    return expectArriveDate;
  }

  public void setExpectArriveDate(Long expectArriveDate) {
    this.expectArriveDate = expectArriveDate;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    this.purchasingAgent = purchasingAgent;
  }

  public Short getAuditingStatus() {
    return auditingStatus;
  }

  public void setAuditingStatus(Short auditingStatus) {
    this.auditingStatus = auditingStatus;
  }

  public Integer getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }
  
}
