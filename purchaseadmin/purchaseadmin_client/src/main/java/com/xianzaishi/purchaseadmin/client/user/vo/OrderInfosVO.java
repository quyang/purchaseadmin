package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.List;

/**
 * Created by quyang on 2017/3/7.
 */
public class OrderInfosVO {


  private List<OrderReturnVO> orders;

  public List<OrderReturnVO> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderReturnVO> orders) {
    this.orders = orders;
  }
}
