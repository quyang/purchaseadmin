package com.xianzaishi.purchaseadmin.client.sale.vo;

/**
 * Created by quyang on 2017/4/5.
 */
public class SaleVO {

  private Long skuId;

  private Long qrId;

  private Short qrType;

  /**
   * 销售渠道
   */
  private Short channel;

  public Short getChannel() {
    return channel;
  }

  public void setChannel(Short channel) {
    this.channel = channel;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Long getQrId() {
    return qrId;
  }

  public void setQrId(Long qrId) {
    this.qrId = qrId;
  }

  public Short getQrType() {
    return qrType;
  }

  public void setQrType(Short qrType) {
    this.qrType = qrType;
  }
}
