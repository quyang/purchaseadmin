package com.xianzaishi.purchaseadmin.client.activity.vo;

import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;

/**
 * 列表页面vo
 * @author zhancang
 */
public class ActivityPageVO extends ActivityPageDTO{

  private static final long serialVersionUID = 1L;
  
  private String stepTitle;
  
  private int stepId;

  public int getStepId() {
    return stepId;
  }

  public void setStepId(int stepId) {
    this.stepId = stepId;
  }

  public String getStepTitle() {
    return stepTitle;
  }

  public void setStepTitle(String stepTitle) {
    this.stepTitle = stepTitle;
  }
  
}
