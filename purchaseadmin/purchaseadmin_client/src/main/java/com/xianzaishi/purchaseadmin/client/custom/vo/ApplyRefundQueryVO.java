package com.xianzaishi.purchaseadmin.client.custom.vo;

import java.util.List;

public class ApplyRefundQueryVO {
  
  private String bizId;
  
  private List<CustomProofQueryVO> customerProof;

  public String getBizId() {
    return bizId;
  }

  public void setBizId(String bizId) {
    this.bizId = bizId;
  }

  public List<CustomProofQueryVO> getCustomerProof() {
    return customerProof;
  }

  public void setCustomerProof(List<CustomProofQueryVO> customerProof) {
    this.customerProof = customerProof;
  }

}
