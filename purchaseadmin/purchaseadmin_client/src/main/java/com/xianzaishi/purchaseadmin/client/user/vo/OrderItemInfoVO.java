package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * Created by quyang on 2017/2/20.
 */
public class OrderItemInfoVO {


  /**
   * 商品ID
   */
  private Long id;

  /**
   * 商品名称
   */
  private String name;

  /**
   * 商品图标URL
   */
  private String iconUrl;

  /**
   * 商品价格
   */
  private Double price;

  /**
   * 商品实际价格
   */
  private Double effePrice;

  /**
   * 商品总值
   */
  private Double amount;

  /**
   * 商品实际总值
   */
  private Double effeAmount;

  /**
   * 购买数量
   */
  private Double count;

  /**
   * 商品SKU信息
   */
  private SkuInformationVO skuInfos;

  /**
   * 类目ID
   */
  private Integer categoryId;

  /**
   * SKU ID
   */
  private Long skuId;


  /**
   * 其他
   */
  private String other;

  /**
   * sku 40000 到50000 为0 其他为1
   */
  private int tag;

  /**
   * 已退数量
   */
  private String alreadyRefound = "0";

  /**
   * 处理结果
   */
  private String dealResult = "未退货";


  public String getDealResult() {
    return dealResult;
  }

  public void setDealResult(String dealResult) {
    this.dealResult = dealResult;
  }

  public String getAlreadyRefound() {
    return alreadyRefound;
  }

  public void setAlreadyRefound(String alreadyRefound) {
    this.alreadyRefound = alreadyRefound;
  }

  public int getTag() {
    return tag;
  }

  public void setTag(int tag) {
    this.tag = tag;
  }

  public String getOther() {
    return other;
  }

  public void setOther(String other) {
    this.other = other;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIconUrl() {
    return iconUrl;
  }

  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getEffePrice() {
    return effePrice;
  }

  public void setEffePrice(Double effePrice) {
    this.effePrice = effePrice;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getEffeAmount() {
    return effeAmount;
  }

  public void setEffeAmount(Double effeAmount) {
    this.effeAmount = effeAmount;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public SkuInformationVO getSkuInfos() {
    return skuInfos;
  }

  public void setSkuInfos(SkuInformationVO skuInfos) {
    this.skuInfos = skuInfos;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }
}
