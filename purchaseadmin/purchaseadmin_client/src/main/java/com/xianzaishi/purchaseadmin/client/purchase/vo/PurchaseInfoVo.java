package com.xianzaishi.purchaseadmin.client.purchase.vo;

import java.io.Serializable;

public class PurchaseInfoVo implements Serializable{
	
	  private String skuid;
      private int inventory;//当前总库存：
      private int safestock;//安全库存
      private long sku_69code;//商品69
      private int specification;//规格属性
      private String stock;//库位
      private String  setKucode;//库位条码
      private  int  ruKuCount;//入库数量
      private int purchaseCount;//采购数量
      private String proName;//商品名称
	
	public String getSkuid() {
		return skuid;
	}
	public void setSkuid(String skuid) {
		this.skuid = skuid;
	}
	public int getInventory() {
		return inventory;
	}
	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	public int getSafestock() {
		return safestock;
	}
	public void setSafestock(int safestock) {
		this.safestock = safestock;
	}
	public long getSku_69code() {
		return sku_69code;
	}
	public void setSku_69code(long sku_69code) {
		this.sku_69code = sku_69code;
	}
	public int getSpecification() {
		return specification;
	}
	public void setSpecification(int specification) {
		this.specification = specification;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getSetKucode() {
		return setKucode;
	}
	public void setSetKucode(String setKucode) {
		this.setKucode = setKucode;
	}
	public int getRuKuCount() {
		return ruKuCount;
	}
	public void setRuKuCount(int ruKuCount) {
		this.ruKuCount = ruKuCount;
	}
	public int getPurchaseCount() {
		return purchaseCount;
	}
	public void setPurchaseCount(int purchaseCount) {
		this.purchaseCount = purchaseCount;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}

	



}
