package com.xianzaishi.purchaseadmin.client.custom.vo;

public class CustomCouponVO {
  /**
   * 优惠券id
   */
  private Long id;

  /**
   * 优惠券标题
   */
  private String title;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

}
