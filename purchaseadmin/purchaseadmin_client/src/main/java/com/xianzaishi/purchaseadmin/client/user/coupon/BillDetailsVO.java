package com.xianzaishi.purchaseadmin.client.user.coupon;

/**
 * Created by Administrator on 2017/2/17.
 */
public class BillDetailsVO {


  /**
   * 订单id
   */
  private Long orderId ;

  /**
   * 用户id
   */
  private Integer userId ;

  /**
   * 订单金额
   */
  private String money ;

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getMoney() {
    return money;
  }

  public void setMoney(String money) {
    this.money = money;
  }
}
