package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * Created by Administrator on 2017/3/15.
 */
public class ResultVO {

  /**
   * 用户token
   */
  private String token;
  /**
   * 数据data
   */
  private ResultData data;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public ResultData getData() {
    return data;
  }

  public void setData(ResultData data) {
    this.data = data;
  }
}
