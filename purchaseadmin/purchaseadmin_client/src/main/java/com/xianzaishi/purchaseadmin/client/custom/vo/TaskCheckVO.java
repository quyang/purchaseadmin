package com.xianzaishi.purchaseadmin.client.custom.vo;

import java.util.List;

/**
 * Created by quyang on 2017/3/14.
 */
public class TaskCheckVO {
//Long taskId,String orderId, Long skuId, Integer count
  /**
   * 退款任务id
   */
  private Long taskId;

  /**
   * 订单id
   */
  private String orderId;

  /**
   * 商品skuid
   */
  private Long skuId;

  /**
   * 商品数量
   */
  private Integer count;


  /**
   * 退款商品集合
   */
  private List<ReturnItemInfo> itemList;


  public List<ReturnItemInfo> getItemList() {
    return itemList;
  }

  public void setItemList(List<ReturnItemInfo> itemList) {
    this.itemList = itemList;
  }

  public Long getTaskId() {
    return taskId;
  }

  public void setTaskId(Long taskId) {
    this.taskId = taskId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
}
