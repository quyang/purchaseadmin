package com.xianzaishi.purchaseadmin.client.user.vo;


import java.util.List;

/**
 *
 * @author quyang
 */
public class UserInfoVO {

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 用户名称
   */
  private String userName;

  /**
   * 用户手机号码
   */
  private Long phoneNum;

  /**
   * 用户积分
   */
  private Integer credit;

  /**
   * 用户类型
   */
  private Short level;

  /**
   * 常用地址列表，和app上的设置模块的地址列表一直
   */
  private List<String> addressList;
  
  /**
   * 用户验证码
   */
  private Integer activityCode;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Long getPhoneNum() {
    return phoneNum;
  }

  public void setPhoneNum(Long phoneNum) {
    this.phoneNum = phoneNum;
  }

  public Integer getCredit() {
    return credit;
  }

  public void setCredit(Integer credit) {
    this.credit = credit;
  }

  public Short getLevel() {
    return level;
  }

  public void setLevel(Short level) {
    this.level = level;
  }

  public List<String> getAddressList() {
    return addressList;
  }

  public void setAddressList(List<String> addressList) {
    this.addressList = addressList;
  }

  public Integer getActivityCode() {
    return activityCode;
  }

  public void setActivityCode(Integer activityCode) {
    this.activityCode = activityCode;
  }
  
}
