package com.xianzaishi.purchaseadmin.client.base.vo;

import java.util.List;

public class CollectionVO<T>{
  
  /**
   * 总页数
   */
  private Integer totalPage;
  
  /**
   * 订单列表
   */
  private List<T> list;

  /**
   * 当前页
   */
  private Integer currentPage;

  public Integer getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(Integer totalPage) {
    this.totalPage = totalPage;
  }

  public List<T> getList() {
    return list;
  }

  public void setList(List<T> list) {
    this.list = list;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }
}
