package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.util.List;

/**
 * 限购活动
 * Created by Administrator on 2017/4/26.
 */
public class LimitBuyActivityVO {

  /**
   * 限时折扣
   */
  private String limitedTimeDiscount;

  /**
   * 限购数
   */
  private String limitCount;


  /**
   * 优惠券Id集合
   */
  private List<Long> couponIds ;

  /**
   * 商品skuId集合
   */
  private List<Long> skuIds ;

  public String getLimitedTimeDiscount() {
    return limitedTimeDiscount;
  }

  public void setLimitedTimeDiscount(String limitedTimeDiscount) {
    this.limitedTimeDiscount = limitedTimeDiscount;
  }

  public String getLimitCount() {
    return limitCount;
  }

  public void setLimitCount(String limitCount) {
    this.limitCount = limitCount;
  }

  public List<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(List<Long> couponIds) {
    this.couponIds = couponIds;
  }

  public List<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<Long> skuIds) {
    this.skuIds = skuIds;
  }
}
