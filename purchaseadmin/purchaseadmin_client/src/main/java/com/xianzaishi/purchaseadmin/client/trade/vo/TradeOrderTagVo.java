package com.xianzaishi.purchaseadmin.client.trade.vo;

/**
 * Created by quyang on 2017/5/8.
 */
public class TradeOrderTagVo {


  /**
   * 订单id
   */
  private Integer orderId;

  /**
   * 是否是达标，false表示去标
   */
  private Boolean add ;

  /**
   * 要打的标
   */
  private Integer tag;

  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    this.orderId = orderId;
  }

  public Boolean getAdd() {
    return add;
  }

  public void setAdd(Boolean add) {
    this.add = add;
  }

  public Integer getTag() {
    return tag;
  }

  public void setTag(Integer tag) {
    this.tag = tag;
  }
}
