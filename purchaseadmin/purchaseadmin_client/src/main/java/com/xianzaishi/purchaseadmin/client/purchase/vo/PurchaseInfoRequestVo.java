package com.xianzaishi.purchaseadmin.client.purchase.vo;

import com.xianzaishi.purchaseadmin.client.user.vo.BaseRequestVo;

public class PurchaseInfoRequestVo extends BaseRequestVo{
	
	private PurchaseInfoVo data;

	public PurchaseInfoVo getData() {
		return data;
	}

	public void setData(PurchaseInfoVo data) {
		this.data = data;
	}
	
	
	

}
