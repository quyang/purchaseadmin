package com.xianzaishi.purchaseadmin.client.monitor.vo;

/**
 * Created by quyang on 2017/3/23.
 */
public class MonitorInformationVO {



  /**
   * 监控对象名称
   */
  private String name;

  /**
   * 日志信息
   */
  private String info;


  /**
   * 监控事件类型
   */
  private Short type;

  /**
   * 目标手机号
   */
  private String phone;

  /**
   * 时间间隔
   */
  private Integer timeScope;

  /**
   * 监控数据是否只更新
   */
  private Boolean isOnlyUpdate;

  public Boolean getOnlyUpdate() {
    return isOnlyUpdate;
  }

  public void setOnlyUpdate(Boolean onlyUpdate) {
    isOnlyUpdate = onlyUpdate;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getTimeScope() {
    return timeScope;
  }

  public void setTimeScope(Integer timeScope) {
    this.timeScope = timeScope;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }
}
