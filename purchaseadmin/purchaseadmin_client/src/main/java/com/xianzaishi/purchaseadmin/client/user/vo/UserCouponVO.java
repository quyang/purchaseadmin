package com.xianzaishi.purchaseadmin.client.user.vo;

import java.util.Date;

/**
 * Created by quyang on 2017/2/14.
 */
public class UserCouponVO {


  private Long id;
  /**
   * 用户id
   */
  private Long userId;
  /**
   * 优惠券id
   */
  private Long couponId;
  /**
   * 优惠券名称
   */
  private String couponTitle;
  /**
   * 发放时间
   */
  private Date couponStartTime;
  /**
   * 结束时间
   */
  private Date couponEndTime;
  /**
   * 订单id
   */
  private Long orderId;
  private String couponRules;
  private String sendRules;
  private Date gmtCreate;
  private Date gmtModify;
  private Short status;
  private Double amount;
  private Integer amountCent;
  private String amountYuanDescription;
  private Short couponType;
  /**
   * 渠道名称
   */
  private Short channelType;
  private Double amountLimit;
  private Integer amountLimitCent;
  private String amountLimitYuanDescription;

  /**
   * 优惠金额
   */
  private String discountAmount;


  /**
   * 当前状态
   */
  private String currentStatus;

  /**
   * 已使用优惠
   */
  private String usedYouHui;

  /**
   * 当前页
   */
  private Integer currentPage;
  /**
   * 总页
   */
  private Integer totalPage;


  /**
   * 渠道
   */
  private String channel;

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Integer getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(Integer totalPage) {
    this.totalPage = totalPage;
  }

  public String getUsedYouHui() {
    return usedYouHui;
  }

  public void setUsedYouHui(String usedYouHui) {
    this.usedYouHui = usedYouHui;
  }

  public String getCurrentStatus() {
    return currentStatus;
  }

  public void setCurrentStatus(String currentStatus) {
    this.currentStatus = currentStatus;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public String getCouponTitle() {
    return couponTitle;
  }

  public void setCouponTitle(String couponTitle) {
    this.couponTitle = couponTitle;
  }

  public Date getCouponStartTime() {
    return couponStartTime;
  }

  public void setCouponStartTime(Date couponStartTime) {
    this.couponStartTime = couponStartTime;
  }

  public Date getCouponEndTime() {
    return couponEndTime;
  }

  public void setCouponEndTime(Date couponEndTime) {
    this.couponEndTime = couponEndTime;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getCouponRules() {
    return couponRules;
  }

  public void setCouponRules(String couponRules) {
    this.couponRules = couponRules;
  }

  public String getSendRules() {
    return sendRules;
  }

  public void setSendRules(String sendRules) {
    this.sendRules = sendRules;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModify() {
    return gmtModify;
  }

  public void setGmtModify(Date gmtModify) {
    this.gmtModify = gmtModify;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Integer getAmountCent() {
    return amountCent;
  }

  public void setAmountCent(Integer amountCent) {
    this.amountCent = amountCent;
  }

  public String getAmountYuanDescription() {
    return amountYuanDescription;
  }

  public void setAmountYuanDescription(String amountYuanDescription) {
    this.amountYuanDescription = amountYuanDescription;
  }

  public Short getCouponType() {
    return couponType;
  }

  public void setCouponType(Short couponType) {
    this.couponType = couponType;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Double getAmountLimit() {
    return amountLimit;
  }

  public void setAmountLimit(Double amountLimit) {
    this.amountLimit = amountLimit;
  }

  public Integer getAmountLimitCent() {
    return amountLimitCent;
  }

  public void setAmountLimitCent(Integer amountLimitCent) {
    this.amountLimitCent = amountLimitCent;
  }

  public String getAmountLimitYuanDescription() {
    return amountLimitYuanDescription;
  }

  public void setAmountLimitYuanDescription(String amountLimitYuanDescription) {
    this.amountLimitYuanDescription = amountLimitYuanDescription;
  }

  public String getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(String discountAmount) {
    this.discountAmount = discountAmount;
  }
}
