package com.xianzaishi.purchaseadmin.client.item.vo;

/**
 * 供应商列表页面查询参数vo
 * 
 * @author zhancang
 */
public class SkuRelationQueryVO {

  /**
   * sku名称
   */
  private String skuName;

  /**
   * 开始id，包含起点
   */
  private long beginItemCode;

  /**
   * 结束id，包含终点
   */
  private long endItemCode;

  /**
   * 查询类型<br/>
   * 0:普通进售价维护查询
   * 1：优惠进售价维护
   */
  private short type;
  
  /**
   * 每页大小
   */
  private int pageSize = 8;
  
  /**
   * 第几页
   */
  private int pageNum = 0;

  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }

  public long getBeginItemCode() {
    return beginItemCode;
  }

  public void setBeginItemCode(long beginItemCode) {
    this.beginItemCode = beginItemCode;
  }

  public long getEndItemCode() {
    return endItemCode;
  }

  public void setEndItemCode(long endItemCode) {
    this.endItemCode = endItemCode;
  }

  public short getType() {
    return type;
  }

  public void setType(short type) {
    this.type = type;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }
  
}
