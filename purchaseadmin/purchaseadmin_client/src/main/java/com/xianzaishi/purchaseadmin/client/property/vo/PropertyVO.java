package com.xianzaishi.purchaseadmin.client.property.vo;

import java.util.HashSet;
import java.util.List;

/**
 * 前台使用属性列表对象
 * 
 * @author zhancang
 */
public class PropertyVO {

  /**
   * 如下属性在审核通过过，采购商品发布时，将不允许修改
   */
  public static final HashSet<String> PRODUCT_PROPERTY_NAME_CHANGE_SET = new HashSet<String>() {
    private static final long serialVersionUID = 1L;
    {
      add("件装数");
      add("件装单位");
      add("件装规格");
      add("件装规格属性");
      add("采购规格");
      add("销售的规格");
    }
  };
  
  /**
   * 如下属性在审核通过过，销售商品修改时，将不允许修改
   */
  public static final HashSet<String> COMMODITY_PROPERTY_NAME_CHANGE_SET = new HashSet<String>() {
    private static final long serialVersionUID = 1L;
    {
      add("外部条码");
      add("销售的规格");
    }
  };
  
  /**
   * 如下在发布前台商品时展示
   */
  public static final HashSet<String> COMMODITY_PROPERTY_SET = new HashSet<String>() {
    private static final long serialVersionUID = 1L;
    {
      add("外部条码");
      add("采购规格");
      add("销售的规格");
    }
  };
  /**
   * 属性id
   */
  private int propertyId;

  /**
   * 属性名称
   */
  private String propertyName;

  /**
   * 属性是否可见
   */
  private boolean propertyDisplay = false;

  /**
   * 属性是否选择属性
   */
  private boolean isSelect = false;

  /**
   * 属性是否可输入属性
   */
  private boolean isInput = false;

  /**
   * 属性是否可输入属性
   */
  private boolean isRequired = false;

  /**
   * 是否允许修改
   */
  private boolean isModified = true;

  /**
   * 对于下拉属性，候选属性列表有什么
   */
  private List<ValueVO> valueList;
  
  /**
   * 有初始值时，选择好的初始值
   */
  private ValueVO selectedValue;

  public int getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(int propertyId) {
    this.propertyId = propertyId;
  }

  public String getPropertyName() {
    return propertyName;
  }

  public void setPropertyName(String propertyName) {
    this.propertyName = propertyName;
  }

  public boolean isPropertyDisplay() {
    return propertyDisplay;
  }

  public void setPropertyDisplay(boolean propertyDisplay) {
    this.propertyDisplay = propertyDisplay;
  }

  public boolean isSelect() {
    return isSelect;
  }

  public void setSelect(boolean isSelect) {
    this.isSelect = isSelect;
  }

  public boolean isInput() {
    return isInput;
  }

  public void setInput(boolean isInput) {
    this.isInput = isInput;
  }

  public boolean isRequired() {
    return isRequired;
  }

  public void setRequired(boolean isRequired) {
    this.isRequired = isRequired;
  }

  public List<ValueVO> getValueList() {
    return valueList;
  }

  public void setValueList(List<ValueVO> valueList) {
    this.valueList = valueList;
  }

  public boolean isModified() {
    return isModified;
  }

  public void setModified(boolean isModified) {
    this.isModified = isModified;
  }

  public ValueVO getSelectedValue() {
    return selectedValue;
  }

  public void setSelectedValue(ValueVO selectedValue) {
    this.selectedValue = selectedValue;
  }
  
}
