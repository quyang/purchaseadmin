package com.xianzaishi.purchaseadmin.client.inventory.vo;

public class OrderItemTransformDetail {

  /**
   * 补差价总价格
   */
  private String price;
  
  /**
   * 补差价痕迹，包括支付宝转账记录，照片记录等等
   */
  private String payInfo;
  
  /**
   * 补差价渠道。PayTypeConstants
   */
  private Short payType;

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getPayInfo() {
    return payInfo;
  }

  public void setPayInfo(String payInfo) {
    this.payInfo = payInfo;
  }

  public Short getPayType() {
    return payType;
  }

  public void setPayType(Short payType) {
    this.payType = payType;
  }
  
  public static class PayTypeConstants {

    /**
     * 支付宝转账公司账户
     */
    public static final int PAY_ALIPAY = 1;

    /**
     * 微信转账公司账户
     */
    public static final int PAY_WEIXIN = 2;
    
    /**
     * 客服代收，支付宝转账公司账户
     */
    public static final int PAY_ALIPAY_BY_CUSTOMER = 3;

    /**
     * 客服代收，微信转账公司账户
     */
    public static final int PAY_WEIXIN_BY_CUSTOMER = 4;
  }
}
