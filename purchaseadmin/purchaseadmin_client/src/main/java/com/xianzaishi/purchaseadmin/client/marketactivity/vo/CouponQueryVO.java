package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.util.List;

/**
 * Created by quyang on 2017/4/14.
 */
public class CouponQueryVO {

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 优惠券id
   */
  private Long couponId;

  /**
   * 有效时长
   */
  private Long duration;

  /**
   * 优惠券名称
   */
  private String title;

  /**
   * 开始时间
   */
  private String startTime;

  /**
   * 失效时间
   */
  private String endTime;

  /**
   * 使用渠道
   * 0通用渠道,1线上渠道,2线下
   */
  private Short channelType;

  /**
   * 优惠券类型
   * /**
   * type=1 看样子是现金券<br/>
   * type=77 早餐券<br/>
   * type=11 新人礼包券<br/>
   * type=12 指定商品优惠
   */
  private Short type;

  /**
   * 面额
   */
  private Integer amount;

  /**
   * 最大金额
   */
  private Integer amountLimit;

  /**
   * 使用条件
   * 0 无门槛
   * 1 满减券
   */
  private Short useConditon;

  /**
   * 限领张数
   */
  private Short limitAmount;

  /**
   * 发行张数
   */
  private Short distributeAmount;

  /**
   * 发放类型
   * 0 有就不发，没有就发
     1 用户拥有优惠券数量不满n张就发，满就不发；
     2 用户有效可用优惠券数量不满n张就发，满就不发；
   */
  private Short sendRulesType;

  /**
   * 是否关联商品，默认要关联商品
   */
  private Boolean related = true;

  /**
   * 商品skuId集合
   */
  private List<Long> skuIds;


  private Integer pageSize;

  private Integer pageNum;


  public Boolean getRelated() {
    return related;
  }

  public void setRelated(Boolean related) {
    this.related = related;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }

  public List<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<Long> skuIds) {
    this.skuIds = skuIds;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Long getDuration() {
    return duration;
  }

  public void setDuration(Long duration) {
    this.duration = duration;
  }

  public Short getSendRulesType() {
    return sendRulesType;
  }

  public void setSendRulesType(Short sendRulesType) {
    this.sendRulesType = sendRulesType;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Integer getAmountLimit() {
    return amountLimit;
  }

  public void setAmountLimit(Integer amountLimit) {
    this.amountLimit = amountLimit;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Short getUseConditon() {
    return useConditon;
  }

  public void setUseConditon(Short useConditon) {
    this.useConditon = useConditon;
  }

  public Short getLimitAmount() {
    return limitAmount;
  }

  public void setLimitAmount(Short limitAmount) {
    this.limitAmount = limitAmount;
  }

  public Short getDistributeAmount() {
    return distributeAmount;
  }

  public void setDistributeAmount(Short distributeAmount) {
    this.distributeAmount = distributeAmount;
  }
}
