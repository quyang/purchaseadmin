package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * Created by Administrator on 2017/2/20.
 */
public class SkuInformationVO {

  private String spec;

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }
}
