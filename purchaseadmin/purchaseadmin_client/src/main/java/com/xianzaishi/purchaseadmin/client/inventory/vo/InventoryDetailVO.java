package com.xianzaishi.purchaseadmin.client.inventory.vo;

public class InventoryDetailVO {

  private String num;
  
  private String poisition;

  public String getNum() {
    return num;
  }

  public void setNum(String num) {
    this.num = num;
  }

  public String getPoisition() {
    return poisition;
  }

  public void setPoisition(String poisition) {
    this.poisition = poisition;
  }
}
