package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;

/**
 * Created by Administrator on 2017/5/22.
 */
public class CouponReturnVO extends CouponDTO {

  private static final long serialVersionUID = 5009190548488446806L;


  /**
   * 优惠券类型
   */
  private String typeString;

  private String channelTypeString;

  private String statuString;

  public String getStatuString() {
    return statuString;
  }

  public void setStatuString(String statuString) {
    this.statuString = statuString;
  }

  public String getChannelTypeString() {
    return channelTypeString;
  }

  public void setChannelTypeString(String channelTypeString) {
    this.channelTypeString = channelTypeString;
  }

  public String getTypeString() {
    return typeString;
  }

  public void setTypeString(String typeString) {
    this.typeString = typeString;
  }

}
