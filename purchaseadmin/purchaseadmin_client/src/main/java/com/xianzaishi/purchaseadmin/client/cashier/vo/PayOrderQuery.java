package com.xianzaishi.purchaseadmin.client.cashier.vo;

public class PayOrderQuery {
  
  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 订单id
   */
  private Long orderId;
  
  /**
   * 支付金额
   */
  private String payMoney;
  
  /**
   * 支付签名
   */
  private String paySign;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getPayMoney() {
    return payMoney;
  }

  public void setPayMoney(String payMoney) {
    this.payMoney = payMoney;
  }

  public String getPaySign() {
    return paySign;
  }

  public void setPaySign(String paySign) {
    this.paySign = paySign;
  }
  
}
