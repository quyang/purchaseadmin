package com.xianzaishi.purchaseadmin.client.erpapp.vo;

import com.xianzaishi.purchaseadmin.client.user.vo.CatVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderSVO;
import java.util.List;

/**
 * Created by quyang on 2017/3/1.
 */
public class ResultDatas {

  /**
   * 类目id集合是否发送变化
   */
  private Boolean windowChange = false;

  /**
   * 类目id集合
   */
  List<CatVO> catlist;


  /**
   * 订单集合
   */
  private List<OrderSVO>  orders;

  public Boolean getWindowChange() {
    return windowChange;
  }

  public void setWindowChange(Boolean windowChange) {
    this.windowChange = windowChange;
  }

  public List<CatVO> getCatlist() {
    return catlist;
  }

  public void setCatlist(List<CatVO> catlist) {
    this.catlist = catlist;
  }

  public List<OrderSVO> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderSVO> orders) {
    this.orders = orders;
  }
}
