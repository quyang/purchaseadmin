package com.xianzaishi.purchaseadmin.client.trade.vo;

public class OrderStatisticsVO {

  /**
   * 收银员id
   */
  private long userId;
  
  /**
   * 开始收银时间
   */
  private long start;
  
  /**
   * 结束收银时间
   */
  private long end;
  
  /**
   * 天威单位的描述格式
   */
  private String date;
  
  private int cash;

  private int yintong;

  private int weixinpay;

  private int weixinpaypos;

  private int weixinpayfula;

  private int alipay;

  private int alipaypos;

  private int alipayfula;

  private int credit;

  private int koubei;

  private int coupon;
  
  /**
   * 账户支付统计，分为单位
   */
  private int accountnum;

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public long getStart() {
    return start;
  }

  public void setStart(long start) {
    this.start = start;
  }

  public long getEnd() {
    return end;
  }

  public void setEnd(long end) {
    this.end = end;
  }

  public int getCash() {
    return cash;
  }

  public void setCash(int cash) {
    this.cash = cash;
  }

  public int getYintong() {
    return yintong;
  }

  public void setYintong(int yintong) {
    this.yintong = yintong;
  }

  public int getWeixinpay() {
    return weixinpay;
  }

  public void setWeixinpay(int weixinpay) {
    this.weixinpay = weixinpay;
  }

  public int getWeixinpaypos() {
    return weixinpaypos;
  }

  public void setWeixinpaypos(int weixinpaypos) {
    this.weixinpaypos = weixinpaypos;
  }

  public int getWeixinpayfula() {
    return weixinpayfula;
  }

  public void setWeixinpayfula(int weixinpayfula) {
    this.weixinpayfula = weixinpayfula;
  }

  public int getAlipay() {
    return alipay;
  }

  public void setAlipay(int alipay) {
    this.alipay = alipay;
  }

  public int getAlipaypos() {
    return alipaypos;
  }

  public void setAlipaypos(int alipaypos) {
    this.alipaypos = alipaypos;
  }

  public int getAlipayfula() {
    return alipayfula;
  }

  public void setAlipayfula(int alipayfula) {
    this.alipayfula = alipayfula;
  }

  public int getCredit() {
    return credit;
  }

  public void setCredit(int credit) {
    this.credit = credit;
  }

  public int getKoubei() {
    return koubei;
  }

  public void setKoubei(int koubei) {
    this.koubei = koubei;
  }

  public int getCoupon() {
    return coupon;
  }

  public void setCoupon(int coupon) {
    this.coupon = coupon;
  }

  public int getAccountnum() {
    return accountnum;
  }

  public void setAccountnum(int accountnum) {
    this.accountnum = accountnum;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }
  
}
