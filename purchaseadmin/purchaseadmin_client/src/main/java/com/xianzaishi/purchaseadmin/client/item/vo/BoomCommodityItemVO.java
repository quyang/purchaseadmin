package com.xianzaishi.purchaseadmin.client.item.vo;

/**
 * boom单前台商品对象
 * @author zhancang
 */
public class BoomCommodityItemVO extends CommodityItemVO{

  /**
   * 销售描述
   */
  private String saleDescribe;
  
  /**
   * 关联boom单id
   */
  private long boomId;
  
  /**
   * 生产数量，代表库存
   */
  private int inventory;
  
  /**
   * 标识是否boom数据
   */
  private boolean isboom = true;

  public String getSaleDescribe() {
    return saleDescribe;
  }

  public void setSaleDescribe(String saleDescribe) {
    this.saleDescribe = saleDescribe;
  }

  public long getBoomId() {
    return boomId;
  }

  public void setBoomId(long boomId) {
    this.boomId = boomId;
  }

  public int getInventory() {
    return inventory;
  }

  public void setInventory(int inventory) {
    this.inventory = inventory;
  }

  public boolean isIsboom() {
    return isboom;
  }

  public void setIsboom(boolean isboom) {
    this.isboom = isboom;
  }
}
