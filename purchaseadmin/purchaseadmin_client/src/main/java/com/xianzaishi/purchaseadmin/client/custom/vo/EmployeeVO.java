package com.xianzaishi.purchaseadmin.client.custom.vo;

/**
 * Created by quyang on 2017/3/15.
 */
public class EmployeeVO {

  /**
   * 员工工号
   */
  private Integer opertatorId;

  public Integer getOpertatorId() {
    return opertatorId;
  }

  public void setOpertatorId(Integer opertatorId) {
    this.opertatorId = opertatorId;
  }
}
