package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * Created by quyang on 2017/3/6.
 */
public class CatVO {

  /**
   * 类目id
   */
  private Integer catId;
  /**
   * 类目名称
   */
  private String catName;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }
}
