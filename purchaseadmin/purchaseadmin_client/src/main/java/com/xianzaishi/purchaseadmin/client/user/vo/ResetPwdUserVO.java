package com.xianzaishi.purchaseadmin.client.user.vo;

/**
 * 重置密码对象
 * @author zhancang
 */
public class ResetPwdUserVO {

  private String userName;
  
  /**
   * md5(pwd)
   */
  private String pwd;
  
  /**
   * md5(phone activeCode)
   */
  private String token;
  
  /**
   * 设备号
   */
  private String equipmentId;
  
  /**
   * 
   */
  private long sysTime;
  
  /**
   * md5(pwd_token_sysTime_equipmentId)
   */
  private String sign;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public long getSysTime() {
    return sysTime;
  }

  public void setSysTime(long sysTime) {
    this.sysTime = sysTime;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getEquipmentId() {
    return equipmentId;
  }

  public void setEquipmentId(String equipmentId) {
    this.equipmentId = equipmentId;
  }
  
}
