package com.xianzaishi.purchaseadmin.client.marketactivity.vo;

import java.util.Date;
import java.util.LinkedList;

/**
 * Created by Administrator on 2017/4/14.
 */
public class MarketActivityVO {

  /**
   * 操作人
   */
  private Integer operator;

  /**
   * 活动id
   */
  private Integer id;

  /**
   * 活动名称
   */
  private String name;
  /**
   * 活动状态
   */
  private Short status;
  /**
   * 活动类型
   */
  private Short type;

  /**
   * 开始时间
   */
  private Date gmtStart;

  private String gmtStartString;

  /**
   * 结束时间
   */
  private Date gmtEnd;

  private String gmtEndString;

  private Date gmtCreate;

  private Date gmtModified;


  //满件

  /**
   * 折扣
   */
  private String discount;

  /**
   * 是否包邮
   */
  private Boolean post;
  /**
   * 满n件
   */
  private String discountCount;



  //限购活动

  /**
   * 限时折扣
   */
  private String limitedTimeDiscount;

  /**
   * 限购数
   */
  private String limitCount;



  private LinkedList<Long> skuIds;

  private LinkedList<Long> couponIds;

  //打标渠道类型  线上场景 1; 线下场景 2;全场场景 0;
  private Short channelType;

  public String getLimitedTimeDiscount() {
    return limitedTimeDiscount;
  }

  public void setLimitedTimeDiscount(String limitedTimeDiscount) {
    this.limitedTimeDiscount = limitedTimeDiscount;
  }

  public Integer getOperator() {
    return operator;
  }

  public void setOperator(Integer operator) {
    this.operator = operator;
  }

  public String getLimitCount() {
    return limitCount;
  }

  public void setLimitCount(String limitCount) {
    this.limitCount = limitCount;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LinkedList<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(LinkedList<Long> skuIds) {
    this.skuIds = skuIds;
  }

  public LinkedList<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(LinkedList<Long> couponIds) {
    this.couponIds = couponIds;
  }

  public String getDiscount() {
    return discount;
  }

  public void setDiscount(String discount) {
    this.discount = discount;
  }

  public Boolean getPost() {
    return post;
  }

  public void setPost(Boolean post) {
    this.post = post;
  }

  public String getDiscountCount() {
    return discountCount;
  }

  public void setDiscountCount(String discountCount) {
    this.discountCount = discountCount;
  }

  public String getGmtStartString() {
    return gmtStartString;
  }

  public void setGmtStartString(String gmtStartString) {
    this.gmtStartString = gmtStartString;
  }

  public String getGmtEndString() {
    return gmtEndString;
  }

  public void setGmtEndString(String gmtEndString) {
    this.gmtEndString = gmtEndString;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }
}
