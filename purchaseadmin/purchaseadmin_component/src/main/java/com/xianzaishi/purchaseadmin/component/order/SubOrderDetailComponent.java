package com.xianzaishi.purchaseadmin.component.order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.order.vo.SubOrderDetailVO;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderTypeConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.StorageOrderDTO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.EmployeeService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.EmployeeDTO;

/**
 * 订单详情页接口实现类
 * 
 * @author dongpo
 * 
 */
@Component("subOrderDetailComponent")
public class SubOrderDetailComponent {

  @Autowired
  private PurchaseService purchaseService;

  @Autowired
  private EmployeeService employeeservice;
  
  @Autowired
  private BackGroundUserService backGroundUserService;

  @Autowired
  private SkuService skuService;

  @Autowired
  private ValueService valueService;


  public String queryOrderDetail(Integer subOrderId, Boolean displayReviewStatus) {
    Result<PurchaseSubOrderDTO> purchaseSubOrderResult =
        purchaseService.querySubPurchase(subOrderId);
    PurchaseSubOrderDTO purchaseSubOrderDTO = purchaseSubOrderResult.getModule();
    if (null == purchaseSubOrderDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "采购订单不存在"));
    }
    SubOrderDetailVO subOrderDetailVO = getOrderDetail(purchaseSubOrderDTO);
    subOrderDetailVO.setDisplayReviewStatus(displayReviewStatus);
    return toJsonData(Result.getSuccDataResult(subOrderDetailVO));
  }

  /**
   * 数据库数据格式转换为前端可视化数据格式
   * @param purchaseSubOrderDTO
   * @return
   */
  private SubOrderDetailVO getOrderDetail(PurchaseSubOrderDTO purchaseSubOrderDTO) {
    if (null == purchaseSubOrderDTO) {
      return null;
    }
    SubOrderDetailVO subOrderDetailVO = new SubOrderDetailVO();
    BeanCopierUtils.copyProperties(purchaseSubOrderDTO, subOrderDetailVO);//将purchaseSubOrderDTO和subOrderDetailVO一些公共字段的值赋值到subOrderDetailVO中
    Result<EmployeeDTO> employeeResult =
        employeeservice.queryEmployeeById(purchaseSubOrderDTO.getPurchasingAgent());//获取员工信息
    EmployeeDTO employeeDTO = employeeResult.getModule();
    if(null != employeeDTO){
      String purchasingAgentName = employeeDTO.getName();//获取员工名字
      subOrderDetailVO.setPurchasingAgent(purchasingAgentName);//设置采购人名
    }
    StringBuffer title = new StringBuffer();
    title.append(purchaseSubOrderDTO.getTitle());//获取sku名称
    title.append("_");//“_”区分sku名称和年月日
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    title.append(df.format(new Date()));
    subOrderDetailVO.setTitle(title.toString());//设置sku名称+年月日
    Result<List<ItemProductSkuDTO>> itemProductSkuResult =
        skuService.queryProductSku(SkuQuery.querySkuById(purchaseSubOrderDTO.getSkuId()));//查询商品sku信息
    List<ItemProductSkuDTO> itemProductSkuDTOs = itemProductSkuResult.getModule();
    if (null != itemProductSkuDTOs && itemProductSkuDTOs.size() != 0) {
      Result<String> valueNameResult =
          valueService.queryValueNameByValueId(itemProductSkuDTOs.get(0).getSkuUnit());//获取属性值名称
      String valueName = valueNameResult.getModule();
      if (null != valueName) {
        subOrderDetailVO.setSkuUnit(valueName);// 设置sku数量单位
      }
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String expectArriveDate = sdf.format(new Date());
    subOrderDetailVO.setExpectArriveDate(expectArriveDate);

    subOrderDetailVO.setQualityId(null);// TODO:质检id列表
    Result<List<StorageOrderDTO>> storageOrderResult =
        purchaseService.queryStorageOrderByPurchaseSubOrder(purchaseSubOrderDTO.getPurchaseSubId());//查询入库信息
    List<StorageOrderDTO> storageOrderDTOs = storageOrderResult.getModule();
    List<Integer> storageIds = Lists.newArrayList();
    if (null != storageOrderDTOs) {
      for (StorageOrderDTO storageOrderDTO : storageOrderDTOs)
        storageIds.add(storageOrderDTO.getStorageId());//获取入库信息id
    }
    subOrderDetailVO.setStorageId(storageIds);//设置入库id
    subOrderDetailVO.setDisplayReviewStatus(true);//设置审核失败原因以及审核人列表可见性,TODO:根据员工权限来决定可见性
    if(null == subOrderDetailVO.getSettlementPrice()){
      Integer settlementPrice = purchaseSubOrderDTO.getCount() * purchaseSubOrderDTO.getUnitCost();
      subOrderDetailVO.setSettlementPrice(settlementPrice);
    }
    List<Integer> checkerList = purchaseSubOrderDTO.getCheckerList();
    
    if(null != checkerList && checkerList.size() != 0){
      Result<List<BackGroundUserDTO>> backGroundUserResult = backGroundUserService.queryUserDTOByIdList(checkerList);
      List<BackGroundUserDTO> backGroundUserDTOs = backGroundUserResult.getModule();
      List<String> checkerName = Lists.newArrayList();
      for(BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs){
        checkerName.add(backGroundUserDTO.getName());
      }
      subOrderDetailVO.setCheckerList(checkerName);//设置审核人列表
    }
    Short typeCode = purchaseSubOrderDTO.getType();
    subOrderDetailVO.setTypeCode(typeCode);//采购类型码
    setTypeStr(typeCode,subOrderDetailVO);//采购类型字符串
    Short statusCode = purchaseSubOrderDTO.getStatus();//获取采购状态码
    subOrderDetailVO.setStatusCode(statusCode);//采购状态码
    setStatusStr(statusCode, subOrderDetailVO);//设置采购状态字符串
    return subOrderDetailVO;
  }
  
  
  /**
   * 提交审核以及审核通过和不通过时更新数据库
   * @param subOrderDetailVO
   * @return
   */
  public String updateSubOrder(SubOrderDetailVO subOrderDetailVO,Integer userId){
    if(null == subOrderDetailVO){
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "数据为空"));
    }
    Result<PurchaseSubOrderDTO> purchaseSubOrderResult = purchaseService.querySubPurchase(subOrderDetailVO.getPurchaseSubId());
    if(!purchaseSubOrderResult.getSuccess()){
      return toJsonData(purchaseSubOrderResult);
    }
    PurchaseSubOrderDTO purchaseSubOrderDTO = purchaseSubOrderResult.getModule();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      purchaseSubOrderDTO.setExpectArriveDate(sdf.parse(subOrderDetailVO.getExpectArriveDate()));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    if(null != subOrderDetailVO.getCheckedFailedReason()){
      purchaseSubOrderDTO.setCheckedFailedReason(subOrderDetailVO.getCheckedFailedReason());
    }
    purchaseSubOrderDTO.setStatus(subOrderDetailVO.getStatusCode());
    if(null != userId){
      List<Integer> checkList = purchaseSubOrderDTO.getCheckerList();
      checkList.add(userId);
      purchaseSubOrderDTO.setCheckerList(checkList);
    }
    Result<Boolean> result = purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO);
    return toJsonData(result);
  }
  
  /**
   * 设置采购单的采购类型
   * @param type
   * @param subOrderDetailVO
   */
  private void setTypeStr(Short type, SubOrderDetailVO subOrderDetailVO) {
    if(null == type ||PurchaseSubOrderTypeConstants.PURCHASE_TYPE_NEW == type.shortValue()){
      subOrderDetailVO.setType("新品采购");
    }else if(PurchaseSubOrderTypeConstants.PURCHASE_TYPE_REPLENISHMENT == type.shortValue()){
      subOrderDetailVO.setType("补货采购");
    }else{
      subOrderDetailVO.setType("其他采购");
    }
  }
  
  /**
   * 设置采购单的采购状态
   * @param status
   * @param subOrderDetailVO
   */
  private void setStatusStr(Short status, SubOrderDetailVO subOrderDetailVO) {
    if (null == status
        || PurchaseSubOrderStatusConstants.SUBORDER_STATUS_WAITCHECK == status.shortValue()) {
      subOrderDetailVO.setStatus("正在待审核状态");
    } else if (PurchaseSubOrderStatusConstants.SUBORDER_STATUS_PASS == status.shortValue()) {
      subOrderDetailVO.setStatus("审核通过状态");
    } else if (PurchaseSubOrderStatusConstants.SUBORDER_STATUS_CHECKED_NOTPASS == status.shortValue()) {
      subOrderDetailVO.setStatus("审核不通过状态");
    } else if (PurchaseSubOrderStatusConstants.SUBORDER_STATUS_CHECKING.shortValue() == status
        .shortValue()) {
      subOrderDetailVO.setStatus("正在审核状态");
    } else {
      subOrderDetailVO.setStatus("其它状态");
    }
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

}
