package com.xianzaishi.purchaseadmin.component.monitor;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.monitor.vo.MonitorInformationVO;
import com.xianzaishi.purchasecenter.client.logmonitor.LogMonitorService;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO.LogMonitorDTOConstants;
import com.xianzaishi.purchasecenter.client.logmonitor.query.LogMonitorQuery;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.LogMonitorConfigService;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.dto.LogMonitorConfigDTO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.service.MessageService;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("monitorComponent")
public class MonitorComponent {


  @Autowired
  private LogMonitorService logMonitorService;

  @Autowired
  private MessageService messageservice;

  @Autowired
  private BackGroundUserService backGroundUserService;

  @Autowired
  private LogMonitorConfigService logMonitorConfigService;

  private static final Logger logger = Logger.getLogger(MonitorComponent.class);

  private static final String TEMPLATE_NAME = "SMS_45825035";

  /**
   * 插入监控信息
   */
  public String insertLog(MonitorInformationVO query) {

    //name type info
    logger.error("query=" + JackSonUtil.getJson(query));

    if (null == query || null == query.getName()) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    LogMonitorQuery logMonitorQuery = new LogMonitorQuery();
    logMonitorQuery.setName(query.getName());
    logMonitorQuery.setStatus(LogMonitorDTOConstants.NEW_DATA);//默认最新数据

    logger.error("查询参数=" + JackSonUtil.getJson(logMonitorQuery));

    Result<List<LogMonitorDTO>> logMonitorDTOResult = logMonitorService
        .queryLog(logMonitorQuery);//主要通过name和status=1
    if (null != logMonitorDTOResult && logMonitorDTOResult.getSuccess()
        && null != logMonitorDTOResult.getModule() && CollectionUtils
        .isNotEmpty(logMonitorDTOResult.getModule())) {
      //有插入过监控数据

      List<LogMonitorDTO> monitorDTOList = logMonitorDTOResult.getModule();
      LogMonitorDTO monitorDTO = monitorDTOList.get(0);

      //在监控时间范围  修改时间
      logger.error("存在监控数据=" + JackSonUtil.getJson(monitorDTO));

      monitorDTO.setStatus(LogMonitorDTOConstants.OLD_DATA);

      //更新数据时间
      Result<Boolean> booleanResult = logMonitorService.updateLog(monitorDTO);
      logger.error("更新数据结果=" + JackSonUtil.getJson(booleanResult));
      if (null != booleanResult && booleanResult.getSuccess() && booleanResult.getModule()) {

        LogMonitorDTO logMonitorDTO = new LogMonitorDTO();
        BeanCopierUtils.copyProperties(query, logMonitorDTO);
        logger.error("插入数据=" + JackSonUtil.getJson(logMonitorDTO));
        return insertLogMonitorInfo(logMonitorDTO);
      } else {
        return JackSonUtil
            .getJson(
                Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新监控数据失败"));
      }
    }

    //之前没有监控数据
    LogMonitorDTO logMonitorDTO = new LogMonitorDTO();
    BeanCopierUtils.copyProperties(query, logMonitorDTO);
    return insertLogMonitorInfo(logMonitorDTO);
  }

  /**
   * 插入一条监控信息
   */
  private String insertLogMonitorInfo(LogMonitorDTO logMonitorDTO) {
    logMonitorDTO.setStatus(LogMonitorDTOConstants.NEW_DATA);
    Result<Integer> integerResult = logMonitorService.insertLog(logMonitorDTO);
    logger.error("插入结果=" + JackSonUtil.getJson(integerResult));
    if (null != integerResult && integerResult.getSuccess() && null != integerResult.getModule()
        && integerResult.getModule() == 1) {
      return JackSonUtil.getJson(Result.getSuccDataResult(true));
    }

    return JackSonUtil
        .getJson(
            Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入监控数据失败"));
  }


  /**
   * 根据手机号数组发送短信
   */
  public void sendSMS(LogMonitorConfigDTO logMonitorConfigDTO) {
    if (null == logMonitorConfigDTO) {
      return;
    }

    if (StringUtils.isEmpty(logMonitorConfigDTO.getPhone())) {
      return;
    }

    String[] phoneList = logMonitorConfigDTO.getPhone().split(";");

    if (phoneList.length == 0) {
      return;
    }

    for (String phone : phoneList) {
      Map<String, String> checkCodeMap = Maps.newHashMap();
      checkCodeMap.put("code", "   业务类 " + logMonitorConfigDTO.getType() + " 异常");// 短信内容
      messageservice.sendSmsMessage(phone, TEMPLATE_NAME, checkCodeMap);
    }

  }


  /**
   * 更新一条监控数据
   */
  public String updateLog(MonitorInformationVO data) {

    if (null == data) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求参数不正确"));
    }

    if (null == data.getType() && StringUtils.isEmpty(data.getName())) {
      return JackSonUtil
          .getJson(Result
              .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "监控事件名称和事件类型不能同时为空"));
    }

    LogMonitorQuery logMonitorQuery = new LogMonitorQuery();
    logMonitorQuery.setName(data.getName());
    logMonitorQuery.setType(data.getType());
    logMonitorQuery.setStatus(LogMonitorDTOConstants.NEW_DATA);
    logger.error("查询参数=" + JackSonUtil.getJson(logMonitorQuery));
    Result<List<LogMonitorDTO>> logMonitorDTOResult = logMonitorService
        .queryLog(logMonitorQuery);//主要通过name和status=1

    if (null == logMonitorDTOResult || !logMonitorDTOResult.getSuccess()
        || null == logMonitorDTOResult
        .getModule() || CollectionUtils.isEmpty(logMonitorDTOResult.getModule())) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该监控数据不存在，无法更新"));
    }

    List<LogMonitorDTO> moduleList = logMonitorDTOResult.getModule();

    if (CollectionUtils.isNotEmpty(moduleList)) {
      LogMonitorDTO logMonitorDTO = moduleList.get(0);

      Result<Boolean> booleanResult = logMonitorService.updateLog(logMonitorDTO);
      if (null != booleanResult && booleanResult.getSuccess()) {
        return JackSonUtil.getJson(Result.getSuccDataResult(true));
      }

      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新数据失败"));
    }

    return JackSonUtil
        .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "不存在该类数据"));

  }

  /**
   * 更新监控配置信息
   * @param data
   * @return
   */
  public String updateLogConfig(MonitorInformationVO data) {
    if (null == data ) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"参数不正确" ));
    }

    if (data.getType() == null && StringUtils.isEmpty(data.getName()) && StringUtils.isEmpty(data.getPhone())) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"参数不能全部为空" ));
    }

    LogMonitorConfigDTO configDTO = new LogMonitorConfigDTO();
    configDTO.setType(data.getType());
    logger.error("查询条件=" + JackSonUtil.getJson(configDTO));
    Result<List<LogMonitorConfigDTO>> logResult = logMonitorConfigService.select(configDTO);
    logger.error("查询结果=" + JackSonUtil.getJson(logResult));
    if (null == logResult || !logResult.getSuccess() || null == logResult.getModule()
        || CollectionUtils.isEmpty(logResult.getModule())) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"要更新的配置信息不存在" ));
    }

    List<LogMonitorConfigDTO> configDTOList = logResult.getModule();
    LogMonitorConfigDTO logMonitorConfigDTO = configDTOList.get(0);
    if (null != data.getTimeScope() && data.getTimeScope() > 0) {
      logMonitorConfigDTO.setTimeScope(data.getTimeScope());
    }
    if (StringUtils.isNotEmpty(data.getPhone())) {
      logMonitorConfigDTO.setPhone(data.getPhone());
    }

    Result<Boolean> booleanResult = logMonitorConfigService.update(logMonitorConfigDTO);
    if (null == booleanResult || !booleanResult.getSuccess() || null == booleanResult.getModule() || !booleanResult.getModule()) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"更新配置信息失败" ));
    }

    return JackSonUtil.getJson(Result.getSuccDataResult(booleanResult.getModule()));
  }

  /**
   * 插入一条监控配置信息
   */
  public String insertLogConfig(MonitorInformationVO data) {
    if (null == data || null == data.getTimeScope() || null == data.getType() || StringUtils
        .isEmpty(data.getPhone())) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数信息不正确"));
    }

    LogMonitorConfigDTO logMonitorConfigDTO = new LogMonitorConfigDTO();
    logMonitorConfigDTO.setType(data.getType());
    logMonitorConfigDTO.setPhone(data.getPhone());
    logMonitorConfigDTO.setTimeScope(data.getTimeScope());
    logger.error("插入数据=" + JackSonUtil.getJson(logMonitorConfigDTO));
    Result<Boolean> booleanResult = logMonitorConfigService.insert(logMonitorConfigDTO);
    logger.error("插入结果=" + JackSonUtil.getJson(booleanResult));
    if (null == booleanResult || !booleanResult.getSuccess() || null == booleanResult.getModule() || !booleanResult.getModule()) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"插入配置信息失败" ));
    }

    return JackSonUtil.getJson(Result.getSuccDataResult(booleanResult.getModule()));
  }

  /**
   * 插入或更新一条监控数据
   */
  public String insertOrUpdateLog(MonitorInformationVO data) {
    if (null == data || null == data.getType() || data.getType() < 0 || null == data.getOnlyUpdate()) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确"));
    }

    Result<Boolean> booleanResult = logMonitorService.insertOrUpdate(data.getType(),data.getOnlyUpdate());
    logger.error("插入或更新数据结果=" + JackSonUtil.getJson(booleanResult));
    if (null == booleanResult || !booleanResult.getSuccess() || !booleanResult.getModule()) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入或更新数据失败"));
    }

    return JackSonUtil.getJson(Result.getSuccDataResult(booleanResult.getModule()));
  }
}
