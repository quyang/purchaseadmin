package com.xianzaishi.purchaseadmin.component.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识需要跨域
 * 
 * @author zhancang
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Crossdomain {

  public enum PageType {
    
    ERP_DEV,//erp-dev.xianzaishi.net 需要跨域
    ERP_PAGE, // erp.xianzaishi.com;erp.xianzaishi.net 页面需要跨域
    HOME_PAGE // www.xianzaishi.com;www.xianzaishi.net 页面需要跨域

  }

  PageType targetDomain() default PageType.ERP_PAGE;

}
