package com.xianzaishi.purchaseadmin.component.category;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.xianzaishi.itemcenter.client.property.PropertyService;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.base.vo.excel.ExcelEmployee;

@Component("categoryComponent")
public class CategoryComponent {

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private StdCategoryService stdcategoryservice;

  @Autowired
  private ValueService valueService;
  
  @Autowired
  private PropertyInsertComponent propertyInsertComponent;

  /**
   * 临时缓存数据
   */
  private Map<String, String> propertyM = Maps.newHashMap();

  /**
   * 临时缓存数据
   */
  private Map<String, String> valueM = Maps.newHashMap();
  
  private static final Logger logger = Logger.getLogger(CategoryComponent.class);

  /**
   * 取属性对象
   * 
   * @param catId
   * @param proName
   * @return
   */
  public PropertyDTO getPropertyDTO(int catId, String proName, boolean includeValues) {
    Result<PropertyDTO> tmpProResult =
        propertyService.queryPropertyByPropertyName(catId, proName, true);
    if (null != tmpProResult && tmpProResult.getSuccess()) {
      return tmpProResult.getModule();
    }
    return null;
  }

  public List<PropertyDTO> getPropertyDTOByCatId(int catId,boolean includeValue) {
    // 获取当前类目的属性属性值列表
    Result<List<PropertyDTO>> dtoResult = propertyService.queryPropertyByCatId(catId,includeValue);
    if (null == dtoResult || !dtoResult.getSuccess()
        || CollectionUtils.isEmpty(dtoResult.getModule())) {
      return Collections.emptyList();
    }
    return dtoResult.getModule();
  }



  /**
   * 根据属性名取一个属性id
   * 
   * @param catId
   * @param proNamme
   * @return
   */
  public int getPropertyId(int catId, String proName) {
    String key = "" + catId + "_" + proName;
    String value = this.propertyM.get(key);
    if (StringUtils.isNotEmpty(value)) {
      String valueArray[] = value.split("_");
      long time = Long.valueOf(valueArray[1]);
      if (new Date().getTime() - time > 10 * 60 * 1000) {
        return Integer.valueOf(valueArray[0]);
      }
    }
    Result<PropertyDTO> tmpProResult =
        propertyService.queryPropertyByPropertyName(catId, proName, false);
    if (null != tmpProResult && tmpProResult.getSuccess() && null != tmpProResult.getModule()) {
      Integer id = tmpProResult.getModule().getPropertyId();
      if (null != id && id > 0) {
        String cacheV = id + "_" + new Date().getTime();
        this.propertyM.put(key, cacheV);
        return id;
      }
    }

    return -1;
  }

  /**
   * 取属性值对象
   * 
   * @param catId
   * @param propertyId
   * @param valueName
   * @return
   */
  public ValueDTO getValue(int catId, int propertyId, int valueId) {
    Result<List<ValueDTO>> valueResult = valueService.queryValue(catId, propertyId, valueId);
    if (null != valueResult && valueResult.getSuccess()) {
      return valueResult.getModule().get(0);
    }
    return null;
  }

  /**
   * 取属性值名称
   * 
   * @param catId
   * @param propertyId
   * @param valueName
   * @return
   */
  public String getValueNameByValueId(int valueId) {
    Result<String> valueNameResult = valueService.queryValueNameByValueId(valueId);
    if (valueNameResult == null || !valueNameResult.getSuccess()) {
      return "";
    } else {
      return valueNameResult.getModule();
    }
  }

  /**
   * 取属性值名称
   * 
   * @param catId
   * @param propertyId
   * @param valueName
   * @return
   */
  public Map<Integer,String> getValueNameByValueIdList(List<Integer> idList) {
    Result<Map<Integer,String>> valueNameResult = valueService.queryValueNameListByValueId(idList);
    if (valueNameResult == null || !valueNameResult.getSuccess()) {
      return Collections.emptyMap();
    } else {
      return valueNameResult.getModule();
    }
  }
  
  /**
   * 按照属性值查询属性值对象
   * 
   * @param catId
   * @param propertyId
   * @param valueName
   * @return
   */
  public int getValueId(int catId, int propertyId, String valueName) {
    String key = "" + catId + "_" + propertyId + "_" + valueName;
    String value = this.valueM.get(key);
    if (StringUtils.isNotEmpty(value)) {
      String valueArray[] = value.split("_");
      long time = Long.valueOf(valueArray[1]);
      if (new Date().getTime() - time > 10 * 60 * 1000) {
        return Integer.valueOf(valueArray[0]);
      }
    }
    Result<Integer> valueResult = valueService.queryValueIdByValueName(valueName);
    if (null != valueResult && valueResult.getSuccess()) {
      int id = valueResult.getModule();
      if (id > 0) {
        String cacheV = id + "_" + new Date().getTime();
        this.valueM.put(key, cacheV);
        return id;
      }
    }
    return -1;
  }

  public CategoryDTO getCategoryDTO(String name) {
    Result<CategoryDTO> result = stdcategoryservice.queryCategoryByName(name);
    if (null == result || !result.getSuccess()) {
      return null;
    }
    return result.getModule();
  }
  
  public CategoryDTO getCategoryDTO(Integer id) {
    Result<CategoryDTO> result = stdcategoryservice.queryCategoryById(id);
    if (null == result || !result.getSuccess() || null == result.getModule()) {
      return null;
    }
    return result.getModule();
  }
  
  public void addAllCategory(String pathFlag) {
    String path = "D:/work/input/cmcat.xls";
    if (pathFlag.equals("local")) {
      path = "D:/work/input/cmcat.xls";
    } else {
      path = "/usr/works/datainit/cmcat.xls";
    }
    List<ExcelEmployee> employeeList = null;
    try {
      employeeList = this.readEmployeeExcel(path);
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    Set<Integer> idList = Sets.newHashSet();
    Set<String> errorSet = Sets.newHashSet();
    for(ExcelEmployee em:employeeList){
      String stdName3 = em.getProperty().get(3);
      Result<CategoryDTO> catDO = stdcategoryservice.queryCategoryByName(stdName3);
      if(null != catDO && catDO.getSuccess() && null != catDO.getModule()){
        System.out.println("success:"+catDO.getModule().getCatName());
        if(StringUtils.isEmpty(catDO.getModule().getCatName())){
          errorSet.add(stdName3);
        }
        idList.add(catDO.getModule().getCatId());
      }else{
        System.out.println("error:catname:"+stdName3);
        errorSet.add(stdName3);
      }
    }
    System.out.println(idList);
    System.out.println(errorSet);
  }
  /**
   * 读取一个excel文件，转化为可用对象
   * 
   * @return
   * @throws Exception
   */
  public List<ExcelEmployee> readEmployeeExcel(String path) throws Exception {
    File file = new File(path);
    POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream(file));
    HSSFWorkbook hssfWorkbook = new HSSFWorkbook(poifsFileSystem);
    HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

    List<ExcelEmployee> result = Lists.newArrayList();

    int rowstart = hssfSheet.getFirstRowNum() + 1;
    int rowEnd = hssfSheet.getLastRowNum();
    Map<Integer,String> idMap = Maps.newHashMap();
    for (int i = rowstart; i <= rowEnd; i++) {
      HSSFRow row = hssfSheet.getRow(i);
      if (null == row) {
        continue;
      }

      int cellStart = row.getFirstCellNum();
      int cellEnd = row.getLastCellNum();

      ExcelEmployee employee = new ExcelEmployee();
      for (int k = cellStart; k <= cellEnd; k++) {
        HSSFCell cell = row.getCell(k);
        if (null == cell) {
          continue;
        } else {
          cell.setCellType(Cell.CELL_TYPE_STRING);
          String value = cell.getStringCellValue();
          if(StringUtils.isEmpty(value)){
            employee.addProperty(idMap.get(k));
          }else{
            employee.addProperty(value);
            idMap.put(k, cell.getStringCellValue());
          }
        }
      }
      result.add(employee);
    }
    return result;
  }
  
  /**
   * 插入单个子类目
   * 
   * @param parentId
   * @param catName
   * @return
   */
  public Result<Integer> insertSubCategoryDate(Integer parentId, String catName) {
    if (null == parentId || null == catName) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    Result<CategoryDTO> catNameResult = stdcategoryservice.queryCategoryByName(catName);
    logger.error("数据库是否存在该类目名：" + JackSonUtil.getJson(catNameResult));
    if (null != catNameResult && catNameResult.getSuccess() && null != catNameResult.getModule()
        && null != catNameResult.getModule().getCatId()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST,
          "该类目名已经存在数据库，请换一个类目名称");
    }
    Result<List<CategoryDTO>> categoryResult = stdcategoryservice.querySubcategoryById(parentId);
    Long catCode = 0L;
    Integer level = 0;
    Short sortValue = 1;
    Map<String, Object> mapResult = getCategoryCode(parentId);
    if (null == categoryResult || !categoryResult.getSuccess()
        || CollectionUtils.isEmpty(categoryResult.getModule())) {
      catCode = (Long) mapResult.get("subCatCode");// 获取类目编码
      level = Integer.valueOf((String) mapResult.get("level")) + 1;
    } else {
      List<CategoryDTO> categoryDTOs = categoryResult.getModule();
      CategoryDTO catCodeMax = categoryDTOs.get(0);
      for (CategoryDTO categoryDTO : categoryDTOs) {
        if (categoryDTO.getCatCode().longValue() > catCodeMax.getCatCode().longValue()) {
          catCodeMax = categoryDTO;
        }
      }
      catCode = catCodeMax.getCatCode() + 1;// 类目编码最大的加1
      level = Integer.valueOf(catCodeMax.getFeatures().trim().split("=")[1]);
      sortValue = (short) (catCodeMax.getSortValue() + 1);
    }
    CategoryDTO categoryDTO = new CategoryDTO();
    categoryDTO.setCatCode(catCode);
    categoryDTO.setCatName(catName);
    categoryDTO.setCatType((short) 1);
    categoryDTO.setCreatorId(1);

    Short isLeaf = 0;
    if (level == 4) {
      isLeaf = 1;
    }
    String features = "l=" + level;
    categoryDTO.setFeatures(features);
    categoryDTO.setGmtCreate(new Date());
    categoryDTO.setGmtModified(new Date());
    categoryDTO.setLeaf(isLeaf);
    categoryDTO.setMemo(catName);
    categoryDTO.setParentId(parentId);
    categoryDTO.setSortValue(sortValue);
    categoryDTO.setStatus((short) 1);
    Result<Integer> catIdResult = stdcategoryservice.insertCategory(categoryDTO);
    if (null != categoryDTO && categoryDTO.getLeaf() == 1) {
      propertyInsertComponent.insertPropertyData(catIdResult.getModule(),catCode);
    }
    return catIdResult;
  }

  /**
   * 获取类目编码
   * 
   * @param parentId
   * @return
   */
  private Map<String, Object> getCategoryCode(Integer parentId) {
    Long subCatCode = 0L;
    String level = "0";
    Map<String, Object> map = Maps.newHashMap();
    if (parentId != 0) {
      Result<CategoryDTO> categoryResult = stdcategoryservice.queryCategoryById(parentId);
      logger.error("父类目：" + JackSonUtil.getJson(categoryResult));
      if (null != categoryResult && categoryResult.getSuccess()
          && null != categoryResult.getModule()) {
        CategoryDTO categoryDTO = categoryResult.getModule();
        String features = categoryDTO.getFeatures();
        if(null == features){
          features = "l=0";
        }
        Long catCode = categoryDTO.getCatCode();
        level = features.trim().split("=")[1];
        if (StringUtils.isBlank(level)) {
          map.put("subCatCode", subCatCode);
          map.put("level", "0");
          return map;
        }
        switch (level) {
          case "1":
            subCatCode = catCode * 1000 + 1L;
            break;
          case "2":
            subCatCode = catCode * 1000 + 1L;
            break;
          case "3":
            subCatCode = catCode * 10000 + 1L;
            break;
        }
      } else {
        subCatCode = 1L;
      }
    }
    map.put("subCatCode", subCatCode);
    map.put("level", level);
    return map;
  }
}
