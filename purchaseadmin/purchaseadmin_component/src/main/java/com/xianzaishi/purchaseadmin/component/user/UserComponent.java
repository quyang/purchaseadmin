package com.xianzaishi.purchaseadmin.component.user;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.CodeFactoryUtils;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.MD5Util;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.msg.domain.RpcResult;
import com.xianzaishi.purchaseadmin.client.base.vo.excel.ExcelEmployee;
import com.xianzaishi.purchaseadmin.client.base.vo.excel.ExcelSupplier;
import com.xianzaishi.purchaseadmin.client.bill.vo.BillQueryVO;
import com.xianzaishi.purchaseadmin.client.bill.vo.BillUpdateVO;
import com.xianzaishi.purchaseadmin.client.bill.vo.BillsInsertVO;
import com.xianzaishi.purchaseadmin.client.erpapp.vo.ERPQueryVO;
import com.xianzaishi.purchaseadmin.client.user.vo.EmployeeQueryVO;
import com.xianzaishi.purchaseadmin.client.user.vo.LogisticalInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderItemInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderListResultVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderListResultVO.RefoundGoodsType;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderReturnVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrderSVO;
import com.xianzaishi.purchaseadmin.client.user.vo.OrdersInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.QueryDataVO;
import com.xianzaishi.purchaseadmin.client.user.vo.ResetPwdUserVO;
import com.xianzaishi.purchaseadmin.client.user.vo.ResultVO;
import com.xianzaishi.purchaseadmin.client.user.vo.SkuInformationVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserCouponVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserOrderVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserQueryVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserVO;
import com.xianzaishi.purchaseadmin.component.inventory.InventoryComponent;
import com.xianzaishi.purchaseadmin.component.organizaion.OrganizationComponent;
import com.xianzaishi.purchaseadmin.component.pic.PicComponent;
import com.xianzaishi.purchasecenter.client.bill.BillService;
import com.xianzaishi.purchasecenter.client.bill.dto.BillDetailsDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillInformationDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillInsertDTO;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;
import com.xianzaishi.purchasecenter.client.role.RoleService;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.EmployeeService;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;
import com.xianzaishi.purchasecenter.client.user.dto.EmployeeDTO;
import com.xianzaishi.purchasecenter.client.user.dto.EmployeeDTO.EmployeeSexConstants;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierCompanyDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierContactDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO.SupplierStatusConstants;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierFinanceDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierLicensesDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierLicensesDTO.LicensesTypeConstants;
import com.xianzaishi.service.MessageService;
import com.xianzaishi.trade.client.CreditService;
import com.xianzaishi.trade.client.DeliveryService;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.vo.DeliveryVO;
import com.xianzaishi.trade.client.vo.LogisticalVO;
import com.xianzaishi.trade.client.vo.OrderDiscountVO;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.trade.client.vo.RefundDetailVO;
import com.xianzaishi.trade.client.vo.SkuInfoVO;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.DemicalUtils;
import com.xianzaishi.wms.common.utils.ListUtils;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionDomainClient;
import com.xianzaishi.wms.tmscore.vo.DistributionVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("userComponent")
public class UserComponent {

  @Autowired
  private BackGroundUserService backgrounduserservice;

  @Autowired
  private EmployeeService employeeservice;

  @Autowired
  private RoleService roleservice;

  @Autowired
  private SupplierService supplierservice;

  @Autowired
  private MessageService messageservice;

  @Autowired
  private OrganizationComponent organizationComponent;

  @Autowired
  private PicComponent picComponent;

  @Autowired
  private UserService userService;

  @Autowired
  private CreditService creditService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private DeliveryService deliveryService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private BillService billService;

  @Autowired
  private IDistributionDomainClient distributionDomainClient;

  @Autowired
  private StdCategoryService stdcategoryservice;

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private InventoryComponent inventoryComponent;


  private static final Logger logger = Logger.getLogger(UserComponent.class);

  public static final int ACTIVE_CODE_TIME = 10 * 60 * 1000;// 验证码使用有效期

  public static final String ACTIVE_CODE_DISPLAY_TIME = "5分钟"; // 验证码使用短信提醒时间

  public static final String ACTIVE_CODE_SPLIT = ";"; // 验证码分隔符

  private static final int LOGIN_EXPIRE_TIME = 20 * 60 * 1000;// 请求不能超过5分钟

  private static final int RESETPWD_EXPIRE_TIME = 10 * 60 * 1000;// 重置密码超时10分钟

  private static final int TIME_SCOPE = 2 * 30 * 60 * 1000;// 60分钟时差

  private static final int ONE_DAY = 24 * 60 * 60 * 1000;// 一天时差

  private static final String TEMPLATE_NAME = "SMS_45825035";

  public String addAllSupplier(File file) {
    List<ExcelSupplier> employeeList = null;
    try {
      employeeList = this.readSupplierExcel(file);
    } catch (Exception e) {
      e.printStackTrace();
      return "导入失败，读取供应商文件失败，请检查文件格式";
    }
    Result<Integer> result = null;
    String display = "";
    List<SupplierDTO> supplierDTOList = this.getSupplierList(employeeList);
    if (CollectionUtils.isNotEmpty(supplierDTOList)) {
      for (SupplierDTO supplier : supplierDTOList) {

        Result<SupplierDTO> queryResult =
            supplierservice.querySupplierByName(supplier.getName(), true);
        if (queryResult != null && queryResult.getSuccess()) {
          SupplierDTO sup = queryResult.getModule();
          display =
              display + "<br/>导入校验失败，已经存在同名供应商，供应商简介信息如下：<br/>供应商用户名：" + sup.getName() + "<br/>"
                  + "联系方式：" + sup.getPhone() + "<br/>" + "上级名称："
                  + sup.getSupplierContactDTO().getManagerName() + "<br/>" + "公司名称："
                  + sup.getCompanyName() + "<br/>" + "法人："
                  + sup.getSupplierCompanyDTO().getJuridicalPerson() + "<br/>";
          continue;
        }

        Result<BackGroundUserDTO> userInfo =
            backgrounduserservice.queryUserDTOByPhone(supplier.getPhone());
        if (null != userInfo && userInfo.getSuccess()) {
          queryResult = supplierservice.querySupplierById(userInfo.getModule().getUserId(), true);
          if (queryResult != null && queryResult.getSuccess()) {
            SupplierDTO sup = queryResult.getModule();
            display =
                display + "<br/>导入校验失败，已经存在同手机号供应商，供应商简介信息如下：<br/>供应商用户名：" + sup.getName()
                    + "<br/>" + "联系方式：" + sup.getPhone() + "<br/>" + "上级名称："
                    + sup.getSupplierContactDTO().getManagerName() + "<br/>" + "公司名称："
                    + sup.getCompanyName() + "<br/>" + "法人："
                    + sup.getSupplierCompanyDTO().getJuridicalPerson() + "<br/>";
            continue;
          }
        }
        result = supplierservice.insertSupplier(supplier);
        if (result != null && result.getSuccess()) {
          queryResult = supplierservice.querySupplierById(result.getModule(), true);
          if (null != queryResult && queryResult.getSuccess()) {
            SupplierDTO sup = queryResult.getModule();
            display =
                display + "<br/>导入成功，供应商简介信息如下：<br/>供应商用户名：" + sup.getName() + "<br/>" + "联系方式："
                    + sup.getPhone() + "<br/>" + "上级名称："
                    + sup.getSupplierContactDTO().getManagerName() + "<br/>" + "公司名称："
                    + sup.getCompanyName() + "<br/>" + "法人："
                    + sup.getSupplierCompanyDTO().getJuridicalPerson() + "<br/>";
          }
        } else {
          logger.error("Insert supplier failed,name:" + supplier.getName());
          return "导入失败，读取供应商文件失败，联系开发";
        }
      }
    }
    if (StringUtils.isNotEmpty(display)) {
      return display;
    }
    return "导入失败，读取供应商文件失败，请检查文件内容";
  }

  /**
   * 导入所有员工信息
   */
  public void addAllEmployee(String pathFlag, boolean isTest) {
    String path = "D:/work/input/employee.xls";
    if (pathFlag.equals("local")) {
      path = "D:/work/input/employee.xls";
    } else {
      path = "/usr/works/datainit/employee.xls";
    }
    List<ExcelEmployee> employeeList = null;
    try {
      employeeList = this.readEmployeeExcel(path);
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    Result<Integer> result = null;
    List<EmployeeDTO> employeeDTOList = this.getEmployeeList(employeeList);
    if (CollectionUtils.isNotEmpty(employeeDTOList)) {
      for (EmployeeDTO employee : employeeDTOList) {
        if (isTest) {
          logger.error("User info:" + employee.getName() + "," + employee.getNickname());
        } else {
          result = employeeservice.insertEmployee(employee);
        }
        if (result != null && result.getSuccess()) {
          logger.error("success,id:" + result.getModule());
        } else {
          logger.error("Insert supplier failed,name:" + employee.getName());
        }
      }
    }
  }

  /**
   * 登录成功逻辑，获取token返回前台<br/>
   * 登录失败，返回前台错误信息
   */
  public Result<Map<String, String>> login(UserVO userInfo) {

    logger.error("userInfo="+JackSonUtil.getJson(userInfo));
    if (null == userInfo) {
      logger.error("Login user is null");
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "login user not exit");
    }

    String equipment = userInfo.getEquipmentId();
    if (StringUtils.isEmpty(equipment)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_EQUIPMENT_INFO_NULL,
          "login input info error");
    }

    long dataInfo = userInfo.getSysTime();
    long now = new Date().getTime();
    // if (Math.abs(now - dataInfo) > LOGIN_EXPIRE_TIME) {
    // return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_TIMEOUT,
    // "login input info error");
    // }

    String sign = userInfo.getSign();
    if (StringUtils.isEmpty(sign)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_SIGN_NULL,
          "login input input info error");
    }

    String userName = userInfo.getUserName();
    BackGroundUserDTO dbUser = null;
    if (StringUtils.isNotEmpty(userName) && StringUtils.isNumeric(userName)) {
      long inputphone = Long.valueOf(userName);// 由于登录要支持用户手机号这种登录，但是外包前端只肯做一种，所以就通过这种简单的判断
      if (inputphone > 10000000000L && inputphone < 20000000000L) {// 手机号登录
        dbUser = getUserByPhone(inputphone);
      } else if (inputphone < 10000) {// 会员id登录
        dbUser = getUserById((int) inputphone);
      }
    } else {
      dbUser = getUserByName(userName);
      logger.error("姓名结果="+JackSonUtil.getJson(dbUser));
    }
    if (null == dbUser) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "login user not exit");
    }

    Short dbUserType =  dbUser.getUserType();
    Short userType = userInfo.getUserType();
    if(null != dbUserType && !dbUserType.equals(userType)){
      String errorMsg = (userInfo.getUserType().shortValue() == 0 ? "您不是鲜在时内部员工，请勿通过此登录方式登录" : "您还不是供应商，请勿通过此登录方式登录！");
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    String dbPwd = dbUser.getPwd();
    String dbSign = new StringBuilder().append(dbPwd).append(UserVO.SIGN_SPLIT).append(dataInfo)
        .append(UserVO.SIGN_SPLIT).append(equipment).toString();
    dbSign = MD5Util.getMD5Str(dbSign).toLowerCase();
    if (dbSign.equals(sign.toLowerCase())) {
      String token = this.getUuid();
      dbUser.setToken(token);
      dbUser.setHardwareCode(equipment);
      if (updateUser(dbUser)) {
//        OrganizationDTO orgInfo =
//            organizationComponent.getOrganizationDTOById(dbUser.getUserId());
        String company = "";
//        if (null != orgInfo) {
//          company = orgInfo.getName();
//        }
        logger.error("结果="+JackSonUtil.getJson(dbUser));
//        if (null != orgInfo) {
//          company = orgInfo.getName();
//        }
        Map<String, String> result = Maps.newHashMap();
        result.put("token", token);
        result.put("name", dbUser.getName());
        result.put("company", company);
        result.put("operatorId", dbUser.getUserId()+"");
        return Result.getSuccDataResult(result);
      } else {
        return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_GETTOKEN_ERROR,
            "Login get token failed");
      }
    } else {
      logger.error("Login sign is:" + sign);
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_SIGN_ERROR,
          "login input info error");
    }
  }

  /**
   * 用户退出逻辑
   */
  public Result<Boolean> logout(UserVO userVO) {
    if (null == userVO) {
      logger.error("Logout user is null");
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "logout user not exit");
    }
    String token = userVO.getToken();
    if (StringUtils.isEmpty(token)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_GETTOKEN_ERROR,
          "logout input info error");
    }
    String equipment = userVO.getEquipmentId();
    if (StringUtils.isEmpty(equipment)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_EQUIPMENT_INFO_NULL,
          "logout input info error");
    }

    long dataInfo = userVO.getSysTime();
    long now = new Date().getTime();
    if (!(Math.abs(now - dataInfo) < LOGIN_EXPIRE_TIME)) {
      // return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_TIMEOUT,
      // "logout input info error");
    }
    Result<BackGroundUserDTO> userResult = backgrounduserservice.queryUserDTOByToken(token);
    if (!userResult.getSuccess()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, userResult.getErrorMsg());
    }
    BackGroundUserDTO userDTO = userResult.getModule();
    if (null == userDTO || null == userDTO.getUserId()) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "user not exit");
    }
    if (!equipment.equals(userDTO.getHardwareCode())) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_USER_NOT_SUPPORT,
          "system not support login with one equipment but logout with another equipment");
    }
    userDTO.setToken(null);
    Result<Boolean> result = backgrounduserservice.updateUserDTO(userDTO);
    return result;
  }

  /**
   * 员工的登录逻辑，获取token返回前台<br/>
   * 登录失败，返回前台错误信息
   */
  public Result<BackGroundUserDTO> employeelogin(UserVO userInfo) {
    if (null == userInfo) {
      logger.error("Login user is null");
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "login user not exit");
    }

    String equipment = userInfo.getEquipmentId();
    if (StringUtils.isEmpty(equipment)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_EQUIPMENT_INFO_NULL,
          "login input info error");
    }

    long dataInfo = userInfo.getSysTime();
    long now = new Date().getTime();
    // if (Math.abs(now - dataInfo) > LOGIN_EXPIRE_TIME) {
    // return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_TIMEOUT,
    // "login input info error");
    // }

    String sign = userInfo.getSign();
    if (StringUtils.isEmpty(sign)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_SIGN_NULL,
          "login input input info error");
    }

    int userId = userInfo.getUserId();
    BackGroundUserDTO dbUser = null;
    if (userId > 0) {
      dbUser = getUserById(userId);
      if (null == dbUser) {
        return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
            "login user not exit by id");
      }
    }

    long phone = userInfo.getPhone();
    if (phone > 0) {
      dbUser = getUserByPhone(phone);
      if (null == dbUser) {
        return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
            "login user not exit by phone");
      }
    }

    if (!UserTypeConstants.USER_EMPLOYEE.equals(dbUser.getUserType())) {// 用户不是店内员工
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_USER_NOT_SUPPORT,
          "login user not exit");
    }

    String dbPwd = dbUser.getPwd();
    String dbSign =
        new StringBuilder().append(dbPwd).append(UserVO.SIGN_SPLIT).append(dataInfo)
            .append(UserVO.SIGN_SPLIT).append(equipment).toString();
    dbSign = MD5Util.getMD5Str(dbSign).toLowerCase();
    if (dbSign.equals(sign.toLowerCase())) {
      String token = this.getUuid();
      dbUser.setToken(token);
      dbUser.setHardwareCode(equipment);
      if (updateUser(dbUser)) {
        return Result.getSuccDataResult(dbUser);
      } else {
        return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_GETTOKEN_ERROR,
            "Login get token failed");
      }
    } else {
      logger.error("Login sign is:" + sign);
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_SIGN_ERROR,
          "login input info error");
    }
  }

  /**
   * @return
   */
  public Result<Boolean> resetUserPwdSendToken(String userName) {
    if (null == userName) {
      logger.error("resetuserpwd user is null");
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "resetuserpwd user not exit");
    }

    BackGroundUserDTO dbUser = getUserByName(userName);
    if (null == dbUser) {
      logger.error("resetuserpwd user is null,userName:" + userName);
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "resetuserpwd user not exit");
    }

    Long phone = dbUser.getPhone();
    if (null == phone || phone < 0) {
      logger.error("resetuserpwd user phone is null,userName:" + userName);
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_INFO_ERROR,
          "resetuserpwd user phone is null");
    }

    String checkCode = CodeFactoryUtils.createCheckCode();

    Map<String, String> checkCodeMap = Maps.newHashMap();
    checkCodeMap.put("code", checkCode);// 验证码
    RpcResult<Boolean> sendResult =
        messageservice.sendSmsMessage(String.valueOf(phone), TEMPLATE_NAME, checkCodeMap);
    if (null != sendResult && sendResult.isSuccess()) {
      String activeCode = this.joinActiveCode(checkCode);
      dbUser.setActiveCode(activeCode);
      if (!updateUser(dbUser)) {
        logger.error("resetuserpwd user update db failed,userId:" + dbUser.getUserId()
            + ",activeCode:" + activeCode);
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            "resetuserpwd set user activeCode failed, please retry.");
      } else {
        return Result.getSuccDataResult(true);
      }
    }
    logger.error("resetuserpwd user send msg failed,userId:" + dbUser.getUserId());
    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, sendResult.getMsg());
  }

  /***
   * 重置密码并且登录
   * 
   * @param userInfo
   * @return
   */
  public Result<String> resetPwdAndLogin(ResetPwdUserVO userInfo) {
    if (null == userInfo) {
      logger.error("resetPwdAndLogin user is null");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "resetPwdAndLogin user not exit");
    }

    String pwd = userInfo.getPwd();
    if (StringUtils.isEmpty(pwd)) {
      logger.error("resetPwdAndLogin pwd is null");
      return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_PWD_NULL,
          "resetPwdAndLogin input info error");
    }

    String activecode = userInfo.getToken();
    if (StringUtils.isEmpty(activecode)) {
      logger.error("resetPwdAndLogin token is null");
      return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_TOKEN_NULL,
          "resetPwdAndLogin input info error");
    }

    String equipment = userInfo.getEquipmentId();
    if (StringUtils.isEmpty(equipment)) {
      logger.error("resetPwdAndLogin equipment is null");
      return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE__EQUIPMENT_NULL,
          "resetPwdAndLogin input info error");
    }

    long dataInfo = userInfo.getSysTime();
    long now = new Date().getTime();
    // if (Math.abs(now - dataInfo) > LOGIN_EXPIRE_TIME) {
    // logger.error("resetPwdAndLogin post pwd info timeout");
    // return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_TIMEOUT,
    // "resetPwdAndLogin input info error");
    // }

    String sign = userInfo.getSign();
    if (StringUtils.isEmpty(sign)) {
      logger.error("resetPwdAndLogin sign is null");
      return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_SIGN_NULL,
          "resetPwdAndLogin input info error");
    }

    String userName = userInfo.getUserName();
    BackGroundUserDTO dbUser = getUserByName(userName);
    if (null == dbUser) {
      logger.error("resetPwdAndLogin login user not exit");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "login user not exit");
    }

    String dbActiveCodePV[] = this.splitActiveCode(dbUser.getActiveCode());
    if (null != dbActiveCodePV && dbActiveCodePV.length >= 2
        && StringUtils.isNumeric(dbActiveCodePV[1])) {
      String activeCodeStr = dbActiveCodePV[0];
      long activeSysTime = Long.valueOf(dbActiveCodePV[1]);

      if (!MD5Util.getMD5Str(activeCodeStr).equalsIgnoreCase(activecode)) {
        logger.error("resetPwdAndLogin input activeCode is wrong");
        return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_TOKEN_ERROR,
            "resetPwdAndLogin input info error");
      }

      if (Math.abs(now - activeSysTime) > RESETPWD_EXPIRE_TIME) {
        // logger.error("resetPwdAndLogin input token timeout");
        // return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_TOKEN_TIMEOUT,
        // "resetPwdAndLogin input info error");
      }
    }

    String signStr =
        new StringBuilder().append(pwd).append("_").append(activecode).append("_").append(dataInfo)
            .append("_").append(equipment).toString();
    String md5Sign = MD5Util.getMD5Str(signStr);
    if (!md5Sign.equals(sign.toLowerCase())) {
      logger.error("resetPwdAndLogin input sign error");
      return Result.getErrDataResult(ServerResultCode.RESETPWD_ERROR_CODE_SIGN_ERROR,
          "resetPwdAndLogin input info error");
    }

    String token = this.getUuid();
    dbUser.setToken(token);
    dbUser.setPwd(pwd);
    dbUser.setHardwareCode(equipment);
    if (updateUser(dbUser)) {
      return Result.getSuccDataResult(token);
    } else {
      logger.error("resetPwdAndLogin login update set token");
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_GETTOKEN_ERROR,
          "resetPwdAndLogin login get token failed");
    }
  }

  /**
   * 获取激活码
   */
  private String joinActiveCode(String checkCode) {
    if (null == checkCode) {
      return null;
    }
    StringBuffer activeCode = new StringBuffer();
    activeCode.append(checkCode);
    activeCode.append(ACTIVE_CODE_SPLIT);// 以;为分隔符
    long date = new Date().getTime();// 当前发送验证码时间
    activeCode.append(date);
    return activeCode.toString();
  }

  /**
   * 获取激活码
   */
  private String[] splitActiveCode(String checkCode) {
    if (null == checkCode) {
      return null;
    }
    return checkCode.split(ACTIVE_CODE_SPLIT);
  }

  /**
   * 获取用户信息
   */
  public BackGroundUserDTO getUserByName(String userName) {
    Result<BackGroundUserDTO> user = backgrounduserservice.queryUserDTOByName(userName);
    BackGroundUserDTO userDTO;
    if (null == user || !user.getSuccess() || null == user.getModule()) {

      userDTO = getUserByPhone(userName);
    } else {
      userDTO = user.getModule();
    }
    return userDTO;
  }

  /**
   * 获取用户信息
   */
  public BackGroundUserDTO getUserByPhone(String phone) {
    if (StringUtils.isEmpty(phone) || !StringUtils.isNumeric(phone)) {
      return null;
    }
    Result<BackGroundUserDTO> user = backgrounduserservice.queryUserDTOByPhone(Long.valueOf(phone));
    if (null == user || !user.getSuccess()) {
      return null;
    } else {
      return user.getModule();
    }
  }

  /**
   * 获取用户信息
   */
  public BackGroundUserDTO getUserById(int userId) {
    Result<BackGroundUserDTO> user = backgrounduserservice.queryUserDTOById(userId);
    if (null == user || !user.getSuccess()) {
      return null;
    } else {
      return user.getModule();
    }
  }

  /**
   * 获取用户信息
   */
  public BackGroundUserDTO getUserByPhone(long phone) {
    Result<BackGroundUserDTO> user = backgrounduserservice.queryUserDTOByPhone(phone);
    if (null == user || !user.getSuccess()) {
      return null;
    } else {
      return user.getModule();
    }
  }

  /**
   * 获取用户信息
   */
  public BackGroundUserDTO getUserByToken(String token) {
    Result<BackGroundUserDTO> user = backgrounduserservice.queryUserDTOByToken(token);
    if (null == user || !user.getSuccess()) {
      return null;
    } else {
      return user.getModule();
    }
  }

  /**
   * 获取用户信息
   */
  public List<BackGroundUserDTO> getUserByIdList(List<Integer> userIdList) {
    Result<List<BackGroundUserDTO>> user = backgrounduserservice.queryUserDTOByIdList(userIdList);
    if (null == user || !user.getSuccess()) {
      return Lists.newArrayList();
    } else {
      return user.getModule();
    }
  }

  /**
   * 更新用户信息
   */
  public boolean updateUser(BackGroundUserDTO dbUser) {
    Result<Boolean> updateResult = backgrounduserservice.updateUserDTO(dbUser);
    if (null == updateResult || !updateResult.getSuccess() || !updateResult.getModule()) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * 获取供应商
   */
  public SupplierDTO getSupplierById(int userId) {
    Result<SupplierDTO> supplierResult = supplierservice.querySupplierById(userId, true);
    if (null != supplierResult && supplierResult.getSuccess() && null != supplierResult.getModule()) {
      return supplierResult.getModule();
    }
    return null;
  }

  /**
   * 获取列表 信息
   */
  public List<RoleDTO> getPageRequireRole(String pageId) {
    Result<List<RoleDTO>> result = roleservice.queryRoleList(pageId);
    if (result.getSuccess()) {
      return result.getModule();
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * 给页面增加权限
   */
  public Result<Boolean> addPageRequireRole(String pageId, String role) {
    Result<RoleDTO> roleResult = roleservice.queryRoleByName(role);
    if (null != roleResult && roleResult.getSuccess() && null != roleResult.getModule()
        && null != roleResult.getModule().getRoleId()) {
      return roleservice.addRole(pageId, roleResult.getModule().getRoleId());
    } else {
      return Result.getErrDataResult(-1, "权限不存在");
    }
  }

  /**
   * 给用户加权限
   */
  public int addUserRole(String roleName, String userName) {
    BackGroundUserDTO user = this.getUserByName(userName);
    if (null == user) {
      return -1;
    }
    if (!UserTypeConstants.USER_EMPLOYEE.equals(user.getUserType())) {
      return -2;
    }
    int newRoleId = this.addRole(roleName);
    user.addRole(newRoleId);
    boolean result = this.updateUser(user);
    if (result) {
      return 1;
    } else {
      return -3;
    }
  }

  /**
   * 按照token取名称
   */
  public String getOrganizationName(String token) {
    if (StringUtils.isEmpty(token)) {
      return "";
    }
    OrganizationDTO organization = organizationComponent.getOrganizationDTOByUserToken(token);
    if (null != organization) {
      return organization.getName();
    }
    return "";
  }


  private String getUuid() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString().replaceAll("-", "").toLowerCase();
  }

  /**
   * 读取一个excel文件，转化为可用对象
   */
  private List<ExcelSupplier> readSupplierExcel(File file) throws Exception {
    POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream(file));
    HSSFWorkbook hssfWorkbook = new HSSFWorkbook(poifsFileSystem);
    HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

    Map<Integer, List<HSSFPicture>> yingyezhizhaoMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> zuzhijigoudaimaMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> shuiwudengjiMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> kaihuhangxukezhengMap = Maps.newHashMap();

    Map<Integer, List<HSSFPicture>> shengchanxukezhengMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> guojitiaomazhengMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> shipingliutongxukezhengMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> otherxukezhengMap = Maps.newHashMap();

    int yingyezhizhaoColl = 29;// 营业执照主图在哪一列
    int zuzhijigoudaimaColl = 33;// 组织机构代码图在哪一列
    int shuiwudengjiColl = 37;// 税务登记图在哪一列
    int kaihuhangxukezhengColl = 41;// 开户行许可证图在哪一列
    int shengchanxukezhengColl = 45;// 生产许可证登记图在哪一列
    int guojitiaomazhengColl = 49;// 国际条码证图在哪一列
    int shipingliutongxukezhengColl = 53;// 食品流通许可证图在哪一列
    int otherxukezhengColl = 57;// 其它证件图在哪一列

    List<ExcelSupplier> result = Lists.newArrayList();
    if (null != hssfSheet && null != hssfSheet.getDrawingPatriarch()
        && null != hssfSheet.getDrawingPatriarch().getChildren()) {
      for (HSSFShape shape : hssfSheet.getDrawingPatriarch().getChildren()) {
        HSSFClientAnchor anchor = (HSSFClientAnchor) shape.getAnchor();
        if (shape instanceof HSSFPicture) {
          HSSFPicture pic = (HSSFPicture) shape;
          int row = anchor.getRow1();
          int coll = anchor.getCol1();

          if (coll == yingyezhizhaoColl) {
            List<HSSFPicture> picL = yingyezhizhaoMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            yingyezhizhaoMap.put(row, picL);
          } else if (coll == zuzhijigoudaimaColl) {
            List<HSSFPicture> picL = zuzhijigoudaimaMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            zuzhijigoudaimaMap.put(row, picL);
          } else if (coll == shuiwudengjiColl) {
            List<HSSFPicture> picL = shuiwudengjiMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            shuiwudengjiMap.put(row, picL);
          } else if (coll == kaihuhangxukezhengColl) {
            List<HSSFPicture> picL = kaihuhangxukezhengMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            kaihuhangxukezhengMap.put(row, picL);
          } else if (coll == shengchanxukezhengColl) {
            List<HSSFPicture> picL = shengchanxukezhengMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            shengchanxukezhengMap.put(row, picL);
          } else if (coll == guojitiaomazhengColl) {
            List<HSSFPicture> picL = guojitiaomazhengMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            guojitiaomazhengMap.put(row, picL);
          } else if (coll == shipingliutongxukezhengColl) {
            List<HSSFPicture> picL = shipingliutongxukezhengMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            shipingliutongxukezhengMap.put(row, picL);
          } else if (coll == otherxukezhengColl) {
            List<HSSFPicture> picL = otherxukezhengMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            otherxukezhengMap.put(row, picL);
          }
        }
      }
    }

    int rowstart = hssfSheet.getFirstRowNum() + 2;
    int rowEnd = hssfSheet.getLastRowNum();
    for (int i = rowstart; i <= rowEnd; i++) {
      HSSFRow row = hssfSheet.getRow(i);
      if (null == row) {
        continue;
      }
      // 除去页头
      int cellStart = row.getFirstCellNum();
      int cellEnd = row.getLastCellNum();

      ExcelSupplier supplier = new ExcelSupplier();
      for (int k = cellStart; k <= cellEnd; k++) {
        HSSFCell cell = row.getCell(k);
        if (null == cell) {
          supplier.addProperty(null);
          continue;
        } else {
          cell.setCellType(Cell.CELL_TYPE_STRING);
          supplier.addProperty(cell.getStringCellValue());
        }
      }
      supplier.setYingyezhizhaoList(yingyezhizhaoMap.get(i));
      supplier.setZuzhijigoudaimaList(zuzhijigoudaimaMap.get(i));
      supplier.setShuiwudengjiList(shuiwudengjiMap.get(i));
      supplier.setKaihuhangxukezhengList(kaihuhangxukezhengMap.get(i));

      supplier.setShengchanxukezhengList(shengchanxukezhengMap.get(i));
      supplier.setGuojitiaomazhengList(guojitiaomazhengMap.get(i));
      supplier.setShipingliutongxukezhengList(shipingliutongxukezhengMap.get(i));
      supplier.setOtherxukezhengList(otherxukezhengMap.get(i));
      if (null != supplier.getProperty() && supplier.getProperty().size() > 20) {
        result.add(supplier);
      }
    }
    return result;
  }


  /**
   * 读取一个excel文件，转化为可用对象
   */
  private List<ExcelEmployee> readEmployeeExcel(String path) throws Exception {
    File file = new File(path);
    POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream(file));
    HSSFWorkbook hssfWorkbook = new HSSFWorkbook(poifsFileSystem);
    HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

    List<ExcelEmployee> result = Lists.newArrayList();

    int rowstart = hssfSheet.getFirstRowNum() + 1;
    int rowEnd = hssfSheet.getLastRowNum();
    for (int i = rowstart; i <= rowEnd; i++) {
      HSSFRow row = hssfSheet.getRow(i);
      if (null == row) {
        continue;
      }

      int cellStart = row.getFirstCellNum();
      int cellEnd = row.getLastCellNum();

      ExcelEmployee employee = new ExcelEmployee();
      for (int k = cellStart; k <= cellEnd; k++) {
        HSSFCell cell = row.getCell(k);
        if (null == cell) {
          employee.addProperty(null);
          continue;
        } else {
          cell.setCellType(Cell.CELL_TYPE_STRING);
          employee.addProperty(cell.getStringCellValue());
        }
      }
      result.add(employee);
    }
    return result;
  }


  private List<SupplierDTO> getSupplierList(List<ExcelSupplier> supplierList) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    List<SupplierDTO> result = Lists.newArrayList();
    Date now = new Date();
    for (ExcelSupplier supplier : supplierList) {
      SupplierDTO tmpResult = new SupplierDTO();
      tmpResult.setName(supplier.getProperty().get(0));
      tmpResult.setUserType(UserTypeConstants.USER_SUPPLIER);
      tmpResult.setGmtCreate(now);
      tmpResult.setGmtModified(now);
      tmpResult.setPurchasingAgent(42);
      tmpResult.setPwd(UUID.randomUUID().toString().replace("-", ""));
      String cmpname = supplier.getProperty().get(13);
      if (StringUtils.isEmpty(cmpname)) {
        cmpname = tmpResult.getName();
      }
      tmpResult.setCompanyName(cmpname);
      tmpResult.setStatus(SupplierStatusConstants.SUPPLIER_STATUS_PASS);
      tmpResult.addRole(42);
      if (StringUtils.isNotEmpty(supplier.getProperty().get(1))
          && StringUtils.isNumeric(supplier.getProperty().get(1))) {
        tmpResult.setPhone(Long.valueOf(supplier.getProperty().get(1)));
      } else {
        logger.error("input user phone is:" + supplier.getProperty().get(1));
        continue;
      }

      SupplierContactDTO tmpContact = new SupplierContactDTO();
      tmpContact.setContactName(supplier.getProperty().get(2));
      if (StringUtils.isNotEmpty(supplier.getProperty().get(3))
          && StringUtils.isNumeric(supplier.getProperty().get(3))) {
        tmpContact.setContactPhoneBk(Long.valueOf(supplier.getProperty().get(3)));
      } else {
        logger.error("input user contactPhoneBk is:" + supplier.getProperty().get(3));
        continue;
      }
      tmpContact.setContactMail(supplier.getProperty().get(4));
      if (StringUtils.isNotEmpty(supplier.getProperty().get(5))
          && StringUtils.isNumeric(supplier.getProperty().get(5))) {
        tmpContact.setContactQq(Long.valueOf(supplier.getProperty().get(5)));
      }
      tmpContact.setManagerName(supplier.getProperty().get(6));
      if (StringUtils.isNotEmpty(supplier.getProperty().get(7))
          && StringUtils.isNumeric(supplier.getProperty().get(7))) {
        tmpContact.setManagerPhone(Long.valueOf(supplier.getProperty().get(7)));
      } else {
        logger.error("input managerPhone is:" + supplier.getProperty().get(7));
        continue;
      }
      if (StringUtils.isNotEmpty(supplier.getProperty().get(8))
          && StringUtils.isNumeric(supplier.getProperty().get(8))) {
        tmpContact.setManagerPhoneBk(Long.valueOf(supplier.getProperty().get(8)));
      }

      SupplierCompanyDTO tmpCompany = new SupplierCompanyDTO();
      if (StringUtils.isNotEmpty(supplier.getProperty().get(9))
          && StringUtils.isNumeric(supplier.getProperty().get(9))) {
        tmpCompany.setSupplierType(Short.valueOf(supplier.getProperty().get(9)));
      }

      if (StringUtils.isNotEmpty(supplier.getProperty().get(10))
          && StringUtils.isNumeric(supplier.getProperty().get(10))) {
        tmpCompany.setCompanyType(Short.valueOf(supplier.getProperty().get(10)));
      }

      if (StringUtils.isNotEmpty(supplier.getProperty().get(11))
          && StringUtils.isNumeric(supplier.getProperty().get(11))) {
        tmpCompany.setMarketingType(Short.valueOf(supplier.getProperty().get(11)));
      }

      if (StringUtils.isNotEmpty(supplier.getProperty().get(12))
          && StringUtils.isNumeric(supplier.getProperty().get(12))) {
        tmpCompany.setDeliveryType(Short.valueOf(supplier.getProperty().get(12)));
      }

      tmpCompany.setCompanyName(cmpname);
      tmpCompany.setCompanyAddress(supplier.getProperty().get(14));
      tmpCompany.setCompanyPostcode(supplier.getProperty().get(15));
      tmpCompany.setCompanyFax(supplier.getProperty().get(16));
      tmpCompany.setCompanyEmail(supplier.getProperty().get(17));
      tmpCompany.setCompanyPhone(supplier.getProperty().get(18));
      tmpCompany.setJuridicalPerson(supplier.getProperty().get(19));

      SupplierFinanceDTO tmpFinanceDTO = new SupplierFinanceDTO();
      tmpFinanceDTO.setBankInfo(supplier.getProperty().get(20));
      tmpFinanceDTO.setBankName(supplier.getProperty().get(21));
      tmpFinanceDTO.setBankAccount(supplier.getProperty().get(22));
      tmpFinanceDTO.setInvoice(supplier.getProperty().get(23));
      tmpFinanceDTO.setAlipayAccount(supplier.getProperty().get(24));
      tmpFinanceDTO.setAlipayName(supplier.getProperty().get(25));

      SupplierLicensesDTO license1 = new SupplierLicensesDTO();
      license1.setLicensesType(LicensesTypeConstants.BUSINESS_LICENSES);
      license1.setLicensesCode(supplier.getProperty().get(26));
      license1.setGmtCreate(now);
      license1.setGmtModified(now);
      try {
        license1.setLicensesBegin(df.parse(supplier.getProperty().get(27)));
        if (StringUtils.isNotEmpty(supplier.getProperty().get(28))) {
          license1.setLicensesEnd(df.parse(supplier.getProperty().get(28)));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      if (CollectionUtils.isNotEmpty(supplier.getYingyezhizhaoList())) {
        Result<List<String>> upYingyezhizhaoResult =
            picComponent.updatePic(supplier.getYingyezhizhaoList(), null, 6);
        if (upYingyezhizhaoResult.getSuccess()) {
          license1.setLicensesPicList(upYingyezhizhaoResult.getModule());
        } else {
          logger.error("update user getYingyezhizhaoList failed:" + tmpResult.getName());
          continue;
        }
      }

      SupplierLicensesDTO license2 = null;
      if (supplier.getProperty().size() >= 31
          && StringUtils.isNotEmpty(supplier.getProperty().get(30))) {
        license2 = new SupplierLicensesDTO();

        license2.setLicensesType(LicensesTypeConstants.ORGANIZATION_CODE);
        license2.setLicensesCode(supplier.getProperty().get(30));
        license2.setGmtCreate(now);
        license2.setGmtModified(now);
        try {
          if (StringUtils.isNotEmpty(supplier.getProperty().get(31))) {
            license2.setLicensesBegin(df.parse(supplier.getProperty().get(31)));
          }
          if (StringUtils.isNotEmpty(supplier.getProperty().get(32))) {
            license2.setLicensesEnd(df.parse(supplier.getProperty().get(32)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getZuzhijigoudaimaList())) {
          Result<List<String>> upZuzhijigoudaimaResult =
              picComponent.updatePic(supplier.getZuzhijigoudaimaList(), null, 6);
          if (upZuzhijigoudaimaResult.getSuccess()) {
            license2.setLicensesPicList(upZuzhijigoudaimaResult.getModule());
          } else {
            logger.error("update user getZuzhijigoudaimaList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      SupplierLicensesDTO license3 = null;
      if (supplier.getProperty().size() >= 35
          && StringUtils.isNotEmpty(supplier.getProperty().get(34))) {
        license3 = new SupplierLicensesDTO();
        license3.setLicensesType(LicensesTypeConstants.TAX_REGISTRATION);
        license3.setLicensesCode(supplier.getProperty().get(34));
        license3.setGmtCreate(now);
        license3.setGmtModified(now);
        try {
          if (StringUtils.isNotEmpty(supplier.getProperty().get(35))) {
            license3.setLicensesBegin(df.parse(supplier.getProperty().get(35)));
          }
          if (StringUtils.isNotEmpty(supplier.getProperty().get(36))) {
            license3.setLicensesEnd(df.parse(supplier.getProperty().get(36)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getShuiwudengjiList())) {
          Result<List<String>> upShuiwudengjiResult =
              picComponent.updatePic(supplier.getShuiwudengjiList(), null, 6);
          if (upShuiwudengjiResult.getSuccess()) {
            license3.setLicensesPicList(upShuiwudengjiResult.getModule());
          } else {
            logger.error("update user getShuiwudengjiList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      SupplierLicensesDTO license4 = null;
      if (supplier.getProperty().size() >= 39
          && StringUtils.isNotEmpty(supplier.getProperty().get(38))) {
        license4 = new SupplierLicensesDTO();
        license4.setLicensesType(LicensesTypeConstants.ACCOUNT_PERMIT);
        license4.setLicensesCode(supplier.getProperty().get(38));
        license4.setGmtCreate(now);
        license4.setGmtModified(now);
        try {
          license4.setLicensesBegin(df.parse(supplier.getProperty().get(39)));
          if (StringUtils.isNotEmpty(supplier.getProperty().get(40))) {
            license4.setLicensesEnd(df.parse(supplier.getProperty().get(40)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getKaihuhangxukezhengList())) {
          Result<List<String>> upKaihuhangxukezhengResult =
              picComponent.updatePic(supplier.getKaihuhangxukezhengList(), null, 6);
          if (upKaihuhangxukezhengResult.getSuccess()) {
            license4.setLicensesPicList(upKaihuhangxukezhengResult.getModule());
          } else {
            logger.error("update user getKaihuhangxukezhengList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      SupplierLicensesDTO license5 = null;
      if (supplier.getProperty().size() >= 43
          && StringUtils.isNotEmpty(supplier.getProperty().get(42))) {
        license5 = new SupplierLicensesDTO();
        license5.setLicensesType(LicensesTypeConstants.QS);
        license5.setLicensesCode(supplier.getProperty().get(42));
        license5.setGmtCreate(now);
        license5.setGmtModified(now);
        try {
          license5.setLicensesBegin(df.parse(supplier.getProperty().get(43)));
          if (StringUtils.isNotEmpty(supplier.getProperty().get(44))) {
            license5.setLicensesEnd(df.parse(supplier.getProperty().get(44)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getShengchanxukezhengList())) {
          Result<List<String>> upShengchanxukezhengResult =
              picComponent.updatePic(supplier.getShengchanxukezhengList(), null, 6);
          if (upShengchanxukezhengResult.getSuccess()) {
            license5.setLicensesPicList(upShengchanxukezhengResult.getModule());
          } else {
            logger.error("update user getShengchanxukezhengList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      SupplierLicensesDTO license6 = null;
      if (supplier.getProperty().size() >= 47
          && StringUtils.isNotEmpty(supplier.getProperty().get(46))) {
        license6 = new SupplierLicensesDTO();
        license6.setLicensesType(LicensesTypeConstants.BAR_CODE);
        license6.setLicensesCode(supplier.getProperty().get(46));
        license6.setGmtCreate(now);
        license6.setGmtModified(now);
        try {
          license6.setLicensesBegin(df.parse(supplier.getProperty().get(47)));
          if (StringUtils.isNotEmpty(supplier.getProperty().get(48))) {
            license6.setLicensesEnd(df.parse(supplier.getProperty().get(48)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getGuojitiaomazhengList())) {
          Result<List<String>> upGuojitiaomazhengResult =
              picComponent.updatePic(supplier.getGuojitiaomazhengList(), null, 6);
          if (upGuojitiaomazhengResult.getSuccess()) {
            license6.setLicensesPicList(upGuojitiaomazhengResult.getModule());
          } else {
            logger.error("update user getGuojitiaomazhengList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      SupplierLicensesDTO license7 = null;
      if (supplier.getProperty().size() >= 51
          && StringUtils.isNotEmpty(supplier.getProperty().get(50))) {
        license7 = new SupplierLicensesDTO();
        license7.setLicensesType(LicensesTypeConstants.FOOD_PERMIT);
        license7.setLicensesCode(supplier.getProperty().get(50));
        license7.setGmtCreate(now);
        license7.setGmtModified(now);
        try {
          license7.setLicensesBegin(df.parse(supplier.getProperty().get(51)));
          if (StringUtils.isNotEmpty(supplier.getProperty().get(52))) {
            license7.setLicensesEnd(df.parse(supplier.getProperty().get(52)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getShipingliutongxukezhengList())) {
          Result<List<String>> upShipingliutongxukezhengResult =
              picComponent.updatePic(supplier.getShipingliutongxukezhengList(), null, 6);
          if (upShipingliutongxukezhengResult.getSuccess()) {
            license7.setLicensesPicList(upShipingliutongxukezhengResult.getModule());
          } else {
            logger
                .error("update user getShipingliutongxukezhengList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      SupplierLicensesDTO license8 = null;
      if (supplier.getProperty().size() >= 55
          && StringUtils.isNotEmpty(supplier.getProperty().get(54))) {
        license8 = new SupplierLicensesDTO();
        license8.setLicensesType(LicensesTypeConstants.OTHER);
        license8.setLicensesCode(supplier.getProperty().get(54));
        license8.setGmtCreate(now);
        license8.setGmtModified(now);
        try {
          if (StringUtils.isNotEmpty(supplier.getProperty().get(55))) {
            license8.setLicensesBegin(df.parse(supplier.getProperty().get(55)));
          }
          if (StringUtils.isNotEmpty(supplier.getProperty().get(56))) {
            license8.setLicensesEnd(df.parse(supplier.getProperty().get(56)));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(supplier.getOtherxukezhengList())) {
          Result<List<String>> upOtherxukezhengResult =
              picComponent.updatePic(supplier.getOtherxukezhengList(), null, 6);
          if (upOtherxukezhengResult.getSuccess()) {
            license8.setLicensesPicList(upOtherxukezhengResult.getModule());
          } else {
            logger.error("update user getOtherxukezhengList failed:" + tmpResult.getName());
            continue;
          }
        }
      }

      List<SupplierLicensesDTO> licenseList = Lists.newArrayList();
      if (null != license1) {
        licenseList.add(license1);
      }
      if (null != license2) {
        licenseList.add(license2);
      }
      if (null != license3) {
        licenseList.add(license3);
      }
      if (null != license4) {
        licenseList.add(license4);
      }
      if (null != license5) {
        licenseList.add(license5);
      }
      if (null != license6) {
        licenseList.add(license6);
      }
      if (null != license7) {
        licenseList.add(license7);
      }
      if (null != license8) {
        licenseList.add(license8);
      }

      tmpResult.setSupplierLicensesDTOList(licenseList);
      tmpResult.setSupplierFinanceDTO(tmpFinanceDTO);
      tmpResult.setSupplierContactDTO(tmpContact);
      tmpResult.setSupplierCompanyDTO(tmpCompany);
      result.add(tmpResult);
    }
    return result;
  }

  private List<EmployeeDTO> getEmployeeList(List<ExcelEmployee> employeeList) {
    List<EmployeeDTO> result = Lists.newArrayList();

    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    Date now = new Date();
    for (ExcelEmployee employee : employeeList) {
      EmployeeDTO tmpResult = new EmployeeDTO();
      tmpResult.setName(employee.getProperty().get(0));
      tmpResult.setUserType(UserTypeConstants.USER_EMPLOYEE);
      tmpResult.setGmtCreate(now);
      tmpResult.setGmtModified(now);
      tmpResult.setPwd(UUID.randomUUID().toString().replace("-", ""));
      tmpResult.setOrganizationId(1);
      int roleId = this.addRole(employee.getProperty().get(1));
      if (roleId > 0) {
        tmpResult.addRole(roleId);
      }
      if (StringUtils.isNotEmpty(employee.getProperty().get(2))
          && StringUtils.isNumeric(employee.getProperty().get(2))) {
        tmpResult.setPhone(Long.valueOf(employee.getProperty().get(2)));
      } else {
        logger.error("input user phone is:" + employee.getProperty().get(1));
        continue;
      }
      tmpResult.setNickname(employee.getProperty().get(3));
      try {
        tmpResult.setJoiningTime(df.parse(employee.getProperty().get(4)));
      } catch (Exception e) {
        e.printStackTrace();
      }
      tmpResult.setHujiAddress(employee.getProperty().get(5));
      tmpResult.setAddress(employee.getProperty().get(6));
      if ("女".equals(employee.getProperty().get(7))) {
        tmpResult.setSex(EmployeeSexConstants.EMPLOYEE_FAMALE);
      } else {
        tmpResult.setSex(EmployeeSexConstants.EMPLOYEE_MALE);
      }
      tmpResult.setSocialId(employee.getProperty().get(8));
      long managerPhone = 0;
      if (StringUtils.isNotEmpty(employee.getProperty().get(9))
          && StringUtils.isNumeric(employee.getProperty().get(9))) {
        managerPhone = Long.valueOf(employee.getProperty().get(9));
      } else {
        logger.error("input manager phone is:" + employee.getProperty().get(1));
        continue;
      }
      BackGroundUserDTO manager = this.getUserByPhone(managerPhone);
      if (null == manager || !UserTypeConstants.USER_EMPLOYEE.equals(manager.getUserType())) {
        logger.error("input user get manager null,manager phone is:"
            + employee.getProperty().get(9));
        continue;
      }
      tmpResult.setManagerId(manager.getUserId());
      tmpResult.setEmail(employee.getProperty().get(10));
      result.add(tmpResult);
    }
    return result;
  }

  private int addRole(String role) {
    int roleId = 0;
    Date now = new Date();
    Result<RoleDTO> roleResult = roleservice.queryRoleByName(role);
    if (null != roleResult && roleResult.getSuccess()) {
      roleId = roleResult.getModule().getRoleId();
    } else {
      RoleDTO roleDto = new RoleDTO();
      roleDto.setRoleName(role);
      roleDto.setIntroduction(role);
      roleDto.setGmtCreate(now);
      roleDto.setGmtModified(now);
      Result<Integer> insertResult = roleservice.insertRole(roleDto);
      if (insertResult != null && insertResult.getSuccess()) {
        roleId = insertResult.getModule();
      } else {
        logger.error("Add role failed");
        return -1;
      }
    }
    return roleId;
  }

  public String queryUserInfo(long phone) {
    Result<? extends BaseUserDTO> userResult = userService.queryUserByPhone(phone, true);
    if (null != userResult && userResult.getSuccess()) {
      if (userResult.getModule() instanceof UserDTO) {
        UserDTO user = (UserDTO) userResult.getModule();
        return user.getActiveCode();
      }
    }
    return "用户查询不存在";
  }

  /**
   * 查询用户信息
   */
  public UserDTO getUserInfo(Long phone) {
    Result<? extends BaseUserDTO> userResult = null;
    try {
      userResult = userService.queryUserByPhone(phone, true);
      if (null != userResult && userResult.getSuccess()) {
        return (UserDTO) userResult.getModule();
      }
      return null;
    } catch (Exception e) {
      logger.error("调用用户服务失败");
      return null;
    }

  }

  /**
   * 获取用户积分
   */
  public Integer getUserCredit(Long userId) {
    com.xianzaishi.trade.client.Result<Integer> userCredit = creditService.getUserCredit(userId);
    if (userCredit.isSuccess() && null != userCredit.getModel()) {
      return userCredit.getModel();
    }
    return null;
  }


  /**
   * 获取用户订单列表
   * 
   * @return 订单列表
   */
  public Result<OrdersInfoVO> getOrderList(UserQueryVO userQueryVO) {
    if (null == userQueryVO || null == userQueryVO.getUid() || (long) userQueryVO.getUid() <= 0
        || null == userQueryVO.getPageSize() || null == userQueryVO.getCurrentPage()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "获取订单列表失败，参数不正确");
    }

    List<OrderListResultVO> orders = new ArrayList<>();
    OrdersInfoVO ordersInfoVO = new OrdersInfoVO();

    // 获取订单列表
    com.xianzaishi.trade.client.Result<List<OrderVO>> result = null;
    try {
      logger.error("查询参数=" + JackSonUtil.getJson(userQueryVO));
      result =
          orderService.getOrdersByUserId(userQueryVO.getUid(), getAllOrderStatus(),
              userQueryVO.getCurrentPage(), userQueryVO.getPageSize(), true);
    } catch (Exception e) {
      logger.error("调用订单服务失败，" + e.getMessage());
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

    if (null != result && result.isSuccess() && null != result.getModel()) {

      List<OrderVO> model = result.getModel();

      BigDecimal allCash = new BigDecimal("0");// 总消费现金
      BigDecimal allMoney = new BigDecimal("0");// 总消费金额
      Long billedMoney = 0L;// 总已开发票金额

      for (int i = 0; i < model.size(); i++) {
        if (null == model.get(i)) {
          continue;
        }

        OrderListResultVO orderInformationVO = new OrderListResultVO();
        BeanCopierUtils.copyProperties(model.get(i), orderInformationVO);

        //获取退货明细
        List<RefundDetailVO> refundDetailVOList = getRefundDetailVOList(orderInformationVO.getId());
        logger.error("退货明细=" + JackSonUtil.getJson(refundDetailVOList));
        //入库明细
        List<StorageDetailVO> storageDetailsList = inventoryComponent
            .getStorageDetailsByRelationId(orderInformationVO.getId());
        if (null == refundDetailVOList) {
          if (CollectionUtils.isNotEmpty(storageDetailsList)) {
            orderInformationVO.setHavedRefoud(true);
          } else {
            orderInformationVO.setHavedRefoud(false);
          }
        } else {
          orderInformationVO.setHavedRefoud(true);
        }

        //将付款时间修改为订单创建时间
        orderInformationVO.setGmtPay(orderInformationVO.getGmtCreate());

        //设置items
        List<OrderItemVO> items = model.get(i).getItems();
        List<OrderItemInfoVO> itemList = new ArrayList<>();

        //是否是全部退货，默认不是
        Integer a = 0;
        Boolean part = false;

        if (null != items && items.size() != 0) {
          for (int j = 0; j < items.size(); j++) {
            OrderItemInfoVO orderItemVO = new OrderItemInfoVO();
            BeanCopierUtils.copyProperties(items.get(j), orderItemVO);

            orderItemVO.setAlreadyRefound("0");
            orderItemVO.setDealResult("");

            Double itemCount = orderItemVO.getCount();

            if (null != refundDetailVOList) {//退过货，退换货表里面有记录
              List<RefundDetailVO> detailVOBySkuId = getRefundDetailVOBySkuId(
                  orderItemVO.getSkuId(), refundDetailVOList);
              logger.error("skuId：" + orderItemVO.getSkuId() + "对应的退货明细=" + JackSonUtil
                  .getJson(detailVOBySkuId));
              if (CollectionUtils.isNotEmpty(detailVOBySkuId)) {
                String countBySkuId = getRefoundCountBySkuId(orderItemVO.getSkuId(),
                    detailVOBySkuId);
                if (new BigDecimal(countBySkuId)
                    .compareTo(new BigDecimal(String.valueOf(itemCount))) == 0) {
                  //相等
                  a++;
                } else if (new BigDecimal(countBySkuId)
                    .compareTo(new BigDecimal(String.valueOf(itemCount))) == -1
                    && new BigDecimal(countBySkuId).compareTo(new BigDecimal(String.valueOf("0")))
                    == 1) {
                  part = true;
                }
              }
            } else if (CollectionUtils.isNotEmpty(storageDetailsList) && CollectionUtils
                .isEmpty(refundDetailVOList)) {//该订单在入库表里有记录

              //TODO page/size
              String insertedCount = inventoryComponent
                  .getStorageCountBySkuId(storageDetailsList, orderItemVO.getSkuId());

              logger.error("入库明细中的商品skuId：" + orderItemVO.getSkuId() + "总数量：" + insertedCount);

              if (new BigDecimal(insertedCount)
                  .compareTo(new BigDecimal(String.valueOf(orderItemVO.getCount()))) > -1
                  && new BigDecimal(insertedCount)
                  .compareTo(new BigDecimal("0")) > 1) {
                part = true;
              }
              if (new BigDecimal(insertedCount)
                  .compareTo(new BigDecimal(String.valueOf(orderItemVO.getCount()))) == 0
                  ) {
                a++;
              }

            }
            itemList.add(orderItemVO);
          }
          orderInformationVO.setItemList(itemList);

          logger.error("a==" + a + ", item.size=" + items.size());
          logger.error("part==" + part);

          //设置订单是否全部退款
          if (a == items.size() && a > 0) {
            orderInformationVO.setAllRefound(true);
            orderInformationVO.setHavedRefoud(true);
            orderInformationVO.setRefoundGoodsType(RefoundGoodsType.REFOUND_ALL);//全部退货
            orderInformationVO.setRefoundGoodsTypeString("全部退货");
          }

          if (part) {
            orderInformationVO.setAllRefound(false);
            orderInformationVO.setHavedRefoud(true);
            orderInformationVO.setRefoundGoodsType(RefoundGoodsType.REFOUND_PART);//部分退货
            orderInformationVO.setRefoundGoodsTypeString("部分退货");
          }
          if (a == 0 && !part) {
            orderInformationVO.setAllRefound(false);
            orderInformationVO.setHavedRefoud(false);
            orderInformationVO.setRefoundGoodsType(RefoundGoodsType.NO_REFOUND);//没有退过货
            orderInformationVO.setRefoundGoodsTypeString("没有退过货");
          }
        }

          // 设置物流信息
          List<LogisticalVO> logisticalVOList = model.get(i).getLogisticalInfo();
          List<LogisticalInfoVO> logisticalInfoVO = new ArrayList<>();
          if (null != logisticalVOList && logisticalVOList.size() != 0) {
            for (int j = 0; j < logisticalVOList.size(); j++) {
              LogisticalInfoVO orderItemVO = new LogisticalInfoVO();
              BeanCopierUtils.copyProperties(logisticalVOList.get(j), orderItemVO);
              logisticalInfoVO.add(orderItemVO);
            }
            orderInformationVO.setLogisticalInfoList(logisticalInfoVO);
          }

          // 设置积分折扣
          orderInformationVO.setCreditDiscount(getCreditDiscount(model.get(i).getCredit()));

          //设置优惠券折扣
          com.xianzaishi.trade.client.Result<List<OrderDiscountVO>> discountResult = null;
          try {
            discountResult = orderService
                .getOrderDiscountVOByOrderIdAndType(orderInformationVO.getId(), (short) 0);
          } catch (Exception e) {
            logger.error(e.getMessage());
            return Result
                .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
          }
          if (discountResult != null && discountResult.isSuccess() && null != discountResult
              .getModel()) {
            //设置优惠券折扣
            List<OrderDiscountVO> orderDiscountVOList = discountResult.getModel();
            if (CollectionUtils.isNotEmpty(orderDiscountVOList)) {
              OrderDiscountVO orderDiscountVO = orderDiscountVOList.get(0);
              Integer price = orderDiscountVO.getPrice();
              Integer originPrice = orderDiscountVO.getOriginPrice();
              if (null != price && null != originPrice) {
                orderInformationVO
                    .setCouponDiscount(
                        BigDecimal.valueOf(originPrice - price).divide(new BigDecimal(100))
                            .toString());
              } else {
                orderInformationVO.setCouponDiscount("0.00");
              }
            }
          } else {
            orderInformationVO.setCouponDiscount("0.00");
            logger.error("查询订单" + userQueryVO.getOrderId() + "优惠折扣失败");
          }



          //设置订单状态
          orderInformationVO.setStatusString(getDeliveryStatus(orderInformationVO.getStatus()));

          // 业务状态 1表示用户已取消订单 其他表示订单正常
          if (null != orderInformationVO.getStatus()) {
            orderInformationVO.setBusinessStatus(orderInformationVO.getStatus().equals(
                OrderStatus.CLOSED.getValue()) ? 1 : 0);
          }

        orderInformationVO
            .setChannelTypeString(getChannetString(orderInformationVO.getChannelType()));

        // 消费现金
        if (!StringUtils.isEmpty(model.get(i).getEffeAmount())) {
          allCash = allCash.add(new BigDecimal(model.get(i).getEffeAmount()));// 计算总消费现金
        }

        // 消费金额
        if (!StringUtils.isEmpty(model.get(i).getEffeAmount())) {
          allMoney = allMoney.add(new BigDecimal(model.get(i).getPayAmount()));// 计算总消费金额
        }

        Result<BillInformationDTO> billDTOResult = null;

        try {
          if (null != model.get(i).getId()) {
            // 发票金额
            billDTOResult = billService.selectBillInfoByOrderId(model.get(i).getId());

          // 渠道类型
          orderInformationVO.setChannelTypeString(getChannetString(orderInformationVO
              .getChannelType()));

          // 消费金额
          if (!StringUtils.isEmpty(model.get(i).getEffeAmount())) {
            allCash = allCash.add(new BigDecimal(model.get(i).getEffeAmount()));// 计算总消费净额 现金

          }
          Result<BillInformationDTO> billDTOResult = null;

          try {
            if (null != model.get(i).getId()) {
              // 发票金额
              billDTOResult = billService.selectBillInfoByOrderId(model.get(i).getId());
            }
          } catch (Exception e) {
            e.printStackTrace();
            logger.error("查询发票信息失败");
            return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
                e.getMessage());
          }
          if (null != billDTOResult && billDTOResult.getSuccess()
              && null != billDTOResult.getModule()) {
            Long money = billDTOResult.getModule().getMoney();
            if (null != money) {
              billedMoney = billedMoney + money;// 发票金额累加
            }
            orderInformationVO.setBillMoney(String.valueOf(money));// 设置订单金额

            // 查询该oid是否开过发票
            orderInformationVO.setBilled(true);// 是否开过发票
            orderInformationVO.setBillNumber(billDTOResult.getModule().getBillNumber());


        } else {
          orderInformationVO.setBilled(false);// 是否开过发票
          orderInformationVO.setBillNumber(0L);
        }

      }



          orders.add(orderInformationVO);

      }

      // 设置总消费现金
      ordersInfoVO.setAllCash(allCash.toString());
      //设置总消费金额
      ordersInfoVO.setAllMoney(allMoney.toString());

      // 已开发票金额
      String realBilled = BigDecimal.valueOf(billedMoney).divide(new BigDecimal(100)).toString();// 元
      ordersInfoVO.setMoneyHasBilled(realBilled);

        // 设置总消费金额
        ordersInfoVO.setAllCash(allCash.toString());
        // 已开发票金额
        String realBilled = BigDecimal.valueOf(billedMoney).divide(new BigDecimal(100))
            .toString();// 元
        ordersInfoVO.setMoneyHasBilled(realBilled);
        // 获取用户订单数量
        com.xianzaishi.trade.client.Result<Integer> orderCountResult = null;
        try {
          if (null != userQueryVO.getUid()) {
            orderCountResult =
                orderService.getOrderCountByUserId(userQueryVO.getUid(), getAllOrderStatus());
          }
        } catch (Exception e) {
          logger.error("调用用户服务失败，" + e.getMessage());
          return Result
              .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
        }

        if (null != orderCountResult && orderCountResult.isSuccess()
            && null != orderCountResult.getModel()) {
          // 设置总数
          ordersInfoVO.setCount(orderCountResult.getModel());

          // 用户订单总数
          int count = (int) orderCountResult.getModel();

          if (null != userQueryVO.getPageSize() && (int) userQueryVO.getPageSize() > 0) {
            int yushu = count % userQueryVO.getPageSize();
            int pageCount = count / userQueryVO.getPageSize();
            if (0 != yushu) {
              pageCount++;
            }
            // 设置总页数
            ordersInfoVO.setTotalPage(pageCount);
            // 返回当前页
            ordersInfoVO.setCurrentPage(userQueryVO.getCurrentPage());
          }
        }

      // 查询用户基本
      UserDTO userDTO = getUserInformation(userQueryVO.getUid());
      if (null != userDTO) {
        ordersInfoVO.setUserName(userDTO.getName());
        ordersInfoVO.setPhoneNumber(String.valueOf(userDTO.getPhone()));
      }

      ordersInfoVO.setOrders(orders);
      return Result.getSuccDataResult(ordersInfoVO);
    }
    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "获取订单列表失败");
  }


  /**
   * 获取用户常用地址列表
   *
   * @param userId 用户列表
   * @return 地址列表
   */
  public List<String> getAllLinkman(Long userId) {

    if (null == userId || userId <= 0) {
      return null;
    }

    com.xianzaishi.trade.client.Result<List<DeliveryVO>> addreses = null;
    try {
      addreses = deliveryService.getUseralwaysAddreses(userId);
    } catch (Exception e) {
      logger.error("调用deliveryService失败，" + e.getMessage());
      return null;
    }

    if (null != addreses && addreses.isSuccess() && null != addreses.getModel()) {

      List<String> list = new ArrayList<String>();
      List<DeliveryVO> model = addreses.getModel();

      for (int i = 0; i < model.size(); i++) {

        DeliveryVO deliveryVO = model.get(i);
        if (null == deliveryVO) {
          continue;
        }

        // 大概地址
        String codeAddress = deliveryVO.getCodeAddress();
        // 详细地址
        String address = deliveryVO.getAddress();
        // 配送姓名
        String name = deliveryVO.getName();
        // 配送号码
        String phone = deliveryVO.getPhone();
        //门牌号
        String addressDetail = deliveryVO.getAddressDetail();

        String myAddress = "";
        if (null != codeAddress && null != addreses) {
          myAddress = name + " " + phone + " " + myAddress + codeAddress + address + addressDetail;
        }

        if (null != codeAddress && null == addreses) {
          myAddress = name + " " + phone + " " + myAddress + codeAddress + addressDetail;
        }

        if (null == codeAddress && null != addreses) {
          myAddress = myAddress + "地址选择错误";
        }

        list.add(myAddress);
      }

      return list;

    }

    return null;
  }

  /**
   * 查询订单信息
   * 
   * @return 订单信息
   */
  public OrderVO getOrderInfo(Long oid) {

    try {
      if (null != oid && orderService.getOrder(oid).isSuccess()
          && null != orderService.getOrder(oid).getModel()) {
        return orderService.getOrder(oid).getModel();
      }

      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 根据uid获取用户信息
   */
  public UserDTO getUserInformation(Long uid) {
    try {
      Result<? extends BaseUserDTO> result = userService.queryUserByUserId(uid, true);
      if (result.getSuccess() && null != result.getModule()) {
        return (UserDTO) result.getModule();
      }
      return null;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * 获取用户优惠券列表
   * 
   * @param userQueryVO 用户id
   */
  @SuppressWarnings("deprecation")
  public Result<List<UserCouponVO>> getCouponList(UserQueryVO userQueryVO) {

    if (null == userQueryVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "封装参数对象为空");
    }

    if (null == userQueryVO.getUid() || (long) userQueryVO.getUid() <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "用户uid异常");
    }

    Result<List<UserCouponDTO>> result = null;
    try {
      result = userCouponService.getUserAllCouponByUserId(userQueryVO.getUid());
    } catch (Exception e) {
      logger.error("调用userCouponService异常");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "调用用户优惠券服务异常");
    }

    if (null != result && result.getSuccess() && null != result.getModule()) {

      List<UserCouponDTO> couponDTOList = result.getModule();
      List<UserCouponVO> list = new ArrayList<>();

      for (int i = 0; i < couponDTOList.size(); i++) {
        if (null == couponDTOList.get(i)) {
          continue;
        }

        UserCouponVO userCouponVO = new UserCouponVO();

        // 转移变量
        BeanCopierUtils.copyProperties(couponDTOList.get(i), userCouponVO);
        UserCouponDTO couponDTO = couponDTOList.get(i);

        // 优惠金额
        userCouponVO.setDiscountAmount(
            userCouponVO.getAmount() == null ? "0.00" : userCouponVO.getAmount() + "");

        // 渠道名
        Short channelType = couponDTO.getChannelType();// 0 通用 1 线上 2 线下
        userCouponVO.setChannel(getChannetString(channelType));

        Short status = couponDTO.getStatus();
        switch (status) {
          case 0:
            userCouponVO.setCurrentStatus("已使用");
            // userCouponVO.setUsedYouHui(String.valueOf(couponDTO.getAmount()));
            userCouponVO.setUsedYouHui("0");
            break;
          case 1:
            userCouponVO.setCurrentStatus("未使用");
            userCouponVO.setUsedYouHui("0");
            break;
        }

        list.add(userCouponVO);

      }
      return Result.getSuccDataResult(list);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询优惠券失败");
  }

  /**
   * 插入发票信息
   */
  public Result<Boolean> insertBillInfo(BillsInsertVO vo) {

    if (null == vo || (long) vo.getBillNumber() < 0) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "添加发票信息失败，发票号不正确");
    }

    if (null == vo.getOrders() || vo.getOrders().size() == 0) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "添加发票信息失败，订单信息为空");
    }

    BillInsertDTO dto = new BillInsertDTO();
    dto.setBillNumber(vo.getBillNumber());
    dto.setOperator(vo.getOperator());
    dto.setContribution(vo.getContribution());

    List<BillDetailsDTO> list = new ArrayList<>();
    for (int i = 0; i < vo.getOrders().size(); i++) {
      if (null == vo.getOrders().get(i)) {
        continue;
      }
      BillDetailsDTO detailsDTO = new BillDetailsDTO();
      BeanCopierUtils.copyProperties(vo.getOrders().get(i), detailsDTO);
      list.add(detailsDTO);
    }

    dto.setOrders(list);

    Result<Boolean> batchResult = null;// 插入发票信息
    try {
      batchResult = billService.bactchBillVO(dto);
    } catch (Exception e) {
      logger.error("插入发票信息失败");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

    if (null != batchResult && batchResult.getSuccess() && batchResult.getModule()) {
      return Result.getSuccDataResult(true);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入发票信息失败");

  }

  /**
   * 获取erp查询用户界面的信息
   */
  public Result<UserInfoVO> getBasicUserInfo(UserOrderVO userQueryVO) {

    if (null == userQueryVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询失败，参数不正确");
    }

    if (null != userQueryVO.getPhoneNum() && (userQueryVO.getPhoneNum()+"").length() != 11) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "用户手机号码不正确");
    }

    if (null == userQueryVO.getOid() &&  null == userQueryVO.getPhoneNum()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "用户手机号码和订单id不正确");
    }


    Long uid = null;
    UserDTO dto = null;

    // 只有手机号码
    if (null != userQueryVO.getPhoneNum()) {
      dto = getUserInfo(userQueryVO.getPhoneNum());
      if (null == dto) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "获取用户信息失败");
      }
      uid = dto.getUserId();
    }

    if (null == userQueryVO.getPhoneNum() && null != userQueryVO.getOid()) {

      OrderVO orderVO = getOrderInfo(userQueryVO.getOid());

      if (null == orderVO) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询用户失败");
      }

      uid = orderVO.getUserId();
      dto = getUserInformation(uid);

      if (null == dto) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询查询用户失败");
      }
    }

    Long userId = dto.getUserId();// 用户id
    String name = dto.getName();// 用户名字
    Short userType = dto.getUserType();// 用户类型
    Long phone = dto.getPhone();// 手机号码
    Integer code = 0;//验证码
    String activityCodeStr = dto.getActiveCode();
    if(StringUtils.isNotEmpty(activityCodeStr)){
      activityCodeStr = activityCodeStr.substring(0, activityCodeStr.indexOf(";"));
      if(StringUtils.isNumeric(activityCodeStr)){
        code = Integer.valueOf(activityCodeStr);
      }
    }

    // 查询用户积分
    Integer userCredit = getUserCredit(userId);

    // 获取常用地址列表
    List<String> alwaylsAddress = getAllLinkman(userId);

    UserInfoVO userInfoVO = new UserInfoVO();
    userInfoVO.setUserName(name);
    userInfoVO.setUserId(userId);
    userInfoVO.setPhoneNum(phone);
    userInfoVO.setCredit(userCredit);
    userInfoVO.setLevel(userType);
    userInfoVO.setAddressList(alwaylsAddress);
    userInfoVO.setActivityCode(code);

    return Result.getSuccDataResult(userInfoVO);
  }


  /**
   * 判断订单是否已开发票 bill
   */
  public Result<Boolean> hasBilled(BillQueryVO queryVO) {
    if (null == queryVO || null == queryVO.getOrderId() || (long) queryVO.getOrderId() < 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数不正确");
    }
    try {
      return billService.hasBillByOrderId(queryVO.getOrderId());
    } catch (Exception e) {
      logger.error("调用发票服务失败，" + e.getMessage());
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
  }


  /**
   * 修改发票信息
   */
  public Result<Boolean> updateBIllVO(BillUpdateVO params) {
    if (null == params || params.getBillMoney() == null || params.getBillNumber() == null) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "修改发票数据失败，数据不正确");
    }

    BillInformationDTO billInformationDTO = new BillInformationDTO();
    BeanCopierUtils.copyProperties(params, billInformationDTO);

    if (null == params.getUserId() && null != params.getPhoneNumber()) {
      Long phoneNumber = params.getPhoneNumber();
      try {
        Result<? extends BaseUserDTO> phoneResult =
            userService.queryUserByPhone(phoneNumber, false);
        if (null != phoneResult && phoneResult.getSuccess() && null != phoneResult.getModule()) {
          billInformationDTO.setUserId(Integer.parseInt(String.valueOf(phoneResult.getModule()
              .getUserId())));
        }
      } catch (NumberFormatException e) {
        logger.error("调用userService失败");
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
      }
    }


    try {
      Result<Boolean> updateBillInfo = billService.updateBillInfo(billInformationDTO);
      if (null != updateBillInfo && updateBillInfo.getSuccess() && updateBillInfo.getModule()) {
        return Result.getSuccDataResult(updateBillInfo.getModule());
      }
    } catch (Exception e) {
      logger.error("调用发票服务失败");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "修改发票数据失败");
  }

  /**
   * 根据订单id获取退货明细
   * @param orderId  订单id
   * @return List<RefundDetailVO>
   */
  public List<RefundDetailVO> getRefundDetailVOList(Long orderId) {
    isNull(orderId, "订单id不能为空");
    com.xianzaishi.trade.client.Result<List<RefundDetailVO>> refundDetailInfo = orderService
        .getRefundDetailInfo(orderId);
    if (!refundDetailInfo.isSuccess()) {
      return null;
    }
    if (CollectionUtils.isEmpty(refundDetailInfo.getModel())) {
      return null;
    }
    return refundDetailInfo.getModel();
  }


  public void notSuccess(Result result, String showMsg) {
    if (null == result) {
      throw new BizException("result参数为空");
    }
    notTrue(result.getSuccess(), showMsg);
  }

  public void notTrue(Boolean a, String showMsg) {
    if (a == null) {
      throw new BizException("布尔参数为空");
    }
    if (!a) {
      throw new BizException(showMsg);
    }
  }



  /**
   * 根据skuId获取明细集合中的退货明细
   *@param skuId 商品skuId
   * @param refundDetailVOS 入库明细
   * @return List<RefundDetailVO> 结果集合可能为空
   */
  public List<RefundDetailVO> getRefundDetailVOBySkuId(Long skuId,
      List<RefundDetailVO> refundDetailVOS) {
    isNull(skuId, "skuId不能为空");
    ListUtils.isEmpty(refundDetailVOS, "退货明细集合为空");

    List<RefundDetailVO> list = new ArrayList<>();
    for (RefundDetailVO refundDetailVO : refundDetailVOS) {
      if (null == refundDetailVO) {
        continue;
      }
      if (skuId.equals(refundDetailVO.getSkuId())) {
        list.add(refundDetailVO);
      }
    }
    return list;
  }

  /**
   * 根据skuId获取退货数量
   * @param skuId
   * @param list 某一个skuid对应的退货明细的多次退货明细集合
   * @return 总的退货数量
   */
  public String getRefoundCountBySkuId(Long skuId,List<RefundDetailVO> list) {
    isNull(skuId, "商品skuId不能为空");
    ListUtils.isEmpty(list, "退货明细为空");
    String count = "0";
    for (RefundDetailVO refundDetailVO:list) {
      if (null == refundDetailVO) {
        continue;
      }
      count = DemicalUtils.add(count, refundDetailVO.getCount()).toString();
    }
    logger.error("skuId=" + skuId + ", count=" + count);
    return count;
  }

  public void isNull(Object a ,String showMsg) {
    if (a == null) {
      throw new BizException(showMsg);
    }
  }

  /**
   * 获取订单信息
   */
  public Result<OrderSVO> getOrderDetailsInfo(UserQueryVO userQueryVO) {
    if (null == userQueryVO || (long) userQueryVO.getOrderId() <= 0) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "获取订单信息失败，参数不正确");
    }
    com.xianzaishi.trade.client.Result<OrderVO> orderVOResult = null;

    try {
      // 获取订单
      orderVOResult = orderService.getOrder(userQueryVO.getOrderId());
    } catch (Exception e) {
      logger.error("调用orderService服务失败");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

    if (null == orderVOResult || !orderVOResult.isSuccess() ) {
      Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "获取订单信息失败");
    }

    OrderVO orderVO = orderVOResult.getModel();
    if (null == orderVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "获取订单信息失败");
    }

    OrderSVO order = new OrderSVO();
    BeanCopierUtils.copyProperties(orderVO, order);

    List<OrderItemVO> items = orderVO.getItems();
    List<LogisticalVO> logisticalInfo = orderVO.getLogisticalInfo();


    //获取退货明细
    List<RefundDetailVO> refundDetailVOList = getRefundDetailVOList(userQueryVO.getOrderId());
    logger.error("退货明细=" + JackSonUtil.getJson(refundDetailVOList));

    List<OrderItemInfoVO> orderItemInfoVOSList = new ArrayList<>();
    List<LogisticalInfoVO> logisticalInfoVOSList = new ArrayList<>();

    //入库明细
    List<StorageDetailVO> storageDetailsList = inventoryComponent
        .getStorageDetailsByRelationId(order.getId());

    logger.error("入库明细=" + JackSonUtil.getJson(storageDetailsList));

    Integer a = 0;
    Boolean part = false;

    if (null != items) {
      for (int i = 0; i < items.size(); i++) {
        OrderItemInfoVO orderItemInfoVO = new OrderItemInfoVO();
        BeanCopierUtils.copyProperties(items.get(i), orderItemInfoVO);

        Double orderItemInfoVOCount = orderItemInfoVO.getCount();

        if (null != refundDetailVOList) {

          List<RefundDetailVO> detailVOBySkuId = getRefundDetailVOBySkuId(
              orderItemInfoVO.getSkuId(), refundDetailVOList);

          if (CollectionUtils.isNotEmpty(detailVOBySkuId)) {
            //退货情况
            String count = getRefoundCountBySkuId(orderItemInfoVO.getSkuId(), detailVOBySkuId);

            logger.error("skuId：" + orderItemInfoVO.getSkuId() + "的退货数量：" + count);

            orderItemInfoVO.setAlreadyRefound(count);//已退货数量
            orderItemInfoVO.setDealResult(getDealResult(count, orderItemInfoVOCount));//处理结果

            if (new BigDecimal(count)
                .compareTo(new BigDecimal(String.valueOf(orderItemInfoVOCount))) == 0) {
              //相等
              a++;
            } else if (new BigDecimal(count)
                .compareTo(new BigDecimal(String.valueOf(orderItemInfoVOCount))) == -1
                && new BigDecimal(count).compareTo(new BigDecimal(String.valueOf("0"))) == 1) {
              part = true;
            }
          }
        } else if (CollectionUtils.isNotEmpty(storageDetailsList) && CollectionUtils
            .isEmpty(refundDetailVOList)) {//该订单在入库表里有记录

          //TODO page/size
          String insertedCount = inventoryComponent
              .getStorageCountBySkuId(storageDetailsList, orderItemInfoVO.getSkuId());

          logger.error("入库明细中的商品skuId：" + orderItemInfoVO.getSkuId() + "总数量：" + insertedCount);

          orderItemInfoVO.setAlreadyRefound(insertedCount);//已退货数量
          orderItemInfoVO.setDealResult(getDealResult(insertedCount, orderItemInfoVOCount));//处理结果

          if (new BigDecimal(insertedCount)
              .compareTo(new BigDecimal(String.valueOf(orderItemInfoVO.getCount()))) > -1
              && new BigDecimal(insertedCount)
              .compareTo(new BigDecimal("0")) > 1) {
            part = true;
          }
          if (new BigDecimal(insertedCount)
              .compareTo(new BigDecimal(String.valueOf(orderItemInfoVO.getCount()))) == 0
              ) {
            a++;
          }

        }

        orderItemInfoVOSList.add(orderItemInfoVO);
      }

      logger.error("a=" + a + ", items.size=" + items.size());

      //设置订单是否全部退款
      if (a == items.size() && a > 0) {
        order.setAllRefound(true);//是否全部退货
        order.setHavedRefoud(true);//是否退过货
        order.setRefoundGoodsType(RefoundGoodsType.REFOUND_ALL);//全部退货
        order.setRefoundGoodsTypeString("全部退货");
      }

      if (part) {
        order.setAllRefound(false);//是否全部退货
        order.setHavedRefoud(true);//是否退过货
        order.setRefoundGoodsType(RefoundGoodsType.REFOUND_PART);//部分退货
        order.setRefoundGoodsTypeString("部分退货");
      }
      if (a == 0 && !part ){
        order.setAllRefound(false);//是否全部退货
        order.setHavedRefoud(false);//是否退过货
        order.setRefoundGoodsType(RefoundGoodsType.NO_REFOUND);//没有退过货
        order.setRefoundGoodsTypeString("没有退过货");
      }

    }

    if (null != logisticalInfo) {
      for (int i = 0; i < logisticalInfo.size(); i++) {
        LogisticalInfoVO logisticalInfoVO = new LogisticalInfoVO();
        BeanCopierUtils.copyProperties(logisticalInfo.get(i), logisticalInfo);
        logisticalInfoVOSList.add(logisticalInfoVO);
      }
    }

    // 设置订单状态
    if (null != order.getStatus()) {
      String deliveryStatus = getDeliveryStatus((short) order.getStatus());
      order.setStatusString(deliveryStatus);
    }

    try {
      if (null != order.getId() && (long) order.getId() > 0) {
        // 获取配送单信息
        SimpleResultVO<List<DistributionVO>> simpleResultVO =
            distributionDomainClient.getDistributionByOrderID(order.getId());
        if (simpleResultVO.isSuccess() && null != simpleResultVO.getTarget()
            && simpleResultVO.getTarget().size() > 0) {

          DistributionVO distributionVO = simpleResultVO.getTarget().get(0);

          Date appointTime = distributionVO.getAppointTime();
          Date deliveredTime = distributionVO.getDeliveredTime();
          Date orderTime = distributionVO.getOrderTime();
          Date createTime = distributionVO.getCreateTime();
          String userName = distributionVO.getUserName();
          String userPhone = distributionVO.getUserPhone();
          String userAddress = distributionVO.getUserAddress();

          order.setDistributinStartTime(createTime);//开始配送时间
          order.setDistributinEndTime(appointTime);//送达时间

          order.setPhoneNumber(userPhone);//号码
          order.setName(userName);//配送单名字
          order.setDistributinaAddress(userAddress);//配送地址

          // 配送状态
          order.setDistributinStatus(getPeisongString(distributionVO.getStatu()));

        } else {
          logger.error("调用distributionDomainClient失败");
        }
      }
    } catch (Exception e) {
      logger.error("调用distributionDomainClient服务异常");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

    // 设置商品
    order.setItemsList(orderItemInfoVOSList);
    order.setLogisticalInfoList(logisticalInfoVOSList);

    //设置优惠券折扣
    com.xianzaishi.trade.client.Result<List<OrderDiscountVO>> discountResult = null;

    try {
      discountResult = orderService
          .getOrderDiscountVOByOrderIdAndType(userQueryVO.getOrderId(), (short) 0);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
    if (discountResult != null && discountResult.isSuccess() && null != discountResult.getModel()) {
      //设置优惠券折扣
      List<OrderDiscountVO> orderDiscountVOList = discountResult.getModel();
      if (CollectionUtils.isNotEmpty(orderDiscountVOList)) {
        OrderDiscountVO orderDiscountVO = orderDiscountVOList.get(0);
        Integer price = orderDiscountVO.getPrice();
        Integer originPrice = orderDiscountVO.getOriginPrice();
        if (null != price && null != originPrice) {
          order.setCouponDiscount(
              BigDecimal.valueOf(originPrice - price).divide(new BigDecimal(100))
                  .toString());//设置优惠券折扣
        } else {
          order.setCouponDiscount("0.00");
        }
      }
    } else {
      order.setCouponDiscount("0.00");
      logger.error("查询订单"+userQueryVO.getOrderId()+"优惠折扣失败");
    }

    //积分折扣
    order.setCreditDiscount(getCreditDiscount(order.getCredit()));

    // 业务状态
    if (null != order.getStatus()) {// 1 表示正常 0 表示用户已取消订单
      order.setBusinessStatus(order.getStatus().equals(OrderStatus.CLOSED.getValue()) ? 1 : 0);
    }

    return Result.getSuccDataResult(order);
  }

  /**
   * 获取处理结果string
   */
  private String getDealResult(String returnCount, Double buyCount) {
    if (null == returnCount || null == buyCount) {
      return "未知结果";
    }

    if ((new BigDecimal(returnCount).compareTo(new BigDecimal(String.valueOf(buyCount))) == -1) && (
        new BigDecimal(returnCount).compareTo(new BigDecimal("0")) == 1)) {
      return "部分退货";
    }

    if ((new BigDecimal(returnCount).compareTo(new BigDecimal(String.valueOf(buyCount))) == 0)) {
      return "全部退货";
    }

    if ((new BigDecimal(returnCount).compareTo(new BigDecimal("0")) == 0)) {
      return "没有退货";
    }

    return "未知结果";
  }

  /**
   * 获取当前格式化后的时间
   */
  public String getCurrentDateTime(Date date) {
    if (null == date) {
      return null;
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return sdf.format(date);
  }

  /**
   * 获取配送单状态
   */
  public String getDeliveryStatus(short status) {

    String statuString = "";

    if (status == (short) OrderStatus.INVALID.getValue()) {
      statuString = "删除";
    }

    if (status == (short) OrderStatus.INIT.getValue()) {
      statuString = "待付款";
    }

    if (status == (short) OrderStatus.PAY.getValue()) {
      statuString = "待捡货";
    }

    if (status == (short) OrderStatus.PICKING.getValue()) {
      statuString = "捡货中";
    }

    if (status == (short) OrderStatus.PICKED.getValue()) {
      statuString = "待配送";
    }

    if (status == (short) OrderStatus.DISPATCHING.getValue()) {
      statuString = "配送中";
    }

    if (status == (short) OrderStatus.VALID.getValue()) {
      statuString = "交易成功";
    }

    if (status == (short) OrderStatus.AFTERSALES.getValue()) {
      statuString = "客服处理";
    }

    if (status == (short) OrderStatus.REFOUDING.getValue()) {
      statuString = "退款中";
    }

    if (status == (short) OrderStatus.REFOUDED.getValue()) {
      statuString = "退款成功";
    }

    if (status == (short) OrderStatus.CLOSED.getValue()) {
      statuString = "交易关闭";
    }

    return statuString;
  }


  /**
   * 获取优惠券折扣
   *
   * @param amout 优惠金额
   */
  public String getCouponDiscount(short status, double amout) {
    String a = "";
    if (0 == status) {// 0 使用 1未使用
      return a + amout;
    }
    if (1 == status) {
      return a + 0;
    }

    return a;
  }

  /**
   * 获取积分折扣
   */
  public String getCreditDiscount(Integer credit) {
    if (null == credit) {
      return "0";
    }
    BigDecimal bigDecimal = new BigDecimal(credit);
    BigDecimal bigDecimal1 = new BigDecimal("0.01");
    return bigDecimal.multiply(bigDecimal1).toString();

  }

  /**
   * 获取所有订单状态
   */
  public short[] getAllOrderStatus() {
    short[] status =
        {OrderStatus.INVALID.getValue(), OrderStatus.INIT.getValue(), OrderStatus.PAY.getValue(),
            // PAY("待拣货",(short)0x3),//已付款
            // -->退款中(3-5个工作日)
            OrderStatus.PICKING.getValue(), OrderStatus.PICKED.getValue(),
            // PICKED("待配送",(short)0x5),//拣货完成待配送
            OrderStatus.DISPATCHING.getValue(), // DISPATCHING("配送中",(short)0x6),
            OrderStatus.VALID.getValue(), // VALID("成功",(short)7),//交易成功,确认收货
            OrderStatus.AFTERSALES.getValue(), // AFTERSALES("客服处理",(short)0x8),//售后服务
            OrderStatus.REFOUDING.getValue(), // REFOUDING("退款中",(short)0x9),//客服已受理
            OrderStatus.REFOUDED.getValue(), // REFOUDED("退款成功",(short)0xA),
            // //部分退款仍然是交易成功，全部退款表示交易关闭
            OrderStatus.CLOSED.getValue()// CLOSED("交易关闭",(short)0xB);
        };

    return status;
  }

  /**
   * 获取渠道码对应的渠道名
   */
  public String getChannetString(short channelType) {
    switch (channelType) {
      case 0:
        return "通用";
      case 1:
        return "线上";
      case 2:
        return "线下";
    }
    return "";
  }

  /**
   * 获取验证码
   * @param checkCode
   * @return
   */
  private String getActiveCode(String checkCode) {
    if (null == checkCode) {
      return null;
    }
    StringBuffer activeCode = new StringBuffer();
    activeCode.append(checkCode);
    activeCode.append(";");// 以;为分隔符
    long date = new Date().getTime();// 当前发送验证码时间
    activeCode.append(date);
    logger.equals("激活码="+activeCode.toString());
    return activeCode.toString();
  }

  /**
   * erp app 登陆
   */
  public Result<ResultVO> employeeloginERP(EmployeeQueryVO userQueryVO) {

    if (null == userQueryVO || null == userQueryVO.getData() || null == userQueryVO.getData()
        .getPhone() || null == userQueryVO.getData().getCheckCode()) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "用户参数不正确");
    }

    BackGroundUserDTO dbUser = null;

    QueryDataVO queryDataVO = userQueryVO.getData();
    Long phone = queryDataVO.getPhone();//手机号
    String checkCode = queryDataVO.getCheckCode();//验证码

    logger.error("手机号码，phone=" + phone + ", checkCode=" + checkCode);

    if (null != phone && (long) phone > 0) {
      dbUser = getUserByPhone(phone);
      if (null == dbUser) {
        return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
            "该用户不存在");
      }
    }

    if (null != dbUser.getUserType() && !UserTypeConstants.USER_EMPLOYEE
        .equals(dbUser.getUserType())) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_USER_NOT_SUPPORT,
          "该用户不是店内员工");
    }

    //获取激活码
    String activeCode = dbUser.getActiveCode();
    if (StringUtils.isNotEmpty(activeCode)) {
      String[] strings = activeCode.split(";");
      String code = strings[0];
      String sendTime = strings[1];

      logger.error("code=" + code + ", sendTime=" + sendTime);

      long nowTime = new Date().getTime();
      if (StringUtils.isNumeric(sendTime) && (nowTime - Long.parseLong(sendTime.trim())
          < 1000 * 60 * 30)) {
        if (StringUtils.isNumeric(code) && StringUtils.isNotEmpty(checkCode) && code
            .equals(checkCode)) {
          String token = getUuid();
          dbUser.setToken(token);
          if (updateUser(dbUser)) {
            ResultVO employeeResultVO = new ResultVO();
            employeeResultVO.setToken(token);
            return Result.getSuccDataResult(employeeResultVO);
          }
          return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_GETTOKEN_ERROR,
              "更新用户token失败");
        }
      }

      return Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "登陆失败，验证码失效");
    }

    return Result
        .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "登陆失败，用户验证码为空");
  }


  /**
   * 获取配送单状态
   */
  public String getDistributionStatus(Integer status) {
    if (null == status) {
      return null;
    }

    String statuString = "";
    // 0，创建成功，1，进入等待队列，2，波次创建完成，3，送货中，4，送货完成，-1，挂起

    if (status == 0) {
      statuString = "创建成功";
    }

    if (status == 1) {
      statuString = "进入等待队列";
    }

    if (status == 2) {
      statuString = "波次创建完成";
    }

    if (status == 3) {
      statuString = "送货中";
    }

    if (status == 4) {
      statuString = "送货完成";
    }

    if (status == -1) {
      statuString = "挂起";
    }

    return statuString;
  }
  /**
   * 获取未处理的订单
   */
  public Result<HashMap<String, List<OrderReturnVO>>> getUndealedOrderList(ERPQueryVO query) {

    if (null == query || null == query.getData() || null == query.getData().getOffLine()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求参数异常");
    }

    if (CollectionUtils.isEmpty(query.getData().getCatIdList())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请选择类目");
    }


    com.xianzaishi.trade.client.Result<List<OrderVO>> listResult = null;
    List<Integer> catIdList = query.getData().getCatIdList();//类目id

    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      Date end = new Date();
      Date begin = new Date(end.getTime() - TIME_SCOPE);// one hour

      //1线上,2线下
      OrderVO orderVO = new OrderVO();
      orderVO.setBegin(begin);
      orderVO.setEnd(end);
      orderVO.setChannelType(query.getData().getOffLine() ? (short)2 : (short)1);

      logger.error("请求参数="+JackSonUtil.getJson(orderVO));
      listResult = orderService
          .getOrdersByVO(orderVO);

    } catch (Exception e) {
      logger.error("调用订单服务失败");
      return Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "调用订单服务失败");
    }

    logger.error("请求结果="+JackSonUtil.getJson(listResult));

    if (null == listResult || CollectionUtils.isEmpty(listResult.getModel())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询订单信息为空");
    }

    //获取结果集合
    List<OrderVO> listResultModel = listResult.getModel();

    List<OrderReturnVO> orderList = new ArrayList<>();
    for (OrderVO orderVO : listResultModel) {

      if (null == orderVO) {
        continue;
      }

      if (!(orderVO.getStatus() >= 4 && orderVO.getStatus() <= 7)) {
        continue;
      }

      //过滤出 4w到5w 符合类目id集合的 itemList数据
      List<OrderItemInfoVO> targetItemList = getTargetItemList(orderVO.getItems(),catIdList);
      if (null == targetItemList || targetItemList.size() == 0) {
        continue;
      }

      //过滤出待处理的
      if (null == orderVO.getTag()) {
        continue;
      }
      if ( (orderVO.getTag() & 0x1) == 1 || ((orderVO.getTag()) & 0x2) == 2) {
        continue;
      }

      OrderReturnVO orderSVO = new OrderReturnVO();
      BeanCopierUtils.copyProperties(orderVO, orderSVO);//订单基本数据

      //设置待处理状态码
      orderSVO.setTagStatus(0);

      //判断订单是否取消
      if ( null != orderVO.getStatus() && OrderStatus.CLOSED.getValue() ==  orderVO.getStatus()) {
        orderSVO.setTagStatus(-1);
      }

      //设置item数据
      orderSVO.setItemsList(targetItemList);

      //设置配送地址
      DistributionVO distributionVO = getDistributionInfo(orderVO.getId());
      if (null != distributionVO) {
        orderSVO.setDistributinaAddress(distributionVO.getUserAddress());//配送地址
        orderSVO.setDistributionPhone(distributionVO.getUserPhone());//配送单手机号
        orderSVO.setGmtDistribution(distributionVO.getAppointTime());//送达时间
      }

      //会员手机号
      try {
        Result<? extends BaseUserDTO> userByUserId = userService
            .queryUserByUserId(orderVO.getUserId(), false);
        if (null != userByUserId && userByUserId.getSuccess() && null != userByUserId.getModule()) {
          orderSVO.setPhoneNumber(userByUserId.getModule().getPhone());//会员号码
        }
      } catch (Exception e) {
        logger.error("调用userService服务失败");
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,e.getMessage());
      }

      //设置订单折扣
      com.xianzaishi.trade.client.Result<List<OrderDiscountVO>> discountResult = null;
      try {
        discountResult = orderService
            .getOrderDiscountVOByOrderIdAndType(orderSVO.getId(),(short)0);
      } catch (Exception e) {
        logger.error("调用orderService服务异常，"+e.getMessage());
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
      }
      if (discountResult != null && discountResult.isSuccess() && null != discountResult.getModel()) {
        //设置优惠券折扣
        List<OrderDiscountVO> orderDiscountVOList = discountResult.getModel();
        if (CollectionUtils.isNotEmpty(orderDiscountVOList)) {
          OrderDiscountVO orderDiscountVO = orderDiscountVOList.get(0);
          Integer price = orderDiscountVO.getPrice();
          Integer originPrice = orderDiscountVO.getOriginPrice();
          if (null != price && null != originPrice) {
            orderSVO.setDiscountAmount(
                BigDecimal.valueOf(originPrice - price).divide(new BigDecimal(100)).toString());
          } else {
            orderSVO.setDiscountAmount("0.00");
          }
        }
      }else {
        logger.error("");
        orderSVO.setDiscountAmount("0.00");
      }

      orderList.add(orderSVO);
    }

    logger.error("orderList集合大小=" + orderList.size());

    if (CollectionUtils.isEmpty(orderList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"查询订单结果为空");
    }

    //设置data
    HashMap<String, List<OrderReturnVO>> map = Maps.newHashMap();
    map.put("orders",orderList);

    return Result.getSuccDataResult(map);

  }

  /**
   * 根据 orderid phone time查询订单 类目id 是否线下
   */
  public Result<HashMap<String, List<OrderReturnVO>>> getOrderListByTimeOrOrderID(
      ERPQueryVO query) {

    if (null == query || null == query.getData() || null == query.getData().getOffLine()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求参数异常");
    }

    if (CollectionUtils.isEmpty(query.getData().getCatIdList())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请选择类目");
    }

    //小于等于7为订单号 11位为手机号
    if (null != query.getData().getPhone()) {
      if ((query.getData().getPhone() + "").length() > 11
          || (long) query.getData().getPhone() <= 0) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请输入正确的订单号或手机号");
      }

      if ((query.getData().getPhone() + "").length() > 7
          && (query.getData().getPhone() + "").length() < 11) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请输入正确的手机号");
      }
    }

    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    com.xianzaishi.trade.client.Result<List<OrderVO>> listResult = null;
    List<Integer> catIdList = query.getData().getCatIdList();//类目集合

    Date begin = null;
    Date end = null;

    //判断是否查询今天的订单
    if (null != query.getData().getTime()) {
      begin = new Date(query.getData().getTime());
      end = new Date(query.getData().getTime() + ONE_DAY);
    }

    Long oid = null;
    Long phone = null;

    if (null != query.getData().getPhone()) {
      Long number = query.getData().getPhone();
      if (null != number && (number + "").length() <= 7) {//为订单
        oid = number;
      } else if (null != number && (number + "").length() == 11) {//手机号
        phone = number;
      }
    }

    //设置查询参数
    OrderVO orderVO = new OrderVO();
    orderVO.setBegin(begin);
    orderVO.setEnd(end);
    orderVO.setId(oid);
    orderVO.setChannelType(query.getData().getOffLine() ? (short) 2 : (short) 1);

    //如果通过手机号查询
    if (null != phone) {
      logger.error("phone=" + phone);
      Result<? extends BaseUserDTO> userByPhoneResult = userService
          .queryUserByPhone(phone, false);
      logger.error("结果="+JackSonUtil.getJson(userByPhoneResult));
      if (null != userByPhoneResult && userByPhoneResult.getSuccess() && null != userByPhoneResult
          .getModule()) {
        Long userId = userByPhoneResult.getModule().getUserId();
        if (null != userId && userId > 0) {
          orderVO.setUserId(userId);
        }
      } else {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该手机号不存在");
      }
    }

    logger.error("参数==" + JackSonUtil.getJson(orderVO));

    try {
      //查询订单
      listResult = orderService.getOrdersByVO(orderVO);
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("调用订单服务失败");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "调用订单服务失败");
    }

    logger.error("请求结果=" + JackSonUtil.getJson(listResult));

    if (null == listResult || CollectionUtils.isEmpty(listResult.getModel())) {
      logger.error("查询订单信息不存在");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询订单信息不存在");
    }

    List<OrderVO> listResultModel = listResult.getModel();

    List<OrderReturnVO> orderList = new ArrayList<>();
    for (OrderVO orderVOs : listResultModel) {

      if (null == orderVOs) {
        continue;
      }

      if (!(orderVOs.getStatus() >= 4 && orderVOs.getStatus() <= 7)) {
        continue;
      }

      //转移itemList数据  过滤出4万到5万 类目id集合
      logger.error("订单id=" + orderVOs.getId());
      List<OrderItemInfoVO> targetItemList = getTargetItemList(orderVOs.getItems(), catIdList);
      if (CollectionUtils.isEmpty(targetItemList)) {
        continue;
      }

      OrderReturnVO orderSVO = new OrderReturnVO();
      BeanCopierUtils.copyProperties(orderVOs, orderSVO);//订单基本数据

      //设置tagStatus
      orderSVO.setTagStatus(getRightTagStatus(orderVOs.getTag()));

      //判断订单是否取消
      if (orderVOs.getStatus() != null && OrderStatus.CLOSED.getValue() == orderVOs.getStatus()) {
        orderSVO.setTagStatus(-1);
      }

      //积分折扣
      orderSVO.setCreditDiscount(getCreditDiscount(orderVOs.getCredit()));

      //转移item数据
      orderSVO.setItemsList(targetItemList);

      //设置配送信息
      DistributionVO distributionVO = getDistributionInfo(orderVOs.getId());
//      DistributionVO distributionVO = getDistributionInfo(12172L);
      if (null != distributionVO) {
        orderSVO.setDistributinaAddress(distributionVO.getUserAddress());//配送地址
        orderSVO.setDistributionPhone(distributionVO.getUserPhone());//手机号码
        orderSVO.setGmtDistribution(distributionVO.getAppointTime());//送达时间 null 表示立即送达
      }

      //会员手机号
      try {
        Result<? extends BaseUserDTO> userByUserId = userService
            .queryUserByUserId(orderVOs.getUserId(), false);
        if (null != userByUserId && userByUserId.getSuccess() && null != userByUserId
            .getModule()) {
          orderSVO.setPhoneNumber(userByUserId.getModule().getPhone());//会员号码
        }
      } catch (Exception e) {
        logger.error("调用userService服务失败");
        return Result
            .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, e.getMessage());
      }

      //设置订单折扣
      com.xianzaishi.trade.client.Result<List<OrderDiscountVO>> discountResult = null;
      try {
        discountResult = orderService
            .getOrderDiscountVOByOrderIdAndType(orderSVO.getId(), (short) 0);
      } catch (Exception e) {
        logger.error("调用orderService服务异常，" + e.getMessage());
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
      }
      if (discountResult != null && discountResult.isSuccess() && null != discountResult
          .getModel()) {
        //设置优惠券折扣
        List<OrderDiscountVO> orderDiscountVOList = discountResult.getModel();
        if (CollectionUtils.isNotEmpty(orderDiscountVOList)) {
          OrderDiscountVO orderDiscountVO = orderDiscountVOList.get(0);
          Integer price = orderDiscountVO.getPrice();
          Integer originPrice = orderDiscountVO.getOriginPrice();
          if (null != price && null != originPrice) {
            orderSVO.setDiscountAmount(
                BigDecimal.valueOf(originPrice - price).divide(new BigDecimal(100)).toString());
          } else {
            orderSVO.setDiscountAmount("0.00");
          }
        }
      } else {
        logger.error("查询订单优惠折扣失败");
        orderSVO.setDiscountAmount("0.00");
      }

      orderList.add(orderSVO);
    }

    logger.error("orderList集合大小=" + orderList.size());

    if (CollectionUtils.isEmpty(orderList)) {
      return Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "查询订单信息为空");
    }

    //设置data
    HashMap<String, List<OrderReturnVO>> map = Maps.newHashMap();
    map.put("orders", orderList);

    return Result.getSuccDataResult(map);
  }

  /**
   * 订单加工完成
   */
  public Result<Boolean> updateOrder(ERPQueryVO query) {

    if (null == query || null == query.getData() || null == query.getData().getOrderId()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "订单id不正确");
    }

    Long orderId = query.getData().getOrderId();//订单id
    com.xianzaishi.trade.client.Result<OrderVO> orderVOResult = null;
    try {
      OrderVO orderVO = new OrderVO();
      orderVO.setId(orderId);
      logger.error("订单id=" + orderId);
//      orderVOResult = orderService.getOrdersByVO(orderVO);
       orderVOResult = orderService.getOrder(orderId);
      logger.error("结果=" + JackSonUtil.getJson(orderVOResult));
    } catch (Exception e) {
      logger.error("调用订单服务失败"+e.getMessage());
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

    if (null != orderVOResult && orderVOResult.isSuccess() && null != orderVOResult.getModel()) {
//      OrderVO orderVO = orderVOResult.getModel().get(0);
      OrderVO orderVO = orderVOResult.getModel();
      Long id = orderVO.getId();
      if (null != id) {
        Long tag = orderVO.getTag();
        logger.error("tag=" + tag);
        if (null != tag) {
          if (((long) (tag & 0x1)) == 0 && (long) (tag & 0x2) == 0) {//是待处理状态，并且第二位是0,表示为处理完成
            long b = (long) tag | 0x2;
            logger.error("单与 =" + b);
            com.xianzaishi.trade.client.Result<Boolean> booleanResult = null;
            OrderVO vo = new OrderVO();
            vo.setTag(b);
            vo.setId(orderId);
            try {
              logger.error("参数=" + JackSonUtil.getJson(vo));
              booleanResult = orderService
                  .updateOrderVO(vo);
            } catch (Exception e) {
              logger.error("调用订单服务失败");
              return Result
                  .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "调用订单服务失败");
            }
            if (null != booleanResult && null != booleanResult.getModel() && booleanResult
                .getModel()) {
              return Result.getSuccDataResult(true);
            } else {
              return Result
                  .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新订单状态失败");
            }
          } else {
            return Result
                .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该订单不是待处理状态");
          }
        } else {
          return Result
              .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该订单标记不正确 null");
        }
      }
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新订单状态失败！");
  }

  /**
   * 根据订单获取配送单信息
   */
  public DistributionVO getDistributionInfo(Long ordreId) {
    if (null != ordreId && (long) ordreId > 0) {

      SimpleResultVO<List<DistributionVO>> simpleResultVO = null;
      try {
        simpleResultVO = distributionDomainClient
            .getDistributionByOrderID(ordreId);
      } catch (Exception e) {
        logger.error("调用distributionDomainClient服务失败");
        return null;
      }
      if (null != simpleResultVO && simpleResultVO.isSuccess() && null != simpleResultVO.getTarget()
          && simpleResultVO.getTarget().size() > 0) {

        DistributionVO distributionVO = simpleResultVO.getTarget().get(0);

        logger.error("根据订单获取配送单信息成功");
        return distributionVO;

      } else {
        logger.error("根据订单获取配送单信息失败");
        return null;
      }
    } else {
      return null;
    }
  }

  /**
   * copy 订单中itemList
   */
  public List<OrderItemInfoVO> getTargetItemList(List<OrderItemVO> srcItems,List<Integer> catIdList) {
    if (CollectionUtils.isEmpty(srcItems) || CollectionUtils.isEmpty(catIdList)) {
      logger.error("srcItems 或类目集合为空");
      return null;
    }

    //转移item数据
    List<OrderItemInfoVO> itemTarList = new ArrayList<>();
    for (OrderItemVO itemVO : srcItems) {
      if (null == itemVO) {
        continue;
      }

      //过滤sku
      if (null != itemVO.getSkuId() && itemVO.getSkuId() >= 40000 && itemVO.getSkuId() <= 50000) {

        Result<ItemBaseDTO> itemBaseDTOResult = null;
        try {
          ItemQuery itemQuery = new ItemQuery();
          itemQuery.setItemId(itemVO.getItemId());//商品id

          itemBaseDTOResult = commodityService.queryCommodity(itemQuery);

        } catch (Exception e) {
          logger.error("调用commodityService服务失败"+e.getMessage());
          e.printStackTrace();
          return null;
        }

        //-----------------------------------------------------------

        if (null != itemBaseDTOResult && itemBaseDTOResult.getSuccess() && null != itemBaseDTOResult
            .getModule()) {
          ItemBaseDTO itemBaseDTO = itemBaseDTOResult.getModule();
          Integer catId = itemBaseDTO.getCategoryId();//cat_id
          //判断是否含有
          if (null != catId &&  catIdList.contains(catId)) {
            if (catIdList.contains(catId)) {
              OrderItemInfoVO itemInfoVO = new OrderItemInfoVO();
              BeanCopierUtils.copyProperties(itemVO, itemInfoVO);

              //copy sku
              SkuInfoVO srcSkuInfo = itemVO.getSkuInfo();
              SkuInformationVO tarskuInformationVO = new SkuInformationVO();
              BeanCopierUtils.copyProperties(srcSkuInfo, tarskuInformationVO);
              itemInfoVO.setSkuInfos(tarskuInformationVO);

              //添加对象
              itemTarList.add(itemInfoVO);
            }
          } else {
            //不包含，查询parentId
            Result<CategoryDTO> categoryDTOResult = null;
            try {
              categoryDTOResult = stdcategoryservice.queryCategoryById(catId);
            } catch (Exception e) {
              logger.error("调用stdcategoryservice服务失败");
              e.printStackTrace();
              logger.error(e.getMessage());
              return null;
            }

            if (null != categoryDTOResult && categoryDTOResult.getSuccess()
                && null != categoryDTOResult
                .getModule()) {

              CategoryDTO categoryDTO = categoryDTOResult.getModule();
              Integer cattogryId = categoryDTO.getCatId();
              Integer parentId = categoryDTO.getParentId();//parentId

              Boolean has = containsCatId(cattogryId, parentId,catIdList);//判断是否符合要求catId

              if (has) {
                OrderItemInfoVO itemInfoVO = new OrderItemInfoVO();
                BeanCopierUtils.copyProperties(itemVO, itemInfoVO);

                //copy sku
                SkuInfoVO srcSkuInfo = itemVO.getSkuInfo();
                SkuInformationVO tarskuInformationVO = new SkuInformationVO();
                BeanCopierUtils.copyProperties(srcSkuInfo, tarskuInformationVO);
                itemInfoVO.setSkuInfos(tarskuInformationVO);

                //添加对象
                itemTarList.add(itemInfoVO);
              }
            }
          }
        }

      } else {
//        logger.error("skuid 不在 4万和5万之间");
      }
    }

    return itemTarList;
  }

  /**
   * 判断是否含有目录id
   */
  public Boolean containsCatId(Integer catId, Integer parentId,List<Integer> catIdList) {

    if (null == catId || null == parentId || CollectionUtils.isEmpty(catIdList)) {
      return false;
    }

    //本catid符合
    if (catIdList.contains(catId) || catIdList.contains(parentId)) {
      return true;
    } else {
      //不包含
      if (parentId == 0) {
        return false;
      }

      Result<CategoryDTO> parentResult = null;
      try {
        parentResult = stdcategoryservice
            .queryCategoryById(parentId);
      } catch (Exception e) {
        logger.error("调用stdcategoryservice服务失败");
        return false;
      }

      if (null != parentResult && parentResult.getSuccess() && null != parentResult.getModule()) {
        CategoryDTO categoryDTO = parentResult.getModule();
        if (null != categoryDTO) {
          return containsCatId(categoryDTO.getCatId(), categoryDTO.getParentId(), catIdList);
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  /**
   * 获取类目id集合
   */
  public List<Integer> getCategoryList() {
    ArrayList<Integer> list = new ArrayList<>();
    list.add(436);
    list.add(481);

    return list;
  }


  /**
   * copy 订单中物流集合
   */
  public List<LogisticalInfoVO> getTargetLogistList(List<LogisticalVO> srcLogisticalList) {
    if (null == srcLogisticalList || srcLogisticalList.size() == 0) {
      return null;
    }

    //转移数据
    List<LogisticalInfoVO> logistTarList = new ArrayList<>();
    for (LogisticalVO logitVO : srcLogisticalList) {
      if (null == logitVO) {
        continue;
      }
      LogisticalInfoVO itemInfoVO = new LogisticalInfoVO();
      BeanCopierUtils.copyProperties(logitVO, itemInfoVO);
      logistTarList.add(itemInfoVO);
    }
    logger.error("集合itemTarList大小=" + logistTarList.size());
    return logistTarList;
  }

  /**
   * 获取配送单状态
   */
  public String getPeisongString(Integer status) {

    if (null == status) {
      return "无";
    }

    String a = "";
//    0，创建成功，1，进入等待队列，2，波次创建完成，3，送货中，4，送货完成，-1，挂起
    switch (status) {
      case 0:
        a = "创建成功";
        break;
      case 1:
        a = "进入等待队列";
        break;
      case 2:
        a = "波次创建完成";
        break;
      case 3:
        a = "送货中";
        break;
      case 4:
        a = "送货完成";
        break;
      case -1:
        a = "挂起";
        break;
    }
    return a;
  }


  /**
   * 判断是个时间戳是否是今天
   */
  public Boolean isToday(Long time) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    if (format.format(time).toString()
        .equals(format.format(new Date()).toString())) {
      return true;
    } else {
      return false;
    }
  }

  public int getRightTagStatus(Long tag) {
    if (null == tag) {
      return 0;
    }

    if ((tag & 0x1) == 0 && (tag & 0x2) == 0) {
      return 0;//待处理
    }
    if ((tag & 0x1) == 1 && (tag & 0x2) == 0) {
      return 1;//处理中
    }
    if ((tag & 0x2) == 2) {
      return 2;//加工完成
    }

    return 0;
  }

  /**
   * 店内<br/>
   * 登录失败，返回前台错误信息
   */
  public Result<Boolean> verifyPaySign(String sign,String token,Long orderId) {
    return null;
  }
  /**
   *
   * @param query
   * @return
   */
  public Result<Boolean> sendCode(EmployeeQueryVO query) {
    if (null == query || null == query.getData() || null == query.getData().getPhone()) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "参数异常");
    }

    logger.error("手机号=============" + query.getData().getPhone());

    //查询用户
    Result<BackGroundUserDTO> userDTOResult = null;

    userDTOResult = backgrounduserservice
        .queryUserDTOByPhone(query.getData().getPhone());

    if (null == userDTOResult || !userDTOResult.getSuccess() || null == userDTOResult.getModule()) {
      return Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "该员工未注册");
    }

    //判断是否是店内员工
    if (!UserTypeConstants.USER_EMPLOYEE.equals(userDTOResult.getModule().getUserType())) {
      return Result
          .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "该用户不是店内用户");
    }

    //获取名称
    String name = userDTOResult.getModule().getName();

    logger.error("姓名name=" + name);

    //发送验证码
    Result<Boolean> booleanResult = resetUserPwdSendToken(name);
    if (null == booleanResult || !booleanResult.getSuccess() || null == booleanResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "发送验证码失败");
    }

    return Result.getSuccDataResult(true);
  }
}
