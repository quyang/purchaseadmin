package com.xianzaishi.purchaseadmin.component.trade;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.purchaseadmin.client.trade.vo.OrderStatisticsVO;
import com.xianzaishi.purchaseadmin.client.trade.vo.TradeOrderTagVo;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.OrderInfoSumVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.ObjectUtils;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("tradeOrderComponent")
public class TradeOrderComponent {

  private static final Logger logger = Logger.getLogger(TradeOrderComponent.class);

  @Autowired
  private OrderService orderService;
  
  public OrderVO getOrder(Long orderId){
    Result<OrderVO> orderResult = orderService.getOrder(orderId);
    if(null != orderResult && orderResult.getCode() > 0 && orderResult.getModel() != null){
      return orderResult.getModel();
    }else{
      return null;
    }
  }
  
  public OrderVO synOrder(Long orderId, short payway){
    Result<OrderVO> orderResult = orderService.synOrder(orderId, payway);
    if(null != orderResult && orderResult.getCode() > 0 && orderResult.getModel() != null){
      return orderResult.getModel();
    }else{
      return null;
    }
  }
  
  
  public List<OrderStatisticsVO> getOrderInfoSumInfoList(Long userId,Long start,Long end){
    Result<List<OrderInfoSumVO>> orderResult = orderService.getOrderSumInfo(userId, new Date(start), new Date(end));
    if(null != orderResult && orderResult.getCode()==1 && CollectionUtils.isNotEmpty(orderResult.getModel())){
      List<OrderInfoSumVO> orderList = orderResult.getModel();
      List<OrderStatisticsVO> result = Lists.newArrayList();
      for(OrderInfoSumVO sum:orderList){
        OrderStatisticsVO resultStatistics = new OrderStatisticsVO();
        BeanCopierUtils.copyProperties(sum, resultStatistics);
        resultStatistics.setUserId(userId);
        resultStatistics.setStart(start);
        resultStatistics.setEnd(end);
        result.add(resultStatistics);
      }
      return result;
    }
    return Lists.newArrayList();
  }
  
  public OrderStatisticsVO getOrderInfoSumInfoTotal(Long userId,Long start,Long end){
    List<OrderStatisticsVO> listResult = getOrderInfoSumInfoList(userId,start,end);
    if(CollectionUtils.isEmpty(listResult)){
      return null;
    }
    OrderStatisticsVO result = new OrderStatisticsVO();
    for(OrderStatisticsVO tmp:listResult){
      result.setAccountnum(result.getAccountnum()+tmp.getAccountnum());
      result.setAlipay(result.getAccountnum()+tmp.getAlipay());
      result.setAlipayfula(result.getAlipayfula()+tmp.getAlipayfula());
      result.setAlipaypos(result.getAlipaypos()+tmp.getAlipaypos());
      result.setCash(result.getCash()+tmp.getCash());
      result.setCoupon(result.getCoupon()+tmp.getCoupon());
      result.setCredit(result.getCredit()+tmp.getCredit());
      result.setKoubei(result.getKoubei()+tmp.getKoubei());
      result.setWeixinpay(result.getWeixinpay()+tmp.getWeixinpay());
      result.setWeixinpayfula(result.getWeixinpayfula()+tmp.getWeixinpayfula());
      result.setWeixinpaypos(result.getWeixinpaypos()+tmp.getWeixinpaypos());
      result.setYintong(result.getYintong()+tmp.getYintong());
    }
    return result;
  }

  /**
   * 给订单打标
   */
  public Boolean setTag2Order(TradeOrderTagVo data) {
    data.setAdd(true);
    data.setTag(32);
    ObjectUtils.isNull(data, "参数对象为空");
    ObjectUtils.isNull(data.getOrderId(),"订单id不能为空");
    ObjectUtils.isNull(data.getAdd(), "add参数不能为空");
    ObjectUtils.isNull(data.getTag(), "标不能为空");

    logger.error("参数=" + JackSonUtil.getJson(data));
    //打标
    Result<Boolean> booleanResult = orderService
        .updateOrderTag(Long.parseLong(data.getOrderId() + ""), data.getTag(), data.getAdd());
    logger.error("更新结果=" + JackSonUtil.getJson(booleanResult));
    notSuccess(booleanResult, "修改标失败");
    return true;
  }

  public void notSuccess(Result result, String showmsg) {
    ObjectUtils.isNull(result, "result结果为空");
    if (!result.isSuccess()) {
      throw new BizException(showmsg);
    }
  }
}
