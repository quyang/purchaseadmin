package com.xianzaishi.purchaseadmin.component.category;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

/**
 * 导入值数据
 * @author dongpo
 *
 */
@Component("valueInsertComponent")
public class ValueInsertComponent {

  @Autowired
  private ValueService valueService;
  
  @Autowired
  private StdCategoryService stdcategoryservice;
  
  
  public String insertValueData(Integer catId, Integer propertyId,List<String> valueData) {
    int count = 0;
    for (int i = 0; i < valueData.size(); i++) {
      ValueDTO valueDTO = new ValueDTO();
      valueDTO.setCatId(catId);
      valueDTO.setPropertyId(propertyId);
      valueDTO.setCreatorId(1);
      valueDTO.setStatus((short) 1);
      valueDTO.setValueData(valueData.get(i));
      valueDTO.setAlias(valueData.get(i));
      valueDTO.setMemo(valueData.get(i));
      valueDTO.setSortValue((short) (i+1));
      Result<Integer> insertResult = valueService.insertPropertyValue(valueDTO);
      if(insertResult.getModule() > 0){
        count ++;
      }
    }
    return toJsonData(Result.getSuccDataResult(count));
  }
  
  public String updateValueData(String oldValue,String newValue){
    if(null == oldValue){
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Result<Integer> valueResult = valueService.queryValueIdByValueName(oldValue);
    Integer valueId = valueResult.getModule();
    ValueDTO valueDTO = new ValueDTO();
    valueDTO.setValueId(valueId);
    valueDTO.setCreatorId(1);
    valueDTO.setValueData(newValue);
    valueDTO.setAlias(newValue);
    valueDTO.setMemo(newValue);
    Result<Boolean> updateResult = valueService.updatePropertyValue(valueDTO);
    if(!updateResult.getSuccess()){
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "数据更新失败"));
    }
    
    
    return toJsonData(updateResult);
  }
  
  public String addValueDataWithDeal(String value, Integer propertyId){
    Result<List<CategoryDTO>> categoryResult = stdcategoryservice.queryCategoriesByCatCode(4L);
    if(!categoryResult.getSuccess()){
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "没有找到相关的类目"));
    }
    List<CategoryDTO> categoryDTOs = categoryResult.getModule();
    if(null == categoryDTOs || categoryDTOs.size() == 0){
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "没有找到相关的类目"));
    }
    String result = null;
    for(CategoryDTO categoryDTO : categoryDTOs){
      result = insertValueData(categoryDTO.getCatId(), propertyId, Arrays.asList(value));
    }
    
    return result;
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

}
