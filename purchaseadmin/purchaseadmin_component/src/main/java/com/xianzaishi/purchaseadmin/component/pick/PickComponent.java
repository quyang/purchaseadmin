package com.xianzaishi.purchaseadmin.component.pick;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.pick.vo.PickTokenVO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickDetailVO;
import com.xianzaishi.wms.tmscore.vo.PickQueryVO;
import com.xianzaishi.wms.tmscore.vo.PickVO;

@Component("pickComponent")
public class PickComponent extends BasePickComponent {

  private static final Logger logger = Logger.getLogger(PickComponent.class);

  @Autowired
  private IPickDomainClient pickDomainClient;

  @Autowired
  private IInventory2CDomainClient iInventory2CDomainClient;
  
  @Autowired
  private SkuService skuService;
  
  /**
   * 获取拣货任务
   * 
   * @param token
   * @return
   */
  public Result<PickVO> getPickedTask(BackGroundUserDTO user) {
    if (null == user) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "用户已失效，请重新登陆");
    }
    try {
      PickVO pickVO = null;
      PickTokenVO tokenVO = getTokenInfo(user);

      PickQueryVO queryVO = new PickQueryVO();
      queryVO.setAgencyId(tokenVO.getAgencyID());
      queryVO.setOperator(tokenVO.getOperator());
      queryVO.setStatu(Short.parseShort("1"));

      SimpleResultVO tmps = pickDomainClient.queryPickVOList(queryVO);

      if (null == tmps || !tmps.isSuccess()) {
        String errorMsg = null == tmps ? "网络连接失败，请稍后再试" : tmps.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      if (tmps.getTarget() == null || ((List<PickVO>) tmps.getTarget()).isEmpty()) {
        tmps = pickDomainClient.assignPickVO(tokenVO.getAgencyID(), tokenVO.getOperator());
        if (null == tmps || !tmps.isSuccess()) {
          String errorMsg = null == tmps ? "网络连接失败，请稍后再试" : tmps.getMessage();
          return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
        } else {
          pickVO = (PickVO) tmps.getTarget();
        }
      } else {
        pickVO = ((List<PickVO>) tmps.getTarget()).get(0);
      }
      if (pickVO != null) {
        List<PickDetailVO> pickDetailList = pickDomainClient.getPickDetailVOListByPickID(pickVO.getId()).getTarget();
        appendInventoryInfoToPickVO(pickDetailList);
        appendISkuInfoToPickVO(pickDetailList);
        pickVO.setDetails(pickDetailList);
      }
      return Result.getSuccDataResult(pickVO);
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
  }

  private void appendInventoryInfoToPickVO(List<PickDetailVO> pickDetailList){
    if(CollectionUtils.isEmpty(pickDetailList)){
      return;
    }
    List<Long> skuIds = Lists.newArrayList();
    Map<Long,PickDetailVO> detailM = Maps.newHashMap();
    for(PickDetailVO detail:pickDetailList){
      skuIds.add(detail.getSkuId());
      detailM.put(detail.getSkuId(), detail);
    }
    
    SimpleResultVO<List<InventoryVO>>  result = iInventory2CDomainClient.batchGetInventoryBySKUID(1L, skuIds);
    logger.error(JackSonUtil.getJson(result)+",para:"+JackSonUtil.getJson(skuIds));
    if(null != result && result.isSuccess() && CollectionUtils.isNotEmpty(result.getTarget())){
      for(InventoryVO inv:result.getTarget()){
        Long skuId = inv.getSkuId();
        String showNum = inv.getNumberShow();
        String safeNum = inv.getGuardBit();
        int showNumInt = new BigDecimal(showNum).multiply(new BigDecimal(1000)).intValue();
        int safeNumInt = new BigDecimal(safeNum).multiply(new BigDecimal(1000)).intValue();
        int total = showNumInt + safeNumInt;
        String totleNum = new BigDecimal(total).divide(new BigDecimal(1000)).toString();
        PickDetailVO detail = detailM.get(skuId);
        if(null != detail){
          detail.setTotalStocNum(totleNum);
          detail.setSafeStocNum(safeNum);
        }
      }
    }
  }
  
  private void appendISkuInfoToPickVO(List<PickDetailVO> pickDetailList){
    if(CollectionUtils.isEmpty(pickDetailList)){
      return;
    }
    List<Long> skuIds = Lists.newArrayList();
    Map<Long,PickDetailVO> detailM = Maps.newHashMap();
    for(PickDetailVO detail:pickDetailList){
      skuIds.add(detail.getSkuId());
      detailM.put(detail.getSkuId(), detail);
    }
    
    Result<List<ItemCommoditySkuDTO>>  skuResult = skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
    if(null != skuResult && skuResult.getSuccess() && CollectionUtils.isNotEmpty(skuResult.getModule())){
      for(ItemCommoditySkuDTO sku:skuResult.getModule()){
        Long skuId = sku.getSkuId();
        String skuCode = sku.getSku69code().toString();
        PickDetailVO detail = detailM.get(skuId);
        if(null != detail){
          detail.setSkuBarcode(skuCode);
        }
      }
    }
  }
  /**
   * 提交拣货详细
   * 
   * @param pickDetailVO
   * @param user 
   * @return
   */
  public Result<Boolean> submitPickedDetail(PickDetailVO pickDetailVO, BackGroundUserDTO user) {
    if (null == pickDetailVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    try {
      PickTokenVO tokenVO = getTokenInfo(user);
      List<PickDetailVO> pickDetailVOs = Arrays.asList(pickDetailVO);
      PickQueryVO queryVO = new PickQueryVO();
      queryVO.setOperator(tokenVO.getOperator());
      queryVO.setId(pickDetailVO.getPickId());
      SimpleResultVO<List<PickVO>> pickVOs = pickDomainClient.queryPickVOList(queryVO);
      if (pickVOs.getTarget().get(0).getStatu() != 1) {
        throw new BizException("请确认该拣货单任务是否已完成");
      }
      SimpleResultVO<Boolean> submitPikedResult =
          pickDomainClient.submitPickedDetail(pickDetailVOs);
      if (null == submitPikedResult || !submitPikedResult.isSuccess()) {
        String errorMsg =
            null == submitPikedResult ? "网络连接失败，请稍后再试" : submitPikedResult.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
      }
      return Result.getSuccDataResult(submitPikedResult.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }

  }

  public Object getPickedTaskCount(BackGroundUserDTO user) {
    if (null == user) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "用户已失效，请重新登陆");
    }
    try {
      PickTokenVO tokenVO = getTokenInfo(user);

      PickQueryVO queryVO = new PickQueryVO();
      queryVO.setAgencyId(tokenVO.getAgencyID());
      queryVO.setOperator(tokenVO.getOperator());
      queryVO.setStatuList(Arrays.asList((short) 0, (short) 1));

      SimpleResultVO<Integer> tmps = pickDomainClient.queryPickVOCount(queryVO);
      if (null == tmps || !tmps.isSuccess()) {
        String errorMsg = null == tmps ? "网络连接失败，请稍后再试" : tmps.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      logger.error("Get pickcount result end is:" + JackSonUtil.getJson(tmps.getTarget()));
      return Result.getSuccDataResult(tmps.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
  }


}
