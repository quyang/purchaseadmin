package com.xianzaishi.purchaseadmin.component.steelyard;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.result.Result;

@Component("steelyardcomponent")
public class SteelyardComponent {
  
  @Autowired
  private SkuService skuService;
  
  private static final Long MODIFIED_TIME = 900000L;//15分钟内的更新数据
  
  /**
   * 获取全部的商品数据
   * @param pageNum
   * @return
   */
  public List<ItemCommoditySkuDTO> queryCommodiesAll(Integer pageNum) {
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySku(100, pageNum));
    if (!itemCommoditySkuResult.getSuccess()) {
      return null;
    }
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
    return itemCommoditySkuDTOs;
  }

  /**
   * 获取规定时间内修改的商品数据
   * @return
   */
  public List<ItemCommoditySkuDTO> queryItemCommodies() {
    Date now = new Date();
    Date modifiedTime = new Date(now.getTime() - MODIFIED_TIME);
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.queryByModifiedTime(modifiedTime));
    if (!itemCommoditySkuResult.getSuccess()) {
      return null;
    }
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTOs || itemCommoditySkuDTOs.size() == 0) {
      return null;
    }
    return itemCommoditySkuDTOs;
  }

}
