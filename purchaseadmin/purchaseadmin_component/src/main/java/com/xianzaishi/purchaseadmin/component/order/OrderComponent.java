package com.xianzaishi.purchaseadmin.component.order;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.tmporder.OrderTmpService;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.item.vo.PurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SubPurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.order.vo.OrderVO;
import com.xianzaishi.purchaseadmin.client.order.vo.SubOrderVO;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseOrderAuditingStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;

/**
 * 下采购订单接口实现
 * 
 * @author dongpo
 * 
 */
@Component("orderComponent")
public class OrderComponent {

  @Autowired
  private PurchaseService purchaseService;

  @Autowired
  private SkuService skuService;

  @Autowired
  private SupplierService supplierService;

  @Autowired
  private ValueService valueService;

  @Autowired
  private OrderTmpService orderTmpService;

  private static final Logger LOGGER = Logger.getLogger(OrderComponent.class);

  // /**
  // * 根据起始skuId和结束skuId查询
  // *
  // * @param startSkuId 起始skuId
  // * @param endSkuId 结束skuId
  // * @return
  // */
  // public String queryPurchaseBySkuId(Long startSkuCode, Long endSkuCode) {
  // Result<List<ItemProductSkuDTO>> itemProductSkuResult = null;
  // if (null == startSkuCode || startSkuCode.equals(null)) {
  // return toJsonData(Result
  // .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
  // } else if (null == endSkuCode) {
  // itemProductSkuResult = skuService.queryProductSku(SkuQuery.querySkuById(startSkuCode));//
  // 没有结束skuCode的时候,以起始skuCode查询
  // } else if (null != startSkuCode && null != endSkuCode) {
  // itemProductSkuResult =
  // skuService.queryProductSku(SkuQuery.querySkuRange(startSkuCode, endSkuCode, 100, 0));
  // }
  // OrderVO orderVO = new OrderVO();
  // if (null != itemProductSkuResult) {
  // List<ItemProductSkuDTO> itemProductSkuDTOs = itemProductSkuResult.getModule();// 后台查询商品sku
  // List<SubOrderVO> subOrderVOs = Lists.newArrayList();
  // getSubOrders(itemProductSkuDTOs, subOrderVOs);
  // if (null != subOrderVOs) {
  // orderVO.setSubOrderVOs(subOrderVOs);
  // }
  // }
  //
  // return toJsonData(Result.getSuccDataResult(orderVO));
  // }


  // /**
  // * 根据商品关键字和供应商查询采购单
  // *
  // * @param supplier
  // * @param itemKeyWord
  // * @return
  // */
  // public String queryPurchaseByKeyWord(String supplier, String itemKeyWord) {
  // Result<OrderVO> result = null;
  // if (null == supplier && null == itemKeyWord) {
  // return toJsonData(Result
  // .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
  // } else if (null != supplier && !supplier.equals(null)) {
  // result = createSubOrderBySupplier(supplier);
  // } else if (null != itemKeyWord && !itemKeyWord.equals(null)) {
  // result = createSubOrderByItem(itemKeyWord);
  // }
  // return toJsonData(result);
  // }


  // /**
  // * 根据供应商查询订单
  // *
  // * @param supplier
  // * @return
  // */
  // private Result<OrderVO> createSubOrderBySupplier(String supplier) {
  // if (null == supplier) {
  // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
  // "supplier is null");
  // }
  //
  // Result<SupplierDTO> supplierResult = supplierService.querySupplierByName(supplier, false);
  // if (null == supplierResult || !supplierResult.getSuccess()
  // || null == supplierResult.getModule()) {
  // return Result.getErrDataResult(ServerResultCode.SUPPLIER_UNEXIST, "没有找到与" + supplier
  // + "相关的供应商");
  // }
  // List<ItemProductSkuDTO> itemProductSkuDTOs = null;
  // Result<List<ItemProductSkuDTO>> proskuListResult =
  // skuService.queryProductSku(SkuQuery.querySkuBySupplier(supplierResult.getModule()
  // .getUserId()));
  // if (null != proskuListResult && proskuListResult.getSuccess()
  // && CollectionUtils.isNotEmpty(proskuListResult.getModule())) {
  // itemProductSkuDTOs = proskuListResult.getModule();
  // } else {
  // itemProductSkuDTOs = Collections.emptyList();
  // }
  //
  // if (CollectionUtils.isEmpty(itemProductSkuDTOs)) {
  // return Result.getErrDataResult(ServerResultCode.SUPPLIER_ITEM_PRODUCT_UNEXIST, "与" + supplier
  // + "相关的供应商下没有商品");
  // }
  // List<SubOrderVO> subOrderVOs = Lists.newArrayList();
  // getSubOrders(itemProductSkuDTOs, subOrderVOs);
  // OrderVO orderVO = new OrderVO();
  // orderVO.setSubOrderVOs(subOrderVOs);
  //
  // return Result.getSuccDataResult(orderVO);
  // }

  // /**
  // * 根据商品关键字查询采购单
  // *
  // * @param itemKeyWord
  // * @return
  // */
  // private Result<OrderVO> createSubOrderByItem(String itemKeyWord) {
  // if (null == itemKeyWord) {
  // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
  // "itemKeyWord is null");
  // }
  // Result<List<ItemProductSkuDTO>> itemResult =
  // skuService.queryProductSku(SkuQuery.querySkuByTitle(itemKeyWord, 200, 0));
  // List<ItemProductSkuDTO> itemProductSkuDTOs = null;
  // if (null != itemResult && itemResult.getSuccess()
  // && CollectionUtils.isNotEmpty(itemResult.getModule())) {
  // itemProductSkuDTOs = itemResult.getModule();
  // } else {
  // itemProductSkuDTOs = Collections.emptyList();
  // }
  // if (CollectionUtils.isEmpty(itemProductSkuDTOs)) {
  // return Result
  // .getErrDataResult(ServerResultCode.ITEM_UNEXIST, "没有找到与" + itemKeyWord + "相关的商品");
  // }
  // List<SubOrderVO> subOrderVOs = Lists.newArrayList();
  // getSubOrders(itemProductSkuDTOs, subOrderVOs);
  // OrderVO orderVO = new OrderVO();
  // orderVO.setSubOrderVOs(subOrderVOs);
  // return Result.getSuccDataResult(orderVO);
  // }


  // /**
  // * 商品sku数据转换为前端可视化数据
  // *
  // * @param itemProductSkuDTOs
  // * @param subOrderVOs
  // */
  // private void getSubOrders(List<ItemProductSkuDTO> itemProductSkuDTOs, List<SubOrderVO>
  // subOrderVOs) {
  // if (null == itemProductSkuDTOs) {
  // return;
  // }
  // for (ItemProductSkuDTO itemProductSkuDTO : itemProductSkuDTOs) {
  // SubOrderVO subOrderVO = new SubOrderVO();
  // subOrderVO.setSkuId(itemProductSkuDTO.getSkuId());
  // subOrderVO.setSkuCode(itemProductSkuDTO.getSkuCode());
  // Result<String> valueDataResult =
  // valueService.queryValueNameByValueId(itemProductSkuDTO.getSkuUnit());
  // String valueData = valueDataResult.getModule();
  // if (null == valueData) {
  // valueData = "箱";
  // }
  // subOrderVO.setSkuUnit(valueData);
  // subOrderVO.setTitle(itemProductSkuDTO.getTitle());
  // Long tomorrow = new Date().getTime() + 60 * 60 * 24;
  // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  // String expectArriveDate = sdf.format(new Date(tomorrow));
  // subOrderVO.setExpectArriveDate(expectArriveDate);
  // subOrderVOs.add(subOrderVO);
  // }
  //
  // }

  /**
   * 下采购单时插入数据
   * 
   * @param orderVO
   * @return
   */
  public String insertOrder(OrderVO orderVO) {
    if (null == orderVO) {
      return toJsonData(Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO();
    if (null != orderVO.getPurchasingAgent()
        && (null == orderVO.getPurchaseId() || orderVO.getPurchaseId() <= 0)) {
      purchaseOrderDTO.setPurchasingAgent(orderVO.getPurchasingAgent());
      purchaseOrderDTO.setSupplierId(orderVO.getUserId());
    }
    if (null != orderVO.getAuditingStatus()) {
      purchaseOrderDTO.setAuditingStatus(orderVO.getAuditingStatus());
    }
    purchaseOrderDTO.setRemarks(orderVO.getRemarks());
    
    //如果总采购单已经存在了则只需要更新即可
    if (null != orderVO.getPurchaseId() && orderVO.getPurchaseId() > 0) {
      Integer purchaseId = orderVO.getPurchaseId();
      PurchaseQuery query = new PurchaseQuery();
      query.setParentPurchaseId(purchaseId);
      PagedResult<List<PurchaseOrderDTO>> purchaseOrderResult = purchaseService.queryPurchase(query);
      if (null != purchaseOrderResult && purchaseOrderResult.getSuccess()
          && CollectionUtils.isNotEmpty(purchaseOrderResult.getModule())) {
        List<PurchaseOrderDTO> purchaseOrderDTOs = purchaseOrderResult.getModule();
        PurchaseOrderDTO purchaseOrderDTODb = purchaseOrderDTOs.get(0);
        Short auditingStatusDB = purchaseOrderDTODb.getAuditingStatus();
        if (auditingStatusDB.shortValue() >= PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS
            .shortValue()) {
          return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "已经提交供应商的采购订单不能再次提交审核"));
        }
      }
      purchaseOrderDTO.setPurchaseId(purchaseId);
      Result<Boolean> updateResult = purchaseService.updatePurchaseOrder(purchaseOrderDTO);
      if (null == updateResult || !updateResult.getSuccess() || null == updateResult.getModule()) {
        LOGGER.error("更新采购单失败，采购单id:" + purchaseId);
      }
      return toJsonData(Result.getSuccDataResult(purchaseId));
    }
    
    //否则插入一个总采购单以及子采购单对象
    long totalAmount = 0;
    List<PurchaseSubOrderDTO> purchaseSubOrderDTOs = getPurchaseSubOrder(orderVO);
    if(CollectionUtils.isNotEmpty(purchaseSubOrderDTOs)){
      for (PurchaseSubOrderDTO purchaseSubOrderDTO : purchaseSubOrderDTOs) {
        Integer subPurchaseCost =
            new BigDecimal(purchaseSubOrderDTO.getUnitCost()).multiply(
                new BigDecimal(purchaseSubOrderDTO.getCount())).intValue();
        totalAmount = new BigDecimal(subPurchaseCost).add(new BigDecimal(totalAmount)).longValue();
      }
    }
    purchaseOrderDTO.setTotalAmount(totalAmount);
    purchaseOrderDTO.setSubOrderList(purchaseSubOrderDTOs);
    Result<Integer> insertResult = purchaseService.insertPurchase(purchaseOrderDTO);// 插入数据
    if (null == insertResult || !insertResult.getSuccess() || null == insertResult.getModule()) {
      LOGGER.error("插入采购单失败");
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "网络连接失败，请稍后再试"));
    }

    return toJsonData(Result.getSuccDataResult(insertResult.getModule()));
  }

  /**
   * 前端VO数据转换为数据库中数据格式
   * 
   * @param purchasingAgent
   * @param itemProductSkuDTOs
   * @param orderVO
   * @param skuBoxCostMap
   * @return
   */
  private List<PurchaseSubOrderDTO> getPurchaseSubOrder(OrderVO orderVO) {
    if (null == orderVO) {
      return Lists.newArrayList();
    }
    List<Long> skuIds = Lists.newArrayList();
    List<SubOrderVO> subOrderVOs = orderVO.getItems();
    for (SubOrderVO subOrderVO : subOrderVOs) {
      skuIds.add(subOrderVO.getSkuId());
    }
    
    Map<Long, Integer> skuBoxCostMap = Maps.newHashMap();// 后台sku id和进货成本映射表
    Map<Long, Integer> specificationMap = Maps.newHashMap();// 前台skuid和进售属性映射表
    Map<Long, Long> cPSkuIdMap = Maps.newHashMap();// 后台sku id和前台sku id映射表
    List<Long> pskuIds = Lists.newArrayList();
    Result<List<ItemSkuRelationDTO>> skuRelationResult =
        skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(skuIds, (short) 1));
    if (null != skuRelationResult && skuRelationResult.getSuccess()
        && null != skuRelationResult.getModule()) {
      List<ItemSkuRelationDTO> itemSkuRelationDTOs = skuRelationResult.getModule();
      for (ItemSkuRelationDTO itemSkuRelationDTO : itemSkuRelationDTOs) {
        skuBoxCostMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getBoxCost());
        if (null == itemSkuRelationDTO.getPcProportion()) {
          itemSkuRelationDTO.setPcProportion(1000);
        }
        specificationMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getPcProportion());
        pskuIds.add(itemSkuRelationDTO.getPskuId());
        cPSkuIdMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getPskuId());
      }
    }
    // 获取后台商品规格属性
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
    Map<Long, ItemCommoditySkuDTO> commodityMap = Maps.newHashMap();// 前台sku id和后台规格映射关系表
    if (null != itemCommoditySkuResult && itemCommoditySkuResult.getSuccess()
        && null != itemCommoditySkuResult.getModule()) {
      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
      for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
        commodityMap.put(itemCommoditySkuDTO.getSkuId(), itemCommoditySkuDTO);
      }
    }
    List<PurchaseSubOrderDTO> purchaseSubOrderDTOs = Lists.newArrayList();
    for (int i = 0; i < subOrderVOs.size(); i++) {
      SubOrderVO subOrderVO = subOrderVOs.get(i);
      PurchaseSubOrderDTO purchaseSubOrderDTO = new PurchaseSubOrderDTO();
      Long commoditySkuId = subOrderVO.getSkuId();
      ItemCommoditySkuDTO itemCommoditySkuDTO = commodityMap.get(commoditySkuId);
      purchaseSubOrderDTO.setSkuId(subOrderVO.getSkuId());
      purchaseSubOrderDTO.setSupplierId(itemCommoditySkuDTO.getSupplierId());
      purchaseSubOrderDTO.setContractId(1);
      purchaseSubOrderDTO.setTitle(subOrderVO.getTitle());
      purchaseSubOrderDTO.setCheckinfo(subOrderVO.getCheckInfo());
      if (StringUtils.isNotBlank(subOrderVO.getWareHouse())) {
        purchaseSubOrderDTO.setWareHouse(Integer.valueOf(subOrderVO.getWareHouse()));
      }
      purchaseSubOrderDTO.setCount(subOrderVO.getCount());
      if (null == orderVO.getPurchaseId() || orderVO.getPurchaseId() <= 0) {
        purchaseSubOrderDTO.setPurchasingAgent(orderVO.getPurchasingAgent());
        purchaseSubOrderDTO.setCreator(orderVO.getPurchasingAgent());
      }
      Integer singleBoxPrice = skuBoxCostMap.get(commoditySkuId);
      if (null == singleBoxPrice) {
        singleBoxPrice = 0;
      }
      String saleUnitStr =
          (itemCommoditySkuDTO.getIsSteelyardSku() ? "公斤" : itemCommoditySkuDTO.getSaleUnitStr());
      purchaseSubOrderDTO
          .setSteelyardSku((short) (itemCommoditySkuDTO.getIsSteelyardSku() ? 1 : 0));
      purchaseSubOrderDTO.setPcProportionUnit(saleUnitStr);
      purchaseSubOrderDTO.setPcProportion(specificationMap.get(commoditySkuId));
      purchaseSubOrderDTO.setUnitCost(singleBoxPrice);
      purchaseSubOrderDTO.setPurchaseDate(new Date(orderVO.getPurchaseDate()));
      purchaseSubOrderDTO.setExpectArriveDate(new Date(orderVO.getExpectArriveDate()));
      purchaseSubOrderDTOs.add(purchaseSubOrderDTO);
    }

    return purchaseSubOrderDTOs;
  }


  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

  /**
   * 临时方法
   * 
   * @param start
   * @param end
   * @param userids
   * @return
   */
  public String getOrderInfo(String start, String end, String userids) {
    if (StringUtils.isEmpty(start)) {
      return "开始时间错误";
    }
    if (StringUtils.isEmpty(end)) {
      return "结束时间错误";
    }
    String startArray[] = start.split("-");
    String endArray[] = end.split("-");
    SimpleDateFormat sf1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date startTime = null;
    Date endTime = null;
    try {
      if (startArray.length == 3) {
        startTime = sf2.parse(start);
      } else {
        startTime = sf1.parse(start);
      }
      if (endArray.length == 3) {
        endTime = sf2.parse(end);
      } else {
        endTime = sf1.parse(end);
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    if (null == startTime) {
      return "开始时间格式化错误";
    }
    if (null == endTime) {
      return "结束时间格式化错误";
    }

    List<Long> userIds = Lists.newArrayList();
    String idarray[] = userids.split("-");
    for (String tmp : idarray) {
      userIds.add(Long.valueOf(tmp));
    }
    userids = "";
    for (Long idNum : userIds) {
      userids = userids + idNum + ",";
    }

    String startInfo = sf3.format(startTime);
    String endInfo = sf3.format(endTime);
    Map<String, String> input = Maps.newHashMap();
    input.put("start", startInfo);
    input.put("end", endInfo);
    input.put("ids", userids);
    String result = "";
    result =
        result + "query parameter[start:" + startInfo + ",endInfo:" + endInfo + ",userInfo:"
            + userids + "]<br/>";
    result = result + "query result<br/>" + orderTmpService.getOrderInfo(input);
    return result;
  }


  /**
   * 更新子采购订单
   * 
   * @param purchaseItemVO
   * @return
   */
  public String updateSubOrder(SubPurchaseItemVO purchaseItemVO) {
    if (null == purchaseItemVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Integer subPurchaseId = purchaseItemVO.getSubPurchaseId();
    Result<PurchaseSubOrderDTO> subPurchaseResult = purchaseService.querySubPurchase(subPurchaseId);
    if (null == subPurchaseResult || !subPurchaseResult.getSuccess()
        || null == subPurchaseResult.getModule()) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "子采购单数据不存在"));
    }
    
    if (PurchaseSubOrderStatusConstants.SUBORDER_STATUS_INVALID.equals(purchaseItemVO.getStatus())) {
      purchaseItemVO.setCount(0);
    }
    // 已经提交供应商的采购订单不能再修改
    PurchaseSubOrderDTO purchaseSubOrderDTO = subPurchaseResult.getModule();
    PurchaseQuery query = new PurchaseQuery();
    query.setParentPurchaseId(purchaseSubOrderDTO.getPurchaseId());
    PagedResult<List<PurchaseOrderDTO>> purchaseResult = purchaseService.queryPurchase(query);
    if (null != purchaseResult && purchaseResult.getSuccess() && null != purchaseResult.getModule()) {
      PurchaseOrderDTO purchaseOrderDTO = purchaseResult.getModule().get(0);
      if (purchaseOrderDTO.getAuditingStatus().shortValue() >= PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS
          .shortValue()) {
        return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            "已经提交供应商的采购订单不能再修改"));
      }
      int countOld = purchaseSubOrderDTO.getCount();
      int countNew = purchaseItemVO.getCount();
      int countTemp = countNew - countOld;
      long amountTemp = new BigDecimal(purchaseSubOrderDTO.getUnitCost()).multiply(new BigDecimal(countTemp)).add(new BigDecimal(purchaseOrderDTO.getTotalAmount())).longValue();
      purchaseOrderDTO.setTotalAmount(amountTemp);
      Result<Boolean> updatePurchaseResult = purchaseService.updatePurchaseOrder(purchaseOrderDTO);
      if(null == updatePurchaseResult || !updatePurchaseResult.getSuccess() || null == updatePurchaseResult.getModule()){
        LOGGER.error("更新采购单数据失败，原因：" + JackSonUtil.getJson(updatePurchaseResult));
      }
    }
    purchaseSubOrderDTO.setCount(purchaseItemVO.getCount());
    purchaseSubOrderDTO.setCheckinfo(purchaseItemVO.getCheckInfo());
    if (StringUtils.isNotBlank(purchaseItemVO.getWareHouse())) {
      purchaseSubOrderDTO.setWareHouse(Integer.valueOf(purchaseItemVO.getWareHouse()));
    }
    if (PurchaseSubOrderStatusConstants.SUBORDER_STATUS_INVALID.equals(purchaseItemVO.getStatus())) {
      purchaseSubOrderDTO.setStatus(purchaseItemVO.getStatus());
    }
    Result<Boolean> result = purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO);
    if (null == result) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "网络出错，请稍后重试"));
    }
    return JackSonUtil.getJson(result);
  }



  /**
   * 添加子采购单
   * 
   * @param purchaseItemVOs
   * @param backGroundUserDTO
   * @return
   */
  public Result<Boolean> insertSubOrder(List<PurchaseItemVO> purchaseItemVOs,
      BackGroundUserDTO backGroundUserDTO) {
    if (CollectionUtils.isEmpty(purchaseItemVOs)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    Integer purchaseId = purchaseItemVOs.get(0).getPurchaseId();
    PurchaseQuery purchaseQuery = new PurchaseQuery();
    purchaseQuery.setParentPurchaseId(purchaseId);
    // 已经提交供应商的采购订单不能再修改
    PagedResult<List<PurchaseOrderDTO>> purchaseResult =
        purchaseService.queryPurchase(purchaseQuery);
    if (null != purchaseResult && purchaseResult.getSuccess() && null != purchaseResult.getModule()) {
      PurchaseOrderDTO purchaseOrderDTO = purchaseResult.getModule().get(0);
      if (purchaseOrderDTO.getAuditingStatus().shortValue() >= PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS
          .shortValue()) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "已经提交供应商的采购订单不能再修改");
      }
      long totalAmount = 0;
      for(PurchaseItemVO purchaseItemVO : purchaseItemVOs){
        long amountTemp = new BigDecimal(purchaseItemVO.getDiscountPrice()).multiply(new BigDecimal(100)).multiply(new BigDecimal(purchaseItemVO.getCount())).longValue();
        totalAmount = new BigDecimal(totalAmount).add(new BigDecimal(amountTemp)).longValue();
      }
      purchaseOrderDTO.setTotalAmount(new BigDecimal(purchaseOrderDTO.getTotalAmount()).add(new BigDecimal(totalAmount)).longValue());
      Result<Boolean> updatePurchaseResult = purchaseService.updatePurchaseOrder(purchaseOrderDTO);
      if(null == updatePurchaseResult || !updatePurchaseResult.getSuccess() || null == updatePurchaseResult.getModule()){
        LOGGER.error("更新采购单数据失败，原因：" + JackSonUtil.getJson(updatePurchaseResult));
      }
    }
    Result<Map<String, Object>> subPurchaseResult =
        purchaseService.querySubPurchaseList(purchaseQuery);
    List<Long> skuIdsDb = Lists.newArrayList();
    Date expectArriveDate = new Date();
    Map<Long, PurchaseSubOrderDTO> subPurchaseOrderMapCache = Maps.newHashMap();
    if (null != subPurchaseResult && subPurchaseResult.getSuccess()
        && null != subPurchaseResult.getModule()) {
      Map<String, Object> subPurchaseMap = subPurchaseResult.getModule();
      List<PurchaseSubOrderDTO> purchaseSubOrderDTOs =
          (List<PurchaseSubOrderDTO>) subPurchaseMap.get(PurchaseService.SUB_PURCHASE_LIST_KEY);
      for (PurchaseSubOrderDTO purchaseSubOrderDTO : purchaseSubOrderDTOs) {
        Long skuIdDb = purchaseSubOrderDTO.getSkuId();
        skuIdsDb.add(skuIdDb);
        subPurchaseOrderMapCache.put(skuIdDb, purchaseSubOrderDTO);
      }
      expectArriveDate = purchaseSubOrderDTOs.get(0).getExpectArriveDate();
    }
    // 如果添加了相同的商品，那么更新原来的商品数量即可，否则将该商品插入子采购单
    List<Long> skuIds = Lists.newArrayList();
    List<PurchaseSubOrderDTO> updatePurchaseSubDTOs = Lists.newArrayList();
    List<PurchaseItemVO> insertPurchaseItemVOs = Lists.newArrayList();
    for (PurchaseItemVO purchaseItemVO : purchaseItemVOs) {
      Long skuId = purchaseItemVO.getSkuId();
      skuIds.add(skuId);
      if (CollectionUtils.isNotEmpty(skuIdsDb) && skuIdsDb.contains(skuId)) {
        PurchaseSubOrderDTO purchaseSubOrderDTO = subPurchaseOrderMapCache.get(skuId);
        if (null != purchaseSubOrderDTO) {
          Integer count = purchaseItemVO.getCount() + purchaseSubOrderDTO.getCount();
          purchaseSubOrderDTO.setCount(count);
          updatePurchaseSubDTOs.add(purchaseSubOrderDTO);
        }
      } else {
        insertPurchaseItemVOs.add(purchaseItemVO);
      }
    }
    boolean update = true;
    // 更新子采购单
    if (CollectionUtils.isNotEmpty(updatePurchaseSubDTOs)) {
      for (PurchaseSubOrderDTO purchaseSubOrderDTO : updatePurchaseSubDTOs) {
        Result<Boolean> updateResult = purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO);
        if (null == updateResult || !updateResult.getSuccess() || null == updateResult.getModule()
            || !updateResult.getModule()) {
          LOGGER.error("更新子采购单失败：" + purchaseSubOrderDTO.getPurchaseSubId());
          update = false;
        }
      }
    }
    // 插入子采购单
    if (CollectionUtils.isNotEmpty(insertPurchaseItemVOs)) {
      Result<List<ItemSkuRelationDTO>> itemRelationSkuResult =
          skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(skuIds, RelationTypeConstants.RELATION_TYPE_DISCOUNT));
      Map<Long, Integer> settlementPriceMap = Maps.newHashMap();
      Map<Long, Integer> specificationMap = Maps.newHashMap();// 前台skuid和进售属性映射表
      if (null != itemRelationSkuResult && itemRelationSkuResult.getSuccess()
          && null != itemRelationSkuResult.getModule()) {
        List<ItemSkuRelationDTO> itemSkuRelationDTOs = itemRelationSkuResult.getModule();
        for (ItemSkuRelationDTO itemSkuRelationDTO : itemSkuRelationDTOs) {
          settlementPriceMap
              .put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getBoxCost());
          specificationMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getPcProportion());
        }
      }
      
      Result<List<ItemCommoditySkuDTO>> itemCommodityResult = skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
      Map<Long, ItemCommoditySkuDTO> commodityMap = Maps.newHashMap();
      if(null != itemCommodityResult && itemCommodityResult.getSuccess()
          && null != itemCommodityResult.getModule()){
        List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommodityResult.getModule();
        for(ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs){
          commodityMap.put(itemCommoditySkuDTO.getSkuId(), itemCommoditySkuDTO);
        }
      }
      List<PurchaseSubOrderDTO> purchaseSubOrderDTOs = Lists.newArrayList();
      for (PurchaseItemVO purchaseItemVO : insertPurchaseItemVOs) {
        PurchaseSubOrderDTO subPurchaseOrderDTO = new PurchaseSubOrderDTO();
        subPurchaseOrderDTO.setCount(purchaseItemVO.getCount());
        subPurchaseOrderDTO.setCheckinfo(purchaseItemVO.getCheckInfo());
        subPurchaseOrderDTO.setCreateName(backGroundUserDTO.getName());
        subPurchaseOrderDTO.setCreator(backGroundUserDTO.getUserId());
        if (null != expectArriveDate) {
          subPurchaseOrderDTO.setExpectArriveDate(expectArriveDate);
        }
        subPurchaseOrderDTO.setFlowStatus((short) 2);
        subPurchaseOrderDTO.setPurchaseDate(new Date());
        subPurchaseOrderDTO.setPurchaseId(purchaseId);
        subPurchaseOrderDTO.setPurchasingAgent(backGroundUserDTO.getUserId());
        subPurchaseOrderDTO.setUnitCost(settlementPriceMap.get(purchaseItemVO.getSkuId()));
        subPurchaseOrderDTO.setSkuId(purchaseItemVO.getSkuId());
        subPurchaseOrderDTO.setSupplierId(purchaseItemVO.getSupplierId());
        subPurchaseOrderDTO.setSupplierName(purchaseItemVO.getSupplierName());
        subPurchaseOrderDTO.setTitle(purchaseItemVO.getTitle());
        subPurchaseOrderDTO.setContractId(0);
        subPurchaseOrderDTO.setWareHouse(1);
        ItemCommoditySkuDTO itemCommoditySkuDTO = commodityMap.get(purchaseItemVO.getSkuId());
        if(null != itemCommoditySkuDTO && null != itemCommoditySkuDTO.getIsSteelyardSku()){
          subPurchaseOrderDTO.setSteelyardSku((short) (itemCommoditySkuDTO.getIsSteelyardSku() ? 1 : 0));
        }else{
          subPurchaseOrderDTO.setSteelyardSku((short) 0);
        }
        String saleUnitStr =
            (itemCommoditySkuDTO.getIsSteelyardSku() ? "公斤" : itemCommoditySkuDTO.getSaleUnitStr());
        subPurchaseOrderDTO.setPcProportionUnit(saleUnitStr);
        subPurchaseOrderDTO.setPcProportion(specificationMap.get(purchaseItemVO.getSkuId()));
        purchaseSubOrderDTOs.add(subPurchaseOrderDTO);
      }
      return purchaseService.batchInsertSubPurchase(purchaseSubOrderDTOs);
    }
    // 如果没有需要插入的数据则返回更新的结果
    return Result.getSuccDataResult(update);
  }

}
