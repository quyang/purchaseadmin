package com.xianzaishi.purchaseadmin.component.promotion;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendItemCouponParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO.SendTypeConstants;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.inventory.vo.InventoryDetailVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.InventoryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.PromotionItemVO;
import com.xianzaishi.purchaseadmin.client.promotion.vo.SetDiscountQuery;
import com.xianzaishi.purchaseadmin.client.promotion.vo.SkuDiscountVO;
import com.xianzaishi.purchaseadmin.component.category.CategoryComponent;
import com.xianzaishi.purchaseadmin.component.inventory.InventoryComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;

@Component("promotionComponent")
public class PromotionComponent {

  @Autowired
  private SkuService skuService;
  
  @Autowired
  private CommodityService commodityService;
  
  @Autowired
  private UserCouponService userCouponService;
  
  @Autowired
  private CategoryComponent categoryComponent;
  
  @Autowired
  private UserComponent userComponent;
  
  @Autowired
  private InventoryComponent inventoryComponent;
  
  /** 
   * 
   * 根据userId 发送优惠卷 
   * 
   * @author huanhuan
   * 
   * @param userId
   * @param couponId
   * 
   * */
  
  public Result<Boolean> addUserCoupon(Long userId,int couponId){
	UserDTO user = userComponent.getUserInformation(userId);
	if(null==user){
	  return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, 
			  "Query user not exit,input userId:" + userId);	
	}
	if (couponId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误发送失败");
    }
	
	SendParaDTO sendQuery = new SendParaDTO();
    sendQuery.setPackageId(couponId);
    sendQuery.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
    sendQuery.setUserId((long)userId);
    
    Result<Boolean> sendResult = userCouponService.sendCoupon(sendQuery);
    if (null != sendResult && sendResult.getSuccess() && sendResult.getModule()) {
      return Result.getResult(true, true, 1, "发送成功");
    } else {
      return Result.getResult(true, true, 1, "已拥有优惠券，无需领取");
    }
  }
  
  public Result<PromotionItemVO> getPromotionItemBySkuId(long skuId){
    Result<ItemCommoditySkuDTO> querySkuResult = skuService.queryItemSku(SkuQuery.queryBaseSkuBySku(skuId));
    if(null == querySkuResult || !querySkuResult.getSuccess() || null == querySkuResult.getModule()){
      return Result.getErrDataResult(-1, "查询sku信息出错");
    }
    
    ItemCommoditySkuDTO sku = querySkuResult.getModule();
    
    ItemQuery query = new ItemQuery();
    query.setItemId(sku.getItemId());
    Result<ItemBaseDTO> queryItemResult = commodityService.queryCommodity(query);
    if(null == queryItemResult || !queryItemResult.getSuccess() || null == queryItemResult.getModule()){
      return Result.getErrDataResult(-1, "查询item信息出错");
    }
    ItemBaseDTO item = queryItemResult.getModule();
    
    CategoryDTO cat = categoryComponent.getCategoryDTO(item.getCategoryId());
    
    Result<InventoryVO> inventoryResult = inventoryComponent.getInventory(skuId);
    InventoryVO inventory = null;
    if(null != inventoryResult && inventoryResult.getSuccess()){
      inventory = inventoryResult.getModule();
    }else{
      return Result.getErrDataResult(-1, inventoryResult.getErrorMsg());
    }
    
    SupplierDTO supplier = userComponent.getSupplierById(sku.getSupplierId());
    
    String catName = "";
    if(null != cat){
      catName = cat.getCatName();
    }
    String supplierName = "";
    if(null != supplier){
      supplierName = supplier.getName();
    }
    
    PromotionItemVO skuResult = new PromotionItemVO();
    skuResult.setSkuId(skuId);
    skuResult.setTitle(sku.getTitle());
    skuResult.setCatName(catName);
    
    int orignalPrice = sku.getUnitPrice();
    int discountPrice = sku.getDiscountUnitPrice();
    
    skuResult.setPriceStr(new BigDecimal(orignalPrice).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString());
    skuResult.setPromotionPriceStr(new BigDecimal(discountPrice).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString());
    skuResult.setDisplaySpecifications(sku.getSpecification());
    skuResult.setId(item.getItemId());
    skuResult.setStocNum(inventory.getNum());
    skuResult.setSafeStocNum(inventory.getSafeNum());
    skuResult.setSupplierName(supplierName);
    List<InventoryDetailVO> all = inventory.getAvailableInventory();
    if(CollectionUtils.isNotEmpty(all)){
      List<String> tmpResult = Lists.newArrayList();
      for(InventoryDetailVO vo:all){
        tmpResult.add(vo.getPoisition());
      }
      skuResult.setAvailableStocPosition(tmpResult);
    }
    List<InventoryDetailVO> recommond = inventory.getRecommondInventory();
    if(CollectionUtils.isNotEmpty(recommond)){
      List<String> tmpResult = Lists.newArrayList();
      for(InventoryDetailVO vo:recommond){
        tmpResult.add(vo.getPoisition());
      }
      skuResult.setRecommondStocPosition(tmpResult);
    }
    
    QueryParaDTO para = new QueryParaDTO();
    para.setIncludeAllCoupon(true);
    para.setSkuId(10000*sku.getSkuId());
    Result<List<UserCouponDTO>> couponListResult = userCouponService.getItemCoupon(para);
    if(null == couponListResult || !couponListResult.getSuccess()){
      return Result.getErrDataResult(-1, "查询优惠信息出错");
    }
    if(CollectionUtils.isNotEmpty(couponListResult.getModule())){
      List<SkuDiscountVO> discountList = Lists.newArrayList();
      for(UserCouponDTO coupon:couponListResult.getModule()){
        SkuDiscountVO discount1 = new SkuDiscountVO();
        if(coupon.getGmtCreate() != null){
          discount1.setGmtCreate(coupon.getGmtCreate().getTime());
        }else{
          discount1.setGmtCreate(0);
        }
        
        discount1.setId(getId(coupon.getId(),sku.getSkuId()));
        discount1.setOrderId(coupon.getOrderId());
        discount1.setPrice(coupon.getAmount().toString());
        if(coupon.getCouponStartTime() != null){
          discount1.setStartTime(coupon.getCouponStartTime().getTime());
        }else{
          discount1.setStartTime(0);
        }
        if(coupon.getCouponEndTime() != null){
          discount1.setEndTime(coupon.getCouponEndTime().getTime());
        }else{
          discount1.setEndTime(0);
        }
        discount1.setStatus(coupon.getStatus());
        discount1.setType(1);
        discount1.setShopName("蓝村路店");
        discountList.add(discount1);
      }
      skuResult.setDiscountItemHistory(discountList);
    }
    return Result.getSuccDataResult(skuResult);
  }
  
  private String getId(long couponId,long skuId){
    return new StringBuilder().append("027001").append(skuId).append(couponId).toString();
  }
  
  public Result<Boolean> setDiscount(SetDiscountQuery setQuery){
    if(null == setQuery || setQuery.getCount() <= 0 || setQuery.getSkuId() <= 0 || StringUtils.isEmpty(setQuery.getSetPrice())){
      return Result.getErrDataResult(-1, "请求参数错误,"+"count is:"+setQuery.getCount()+",price is:"+setQuery.getSetPrice()+",skuId is:"+setQuery.getSkuId());
    }
    SendItemCouponParaDTO para = new SendItemCouponParaDTO();
    long skuId = setQuery.getSkuId();
    if(setQuery.getDay() >= 0){
      para.setCouponDayTime(setQuery.getDay());
    }else{
      para.setCouponDayTime(5);
    }
    int price = new BigDecimal(setQuery.getSetPrice()).multiply(new BigDecimal(100)).intValue();
    if(price <= 0){
      return Result.getErrDataResult(-1, "请求参数错误,"+"price is:"+setQuery.getSetPrice());
    }else{
      Result<ItemCommoditySkuDTO> querySkuResult = skuService.queryItemSku(SkuQuery.querySkuById(skuId));
      if(null == querySkuResult || !querySkuResult.getSuccess() || null == querySkuResult.getModule()){
        return Result.getErrDataResult(-1, "查询sku信息出错");
      }
      
      ItemCommoditySkuDTO sku = querySkuResult.getModule();
      if(sku.getIsSteelyardSku()){
        return Result.getErrDataResult(-1, "查询sku为称重类型商品，不支持设置");
      }else if(sku.getDiscountUnitPrice() <= price){
        return Result.getErrDataResult(-1, "查询sku原价低于当前设置，不支持设置");
      }
    }
    para.setBuyPrice(price);
    para.setCouponId(87L);
    para.setLimitCount(setQuery.getCount());
    para.setSkuId(skuId);
    Result<List<Long>> result = userCouponService.addItemCoupon(para);
    if(null == result || !result.getSuccess() || CollectionUtils.isEmpty(result.getModule())){
      return Result.getErrDataResult(-1, "设置优惠券失败，请重试");
    }
    if(result.getModule().size() != setQuery.getCount()){
      return Result.getErrDataResult(-1, "设置优惠券部分生效，请刷新页面");
    }else{
      return Result.getSuccDataResult(true);
    }
  }
}
