package com.xianzaishi.purchaseadmin.component.activitypage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.activity.vo.ActivityPageVO;
import com.xianzaishi.purchaseadmin.client.activity.vo.ActivityStepVO;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.purchasecenter.client.activity.ActivityPageService;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepItemDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepPicDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepPicDTO.ActivityPicTypeConstants;

@Component("activityPageComponent")
public class ActivityPageComponent {

  private static final Logger logger = Logger.getLogger(ActivityPageComponent.class);
  
  @Autowired
  private ActivityPageService activitypageservice;

  @Autowired
  private SkuService skuService;

  @Autowired
  private CommodityService commodityService;
  
  @Autowired
  private SystemPropertyService systempropertyservice;

  private static final String STEP_FILE = "/usr/works/systemconfig/steptype.properties";
  private static final Logger LOGGER = Logger.getLogger(ActivityPageComponent.class);

  public Result<List<ActivityPageVO>> getList() {
    Result<List<ActivityPageDTO>> pageList = activitypageservice.getPageAll();
    List<ActivityPageVO> result = Lists.newArrayList();
    if (null == pageList || !pageList.getSuccess() || CollectionUtils.isEmpty(pageList.getModule())) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "Query page list empty");
    }

    for (ActivityPageDTO tmpPage : pageList.getModule()) {
      int pageId = tmpPage.getPageId();
      if (CollectionUtils.isEmpty(tmpPage.getStepList())) {
        ActivityPageVO vo = new ActivityPageVO();
        vo.setPageId(pageId);
        vo.setTitle(tmpPage.getTitle());
        result.add(vo);
      } else {
        for (ActivityStepDTO stepInfo : tmpPage.getStepList()) {
          ActivityPageVO vo = new ActivityPageVO();
          vo.setPageId(pageId);
          vo.setStepId(stepInfo.getStepId());
          vo.setTitle(tmpPage.getTitle());
          if (StringUtils.isEmpty(stepInfo.getTitle())) {
            vo.setStepTitle("");
          } else {
            vo.setStepTitle(stepInfo.getTitle());
          }
          result.add(vo);
        }
      }
    }

    if (CollectionUtils.isEmpty(result)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "Query page list change result is empty");
    }
    return Result.getSuccDataResult(result);
  }

  public Result<Integer> addPage(ActivityPageVO pageInfo) {
    boolean result = false;
    ActivityPageDTO activityPageDTO = new ActivityPageDTO();
    if (null == pageInfo || StringUtils.isEmpty(pageInfo.getTitle())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Add page title is null,");
    }

    activityPageDTO.setTitle(pageInfo.getTitle());
    Result<Integer> insertResult = activitypageservice.insertPageStepInfo(activityPageDTO);
    if (insertResult.getSuccess()) {
      return insertResult;
    } else {
      return Result.getErrDataResult(insertResult.getResultCode(), insertResult.getErrorMsg());
    }
  }

  // 这里设计的不好，设计的是填写商品id，但是后来发现后台只有查询sku的入口，导致这里item向skuid需要做转换
  public Result<Boolean> addStep(ActivityStepVO stepInfo) {
    LOGGER.info("开始执行addstep");
    String stepdatas = getStepInfo();
    Map<String, String> mapCheck = Maps.newHashMap();
    if (null != stepdatas) {
      String[] steps = stepdatas.split("\n");
      for (String step : steps) {
        Map<String, String> map = getStepElement(step.split(";"));
        if (stepInfo.getStepType().intValue() == Integer.valueOf(map.get("id")).intValue()) {
          mapCheck = map;
          break;
        }
      }
    }
    LOGGER.error("获取到的map数据：" + JackSonUtil.getJson(mapCheck));
    if (MapUtils.isNotEmpty(mapCheck)) {
      Result<Boolean> checkPrefixResult = checkPrefixCondition(mapCheck, stepInfo);// 检查前置条件
      if (null == checkPrefixResult || !checkPrefixResult.getSuccess()) {
        LOGGER.error(JackSonUtil.getJson(checkPrefixResult));
        return checkPrefixResult;
      }
    } else {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请选择正确的楼层类型数据");
    }
    Map<String,String> sysconfigM = getSystemConfig(stepInfo); 
    intercept(stepInfo);
    boolean result = false;
    ActivityStepDTO activityStepDTO = new ActivityStepDTO();
    if (CollectionUtils.isNotEmpty(stepInfo.getItemList())) {
      List<ActivityStepItemDTO> insertItemList = stepInfo.getItemList();
      List<ActivityStepItemDTO> itemList = Lists.newArrayList();
      for (ActivityStepItemDTO item : insertItemList) {
        if (null != item && null != item.getItemId() && item.getItemId() > 0) {
          itemList.add(item);
        }
      }
      List<Long> itemIdlist = Lists.newArrayList();
      for (ActivityStepItemDTO item : itemList) {
        itemIdlist.add(item.getItemId());
      }
      if (CollectionUtils.isNotEmpty(itemIdlist)) {
        Result<Map<Long, Long>> checkResult = checkItemSkuIds(itemIdlist);
        if (checkResult.getSuccess()) {// 通过skuid替换成商品id
          for (int i = 0; i < itemIdlist.size(); i++) {
            ActivityStepItemDTO item = itemList.get(i);
            item.setItemId(checkResult.getModule().get(item.getItemId()));
          }
          activityStepDTO.setItemList(itemList);
        } else {
          return Result.getErrDataResult(checkResult.getResultCode(), checkResult.getErrorMsg());
        }
      }
    }

    if (CollectionUtils.isNotEmpty(stepInfo.getPicList())) {
      List<ActivityStepPicDTO> insertItemList = stepInfo.getPicList();
      List<ActivityStepPicDTO> picList = Lists.newArrayList();
      for (ActivityStepPicDTO pic : insertItemList) {
        if (null != pic && StringUtils.isNotEmpty(pic.getPicUrl())) {
          picList.add(pic);
        }
      }
      List<Long> itemIdlist = Lists.newArrayList();
      for (ActivityStepPicDTO pic : picList) {
        if (ActivityPicTypeConstants.PIC_ITEM.equals(pic.getTargetType())) {
          itemIdlist.add(Long.valueOf(pic.getTargetId()));
        }
      }
      Result<Map<Long, Long>> checkResult = null;
      if (CollectionUtils.isNotEmpty(itemIdlist)) {
        checkResult = checkItemSkuIds(itemIdlist);
        if (!checkResult.getSuccess()) {
          return Result.getErrDataResult(checkResult.getResultCode(), checkResult.getErrorMsg());
        }
      }
      for (int i = 0; i < picList.size(); i++) {
        ActivityStepPicDTO pic = picList.get(i);
        if (ActivityPicTypeConstants.PIC_ITEM.equals(pic.getTargetType())) {
          pic.setTargetId(checkResult.getModule().get(Long.valueOf(pic.getTargetId())).toString());
        }
      }
      activityStepDTO.setPicList(picList);
    }
    activityStepDTO.setTitle(stepInfo.getTitle());
    activityStepDTO.setStepType(stepInfo.getStepType());


    int pageId = stepInfo.getPageId();
    if (null != stepInfo.getSortId()) {
      activityStepDTO.setSortId(stepInfo.getSortId());
    } else {
      activityStepDTO.setSortId(100);
    }

    long current = System.currentTimeMillis();
    if(null != stepInfo.getBegin() && stepInfo.getBegin() > current){
      activityStepDTO.setBegin(stepInfo.getBegin());
    }
    if(null != stepInfo.getEnd() && stepInfo.getEnd() > current){
      activityStepDTO.setEnd(stepInfo.getEnd());
    }
    if(null != stepInfo.getHeadSpace()){
      activityStepDTO.setHeadSpace(stepInfo.getHeadSpace());
    }
    
    activityStepDTO.setHeadSpace(stepInfo.getHeadSpace());
    
    if (stepInfo.getStepId() != null && stepInfo.getStepId() > 0) {
      activityStepDTO.setStepId(stepInfo.getStepId());
      Result<Boolean> updateResult = activitypageservice.updateStep(activityStepDTO, pageId);
      if (updateResult.getSuccess()) {
        updateSystemConfig(sysconfigM);
        return Result.getSuccDataResult(result);
      } else {
        return Result.getErrDataResult(updateResult.getResultCode(), updateResult.getErrorMsg());
      }
    } else {
      Result<Integer> insertResult = activitypageservice.addPageStepInfo(activityStepDTO, pageId);
      if (insertResult.getSuccess()) {
        return Result.getSuccDataResult(result);
      } else {
        return Result.getErrDataResult(insertResult.getResultCode(), insertResult.getErrorMsg());
      }
    }
  }

  /**
   * 校验数据前置条件
   * 
   * @param mapCheck
   * @param stepInfo
   * @return
   */
  private Result<Boolean> checkPrefixCondition(Map<String, String> mapCheck, ActivityStepVO stepInfo) {
    Integer itemRequire = Integer.valueOf(mapCheck.get("itemrequire"));
    Integer itemMax = Integer.valueOf(mapCheck.get("itemmax"));
    Integer picRequire = Integer.valueOf(mapCheck.get("picrequire"));
    Integer picMax = Integer.valueOf(mapCheck.get("picmax"));
    Integer picMin = Integer.valueOf(mapCheck.get("picmin"));
    Integer titleRequire = Integer.valueOf(mapCheck.get("titlerequire"));
    Integer titleMax = Integer.valueOf(mapCheck.get("titlemax"));
    Integer titleMin = Integer.valueOf(mapCheck.get("titlemin"));
    Integer itemMin = 1;
    if (itemRequire.intValue() == 0) {
      itemMin = 0;
    }
    List<ActivityStepItemDTO> itemList = Lists.newArrayList();//商品列表二次处理
    if (CollectionUtils.isNotEmpty(stepInfo.getItemList())) {
      List<ActivityStepItemDTO> insertItemList = stepInfo.getItemList();
      for (ActivityStepItemDTO item : insertItemList) {
        if (null != item && null != item.getItemId() && item.getItemId() > 0) {
          itemList.add(item);
        }
      }
    }
    List<ActivityStepPicDTO> picList = Lists.newArrayList();//图片列表二次处理
    if (CollectionUtils.isNotEmpty(stepInfo.getPicList())) {
      List<ActivityStepPicDTO> insertItemList = stepInfo.getPicList();
      for (ActivityStepPicDTO pic : insertItemList) {
        if (null != pic && StringUtils.isNotEmpty(pic.getPicUrl())) {
          picList.add(pic);
        }
      }
    }
    
    Result<Boolean> checkItemResult =
        checkElements(itemRequire, itemMax, itemMin, itemList.size(), "商品");// 校验商品
    if (null != checkItemResult && !checkItemResult.getSuccess()) {
      return checkItemResult;
    }
    Result<Boolean> checkPicResult =
        checkElements(picRequire, picMax, picMin, picList.size(), "图片");// 校验图片
    if (null != checkPicResult && !checkPicResult.getSuccess()) {
      return checkPicResult;
    }
    Result<Boolean> checkTitleResult =
        checkElements(titleRequire, titleMax, titleMin, stepInfo.getTitle().length(), "标题");// 校验标题
    if (null != checkTitleResult && !checkTitleResult.getSuccess()) {
      return checkTitleResult;
    }
    return Result.getSuccDataResult(true);
  }

  public Result<Boolean> intercept(ActivityStepVO stepInfo) {
    Result<Boolean> updateItemSku = null;
    if (4 == stepInfo.getPageId()) {// 只拦截pageId=4的
      String title = stepInfo.getTitle();// 获得楼层标题=skuid+口味taste
      String[] split = title.split("\\\\");
      String skuid = split[0];
      String taste = split[1];
      if (CollectionUtils.isNotEmpty(stepInfo.getItemList())) {
        List<ActivityStepItemDTO> itemList = stepInfo.getItemList();
        List<Long> itemIdlist = Lists.newArrayList();// 得到相关skuid列表
        for (ActivityStepItemDTO activityStepItemDTO : itemList) {
          itemIdlist.add(activityStepItemDTO.getItemId());
        }
        Pattern pattern = Pattern.compile("[0-9]*");// 判定skuid为数字
        Matcher isNum = pattern.matcher(skuid);
        Long long1 = null;
        if (!isNum.matches()) {
          return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
              "标题格式：skuid\\口味，skuid为数字");
        }
        long1 = Long.parseLong(skuid);
        Result<ItemCommoditySkuDTO> sku = skuService.queryItemSku(SkuQuery.querySkuById(long1));// 验证skuid
        if (null == sku) {
          return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
              "update request parameter is null");
        }
        StringBuilder stringBuilder = new StringBuilder();// 拼接字符串
        stringBuilder.append("[" + taste + "#");
        for (int i = 0; i < itemIdlist.size(); i++) {
          if (i < itemIdlist.size() - 1) {
            stringBuilder.append(itemIdlist.get(i));
            stringBuilder.append("\\");
          } else {
            stringBuilder.append(itemIdlist.get(i));
          }
        }
        stringBuilder.append("]");

        sku.getModule().addRelatedItems(stringBuilder.toString());
        updateItemSku = skuService.updateItemSku(sku.getModule());// 更新数据
      }
    }
    return updateItemSku;
  }

  public Map<String,String> getSystemConfig(ActivityStepVO stepInfo) {
    if(null == stepInfo){
      return null;
    }
    if(stepInfo.getStepType() != 11){
      logger.error("Get step type is:" + stepInfo.getStepType());
      return null;
    }
    List<ActivityStepItemDTO> stepIdList = stepInfo.getItemList();
    String title = stepInfo.getTitle();
    if(CollectionUtils.isEmpty(stepIdList)){
      logger.error("Get step id list is null");
      return null;
    }
    String idList = "";
    for(ActivityStepItemDTO item:stepIdList){
      idList = idList + item.getItemId()+",";
    }
    String key = title.replace("=", ":00#");
    key = key + ":00";
    Map<String,String> result = Maps.newHashMap();
    result.put(key, idList);
    return result;
  }
  
  private void updateSystemConfig(Map<String,String> parameter){
    Result<SystemConfigDTO> queryResult = systempropertyservice.getSystemConfig(1);
    if(null == parameter || CollectionUtils.isEmpty(parameter.keySet()) || null == queryResult || !queryResult.getSuccess()){
      return;
    }
    SystemConfigDTO sysConfig = queryResult.getModule();
    String json = sysConfig.getConfigInfo();
    Map<String,String> oldM = (Map<String, String>) JackSonUtil.jsonToObject(json, Map.class);
    oldM.putAll(parameter);
    String newJson = JackSonUtil.getJson(oldM);
    sysConfig.setConfigInfo(newJson);
    systempropertyservice.updateSystemConfig(sysConfig);
  }
  
  public Result<Boolean> resetStep(int pageId, int stepId) {
    if (pageId <= 0 || stepId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "reset step paramter error,pageId is:" + pageId + ",stepId:" + stepId);
    }

    return activitypageservice.resetStep(pageId, stepId);
  }

  public Result<ActivityStepVO> queryStep(int pageId, int stepId) {
    if (pageId <= 0 || stepId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query step paramter error,pageId is:" + pageId + ",stepId:" + stepId);
    }
    Result<ActivityPageDTO> pageInfo = activitypageservice.getPage(pageId, Arrays.asList(stepId));
    if (null == pageInfo || !pageInfo.getSuccess()
        || CollectionUtils.isEmpty(pageInfo.getModule().getStepList())) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "query step return null,pageId is:" + pageId + ",stepId:" + stepId);
    }

    ActivityStepDTO stepInfo = pageInfo.getModule().getStepList().get(0);

    ActivityStepVO result = new ActivityStepVO();
    result.setPageId(pageId);
    result.setStepType(stepInfo.getStepType());

    if (CollectionUtils.isNotEmpty(stepInfo.getItemList())) {
      List<ActivityStepItemDTO> itemList = stepInfo.getItemList();
      List<Long> itemIdlist = Lists.newArrayList();
      for (ActivityStepItemDTO item : itemList) {
        itemIdlist.add(item.getItemId());
      }
      Result<Map<Long, Long>> itemSkuMResult = changeItemIdToSkuId(itemIdlist);
      if (null != itemSkuMResult && itemSkuMResult.getSuccess()) {
        for (ActivityStepItemDTO item : itemList) {
          item.setItemId(itemSkuMResult.getModule().get(item.getItemId()));
        }
      }
      result.setItemList(itemList);
    }

    if (CollectionUtils.isNotEmpty(stepInfo.getPicList())) {
      List<ActivityStepPicDTO> picList = stepInfo.getPicList();
      List<Long> picIdlist = Lists.newArrayList();
      for (ActivityStepPicDTO pic : picList) {
        if (ActivityPicTypeConstants.PIC_ITEM.equals(pic.getTargetType())) {
          picIdlist.add(Long.valueOf(pic.getTargetId()));
        }
        if(StringUtils.isEmpty(pic.getTargetId())){
          pic.setTargetId("0");
        }
      }
      Result<Map<Long, Long>> itemSkuMResult = changeItemIdToSkuId(picIdlist);
      if (null != itemSkuMResult && itemSkuMResult.getSuccess()) {
        for (ActivityStepPicDTO pic : picList) {
          if (ActivityPicTypeConstants.PIC_ITEM.equals(pic.getTargetType())) {
            pic.setTargetId(itemSkuMResult.getModule().get(Long.valueOf(pic.getTargetId()))
                .toString());
          }
        }
      }
      result.setPicList(picList);
    }
    result.setTitle(stepInfo.getTitle());
    result.setSortId(stepInfo.getSortId());
    result.setStepId(stepInfo.getStepId());
    result.setBegin(stepInfo.getBegin());
    result.setEnd(stepInfo.getEnd());
    result.setHeadSpace(stepInfo.getHeadSpace());
    return Result.getSuccDataResult(result);
  }

  /**
   * 根据skuid列表返回商品列表
   * 
   * @param skuIdlist
   * @return
   */
  private Result<Map<Long, Long>> checkItemSkuIds(List<Long> skuIdlist) {
    Result<List<ItemCommoditySkuDTO>> queryResult =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIdlist));
    if (null != queryResult && queryResult.getSuccess()
        && CollectionUtils.isNotEmpty(queryResult.getModule())) {
      if (queryResult.getModule().size() == skuIdlist.size()) {
        Map<Long, Long> skuItemM = Maps.newHashMap();
        for (ItemCommoditySkuDTO sku : queryResult.getModule()) {
          skuItemM.put(sku.getSkuId(), sku.getItemId());
        }
        return Result.getSuccDataResult(skuItemM);
      }
    }
    return Result.getErrDataResult(-1, "check skulist failed");
  }

  private final static String ITEMLISTKEY = "itemList";

  private Result<Map<Long, Long>> changeItemIdToSkuId(List<Long> itemIdlist) {
    ItemListQuery query = new ItemListQuery();
    query.setItemIds(itemIdlist);
    query.setPageNum(0);
    query.setPageSize(50);
    Result<Map<String, Object>> queryResult = commodityService.queryCommodies(query);
    if (null == queryResult || null == queryResult.getModule()
        || CollectionUtils.isEmpty((List) queryResult.getModule().get(ITEMLISTKEY))) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Add item info error,item id is error," + itemIdlist);
    }
    List<ItemDTO> dtoResult = (List<ItemDTO>) queryResult.getModule().get(ITEMLISTKEY);
    Map<Long, Long> itemSkuM = Maps.newHashMap();
    for (ItemDTO item : dtoResult) {
      ItemCommoditySkuDTO sku = item.getItemSkuDtos().get(0);
      itemSkuM.put(sku.getItemId(), sku.getSkuId());
    }
    return Result.getSuccDataResult(itemSkuM);
  }

  /**
   * 删除楼层数据
   * 
   * @param stepId
   * @return
   */
  public String dropPageStep(Integer stepId) {
    Result<Boolean> result = activitypageservice.deletePageStepInfo(stepId);
    return JackSonUtil.getJson(result);
  }

  /**
   * 获取楼层类型列表
   * 
   * @return
   */
  public String getStepTypeList() {
    List<String[]> stepTypeList = Lists.newArrayList();
    String stepInfo = getStepInfo();
    if (null != stepInfo) {
      String[] steps = stepInfo.split("\n");
      for (String step : steps) {
        Map<String, String> map = getStepElement(step.split(";"));
        if (MapUtils.isNotEmpty(map)) {
          String[] stepType = new String[2];
          stepType[0] = map.get("id");
          stepType[1] = map.get("title");
          stepTypeList.add(stepType);
        }
      }
    }
    LOGGER.info("楼层类型数据：" + JackSonUtil.getJson(stepTypeList));
    return JackSonUtil.getJson(Result.getSuccDataResult(stepTypeList));
  }

  /**
   * 获取每一楼层的kv对
   * 
   * @param elements
   * @return
   */
  private Map<String, String> getStepElement(String[] elements) {
    if (null == elements) {
      return null;
    }
    Map<String, String> map = Maps.newHashMap();
    for (String element : elements) {
      String[] kv = element.split("=");
      map.put(kv[0], kv[1]);
    }
    return map;
  }

  /**
   * 读取配置文件楼层信息
   * 
   * @return
   */
  private String getStepInfo() {
    FileReader reader = null;
    BufferedReader br = null;
    try {
      reader = new FileReader(STEP_FILE);
      br = new BufferedReader(reader);
      StringBuffer buffer = new StringBuffer();
      String line = "";
      while (null != (line = br.readLine())) {
        buffer.append(line);
        buffer.append("\n");
      }
      LOGGER.info(buffer);
      return buffer.toString();
    } catch (FileNotFoundException e) {
      LOGGER.error("文件未找到：" + e);
      e.printStackTrace();
    } catch (IOException e) {
      LOGGER.error("IO错误：" + e);
      e.printStackTrace();
    } finally {
      if (null != br) {
        try {
          br.close();// 关闭流
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if (null != reader) {
        try {
          reader.close();// 关闭文件
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  /**
   * 校验商品、图片、标题是否符合要求
   * 
   * @param itemRequire
   * @param itemMax
   * @param itemSize
   * @return
   */
  private Result<Boolean> checkElements(Integer require, Integer max, Integer min, Integer size,
      String element) {
    if (require == 0) {
      if (size > 0) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "此楼层不支持"
            + element + "数据的展示");
      }
    } else {
      if (size > max || size < min) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, element
            + "数据不在所规定的范围内(" + min + "," + max + ")");
      }
    }

    return Result.getSuccDataResult(true);
  }
}
