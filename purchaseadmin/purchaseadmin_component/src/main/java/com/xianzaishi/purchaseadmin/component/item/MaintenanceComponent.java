package com.xianzaishi.purchaseadmin.component.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemListVO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;

/**
 * @author wangxiao
 */
@Component("MaintenanceComponent")

public class MaintenanceComponent {

  @Autowired
  private BackGroundUserService backGroundUserService;
  @Autowired
  private SkuService skuService;

  public Result<ItemListVO> getSkuList(Long date) {
    Result<ItemCommoditySkuDTO> result = skuService.queryItemSku(SkuQuery.querySkuById(date));
    if (null == result || !result.getSuccess() || null == result.getModule()) {
      return Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "通过sku码查询数据无结果,此商品不存在");
    }
    ItemCommoditySkuDTO module = result.getModule();
    ItemListVO itemListVO = new ItemListVO();
    itemListVO.setInventory(module.getInventory());
    itemListVO.setPrice(module.getPrice());
    itemListVO.setSaleUnit(module.getSaleUnitStr());
    itemListVO.setSaleStyle(getsalestyle(module.getSaleStatus()));
    itemListVO.setSku69Id(module.getSku69code());
    itemListVO.setSkuId(module.getSkuId());
    itemListVO.setTitle(module.getTitle());
    // 获取供应商
    itemListVO.setCompanyName(
        backGroundUserService.queryUserDTOById(module.getSupplierId()).getModule().getName());
    return Result.getSuccDataResult(itemListVO);

  }

  public Result<ItemListVO> get69List(Long date) {
    Result<ItemCommoditySkuDTO> result =
        skuService.queryItemSku(SkuQuery.querySkuBySku69Code(date));
    ItemCommoditySkuDTO module = result.getModule();
    if (null == result || !result.getSuccess() || null == module) {
      return Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "通过69码或其他查询数据无结果，此商品不存在");
    }
    ItemListVO itemListVO = new ItemListVO();
    itemListVO.setInventory(module.getInventory());
    itemListVO.setPrice(module.getPrice());
    itemListVO.setSaleUnit(module.getSaleUnitStr());
    itemListVO.setSaleStyle(getsalestyle(module.getSaleStatus()));
    itemListVO.setSku69Id(module.getSku69code());
    itemListVO.setSkuId(module.getSkuId());
    itemListVO.setTitle(module.getTitle());
    // 获取供应商
    itemListVO.setCompanyName(
        backGroundUserService.queryUserDTOById(module.getSupplierId()).getModule().getName());
    return Result.getSuccDataResult(itemListVO);

  }

  public Result<ItemListVO> getTitleList(String date) {
    int pageNum = 0;
    int pageSize = 50;
    Result<ItemCommoditySkuDTO> result =
        skuService.queryItemSku(SkuQuery.querySkuByTitle(date, pageSize, pageNum));
    ItemCommoditySkuDTO module = result.getModule();
    if (null == result || !result.getSuccess() || null == module) {
      return Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "通过商品名称查询数据无结果，此商品不存在");
    }
    ItemListVO itemListVO = new ItemListVO();
    itemListVO.setInventory(module.getInventory());
    itemListVO.setPrice(module.getPrice());
    itemListVO.setSaleUnit(module.getSaleUnitStr());
    itemListVO.setSaleStyle(getsalestyle(module.getSaleStatus()));
    itemListVO.setSku69Id(module.getSku69code());
    itemListVO.setSkuId(module.getSkuId());
    itemListVO.setTitle(module.getTitle());
    // 获取供应商
    itemListVO.setCompanyName(
        backGroundUserService.queryUserDTOById(module.getSupplierId()).getModule().getName());
    return Result.getSuccDataResult(itemListVO);

  }

  public String getsalestyle(Short date) {
    StringBuffer sb = new StringBuffer();
    if (date == 1) {
      sb.append("线上线下同步销售");
    } else if (date == 2) {
      sb.append("线上销售");
    } else if (date == 3) {
      sb.append("线下销售");
    }
    return sb.toString();

  }
}
