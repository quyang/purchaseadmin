package com.xianzaishi.purchaseadmin.component.pick;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.pick.vo.PickTokenVO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingBasketDomainClient;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingWallPositionDomainClient;
import com.xianzaishi.wms.tmscore.domain.client.itf.IWaveDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickingWallPositionVO;
import com.xianzaishi.wms.tmscore.vo.WaveVO;

@Component("pickWallComponent")
public class PickWallComponent extends BasePickComponent {

  private static final Logger logger = Logger.getLogger(PickWallComponent.class);

  @Autowired
  private IWaveDomainClient waveDomainClient = null;

  @Autowired
  private IPickingBasketDomainClient pickingBasketDomainClient = null;

  @Autowired
  private IPickingWallPositionDomainClient pickingWallPositionDomainClient = null;

  public Result<WaveVO> getWaveVOByPickingBasketBarcode(BackGroundUserDTO user, String barcode) {
    if (null == user || StringUtils.isEmpty(barcode)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }

    try {
      PickTokenVO tokenVO = getTokenInfo(user);

      SimpleResultVO<WaveVO> waveResult =
          waveDomainClient.getWaveByPickingBasketID(pickingBasketDomainClient
              .getPickingBasketVOByBarcode(barcode).getTarget().getId());
      if (null == waveResult || !waveResult.isSuccess()) {
        String errorMsg = waveResult == null ? "网络连接失败，请稍后再试" : waveResult.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      return Result.getSuccDataResult(waveResult.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }

  }

  public Object getPickingWallPositionByBarcode(BackGroundUserDTO user, String barcode) {
    if (null == user || StringUtils.isEmpty(barcode)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    try {
      SimpleResultVO<PickingWallPositionVO> pickingWallPositionResult =
          pickingWallPositionDomainClient.getPickingWallPositionVOByBarcode(barcode);
      if (null == pickingWallPositionResult || !pickingWallPositionResult.isSuccess()) {
        String errorMsg =
            pickingWallPositionResult == null ? "网络连接失败，请稍后再试" : pickingWallPositionResult
                .getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      return Result.getSuccDataResult(pickingWallPositionResult.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
  }

  public Result<Boolean> assignPickingWallPosition(BackGroundUserDTO user, Long wallPositionId,
      Long assignmentId) {
    if (null == wallPositionId || null == assignmentId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    try {
      PickTokenVO tokenVO = getTokenInfo(user);
      SimpleResultVO<Boolean> waveResult =
          waveDomainClient.assignPickingWallPosition(assignmentId, wallPositionId);
      if (null == waveResult || !waveResult.isSuccess()) {
        String errorMsg = waveResult == null ? "网络连接失败，请稍后再试" : waveResult.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      return Result.getSuccDataResult(waveResult.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
  }

}
