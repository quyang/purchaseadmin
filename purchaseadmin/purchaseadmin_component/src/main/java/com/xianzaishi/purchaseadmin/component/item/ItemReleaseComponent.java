package com.xianzaishi.purchaseadmin.component.item;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

@Component("itemReleaseComponent")
public class ItemReleaseComponent {
  
  @Autowired
  private StdCategoryService stdCategoryservice;
  
  /**
   * 类目id key
   */
  private static final String idKey = "catId";
  
  /**
   * 类目名称key
   */
  private static final String nameKey = "catName";
  
  private static final String leafkey = "leaf";
  
  
  public String queryCategories(Integer parentId){
    if(null == parentId){
      parentId = 0;
      
    }
    Result<List<Map<String, Object>>> result = querySubCategory(parentId);
    
    return toJsonData(result);
  }
  
  /**
   * 按照类目id来查询子类目
   * @param catId 类目id
   * @return
   */
  private Result<List<Map<String, Object>>> querySubCategory(Integer catId){
    Result<List<CategoryDTO>> categoryResult = stdCategoryservice.querySubcategoryById(catId);
    List<CategoryDTO> categoryDTOs = categoryResult.getModule();
    List<Map<String, Object>> catList = Lists.newArrayList();
    for(CategoryDTO categoryDTO : categoryDTOs){
      Map<String,Object> map = Maps.newHashMap();
      Integer categoryId = categoryDTO.getCatId();
      String categoryName = categoryDTO.getCatName();
      Short leaf = categoryDTO.getLeaf();
      map.put(idKey, categoryId);
      map.put(nameKey, categoryName);
      map.put(leafkey, leaf);
      catList.add(map);
    }
    
    return Result.getSuccDataResult(catList);
  }
  
  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

}
