package com.xianzaishi.purchaseadmin.component.salelist;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.qr.QrDetailVO;
import com.xianzaishi.itemcenter.common.qr.QrInfoVO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.base.vo.CollectionVO;
import com.xianzaishi.purchaseadmin.client.sale.vo.SaleListVO;
import com.xianzaishi.purchaseadmin.client.sale.vo.SaleVO;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 销售商品列表实现
 * 
 * @author dongpo
 * 
 */
@Component("saleListComponent")
public class SaleListComponent {
  @Autowired
  private SkuService skuService;

  @Autowired
  private CommodityService commodityService;
  
  @Autowired
  private SupplierService supplierService;

  private static final Logger logger = Logger.getLogger(SaleListComponent.class);


  /**
   * 请求销售商品数据
   * 
   * @param itemKeyWord
   * @param skuId
   * @param skuName
   * @param skuCode 
   * @param pageSize
   * @param pageNum
   * @return
   */
  public String querySaleList(String itemKeyWord, Long skuId, String skuName, Long skuCode, Integer pageSize,
      Integer pageNum) {
    if (null == pageSize) {
      pageSize = 8;
    }
    if (null == pageNum) {
      pageNum = 0;
    }
    if (StringUtils.isEmpty(itemKeyWord) && null == skuId && StringUtils.isEmpty(skuName) && null == skuCode) {
      logger.error("hello");
      return queryDeafultData(pageSize, pageNum);
    }
    String result = null;

    if (StringUtils.isNotEmpty(itemKeyWord)) {
      result = querySaleListByItem(itemKeyWord, pageSize, pageNum);
    } else if (null != skuId) {
      result = querySaleListBySkuId(skuId, pageSize, pageNum);
    } else if (StringUtils.isNotEmpty(skuName)) {
      result = querySaleListBySkuTitle(skuName, pageSize, pageNum);
    }else if(null != skuCode){
      result = querySaleListBySkuCode(skuCode, pageSize, pageNum);
    }
    return result;
  }
  
  /**
   * 通过sku id请求销售商品数据
   * 
   * @param
   * @param pageSize
   * @param pageNum
   * @return
   */
  private String querySaleListBySkuCode(Long skuCode, Integer pageSize, Integer pageNum) {
    Integer totalPage = 1;

    Result<ItemCommoditySkuDTO> itemCommoditySkuResult =
        skuService.queryItemSku(SkuQuery.querySkuBySku69Code(skuCode));// 查询销售商品sku数据
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku数据为空"));
    }
    List<SaleListVO> saleListVOs = Lists.newArrayList();
    SaleListVO saleListVO = new SaleListVO();
    BeanCopierUtils.copyProperties(itemCommoditySkuDTO, saleListVO);
    ItemQuery query = new ItemQuery();
    query.setSkuId(itemCommoditySkuDTO.getSkuId());
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(query);// 查询商品
    ItemBaseDTO itemBaseDTO = itemResult.getModule();
    if (null == itemBaseDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品数据为空"));
    }
    saleListVO.setItemId(itemCommoditySkuDTO.getItemId());// 设置商品id
    saleListVO.setSpecification(itemCommoditySkuDTO.querySpecification());// 设置商品规格
    Short statusCode = itemCommoditySkuDTO.getStatus();
    Short saleStatusCode = itemCommoditySkuDTO.getSaleStatus();
    setStatusStr(statusCode, saleStatusCode, saleListVO);// 设置返回数据的状态、销售状态的中文字符
    saleListVO.setSupplierName(queryItemSupplier(itemCommoditySkuDTO.getSupplierId()));
    saleListVOs.add(saleListVO);
    CollectionVO<SaleListVO> collectionVO = new CollectionVO<>();
    collectionVO.setTotalPage(totalPage);// 设置总页数
    collectionVO.setList(saleListVOs);
    collectionVO.setCurrentPage(pageNum);

    return toJsonData(Result.getSuccDataResult(collectionVO));

  }

  /**
   * 请求默认数据
   * 
   * @param pageSize
   * @param pageNum
   * @return
   */
  private String queryDeafultData(Integer pageSize, Integer pageNum) {

    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySku(pageSize, pageNum));// 查询销售商品sku数据
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTOs || itemCommoditySkuDTOs.size() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku数据为空"));
    }
    Result<Integer> numberResult =
        skuService.queryItemSkuCount(SkuQuery.querySku(pageSize, pageNum));// 查询数量
    Integer number = numberResult.getModule();
    Integer totalPage = getTotalPage(number, pageSize);
    List<SaleListVO> saleListVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(itemCommoditySkuDTOs, saleListVOs, SaleListVO.class);
    List<Long> itemIds = Lists.newArrayList();
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      itemIds.add(itemCommoditySkuDTO.getItemId());
    }
    // ItemListQuery query = new ItemListQuery();
    // query.setItemIds(itemIds);
    // Result<Map<String, Object>> itemResult = commodityService.queryCommodies(query);// 查询商品数据
    // Map<String, Object> itemMap = itemResult.getModule();
    // List<ItemDTO> itemDTOs = (List<ItemDTO>) itemMap.get("itemList");
    // if (null == itemDTOs || itemDTOs.size() == 0) {
    // return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品数据为空"));
    // }
    // Map<Long, String> map = Maps.newHashMap();//存放商品itemId、itemTitle键值对，防止后期一个商品对应多个商品sku
    // for(ItemDTO itemDTO : itemDTOs){
    // map.put(itemDTO.getItemId(), itemDTO.getTitle());
    // }

    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      int index = itemCommoditySkuDTOs.indexOf(itemCommoditySkuDTO);
      SaleListVO saleListVO = saleListVOs.get(index);
      saleListVO.setItemId(itemCommoditySkuDTO.getItemId());
      saleListVO.setSpecification(itemCommoditySkuDTO.querySpecification());// 设置商品规格
      Short statusCode = itemCommoditySkuDTO.getStatus();
      Short saleStatusCode = itemCommoditySkuDTO.getSaleStatus();
      setStatusStr(statusCode, saleStatusCode, saleListVO);
      saleListVO.setSupplierName(queryItemSupplier(itemCommoditySkuDTO.getSupplierId()));
    }

    CollectionVO<SaleListVO> collectionVO = new CollectionVO<>();
    collectionVO.setTotalPage(totalPage);
    collectionVO.setList(saleListVOs);
    collectionVO.setCurrentPage(pageNum);

    return toJsonData(Result.getSuccDataResult(collectionVO));
  }

  /**
   * 通过商品关键字请求销售商品数据
   * 
   * @param itemKeyWord
   * @param pageSize
   * @param pageNum
   * @return
   */
  private String querySaleListByItem(String itemKeyWord, Integer pageSize, Integer pageNum) {
    ItemListQuery query = new ItemListQuery();
    query.setItemTitle(itemKeyWord);
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    Result<Map<String, Object>> itemResult = commodityService.queryCommodies(query);// 通过商品关键字查询商品
    Map<String, Object> itemMap = itemResult.getModule();
    List<ItemDTO> itemDTOs = (List<ItemDTO>) itemMap.get("itemList");
    logger.error("itemDTOs大小=" + itemDTOs.size());
    if (null == itemDTOs || itemDTOs.size() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "没有找到与"
          + itemKeyWord + "相关的商品"));
    }
    List<Long> skuIds = Lists.newArrayList();
    // Map<Long, String> map = Maps.newHashMap();//存放商品itemId、itemTitle键值对，防止后期一个商品对应多个商品sku
    for (ItemDTO itemDTO : itemDTOs) {
      // map.put(itemDTO.getItemId(), itemDTO.getTitle());
      String sku = itemDTO.getSku();
      String[] skuIdList = sku.split(",");
      for (String skuId : skuIdList) {
        skuIds.add(Long.valueOf(skuId));// 获取所有的sku id列表
      }
    }
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));// 查询销售商品sku数据
    if (!itemCommoditySkuResult.getSuccess()) {
      return toJsonData(itemCommoditySkuResult);
    }
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
    logger.error("集合大小=" + itemCommoditySkuDTOs.size());
    if (null == itemCommoditySkuDTOs || itemCommoditySkuDTOs.size() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku数据为空"));
    }
    List<SaleListVO> saleListVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(itemCommoditySkuDTOs, saleListVOs, SaleListVO.class);
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      int index = itemCommoditySkuDTOs.indexOf(itemCommoditySkuDTO);
      SaleListVO saleListVO = saleListVOs.get(index);
      saleListVO.setItemId(itemCommoditySkuDTO.getItemId());
      saleListVO.setSpecification(itemCommoditySkuDTO.querySpecification());// 设置商品title
      Short statusCode = itemCommoditySkuDTO.getStatus();// 获取sku状态
      Short saleStatusCode = itemCommoditySkuDTO.getSaleStatus();// 获取销售状态
      setStatusStr(statusCode, saleStatusCode, saleListVO);
      saleListVO.setSupplierName(queryItemSupplier(itemCommoditySkuDTO.getSupplierId()));
    }
    logger.error("skuIds=" + skuIds);
    Result<Integer> numberResult = skuService.queryItemSkuCount(SkuQuery.querySkuBySkuIds(skuIds));// 查找出来的数量
    Integer number = numberResult.getModule();
//    Integer number = itemCommoditySkuDTOs.size();
    logger.error("总数量=" + number);
    Integer totalPage = getTotalPage(number, pageSize);// 获取分页数量
    logger.error("总页数=" + totalPage);
    CollectionVO<SaleListVO> collectionVO = new CollectionVO<>();
    collectionVO.setTotalPage(totalPage);// 设置总页数
    collectionVO.setList(saleListVOs);
    collectionVO.setCurrentPage(pageNum);

    return toJsonData(Result.getSuccDataResult(collectionVO));

  }

  /**
   * 通过sku id请求销售商品数据
   * 
   * @param skuId
   * @param pageSize
   * @param pageNum
   * @return
   */
  private String querySaleListBySkuId(Long skuId, Integer pageSize, Integer pageNum) {
    Integer totalPage = 1;

    Result<ItemCommoditySkuDTO> itemCommoditySkuResult =
        skuService.queryItemSku(SkuQuery.querySkuById(skuId));// 查询销售商品sku数据
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku数据为空"));
    }
    List<SaleListVO> saleListVOs = Lists.newArrayList();
    SaleListVO saleListVO = new SaleListVO();
    BeanCopierUtils.copyProperties(itemCommoditySkuDTO, saleListVO);
    ItemQuery query = new ItemQuery();
    query.setSkuId(skuId);
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(query);// 查询商品
    ItemBaseDTO itemBaseDTO = itemResult.getModule();
    if (null == itemBaseDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品数据为空"));
    }
    saleListVO.setSupplierName(queryItemSupplier(itemCommoditySkuDTO.getSupplierId()));
    saleListVO.setItemId(itemCommoditySkuDTO.getItemId());// 设置商品id
    saleListVO.setSpecification(itemCommoditySkuDTO.querySpecification());// 设置商品规格
    Short statusCode = itemCommoditySkuDTO.getStatus();
    Short saleStatusCode = itemCommoditySkuDTO.getSaleStatus();
    setStatusStr(statusCode, saleStatusCode, saleListVO);// 设置返回数据的状态、销售状态的中文字符
    saleListVOs.add(saleListVO);
    CollectionVO<SaleListVO> collectionVO = new CollectionVO<>();
    collectionVO.setTotalPage(totalPage);// 设置总页数
    collectionVO.setList(saleListVOs);
    collectionVO.setCurrentPage(pageNum);

    return toJsonData(Result.getSuccDataResult(collectionVO));

  }

  /**
   * 通过sku title请求销售商品数据
   * 
   * @param skuTitle
   * @param pageSize
   * @param pageNum
   * @return
   */
  private String querySaleListBySkuTitle(String skuTitle, Integer pageSize, Integer pageNum) {
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySkuByTitle(skuTitle, pageSize, pageNum));
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTOs || itemCommoditySkuDTOs.size() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku数据为空"));// 查询销售商品
    }
    Result<Integer> numberResult =
        skuService.queryItemSkuCount(SkuQuery.querySkuByTitle(skuTitle, pageSize, pageNum));// 查询出来的数量
    Integer number = numberResult.getModule();
    Integer totalPage = getTotalPage(number, pageSize);
    List<SaleListVO> saleListVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(itemCommoditySkuDTOs, saleListVOs, SaleListVO.class);
    List<Long> itemIds = Lists.newArrayList();
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      itemIds.add(itemCommoditySkuDTO.getItemId());// 获取商品id
    }
    // ItemListQuery query = new ItemListQuery();
    // query.setItemIds(itemIds);
    // Result<Map<String, Object>> itemResult = commodityService.queryCommodies(query);// 查询商品数据
    // Map<String, Object> itemMap = itemResult.getModule();
    // List<ItemDTO> itemDTOs = (List<ItemDTO>) itemMap.get("itemList");
    // if (null == itemDTOs || itemDTOs.size() == 0) {
    // return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品数据为空"));
    // }
    // Map<Long, String> map = Maps.newHashMap();//存放商品itemId、itemTitle键值对，防止后期一个商品对应多个商品sku
    // for(ItemDTO itemDTO : itemDTOs){
    // map.put(itemDTO.getItemId(), itemDTO.getTitle());
    // }
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      int index = itemCommoditySkuDTOs.indexOf(itemCommoditySkuDTO);
      SaleListVO saleListVO = saleListVOs.get(index);
      saleListVO.setSupplierId(itemCommoditySkuDTO.getSupplierId());
      saleListVO.setItemId(itemCommoditySkuDTO.getItemId());
      saleListVO.setSpecification(itemCommoditySkuDTO.querySpecification());// 设置商品规格
      Short statusCode = itemCommoditySkuDTO.getStatus();
      Short saleStatusCode = itemCommoditySkuDTO.getSaleStatus();
      setStatusStr(statusCode, saleStatusCode, saleListVO);
      saleListVO.setSupplierName(queryItemSupplier(itemCommoditySkuDTO.getSupplierId()));
    }

    CollectionVO<SaleListVO> collectionVO = new CollectionVO<SaleListVO>();
    collectionVO.setTotalPage(totalPage);
    collectionVO.setList(saleListVOs);
    collectionVO.setCurrentPage(pageNum);

    return toJsonData(Result.getSuccDataResult(collectionVO));

  }

  /**
   * 设置状态、销售状态的中文字符
   * 
   * @param status
   * @param saleStatus
   * @param saleListVO
   */
  private void setStatusStr(Short status, Short saleStatus, SaleListVO saleListVO) {
    saleListVO.setStatusCode(status);
    saleListVO.setSaleStatusCode(saleStatus);
    if (null == status
        || ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES.shortValue() == status.shortValue()) {
      saleListVO.setStatus("审核通过并上架");
    } else if (ItemStatusConstants.ITEM_STATUS_CHECKED_NOTPASS.shortValue() == status.shortValue()) {
      saleListVO.setStatus("审核不通过状态");
    } else if (ItemStatusConstants.ITEM_STATUS_DRAFT.shortValue() == status.shortValue()) {
      saleListVO.setStatus("草稿版本");
    } else if (ItemStatusConstants.ITEM_STATUS_PASS_AND_DOWN_SHELVES.shortValue() == status
        .shortValue()) {
      saleListVO.setStatus("审核通过状态并未上架");
    } else if (ItemStatusConstants.ITEM_STATUS_PRE.shortValue() == status.shortValue()) {
      saleListVO.setStatus("预发布状态");
    } else if (ItemStatusConstants.ITEM_STATUS_WAITCHECK.shortValue() == status.shortValue()) {
      saleListVO.setStatus("正在待审核状态");
    } else {
      saleListVO.setStatus("其他状态");
    }

    if (null == saleStatus
        || SaleStatusConstants.SALE_ONLINE_AND_MARKET.shortValue() == saleStatus.shortValue()) {
      saleListVO.setSaleStatus("线上线下同步销售");
    } else if (SaleStatusConstants.SALE_ONLY_MARKET.shortValue() == saleStatus.shortValue()) {
      saleListVO.setSaleStatus("线下销售");
    } else if (SaleStatusConstants.SALE_ONLY_ONLINE.shortValue() == saleStatus.shortValue()) {
      saleListVO.setSaleStatus("线上销售");
    } else {
      saleListVO.setSaleStatus("其他销售方式");
    }
  }

  /**
   * 获取分页数量
   * 
   * @param number 商品数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

  public String createQrCode(Long qrId, Short qrType) {
    return JackSonUtil.getJson(Result.getSuccDataResult(getQrCode(qrId,qrType)));
  }

  /**
   * 获取二维码信息
   * 
   * @param
   * @param qrType 
   * @return
   */
  private String getQrCode(Long qrId, Short qrType) {
    QrInfoVO qrInfoVO = new QrInfoVO();
    QrDetailVO qrDetailVO = new QrDetailVO();
    Map<String, String> queryParameterMap = Maps.newHashMap();
    queryParameterMap.put("id", String.valueOf(qrId));
    qrDetailVO.setQueryParameter(queryParameterMap);
    if(qrType.shortValue() == 0){
      qrDetailVO.setData("detail");
    }else{
      qrDetailVO.setData("activity");
    }
    qrDetailVO.setQueryType("get");
    qrInfoVO.setQrInfoType("mobile");
    qrInfoVO.setQueryDetail(qrDetailVO);
    return JackSonUtil.getJson(qrInfoVO);
  }

  /**
   * 上架
   * 
   * @param skuId
   * @return
   */
  public String onShelf(Long skuId) {
    Result<ItemCommoditySkuDTO> itemCommoditySkuResult =
        skuService.queryItemSku(SkuQuery.querySkuById(skuId));// 查询item_commodity_sku表数据
    if (null == itemCommoditySkuResult || !itemCommoditySkuResult.getSuccess()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku不存在"));
    }
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku不存在"));
    }
    
    ItemQuery itemQuery = new ItemQuery();//商品查询条件
    itemQuery.setItemId(itemCommoditySkuDTO.getItemId());
    itemQuery.setReturnAll(true);//返回全部数据
    Result<ItemBaseDTO> itemBaseResult = commodityService.queryCommodity(itemQuery);//查询商品
    if(null == itemBaseResult || !itemBaseResult.getSuccess() || null == itemBaseResult.getModule()){
      return toJsonData(itemBaseResult);
    }
    ItemDTO itemDTO = (ItemDTO) itemBaseResult.getModule();
    if (null != itemDTO.getStatus()
        && itemDTO.getStatus().equals(
            ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES)) {
      return toJsonData(Result.getErrDataResult(
          ServerResultCode.SERVER_DB_ERROR_COMMODITY_SKU_UPDATE, "该商品已经上架，请勿重复上架"));

    }
    itemCommoditySkuDTO.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);//sku设置上架
    itemDTO.setItemSkuDtos(Arrays.asList(itemCommoditySkuDTO));
    
//    Result<Boolean> result = onShelfCondition(skuId, itemCommoditySkuDTO);// 是否符合上架条件
//    if (null == result) {
//      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "系统出现错误"));
//    }
//    if (!result.getSuccess() || null == result.getModule() || !result.getModule()) {
//      return toJsonData(result);
//    }
    itemDTO.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);//商品设置上架
    Result<Boolean> updateResult = commodityService.updateCommodity(itemDTO);
    return toJsonData(updateResult);
  }

  /**
   * 上架前置条件
   * 
   * @param skuId
   * @param itemCommoditySkuDTO
   * @return
   */
  private Result<Boolean> onShelfCondition(Long skuId, ItemCommoditySkuDTO itemCommoditySkuDTO) {
    if (null == skuId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "skuId为空");
    }
    Result<List<ItemSkuRelationDTO>> itemSkuRelationResult =
        skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(Arrays.asList(skuId),
            null));
    if (null == itemSkuRelationResult) {
      return null;
    }
    if (!itemSkuRelationResult.getSuccess()) {
      return Result.getErrDataResult(itemSkuRelationResult.getResultCode(),
          itemSkuRelationResult.getErrorMsg());
    }
    List<ItemSkuRelationDTO> itemSkuRelationDTOs = itemSkuRelationResult.getModule();
    if (CollectionUtils.isEmpty(itemSkuRelationDTOs)) {
      return Result.getErrDataResult(ServerResultCode.SKU_RELATION_INFO_NOT_EXIT,
          "sku relation数据为空");
    }
    int size = itemSkuRelationDTOs.size() > 2 ? 2 : itemSkuRelationDTOs.size();
    int max = 0;
    for (int i = 0; i < size; i++) {
      ItemSkuRelationDTO itemSkuRelationDTO = itemSkuRelationDTOs.get(i);
      if (max < itemSkuRelationDTO.getRelationType()) {
        max = itemSkuRelationDTO.getRelationType();
      }
    }

    Integer price = itemCommoditySkuDTO.getPrice();
    Integer discountPrice = itemCommoditySkuDTO.getDiscountPrice();
    for (; max >= 0; max--) {
      ItemSkuRelationDTO itemSkuRelationDTO = itemSkuRelationDTOs.get(max);
      Date start = itemSkuRelationDTO.getSaleStart();
      Date end = itemSkuRelationDTO.getSaleEnd();
      if (checkInTime(start, end)) {
        if (null != itemSkuRelationDTO.getPrice() && itemSkuRelationDTO.getPrice().equals(price)) {
          return Result.getSuccDataResult(true);
        }
      }
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "当前时间不在商品销售时间范围内或者价格不同步");
  }

  /**
   * 判断是否在销售时间内上架
   * 
   * @param start
   * @param end
   * @return
   */
  private boolean checkInTime(Date start, Date end) {
    if (null == start || null == end) {
      return true;
    }
    Long now = new Date().getTime();
    if (now >= start.getTime() && now <= end.getTime()) {
      return true;
    }
    return false;
  }

  /**
   * 商品下架
   * 
   * @param skuId
   * @return
   */
  public String offShelf(Long skuId) {
    Result<ItemCommoditySkuDTO> itemCommoditySkuResult =
        skuService.queryItemSku(SkuQuery.querySkuById(skuId));
    if (null == itemCommoditySkuResult || !itemCommoditySkuResult.getSuccess()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku不存在"));
    }
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuResult.getModule();
    if (null == itemCommoditySkuDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "商品sku不存在"));
    }
    
    itemCommoditySkuDTO.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_DOWN_SHELVES);
    ItemQuery itemQuery = new ItemQuery();//商品查询条件
    itemQuery.setItemId(itemCommoditySkuDTO.getItemId());
    itemQuery.setReturnAll(true);//返回全部数据
    Result<ItemBaseDTO> itemBaseResult = commodityService.queryCommodity(itemQuery);//查询商品
    if(null == itemBaseResult || !itemBaseResult.getSuccess() || null == itemBaseResult.getModule()){
      return toJsonData(itemBaseResult);
    }
    ItemDTO itemDTO = (ItemDTO) itemBaseResult.getModule();
    if (null != itemDTO.getStatus()
        && itemDTO.getStatus().equals(
            ItemStatusConstants.ITEM_STATUS_PASS_AND_DOWN_SHELVES)) {
      return toJsonData(Result.getErrDataResult(
          ServerResultCode.SERVER_DB_ERROR_COMMODITY_SKU_UPDATE, "该商品已经下架，请勿重复下架"));

    }
    itemDTO.setItemSkuDtos(Arrays.asList(itemCommoditySkuDTO));
    itemDTO.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_DOWN_SHELVES);
    Result<Boolean> updateResult = commodityService.updateCommodity(itemDTO);
    return toJsonData(updateResult);
  }

  /**
   * 修改销售渠道
   */
  public String updateChannel(SaleVO data) {
    if (null == data || null == data.getChannel() || data.getChannel() < 0 || null == data
        .getSkuId() || data.getSkuId() < 0) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常"));
    }

    Result<Boolean> booleanResult = skuService
        .updateItemSkuChannel(data.getSkuId(), data.getChannel());

    logger.error("更新结果="+JackSonUtil.getJson(booleanResult));

    if (null != booleanResult && booleanResult.getSuccess() && null != booleanResult.getModule()
        && booleanResult.getModule()) {
      return JackSonUtil
          .getJson(Result.getSuccDataResult(true));
    }

    return JackSonUtil
        .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新销售渠道失败"));
  }

  /**
   * 更新商品供应商
   * @param skuId
   * @param oldSupplierId
   * @param newSupplierId
   * @return
   */
  public Result<Boolean> updateItemSupplier(Long skuId, Integer oldSupplierId, Integer newSupplierId) {
    if(null == skuId || null == oldSupplierId || null == newSupplierId){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    if(oldSupplierId.intValue() == newSupplierId.intValue()){
      return Result.getSuccDataResult(true);
    }
    Result<Boolean> result;
    try {
      Result<SupplierDTO> supplierResult = supplierService.querySupplierById(newSupplierId, false);
      if(null == supplierResult || !supplierResult.getSuccess() || null == supplierResult.getModule()){
        logger.error("[updateItemSupplier] supplier not exist with supplier id:" + newSupplierId);
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "要更换的供应商不存在");
      }
      result = skuService.updateItemSkuSupplier(skuId, newSupplierId);
    } catch (Exception e) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
    return result;
  }
  

  /**
   * 根据供应商id获取供应商名称
   * @param supplierId
   * @return
   */
  private String queryItemSupplier(Integer supplierId) {
    if(null == supplierId || supplierId <= 0){
      return null;
    }
    Result<SupplierDTO> supplierResult = supplierService.querySupplierById(supplierId, false);
    if(null == supplierResult || !supplierResult.getSuccess() || null == supplierResult.getModule()){
      return null;
    }
    SupplierDTO supplierDTO = supplierResult.getModule();
    return supplierDTO.getName();
  }

  /**
   * 清除商品json接口
   * @param skuId
   * @param status 
   * @return
   */
  public Result<Boolean> clearItem(Long skuId, Short status) {
    if(null == skuId){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    if(ItemStatusConstants.ITEM_STATUS_CHECKED_NOTPASS.equals(status)){
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST, "该商品已经清除，请勿重复操作");
    }
    Result<Boolean> result = skuService.clearItemSku(skuId);
    return result;
  }

}
