package com.xianzaishi.purchaseadmin.component.custom;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.coupon.CouponService;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskDealTypeConstants;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskStatusConstants;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskTypeConstants;
import com.xianzaishi.customercenter.client.customerservice.dto.ProofDataDTO;
import com.xianzaishi.customercenter.client.customerservice.query.CustomerserviceQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.custom.vo.ApplyRefundQueryVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomCouponVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomDetailVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomInfoVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomTaskVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.EmployeeVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.OrderInfoVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.TaskInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.QueryDataVO;
import com.xianzaishi.purchasecenter.client.role.RoleService;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;
import com.xianzaishi.wms.track.vo.StorageVO.StorageReasonType;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 客服实现类
 *
 * @author dongpo
 */
@Component("customComponent")
public class CustomComponent {

  private static final String TASK_TYPE = "交易退款";
  private static final String TASK_TYPE_OTHER = "其他类型";
  private static final String LEFT_BRACKET = "(";
  private static final String RIGHT_BRACKET = ")";
  private static final String UNDER_LINE = "_";
  private static final String DEFAULT_UNIT = "份";
  private static final String CUSTOM_TASK_KEY = "customTask";
  private static final String TOTAL_PAGE_KEY = "totalPage";
  private static final Long COUPON_DEAD_TIME = 2592000000L;// 30天时间
  private static final Logger LOGGER = Logger.getLogger(CustomComponent.class);

  @Autowired
  private CustomerService customerService;

  @Autowired
  private UserService userService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private SkuService skuService;

  @Autowired
  private ValueService valueService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private BackGroundUserService backGroundUserService;

  @Autowired
  private CouponService couponService;



  /**
   * 查询客服任务
   */
  public String queryTasks(String start, String end, Integer pageSize, Integer pageNum, Short type,
      Short status) {
    CustomerserviceQuery query = new CustomerserviceQuery();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    if (null != start) {
      try {
        query.setBegin(sdf.parse(start));
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    if (null != end) {
      try {
        query.setEnd(sdf.parse(end));
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    if (null == pageSize) {
      pageSize = 20;
    }
    if (null == pageNum) {
      pageNum = 0;
    }
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    if (null != type) {
      query.setType(type);
    }
    if (null != status) {
      query.setStatus(status);
    }
    Result<List<CustomerServiceTaskDTO>> customerServiceTaskResult =
        customerService.queryCustomerServiceTask(query);
    if (null == customerServiceTaskResult || !customerServiceTaskResult.getSuccess()) {
      return JackSonUtil.getJson(Result.getSuccDataResult(null));
    }
    List<CustomerServiceTaskDTO> customerServiceTaskDTOs = customerServiceTaskResult.getModule();
    if (CollectionUtils.isEmpty(customerServiceTaskDTOs)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR, "任务为空"));
    }
    List<CustomTaskVO> customTaskVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(customerServiceTaskDTOs, customTaskVOs, CustomTaskVO.class);
    if (CollectionUtils.isEmpty(customTaskVOs)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "服务拷贝错误"));
    }
    for (CustomerServiceTaskDTO customerServiceTaskDTO : customerServiceTaskDTOs) {
      int index = customerServiceTaskDTOs.indexOf(customerServiceTaskDTO);
      CustomTaskVO customTaskVO = customTaskVOs.get(index);
      customTaskVO.setTypeCode(customerServiceTaskDTO.getType());
      customTaskVO.setType(getTaskType(customerServiceTaskDTO.getType()));
      if (customerServiceTaskDTO.getType().equals(CustomerServiceTaskTypeConstants.TYPE_REFUND)) {
        customTaskVO.setBizId("订单号：" + customerServiceTaskDTO.getBizId());
      }
      customTaskVO.setGmtCreate(sdf.format(customerServiceTaskDTO.getGmtCreate()));
      customTaskVO.setStatusString(getStatusString(customerServiceTaskDTO.getStatus()));//任务状态
      customTaskVO.setProcessType(customerServiceTaskDTO.getProcessType() == null ? CustomerServiceTaskDealTypeConstants.DEALING : customerServiceTaskDTO.getProcessType());//设置处理类型
      customTaskVO.setProcessTypeString(getRightProcessTypeString(customTaskVO.getProcessType()));
      if (CustomerServiceTaskTypeConstants.TYPE_RF_OTHER.equals(type)) {//是rf枪审核入库
        customTaskVO.setOpReason(StorageReasonType.OTHER);
        customTaskVO.setOpReasonString("其他原因");
      }

    }
    Result<Integer> countResult = customerService.queryCustomerServiceTaskCount(query);
    Integer count = 20;
    if (null != countResult && countResult.getSuccess() || null != countResult) {
      count = countResult.getModule();
    }
    Map<String, Object> map = Maps.newHashMap();
    map.put(CUSTOM_TASK_KEY, customTaskVOs);
    map.put(TOTAL_PAGE_KEY, getTotalPage(count, pageSize));
    map.put("currentPage", pageNum);//当前页
    return JackSonUtil.getJson(Result.getSuccDataResult(map));
  }

  /**
   * 获取正确的处理方式string
   * @param processType
   * @return
   */
  private String getRightProcessTypeString(Short processType) {
    String result = "正在处理";
    if (null == processType ) {
      return result;
    }

    switch (processType) {
      case 0:
        result = "无需处理";
        break;
      case 1:
        result = "全部退款";
        break;
      case 2:
        result = "全部退款并赠送优惠券";
        break;
      case 3:
        result = "部分退款";
        break;
      case 4:
        result = "部分退款并优惠券";
        break;
      case 5:
        result = "赠送优惠券";
        break;
      case 6:
        result = "正在处理";
        break;
      case 7:
        result = "提交审核";
        break;
    }

    return result;

  }

  /**
   * 翻译任务类型为中文字符
   */
  private String getTaskType(Short type) {
    String typeString = null;
    if (type.shortValue() == CustomerServiceTaskTypeConstants.TYPE_REFUND) {
      typeString = "交易退款";
    } else if(type.shortValue() == CustomerServiceTaskTypeConstants.TYPE_RF_OTHER) {
      typeString = "入库审核";
    }else if(type.shortValue() == CustomerServiceTaskTypeConstants.TYPE_RF_OUT_STORAGE) {
      typeString = "出库审核";
    }
    return typeString;
  }

  /**
   * 获取任务状态信息
   */
  private String getStatusString(Short status) {

    String statuString = null;
    if (CustomerServiceTaskStatusConstants.DEALING.shortValue() == status.shortValue()) {
      statuString = "处理中";
    } else if (CustomerServiceTaskStatusConstants.END.shortValue() == status.shortValue()) {
      statuString = "处理完成";
    } else if (CustomerServiceTaskStatusConstants.AUDIT.shortValue() == status.shortValue()) {
      statuString = "审核通过";
    } else if (CustomerServiceTaskStatusConstants.WAIT_DEAL.shortValue() == status.shortValue()) {
      statuString = "待处理";
    }
    return statuString;
  }

  /**
   * 获取客服详情信息
   */
  public String queryCustomDetail(Long id) {
    if (null == id) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Result<CustomerServiceTaskDTO> customerServiceTaskResult =
        customerService.queryCustomerServiceTaskById(id);
    if (null == customerServiceTaskResult || !customerServiceTaskResult.getSuccess()) {
      return JackSonUtil.getJson(customerServiceTaskResult);
    }
    CustomerServiceTaskDTO customerServiceTaskDTO = customerServiceTaskResult.getModule();
    if (null == customerServiceTaskDTO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR, "客服任务不存在"));
    }
    CustomInfoVO customInfoVO = getUserInfo(customerServiceTaskDTO);
    if (null == customInfoVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "用户不存在"));
    }
    TaskInfoVO taskInfoVO = getTaskInfo(customerServiceTaskDTO);
    taskInfoVO.setSum(customerServiceTaskDTO.getRefoudAmount() == null ? "0.00"
        : BigDecimal.valueOf(customerServiceTaskDTO.getRefoudAmount()).divide(new BigDecimal(100))
            .toString());//退款金额
    taskInfoVO.setRefoudReason(StringUtils.isEmpty(customerServiceTaskDTO.getRefoudReason()) ? "无"
        : customerServiceTaskDTO.getRefoudReason());//退款原因
    taskInfoVO.setAudited(CustomerServiceTaskStatusConstants.AUDIT
        .equals(customerServiceTaskDTO.getStatus()) || CustomerServiceTaskStatusConstants.END.equals(customerServiceTaskDTO.getStatus()));//是否提交过审核状态任务
    taskInfoVO.setStatus(customerServiceTaskDTO.getStatus());//退款任务状态
    taskInfoVO.setTaskId(customerServiceTaskDTO.getId());//退款任务id
    taskInfoVO.setTaskCompleted(
        CustomerServiceTaskStatusConstants.END.equals(customerServiceTaskDTO.getStatus()));
    LOGGER.error("退款任务中的优惠券id="+customerServiceTaskDTO.getCouponId());
    if (null != customerServiceTaskDTO.getCouponId()) {
      taskInfoVO.setCouponId(customerServiceTaskDTO.getCouponId());//赠送的优惠券id
      Result<CouponDTO> couponDTOResult = couponService
          .getCouponById(customerServiceTaskDTO.getCouponId());
      LOGGER.error("查询到的优惠券信息="+JackSonUtil.getJson(couponDTOResult));
      if (null != couponDTOResult && couponDTOResult.getSuccess() && null != couponDTOResult
          .getModule()) {
        taskInfoVO.setCouponTitle(couponDTOResult.getModule().getTitle() == null ? "无优惠券" : couponDTOResult.getModule().getTitle());//优惠券名称
      }
    }

    if (null == taskInfoVO.getOrderId()) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR, "交易详细数据为空"));
    }
    List<OrderInfoVO> orderInfoVOs = getOrderInfo(taskInfoVO.getOrderId());
    if (CollectionUtils.isEmpty(orderInfoVOs)) {
      LOGGER.error("orderInfo:" + JackSonUtil.getJson(orderInfoVOs));
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR, "交易详细数据为空"));
    }

    CustomDetailVO customDetailVO = new CustomDetailVO();
    customDetailVO.setCustomInfo(customInfoVO);
    customDetailVO.setOrderInfo(orderInfoVOs);
    customDetailVO.setTaskInfo(taskInfoVO);
    customDetailVO.setCoupons(gainCoupons());// 设置优惠券信息
    return JackSonUtil.getJson(Result.getSuccDataResult(customDetailVO));
  }

  /**
   * 用户申请售后插入客服任务信息
   */
  public String insertCustomTaskInfo(ApplyRefundQueryVO applyRefundQueryVO) {

    CustomerServiceTaskDTO customerServiceTask = getCustomerServiceTask(applyRefundQueryVO);
    if (null == customerServiceTask) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "服务内部错误，插入数据失败"));
    }
    Result<Long> insertResult = customerService.insertCustomerServiceTask(customerServiceTask);
    return JackSonUtil.getJson(insertResult);
  }

  /**
   * 获取客服任务信息
   */
  private CustomerServiceTaskDTO getCustomerServiceTask(ApplyRefundQueryVO applyRefundQueryVO) {
    CustomerServiceTaskDTO customerServiceTask = new CustomerServiceTaskDTO();
    String bizId = applyRefundQueryVO.getBizId();
    Long orderId = Long.valueOf(bizId);// 订单id
    com.xianzaishi.trade.client.Result<OrderVO> orderResult = orderService
        .getOrder(orderId);// 订单详情数据
    if (null == orderResult || null == orderResult.getModel()) {
      LOGGER.info("订单不存在：" + JackSonUtil.getJson(orderResult));
      return null;
    }
    OrderVO orderVO = orderResult.getModel();
    Long userId = orderVO.getUserId();
    Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
    if (null == userResult || !userResult.getSuccess() || null == userResult.getModule()) {
      LOGGER.error("用户信息：" + JackSonUtil.getJson(userResult));
      return null;
    }
    BaseUserDTO baseUserDTO = userResult.getModule();
    String userName = baseUserDTO.getName();// 用户名
    Long phone = baseUserDTO.getPhone();// 用户手机号
    StringBuffer customTitle = new StringBuffer();// 客服任务标题，用户名+用户手机号+任务创建时间
    customTitle.append(userName);
    customTitle.append(LEFT_BRACKET);
    customTitle.append(phone);
    customTitle.append(RIGHT_BRACKET);
    customTitle.append(UNDER_LINE);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    customTitle.append(sdf.format(customerServiceTask.getGmtCreate()));
    customerServiceTask.setTitle(customTitle.toString());
    customerServiceTask.setBizId(bizId);// 订单id
    customerServiceTask.setCustomerUser(userId);// 用户id
    if (null != applyRefundQueryVO.getCustomerProof()) {
      List<ProofDataDTO> proofDataDTOs = Lists.newArrayList();// 用户传过来的证据
      BeanCopierUtils.copyListBean(applyRefundQueryVO.getCustomerProof(), proofDataDTOs,
          ProofDataDTO.class);
      customerServiceTask.setCustomerProofList(proofDataDTOs);
    }
    return customerServiceTask;
  }

  /**
   * 获取订单信息
   */
  private List<OrderInfoVO> getOrderInfo(Long orderId) {
    com.xianzaishi.trade.client.Result<OrderVO> orderResult = orderService.getOrder(orderId);
    if (null == orderResult || null == orderResult.getModel()) {
      LOGGER.error("订单数据为空:" + JackSonUtil.getJson(orderResult));
      return null;
    }
    OrderVO orderVO = orderResult.getModel();
    List<OrderItemVO> orderItemVOs = orderVO.getItems();
    List<OrderInfoVO> orderInfoVOs = Lists.newArrayList();
    for (OrderItemVO orderItemVO : orderItemVOs) {
      OrderInfoVO orderInfoVO = new OrderInfoVO();
      orderInfoVO.setSkuId(orderItemVO.getSkuId());
      orderInfoVO.setTitle(orderItemVO.getName());
      orderInfoVO.setCouponId(orderVO.getCouponId());
      orderInfoVO.setOrderId(orderVO.getId());
      orderInfoVO.setTotalPrice(String.valueOf(orderItemVO.getEffePrice()));// 商品总价格
      orderInfoVO.setCount(orderItemVO.getCount());
      String unit = getSkuUnit(orderItemVO.getSkuId());
      if (null == unit) {
        unit = DEFAULT_UNIT;
      }
      orderInfoVO.setUnit(unit);// 设置数量单位
      orderInfoVOs.add(orderInfoVO);
    }

    return orderInfoVOs;
  }

  /**
   * 获取sku单位
   */
  private String getSkuUnit(Long skuId) {
    Result<ItemCommoditySkuDTO> itemCommoditySkuResult =
        skuService.queryItemSku(SkuQuery.querySkuById(skuId));
    if (null == itemCommoditySkuResult || !itemCommoditySkuResult.getSuccess()
        || null == itemCommoditySkuResult.getModule()) {
      return null;
    }
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuResult.getModule();
    Result<String> valueResult =
        valueService.queryValueNameByValueId(itemCommoditySkuDTO.getSkuUnit());
    if (null == valueResult || !valueResult.getSuccess() || null == valueResult.getModule()) {
      return null;
    }
    return valueResult.getModule();
  }

  /**
   * 获取用户信息
   */
  private CustomInfoVO getUserInfo(CustomerServiceTaskDTO customerServiceTaskDTO) {
    Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(customerServiceTaskDTO.getCustomerUser(), true);
    if (null == userResult || !userResult.getSuccess()) {
      return null;
    }
    UserDTO userDTO = (UserDTO) userResult.getModule();
    if (null == userDTO) {
      return null;
    }
    CustomInfoVO customInfoVO = new CustomInfoVO();
    BeanCopierUtils.copyProperties(userDTO, customInfoVO);
    return customInfoVO;
  }

  /**
   * 获取任务信息
   */
  private TaskInfoVO getTaskInfo(CustomerServiceTaskDTO customerServiceTaskDTO) {
    TaskInfoVO taskInfoVO = new TaskInfoVO();
    String type = null;
    if (customerServiceTaskDTO.getType().equals(CustomerServiceTaskTypeConstants.TYPE_REFUND)) {
      type = TASK_TYPE;
      // List<ProcessDTO> processDTOs = customerServiceTaskDTO.getCustomerRequireProcess();
      // if(null == processDTOs){
      // processDTOs = Lists.newArrayList();
      // }
      // RefundProcessDTO refundProcessDTO = new RefundProcessDTO();
      // for (ProcessDTO processDTO : processDTOs) {
      // if (processDTO instanceof RefundProcessDTO) {
      // refundProcessDTO = (RefundProcessDTO) processDTO;
      // }
      // }
      taskInfoVO.setOrderId(Long.valueOf(customerServiceTaskDTO.getBizId()));// 设置订单id
    } else {
      type = TASK_TYPE_OTHER;
    }
    taskInfoVO.setType(type);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    taskInfoVO.setGmtCreate(sdf.format(customerServiceTaskDTO.getGmtCreate()));
    taskInfoVO.setTaskDescription(customerServiceTaskDTO.getCustomerProofList());
    return taskInfoVO;
  }

  /**
   * 获取分页数量
   *
   * @param number 商品数量
   * @param pageSize 分页大小
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  /**
   * 任务完成时改变任务状态
   */
  public Boolean taskComplete(Long taskId) {
    if (null == taskId) {
      return false;
    }
    Result<CustomerServiceTaskDTO> customerResult =
        customerService.queryCustomerServiceTaskById(taskId);
    if (null == customerResult || !customerResult.getSuccess()
        || null == customerResult.getModule()) {
      return false;
    }
    CustomerServiceTaskDTO customerServiceTaskDTO = customerResult.getModule();
    customerServiceTaskDTO.setStatus(CustomerServiceTaskStatusConstants.END);
    customerServiceTaskDTO.setProcessType(CustomerServiceTaskDealTypeConstants.NOT_NEED);
    Result<Boolean> updateResult =
        customerService.updateCustomerServiceTask(customerServiceTaskDTO);
    LOGGER.error("更新结果=" + JackSonUtil.getJson(updateResult));
    if (null != updateResult && updateResult.getSuccess() && null != updateResult.getModule()
        && updateResult.getModule()) {
      return true;
    }

    return false;
  }

  /**
   * 确认退款
   */
  public String confirmRefund(String orderId, Double sum, BackGroundUserDTO customerInfo,
      String refundReason) {
    if (StringUtils.isEmpty(orderId) || null == sum) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    if (null == customerInfo || null == customerInfo.getUserId()) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "客服用户参数为空"));
    }

    com.xianzaishi.trade.client.Result<OrderVO> orderResult =
        orderService.getOrder(Long.valueOf(orderId));
    if (null == orderResult || orderResult.getCode() < 0 || null == orderResult.getModel()) {
      LOGGER.error("获取订单失败，原因：" + JackSonUtil.getJson(orderResult));
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "获取订单失败"));
    }
    OrderVO orderVO = orderResult.getModel();
    if (null != orderVO.getStatus() && orderVO.getStatus() == 10) {// 如果订单状态为退款成功或者交易关闭，那么不能再次退款
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "该订单已退款，请勿重复退款"));
    }
    if (null != orderVO.getStatus() && orderVO.getStatus() == 11) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "该订单交易已关闭，不支持退款"));
    }
    com.xianzaishi.trade.client.Result<Boolean> result =
        orderService.refund(Long.valueOf(orderId), sum, refundReason,
            customerInfo.getUserId());// 退款，包括退现金、积分、优惠券
    if (null != result && result.getCode() >= 0 && null != result.getModel() && result.getModel()) {
      LOGGER.info("退款是否成功：" + result.getModel());
      //退款成功
      //退款成功修改退款任务状态
      CustomerserviceQuery customerserviceQuery = new CustomerserviceQuery();
      customerserviceQuery.setBizId(orderId);
      Result<List<CustomerServiceTaskDTO>> queryCustomerResult = customerService
          .queryCustomerServiceTask(customerserviceQuery);
      if (null != queryCustomerResult && queryCustomerResult.getSuccess()
          && null != queryCustomerResult.getModule() && CollectionUtils
          .isNotEmpty(queryCustomerResult.getModule())) {
        CustomerServiceTaskDTO customerServiceTaskDTO = queryCustomerResult.getModule().get(0);
        Long id = customerServiceTaskDTO.getId();//退款任务id


        //修改处理类型
        customerServiceTaskDTO.setProcessType(CustomerServiceTaskDealTypeConstants.REFUND_ALL);//暂时只支持全部退款
        Result<Boolean> booleanResult = customerService
            .updateCustomerServiceTask(customerServiceTaskDTO);
        if (null == booleanResult || !booleanResult.getSuccess()) {
          return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"更新处理类型失败"));
        }

        if (!taskComplete(id)) {
          JackSonUtil
              .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"修改退款任务状态失败" ));
        }

        return JackSonUtil.getJson(Result.getSuccDataResult(true));

      }

      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,"该退款任务不存在"));
    } else {
      if (null != result) {
        LOGGER.error("退款失败，原因：" + JackSonUtil.getJson(result));
        return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            result.getMessage()));
      } else {
        return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            "退款失败，请稍后再试！"));
      }
    }
  }

  /**
   * 赠送优惠券
   */
  public String giftCoupons(Long couponId, Long userId, Long taskId) {
    Result<Boolean> sendCouponResult =
        userCouponService.sendCouponByCouponId(userId, couponId, COUPON_DEAD_TIME);
    if (null == sendCouponResult || !sendCouponResult.getSuccess()
        || null == sendCouponResult.getModule()) {
      return JackSonUtil.getJson(Result.getErrDataResult(sendCouponResult.getResultCode(),
          sendCouponResult.getErrorMsg()));
    }

    if (null != taskId && null != getCustomerServiceTaskDTO(taskId)) {
      CustomerServiceTaskDTO customerServiceTaskDTO = getCustomerServiceTaskDTO(taskId);
      customerServiceTaskDTO.setProcessType(CustomerServiceTaskDealTypeConstants.GIVE_COUPON);
      Result<Boolean> booleanResult = customerService
          .updateCustomerServiceTask(customerServiceTaskDTO);

      if (null != booleanResult && booleanResult.getSuccess() && null != booleanResult.getModule()
          && booleanResult.getModule()) {
        return JackSonUtil
            .getJson(Result.getSuccDataResult(booleanResult.getModule()));

      }
      return JackSonUtil
          .getJson(Result
              .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "赠送优惠券失败"));

    }

    return JackSonUtil.getJson(Result.getSuccDataResult(sendCouponResult.getModule()));
  }


  public CustomerServiceTaskDTO getCustomerServiceTaskDTO(Long taskId) {
    if (null == taskId) {
      return null;
    }

    Result<CustomerServiceTaskDTO> dtoResult = customerService
        .queryCustomerServiceTaskById(taskId);
    if (null != dtoResult && dtoResult.getSuccess() && null != dtoResult.getModule()) {
      return dtoResult.getModule();
    }

    return null;
  }

  /**
   * 获取优惠券
   */
  public List<CustomCouponVO> gainCoupons() {
    Collection<Short> couponType = Lists.newArrayList();
    couponType.add((short) 1);
    Result<List<CouponDTO>> couponResult = userCouponService.getCouponByTypes(couponType);

    if (null == couponResult || null == couponResult.getModule()) {
      return null;
    }
    List<CustomCouponVO> customCouponVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(couponResult.getModule(), customCouponVOs, CustomCouponVO.class);
    return customCouponVOs;
  }

  /**
   * 任务检查
   */
  public String taskCheck(Long taskId, String orderId) {

    if (null == taskId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Result<CustomerServiceTaskDTO> customerResult =
        customerService.queryCustomerServiceTaskById(taskId);// 查询数据库是否存在该任务
    if (null == customerResult || !customerResult.getSuccess()
        || null == customerResult.getModule()) {
      return JackSonUtil.getJson(customerResult);
    }
    CustomerServiceTaskDTO customerServiceTaskDTO = customerResult.getModule();
    customerServiceTaskDTO.setStatus(CustomerServiceTaskStatusConstants.DEALING);// 将任务状态修改为处理中
    Result<Boolean> updateResult =
        customerService.updateCustomerServiceTask(customerServiceTaskDTO);
    LOGGER.info(JackSonUtil.getJson(updateResult));// 打印是否更新成功

    if (null == orderId || !StringUtils.isNumeric(orderId)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "订单编号不存在"));
    }

    Short status = 9;
    com.xianzaishi.trade.client.Result<OrderVO> orderResult =
        orderService.getOrder(Long.valueOf(orderId));
    if (null == orderResult || orderResult.getCode() < 0 || null == orderResult.getModel()) {
      return JackSonUtil.getJson(Result.getErrDataResult(orderResult.getCode(),
          orderResult.getMessage()));
    }
    OrderVO orderVO = orderResult.getModel();
    if (null != orderVO.getStatus() && orderVO.getStatus() == 10) {// 如果订单状态为退款成功或者交易关闭，那么不能再次退款
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "该订单已退款，请勿重复退款"));
    }
    if (null != orderVO.getStatus() && orderVO.getStatus() == 11) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
          "该订单交易已关闭，不支持退款"));
    }
    com.xianzaishi.trade.client.Result<Boolean> updateOrderResult =
        orderService.updateOrderStatus(Long.valueOf(orderId), status);// 修改订单状态为退款中
    LOGGER.info(JackSonUtil.getJson(updateOrderResult));
    if (null == updateOrderResult || !updateOrderResult.getModel()) {
      return JackSonUtil.getJson(Result.getErrDataResult(updateOrderResult.getCode(),
          "订单状态未修改成功,原因：" + updateOrderResult.getMessage()));
    }
    Double effAmount = 0.0;
    if (null != orderVO.getEffeAmount()) {
      effAmount = Double.valueOf(orderVO.getEffeAmount());
    }
    Integer credit = orderVO.getCredit();// 积分
    Long couponId = orderVO.getCouponId();// 优惠券id
    Double couponAmount = 0.0;
    if (null != couponId) {
      Result<UserCouponDTO> couponResult = userCouponService.getUserCouponById(couponId);
      if (null != couponResult && couponResult.getSuccess() && null != couponResult.getModule()) {
        UserCouponDTO userCouponDTO = couponResult.getModule();
        couponAmount = userCouponDTO.getAmount();// 优惠金额
      }
    }
    Double integralArrival = 0.0;
    if (null != credit) {
      integralArrival = credit.doubleValue() / 100;
    }
    Double totalPrice = effAmount + integralArrival + couponAmount;
    Double freight = 0.0;
    Short channelType = orderVO.getChannelType();
    if (totalPrice < 59 && (null == channelType || channelType == 1)) {// 如果订单总价小于59并且是线上购买那么运费为6元
      freight = 6.0;
    }

    StringBuffer sb = new StringBuffer();
    sb.append("运费：");
    sb.append(String.valueOf(freight));
    sb.append("，");
    sb.append("优惠券信息：");
    sb.append(String.valueOf(keep2Decimal(couponAmount)));
    sb.append("，");
    sb.append("积分抵现：");
    sb.append(String.valueOf(keep2Decimal(integralArrival)));
    sb.append("，");
    sb.append("应收：");
    sb.append(String.valueOf(keep2Decimal(totalPrice)));
    sb.append("，");
    sb.append("实收：");
    sb.append(String.valueOf(keep2Decimal(effAmount)));
    sb.append("，");
    sb.append("应退金额：");
    sb.append(String.valueOf(keep2Decimal(effAmount)));
    return JackSonUtil.getJson(Result.getSuccDataResult(sb.toString()));
  }

  private Double keep2Decimal(Double src) {
    BigDecimal bigDecimal = new BigDecimal(src);
    Double tar = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    return tar;
  }


  /**
   * 将退款任务状态修改为审核状态，并添加要要退款的金额
   */
  public String commitAudit(QueryDataVO customQueryVO) {
    LOGGER.error("请求参数=" + JackSonUtil.getJson(customQueryVO));
    if (null == customQueryVO || null == customQueryVO.getId() || null == customQueryVO.getSum()) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求参数不正确"));
    }

    if (null == customQueryVO.getSum() || !StringUtils
        .isNumeric(customQueryVO.getSum().trim().replace(".", "1"))) {
      return JackSonUtil.getJson(
          Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "输入的退款金额不全是数字"));
    }

    Long id = customQueryVO.getId();//taskId
    String sum = customQueryVO.getSum();//退款金额
    String refoudReason = customQueryVO.getRefoudReason();//退款原因
    Long couponId = customQueryVO.getCouponId();//赠送的优惠券
    CustomerServiceTaskDTO customerServiceTaskDTO = new CustomerServiceTaskDTO();
    customerServiceTaskDTO.setId(id);
    Result<CustomerServiceTaskDTO> taskDTOResult = customerService
        .queryCustomerServiceTaskById(id);
    if (null == taskDTOResult || !taskDTOResult.getSuccess() || null == taskDTOResult.getModule()) {
      LOGGER.error("查询退款id" + id + "时失败");
      return "提交审核失败";
    }

    LOGGER.error("查询结果="+JackSonUtil.getJson(taskDTOResult));

    CustomerServiceTaskDTO serviceTaskDTO = taskDTOResult.getModule();
    serviceTaskDTO.setStatus(CustomerServiceTaskStatusConstants.AUDIT);//表示审核状态

    BigDecimal bigDecimal = new BigDecimal(sum.trim());
    BigDecimal refoud = bigDecimal.multiply(new BigDecimal("100"));
    long value = refoud.longValue();
    serviceTaskDTO.setRefoudAmount(value);//退款金额 单位 分
    serviceTaskDTO.setRefoudReason(refoudReason);//退款原因
    serviceTaskDTO.setCouponId(couponId);//优惠券id
    serviceTaskDTO.setProcessType(CustomerServiceTaskDealTypeConstants.COMMIT_AUDIT);//处理类型  审核

    LOGGER.error("更新数据vo=" + JackSonUtil.getJson(serviceTaskDTO));

    Result<Boolean> result = customerService
        .updateCustomerServiceTask(serviceTaskDTO);

    if (null != result && result.getSuccess() && result.getModule() ) {
      if (null != couponId) {
        //赠送优惠券
        giftCoupons(couponId,serviceTaskDTO.getCustomerUser(),id);
      }
      return JackSonUtil.getJson(Result.getSuccDataResult(result.getModule()));
    }

    return JackSonUtil.getJson(Result
        .getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "提交审核失败！"));
  }


  /**
   * 获取角色
   */
  public List<String> getRoleList(BackGroundUserDTO userDTO) {

    List<String> list = new ArrayList<>();

    if (null == userDTO || StringUtils.isEmpty(userDTO.getRole())) {
      return list;
    }

    String role = userDTO.getRole();
    String[] split = role.trim().split(";");

    for (String roleId : split) {
      if (null == roleId) {
        continue;
      }
      Result<RoleDTO> result = roleService.queryRoleById(Integer.parseInt(roleId));
      if (null != result && result.getSuccess() && null != result.getModule()) {
        RoleDTO roleDTO = result.getModule();
        String roleName = roleDTO.getRoleName();
        if (null != roleName) {
          list.add(roleName);
        }
      }
    }

    return list;

  }

  /**
   * 获取登录者类型
   */
  public String roleList(EmployeeVO employeeVO) {
    if (null == employeeVO || null == employeeVO.getOpertatorId()
        || employeeVO.getOpertatorId() < 0) {
      return JackSonUtil
          .getJson(
              Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "员工id不正确！"));
    }

    Result<BackGroundUserDTO> userDTOResult = backGroundUserService
        .queryUserDTOById(employeeVO.getOpertatorId());

    if (null == userDTOResult || !userDTOResult.getSuccess() || null == userDTOResult.getModule()) {
      return JackSonUtil
          .getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该员工不存在"));
    }

    String role = userDTOResult.getModule().getRole();
    if (StringUtils.isEmpty(role)) {
      return JackSonUtil
          .getJson(
              Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该员工不存在任何角色"));
    }

    return JackSonUtil.getJson(Result.getSuccDataResult(role));
  }


}
