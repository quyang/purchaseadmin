package com.xianzaishi.purchaseadmin.component.presenter;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.purchase.vo.PurchaseInfoVo;
import com.xianzaishi.purchaseadmin.client.purchase.vo.PurchaseSubOrderVO;
import com.xianzaishi.purchaseadmin.client.user.vo.PutPurchaseadminVo;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseDeliveryStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component("presenterComponent")
public class PresenterComponent {
	
	 @Autowired
	 private PurchaseService purchaseService;
	 
	 @Autowired
	 private SkuService skuServer;

	private static final Logger logger = Logger.getLogger(PresenterComponent.class);

	/**
	 * 根据ID 获取采购单详情
	 *
	 * @return 采购单详细列表
	 */
	public String queryPresenterById(Integer id) {
		if (StringUtils.isEmpty(id)) {
			return JackSonUtil
					.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "采购单id为空"));
		}

		PurchaseQuery purchaseQuery = new PurchaseQuery();
		purchaseQuery.setParentPurchaseId(id);
		PagedResult<List<PurchaseOrderDTO>> listPagedResult = purchaseService
				.queryPurchase(purchaseQuery);

		if (null == listPagedResult || !listPagedResult.getSuccess() || CollectionUtils
				.isEmpty(listPagedResult.getModule())) {
			return JackSonUtil
					.getJson(
							Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询的采购单信息不存在"));
		}

		//判断之前是否入库过
		if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL
				.equals(listPagedResult.getModule().get(0).getDeliveryStatus())
				|| PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART
				.equals(listPagedResult.getModule().get(0).getDeliveryStatus())) {

			return JackSonUtil
					.getJson(Result
							.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "采购入库失败，该采购单已经采购入库过"));
		}

		//拿到子采购单
		Result<List<PurchaseSubOrderDTO>> querySubPurchaseList = purchaseService
				.querySubPurchaseListByPurId(id);

		if (querySubPurchaseList.getSuccess() && CollectionUtils
				.isNotEmpty(querySubPurchaseList.getModule())) {

			List<PurchaseSubOrderDTO> module = querySubPurchaseList.getModule();
			List<PurchaseSubOrderVO> list = new ArrayList<>();
			for (PurchaseSubOrderDTO purchaseSubOrderDTO : module) {
				PurchaseSubOrderVO purchaseSubOrderVO = new PurchaseSubOrderVO();
				BeanCopierUtils.copyProperties(purchaseSubOrderDTO, purchaseSubOrderVO);

				//获取plu
				SkuRelationQuery skuRelationQuery = SkuRelationQuery
						.getQueryByPskuIds(Arrays.asList(purchaseSubOrderVO.getSkuId()),
								RelationTypeConstants.RELATION_TYPE_DISCOUNT);
				Result<List<ItemSkuRelationDTO>> querySkuRelationList = skuServer
						.querySkuRelationList(skuRelationQuery);

				if (null != querySkuRelationList && querySkuRelationList.getSuccess()
						&& null != querySkuRelationList.getModule() && CollectionUtils
						.isNotEmpty(querySkuRelationList.getModule())) {
					purchaseSubOrderVO.setCsku_id(querySkuRelationList.getModule().get(0).getCskuId());
				}

				list.add(purchaseSubOrderVO);
			}

//			logger.error("集合靠背后="+JackSonUtil.getJson(list));

			return JackSonUtil
					.getJson(Result.getSuccDataResult(list));

		}

		return JackSonUtil
				.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询采购单信息失败"));

	}


	/**
	 * 查询商品详情
	 *
	 * @param skuId 采购单id
	 */
	public PurchaseInfoVo searchPurchaseInfoBySubId(Long skuId) {
		if (skuId <= 0) {
			return null;
		}
		PurchaseInfoVo purchaseVo = new PurchaseInfoVo();
		System.out.println("拿到ID" + skuId);
		SkuQuery querySkuById = SkuQuery.querySkuById(skuId);
		//得到商品详情
		Result<List<ItemProductSkuDTO>> queryProductSku = skuServer.queryProductSku(querySkuById);
		if (queryProductSku.getSuccess()) {
			//成功
			List<ItemProductSkuDTO> module = queryProductSku.getModule();
			System.out.println("module" + module);
			System.out.println("module:" + module.size());
			if (module != null && module.size() > 0) {
				ItemProductSkuDTO itemProductSkuDTO = module.get(0);
				//拿到SKU信息设置商品69
				purchaseVo.setSku_69code(
						itemProductSkuDTO.getSku69code() == null ? 0 : itemProductSkuDTO.getSku69code());
				//总库存
				purchaseVo.setInventory(
						itemProductSkuDTO.getInventory() == null ? 0 : itemProductSkuDTO.getInventory());
			}
			;
		}

		//获取规格单位
		ArrayList<Long> newArrayList = Lists.newArrayList();
		newArrayList.add(skuId);
		SkuRelationQuery skuRelationQuery = SkuRelationQuery.getQueryByPskuIds(newArrayList, (short) 0);
		Result<List<ItemSkuRelationDTO>> querySkuRelationList = skuServer
				.querySkuRelationList(skuRelationQuery);
		if (querySkuRelationList.getSuccess()) {
			//获取到每箱数量
			int i = 0;
			List<ItemSkuRelationDTO> module = querySkuRelationList.getModule();
			if (module != null && module.size() > 0) {
				Integer pcProportion = module.get(0).getPcProportion();
				if (pcProportion != null && pcProportion > 0) {
					//除以1000
					i = pcProportion / 1000;
				}
			}
			//设置规格参数
			purchaseVo.setSpecification(i);
		}
		//设置库位
		purchaseVo.setStock("QC123");

		return purchaseVo;
	}


	public boolean upDatePurchaseInfo(PutPurchaseadminVo putPurBean) {
		//
		
		
		return false;
	}

	
}
