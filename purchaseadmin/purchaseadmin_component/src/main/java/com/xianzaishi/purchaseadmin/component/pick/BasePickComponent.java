package com.xianzaishi.purchaseadmin.component.pick;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.purchaseadmin.client.pick.vo.PickTokenVO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;

public class BasePickComponent {
  
  @Autowired
  private BackGroundUserService backgrounduserservice;
  
  protected PickTokenVO getTokenInfo(BackGroundUserDTO backGroundUserDTO) {
   
    if (backGroundUserDTO == null) {
      throw new BizException("no user got");
    }
    PickTokenVO tokenVO = new PickTokenVO();
    tokenVO.setAgencyID(1l);
    tokenVO.setGovID(1l);
    tokenVO.setOperator(new Long(backGroundUserDTO.getUserId()));
    return tokenVO;
  }

}
