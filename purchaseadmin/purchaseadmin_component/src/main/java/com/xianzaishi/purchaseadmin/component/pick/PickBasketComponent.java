package com.xianzaishi.purchaseadmin.component.pick;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.pick.vo.PickTokenVO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickDomainClient;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingBasketDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickingBasketVO;

@Component("pickBasketComponent")
public class PickBasketComponent extends BasePickComponent{

  private static final Logger logger = Logger.getLogger(PickBasketComponent.class);

  @Autowired
  private IPickingBasketDomainClient pickingBasketDomainClient;

  @Autowired
  private IPickDomainClient pickDomainClient;

  /**
   * 获取捡货框
   * 
   * @param user
   * @param barcode
   * @return
   */
  public Result<PickingBasketVO> getPickingBasketByBarcode(BackGroundUserDTO user, String barcode) {
    if (null == user || StringUtils.isEmpty(barcode)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }

    try {
      SimpleResultVO<PickingBasketVO> pickBasketResult =
          pickingBasketDomainClient.getPickingBasketVOByBarcode(barcode);
      if (null == pickBasketResult || !pickBasketResult.isSuccess()) {
        String errorMsg = pickBasketResult == null ? "网络连接失败，请稍后再试" : pickBasketResult.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      return Result.getSuccDataResult(pickBasketResult.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }

  }

  /**
   * 回收拣货框
   * 
   * @param user
   * @param barcode
   */
  public Result<Boolean> recievePickingBasket(BackGroundUserDTO user, String barcode) {
    if (null == user || StringUtils.isEmpty(barcode)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    try {
      PickTokenVO tokenVO = getTokenInfo(user);

      SimpleResultVO<Boolean> recieveResult = pickDomainClient.recievePickingBasket(barcode);
      if (null == recieveResult || !recieveResult.isSuccess()) {
        String errorMsg = recieveResult == null ? "网络连接失败，请稍后再试" : recieveResult.getMessage();
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
      }
      return Result.getSuccDataResult(recieveResult.getTarget());
    } catch (BizException e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
  }

}
