package com.xianzaishi.purchaseadmin.component.user;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.customercenter.client.sessionexpireservice.SessionExpireService;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.common.barcode.BarcodeInfoVO;
import com.xianzaishi.itemcenter.common.barcode.BarcodeInfoVO.BarcodeTypeConstants;
import com.xianzaishi.itemcenter.common.barcode.BarcodeParsingUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.supplier.vo.SupplierInfoQueryVO;
import com.xianzaishi.purchaseadmin.client.supplier.vo.SupplierInfoVO;
import com.xianzaishi.purchaseadmin.client.user.vo.UserVO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierCompanyDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierContactDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.purchasecenter.client.user.query.BackGroundUserQuery;
import com.xianzaishi.purchasecenter.client.user.query.SupplierQuery;

@Component("supplierComponent")
public class SupplierComponent {
  
  private static final Logger logger = Logger.getLogger(SupplierComponent.class);

  @Autowired
  private SessionExpireService sessionExpireService;

  @Autowired
  private SupplierService supplierservice;

  @Autowired
  private BackGroundUserService backgrounduserservice;

  @Autowired
  private UserComponent userComponent;

  @Autowired
  private SkuService skuService;

  private static final String MAP_COUNT_KEY = "count";
  private static final String MAP_SUPPLIERS_KEY = "suppliers";

  public Result<Boolean> checkSessionExpire(HttpServletRequest request) {
    return null;
  }

  public PagedResult<List<SupplierInfoVO>> querySupplierInfo(SupplierInfoQueryVO supplierInfoQueryVO) {
    if (null == supplierInfoQueryVO) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    Integer pageSize = supplierInfoQueryVO.getPageSize();
    Integer pageNum = supplierInfoQueryVO.getPageNum();
    Short userType = supplierInfoQueryVO.getUserType();
    String query = supplierInfoQueryVO.getQuery();
    String skuCode = supplierInfoQueryVO.getSkuCode();
    if (StringUtils.isNotBlank(query) && StringUtils.isNumeric(query)) {
      return querySupplierInfoByPhone(userType, Long.valueOf(query), pageSize, pageNum);
    }
    if (StringUtils.isNotBlank(skuCode) && StringUtils.isNumeric(skuCode)) {
      return querySupplierInfoBySkuCode(skuCode, pageSize, pageNum);
    }
    return querySupplierInfoByName(userType, query, pageSize, pageNum);
  }

  private PagedResult<List<SupplierInfoVO>> querySupplierInfoBySkuCode(String skuCode, Integer pageSize, Integer pageNum) {
    if(StringUtils.isBlank(skuCode) || !StringUtils.isNumeric(skuCode)){
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    BarcodeInfoVO barcodeInfoVO = BarcodeParsingUtil.parse(skuCode);
    short type = barcodeInfoVO.getType();
    Long skuId = 0L;
    if(type == BarcodeTypeConstants.SIXNINE_BARCODE){
      Result<List<ItemCommoditySkuDTO>> itemCommodityResult = skuService.queryItemSkuList(SkuQuery.querySkuBySku69Code(Long.valueOf(skuCode)));
      if(null == itemCommodityResult || !itemCommodityResult.getSuccess() || CollectionUtils.isEmpty(itemCommodityResult.getModule())){
        logger.error("没有找到与69码：" + skuCode + "相关的供应商");
        return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
      }
      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommodityResult.getModule();
      skuId = itemCommoditySkuDTOs.get(0).getSkuId();
    }else {
      skuId = Long.valueOf(skuCode);
    }
    
    Result<List<ItemSkuRelationDTO>> itemSkuRelationResult = skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(Arrays.asList(skuId), RelationTypeConstants.RELATION_TYPE_DISCOUNT));
    if(null == itemSkuRelationResult || !itemSkuRelationResult.getSuccess() || CollectionUtils.isEmpty(itemSkuRelationResult.getModule())){
      logger.error("没有找到与店内码：" + skuId + "相关的供应商");
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    ItemSkuRelationDTO itemSkuRelationDTO = itemSkuRelationResult.getModule().get(0);
    Long productSkuId = itemSkuRelationDTO.getPskuId();
    Result<List<ItemProductSkuDTO>> itemProductResult = skuService.queryProductSku(SkuQuery.querySkuById(productSkuId));
    if(null == itemProductResult || !itemProductResult.getSuccess() || CollectionUtils.isEmpty(itemProductResult.getModule())){
      logger.error("服务内部错误或网络连接失败（cskuId,pskuId）：[" + skuId + "," + productSkuId + "]");
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "网络连接失败，请稍后再试");
    }
    ItemProductSkuDTO itemProductSkuDTO = itemProductResult.getModule().get(0);
    Integer supplierId = itemProductSkuDTO.getSupplierId();
    Result<SupplierDTO> supplierResult = supplierservice.querySupplierById(supplierId, true);
    if(null == supplierResult || !supplierResult.getSuccess() || null == supplierResult.getModule()){
      logger.error("没有找到与查询条件：" + skuCode + "相关的供应商");
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    SupplierDTO supplierDTO = supplierResult.getModule();
    SupplierInfoVO supplierInfoVO = new SupplierInfoVO();
    supplierInfoVO.setUserId(supplierDTO.getUserId());
    supplierInfoVO.setPhone(supplierDTO.getPhone());
    supplierInfoVO.setName(supplierDTO.getName());
    Short supplierType = supplierDTO.getSupplierCompanyDTO().getSupplierType();
    supplierInfoVO.setUserType(supplierType);
    supplierInfoVO.setUserTypeString(getUserTypeString(supplierType));
    supplierInfoVO.setContract(supplierDTO.getSupplierContactDTO().getContactName());
    List<SupplierInfoVO> supplierInfoVOs = Lists.newArrayList();
    supplierInfoVOs.add(supplierInfoVO);
    return PagedResult.getSuccDataResult(supplierInfoVOs, 1, 1, pageSize, pageNum);
  }

  /**
   * 根据供应商名称或者联系人查询供应商信息
   * 
   * @param userType
   * @param query
   * @param pageSize
   * @param pageNum
   * @return
   */
  private PagedResult<List<SupplierInfoVO>> querySupplierInfoByName(Short userType, String query,
      Integer pageSize, Integer pageNum) {
    // 设置查询条件
    BackGroundUserQuery backGroundUserQuery = new BackGroundUserQuery();
    if (StringUtils.isNotEmpty(query)) {
      backGroundUserQuery.setName(query);
    }
    backGroundUserQuery.setUserType((short) 1);
    // TODO:优化
    backGroundUserQuery.setPageSize(1000);
    backGroundUserQuery.setPageNum(pageNum);
    // 查询供应商信息
    Map<String, Object> supplierInfoMap = querySupplierInfoByQuery(backGroundUserQuery, userType);
    if (null == supplierInfoMap) {
      supplierInfoMap = Maps.newHashMap();
    }
    List<SupplierInfoVO> supplierInfoVOs =
        (List<SupplierInfoVO>) supplierInfoMap.get(MAP_SUPPLIERS_KEY);
    Integer totalCount = (Integer) supplierInfoMap.get(MAP_COUNT_KEY);
    // 如果按照公司名查不到数据，则先查出前200条供应商数据，然后再按照联系人来过滤
    if (CollectionUtils.isEmpty(supplierInfoVOs)) {
      // TODO:优化
      backGroundUserQuery.setName(null);
      backGroundUserQuery.setPageSize(1000);
      backGroundUserQuery.setPageNum(0);
      supplierInfoMap = querySupplierInfoByQuery(backGroundUserQuery, userType);
      if (null == supplierInfoMap) {
        supplierInfoMap = Maps.newHashMap();
      }
      supplierInfoVOs = (List<SupplierInfoVO>) supplierInfoMap.get(MAP_SUPPLIERS_KEY);
      List<SupplierInfoVO> supplierInfoContractVOs = Lists.newArrayList();
      for (SupplierInfoVO supplierInfoVO : supplierInfoVOs) {
        if (StringUtils.isNotBlank(query) && query.equals(supplierInfoVO.getContract())) {
          supplierInfoContractVOs.add(supplierInfoVO);
        }
      }
      totalCount = 1;
      supplierInfoVOs = supplierInfoContractVOs;
    }
    if (CollectionUtils.isEmpty(supplierInfoVOs)) {
        return null;
    }
    return PagedResult.getSuccDataResult(supplierInfoVOs, totalCount,
        getTotalPage(totalCount, pageSize), pageSize, pageNum);
  }

  /**
   * 根据手机号或座机查询供应商信息
   * 
   * @param userType
   * @param phone
   * @param pageSize
   * @param pageNum
   * @return
   */
  private PagedResult<List<SupplierInfoVO>> querySupplierInfoByPhone(Short userType, Long phone,
      Integer pageSize, Integer pageNum) {
    // 设置查询条件
    BackGroundUserQuery query = new BackGroundUserQuery();
    if (null != phone) {
      query.setPhone(phone);
    }
    query.setUserType((short) 1);
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);

    Map<String, Object> supplierInfoMaps = querySupplierInfoByQuery(query, userType);
    if (MapUtils.isEmpty(supplierInfoMaps)) {
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    List<SupplierInfoVO> supplierInfoVOs =
        (List<SupplierInfoVO>) supplierInfoMaps.get(MAP_SUPPLIERS_KEY);
    return PagedResult.getSuccDataResult(supplierInfoVOs, 1, 1, pageSize, pageNum);
  }

  /**
   * 根据查询条件查询供应商信息
   * 
   * @param backGroundUserQuery
   * @param userType
   * @return
   */
  private Map<String, Object> querySupplierInfoByQuery(BackGroundUserQuery backGroundUserQuery,
      Short userType) {
    if (null == backGroundUserQuery) {
      return null;
    }
    // 查询user表中的用户信息，供应商和员工信息均存于user表中
    PagedResult<List<BackGroundUserDTO>> backGroundUserResult =
        backgrounduserservice.queryUserDTOByQuery(backGroundUserQuery);
    if (null == backGroundUserResult || !backGroundUserResult.getSuccess()
        || CollectionUtils.isEmpty(backGroundUserResult.getModule())) {
      return null;
    }
    List<BackGroundUserDTO> backGroundUserDTOs = backGroundUserResult.getModule();
    List<Integer> supplierIds = Lists.newArrayList();
    for (BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs) {
      supplierIds.add(backGroundUserDTO.getUserId());
    }
    SupplierQuery supplierQuery = new SupplierQuery();
    supplierQuery.setSupplierIds(supplierIds);
    supplierQuery.setIncludeContactInfo(true);
    supplierQuery.setIncludeCompanyInfo(true);
    supplierQuery.setSupplierType(userType);
    Result<List<SupplierDTO>> supplierResult = supplierservice.querySupplier(supplierQuery);
    Map<Integer, String> contractMap = Maps.newHashMap();
    Map<Integer, Short> supplierTypeMap = Maps.newHashMap();
    if (null != supplierResult && supplierResult.getSuccess()
        && CollectionUtils.isNotEmpty(supplierResult.getModule())) {
      List<SupplierDTO> supplierDTOs = supplierResult.getModule();
      for (SupplierDTO supplierDTO : supplierDTOs) {
        SupplierContactDTO supplierContactDTO = supplierDTO.getSupplierContactDTO();
        contractMap.put(supplierDTO.getUserId(), supplierContactDTO.getContactName());
        SupplierCompanyDTO supplierCompanyDTO = supplierDTO.getSupplierCompanyDTO();
        supplierTypeMap.put(supplierDTO.getUserId(), supplierCompanyDTO.getSupplierType());
      }
    }
    List<SupplierInfoVO> supplierInfoVOs = Lists.newArrayList();
    for (BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs) {
      if (null != userType && !userType.equals(supplierTypeMap.get(backGroundUserDTO.getUserId()))) {
        continue;
      }
      SupplierInfoVO supplierInfoVO = new SupplierInfoVO();
      supplierInfoVO.setUserId(backGroundUserDTO.getUserId());
      supplierInfoVO.setPhone(backGroundUserDTO.getPhone());
      supplierInfoVO.setName(backGroundUserDTO.getName());
      Short supplierType = supplierTypeMap.get(backGroundUserDTO.getUserId());
      supplierInfoVO.setUserType(supplierType);
      supplierInfoVO.setUserTypeString(getUserTypeString(supplierType));
      supplierInfoVO.setContract(contractMap.get(backGroundUserDTO.getUserId()));
      supplierInfoVOs.add(supplierInfoVO);
    }
    Result<Integer> countResult = supplierservice.querySupplierCount(supplierQuery);
    Integer supplierCount = 0;
    if (null != countResult && countResult.getSuccess() && null != countResult.getModule()) {
      supplierCount = countResult.getModule();
    }
    Map<String, Object> resultMap = Maps.newHashMap();
    resultMap.put(MAP_SUPPLIERS_KEY, supplierInfoVOs);
    resultMap.put(MAP_COUNT_KEY, supplierCount);
    return resultMap;
  }

  /**
   * 解析供应商类型
   * 
   * @param supplierType
   * @return
   */
  private String getUserTypeString(Short supplierType) {
    if (null == supplierType) {
      return null;
    }
    String type = null;
    switch (supplierType) {
      case 1:
        type = "原材料供应商";
        break;
      case 2:
        type = "包装供应商";
        break;
      case 3:
        type = "低值易耗供应商";
        break;
      case 4:
        type = "固定资产供应商";
        break;
      case 5:
        type = "服务产品供应商";
        break;
      case 6:
        type = "其他供应商";
        break;
    }
    return type;
  }

  /**
   * 获取分页数量
   * 
   * @param number 商品数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  public Result<Map<String, String>> login(UserVO userVO) {

    return userComponent.login(userVO);
  }

}
