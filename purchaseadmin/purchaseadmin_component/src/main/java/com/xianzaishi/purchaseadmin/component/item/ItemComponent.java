package com.xianzaishi.purchaseadmin.component.item;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.xianzaishi.couponcenter.client.coupon.CouponService;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO.CouponCondition;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO.CouponSendRulesCondition;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO.CouponStatus;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO.CouponType;
import com.xianzaishi.couponcenter.client.coupon.query.CouponsQuery;
import com.xianzaishi.dumpcenter.client.itemsalecount.ItemSaleCountService;
import com.xianzaishi.dumpcenter.client.itemsalecount.dto.ItemSaleCountDTO;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.ProductService;
import com.xianzaishi.itemcenter.client.item.dto.IntroductDataDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemProductDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemProductDTO.ItemProductStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemSpecificationDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuSaleUnitConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationScheduleDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO.SkuTagDefinitionConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationScheduleQuery;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.barcode.BarcodeInfoVO;
import com.xianzaishi.itemcenter.common.barcode.BarcodeParsingUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.base.vo.CollectionVO;
import com.xianzaishi.purchaseadmin.client.base.vo.excel.ExcelItem;
import com.xianzaishi.purchaseadmin.client.item.vo.BaseItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.BoomCommodityItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.CommodityItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.IntorductionDataVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemDisplaySpecificationVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemListVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemTagTypeVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ItemTagsDetaileVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ProductItemVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ProductListQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.ProductListVO;
import com.xianzaishi.purchaseadmin.client.item.vo.PurchaseItemQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationQueryVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationScheduleVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SkuRelationVO;
import com.xianzaishi.purchaseadmin.client.item.vo.SubPurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.CouponQueryVO;
import com.xianzaishi.purchaseadmin.client.property.vo.PropertyVO;
import com.xianzaishi.purchaseadmin.client.property.vo.UnitVO;
import com.xianzaishi.purchaseadmin.client.property.vo.ValueVO;
import com.xianzaishi.purchaseadmin.component.category.CategoryComponent;
import com.xianzaishi.purchaseadmin.component.marketactivity.MarketActivityComponent;
import com.xianzaishi.purchaseadmin.component.pic.PicComponent;
import com.xianzaishi.purchaseadmin.component.user.UserComponent;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.purchasecenter.client.user.query.SupplierQuery;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

/**
 * @author zhancang
 */
@Component("itemComponent")
public class ItemComponent {

  @Autowired
  private ProductService productService;

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private SkuService skuService;

  @Autowired
  private CategoryComponent categoryComponent;

  @Autowired
  private UserComponent userComponent;

  @Autowired
  private PicComponent picComponent;

  @Autowired
  private IInventory2CDomainClient inventory2CDomainClient;

  @Autowired
  private StdCategoryService stdCategoryService;

  @Autowired
  private SystemPropertyService systemPropertyService;

  @Autowired
  private CouponService couponService;

  @Autowired
  private SupplierService supplierService;

  @Autowired
  private ItemSaleCountService itemSaleCountService;

  @Autowired
  private MarketActivityComponent marketActivityComponent;

  private static final Logger logger = Logger.getLogger(ItemComponent.class);

  protected static final String SPLIT_PROPERTY_GROUT_KEY = "\n";
  protected static final String SPLIT_PROPERTY_KEY = "\\";
  protected static final String SPLIT_KVPROPERTY_KEY = "&";
  protected static final String SPLIT_COMMA_KEY = ",";
  private static final String NULL_STRING = "null";
  private static final Integer TAG_TYPE_CONFIG_ID = 12;
  private static final String[] CHANNEL_STRINGS = {"全场标", "线上标", "线下标"};
  private static final int RELATION_TYPE_BUY = -1;
  private static final int RELATION_TYPE_SALE = 1;
  /**
   * 临期商品条码标志
   */
  private static final String NearShelfLifeSkuBarFlag = "027001";

  public void addAllProduct(boolean isOnline, boolean isTest, boolean isUpdatePic) {
    if (isUpdatePic) {
      addAllProductPic(isOnline, isTest);
      return;
    }
    String basePath = "D:/work/input/";

    if (!isOnline) {
      basePath = "D:/work/input/";
    } else {
      basePath = "/usr/works/datainit/";
    }

    List<ExcelItem> itemList = null;
    try {
      itemList = this.readExcel(new File(basePath + "item.xls"));
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("Add all item,read excel failed");
      return;
    }
    String imgPath = basePath + "img" + "/";
    logger.error("Read excel count is:" + itemList.size());
    List<Map<String, Object>> itemProductDTOList =
        this.getProductList(itemList, isTest, imgPath, true);
    if (isTest) {
      logger.error("test count is:" + itemProductDTOList.size());
      return;
    }
    logger.error("Get excel count is:" + itemProductDTOList.size());
    int insertCount = 0;
    int updateCount = 0;
    if (CollectionUtils.isNotEmpty(itemProductDTOList)) {
      for (Map<String, Object> productMap : itemProductDTOList) {
        ItemProductDTO product = (ItemProductDTO) productMap.get("obj");
        if (null == product) {
          continue;
        }
        // 同样商品重新导入标志
        boolean update = (boolean) productMap.get("update");
        if (isTest) {
          logger.info("test success,title:" + product.getTitle());
        } else {
          if (!update) {
            Result<Long> result = productService.insertItemProduct(product);
            if (result != null && result.getSuccess()) {
              Long productCode = result.getModule();
              if (null != productCode && productCode > 0) {
                CommodityItemVO commodity = beforAddCommodityQueryProduct(productCode);
                if (null != commodity) {
                  Result<Long> insertResult = insertItemDTO(commodity, 5);
                  long itemId = 0;
                  if (null != insertResult && insertResult.getSuccess()) {
                    itemId = insertResult.getModule();
                  }
                  if (itemId > 0) {
                    ++insertCount;
                    logger.info("Insert commodity success,title:" + product.getTitle()
                        + "return commodityId:" + itemId);
                  } else {
                    logger.error("Insert commodity failed,title:" + product.getTitle()
                        + "return productId:" + productCode);
                  }
                } else {
                  logger.error("Insert commodity failed,query insert product failed,title:"
                      + product.getTitle() + "return productId:" + productCode);
                }
              } else {
                logger.error("Insert product failed,title:" + product.getTitle()
                    + "return productId:" + productCode);
              }
            } else {
              logger.error("Insert product failed,title:" + product.getTitle());
            }
          } else {
            Result<Boolean> result = productService.updateItemProduct(product);
            if (result != null && result.getSuccess()) {
              Long productCode = product.getItemProductSkuList().get(0).getSkuCode();
              CommodityItemVO commodity = beforAddCommodityQueryProduct(productCode);
              if (null != commodity) {
                ItemCommoditySkuDTO dbcommodity = null;
                dbcommodity = getItemCommoditySkuDTOBySku69Code(commodity.getSku69Id());
                if (null == dbcommodity) {
                  dbcommodity = getItemCommoditySkuDTOByTitle(commodity.getTitle());
                }
                if (null != dbcommodity) {// db中对应的销售商品已经存在
                  commodity.setId(dbcommodity.getItemId());
                  commodity.setSkuId(dbcommodity.getSkuId());
                  Result<Boolean> updateResultDTO = this.updateCommodityItem(commodity, 5);
                  boolean updateResult = false;
                  if (null != updateResultDTO) {
                    updateResult = updateResultDTO.getModule();
                  }
                  if (!updateResult) {
                    logger.error("update commodity failed,title:" + commodity.getTitle());
                  } else {
                    ++updateCount;
                    logger.info("update commodity success,title:" + commodity.getTitle());
                  }
                } else {
                  Result<Long> insertResult = insertItemDTO(commodity, 5);
                  long itemId = 0;
                  if (null != insertResult && insertResult.getSuccess()) {
                    itemId = insertResult.getModule();
                  }
                  if (itemId > 0) {
                    logger.info("Insert commodity success,title:" + product.getTitle()
                        + "return commodityId:" + itemId);
                  } else {
                    logger.error("Insert commodity failed,title:" + product.getTitle()
                        + "return productId:" + productCode);
                  }
                }
              } else {
                logger.error("update commodity,query commondity by pruduct failed,title:"
                    + product.getTitle());
              }
            } else {
              logger.error("update product failed,title:" + product.getTitle());
            }
          }
        }
      }
    }
    logger.error("Inset count is:" + insertCount + ",update count is:" + updateCount);
  }

  public void addAllProductPic(boolean isOnline, boolean isTest) {
    String basePath = "D:/work/input2/";

    if (!isOnline) {
      basePath = "D:/work/input2/";
    } else {
      basePath = "/usr/works/datainit2/";
    }

    List<ExcelItem> itemList = null;
    try {
      itemList = this.readExcel(new File(basePath + "item.xls"));
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("Add all item,read excel failed");
      return;
    }
    String imgPath = basePath + "img" + "/";
    logger.error("Read excel count is:" + itemList.size());
    List<Map<String, Object>> itemProductDTOList = this.getProductPicList(itemList, imgPath, true);
    logger.error("test excel count is:" + itemProductDTOList.size());
    int insertCount = 0;
    int updateCount = 0;
    if (CollectionUtils.isNotEmpty(itemProductDTOList)) {
      for (Map<String, Object> productMap : itemProductDTOList) {
        ItemProductDTO product = (ItemProductDTO) productMap.get("obj");
        if (null == product) {
          continue;
        }
        String title = product.getTitle();
        ItemListQuery iq = new ItemListQuery();
        iq.setItemTitle(title);
        Result<Map<String, Object>> queryResult = commodityService.queryCommodies(iq);
        if (null != queryResult && queryResult.getSuccess() && null != queryResult.getModule()) {
          Map<String, Object> queryM = queryResult.getModule();
          List<ItemDTO> dtoResult = (List<ItemDTO>) queryM.get("itemList");
          if (CollectionUtils.isNotEmpty(dtoResult)) {
            ItemDTO updateDTO = null;
            for (ItemDTO dto : dtoResult) {
              if (dto.getTitle().equals(title)) {
                updateDTO = dto;
                updateDTO.setPicList(product.getPicList());
                updateDTO.setIntroduction(product.getIntroduction());
                break;
              }
            }
            if (null != updateDTO) {
              if (!isTest) {
                Result<Boolean> updateResult = commodityService.updateCommodityPic(updateDTO);
                if (null != updateResult && updateResult.getSuccess()) {
                  updateCount++;
                  logger.error("Update item pic success,item name:" + title);
                  continue;
                } else {
                  logger.error("Update item pic failed,item name:" + title);
                }
              } else {
                updateCount++;
                logger.error("Test Update item pic success,item name:" + title);
                continue;
              }
            } else {
              logger.error("Update item pic failed,query item is null:" + title);
            }
          }
          logger.error("Update item pic failed,query item failed:" + title);
        }
      }
    }
    logger.error("Inset count is:" + insertCount + ",update count is:" + updateCount);
  }

  public String testAllProduct(File file, boolean isOnline, boolean displayImgInfo) {

    List<ExcelItem> itemList = null;
    try {
      itemList = this.readExcel(file);
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("Add all item,read excel failed");
      return "导入失败，读取供应商文件失败，请检查文件格式";
    }
    String basePath = "D:/work/input/";

    if (!isOnline) {
      basePath = "D:/work/input/";
    } else {
      basePath = "/usr/works/datainit/";
    }
    logger.error("Read excel count is:" + itemList.size());

    String imgPath = basePath + "img" + "/";
    List<Map<String, Object>> itemProductDTOList =
        this.getProductList(itemList, true, imgPath, displayImgInfo);
    String errorInfo = "";
    String debugInfo = "";
    logger.error("Get excel count is:" + itemProductDTOList.size());
    if (CollectionUtils.isNotEmpty(itemProductDTOList)) {
      for (Map<String, Object> productMap : itemProductDTOList) {
        errorInfo = errorInfo + productMap.get("printlog").toString();
        debugInfo = debugInfo + productMap.get("printdebug").toString();
      }
    }
    String printError =
        "\t严重错误如下，必须处理--------------><br/>" + errorInfo + "<br/>\t警告信息如下，请确认--------------><br/>"
            + debugInfo;
    logger.error("Print error:" + printError);
    return printError;
  }

  /**
   * TODO 发布商品前台总的入口
   * 
   * @param itemProductDTO
   * @return
   */
  private ItemProductDTO getProductDTOByCommodityVO(CommodityItemVO itemVO, int userId) {
    ItemProductDTO item = new ItemProductDTO();
    item.setTitle(itemVO.getTitle());
    item.setSubtitle(itemVO.getSubTitle());
    item.setCategoryId(itemVO.getCatId());
    item.setStatus(ItemProductStatusConstants.PRODUCT_STATUS_PASS);
    Date now = new Date();
    item.setGmtCreate(now);
    item.setGmtModified(now);
    item.setPicList(itemVO.getPicList());
    item.setIntroduction(itemVO.getTitle());
    item.setCreator(userId);
    item.setTitletags(itemVO.getTitletags());


    ItemProductSkuDTO sku = new ItemProductSkuDTO();
    sku.setSupplierId(itemVO.getSupplierId());
    sku.setSku69code(itemVO.getSku69Id());
    sku.setPrice(itemVO.getPrice());


    return item;
  }

  /**
   * 查询一个发布好的商品对象
   * 
   * @param productId
   * @return
   */
  public ProductItemVO queryProductItem(long productId) {
    Result<ItemProductDTO> productDTOResult = productService.queryItemProduct(productId, true);
    if (null == productDTOResult || !productDTOResult.getSuccess()) {
      return null;
    }
    ProductItemVO item = getProductItemVOByProductItemDTO(productDTOResult.getModule());
    List<PropertyVO> propertyVOList = getProductPropertyInputList(productDTOResult.getModule());
    // 审核通过后的部分属性不允许修改
    if (!(item.getStatus() == ItemStatusConstants.ITEM_STATUS_CHECKED_NOTPASS
        || item.getStatus() == ItemStatusConstants.ITEM_STATUS_PRE || item.getStatus() == ItemStatusConstants.ITEM_STATUS_WAITCHECK)) {
      for (PropertyVO property : propertyVOList) {
        if (PropertyVO.PRODUCT_PROPERTY_NAME_CHANGE_SET.contains(property.getPropertyName())) {
          property.setModified(false);
        }
      }
    }
    item.setPropertyVO(propertyVOList);
    return item;
  }

  /**
   * 获取采购商品列表
   * 
   * @param query
   * @return
   */
  public CollectionVO<ProductListVO> getProductList(ProductListQueryVO query) {
    SkuQuery dbQuery = null;
    int pageNum = BaseQuery.DEFAULT_PAGE_NUM;
    int pageSize = BaseQuery.DEFAULT_PAGE_SIZE;
    if (null != query) {
      if (query.getPageNum() >= 0) {
        pageNum = query.getPageNum();
      }
      if (query.getPageSize() > 0) {
        pageSize = query.getPageSize();
      }

      int userId = 0;
      if (StringUtils.isNotEmpty(query.getSupplierName())) {
        BackGroundUserDTO user = userComponent.getUserByName(query.getSupplierName());
        if (null != user && UserTypeConstants.USER_SUPPLIER.equals(user.getUserType())) {
          userId = user.getUserId();
        }
      }

      if (userId > 0) {
        dbQuery = SkuQuery.querySkuBySupplierAndTitle(userId, query.getTitle(), pageSize, pageNum);
      } else if (StringUtils.isNotEmpty(query.getTitle())) {
        dbQuery = SkuQuery.querySkuByTitle(query.getTitle(), pageSize, pageNum);
      } else {
        dbQuery = SkuQuery.querySku(pageSize, pageNum);
      }
    } else {
      dbQuery = SkuQuery.querySku(pageSize, pageNum);
    }

    CollectionVO<ProductListVO> result = new CollectionVO<ProductListVO>();
    Result<List<ItemProductSkuDTO>> dbResult = skuService.queryProductSku(dbQuery);
    if (null == dbResult || !dbResult.getSuccess() || CollectionUtils.isEmpty(dbResult.getModule())) {
      result.setList(Lists.<ProductListVO>newArrayList());
      result.setTotalPage(0);
      return result;
    }

    Result<Integer> countResult = skuService.queryProductSkuCount(dbQuery);
    int totalPage = 0;
    List<ItemProductSkuDTO> dbProductList = dbResult.getModule();
    List<ProductListVO> resultList = getProductListVOList(dbProductList);
    result.setList(resultList);
    if (null != countResult && countResult.getSuccess()) {
      totalPage =
          (int) ((countResult.getModule() % pageSize == 0) ? (countResult.getModule() / pageSize)
              : (countResult.getModule() / pageSize) + 1);
    }
    result.setTotalPage(totalPage);
    return result;
  }

  /**
   * 打开进售价列表页面返回数据或者查询进售价页面返回数据.页面层解析出SkuRelationQueryVO
   * 
   * @param query
   * @param isDiscount 是否查询优惠数据
   * @return
   */
  public List<SkuRelationVO> getSkuRelationVOList(SkuRelationQuery query) {
    if (null == query) {
      return Collections.emptyList();
    }
    List<SkuRelationVO> result;
    Result<List<ItemSkuRelationDTO>> skuRelation = skuService.querySkuRelationList(query);
    if (null == skuRelation || !skuRelation.getSuccess()
        || CollectionUtils.isEmpty(skuRelation.getModule())) {
      return Collections.emptyList();
    } else {
      logger.error("sku query list size:" + skuRelation.getModule().size());
    }

    List<ItemSkuRelationDTO> dbRelation = skuRelation.getModule();
    result = getRelationVOList(dbRelation);
    return result;
  }

  /**
   * 过滤采购信息，一些场景允许非采购人员操作
   */
  public void cleanPurchaseInfo(List<SkuRelationVO> relationList) {
    if (CollectionUtils.isEmpty(relationList)) {
      return;
    }
    for (SkuRelationVO relation : relationList) {
      relation.setAvgCost(0);
      relation.setGrossMargin(0);
      relation.setPackageTotalCost(0);
      relation.setPackageUnitCost(0);
    }
  }

  /**
   * 过滤采购信息，一些场景允许非采购人员操作
   */
  public void cleanPurchaseScheduleInfo(List<SkuRelationScheduleVO> relationList) {
    if (CollectionUtils.isEmpty(relationList)) {
      return;
    }
    for (SkuRelationScheduleVO relation : relationList) {
      relation.setAvgCost(0);
      relation.setAvgCostYuan("0");
      relation.setBoxCost(0);
      relation.setBoxCostYuan("0");
      relation.setDiscountAvgCost(0);
      relation.setbPriceYuan("0");
    }
  }

  /**
   * 打开进售价列表页面返回数据或者查询进售价页面返回数据.页面层解析出SkuRelationQueryVO
   * 
   * @param query
   * @param type 0 查询采购计划，1 查询销售计划
   * @return
   */
  public List<SkuRelationScheduleVO> getSkuRelationScheduleList(Long id, int type) {
    if (null == id) {
      return Collections.emptyList();
    }
    Result<ItemSkuRelationDTO> skuRelationResult = skuService.querySkuRelation(id);
    if (null == skuRelationResult || !skuRelationResult.getSuccess()
        || null == skuRelationResult.getModule()) {
      return Collections.emptyList();
    }
    ItemSkuRelationDTO re = skuRelationResult.getModule();
    SkuRelationScheduleQuery skuRelationQuery = new SkuRelationScheduleQuery();
    skuRelationQuery.setCskuIds(Arrays.asList(re.getCskuId()));
    skuRelationQuery.setPskuIds(Arrays.asList(re.getPskuId()));
    skuRelationQuery.setRelationType(re.getRelationType());
    Date now = new Date();
    if (type == 0) {
      skuRelationQuery.setBeforeBuyEnd(now);
    } else if (type == 1) {
      skuRelationQuery.setBeforeSaleEnd(now);
    }
    Result<List<ItemSkuRelationScheduleDTO>> queryResult =
        skuService.querySkuRelationScheduleDetailList(skuRelationQuery);
    if (null == queryResult || !queryResult.getSuccess()
        || CollectionUtils.isEmpty(queryResult.getModule())) {
      return Collections.emptyList();
    }

    List<ItemSkuRelationScheduleDTO> tmp = queryResult.getModule();
    List<SkuRelationScheduleVO> result = Lists.newArrayList();
    BeanCopierUtils.copyListBean(tmp, result, SkuRelationScheduleVO.class);
    return result;
    // SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    // List<String> resultList = Lists.newArrayList();
    // for(ItemSkuRelationScheduleDTO relation:queryResult.getModule()){
    // StringBuffer sb = new StringBuffer();
    // if(type == -1){
    // sb.append("销售开始:")
    // .append(sf.format(relation.getSaleStart()))
    // .append(" 结束:")
    // .append(sf.format(relation.getSaleEnd()))
    // .append(" 价格:")
    // .append(relation.getPrice());
    // }else{
    // sb.append("采购开始:")
    // .append(sf.format(relation.getBuyStart()))
    // .append(" 结束:")
    // .append(sf.format(relation.getBuyEnd()))
    // .append(" 单品价格:")
    // .append(relation.getbPrice());
    // }
    // resultList.add(sb.toString());
    // }
    // return resultList;
  }

  /**
   * 更新定期变价
   * 
   * @param skuRelationVO
   * @param isEmergency
   * @param userId
   * @return
   */
  public Result<Boolean> updateSkuRelationScheduleWhenEmergencyUpdatePrice(
      SkuRelationVO skuRelationVO, int userId, boolean isUpdateBuy, boolean isUpdateSale) {
    Result<Boolean> updatePrice =
        updateSkuRelationSchedule(skuRelationVO, userId, isUpdateBuy, isUpdateSale);
    if (null == updatePrice || !updatePrice.getSuccess() || !updatePrice.getModule()) {
      return updatePrice;
    }
    Result<ItemSkuRelationDTO> tmpSkuResult = skuService.querySkuRelation(skuRelationVO.getId());
    if (null == tmpSkuResult || !tmpSkuResult.getSuccess() || null == tmpSkuResult.getModule()) {
      logger.error("query skurelation is null,id:" + skuRelationVO.getId());
      return Result.getErrDataResult(ServerResultCode.SKU_RELATION_INFO_NOT_EXIT,
          "update parameter query db is null");
    }
    ItemSkuRelationDTO relation = tmpSkuResult.getModule();
    if (relation.getRelationType() == ItemSkuRelationDTO.RelationTypeConstants.RELATION_TYPE_NORMAL
        .shortValue()) {
      Long cskId = relation.getCskuId();
      Long pskuId = relation.getPskuId();
      SkuRelationQuery query = null;
      if (cskId > 0) {
        query =
            SkuRelationQuery.getQueryByCskuIds(Arrays.asList(cskId),
                ItemSkuRelationDTO.RelationTypeConstants.RELATION_TYPE_DISCOUNT);
      }
      if (pskuId > 0) {
        query =
            SkuRelationQuery.getQueryByPskuIds(Arrays.asList(pskuId),
                ItemSkuRelationDTO.RelationTypeConstants.RELATION_TYPE_DISCOUNT);
      }

      Result<List<ItemSkuRelationDTO>> queryResult = skuService.querySkuRelationList(query);
      if (null == queryResult || !queryResult.getSuccess()
          || CollectionUtils.isEmpty(queryResult.getModule())) {
        logger.error("query skurelation is null,id:" + skuRelationVO.getId());
        return Result.getErrDataResult(ServerResultCode.SKU_RELATION_INFO_NOT_EXIT,
            "update parameter query db is null");
      }

      ItemSkuRelationDTO discountRelation = queryResult.getModule().get(0);
      Long oldId = skuRelationVO.getId();
      skuRelationVO.setId(discountRelation.getId());
      updatePrice = updateSkuRelationSchedule(skuRelationVO, userId, isUpdateBuy, isUpdateSale);
      skuRelationVO.setId(oldId);
      return updatePrice;
    }
    return updatePrice;
  }

  /**
   * 更新定期变价
   * 
   * @param skuRelationVO
   * @param isEmergency
   * @param userId
   * @return
   */
  public Result<Boolean> updateSkuRelationSchedule(SkuRelationVO skuRelationVO, int userId,
      boolean isUpdateBuy, boolean isUpdateSale) {
    if (null == skuRelationVO || userId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "update request parameter is null");
    }

    boolean isBuyUpdate = isUpdateBuy;
    boolean isSaleUpdate = isUpdateSale;

    Result<ItemSkuRelationDTO> tmpDTO =
        getSkuRelationDTOByItemSkuRelationVO(skuRelationVO, false, isUpdateBuy, isUpdateSale);
    if (!tmpDTO.getSuccess()) {
      return Result.getErrDataResult(tmpDTO.getResultCode(), tmpDTO.getErrorMsg());
    }


    ItemSkuRelationDTO relation = tmpDTO.getModule();
    relation.setDiscountAvgCost(userId);
    ItemSkuRelationScheduleDTO insertRelation = new ItemSkuRelationScheduleDTO();
    BeanCopierUtils.copyProperties(relation, insertRelation);

    Result<ItemSkuRelationDTO> dbRelationResult =
        skuService.querySkuRelation(skuRelationVO.getId());
    if (null == dbRelationResult || !dbRelationResult.getSuccess()
        || null == dbRelationResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "插入系统变价计划失败，原始信息不存在");
    }
    // if(dbRelationResult.getModule().getPrice().intValue() !=
    // insertRelation.getPrice().intValue()){
    // isSaleUpdate = true;
    // }
    // if(dbRelationResult.getModule().getbPrice().intValue() !=
    // insertRelation.getbPrice().intValue()){
    // isBuyUpdate = true;
    // }
    if (!isBuyUpdate && !isSaleUpdate) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "无变价动作，忽略操作");
    }
    Date now = new Date();
    if (!isBuyUpdate) {// 剔除无效信息
      insertRelation.setbPrice(0);
      insertRelation.setBuyStart(now);
      insertRelation.setBuyEnd(now);
    }
    if (!isSaleUpdate) {// 剔除无效信息
      insertRelation.setPrice(0);
      insertRelation.setSaleStart(now);
      insertRelation.setSaleEnd(now);
    }
    // 插入最新的一条数据
    Result<Boolean> insertResult = skuService.insertItemSkuScheduleRelation(insertRelation);
    if (null == insertResult || !insertResult.getSuccess()) {
      logger.error("InsertItemSkuScheduleRelation failed, " + JackSonUtil.getJson(insertRelation));
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "插入系统变价计划失败，请重试");
    }

    return Result.getSuccDataResult(true);
  }

  /**
   * 更新进售价维护，包括进售价关系和优惠进售价关系
   * 
   * @param skuRelationVO
   * @return
   */
  public Result<Boolean> updateSkuRelationVO(SkuRelationVO skuRelationVO, boolean isEmergency,
      int userId, boolean isUpdateBuy, boolean isUpdateSale) {
    if (null == skuRelationVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "update request parameter is null");
    }
    Result<ItemSkuRelationDTO> tmpDTO =
        getSkuRelationDTOByItemSkuRelationVO(skuRelationVO, isEmergency, isUpdateBuy, isUpdateSale);
    if (!tmpDTO.getSuccess()) {
      return Result.getErrDataResult(tmpDTO.getResultCode(), tmpDTO.getErrorMsg());
    }

    ItemSkuRelationDTO relation = tmpDTO.getModule();

    ItemQuery query = new ItemQuery();
    query.setHasItemSku(true);
    query.setSkuId(relation.getCskuId());
    query.setReturnAll(true);

    relation.setProcessor(Long.valueOf(userId));
    Result<ItemBaseDTO> queryResult = commodityService.queryCommodity(query);
    if (null != queryResult && queryResult.getSuccess()) {
      ItemDTO item = (ItemDTO) queryResult.getModule();
      List<ItemCommoditySkuDTO> skuList = item.getItemSkuDtos();
      if (CollectionUtils.isNotEmpty(skuList)) {
        ItemCommoditySkuDTO sku = skuList.get(0);
        if (relation.getRelationType().equals(RelationTypeConstants.RELATION_TYPE_DISCOUNT)) {
          if (relation.getPrice() > sku.getUnitPrice()) {
            return Result.getErrDataResult(-1, "设置优惠价格高于原价，设置失败");
          }
        }
      }
    }

    Result<Boolean> result = skuService.updateItemSkuRelation(relation);
    if (result != null && result.getSuccess()) {
      if (relation.getRelationType().equals(RelationTypeConstants.RELATION_TYPE_NORMAL)) {

        Result<List<ItemSkuRelationDTO>> querResult =
            skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(
                Arrays.asList(relation.getCskuId()), RelationTypeConstants.RELATION_TYPE_DISCOUNT));
        if (null != querResult && querResult.getSuccess()
            && CollectionUtils.isNotEmpty(querResult.getModule())) {
          ItemSkuRelationDTO disCountRelation = querResult.getModule().get(0);
          relation.setId(disCountRelation.getId());
          relation.setFeature(disCountRelation.getFeature());
          relation.setProcessor(Long.valueOf(userId));
          relation.setRelationType(RelationTypeConstants.RELATION_TYPE_DISCOUNT);
          result = skuService.updateItemSkuRelation(relation);
          if (result != null && result.getSuccess()) {
            return result;
          }
        }
      }
      return result;
    }
    return result;
  }

  private Result<Boolean> emergencyUpdatePrice(ItemSkuRelationDTO skuRelation) {
    if (null == skuRelation) {
      String errorInfo = "emergencyUpdatePrice failed ,input skurelation is null";
      logger.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }
    ItemQuery query = new ItemQuery();
    query.setHasItemSku(true);
    query.setSkuId(skuRelation.getCskuId());
    query.setReturnAll(true);

    Result<ItemBaseDTO> queryResult = commodityService.queryCommodity(query);
    if (null != queryResult && queryResult.getSuccess()) {
      ItemDTO item = (ItemDTO) queryResult.getModule();
      List<ItemCommoditySkuDTO> skuList = item.getItemSkuDtos();
      if (CollectionUtils.isNotEmpty(skuList)) {
        ItemCommoditySkuDTO sku = skuList.get(0);

        String loginfo =
            "emergencyUpdatePrice success, update item price:" + item.getPrice()
                + ",update sku price:" + sku.getPrice() + ", new price:" + skuRelation.getPrice()
                + ",skuId" + sku.getSkuId();
        Result<Boolean> itemResult = null;
        if (skuRelation.getRelationType().equals(RelationTypeConstants.RELATION_TYPE_NORMAL)) {
          if (skuRelation.getPrice() < sku.getDiscountUnitPrice()) {
            return Result.getErrDataResult(-1, "设置价格低于优惠价格，设置失败");
          }
          sku.setPrice(skuRelation.getPrice());
          sku.setUnitPrice(skuRelation.getPrice());
          // item.setPrice(skuRelation.getPrice());
          // itemResult = commodityService.updateCommodityPrice(item, false);
        } else {
          if (skuRelation.getPrice() > sku.getUnitPrice()) {
            return Result.getErrDataResult(-1, "设置优惠价格高于原价，设置失败");
          }
          sku.setDiscountPrice(skuRelation.getPrice());
          sku.setDiscountUnitPrice(skuRelation.getPrice());
          // item.setDiscountPrice(skuRelation.getPrice());
          // itemResult = commodityService.updateCommodityPrice(item, true);
        }

        // Result<Boolean> skuResult = skuService.updateItemSku(sku);
        if (!(null != itemResult && itemResult.getSuccess())) {
          String errorInfo =
              "emergencyUpdatePrice failed ,update item result:"
                  + (null != itemResult && itemResult.getSuccess());
          logger.error(errorInfo);
          return Result.getErrDataResult(-1, errorInfo);
        } else {
          logger.error(loginfo);
        }
      } else {
        String errorInfo =
            ("emergencyUpdatePrice failed ,get skulist is null,skuId:" + skuRelation.getCskuId());
        logger.error(errorInfo);
        return Result.getErrDataResult(-1, errorInfo);
      }
    } else {
      String errorInfo =
          ("emergencyUpdatePrice failed ,get item is null,skuId:" + skuRelation.getCskuId());
      logger.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }
    return Result.getSuccDataResult(true);
  }

  /**
   * 查询一个发布的前台商品对象。先查询对应的后台商品，获取属性设置前台商品属性
   * 
   * @param productId
   * @return
   */
  public CommodityItemVO beforAddCommodityQueryProductBySku(long skuId) {
    Result<ItemProductDTO> productDTO = productService.queryItemProductBySku(skuId, true);
    if (null == productDTO || !productDTO.getSuccess()) {
      logger.error("get productDTO is null or not successs,skuId:" + skuId);
      return null;
    }

    List<ItemProductSkuDTO> skuList = productDTO.getModule().getItemProductSkuList();;
    ItemProductSkuDTO sku;
    if (CollectionUtils.isEmpty(skuList)) {
      logger.error("get skuList is empty,skuId:" + skuId);
      return null;
    }
    CommodityItemVO result = this.getCommodityItemVOByProductItemDTO(productDTO.getModule());

    sku = skuList.get(0);
    int catId = productDTO.getModule().getCategoryId();

    // 属性始终保持与采购商品同步
    List<PropertyVO> propertyVOList = getCommodityPropertyBySKUDTO(sku, catId, false);

    result.setPropertyVO(propertyVOList);
    return result;
  }

  /**
   * 发布加工类商品前，查询所有属性和属性值
   * 
   * @return
   */
  public BoomCommodityItemVO beforAddBoomCommodityQueryProperty(int catId) {
    BoomCommodityItemVO result = new BoomCommodityItemVO();
    List<PropertyDTO> propertyList = categoryComponent.getPropertyDTOByCatId(catId, true);
    List<PropertyVO> propertyVOList = getPropertyListByProperDTO(propertyList);
    result.setPropertyVO(propertyVOList);
    return result;
  }

  /**
   * 将前台输入的json数据转换成后台对象
   * 
   * @param jsonDate
   * @return
   */
  public CommodityItemVO getItemVOByJson(String jsonDate) {
    if (StringUtils.isEmpty(jsonDate)) {
      return null;
    }

    CommodityItemVO itemVO = null;
    boolean isBoomItem = false;
    String boomFlag = "\"isboom\":true";
    if (jsonDate.indexOf(boomFlag) >= 0) {
      isBoomItem = true;
    }

    if (isBoomItem) {
      itemVO = (BoomCommodityItemVO) JackSonUtil.jsonToObject(jsonDate, BoomCommodityItemVO.class);
    } else {
      itemVO = (CommodityItemVO) JackSonUtil.jsonToObject(jsonDate, CommodityItemVO.class);
    }
    return itemVO;
  }

  /**
   * 插入销售端商品，加工类商品+sku拆箱类型sku
   */
  public Result<Long> insertItemDTO(CommodityItemVO itemVO, int userId) {
    if (null == itemVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }

    if (StringUtils.isEmpty(itemVO.getTitle())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input title is null");
    }

    if (itemVO.getCatId() < 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input cat is error");
    }

    // if(itemVO.getPicList() == null || itemVO.getPicList().size() == 0){
    // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
    // "Input pic is error");
    // }
    //
    // if(itemVO.getIntroduction() == null || itemVO.getIntroduction().size() == 0){
    // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
    // "Input introduction is error");
    // }

    // 加工类分支
    if (itemVO instanceof BoomCommodityItemVO) {
      long insertId = this.insertBoomItemDTO((BoomCommodityItemVO) itemVO, userId);
      if (insertId <= 0) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "Input boomitem failed");
      } else {
        return Result.getSuccDataResult(insertId);
      }
    }

    ItemProductDTO productDTO = null;
    ItemProductSkuDTO productSkuDTO = null;
    Result<ItemProductDTO> productDTOResult =
        productService.queryItemProductBySku(itemVO.getSkuId(), true);
    if (null != productDTOResult && productDTOResult.getSuccess()
        && CollectionUtils.isNotEmpty(productDTOResult.getModule().getItemProductSkuList())) {
      productDTO = productDTOResult.getModule();
      productSkuDTO = productDTOResult.getModule().getItemProductSkuList().get(0);
    }

    // 校验对应后台商品存在
    if (null == productDTO || null == productSkuDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input item failed,product not exit");
    }

    ItemDTO item = this.getItemDTOByCommodityVO(itemVO);
    item.setCreator(userId);
    ItemCommoditySkuDTO sku = new ItemCommoditySkuDTO();
    getCommoditySkuByProductSku(itemVO, productSkuDTO, sku);
    item.setItemSkuDtos(Arrays.asList(sku));
    item.setSaleStatus(sku.getSaleStatus());
    item.setProcessor(0L);

    Result<Long> result = commodityService.insertCommodity(item);

    if (!result.getSuccess()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "Input item failed");
    }

    return Result.getSuccDataResult(result.getModule());// 插入成功标志code
  }

  /**
   * 插入加工类商品
   * 
   * @param itemVO
   * @param userId
   * @return
   */
  public long insertBoomItemDTO(BoomCommodityItemVO itemVO, int userId) {
    if (null == itemVO) {
      return -1;
    }

    ItemDTO item = this.getItemDTOByCommodityVO(itemVO);
    item.setPrice(itemVO.getPrice());
    item.setDiscountPrice(itemVO.getPrice());
    item.setCreator(userId);
    ItemCommoditySkuDTO sku = getBoomCommoditySku(itemVO);
    logger.info(JackSonUtil.getJson(sku));
    item.setItemSkuDtos(Arrays.asList(sku));
    item.setSaleStatus(sku.getSaleStatus());
    item.setProcessor(Long.valueOf(userId));

    Result<Long> result = commodityService.insertCommodity(item);

    if (!result.getSuccess()) {
      return 0;// 商品插入出现错误
    }

    return result.getModule();// 插入成功标志code
  }

  /**
   * 查询一个发布的前台商品对象。在修改前台商品时使用
   * 
   * @param productId
   * @return
   */
  public CommodityItemVO queryCommodityItem(long commoditySkuId) {
    ItemQuery iq = new ItemQuery();
    iq.setSkuId(commoditySkuId);
    iq.setReturnAll(true);
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(iq);
    if (null == itemResult || !itemResult.getSuccess()
        || !(itemResult.getModule() instanceof ItemDTO)
        || CollectionUtils.isEmpty(itemResult.getModule().getItemSkuDtos())) {
      return null;
    }
    ItemDTO dbItemDTO = (ItemDTO) itemResult.getModule();

    List<ItemCommoditySkuDTO> skuList = dbItemDTO.getItemSkuDtos();
    ItemCommoditySkuDTO sku = skuList.get(0);
    int catId = dbItemDTO.getCategoryId();

    CommodityItemVO itemVO = getCommodityItemVOByCommodityItemDTO(dbItemDTO, sku.getSupplierId());
    if (SkuTypeConstants.SECONDARY_PROCESSING_SKU.equals(sku.getSkuType().shortValue())) {
      itemVO = getBoomCommodityItemVOByCommodityItemDTO(dbItemDTO);
      ((BoomCommodityItemVO) itemVO).setBoomId(sku.queryBoomId());
      ((BoomCommodityItemVO) itemVO).setSaleDescribe(sku.querySpecification());
      ((BoomCommodityItemVO) itemVO).setInventory(sku.getInventory());
    }
    itemVO.setElement(sku.queryElementString());
    itemVO.setDisplaySpecifications(sku.queryDisplaySpecificationString());

    itemVO.setPrice(sku.getDiscountPrice());
    itemVO.setSkuId(sku.getSkuId());
    List<PropertyVO> propertyVOList = getCommodityPropertyBySKUDTO(sku, catId, true);

    itemVO.setPropertyVO(propertyVOList);
    return itemVO;
  }

  /**
   * 修改消费端商品，修改所有属性
   */
  public Result<Boolean> updateCommodityItem(CommodityItemVO itemVO, int userId) {
    if (null == itemVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }

    // 加工类分支
    if (itemVO instanceof BoomCommodityItemVO) {
      return this.updateBoomCommodityItem((BoomCommodityItemVO) itemVO, userId);
    }

    ItemQuery iq = new ItemQuery();
    iq.setSkuId(itemVO.getSkuId());
    iq.setHasItemSku(true);
    iq.setReturnAll(true);
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(iq);
    if (null == itemResult || !itemResult.getSuccess()
        || !(itemResult.getModule() instanceof ItemDTO)
        || CollectionUtils.isEmpty(itemResult.getModule().getItemSkuDtos())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update parameter query item is null");
    }
    ItemDTO dbItemDTO = (ItemDTO) itemResult.getModule();
    dbItemDTO.setGmtModified(new Date());
    dbItemDTO.setIntroduction(JackSonUtil.getJson(itemVO.getIntroduction()));
    dbItemDTO.setInventory(1);
    dbItemDTO.setPicList(itemVO.getPicList());
    dbItemDTO.setSubtitle(itemVO.getSubTitle());
    dbItemDTO.setTitle(itemVO.getTitle());
    dbItemDTO.setTitletags(itemVO.getTitletags());
    dbItemDTO.setCategoryId(itemVO.getCatId());
    dbItemDTO.setCreator(userId);
    dbItemDTO.setProcessor(0L);

    List<ItemCommoditySkuDTO> skuList = dbItemDTO.getItemSkuDtos();
    if (CollectionUtils.isEmpty(skuList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update parameter query item sku is null");
    }
    ItemCommoditySkuDTO sku = skuList.get(0);

    ItemProductSkuDTO productSkuDTO = null;
    Result<ItemProductDTO> productDTOResult =
        productService.queryItemProduct(dbItemDTO.getProductId(), true);
    if (null != productDTOResult && productDTOResult.getSuccess()
        && CollectionUtils.isNotEmpty(productDTOResult.getModule().getItemProductSkuList())) {
      productSkuDTO = productDTOResult.getModule().getItemProductSkuList().get(0);
    }

    getCommoditySkuByProductSku(itemVO, productSkuDTO, sku);
    dbItemDTO.setSaleStatus(sku.getSaleStatus());

    Result<Boolean> result = commodityService.updateCommodity(dbItemDTO);

    return Result.getSuccDataResult(result.getModule());
  }

  /**
   * 更新销售端商品，<br/>
   * 针对非加工类商品只更新商品部分信息，不修改商品基本属性，<br/>
   * 针对加工类商品只更新商品部分信息，修改所有属性基本属性
   */
  public Result<Boolean> updateCommodityItemInfo(CommodityItemVO itemVO, int userId) {
    if (null == itemVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }

    // 加工类分支
    if (itemVO instanceof BoomCommodityItemVO) {
      return this.updateBoomCommodityItem((BoomCommodityItemVO) itemVO, userId);
    }

    ItemQuery iq = new ItemQuery();
    iq.setSkuId(itemVO.getSkuId());
    iq.setHasItemSku(true);
    iq.setReturnAll(true);
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(iq);
    if (null == itemResult || !itemResult.getSuccess()
        || !(itemResult.getModule() instanceof ItemDTO)
        || CollectionUtils.isEmpty(itemResult.getModule().getItemSkuDtos())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update parameter query item is null");
    }
    ItemDTO dbItemDTO = (ItemDTO) itemResult.getModule();
    dbItemDTO.setGmtModified(new Date());
    dbItemDTO.setIntroduction(JackSonUtil.getJson(itemVO.getIntroduction()));
    dbItemDTO.setInventory(1);
    dbItemDTO.setPicList(itemVO.getPicList());
    dbItemDTO.setSubtitle(itemVO.getSubTitle());
    dbItemDTO.setTitle(itemVO.getTitle());
    dbItemDTO.setTitletags(itemVO.getTitletags());
    dbItemDTO.setCreator(userId);

    List<ItemCommoditySkuDTO> skuList = dbItemDTO.getItemSkuDtos();
    if (CollectionUtils.isEmpty(skuList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update parameter query item sku is null");
    }
    ItemCommoditySkuDTO sku = skuList.get(0);
    sku.setTitle(itemVO.getTitle());
    dbItemDTO.setPrice(sku.getPrice());
    dbItemDTO.setDiscountPrice(sku.getDiscountPrice());
    dbItemDTO.setSaleStatus(sku.getSaleStatus());

    Result<Boolean> result = commodityService.updateCommodity(dbItemDTO);
    return Result.getSuccDataResult(result.getModule());
  }

  /**
   * 更新加工类销售端商品，全量属性更新
   */
  public Result<Boolean> updateBoomCommodityItem(BoomCommodityItemVO itemVO, int userId) {
    ItemQuery iq = new ItemQuery();
    iq.setSkuId(itemVO.getSkuId());
    iq.setHasItemSku(true);
    iq.setReturnAll(true);
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(iq);
    if (null == itemResult || !itemResult.getSuccess()
        || !(itemResult.getModule() instanceof ItemDTO)
        || CollectionUtils.isEmpty(itemResult.getModule().getItemSkuDtos())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update parameter query boom is null");
    }
    ItemDTO dbItem = (ItemDTO) itemResult.getModule();
    List<ItemCommoditySkuDTO> skuList = dbItem.getItemSkuDtos();
    if (CollectionUtils.isEmpty(skuList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update parameter query boom sku is null");
    }
    ItemCommoditySkuDTO dbsku = skuList.get(0);

    ItemDTO item = this.getItemDTOByCommodityVO(itemVO);

    dbItem.setIntroduction(JackSonUtil.getJson(itemVO.getIntroduction()));
    dbItem.setPicList(item.getPicList());
    dbItem.setTitle(item.getTitle());
    dbItem.setSubtitle(item.getSubtitle());
    dbItem.setInventory(item.getInventory());
    dbItem.setTitletags(item.getTitletags());
    dbItem.setProperties(item.getProperties());// 设置属性
    dbItem.setCreator(userId);
    dbItem.setProcessor(Long.valueOf(userId));

    ItemCommoditySkuDTO sku = getBoomCommoditySku(itemVO);
    dbItem.setSaleStatus(sku.getSaleStatus());
    dbItem.setProcessor(Long.valueOf(userId));

    // dbsku.setPrice(sku.getPrice());
    // dbsku.setUnitPrice(sku.getUnitPrice());
    dbsku.setProperty(sku.getProperty());
    dbsku.setInventory(sku.getInventory());
    dbsku.addSpecification(sku.querySpecification());
    dbsku.addBoomId(sku.queryBoomId());
    dbsku.setSkuUnit(sku.getSkuUnit());
    dbsku.setTitle(sku.getTitle());
    dbsku.addElement(sku.queryElementString());
    dbsku.setSaleStatus(sku.getSaleStatus());
    dbsku.addDisplaySpecification(sku.queryDisplaySpecificationString());
    Result<Boolean> result = commodityService.updateCommodity(dbItem);
    return Result.getSuccDataResult(result.getModule());
  }

  /**
   * 判断输入条件是69码并返回
   * 
   * @param codeInfo
   * @return
   */
  private long get69Code(String codeInfo) {
    codeInfo = codeInfo.trim();
    if (StringUtils.isNumeric(codeInfo) && codeInfo.length() >= 8 && codeInfo.length() <= 13
        && (!codeInfo.startsWith("22"))) {
      Long code = Long.valueOf(codeInfo);
      return code;
    }
    return -1;
  }

  /**
   * 判断输入条件是能解析出商品id码并返回
   * 
   * @param codeInfo
   * @return
   */
  private long getSkuIdCode(String codeInfo) {
    codeInfo = codeInfo.trim();
    if (StringUtils.isEmpty(codeInfo) || !StringUtils.isNumeric(codeInfo)) {
      return -1;
    }
    if (codeInfo.startsWith("22") && codeInfo.length() == 13) {// 商品条码13位
      String code = codeInfo.substring(2, 7);// 截取中间plu码
      return Long.valueOf(code);
    } else if (codeInfo.length() == 5) {// 商品id=plu码
      return Long.valueOf(codeInfo);
    } else if (codeInfo.length() > 14 && codeInfo.startsWith(NearShelfLifeSkuBarFlag)) {
      codeInfo = codeInfo.replaceFirst(NearShelfLifeSkuBarFlag, "");
      codeInfo = codeInfo.substring(0, 5);
      return Long.valueOf(codeInfo);
    }
    return -1;
  }

  /**
   * 获取商品详列表信息，通过扫码枪请求进入，只支持单值，不分页。和下面方法完全一致，只是为了代码清晰没有合并
   * 
   * @return
   */
  public Result<CommodityItemVO> getCommodityItemVO(String inputItemCode) {
    if (StringUtils.isEmpty(inputItemCode) || StringUtils.isEmpty(inputItemCode.trim())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    if (!StringUtils.isNumeric(inputItemCode) || StringUtils.isEmpty(inputItemCode)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is not number");
    }
    inputItemCode = inputItemCode.trim();
    SkuQuery itemQuery = null;
    Long skuId = getSkuIdCode(inputItemCode);
    if (null != skuId && skuId > 0) {
      itemQuery = SkuQuery.querySkuById(skuId);
    } else {
      Long skuCode = get69Code(inputItemCode);
      if (null != skuCode && skuCode > 0) {
        itemQuery = SkuQuery.querySkuBySku69Code(skuCode);
      }
    }
    if (null == itemQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is error number");
    }

    if (null != itemQuery) {
      Result<ItemCommoditySkuDTO> itemInfo = skuService.queryItemSku(itemQuery);
      if (null == itemInfo || !itemInfo.getSuccess() || null == itemInfo.getModule()) {
        return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST,
            "Query item not exist,queryId:" + inputItemCode);
      }
      
      BarcodeInfoVO barInfo = BarcodeParsingUtil.parse(inputItemCode);
      CommodityItemVO item = getCommodityItemVOByItemCommoditySkuDTO(itemInfo.getModule());
      if(null !=barInfo ){
        if(barInfo.isScaleCode() || barInfo.isAdventCode()){
          item.setIndependentDisplay(true);
        }
        if(barInfo.isAdventCode()){
          item.setAllowrepeat(false);
        }
        item.setBarType(barInfo.getType());
      }
      return Result.getSuccDataResult(item);
    }
    
    return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "Query item not exist,queryId:"
        + inputItemCode);
  }
  
  /**
   * 查询档口sku列表
   * 
   * @return
   */
  public PagedResult<List<CommodityItemVO>> getKitchenCommodityItemVO(int catId,int pageNum) {
    int pageSize = 50;
    ItemListQuery itemListQuery = new ItemListQuery();
    itemListQuery.setPageSize(pageSize);
    itemListQuery.setPageNum(pageNum);
    itemListQuery.setCmCat2Ids(Arrays.asList(catId));
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_MARKET);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    itemListQuery.setSaleStatusList(saleStatus);
    itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    itemListQuery.setHasItemSku(true);
    PagedResult<List<CommodityItemVO>> result = null;
    Result<Map<String, Object>> queryResult = commodityService.queryCommodies(itemListQuery);
    if(null == queryResult || !queryResult.getSuccess()){
      result = PagedResult.getSuccDataResult(Collections.<CommodityItemVO>emptyList(), 0, 0, pageSize, pageNum);
    }
    List<ItemDTO> dtoList = (List<ItemDTO>) queryResult.getModule().get(CommodityService.ITEM_LIST_KEY);
    long totalNum = (Long)queryResult.getModule().get(CommodityService.ITEM_COUNT_KEY);
    long totalPageCount = totalNum % pageSize == 0 ? (totalNum / pageSize) : ((totalNum / pageSize)+1);
    
    List<CommodityItemVO> resultList = Lists.newArrayList();
    for(ItemDTO item:dtoList){
      CommodityItemVO itemVO = getCommodityItemVOByItemCommoditySkuDTO(item.getItemSkuDtos().get(0));
      itemVO.setPicList(item.getPicList());
      resultList.add(itemVO);
    }
    result = PagedResult.getSuccDataResult(resultList, (int)totalNum, (int)totalPageCount, pageSize, pageNum);
    return result;
  }

  /**
   * 获取商品详列表信息，通过扫码枪请求进入，支持多值，返回分页
   * 
   * @return
   */
  public Result<List<CommodityItemVO>> getCommodityItemVOByIdList(String inputSkuIdList) {
    if (StringUtils.isEmpty(inputSkuIdList) || StringUtils.isEmpty(inputSkuIdList.trim())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    String idArray[] = inputSkuIdList.split(",");
    List<Long> skuIds = Lists.newArrayList();
    List<Long> sku69Codes = Lists.newArrayList();
    List<Long> sortIdLists = Lists.newArrayList();
    for (String tmp : idArray) {
      if (StringUtils.isNotEmpty(tmp) && StringUtils.isNumeric(tmp)) {
        Long skuId = getSkuIdCode(tmp);
        if (null != skuId && skuId > 0) {
          skuIds.add(skuId);
          sortIdLists.add(skuId);
        } else {
          Long sku69Id = get69Code(tmp);
          if (null != sku69Id && sku69Id > 0) {
            sku69Codes.add(sku69Id);
            sortIdLists.add(sku69Id);
          }
        }
      }
    }
    if (CollectionUtils.isEmpty(skuIds) && CollectionUtils.isEmpty(sku69Codes)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_UNSUPPORTEDQUERY,
          "Input parameter change id is empty");
    }

    Map<Long, ItemCommoditySkuDTO> tmpResult = Maps.newHashMap();
    if (!CollectionUtils.isEmpty(skuIds)) {
      SkuQuery itemQuery1 = SkuQuery.querySkuBySkuIds(skuIds);
      Result<List<ItemCommoditySkuDTO>> queryResult = skuService.queryItemSkuList(itemQuery1);
      if (null != queryResult && queryResult.getSuccess()
          && CollectionUtils.isNotEmpty(queryResult.getModule())) {
        for (ItemCommoditySkuDTO item : queryResult.getModule()) {
          tmpResult.put(item.getSkuId(), item);
        }
      }
    }

    if (!CollectionUtils.isEmpty(sku69Codes)) {
      SkuQuery itemQuery2 = SkuQuery.querySkuBySku69CodeList(sku69Codes);
      Result<List<ItemCommoditySkuDTO>> queryResult = skuService.queryItemSkuList(itemQuery2);
      if (null != queryResult && queryResult.getSuccess()
          && CollectionUtils.isNotEmpty(queryResult.getModule())) {
        for (ItemCommoditySkuDTO item : queryResult.getModule()) {
          tmpResult.put(item.getSku69code(), item);
        }
      }
    }

    List<CommodityItemVO> returnResult = Lists.newArrayList();
    for (Long id : sortIdLists) {
      ItemCommoditySkuDTO tmp = tmpResult.get(id);
      if (null != tmp) {
        returnResult.add(getCommodityItemVOByItemCommoditySkuDTO(tmp));
      }
    }
    if (CollectionUtils.isNotEmpty(returnResult)) {
      return Result.getSuccDataResult(returnResult);
    }

    return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "Query item not exist,queryId:"
        + inputSkuIdList);
  }

  /**
   * 获取商品详细信息，通过扫码后传入
   * 
   * @return
   */
  // public Result<? extends BaseItemVO> getItemInfoVO(String inputItemCode) {
  // if (StringUtils.isEmpty(inputItemCode) || StringUtils.isEmpty(inputItemCode.trim())) {
  // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
  // "Input parameter is null");
  // }
  // if (!StringUtils.isNumeric(inputItemCode) || StringUtils.isEmpty(inputItemCode)) {
  // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
  // "Input parameter is not number");
  // }
  // inputItemCode = inputItemCode.trim();
  // int length = inputItemCode.length();
  // String itemQueryCode = "";
  // SkuQuery itemQuery = null;
  // SkuQuery productQuery = null;
  // if (inputItemCode.startsWith("22") && length == 7) {// plu码范围
  // itemQueryCode = inputItemCode.substring(2);// 截取中间plu码
  // itemQuery = SkuQuery.querySkuById(Long.valueOf(itemQueryCode));
  // } else if (inputItemCode.startsWith("22") && length == 13) {// 电子秤条码
  // itemQueryCode = inputItemCode.substring(2, 7);// 截取中间plu码
  // itemQuery = SkuQuery.querySkuById(Long.valueOf(itemQueryCode));
  // } else if (inputItemCode.startsWith("69") && length == 13) {// 普通69码
  // itemQueryCode = inputItemCode;
  // itemQuery = SkuQuery.querySkuBySku69Code(Long.valueOf(itemQueryCode));
  // } else if (length == 15) {// 产品货号
  // productQuery = SkuQuery.querySkuById(Long.valueOf(inputItemCode));
  // } else {
  // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
  // "Input parameter is error number");
  // }
  //
  // if (null != itemQuery) {
  // Result<ItemCommoditySkuDTO> itemInfo = skuService.queryItemSku(itemQuery);
  // if (null == itemInfo || !itemInfo.getSuccess() || null == itemInfo.getModule()) {
  // return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST,
  // "Query item not exist,queryId:" + inputItemCode);
  // }
  // return Result
  // .getSuccDataResult(getCommodityItemVOByItemCommoditySkuDTO(itemInfo.getModule()));
  // } else if (null != productQuery) {
  // Result<List<ItemProductSkuDTO>> itemInfo = skuService.queryProductSku(productQuery);
  // if (null == itemInfo || !itemInfo.getSuccess()
  // || CollectionUtils.isEmpty(itemInfo.getModule())) {
  // return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST,
  // "Query item not exist,queryId:" + inputItemCode);
  // }
  // return Result.getSuccDataResult(getBaseItemVOBySkuDTO(itemInfo.getModule().get(0)));
  // }
  // return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "Query item not exist,queryId:"
  // + inputItemCode);
  // }

  /**
   * 获取查询对象
   * 
   * @return
   */
  public SkuRelationQuery getQueryByQueryVO(SkuRelationQueryVO query) {
    SkuRelationQuery result = null;
    short relationType = query.getType();
    if (relationType != 0 && relationType != 1) {
      relationType = 0;
    }
    int pageSize = query.getPageSize() <= 0 ? 8 : query.getPageSize();
    int pageNum = query.getPageNum() < 0 ? 0 : query.getPageNum();
    List<Long> cskuIdList = Lists.newArrayList();
    if (StringUtils.isNotEmpty(query.getSkuName())) {
      SkuQuery skuquery = SkuQuery.querySkuByTitle(query.getSkuName(), pageSize, pageNum);
      Result<List<ItemCommoditySkuDTO>> cskuResult = skuService.queryItemSkuList(skuquery);
      if (null != cskuResult && cskuResult.getSuccess()
          && CollectionUtils.isNotEmpty(cskuResult.getModule())) {
        for (ItemCommoditySkuDTO sku : cskuResult.getModule()) {
          cskuIdList.add(sku.getSkuId());
        }
      }

      if (CollectionUtils.isEmpty(cskuIdList)) {
        return null;
      } else {
        result = SkuRelationQuery.getQueryByCskuIds(cskuIdList, relationType);
        result.setPageNum(0);
        result.setPageSize(cskuIdList.size());
        return result;
      }
    }

    if (query.getBeginItemCode() > 0 && query.getEndItemCode() > 0) {
      result =
          SkuRelationQuery.querySkuRelationRange(query.getBeginItemCode(), query.getEndItemCode(),
              relationType, pageSize, pageNum);
      return result;
    } else if (query.getBeginItemCode() > 0) {
      result =
          SkuRelationQuery.getQueryByPskuIds(Arrays.asList(query.getBeginItemCode()), relationType);
      result.setPageSize(pageSize);
      result.setPageNum(pageNum);
      return result;
    } else if (query.getEndItemCode() > 0) {
      result =
          SkuRelationQuery.getQueryByPskuIds(Arrays.asList(query.getEndItemCode()), relationType);
      result.setPageSize(pageSize);
      result.setPageNum(pageNum);
      return result;
    }
    result = SkuRelationQuery.getQuery(relationType);
    result.setPageSize(pageSize);
    result.setPageNum(pageNum);
    return result;
  }

  /**
   * 获取查询对象的分页数据
   * 
   * @return
   */
  public int getQueryCountByQueryVO(SkuRelationQueryVO query) {
    SkuRelationQuery result = null;
    short relationType = query.getType();
    if (relationType != 0 && relationType != 1) {
      relationType = 0;
    }
    int pageSize = query.getPageSize() <= 0 ? 20 : query.getPageSize();
    int pageNum = query.getPageNum() < 0 ? 0 : query.getPageNum();
    if (StringUtils.isNotEmpty(query.getSkuName())) {
      SkuQuery skuquery = SkuQuery.querySkuByTitle(query.getSkuName(), pageSize, pageNum);
      Result<Integer> cskuResult = skuService.queryItemSkuCount(skuquery);
      if (null != cskuResult && cskuResult.getSuccess()) {
        int tmpCount =
            (cskuResult.getModule() % pageSize) == 0 ? (cskuResult.getModule() / pageSize)
                : ((cskuResult.getModule() / pageSize) + 1);
        return tmpCount;
      } else {
        return 0;
      }
    }

    if (query.getBeginItemCode() > 0 && query.getEndItemCode() > 0) {
      result =
          SkuRelationQuery.querySkuRelationRange(query.getBeginItemCode(), query.getEndItemCode(),
              relationType, pageSize, pageNum);
    } else if (query.getBeginItemCode() > 0) {
      result =
          SkuRelationQuery.getQueryByPskuIds(Arrays.asList(query.getBeginItemCode()), relationType);
    } else if (query.getEndItemCode() > 0) {
      result =
          SkuRelationQuery.getQueryByPskuIds(Arrays.asList(query.getEndItemCode()), relationType);
    } else {
      result = SkuRelationQuery.getQuery();
    }
    Result<Integer> countResult = skuService.querySkuRelationCount(result);
    if (null != countResult && countResult.getSuccess()) {
      int tmpCount =
          (countResult.getModule() % pageSize) == 0 ? (countResult.getModule() / pageSize)
              : ((countResult.getModule() / pageSize) + 1);
      return tmpCount;
    }
    return 0;
  }

  /**
   * 获取查询对象的分页数据
   * 
   * @return
   */
  public int countSkuRelationByQueryVO(SkuRelationQueryVO query) {
    SkuRelationQuery result = null;
    short relationType = query.getType();
    if (relationType != 0 && relationType != 1) {
      relationType = 0;
    }
    int pageSize = query.getPageSize() <= 0 ? 20 : query.getPageSize();
    int pageNum = query.getPageNum() < 0 ? 0 : query.getPageNum();
    if (StringUtils.isNotEmpty(query.getSkuName())) {
      SkuQuery skuquery = SkuQuery.querySkuByTitle(query.getSkuName(), pageSize, pageNum);
      Result<Integer> cskuResult = skuService.queryItemSkuCount(skuquery);
      if (null != cskuResult && cskuResult.getSuccess()) {
        return cskuResult.getModule();
      } else {
        return 0;
      }
    }

    if (query.getBeginItemCode() > 0 && query.getEndItemCode() > 0) {
      result =
          SkuRelationQuery.querySkuRelationRange(query.getBeginItemCode(), query.getEndItemCode(),
              relationType, pageSize, pageNum);
    } else if (query.getBeginItemCode() > 0) {
      result =
          SkuRelationQuery.getQueryByPskuIds(Arrays.asList(query.getBeginItemCode()), relationType);
    } else if (query.getEndItemCode() > 0) {
      result =
          SkuRelationQuery.getQueryByPskuIds(Arrays.asList(query.getEndItemCode()), relationType);
    } else {
      result = SkuRelationQuery.getQuery();
    }
    Result<Integer> countResult = skuService.querySkuRelationCount(result);
    if (null != countResult && countResult.getSuccess()) {
      return countResult.getModule();
    }
    return 0;
  }

  /**
   * 获取商品详情json数据
   * 
   * @param itemId
   */
  public List<IntroductDataDTO> getIntroJson(long itemId) {
    ItemQuery query = new ItemQuery();
    query.setItemId(itemId);
    query.setReturnAll(true);
    Result<ItemBaseDTO> itemResult = commodityService.queryCommodity(query);
    if (itemResult.getSuccess() && null != itemResult.getModule()) {
      ItemBaseDTO itemDb = itemResult.getModule();
      if (itemDb instanceof ItemDTO) {
        List<IntroductDataDTO> introList = ((ItemDTO) itemDb).getIntroductionDataList();
        if (null == introList) {
          introList = Lists.newArrayList();
        }
        IntroductDataDTO datadto = new IntroductDataDTO();
        datadto.setSource("http://img.xianzaishi.com/1/1478783678786.jpeg");
        datadto.setType(2);
        introList.add(datadto);
        return introList;
      }
    }
    return Collections.emptyList();
  }

  /**
   * 更新后台的skuId
   * 
   * @return
   */
  public String checkUptateSkuCode(String title, long skuId, long sku69Code, String pwd) {
    if (skuId > 0 && sku69Code > 0) {
      if (!("123980".equals(pwd) || "123981".equals(pwd) || "123982".equals(pwd)
          || "123983".equals(pwd) || "123984".equals(pwd) || "123985".equals(pwd) || "123986"
            .equals(pwd))) {
        return "update 69code failed,hehe";
      }

      SkuQuery query = SkuQuery.querySkuBySku69Code(sku69Code);
      Result<ItemCommoditySkuDTO> result = skuService.queryItemSku(query);
      if (null != result && result.getSuccess() && null != result.getModule()
          && !result.getModule().getSkuId().equals(skuId)) {
        return "69码已经存在，不允许更新。系统存在同名sku标题：" + result.getModule().getTitle() + ", plu码："
            + result.getModule().getSkuId();
      }

      Result<Boolean> updateResult = skuService.updateSku69Code(skuId, sku69Code);
      if (null == updateResult) {
        return "update sku69code failed,service error,skuId:" + skuId + ",69code:" + sku69Code;
      } else if (!updateResult.getModule()) {
        return ",update sku69code failed,error msg:" + updateResult.getErrorMsg() + ",errorcode:"
            + updateResult.getResultCode() + ",skuId:" + skuId + ",69code:" + sku69Code;
      } else {
        logger.error("Update sku69code success,skuId:" + skuId + ",new 69code:" + sku69Code
            + ",token:" + pwd);
        return "success";
      }
    }
    List<ItemCommoditySkuDTO> resultDOList = Lists.newArrayList();

    if (skuId > 0) {
      SkuQuery query = null;
      if (skuId > 99999) {
        query = SkuQuery.querySkuBySku69Code(skuId);
      } else {
        query = SkuQuery.querySkuById(skuId);
      }

      Result<ItemCommoditySkuDTO> result = skuService.queryItemSku(query);
      if (null == result || !result.getSuccess()) {
        return "通过plu码或者69码查询数据无结果，输入内容:" + sku69Code;
      }
      resultDOList.add(result.getModule());
    } else if (StringUtils.isNotEmpty(title)) {
      Result<List<ItemCommoditySkuDTO>> result =
          skuService.queryItemSkuList(SkuQuery.querySkuByTitle(title, 200, 0));
      if (null == result || !result.getSuccess() || CollectionUtils.isEmpty(result.getModule())) {
        return "通过标题查询数据无结果，输入内容:" + title;
      }
      resultDOList.addAll(result.getModule());
    }

    String resultInfo = "";
    if (CollectionUtils.isNotEmpty(resultDOList)) {
      for (ItemCommoditySkuDTO sku : resultDOList) {
        resultInfo =
            resultInfo + "plu码:" + sku.getSkuId() + ", 系统内部原有69码:" + sku.getSku69code() + ", 商品标题:"
                + sku.getTitle() + ", 原价：" + sku.getDiscountPrice() + "<br/>";;
      }
      logger.error("query sku info before update sku69Code.result is:" + resultInfo);
      return resultInfo;
    }
    return "Insert or update failed,input parameter title is:" + title + ",skuid" + skuId
        + ",skucode:" + sku69Code;
  }

  /**
   * 更新标内容
   * 
   * @param addremoveflag
   * @param tagContent
   * @param tagRule
   * @return
   */
  public Result<String> updateSkuTags(List<Long> skuIds, Integer tag, Short channel,
      String tagContent, String tagRule) {
    if (CollectionUtils.isEmpty(skuIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "商品数据为空");
    }
    String updateResultInfo = "商品：";
    List<Long> addTagsSuccessIds = Lists.newArrayList();
    List<Long> addTagsFaileIds = Lists.newArrayList();
    for (Long id : skuIds) {
      updateTagsContent(id, tag, channel, tagContent, tagRule);
      Result<Boolean> updateResult = skuService.updateItemSkuTags(id, tag, 1);
      if (null != updateResult && updateResult.getSuccess()) {
        addTagsSuccessIds.add(id);
      } else {
        addTagsFaileIds.add(id);
      }
    }
    if (CollectionUtils.isNotEmpty(addTagsSuccessIds)) {
      logger.error("商品：" + JackSonUtil.getJson(addTagsSuccessIds) + "打标成功");
      updateResultInfo += JackSonUtil.getJson(addTagsSuccessIds) + "打标成功     ";
    }
    if (CollectionUtils.isNotEmpty(addTagsFaileIds)) {
      logger.error("商品：" + JackSonUtil.getJson(addTagsFaileIds) + "打标失败");
      updateResultInfo += JackSonUtil.getJson(addTagsFaileIds) + "打标失败";
    }
    return Result.getSuccDataResult(updateResultInfo);
  }

  /**
   * 更新标内容
   * 
   * @param id
   * @param tag
   * @param channel
   * @param tagContent
   * @param tagRule
   */
  private void updateTagsContent(Long id, Integer tag, Short channel, String tagContent,
      String tagRule) {
    if (null == id) {
      return;
    }
    Result<ItemCommoditySkuDTO> itemCommoditySkuResult =
        skuService.queryItemSku(SkuQuery.querySkuById(id));
    if (null == itemCommoditySkuResult || !itemCommoditySkuResult.getSuccess()
        || null == itemCommoditySkuResult.getModule()) {
      return;
    }
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuResult.getModule();
    String skuTagsDetailExist = itemCommoditySkuDTO.queryTagContent();// 获取商品标详细内容
    String tagId = skuTagDefinition(tag);
    if (StringUtils.isEmpty(tagId)) {
      return;
    }
    SkuTagDetail skuTagDetail = new SkuTagDetail();
    skuTagDetail.setTagId(tagId);
    skuTagDetail.setChannel(channel);
    // 商品限购标
    if (tag == 4096) {
      skuTagDetail.setTagRule(tagContent);
      tagContent = new StringBuilder("限购").append(tagContent).append("件").toString();
    }
    if (StringUtils.isNotBlank(tagContent)) {
      skuTagDetail.setTagName(tagContent);
    }
    if (StringUtils.isNotEmpty(tagRule) && StringUtils.isNumeric(tagRule)) {
      skuTagDetail.setTagRule(tagRule);
    }
    List<SkuTagDetail> skuTagDetails = Lists.newArrayList();
    if (StringUtils.isNotBlank(skuTagsDetailExist)) {
      skuTagDetails = JackSonUtil.jsonToList(skuTagsDetailExist, SkuTagDetail.class);
    }
    if (CollectionUtils.isEmpty(skuTagDetails)) {
      skuTagDetails = Lists.newArrayList();
    }
    final SkuTagDetail skuTagDetailFinal = skuTagDetail;
    // 删除已重复的skuTagDetails中的tagId
    CollectionUtils.filter(skuTagDetails, new Predicate() {
      @Override
      public boolean evaluate(Object input) {
        SkuTagDetail skuTagDetailElement = (SkuTagDetail) input;
        String tagIdElement = skuTagDetailElement.getTagId();
        // tagId不为空并且在skuTagDetailFinal中有相同的tagId则删除
        if (StringUtils.isNotBlank(tagIdElement)
            && tagIdElement.equals(skuTagDetailFinal.getTagId())) {
          // 在删除之前将优惠标的tagRule存入skuTagDetailFinal中
          if (tagIdElement.equals(SkuTagDefinitionConstants.PROMOTION_TAG)
              && StringUtils.isNotBlank(skuTagDetailElement.getTagRule())) {
            StringBuilder sBuilder = new StringBuilder(skuTagDetailElement.getTagRule());
            String tagRuleFinal = skuTagDetailFinal.getTagRule();
            List<String> tagRuleElements =
                Arrays.asList(skuTagDetailElement.getTagRule().split(SPLIT_COMMA_KEY));
            // 判断列表中是否已经存在tagRuleFinal，并且tagRuleFinal不为空
            if (StringUtils.isNotBlank(tagRuleFinal) && !tagRuleElements.contains(tagRuleFinal)) {
              sBuilder.append(",");
              sBuilder.append(tagRuleFinal);
            }
            skuTagDetailFinal.setTagRule(sBuilder.toString());
          }
          return false;
        }
        return true;
      }
    });
    skuTagDetails.add(skuTagDetail);
    itemCommoditySkuDTO.addTagContent(JackSonUtil.getJson(skuTagDetails));
    Result<Boolean> updateResult = skuService.updateItemSku(itemCommoditySkuDTO);
    if (null == updateResult || !updateResult.getSuccess() || null == updateResult.getModule()
        || !updateResult.getModule()) {
      logger.error("商品sku id:" + itemCommoditySkuDTO.getSkuId() + ",更新商品标内容失败:"
          + JackSonUtil.getJson(updateResult));
    }

  }

  /**
   * 根据tag获取tagId
   * 
   * @param tag
   * @return
   */
  private String skuTagDefinition(Integer tag) {
    String tagId = null;
    switch (tag) {
      case 0x1:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x2:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x4:
        tagId = SkuTagDefinitionConstants.DES_TAG;
        break;
      case 0x8:
        tagId = SkuTagDefinitionConstants.LIMITED_TIME_PREFERENCE;
        break;
      case 0x10:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x20:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x40:
        tagId = SkuTagDefinitionConstants.NOT_ALLOW_PROMOTION;
        break;
      case 0x80:
        tagId = SkuTagDefinitionConstants.NOT_ALLOW_CHARGE;
        break;
      case 0x100:
        tagId = SkuTagDefinitionConstants.NOT_ENOUGH_INVENTORY;
        break;
      case 0x200:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x400:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x1000:
        tagId = SkuTagDefinitionConstants.BUY_LIMIT;
        break;
      case 0x2000:
        tagId = SkuTagDefinitionConstants.DISTRIBUTION_FREE;
        break;
      case 0x4000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x8000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x10000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x20000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x40000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x80000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
      case 0x100000:
        tagId = SkuTagDefinitionConstants.PROMOTION_TAG;
        break;
    }
    return tagId;
  }

  /**
   * 查询商品标
   * 
   * @param skuIds
   * @param updateResultInfo
   * @return
   */
  public Result<List<ItemTagsDetaileVO>> querySkuTags(List<Long> skuIds) {
    if (CollectionUtils.isEmpty(skuIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "商品sku id列表为空或者格式错误");
    }
    
    Result<List<ItemSkuTagsDTO>> queryResult = skuService.querySkuTagsDetail(skuIds);
    StringBuilder resultInfo = new StringBuilder();
    if (null == queryResult || !queryResult.getSuccess() || null == queryResult.getModule()) {
      resultInfo.append("商品编号为：").append(skuIds).append("标内容为空");
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          resultInfo.toString());
    }
    List<ItemSkuTagsDTO> itemSkuTagsDTOs = queryResult.getModule();
    List<ItemTagsDetaileVO> queryResultInfo = Lists.newArrayList();
    for (ItemSkuTagsDTO itemSkuTagsDTO : itemSkuTagsDTOs) {
      List<SkuTagDetail> skuTagDetails = itemSkuTagsDTO.getSkuTagDetailList();
      if (CollectionUtils.isEmpty(skuTagDetails)) {
        continue;
      }
      for (SkuTagDetail skuTagDetail : skuTagDetails) {
        // 解析商品优惠标
        if (SkuTagDefinitionConstants.PROMOTION_TAG.equals(skuTagDetail.getTagId())) {
          String[] tagRules = null;
          if (StringUtils.isNotEmpty(skuTagDetail.getTagRule())) {
            tagRules = skuTagDetail.getTagRule().split(SPLIT_COMMA_KEY);
          }
          if (null == tagRules) {
            continue;
          }
          List<Long> couponIds = Lists.newArrayList();
          for (String tagRule : tagRules) {
            if (!StringUtils.isNumeric(tagRule)) {
              continue;
            }
            couponIds.add(Long.valueOf(tagRule));
          }
          // 根据优惠id列表获取优惠标题
          Result<List<CouponDTO>> couponResult = couponService.getCouponByIdList(couponIds);
          Map<String, String> couponTitleMap = Maps.newHashMap();
          if (null != couponResult && couponResult.getSuccess() && null != couponResult.getModule()) {
            List<CouponDTO> couponDTOs = couponResult.getModule();
            for (CouponDTO couponDTO : couponDTOs) {
              couponTitleMap.put(String.valueOf(couponDTO.getId()), couponDTO.getTitle());
            }
          }
          for (String tagRule : tagRules) {
            ItemTagsDetaileVO itemTagsDetaileVO = new ItemTagsDetaileVO();
            itemTagsDetaileVO.setSkuId(itemSkuTagsDTO.getSkuId());
            itemTagsDetaileVO.setTagId(skuTagDetail.getTagId());
            itemTagsDetaileVO.setChannel(skuTagDetail.getChannel());
            if (null != skuTagDetail.getChannel()) {
              itemTagsDetaileVO.setChannelString(CHANNEL_STRINGS[skuTagDetail.getChannel()]);
            }
            itemTagsDetaileVO.setTagContent(couponTitleMap.get(tagRule));
            itemTagsDetaileVO.setTagString("优惠标");
            itemTagsDetaileVO.setContentType((short) 0);
            itemTagsDetaileVO.setTagRule(tagRule);
            queryResultInfo.add(itemTagsDetaileVO);
          }
          continue;
        }
        String tagName = skuTagDetail.getTagName();
        ItemTagsDetaileVO itemTagsDetaileVO = new ItemTagsDetaileVO();
        itemTagsDetaileVO.setSkuId(itemSkuTagsDTO.getSkuId());
        itemTagsDetaileVO.setTagId(skuTagDetail.getTagId());
        itemTagsDetaileVO.setChannel(skuTagDetail.getChannel());
        itemTagsDetaileVO.setTagRule(skuTagDetail.getTagRule());
        if (null != skuTagDetail.getChannel()) {
          itemTagsDetaileVO.setChannelString(CHANNEL_STRINGS[skuTagDetail.getChannel()]);
        }
        if (StringUtils.isNotBlank(tagName)) {
          itemTagsDetaileVO.setTagContent(tagName);
        }
        itemTagsDetaileVO.setTagString(getTagsTypeByTagId(skuTagDetail.getTagId(),
            itemSkuTagsDTO.getTags()));
        queryResultInfo.add(itemTagsDetaileVO);
      }
    }
    if (CollectionUtils.isEmpty(queryResultInfo)) {
      resultInfo.append("商品编号为：").append(skuIds).append("的标内容为空");
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          resultInfo.toString());
    }
    return Result.getSuccDataResult(queryResultInfo);
  }

  /**
   * 获取商品标类型
   * 
   * @param tagId
   * @return
   */
  private String getTagsTypeByTagId(String tagId, Integer tags) {
    if (StringUtils.isBlank(tagId)) {
      return null;
    }
    String tagType = null;
    switch (tagId) {
      case SkuTagDefinitionConstants.BUY_DISCOUNT_99_40:
        tagType = "满99减40标";
        break;
      case SkuTagDefinitionConstants.BUY_DISCOUNT_40_20:
        tagType = "满40减20标";
        break;
      case SkuTagDefinitionConstants.DES_TAG:
        tagType = "商品描述标";
        break;
      case SkuTagDefinitionConstants.NOT_ALLOW_PROMOTION:
        tagType = "商品不参与任何优惠标";
        break;
      case SkuTagDefinitionConstants.NOT_ALLOW_CHARGE:
        tagType = "商品不允许交易标";
        break;
      case SkuTagDefinitionConstants.NOT_ENOUGH_INVENTORY:
        tagType = "商品没有库存标";
        break;
      case SkuTagDefinitionConstants.LIMITED_TIME_PREFERENCE:
        tagType = "商品限时特惠标";
        break;
      case SkuTagDefinitionConstants.BUY_LIMIT:
        tagType = "商品限购标";
        break;
      case SkuTagDefinitionConstants.DISTRIBUTION_FREE:
        tagType = "商品包邮标";
        break;
    }
    return tagType;
  }

  /**
   * 更新前查询商品基本属性，包括sku信息
   * 
   * @param itemProductDTO
   * @return
   */
  private ProductItemVO getProductItemVOByProductItemDTO(ItemProductDTO itemProductDTO) {
    ProductItemVO item = new ProductItemVO();
    item.setId(itemProductDTO.getProductId());
    item.setIntroduction(JackSonUtil.jsonToList(itemProductDTO.getIntroduction(),
        IntorductionDataVO.class));
    item.setPicList(itemProductDTO.getPicList());
    item.setTitle(itemProductDTO.getTitle());
    item.setSubTitle(itemProductDTO.getSubtitle());
    item.setPurchasingAgentName(getSupplierAgentName(itemProductDTO.getItemProductSkuList().get(0)
        .getSupplierAgent()));
    item.setSupplierName(getSupplierName(itemProductDTO.getItemProductSkuList().get(0)
        .getSupplierId()));
    item.setStatus(itemProductDTO.getStatus());
    return item;
  }

  /**
   * 根据后台商品获取一个前台商品对象
   * 
   * @param itemProductDTO
   * @return
   */
  private CommodityItemVO getCommodityItemVOByProductItemDTO(ItemProductDTO itemProductDTO) {
    CommodityItemVO item = new CommodityItemVO();
    item.setIntroduction(JackSonUtil.jsonToList(itemProductDTO.getIntroduction(),
        IntorductionDataVO.class));
    item.setPicList(itemProductDTO.getPicList());
    item.setTitle(itemProductDTO.getTitle());
    item.setSubTitle(itemProductDTO.getSubtitle());
    item.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    item.setSkuId(itemProductDTO.getItemProductSkuList().get(0).getSkuId());
    item.setCatId(itemProductDTO.getCategoryId());
    item.setTitletags(itemProductDTO.getTitletags());
    item.setProductId(itemProductDTO.getProductId());
    return item;
  }

  /**
   * 更新前查询商品基本属性，包括sku信息
   * 
   * @param itemProductDTO
   * @return
   */
  private CommodityItemVO getCommodityItemVOByCommodityItemDTO(ItemDTO itemDTO, int supplierId) {
    CommodityItemVO item = new CommodityItemVO();
    item.setId(itemDTO.getItemId());

    item.setIntroduction(JackSonUtil.jsonToList(itemDTO.getIntroduction(), IntorductionDataVO.class));
    item.setPicList(itemDTO.getPicList());
    item.setTitle(itemDTO.getTitle());
    item.setSubTitle(itemDTO.getSubtitle());
    item.setSupplierName(this.getSupplierName(supplierId));
    item.setStatus(itemDTO.getStatus());
    item.setTitletags(itemDTO.getTitletags());
    return item;
  }

  /**
   * 更新前查询商品基本属性，包括sku信息
   * 
   * @param itemProductDTO
   * @return
   */
  private BoomCommodityItemVO getBoomCommodityItemVOByCommodityItemDTO(ItemDTO itemDTO) {
    BoomCommodityItemVO item = new BoomCommodityItemVO();
    item.setId(itemDTO.getItemId());
    item.setIntroduction(JackSonUtil.jsonToList(itemDTO.getIntroduction(), IntorductionDataVO.class));
    item.setPicList(itemDTO.getPicList());
    item.setTitle(itemDTO.getTitle());
    item.setSubTitle(itemDTO.getSubtitle());
    item.setSupplierName("");
    item.setStatus(itemDTO.getStatus());
    item.setTitletags(itemDTO.getTitletags());
    return item;
  }

  /**
   * 翻译前台需要的字段
   * 
   * @param skuDTO
   * @return
   */
  private CommodityItemVO getCommodityItemVOByItemCommoditySkuDTO(ItemCommoditySkuDTO skuDTO) {
    CommodityItemVO item = new CommodityItemVO();
    item.setId(skuDTO.getItemId());
    item.setTitle(skuDTO.getTitle());
    item.setSubTitle(skuDTO.getTitle());
    item.setSkuId(skuDTO.getSkuId());
    item.setElementList(skuDTO.queryElement());
    item.setStandardGoods(skuDTO.getIsSteelyardSku());
    List<ItemSpecificationDTO> itemSpecificationDTOs = skuDTO.queryDisplaySpecification();// 获取展示规格列表
    List<ItemDisplaySpecificationVO> itemDisplaySpecificationVOs = Lists.newArrayList();// 需要将dto转换为vo
    if (CollectionUtils.isNotEmpty(itemSpecificationDTOs)) {
      BeanCopierUtils.copyListBean(itemSpecificationDTOs, itemDisplaySpecificationVOs,
          ItemDisplaySpecificationVO.class);
    }
    if (CollectionUtils.isNotEmpty(itemDisplaySpecificationVOs)) {
      item.setDisplaySpecificationList(itemDisplaySpecificationVOs);// 设置前台能识别的展示规格vo
    }
    List<ItemSpecificationDTO> relatedItems = skuDTO.queryRelatedItems();// 获取不同关联商品列表
    List<ItemDisplaySpecificationVO> relatedSkuIds = Lists.newArrayList();
    if (CollectionUtils.isNotEmpty(relatedItems)) {
      BeanCopierUtils.copyListBean(relatedItems, relatedSkuIds, ItemDisplaySpecificationVO.class);
    }
    if (CollectionUtils.isNotEmpty(relatedSkuIds)) {
      item.setRelatedItems(relatedSkuIds);
    }
    if (null != skuDTO.getSku69code()) {
      item.setSku69Id(skuDTO.getSku69code());
    } else {
      item.setSku69Id(0);
    }
    if (null != skuDTO.getDiscountPrice() && skuDTO.getDiscountPrice() > 0) {
      item.setPrice(skuDTO.getDiscountPrice());
    } else if (null != skuDTO.getPrice() && skuDTO.getPrice() > 0) {
      item.setPrice(skuDTO.getPrice());
    } else {
      item.setPrice(999999);
    }
    if (null != skuDTO.getDiscountUnitPrice() && skuDTO.getDiscountUnitPrice() > 0) {
      item.setUnitPrice(skuDTO.getDiscountUnitPrice());
    } else if (null != skuDTO.getUnitPrice() && skuDTO.getUnitPrice() > 0) {
      item.setUnitPrice(skuDTO.getUnitPrice());
    } else {
      item.setUnitPrice(999999);
    }
    item.setSpecification(skuDTO.querySpecification());
    String sp = skuDTO.getUnstandardSpecification();
    if (StringUtils.isEmpty(sp) && !skuDTO.getIsSteelyardSku()) {
      sp = skuDTO.getSpecification();
    }
    item.setUnStandardSpecification(sp);
    item.setStorageType(skuDTO.getStorageType());
    item.setSaleUnitType(skuDTO.querySaleUnitType());

    int supplier = skuDTO.getSupplierId();
    SupplierDTO user = userComponent.getSupplierById(supplier);
    if (null != user) {
      item.setSupplierName(user.getCompanyName());
    }
    item.setSaleUnitStr(skuDTO.getSaleUnitStr());
    item.setSaleRange(skuDTO.getSaleRangeNum());
    item.setSkuUnit(skuDTO.getSkuUnit());
    item.setOnlineSkuUnit(skuDTO.getOnlineSkuUnit());
    item.setSkuSpecificationNum(skuDTO.getSkuSpecificationNum());
    item.setSkuSpecificationUnit(skuDTO.getSkuSpecificationUnit());
    Result<List<ItemSkuTagsDTO>> itemSkuTagsResult =
        skuService.querySkuTagsDetail(Arrays.asList(skuDTO.getSkuId()));
    if (null != itemSkuTagsResult && itemSkuTagsResult.getSuccess()
        && CollectionUtils.isNotEmpty(itemSkuTagsResult.getModule())) {
      ItemSkuTagsDTO itemSkuTagsDTO = itemSkuTagsResult.getModule().get(0);
      List<SkuTagDetail> skuTagDetails = itemSkuTagsDTO.getSkuTagDetailList();
      logger.error(JackSonUtil.getJson(skuTagDetails));
      List<String> tagContents = Lists.newArrayList();
      for (SkuTagDetail skuTagDetail : skuTagDetails) {
        // 获取线下商品标
        if ((null == skuTagDetail.getChannel() || skuTagDetail.getChannel() != SkuTagDetail.CHANNEL_ONLINE)
            && StringUtils.isNotBlank(skuTagDetail.getTagName())) {
          tagContents.add(skuTagDetail.getTagName());
        }
      }
      if (CollectionUtils.isNotEmpty(tagContents)) {
        item.setItemTagContent(tagContents);
      }
    }
    if (skuDTO.getTags() != null && skuDTO.getTags() > 0) {
      item.setItemtags(skuDTO.getTags());
    } else {
      item.setItemtags(0);
    }
    return item;
  }

  /**
   * 获取基本信息
   * 
   * @param skuDTO
   * @return
   */
  private BaseItemVO getBaseItemVOBySkuDTO(ItemSkuDTO skuDTO) {
    BaseItemVO item = new BaseItemVO();
    item.setId(skuDTO.getItemId());
    item.setTitle(skuDTO.getTitle());
    item.setSubTitle(skuDTO.getTitle());
    return item;
  }

  /**
   * 更新前查询商品基本属性，包括sku信息
   * 
   * @param itemProductDTO
   * @return
   */
  private ItemDTO getItemDTOByCommodityVO(CommodityItemVO itemVO) {
    ItemDTO item = new ItemDTO();
    item.setIntroduction(JackSonUtil.getJson(itemVO.getIntroduction()));
    item.setPicList(itemVO.getPicList());
    item.setTitle(itemVO.getTitle());
    item.setSubtitle(itemVO.getSubTitle());
    item.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    item.setCategoryId(itemVO.getCatId());
    // TODO 默认数据
    item.setShopId(1);
    item.setOwnerId(1);
    item.setInventory(1);
    item.setProductId(itemVO.getProductId());
    item.setTitletags(itemVO.getTitletags());

    if (itemVO instanceof BoomCommodityItemVO) {
      int inventory = ((BoomCommodityItemVO) itemVO).getInventory();
      if (inventory > 0) {
        item.setInventory(inventory);
      }
    }

    return item;
  }

  /**
   * 更新前查询已经输入的采购商品属性
   * 
   * @param itemProductDTO
   * @return
   */
  private List<PropertyVO> getProductPropertyInputList(ItemProductDTO itemProductDTO) {

    // 获取当前类目的属性属性值列表
    List<PropertyDTO> tmpProlist =
        categoryComponent.getPropertyDTOByCatId(itemProductDTO.getCategoryId(), false);
    if (CollectionUtils.isEmpty(tmpProlist)) {
      return Collections.emptyList();
    }
    List<ItemProductSkuDTO> skuList = itemProductDTO.getItemProductSkuList();
    ItemProductSkuDTO sku;
    if (CollectionUtils.isEmpty(skuList)) {
      return Collections.emptyList();
    }
    sku = skuList.get(0);

    // 聚合属性属性值和待修改商品的属性属性值
    return getProductPropertyInputListBySku(sku, tmpProlist);
  }

  /**
   * 发布前台商品时，根据后台商品对象查询前台商品属性列表
   * 
   * @param itemDTO
   * @return
   */
  private List<PropertyVO> getCommodityPropertyBySKUDTO(ItemSkuDTO sku, int catId,
      boolean isCommoditySku) {

    boolean isCommodityDisplayPartProperty = false;// 是否展示部分属性或者全部属性都展示
    List<PropertyDTO> propertyList = Lists.newArrayList();
    // 只展示允许展示的属性列表，并追加采购商品设置的属性值
    if (isCommodityDisplayPartProperty) {
      for (String catName : PropertyVO.COMMODITY_PROPERTY_SET) {
        propertyList.add(categoryComponent.getPropertyDTO(catId, catName, true));
      }
    } else {
      // 获取当前类目的属性属性值列表
      List<PropertyDTO> tmpProlist = categoryComponent.getPropertyDTOByCatId(catId, false);
      if (CollectionUtils.isEmpty(tmpProlist)) {
        logger.error("Category get property is null,catId:" + catId);
        return Collections.emptyList();
      }
      propertyList = tmpProlist;
    }
    // 获取已经设置的属性与属性值对
    List<PropertyVO> result = getProductPropertyInputListBySku(sku, propertyList);

    // 加工类商品直接返回属性列表即可
    if (isCommoditySku
        && SkuTypeConstants.SECONDARY_PROCESSING_SKU.equals(((ItemCommoditySkuDTO) sku)
            .getSkuType())) {

      List<PropertyDTO> propertyListnew = categoryComponent.getPropertyDTOByCatId(catId, true);
      List<PropertyVO> propertyVOList = getPropertyListByProperDTO(propertyListnew);
      Map<Integer, PropertyVO> proM = Maps.newHashMap();
      for (PropertyVO pro : result) {
        proM.put(pro.getPropertyId(), pro);
      }
      for (PropertyVO pro : propertyVOList) {
        pro.setModified(true);
        PropertyVO selected = proM.get(pro.getPropertyId());
        if (null != selected) {
          pro.setSelectedValue(selected.getSelectedValue());
        }
      }
      return propertyVOList;
    }
    return result;
  }

  /**
   * 返回当前采购商品sku属性和候选属性值，供修改时使用
   * 
   * @param sku
   * @param catPropertyList
   * @return
   */
  private List<PropertyVO> getProductPropertyInputListBySku(ItemSkuDTO sku,
      List<PropertyDTO> catPropertyList) {
    // 属性id不会重复，所以可以直接合并
    Map<String, String> allCatProperty = Maps.newHashMap();
    allCatProperty.putAll(sku.queryStandardCatPropertyValueMap());
    allCatProperty.putAll(sku.queryUnStandardCatPropertyValueMap());

    List<PropertyVO> result = Lists.newArrayList();
    for (PropertyDTO property : catPropertyList) {
      String propertyName = property.getPropertyNameAlias();
      // 忽略掉如下属性
      if (propertyName.equals("进价货币单位") || propertyName.equals("售价货币单位")
          || propertyName.equals("是否可销售") || propertyName.equals("生产厂商名称")
          || propertyName.equals("生产厂商地址") || propertyName.equals("生产厂商名称")) {
        continue;
      }

      if (sku instanceof ItemCommoditySkuDTO) {
        Short skuType = ((ItemCommoditySkuDTO) sku).getSkuType();
        if (SkuTypeConstants.ORIGINAL_GOODS_SKU.equals(skuType.shortValue())) {
          if (propertyName.equals("正常进价")) {
            continue;
          }
        }
      }
      PropertyVO tmpPropertyVO = new PropertyVO();
      int propertyId = property.getPropertyId();
      tmpPropertyVO.setPropertyId(propertyId);
      tmpPropertyVO.setPropertyName(property.getPropertyNameAlias());
      tmpPropertyVO.setInput(property.isInputProperty());
      tmpPropertyVO.setSelect(property.isSelectProperty());
      tmpPropertyVO.setRequired(property.isRequireProperty());
      tmpPropertyVO.setModified(false);

      String valueIdOrInputStr = allCatProperty.get(String.valueOf(propertyId));
      if (property.isInputProperty()) {
        ValueVO value = new ValueVO();
        if (propertyId == 24) {// 正常售价，这里有错误数据，多几位小数点后的数字
          if (StringUtils.isNotEmpty(valueIdOrInputStr) && isDecimal(valueIdOrInputStr)) {
            Integer salePrice = new BigDecimal(valueIdOrInputStr).intValue();
            value.setValue(salePrice.toString());
            tmpPropertyVO.setSelectedValue(value);
          }
        } else {
          value.setValue(valueIdOrInputStr);
          tmpPropertyVO.setSelectedValue(value);
        }
      }

      if (property.isSelectProperty()) {
        int valueId = 0;
        try {
          valueId = Integer.valueOf(valueIdOrInputStr);
        } catch (Exception e) {
        }
        if (valueId > 0) {
          String valueName = categoryComponent.getValueNameByValueId(valueId);
          ValueVO value = new ValueVO();
          value.setValueId(valueId);
          value.setValue(valueName);// 前台页面自己处理该数据
          tmpPropertyVO.setSelectedValue(value);
        }
      }
      result.add(tmpPropertyVO);
    }

    this.appentValueListUnitStr(result);
    return result;
  }

  /**
   * 仅翻译属性对象到前台可用
   * 
   * @param sku
   * @param catPropertyList
   * @return
   */
  private List<PropertyVO> getPropertyListByProperDTO(List<PropertyDTO> catPropertyList) {
    List<PropertyVO> result = Lists.newArrayList();
    if (CollectionUtils.isEmpty(catPropertyList)) {
      return result;
    }
    for (PropertyDTO property : catPropertyList) {
      PropertyVO tmpPropertyVO = new PropertyVO();
      int propertyId = property.getPropertyId();
      tmpPropertyVO.setPropertyId(propertyId);
      tmpPropertyVO.setPropertyName(property.getPropertyNameAlias());
      tmpPropertyVO.setInput(property.isInputProperty());
      tmpPropertyVO.setSelect(property.isSelectProperty());
      tmpPropertyVO.setRequired(property.isRequireProperty());
      tmpPropertyVO.setModified(true);
      if (property.isSelectProperty()
          && CollectionUtils.isNotEmpty(property.getCategoryPropertyValues())) {
        List<ValueVO> valueList = Lists.newArrayList();
        for (ValueDTO value : property.getCategoryPropertyValues()) {
          ValueVO valueVO = new ValueVO();
          valueVO.setValueId(value.getValueId());
          valueVO.setValue(value.getAlias());
          valueList.add(valueVO);
        }
        tmpPropertyVO.setValueList(valueList);
      }
      result.add(tmpPropertyVO);
    }
    return result;
  }

  /**
   * 根据前台商品vo获取非加工类商品对象
   * 
   * @param item
   * @return
   */
  private ItemCommoditySkuDTO getCommoditySkuByProductSku(CommodityItemVO item,
      ItemSkuDTO srcSkuDTO, ItemCommoditySkuDTO sku) {

    sku.setTitle(item.getTitle());
    if (null != item.getElement()) {
      sku.addElement(item.getElement());
    }
    if (null != item.getDisplaySpecifications()
        && !item.getDisplaySpecifications().equals(NULL_STRING)) {
      sku.addDisplaySpecification(item.getDisplaySpecifications());
    }
    long productSkuId = item.getSkuId();
    if (productSkuId <= 0) {
      return null;
    }

    Map<String, String> standM = srcSkuDTO.queryStandardCatPropertyValueMap();
    Map<String, String> unStandM = srcSkuDTO.queryUnStandardCatPropertyValueMap();

    List<PropertyVO> propertyList = item.getPropertyVO();
    if (CollectionUtils.isEmpty(propertyList)) {
      return null;
    }
    String pro69Value = "";
    Integer saleUnit = null;
    Short saleStatus = null;
    for (PropertyVO tmpVO : propertyList) {
      ValueVO v = tmpVO.getSelectedValue();

      if (null != v) {

        String key = String.valueOf(tmpVO.getPropertyId());
        if (standM.containsKey(key)) {
          sku.addSelectCatProperty(tmpVO.getPropertyId(), String.valueOf(v.getValueId()));
        } else if (unStandM.containsKey(key)) {
          sku.addInputCatProperty(tmpVO.getPropertyId(), v.getValue());
        }

        if (tmpVO.getPropertyId() == PropertyDTO.SKU69CODE_PROPERTY_ID) {
          pro69Value = v.getValue();
        }
        // 这里不要校验销售规格必须是三种规格之一，打开页面查询属性时，已经过滤过了
        if (tmpVO.getPropertyId() == PropertyDTO.SALEUNIT_PROPERTY_ID) {
          saleUnit = v.getValueId();
        }

        if (tmpVO.getPropertyId() == PropertyDTO.SALETYPE_PROPERTY_ID) {
          int saleType = v.getValueId();
          if (saleType == ValueDTO.SALE_TYPE_MARKET_VALUE_ID) {
            saleStatus = SaleStatusConstants.SALE_ONLY_MARKET;
          } else if (saleType == ValueDTO.SALE_TYPE_ONLINE_VALUE_ID) {
            saleStatus = SaleStatusConstants.SALE_ONLY_ONLINE;
          } else {// 设置默认值
            saleStatus = SaleStatusConstants.SALE_ONLINE_AND_MARKET;
          }
        }
      }
    }

    // 正常进价售价没有前台显示，需要发布时提取并设置到商品身上
    sku.addInputCatProperty(22, unStandM.get("22"));
    sku.addInputCatProperty(24, unStandM.get("24"));

    Date now = new Date();

    sku.setSkuType(SkuTypeConstants.ORIGINAL_GOODS_SKU);
    if (StringUtils.isNotEmpty(pro69Value) && StringUtils.isNumeric(pro69Value)) {
      sku.setSku69code(Long.valueOf(pro69Value));
    }
    sku.setSupplierId(srcSkuDTO.getSupplierId());
    sku.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    sku.setGmtCreate(now);
    sku.setGmtModified(now);
    sku.setSkuUnit(saleUnit);
    sku.setSaleStatus(saleStatus);
    sku.setSkuCode(srcSkuDTO.getSkuCode());
    sku.setSkuCount(1);// itemservice中计算详细箱规
    String priceStr = unStandM.get("24");
    if (null != priceStr && isDecimal(priceStr)) {
      int price = new BigDecimal(priceStr).intValue();
      sku.setPrice(price);
      sku.setDiscountPrice(price);
      sku.setUnitPrice(price);
      sku.setDiscountUnitPrice(price);
    } else {
      sku.setPrice(999999);
      sku.setDiscountPrice(999999);
      sku.setUnitPrice(999999);
      sku.setDiscountUnitPrice(999999);
    }
    sku.setUnitPrice(sku.getPrice());
    sku.setInventory(1);
    return sku;
  }

  /**
   * 根据前台商品vo获取加工类商品对象
   * 
   * @param item
   * @return
   */
  private ItemCommoditySkuDTO getBoomCommoditySku(BoomCommodityItemVO item) {
    ItemCommoditySkuDTO sku = new ItemCommoditySkuDTO();

    sku.setTitle(item.getTitle());
    Date now = new Date();
    sku.setSku69code(0L);// 加工类没有该属性
    sku.setSupplierId(0);
    sku.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    sku.setGmtCreate(now);
    sku.setGmtModified(now);
    if (null != item.getElement()) {
      sku.addElement(item.getElement());
    }
    if (null != item.getDisplaySpecifications()
        && !item.getDisplaySpecifications().equals(NULL_STRING)) {
      sku.addDisplaySpecification(item.getDisplaySpecifications());
    }

    sku.setInventory(item.getInventory());
    sku.setSkuCount(1);// 加工类型直接输入
    sku.setSkuType(SkuTypeConstants.SECONDARY_PROCESSING_SKU);
    sku.addSpecification(item.getSaleDescribe());
    sku.addBoomId(item.getBoomId());
    sku.setPrice(item.getPrice());
    sku.setUnitPrice(item.getPrice());
    List<PropertyVO> propertyList = item.getPropertyVO();
    if (CollectionUtils.isEmpty(propertyList)) {
      return null;
    }

    Integer saleUnit = null;
    Short saleStatus = null;

    int inputPrice = 0;
    for (PropertyVO property : propertyList) {
      ValueVO valueVO = property.getSelectedValue();
      if (null == valueVO) {
        continue;
      }

      int proId = property.getPropertyId();

      if (property.isInput()) {
        String value = valueVO.getValue();
        sku.addInputCatProperty(proId, value);

        if (proId == 24 && StringUtils.isNotEmpty(value) && isDecimal(value)) {// 售价单位
          inputPrice = Integer.valueOf(value);
        }
      } else {
        int value = valueVO.getValueId();
        sku.addSelectCatProperty(proId, String.valueOf(value));

        // 销售规格
        if (proId == PropertyDTO.SALEUNIT_PROPERTY_ID) {
          saleUnit = value;
        }

        // 销售类型
        if (proId == PropertyDTO.SALETYPE_PROPERTY_ID) {
          if (value == ValueDTO.SALE_TYPE_MARKET_VALUE_ID) {
            saleStatus = SaleStatusConstants.SALE_ONLY_MARKET;
          } else if (value == ValueDTO.SALE_TYPE_ONLINE_VALUE_ID) {
            saleStatus = SaleStatusConstants.SALE_ONLY_ONLINE;
          } else {// 设置默认值
            saleStatus = SaleStatusConstants.SALE_ONLINE_AND_MARKET;
          }
        }
      }
    }

    if (inputPrice != 0) {
      sku.setPrice(inputPrice);
      sku.setUnitPrice(inputPrice);
    } else {
      sku.setPrice(999999);
      sku.setUnitPrice(sku.getPrice());
    }
    // 加工类的销售转化类型
    sku.addSaleUnitType(SkuSaleUnitConstants.BOX);
    sku.addSpecification(item.getSaleDescribe());

    sku.setSkuUnit(saleUnit);
    sku.setSaleStatus(saleStatus);
    return sku;
  }

  /**
   * 属性列表需要单位才有真实含义
   * 
   * @param propertyVOList
   */
  private void appentValueListUnitStr(List<PropertyVO> propertyVOList) {
    if (CollectionUtils.isEmpty(propertyVOList)) {
      return;
    }
    for (PropertyVO property : propertyVOList) {
      if (null == property.getSelectedValue()) {
        continue;
      }

      ValueVO value = property.getSelectedValue();
      if (property.getPropertyId() == PropertyDTO.SALE_PROPERTY_WIDTH
          || property.getPropertyId() == PropertyDTO.SALE_PROPERTY_HEIGHT
          || property.getPropertyId() == PropertyDTO.SALE_PROPERTY_LEN) {// 销售外包装属性key
        value.setValueUnitStr("厘米");
      } else if (property.getPropertyId() == 15 || property.getPropertyId() == 16
          || property.getPropertyId() == 17) {// 外箱属性key
        value.setValueUnitStr("厘米");
      } else if (property.getPropertyId() == 19) {// 保质期属性key
        value.setValueUnitStr("天");
      } else if (property.getPropertyId() == 26 || property.getPropertyId() == 32) {// 最小起定量、1单位重量
        value.setValueUnitStr("克");
      } else if (property.getPropertyId() == 20 || property.getPropertyId() == 21) {// 进销税、项消税
        value.setValueUnitStr("万分之");
      }
    }
  }

  /**
   * 取供应商人员id
   * 
   * @param supplierName
   * @return
   */
  private int getSupplierId(String supplierName) {
    BackGroundUserDTO user = userComponent.getUserByName(supplierName);
    if (null == user || !UserTypeConstants.USER_SUPPLIER.equals(user.getUserType())) {
      logger.error("Get supplierId is null,input name:" + supplierName);
      return -1;
    }
    return user.getUserId();
  }

  /**
   * 根据手机号查询
   * 
   * @param supplierPhone
   * @return
   */
  private int getSupplierIdByPhone(String supplierPhone) {
    long phone = 0;
    if (StringUtils.isNotEmpty(supplierPhone) && StringUtils.isNumeric(supplierPhone)) {
      phone = Long.valueOf(supplierPhone);
    } else {
      logger.error("Get supplierId by phone is null,input phone:" + supplierPhone);
      return -1;
    }
    BackGroundUserDTO user = userComponent.getUserByPhone(phone);
    if (null == user || !UserTypeConstants.USER_SUPPLIER.equals(user.getUserType())) {
      logger.error("Get supplierId by phone is null,input phone:" + supplierPhone);
      return -1;
    }
    return user.getUserId();
  }

  /**
   * 查询供应商名称
   * 
   * @param supplierId
   * @return
   */
  private String getSupplierName(int supplierId) {
    BackGroundUserDTO user = userComponent.getUserById(supplierId);
    if (null == user || !UserTypeConstants.USER_SUPPLIER.equals(user.getUserType())) {
      logger.error("Get supplierName by id is null,input id:" + supplierId);
      return "";
    }
    return user.getName();
  }

  /**
   * 取采购人员id
   * 
   * @param supplierId
   * @return
   */
  private int getSupplierAgentId(int supplierId) {
    return 42;// 一期固定
  }

  /**
   * 取采购人员名称
   * 
   * @param supplierId
   * @return
   */
  private String getSupplierAgentName(int supplierId) {
    return "梓木";// 一期固定
  }

  /**
   * 根据系统产生进售价对应关系列表，获取前台进售价列表数据
   * 
   * @param dbRelationList
   * @return
   */
  private List<SkuRelationVO> getRelationVOList(List<ItemSkuRelationDTO> dbRelationList) {
    if (null == dbRelationList) {
      return Collections.emptyList();
    }

    List<SkuRelationVO> result = Lists.newArrayList();
    for (ItemSkuRelationDTO tmp : dbRelationList) {
      SkuRelationVO relation = getSkuRelationVOByItemSkuRelationDTO(tmp);
      if (null != relation) {
        result.add(relation);
      }
    }
    return result;
  }

  /**
   * 根据系统产生进售价对应关系，获取前台进售价数据
   * 
   * @param dbRelationList
   * @param isDiscount:是否促销进售价
   * @return
   */
  private SkuRelationVO getSkuRelationVOByItemSkuRelationDTO(ItemSkuRelationDTO dbRelation) {
    if (null == dbRelation || null == dbRelation.getItemCommoditySku()) {
      logger.error("Get skurelation getItemCommoditySku is null,skurelationId:"
          + dbRelation.getId());
      return null;
    }
    SkuRelationVO result = new SkuRelationVO();
    ItemProductSkuDTO pskuDTO = dbRelation.getItemProductSku();
    if (null == pskuDTO) {
      return getBoomSkuRelationVOByItemSkuRelationDTO(dbRelation);
    }

    ItemCommoditySkuDTO cskuDTO = dbRelation.getItemCommoditySku();
    if (cskuDTO.getStatus() <= 0) {// 未审核通过的sku，不能展示进售价关系
      logger.error("Get skurelation getItemCommoditySku status error,skurelationId:"
          + dbRelation.getId());
      return null;
    }
    result.setPskuId(pskuDTO.getSkuId());
    result.setCskuId(cskuDTO.getSkuId());
    result.setSkuCode(cskuDTO.getSkuCode());
    result.setTitle(cskuDTO.getTitle());
    result.setId(dbRelation.getId());

    Short saleUnitType = cskuDTO.querySaleUnitType();

    // 销售规格属性id
    int saleunitPropertyId = PropertyDTO.SALEUNIT_PROPERTY_ID;
    if (null == cskuDTO.queryStandardCatPropertyValueMap()
        || cskuDTO.queryStandardCatPropertyValueMap().isEmpty()
        || null == cskuDTO.queryStandardCatPropertyValueMap().get(
            String.valueOf(saleunitPropertyId))) {
      logger.error("Get skurelation saleunitPropertyValueId is null,skurelationId:"
          + dbRelation.getId());
      return null;
    }
    // 销售规格 设置的 属性值id
    int saleunitPropertyValueId =
        Integer.valueOf(cskuDTO.queryStandardCatPropertyValueMap().get(
            String.valueOf(saleunitPropertyId)));
    // 件装数 属性id
    int countPropertyId = PropertyDTO.COUNT_PROPERTY_ID;

    // 件装数设置的具体数字
    int countValueNum = 0;

    String countInfo =
        cskuDTO.queryUnStandardCatPropertyValueMap().get(String.valueOf(countPropertyId));
    if (StringUtils.isNotEmpty(countInfo) && isDecimal(countInfo)) {
      countValueNum = Integer.valueOf(countInfo);
    }

    // 件装规格 属性id
    int gourpPropertyId = PropertyDTO.GOURP_PROPERTY_ID;
    if (null == cskuDTO.queryUnStandardCatPropertyValueMap().get(String.valueOf(gourpPropertyId))) {
      logger.error("Get skurelation gourpPropertyId is null,skurelationId:" + dbRelation.getId());
    }

    // 件装规格的具体数字,也就是一个小组里面包含几个最小单位
    int gourpValueNum =
        Integer.valueOf(cskuDTO.queryUnStandardCatPropertyValueMap().get(
            String.valueOf(gourpPropertyId)));

    // 销售规格 设置的 属性值名称
    String saleUnitValueName = categoryComponent.getValueNameByValueId(saleunitPropertyValueId);

    UnitVO packageUnit = new UnitVO();
    packageUnit.setUnitId(saleunitPropertyValueId);
    packageUnit.setUnitStr(saleUnitValueName);
    result.setPackageUnit(packageUnit);
    result.setSaleUnit(packageUnit);
    Integer dbCost = dbRelation.getBoxCost();
    float packageTotalCost = 0;
    if (null != dbCost && dbCost > 0) {
      packageTotalCost =
          new BigDecimal(dbCost).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN)
              .floatValue();
    }
    result.setPackageTotalCost(packageTotalCost);

    if (saleUnitType == SkuSaleUnitConstants.BOX) {// 采购与销售完全相同
      result.setPackageUnitSize(1);
      result.setPackageUnitCost(packageTotalCost);
      result.setSaleSize(1);
      result.setSaleUnitCost(packageTotalCost);
    } else if (saleUnitType == SkuSaleUnitConstants.GROUP) {// 按组销售
      if ((countValueNum % gourpValueNum) > 0) {
        logger.error("Group sale item, countValueNum % gourpValueNum) > 0." + dbRelation.getId());
        return null;// 按组销售，件装数不能整除件装规格
      }
      int packageUnitSize = countValueNum/gourpValueNum;
      if (packageUnitSize <= 0) {
        logger.error("Group sale item, packageUnitSize <= 0." + dbRelation.getId());
        return null;// 按组销售，件装数整除件装规格取值错误
      }
      result.setPackageUnitSize(packageUnitSize);
      float packageUnitCost =
          new BigDecimal(Float.toString(packageTotalCost)).divide(new BigDecimal(packageUnitSize), 2,
              BigDecimal.ROUND_HALF_EVEN).floatValue();
      result.setPackageUnitCost(packageUnitCost);
      result.setSaleSize(1);
      result.setSaleUnitCost(packageUnitCost);
    } else if (saleUnitType == SkuSaleUnitConstants.UNIT) {// 按件销售
      if (countValueNum <= 0) {
        logger.error("Group sale item, countValueNum <= 0." + dbRelation.getId());
        return null;// 按件销售，件装数 数字小于0
      }
      result.setPackageUnitSize(countValueNum);
      float packageUnitCost =
          new BigDecimal(Float.toString(packageTotalCost)).divide(new BigDecimal(countValueNum), 2,
              BigDecimal.ROUND_HALF_EVEN).floatValue();
      result.setPackageUnitCost(packageUnitCost);
      result.setSaleSize(1);
      result.setSaleUnitCost(packageUnitCost);
    } else if (saleUnitType == SkuSaleUnitConstants.NATURAL) {// 按自然单位销售
      if (countValueNum <= 0) {
        logger.error("Group sale item, countValueNum <= 0." + dbRelation.getId());
        return null;// 按件销售，件装数 数字小于0
      }
      result.setPackageUnitSize(countValueNum);// 称重类的都是kg进货500g销售，所以箱规×1
      float packageUnitCost =
          new BigDecimal(Float.toString(packageTotalCost)).divide(new BigDecimal(countValueNum), 2,
              BigDecimal.ROUND_HALF_EVEN).floatValue();
      result.setPackageUnitCost(packageUnitCost);
      result.setSaleSize(1);// 代表1斤
      result.setSaleUnitCost(packageUnitCost);
      UnitVO saleUnit = new UnitVO();
      saleUnit.setUnitId(3);
      saleUnit.setUnitStr("公斤");
      result.setSaleUnit(saleUnit);
      result.setPackageUnit(saleUnit);
    }

    Date buyStrat = dbRelation.getBuyStart();
    Date buyEnd = dbRelation.getBuyEnd();
    Date saleStrat = dbRelation.getSaleStart();
    Date saleEnd = dbRelation.getSaleEnd();

    SimpleDateFormat sf = new SimpleDateFormat(SkuRelationVO.DATE_FORMAT);
    if (null != buyStrat) {
      result.setBuyStart(sf.format(buyStrat));
      result.setBuyStartTime(buyStrat.getTime());
    }
    if (null != buyEnd) {
      result.setBuyEnd(sf.format(buyEnd));
      result.setBuyEndTime(buyEnd.getTime());
    }
    if (null != saleStrat) {
      result.setSaleStart(sf.format(saleStrat));
      result.setSaleStartTime(saleStrat.getTime());
    }
    if (null != saleEnd) {
      result.setSaleEnd(sf.format(saleEnd));
      result.setSaleEndTime(saleEnd.getTime());
    }

    int avgCost = dbRelation.getAvgCost();
    if (avgCost > 0) {
      result.setAvgCost(new BigDecimal(avgCost).divide(new BigDecimal(100), 2,
          BigDecimal.ROUND_HALF_EVEN).floatValue());
    } else {
      result.setAvgCost(new BigDecimal(Float.toString(packageTotalCost)).divide(
          new BigDecimal(result.getPackageUnitSize()), 2).floatValue());
    }

    Integer price = dbRelation.getPrice();// 销售价格
    float salePrice = 0;
    if (null != price && price > 0) {
      salePrice =
          new BigDecimal(price).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN)
              .floatValue();
    }
    result.setSalePrice(salePrice);

    float grossMargin = 0;
    if (salePrice != 0) {
      grossMargin =
          ((new BigDecimal(Float.toString(salePrice)).subtract(new BigDecimal(result
              .getSaleUnitCost()))).multiply(new BigDecimal(100))).divide(
              new BigDecimal(Float.toString(salePrice)), 2, BigDecimal.ROUND_HALF_EVEN)
              .floatValue();
    }
    result.setGrossMargin(grossMargin);
    return result;
  }

  /**
   * 根据系统产生进售价对应关系，获取前台boom单商品进售价数据
   * 
   * @param dbRelationList
   * @param isDiscount:是否促销进售价
   * @return
   */
  private SkuRelationVO getBoomSkuRelationVOByItemSkuRelationDTO(ItemSkuRelationDTO dbRelation) {
    if (null == dbRelation || null == dbRelation.getItemCommoditySku()) {
      logger.error("Get skurelation getItemCommoditySku is null,skurelationId:"
          + dbRelation.getId());
      return null;
    }
    SkuRelationVO result = new SkuRelationVO();
    ItemProductSkuDTO pskuDTO = dbRelation.getItemProductSku();
    ItemCommoditySkuDTO cskuDTO = dbRelation.getItemCommoditySku();
    if (cskuDTO.getStatus() <= 0) {// 未审核通过的sku，不能展示进售价关系
      logger.error("Get skurelation getItemCommoditySku status error,skurelationId:"
          + dbRelation.getId());
    }
    if (null != pskuDTO) {
      result.setPskuId(pskuDTO.getSkuId());
    }
    result.setCskuId(cskuDTO.getSkuId());
    result.setSkuCode(cskuDTO.getSkuCode());
    result.setTitle(cskuDTO.getTitle());
    result.setId(dbRelation.getId());

    // 销售规格属性id
    int saleunitPropertyId = PropertyDTO.SALEUNIT_PROPERTY_ID;
    if (null == cskuDTO.queryStandardCatPropertyValueMap()
        || cskuDTO.queryStandardCatPropertyValueMap().isEmpty()
        || null == cskuDTO.queryStandardCatPropertyValueMap().get(
            String.valueOf(saleunitPropertyId))) {
      logger.error("Get skurelation saleunitPropertyValueId is null,skurelationId:"
          + dbRelation.getId());
      return null;
    }
    // 销售规格 设置的 属性值id
    int saleunitPropertyValueId =
        Integer.valueOf(cskuDTO.queryStandardCatPropertyValueMap().get(
            String.valueOf(saleunitPropertyId)));


    // 销售规格 设置的 属性值名称
    String saleUnitValueName = categoryComponent.getValueNameByValueId(saleunitPropertyValueId);

    UnitVO saleUnit = new UnitVO();
    saleUnit.setUnitId(saleunitPropertyValueId);
    saleUnit.setUnitStr(saleUnitValueName);
    result.setSaleUnit(saleUnit);

    Integer dbCost = dbRelation.getBoxCost();
    float packageTotalCost = 0;
    if (null != dbCost && dbCost > 0) {
      packageTotalCost =
          new BigDecimal(dbCost).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN)
              .floatValue();
    }
    result.setPackageTotalCost(packageTotalCost);
    result.setPackageUnitCost(packageTotalCost);
    result.setSaleSize(1);
    result.setSaleUnitCost(packageTotalCost);

    Date buyStrat = dbRelation.getBuyStart();
    Date buyEnd = dbRelation.getBuyEnd();
    Date saleStrat = dbRelation.getSaleStart();
    Date saleEnd = dbRelation.getSaleEnd();

    SimpleDateFormat sf = new SimpleDateFormat(SkuRelationVO.DATE_FORMAT);
    if (null != buyStrat) {
      result.setBuyStart(sf.format(buyStrat));
    }
    if (null != buyEnd) {
      result.setBuyEnd(sf.format(buyEnd));
    }
    if (null != saleStrat) {
      result.setSaleStart(sf.format(saleStrat));
    }
    if (null != saleEnd) {
      result.setSaleEnd(sf.format(saleEnd));
    }

    int avgCost = dbRelation.getAvgCost();
    if (avgCost > 0) {
      result.setAvgCost(new BigDecimal(avgCost).divide(new BigDecimal(100), 2,
          BigDecimal.ROUND_HALF_EVEN).floatValue());
    } else {
      result.setAvgCost(result.getPackageTotalCost());
    }

    Integer price = dbRelation.getPrice();// 销售价格
    float salePrice = 0;
    if (null != price && price > 0) {
      salePrice =
          new BigDecimal(price).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN)
              .floatValue();
    }
    result.setSalePrice(salePrice);

    float grossMargin = 0;
    if (salePrice != 0) {
      grossMargin =
          ((new BigDecimal(Float.toString(salePrice)).subtract(new BigDecimal(result
              .getSaleUnitCost()))).multiply(new BigDecimal(100))).divide(
              new BigDecimal(Float.toString(salePrice)), 2, BigDecimal.ROUND_HALF_EVEN)
              .floatValue();
    }
    result.setGrossMargin(grossMargin);
    return result;
  }

  /**
   * 根据前台进售价对应关系，获取系统产生进售价数据
   * 
   * @param dbRelation
   * @param isDiscount
   * @return
   */
  private Result<ItemSkuRelationDTO> getSkuRelationDTOByItemSkuRelationVO(SkuRelationVO relationVO,
      boolean isEmergency, boolean isUpdateBuyPrice, boolean isUpdateSale) {
    if (null == relationVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "update parameter is null");
    }

    Result<ItemSkuRelationDTO> tmpSkuResult = skuService.querySkuRelation(relationVO.getId());
    if (null == tmpSkuResult || !tmpSkuResult.getSuccess()) {
      logger.error("query skurelation is null,id:" + relationVO.getId());
      return Result.getErrDataResult(ServerResultCode.SKU_RELATION_INFO_NOT_EXIT,
          "update parameter query db is null");
    }

    ItemSkuRelationDTO result = tmpSkuResult.getModule();
    SimpleDateFormat sf = new SimpleDateFormat(SkuRelationVO.DATE_FORMAT);
    float updatePrice = relationVO.getSalePrice();
    float boxCost = relationVO.getPackageTotalCost();
    int updatePriceDb =
        new BigDecimal(Float.toString(updatePrice)).multiply(new BigDecimal(100)).intValue();
    if (updatePriceDb <= 0) {
      logger.error("update skurelation price error,id:" + relationVO.getId());
      return Result.getErrDataResult(ServerResultCode.SKU_RELATION_PRICE_ERROR,
          "update price is wrong" + " boxcost price:" + boxCost + " Sale price:" + updatePrice);
    }
    int boxCostDb =
        new BigDecimal(Float.toString(boxCost)).multiply(new BigDecimal(100)).intValue();

    Integer pcProportion = result.getPcProportion();
    if (null != pcProportion && pcProportion > 0) {
      result.setbPrice(new BigDecimal(boxCostDb).multiply(new BigDecimal(1000))
          .divide(new BigDecimal(pcProportion), 2).intValue());
    } else {
      result.setbPrice(boxCostDb);
    }

    result.setPrice(updatePriceDb);
    result.setBoxCost(boxCostDb);

    String buyStart = relationVO.getBuyStart();
    String buyEnd = relationVO.getBuyEnd();
    String saleStart = relationVO.getSaleStart();
    String saleEnd = relationVO.getSaleEnd();

    Date buytartTime = null;
    Date buyEndTime = null;
    Date saleStartTime = null;
    Date saleEndTime = null;
    try {
      if (null != relationVO.getBuyStartTime()) {
        buytartTime = new Date(relationVO.getBuyStartTime());
      } else if (StringUtils.isNotEmpty(buyStart)) {
        buytartTime = sf.parse(buyStart);
      }

      if (null != relationVO.getBuyEndTime()) {
        buyEndTime = new Date(relationVO.getBuyEndTime());
      } else if (StringUtils.isNotEmpty(buyEnd)) {
        buyEndTime = sf.parse(buyEnd);
      }

      if (null != relationVO.getSaleStartTime()) {
        saleStartTime = new Date(relationVO.getSaleStartTime());
      } else if (StringUtils.isNotEmpty(saleStart)) {
        saleStartTime = sf.parse(saleStart);
      }

      if (null != relationVO.getSaleEndTime()) {
        saleEndTime = new Date(relationVO.getSaleEndTime());
      } else if (StringUtils.isNotEmpty(saleEnd)) {
        saleEndTime = sf.parse(saleEnd);
      }
    } catch (Exception e) {
      logger.error("Input relation time format is error,id:" + relationVO.getId());
      return Result.getErrDataResult(ServerResultCode.SKU_RELATION_START_TIME_FORMAT_ERROR,
          "update start time format mush format 16/09/13 or empty." + " Input buystart " + buyStart
              + " Input buyEnd " + buyEnd + " Input saleStart " + saleStart + " Input saleEnd "
              + saleEnd);
    }
    if (isEmergency) {
      if (saleEndTime.before(this.getTodayAMTime()) || buyEndTime.before(this.getTodayAMTime())) {
        return Result.getErrDataResult(ServerResultCode.SKU_RELATION_START_TIME_BEFORE_TODAY_ERROR,
            "进价截止时间、售价截止时间必须大于当前时间");
      }
      if (isUpdateBuyPrice) {
        buytartTime = new Date();
      }
      if (isUpdateSale) {
        saleStartTime = new Date();
      }
    } else {
      if (saleEndTime.before(saleStartTime) || buyEndTime.before(buytartTime)) {
        return Result.getErrDataResult(ServerResultCode.SKU_RELATION_START_TIME_BEFORE_TODAY_ERROR,
            "进价售价截止时间必须大于开始时间");
      }
    }
    result.setSaleStart(saleStartTime);
    result.setSaleEnd(saleEndTime);
    result.setBuyStart(buytartTime);
    result.setBuyEnd(buyEndTime);
    if (relationVO.getPackageUnitSize() > 0) {
      result.setAvgCost(new BigDecimal(relationVO.getPackageTotalCost())
          .divide(new BigDecimal(relationVO.getPackageUnitSize()), 2).multiply(new BigDecimal(100))
          .intValue());
    }
    return Result.getSuccDataResult(result);
  }

  /**
   * 翻译前台使用的列表对象
   * 
   * @param productSkuDTOList
   * @return
   */
  private List<ProductListVO> getProductListVOList(List<ItemProductSkuDTO> productSkuDTOList) {
    List<ProductListVO> result = Lists.newArrayList();
    if (CollectionUtils.isEmpty(productSkuDTOList)) {
      return result;
    }
    Map<Integer, ProductListVO> supplierProductM = Maps.newHashMap();
    Map<Integer, ProductListVO> supplierAgentProductM = Maps.newHashMap();

    Set<Integer> userSet = Sets.newHashSet();
    for (ItemProductSkuDTO productDTO : productSkuDTOList) {
      ProductListVO tmpResult = new ProductListVO();
      tmpResult.setProductCode(productDTO.getSkuCode());
      tmpResult.setProductId(productDTO.getItemId());
      tmpResult.setSkuId(productDTO.getSkuId());
      tmpResult.setTitle(productDTO.getTitle());
      Short status = productDTO.getStatus();
      tmpResult.setStatusCode(status);
      setStatusStr(productDTO, tmpResult);
      Integer agentId = productDTO.getSupplierAgent();
      if (null != agentId) {
        tmpResult.setPurchaseAgentId(agentId);
        userSet.add(agentId);
      }

      Integer supplierId = productDTO.getSupplierId();
      if (null != supplierId) {
        tmpResult.setSupplierId(supplierId);
        userSet.add(supplierId);
      }

      supplierProductM.put(productDTO.getSupplierId(), tmpResult);
      supplierAgentProductM.put(productDTO.getSupplierAgent(), tmpResult);

      result.add(tmpResult);
    }

    Map<Integer, String> idName = Maps.newHashMap();
    List<BackGroundUserDTO> userList =
        userComponent.getUserByIdList(new ArrayList<Integer>(userSet));
    for (BackGroundUserDTO user : userList) {
      idName.put(user.getUserId(), user.getName());
    }

    for (ProductListVO tmpResult : result) {
      int supplierId = tmpResult.getSupplierId();
      if (supplierId > 0) {
        tmpResult.setSupplierName(idName.get(supplierId));
      }
      int purchaseAgentId = tmpResult.getPurchaseAgentId();
      if (purchaseAgentId > 0) {
        tmpResult.setPurchaseAgentName(idName.get(purchaseAgentId));
      }
    }
    return result;
  }

  private void setStatusStr(ItemProductSkuDTO productDTO, ProductListVO tmpResult) {
    Short status = productDTO.getStatus();

    if (null == productDTO.getStatus()
        || ItemStatusConstants.ITEM_STATUS_PRE.shortValue() == status.shortValue()) {
      tmpResult.setStatus("预发布状态");
    } else if (ItemStatusConstants.ITEM_STATUS_CHECKED_NOTPASS.shortValue() == status.shortValue()) {
      tmpResult.setStatus("审核不通过状态");
    } else if (ItemStatusConstants.ITEM_STATUS_WAITCHECK.shortValue() == status.shortValue()) {
      tmpResult.setStatus("待审核状态");
    } else if (ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES.shortValue() == status
        .shortValue()) {
      tmpResult.setStatus("审核通过状态");
    } else if (ItemStatusConstants.ITEM_STATUS_PASS_AND_DOWN_SHELVES.shortValue() == status
        .shortValue()) {
      tmpResult.setStatus("审核通过状态");
    } else if (ItemStatusConstants.ITEM_STATUS_DRAFT.shortValue() == status.shortValue()) {
      tmpResult.setStatus("草稿状态");
    } else {
      tmpResult.setStatus("其它状态");
    }
  }

  /**
   * 翻译成后台可用对象
   * 
   * @param excelItemList
   * @return
   */
  private List<Map<String, Object>> getProductList(List<ExcelItem> excelItemList, boolean isTest,
      String imgBasePath, boolean displayImgError) {
    List<Map<String, Object>> result = Lists.newArrayList();
    int index = 2;
    for (ExcelItem itemTmp : excelItemList) {// 取掉页头数据
      String printlog = "";
      String printdebug = "";
      index++;
      List<String> propertyList = itemTmp.getProperty();
      if (propertyList.size() < 30) {
        continue;// 默认错误数据
      }
      String itemName = propertyList.get(1);
      String oldName = propertyList.get(1);
      String item69Code = propertyList.get(12);// 69码，外部条码
      if (StringUtils.isNotEmpty(itemName)) {
        if (itemName.length() > 32) {
          itemName = itemName.substring(0, 32);
        }
      } else {
        // String tmplog = ("第[" + index + "]个商品标题有错误，无法处理，excel内容为：" + propertyList);
        // printlog = printlog + tmplog + "<br/>";
        // logger.error(tmplog);
        // Map<String, Object> tmpResult = Maps.newHashMap();
        // tmpResult.put("printlog", printlog);
        // tmpResult.put("printdebug", printdebug);
        // if (isTest) {
        // result.add(tmpResult);
        // }
        continue;
      }

      ItemProductDTO item = null;
      // 优先比较69码
      if (StringUtils.isNotEmpty(item69Code) && isDecimal(item69Code)) {
        item = this.getItemProductDTOBy69Code(Long.valueOf(item69Code));
      }
      if (null == item) {
        item = this.getItemProductDTOByTitle(itemName);
      }

      ItemProductSkuDTO sku = null;
      boolean isupdate = false;
      if (null == item) {
        item = new ItemProductDTO();
        sku = new ItemProductSkuDTO();
      } else {
        sku = item.getItemProductSkuList().get(0);
        isupdate = true;
      }

      String catName = propertyList.get(0);
      String subName = propertyList.get(2);
      String isInput = propertyList.get(5);// 是否进出口
      String brand = propertyList.get(6);// 品牌
      String county = propertyList.get(10);// 国家
      String producer = propertyList.get(11);// 产地

      String count = propertyList.get(13);// 件装数
      String countUnit = propertyList.get(14);// 件装单位属性

      String naturalUnit = propertyList.get(15);// 称重类自然属性
      String naturalRate = propertyList.get(16);// 称重比例

      String group = propertyList.get(17);// 件装规格
      String gourpUnit = propertyList.get(18);// 件装属性
      String buyUnit = propertyList.get(19);// 采购规格
      String saleUnit = propertyList.get(20);// 销售规格
      String saleLength = propertyList.get(21);// 销售外包装长度
      String saleWidth = propertyList.get(22);// 销售外包装宽度
      String saleHeigth = propertyList.get(23);// 销售外包装高度
      String boxLength = propertyList.get(24);// 外箱长度
      String boxWidth = propertyList.get(25);// 外箱宽度
      String boxHeigth = propertyList.get(26);// 外箱高度
      String quality = propertyList.get(27);// 质量等级
      String qualityDay = propertyList.get(28);// 保质期
      String outputTax = propertyList.get(29);// 销项税
      String inputTax = propertyList.get(30);// 进项税
      String inputprice = propertyList.get(31);// 正常进价
      String price = propertyList.get(33);// 正常售价
      String mixSize = propertyList.get(35);// 最小起订量

      String isTaxFree = propertyList.get(36);// 是否免农业税
      String temperature = propertyList.get(37);// 温度条件标识
      String phone = propertyList.get(38);// 联系人
      String returnPurchase = propertyList.get(39);// 是否可以退货
      String saleType = propertyList.get(40);// 销售类型
      String saleHeight = propertyList.get(41);// 增加1个单位重量
      String priceType = propertyList.get(42);// 标价类型
      String priceTag = propertyList.get(43);// 价签类型
      String tags = propertyList.get(44);// 商品各种标签
      String orderAddress = propertyList.get(45);// 定货地址
      CategoryDTO cat = categoryComponent.getCategoryDTO(catName);
      if (null != cat && null != cat.getCatId()) {
        item.setCategoryId(cat.getCatId());
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [商品类目] 错误，无法处理，excel内容：[" + catName + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        Map<String, Object> tmpResult = Maps.newHashMap();
        tmpResult.put("printlog", printlog);
        tmpResult.put("printdebug", printdebug);
        if (isTest) {
          result.add(tmpResult);
        }
        continue;
      }
      int catId = cat.getCatId();
      item.setTitle(itemName);

      if (StringUtils.isNotEmpty(subName)) {
        if (subName.length() > 8) {
          subName = subName.substring(0, 8);
        }
        item.setSubtitle(subName);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [商品名称 简称] 错误，已经替换为商品主标题，excel内容：[" + subName + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
        item.setSubtitle(itemName);
      }
      int isInputId = categoryComponent.getPropertyId(catId, "进口/外采品");
      int valueId = categoryComponent.getValueId(catId, isInputId, isInput);
      if (valueId > 0) {
        sku.addSelectCatProperty(isInputId, String.valueOf(valueId));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]   [进口/外采品] 错误，无法处理，excel内容：[" + isInput + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }

      int brandId = categoryComponent.getPropertyId(catId, "品牌");
      if (StringUtils.isNotEmpty(brand) && brandId > 0) {
        sku.addInputCatProperty(brandId, brand);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [品牌] 错误，暂时留空，excel内容：[" + brand + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int countyId = categoryComponent.getPropertyId(catId, "国家");
      if (StringUtils.isNotEmpty(county) && countyId > 0) {
        sku.addInputCatProperty(countyId, county);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [国家] 错误，暂时留空，excel内容：[" + county + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int producerId = categoryComponent.getPropertyId(catId, "产地");
      if (StringUtils.isNotEmpty(producer) && producerId > 0) {
        sku.addInputCatProperty(producerId, producer);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [产地] 错误，暂时留空，excel内容：[" + producer + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      if (StringUtils.isNotEmpty(item69Code)) {
        int item69CodeId = categoryComponent.getPropertyId(catId, "外部条码");
        if (StringUtils.isNotEmpty(item69Code) && item69CodeId > 0 && get69Code(item69Code) > 0) {
          sku.addInputCatProperty(item69CodeId, item69Code);
          sku.setSku69code(get69Code(item69Code));
        } else {
          sku.addInputCatProperty(item69CodeId, "0");
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]  [69码] 错误，转化为0，excel内容：[" + item69Code + "]");
          printlog = printlog + tmplog + "<br/>";
          logger.error(tmplog);
        }
      }

      int countId = categoryComponent.getPropertyId(catId, "件装数");
      if (StringUtils.isNotEmpty(count) && countId > 0 && isDecimal(count)) {
        sku.addInputCatProperty(countId, count);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [件装数] 错误，无法处理，excel内容：[" + count + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }
      int countUnitId = categoryComponent.getPropertyId(catId, "件装单位属性");
      int countUnitValue = categoryComponent.getValueId(catId, countUnitId, countUnit);
      if (countUnitValue > 0 && StringUtils.isNotEmpty(countUnit)) {
        sku.addSelectCatProperty(countUnitId, String.valueOf(countUnitValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [件装单位属性] 错误，无法处理,后续属性无法校验，excel内容：["
                + countUnit + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        Map<String, Object> tmpResult = Maps.newHashMap();
        tmpResult.put("printlog", printlog);
        tmpResult.put("printdebug", printdebug);
        if (isTest) {
          result.add(tmpResult);
        }
        continue;
      }

      int naturalUnitValue = 0;
      Float naturalRateNum = (float) 0;
      if (StringUtils.isNotEmpty(naturalUnit) && StringUtils.isNotEmpty(naturalRate)
          && isDecimal(naturalRate)) {
        int naturalRateId = categoryComponent.getPropertyId(catId, "称重比例");
        int naturalUnitId = categoryComponent.getPropertyId(catId, "称重类自然属性");
        naturalUnitValue = categoryComponent.getValueId(catId, naturalUnitId, naturalUnit);
        naturalRateNum = Float.valueOf(naturalRate);
        if (StringUtils.isNotEmpty(naturalRate) && naturalRateNum > 0 && naturalUnitValue > 0
            && naturalRateId > 0) {
          sku.addInputCatProperty(naturalRateId, naturalRate);
          sku.addSelectCatProperty(naturalUnitId, String.valueOf(naturalUnitValue));
        }
      }

      int gourpId = categoryComponent.getPropertyId(catId, "件装规格");
      if (StringUtils.isNotEmpty(group) && gourpId > 0 && isDecimal(group)) {
        sku.addInputCatProperty(gourpId, group);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [件装规格] 错误，无法处理，excel内容：[" + group + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }
      int gourpUnitId = categoryComponent.getPropertyId(catId, "件装属性");
      int gourpUnitValue = categoryComponent.getValueId(catId, gourpUnitId, gourpUnit);
      if (gourpUnitValue > 0 && StringUtils.isNotEmpty(gourpUnit)) {
        sku.addSelectCatProperty(gourpUnitId, String.valueOf(gourpUnitValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [件装属性] 错误，无法处理，excel内容：[" + gourpUnit + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        Map<String, Object> tmpResult = Maps.newHashMap();
        tmpResult.put("printlog", printlog);
        tmpResult.put("printdebug", printdebug);
        if (isTest) {
          result.add(tmpResult);
        }
        continue;
      }
      int buyUnitId = categoryComponent.getPropertyId(catId, "采购规格");
      int buyUnitValue = categoryComponent.getValueId(catId, buyUnitId, buyUnit);
      if (buyUnitValue > 0 && StringUtils.isNotEmpty(buyUnit)) {
        sku.addSelectCatProperty(buyUnitId, String.valueOf(buyUnitValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [采购规格] 错误，无法处理，excel内容：[" + buyUnit + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        Map<String, Object> tmpResult = Maps.newHashMap();
        tmpResult.put("printlog", printlog);
        tmpResult.put("printdebug", printdebug);
        if (isTest) {
          result.add(tmpResult);
        }
        continue;
      }

      int saleUnitId = categoryComponent.getPropertyId(catId, "销售规格");
      int saleUnitValue = categoryComponent.getValueId(catId, saleUnitId, saleUnit);
      if (saleUnitValue > 0 && StringUtils.isNotEmpty(saleUnit)) {
        sku.addSelectCatProperty(saleUnitId, String.valueOf(saleUnitValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [销售规格] 错误，无法处理，excel内容：[" + saleUnit + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        Map<String, Object> tmpResult = Maps.newHashMap();
        tmpResult.put("printlog", printlog);
        tmpResult.put("printdebug", printdebug);
        if (isTest) {
          result.add(tmpResult);
        }
        continue;
      }

      int saleLengthId = categoryComponent.getPropertyId(catId, "销售外包装长度");
      if (StringUtils.isNotEmpty(saleLength) && saleLengthId > 0 && isDecimal(saleLength)) {
        sku.addInputCatProperty(saleLengthId, saleLength);
      } else {
        sku.addInputCatProperty(saleLengthId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [销售外包装长度] 错误，替换为0，excel内容：[" + saleLength + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int saleWidthId = categoryComponent.getPropertyId(catId, "销售外包装宽度");
      if (StringUtils.isNotEmpty(saleWidth) && saleWidthId > 0 && isDecimal(saleWidth)) {
        sku.addInputCatProperty(saleWidthId, saleWidth);
      } else {
        sku.addInputCatProperty(saleWidthId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [销售外包装宽度] 错误，替换为0，excel内容：[" + saleWidth + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int saleHeigthId = categoryComponent.getPropertyId(catId, "销售外包装高度");
      if (StringUtils.isNotEmpty(saleHeigth) && saleHeigthId > 0 && isDecimal(saleHeigth)) {
        sku.addInputCatProperty(saleHeigthId, saleHeigth);
      } else {
        sku.addInputCatProperty(saleHeigthId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [销售外包装高度] 错误，替换为0，excel内容：[" + saleHeigth + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int boxLengthId = categoryComponent.getPropertyId(catId, "外箱长度");
      if (StringUtils.isNotEmpty(boxLength) && boxLengthId > 0 && isDecimal(boxLength)) {
        sku.addInputCatProperty(boxLengthId, boxLength);
      } else {
        sku.addInputCatProperty(boxLengthId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [外箱长度] 错误，替换为0，excel内容：[" + boxLength + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int boxWidthId = categoryComponent.getPropertyId(catId, "外箱宽度");
      if (StringUtils.isNotEmpty(boxWidth) && boxWidthId > 0 && isDecimal(boxWidth)) {
        sku.addInputCatProperty(boxWidthId, boxWidth);
      } else {
        sku.addInputCatProperty(boxWidthId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [外箱宽度] 错误，替换为0，excel内容：[" + boxWidth + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int boxHeigthId = categoryComponent.getPropertyId(catId, "外箱高度");
      if (StringUtils.isNotEmpty(boxHeigth) && boxHeigthId > 0 && isDecimal(boxHeigth)) {
        sku.addInputCatProperty(boxHeigthId, boxHeigth);
      } else {
        sku.addInputCatProperty(boxHeigthId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [外箱高度] 错误，替换为0，excel内容：[" + boxHeigth + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }
      int qualityId = categoryComponent.getPropertyId(catId, "质量等级");
      if (StringUtils.isEmpty(quality)) {
        sku.addSelectCatProperty(qualityId, String.valueOf(72));
      } else {
        int qualityValue = categoryComponent.getValueId(catId, qualityId, quality);
        if (qualityValue > 0) {
          sku.addSelectCatProperty(qualityId, String.valueOf(qualityValue));
        } else {
          sku.addSelectCatProperty(qualityId, String.valueOf(72));
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]  [质量等级] 错误，替换为[一级]，excel内容：[" + quality + "]");
          printdebug = printdebug + tmplog + "<br/>";
          logger.error(tmplog);
        }
      }

      int qualityDayId = categoryComponent.getPropertyId(catId, "保质期");
      if (StringUtils.isNotEmpty(qualityDay) && qualityDayId > 0 && isDecimal(qualityDay)) {
        sku.addInputCatProperty(qualityDayId, qualityDay);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [保质期] 错误，替换为30天，excel内容：[" + qualityDay + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
        sku.addInputCatProperty(qualityDayId, "30");
      }
      int outputTaxId = categoryComponent.getPropertyId(catId, "销项税");
      if (StringUtils.isNotEmpty(outputTax) && outputTaxId > 0 && isDecimal(outputTax)) {
        sku.addInputCatProperty(outputTaxId, outputTax);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [销项税] 错误，替换为 0，excel内容：[" + outputTax + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
        sku.addInputCatProperty(outputTaxId, "0");
      }
      int inputTaxId = categoryComponent.getPropertyId(catId, "进项税");
      if (StringUtils.isNotEmpty(inputTax) && inputTaxId > 0 && isDecimal(inputTax)) {
        sku.addInputCatProperty(inputTaxId, inputTax);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [进项税] 错误，替换为 0，excel内容：[" + inputTax + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
        sku.addInputCatProperty(inputTaxId, "0");
      }

      int inputpriceId = categoryComponent.getPropertyId(catId, "正常进价");
      if (StringUtils.isNotEmpty(inputprice) && inputpriceId > 0 && isDecimal(inputprice)) {
        sku.addInputCatProperty(inputpriceId,
            String.valueOf(new BigDecimal(inputprice).multiply(new BigDecimal(100)).intValue()));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [正常进价] 错误，替换为 0待进售价修改，excel内容：[" + inputprice + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
        sku.addInputCatProperty(inputpriceId, "0");
      }
      sku.addSelectCatProperty(23, "75");// 进价货币单位固定为CNY

      int priceId = categoryComponent.getPropertyId(catId, "正常售价");
      if (StringUtils.isNotEmpty(price) && priceId > 0 && isDecimal(inputprice)) {
        sku.addInputCatProperty(priceId,
            String.valueOf(new BigDecimal(price).multiply(new BigDecimal(100)).intValue()));
      } else {
        sku.addInputCatProperty(priceId, "0");
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [正常售价] 错误，替换为 0待进售价修改，excel内容：[" + price + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }

      sku.addSelectCatProperty(25, "75");// 售价货币单位固定为CNY

      int mixSizeId = categoryComponent.getPropertyId(catId, "最小起定量");
      if (StringUtils.isNotEmpty(mixSize) && mixSizeId > 0 && isDecimal(mixSize)) {
        sku.addInputCatProperty(mixSizeId, mixSize);
      } else {
        sku.addInputCatProperty(mixSizeId, "0");
        // logger.error("第[" + index + "]个商品["+ itemName +"]  [最小起定量] 错误，替换为 0，excel内容：[" + mixSize
        // +
        // "] + itemName+"]");
      }

      int isTaxFreeId = categoryComponent.getPropertyId(catId, "是否免农业税");
      int isTaxFreeValue = categoryComponent.getValueId(catId, isTaxFreeId, isTaxFree);
      if (isTaxFreeValue > 0) {
        sku.addSelectCatProperty(isTaxFreeId, String.valueOf(isTaxFreeValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [是否免农业税] 错误，无法处理，excel内容：[" + isTaxFree + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }
      int temperatureId = categoryComponent.getPropertyId(catId, "温度条件标识");
      int temperatureValue = categoryComponent.getValueId(catId, temperatureId, temperature);
      if (temperatureValue > 0) {
        sku.addSelectCatProperty(temperatureId, String.valueOf(temperatureValue));
      } else {
        if (StringUtils.isEmpty(temperature) || "恒温".equals(temperature)) {
          sku.addSelectCatProperty(temperatureId, "79");
        } else {
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]  [温度条件标识] 错误，无法处理，excel内容：[" + temperature + "]");
          printlog = printlog + tmplog + "<br/>";
          logger.error(tmplog);
          if (!isTest) {
            continue;
          }
        }
      }
      int returnPurchaseId = categoryComponent.getPropertyId(catId, "是否可以退货");
      int returnPurchaseValue =
          categoryComponent.getValueId(catId, returnPurchaseId, returnPurchase);
      if (returnPurchaseValue > 0) {
        sku.addSelectCatProperty(returnPurchaseId, String.valueOf(returnPurchaseValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [是否可以退货] 错误，无法处理，excel内容：[" + returnPurchase + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }
      int saleTypeId = categoryComponent.getPropertyId(catId, "销售类型");
      int saleTypeValue = categoryComponent.getValueId(catId, saleTypeId, saleType);
      if (saleTypeValue > 0) {
        sku.addSelectCatProperty(saleTypeId, String.valueOf(saleTypeValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [销售类型] 错误，无法处理，excel内容：[" + saleType + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }
      int saleHeightId = categoryComponent.getPropertyId(catId, "增加1个单位重量");
      if (StringUtils.isNotEmpty(saleHeight) && saleHeightId > 0 && isDecimal(saleHeight)) {
        sku.addInputCatProperty(saleHeightId, saleHeight);
      } else {
        sku.addInputCatProperty(saleHeightId, "0");
        // logger.error("第[" + index + "]个商品["+ itemName +"]  [增加1个单位重量] 错误，获取信息错误替换为0，excel内容：[" +
        // saleHeight +
        // "] + itemName+"]");
      }
      int priceTypeId = categoryComponent.getPropertyId(catId, "标价类型");
      if (StringUtils.isNotEmpty(priceType)) {
        int priceTypeValue = categoryComponent.getValueId(catId, priceTypeId, priceType);
        if (priceTypeValue > 0) {
          sku.addSelectCatProperty(priceTypeId, String.valueOf(priceTypeValue));
        } else {
          sku.addSelectCatProperty(priceTypeId, String.valueOf(89));// 默认小标签
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]  [标价类型] 错误，设置默认小标无日期 ，excel内容：[" + priceType + "]");
          printdebug = printdebug + tmplog + "<br/>";
          logger.error(tmplog);
        }
      } else {
        sku.addSelectCatProperty(priceTypeId, String.valueOf(89));// 默认小标签
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [标价类型] 错误，设置默认小标无日期，excel内容：[" + priceType + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }

      int priceTagTypeId = categoryComponent.getPropertyId(catId, "价签类型");
      if (StringUtils.isNotEmpty(priceTag)) {
        int priceTagTypeValue = categoryComponent.getValueId(catId, priceTagTypeId, priceTag);
        if (priceTagTypeValue > 0) {
          sku.addSelectCatProperty(priceTagTypeId, String.valueOf(priceTagTypeValue));
        } else {
          sku.addSelectCatProperty(priceTagTypeId, "93");// 默认电小 价签类型
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]  [价签类型] 错误，设置默认格式  电小 价签类型，excel内容：["
                  + priceTag + "]]");
          printdebug = printdebug + tmplog + "<br/>";
          logger.error(tmplog);
        }
      } else {
        sku.addSelectCatProperty(priceTagTypeId, "93");// 默认电小 价签类型
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [价签类型] 错误，设置默认格式  电小 价签类型，excel内容：["
                + priceTag + "]");
        printdebug = printdebug + tmplog + "<br/>";
        logger.error(tmplog);
      }

      int orderAddressId = categoryComponent.getPropertyId(catId, "订货地址");
      int orderAddressValue = categoryComponent.getValueId(catId, orderAddressId, orderAddress);
      if (orderAddressValue > 0) {
        sku.addSelectCatProperty(orderAddressId, String.valueOf(orderAddressValue));
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [订货地址] 错误，无法处理，excel内容：[" + orderAddress + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }
      // 默认全包含固定加工费有，费有为0
      sku.addSelectCatProperty(44, "76");
      sku.addSelectCatProperty(45, "101");
      sku.addInputCatProperty(46, "0");

      if (StringUtils.isEmpty(tags)) {
        item.setTitletags(subName);
      } else {
        item.setTitletags(tags);
      }

      boolean isBoxSale = (buyUnitValue == saleUnitValue);// 按大组销售
      boolean ifGroupSale = (gourpUnitValue == saleUnitValue);// 按大包装内的组销售
      boolean ifUnitSale =
          (gourpUnitValue == buyUnitValue && countUnitValue == saleUnitValue && buyUnitValue != saleUnitValue);// 按大包内最小小件销售
      boolean isNaturalSale = ((naturalUnitValue == saleUnitValue) && (naturalRateNum > 0));
      if (!(isBoxSale || ifGroupSale || ifUnitSale || isNaturalSale)) {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "]  [箱规数据属性不匹配] 错误，无法处理，商品标题excel内容为：[" + itemName + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }

      if (StringUtils.isNotEmpty(count) && StringUtils.isNotEmpty(group)) {
        int countNum = Integer.valueOf(count);
        int groupNum = Integer.valueOf(group);
        if (groupNum > countNum) {
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]   [件装数应该大于等于件装规格] 错误，无法处理，件装数excel内容为：["
                  + count + "],件装规格excel内容为：[" + group + "]");
          printlog = printlog + tmplog + "<br/>";
          logger.error(tmplog);
          if (!isTest) {
            continue;
          }
        }
        if (countNum % groupNum != 0) {
          String tmplog =
              ("第[" + index + "]个商品[" + itemName + "]   [件装数应该 整除 件装规格] 错误，无法处理，件装数excel内容为：["
                  + count + "],件装规格excel内容为：[" + group + "]");
          printlog = printlog + tmplog + "<br/>";
          logger.error(tmplog);
          if (!isTest) {
            continue;
          }
        }
      }

      item.setTitle(itemName);
      item.setSubtitle(subName);

      String folderTitle = oldName.replaceAll("\\*", "");
      folderTitle = folderTitle.replaceAll("/", "");
      folderTitle = folderTitle.replaceAll("\\|", "");

      String titlename[] = new String[] {"/title/", "/主图/"};
      String imgPath = "";
      List<File> titleListPic = null;
      for (String path : titlename) {
        File introfolder = new File(imgBasePath + folderTitle + path);
        if (!introfolder.exists() || !introfolder.isDirectory()) {
          titleListPic = Lists.newArrayList();
          continue;
        } else {
          imgPath = imgBasePath + folderTitle + path;
          titleListPic = this.getFileList(introfolder);
          break;
        }
      }

      logger.error("zhancang test get pic path:" + imgPath);
      List<HSSFPicture> listPic = itemTmp.getPictureList();
      if (CollectionUtils.isNotEmpty(titleListPic)) {
        if (!isTest) {
          Result<List<String>> picUrlResult =
              picComponent.updateFilePic(titleListPic, imgPath + "press", 2);
          if (picUrlResult.getSuccess() && CollectionUtils.isNotEmpty(picUrlResult.getModule())) {
            item.setPicList(picUrlResult.getModule());
          } else {
            logger.error("第[" + index + "]个商品[" + itemName + "] ，文件夹上传 主图图片上传失败");
          }
        }
      } else {
        if (CollectionUtils.isNotEmpty(listPic)) {
          if (!isTest) {
            Result<List<String>> picUrlResult =
                picComponent.updatePic(listPic, imgPath + "press", 2);
            if (picUrlResult.getSuccess() && CollectionUtils.isNotEmpty(picUrlResult.getModule())) {
              item.setPicList(picUrlResult.getModule());
            } else {
              logger.error("第[" + index + "]个商品[" + itemName + "] ，excel上传主图 图片上传失败");
            }
          }
        } else {
          String tmplog = "";
          if (saleTypeValue == 85) {// 仅线下
            tmplog = ("第[" + index + "]个商品[" + itemName + "] 找不到任何主图图片，线下销售忽略图片");
            if (displayImgError) {
              printdebug = printdebug + tmplog + "<br/>";
            }
          } else {
            tmplog = ("第[" + index + "]个商品[" + itemName + "] 找不到任何主图图片，需要线上销售没有图片，无法处理]");
            if (displayImgError) {
              printlog = printlog + tmplog + "<br/>";
            }
          }
          logger.error(tmplog);
        }
      }

      String folderInfro = oldName.replaceAll("\\*", "");
      folderInfro = folderInfro.replaceAll("/", "");
      folderInfro = folderInfro.replaceAll("\\|", "");

      String introname[] = new String[] {"/introduction/", "/images/", "/文描/", "/详情页/", "/详情/"};
      String introPath = "";
      List<File> introListPic = null;
      for (String path : introname) {
        File introfolder = new File(imgBasePath + folderInfro + path);
        if (!introfolder.exists() || !introfolder.isDirectory()) {
          introListPic = Lists.newArrayList();
          continue;
        } else {
          introPath = imgBasePath + folderInfro + path;
          introListPic = this.getFileList(introfolder);
          break;
        }
      }

      List<HSSFPicture> introPic = itemTmp.getIntroPictureList();
      if (CollectionUtils.isNotEmpty(introListPic)) {
        if (!isTest) {
          Result<List<String>> introUrlResult =
              picComponent.updateFilePic(introListPic, introPath + "press", 2);
          if (introUrlResult.getSuccess() && CollectionUtils.isNotEmpty(introUrlResult.getModule())) {
            item.setIntroduction(getIntroStr(introUrlResult.getModule()));
          } else {
            logger.error("第[" + index + "]个商品[" + itemName + "] ，文件夹上传详情图片上传失败");
          }
        }
      } else {
        if (CollectionUtils.isNotEmpty(introPic)) {
          if (!isTest) {
            Result<List<String>> introUrlResult =
                picComponent.updatePic(introPic, introPath + "press", 2);
            if (introUrlResult.getSuccess()
                && CollectionUtils.isNotEmpty(introUrlResult.getModule())) {
              item.setIntroduction(getIntroStr(introUrlResult.getModule()));
            } else {
              logger.error("第[" + index + "]个商品[" + itemName + "] ，excel上传详情 图片上传失败");
            }
          }
        } else {
          String tmplog = "";
          if (saleTypeValue == 85) {// 仅线下
            tmplog = ("第[" + index + "]个商品[" + itemName + "] 找不到任何商品详情图片，线下销售忽略图片");
            if (displayImgError) {
              printdebug = printdebug + tmplog + "<br/>";
            }
          } else {
            tmplog = ("第[" + index + "]个商品[" + itemName + "] 找不到任何商品详情图片，需要线上销售没有图片，无法处理");
            if (displayImgError) {
              printlog = printlog + tmplog + "<br/>";
            }
          }
          logger.error(tmplog);
        }
      }

      Date now = new Date();

      item.setCategoryId(catId);
      item.setGmtCreate(now);
      item.setGmtModified(now);
      item.setStatus(ItemProductStatusConstants.PRODUCT_STATUS_PASS);
      item.setCreator(5);

      sku.setTitle(itemName);
      int supplierId = getSupplierIdByPhone(phone);
      if (supplierId > 0) {
        sku.setSupplierId(supplierId);
      } else {
        String tmplog =
            ("第[" + index + "]个商品[" + itemName + "] ，找不到任供应商信息,无法处理,excel手机号[" + phone + "]");
        printlog = printlog + tmplog + "<br/>";
        logger.error(tmplog);
        if (!isTest) {
          continue;
        }
      }

      sku.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
      sku.setGmtCreate(now);
      sku.setGmtModified(now);
      sku.setSupplierAgent(getSupplierAgentId(sku.getSupplierId()));
      sku.setSkuUnit(buyUnitValue);
      sku.setSkuCount(1);
      int priceNum = 0;
      int buyPriceNum = 0;
      try {
        if (saleUnitValue == 3) {
          // 页面输入的是斤的价格，这里直接转化成公斤的价格
          priceNum =
              new BigDecimal(price).multiply(new BigDecimal(2)).multiply(new BigDecimal(100))
                  .intValue();
          buyPriceNum =
              new BigDecimal(inputprice).multiply(new BigDecimal(2)).multiply(new BigDecimal(100))
                  .intValue();
        } else {
          priceNum = new BigDecimal(price).multiply(new BigDecimal(100)).intValue();
          buyPriceNum = new BigDecimal(inputprice).multiply(new BigDecimal(100)).intValue();
        }
        sku.addInputCatProperty(priceId, new BigDecimal(priceNum).toString());
      } catch (Exception e) {
      }

      sku.setPrice(buyPriceNum);
      sku.setDiscountPrice(buyPriceNum);
      sku.setUnitPrice(buyPriceNum);
      sku.setDiscountUnitPrice(buyPriceNum);

      item.setItemProductSkuList(Arrays.asList(sku));
      Map<String, Object> tmpResult = Maps.newHashMap();
      tmpResult.put("obj", item);
      tmpResult.put("update", isupdate);
      tmpResult.put("printlog", printlog);
      tmpResult.put("printdebug", printdebug);
      result.add(tmpResult);
      logger.info("Format item success,title:" + itemName);
    }
    return result;
  }


  /**
   * 翻译成后台可用对象
   * 
   * @param excelItemList
   * @return
   */
  private List<Map<String, Object>> getProductPicList(List<ExcelItem> excelItemList,
      String imgBasePath, boolean displayImgError) {
    List<Map<String, Object>> result = Lists.newArrayList();
    int index = 2;
    for (ExcelItem itemTmp : excelItemList) {// 取掉页头数据
      String printlog = "";
      String printdebug = "";
      index++;
      List<String> propertyList = itemTmp.getProperty();
      if (propertyList.size() < 5) {
        continue;// 默认错误数据
      }
      String itemName = propertyList.get(1);
      // String item69Code = propertyList.get(12);// 69码，外部条码
      if (StringUtils.isNotEmpty(itemName)) {
        if (itemName.length() > 32) {
          itemName = itemName.substring(0, 32);
        }
      } else {
        continue;
      }

      ItemProductDTO item = new ItemProductDTO();
      item.setTitle(itemName);
      String folderTitle = itemName.replaceAll("\\*", "");
      folderTitle = folderTitle.replaceAll("/", "");
      folderTitle = folderTitle.replaceAll("\\|", "");

      String titlename[] = new String[] {"/title/", "/主图/"};
      String imgPath = "";
      List<File> titleListPic = null;
      for (String path : titlename) {
        File introfolder = new File(imgBasePath + folderTitle + path);
        if (!introfolder.exists() || !introfolder.isDirectory()) {
          titleListPic = Lists.newArrayList();
          continue;
        } else {
          imgPath = imgBasePath + folderTitle + path;
          titleListPic = this.getFileList(introfolder);
          break;
        }
      }

      if (CollectionUtils.isNotEmpty(titleListPic)) {
        Result<List<String>> picUrlResult =
            picComponent.updateFilePic(titleListPic, imgPath + "press", 2);
        if (picUrlResult.getSuccess() && CollectionUtils.isNotEmpty(picUrlResult.getModule())) {
          item.setPicList(picUrlResult.getModule());
        } else {
          logger.error("第[" + index + "]个商品[" + itemName + "] ，文件夹上传 主图图片上传失败");
        }
      } else {
        continue;
      }

      String folderInfro = itemName.replaceAll("\\*", "");
      folderInfro = folderInfro.replaceAll("/", "");
      folderInfro = folderInfro.replaceAll("\\|", "");

      String introname[] = new String[] {"/introduction/", "/images/", "/文描/", "/详情页/", "/详情/"};
      String introPath = "";
      List<File> introListPic = null;
      for (String path : introname) {
        File introfolder = new File(imgBasePath + folderInfro + path);
        if (!introfolder.exists() || !introfolder.isDirectory()) {
          introListPic = Lists.newArrayList();
          continue;
        } else {
          introPath = imgBasePath + folderInfro + path;
          introListPic = this.getFileList(introfolder);
          break;
        }
      }

      if (CollectionUtils.isNotEmpty(introListPic)) {
        Result<List<String>> introUrlResult =
            picComponent.updateFilePic(introListPic, introPath + "press", 2);
        if (introUrlResult.getSuccess() && CollectionUtils.isNotEmpty(introUrlResult.getModule())) {
          item.setIntroduction(getIntroStr(introUrlResult.getModule()));
        } else {
          logger.error("第[" + index + "]个商品[" + itemName + "] ，文件夹上传详情图片上传失败");
        }
      } else {
        continue;
      }

      Map<String, Object> tmpResult = Maps.newHashMap();
      tmpResult.put("obj", item);
      tmpResult.put("printlog", printlog);
      tmpResult.put("printdebug", printdebug);
      result.add(tmpResult);
      logger.info("Format item success,title:" + itemName);
    }
    return result;
  }


  /**
   * 读取一个excel文件，转化为可用对象
   * 
   * @return
   * @throws Exception
   */
  private List<ExcelItem> readExcel(File file) throws Exception {
    POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream(file));
    HSSFWorkbook hssfWorkbook = new HSSFWorkbook(poifsFileSystem);
    HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

    Map<Integer, List<HSSFPicture>> pictureMap = Maps.newHashMap();
    Map<Integer, List<HSSFPicture>> introPictureMap = Maps.newHashMap();

    int picColl = 3;// 商品主图在哪一列
    int introPicColl = 4;// 商品详情图在哪一列

    List<ExcelItem> result = Lists.newArrayList();
    if (null != hssfSheet && null != hssfSheet.getDrawingPatriarch()
        && CollectionUtils.isNotEmpty(hssfSheet.getDrawingPatriarch().getChildren())) {
      for (HSSFShape shape : hssfSheet.getDrawingPatriarch().getChildren()) {
        HSSFClientAnchor anchor = (HSSFClientAnchor) shape.getAnchor();
        if (shape instanceof HSSFPicture) {
          HSSFPicture pic = (HSSFPicture) shape;
          int row = anchor.getRow1();
          int coll = anchor.getCol1();
          if (coll == picColl) {
            List<HSSFPicture> picL = pictureMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            pictureMap.put(row, picL);
          } else if (coll == introPicColl) {
            List<HSSFPicture> picL = introPictureMap.get(row);
            if (CollectionUtils.isEmpty(picL)) {
              picL = Lists.newArrayList();
            }
            picL.add(pic);
            introPictureMap.put(row, picL);
          }
        }
      }
    }

    int rowstart = hssfSheet.getFirstRowNum() + 2;
    int rowEnd = hssfSheet.getLastRowNum();
    for (int i = rowstart; i <= rowEnd; i++) {
      HSSFRow row = hssfSheet.getRow(i);
      if (null == row) {
        continue;
      }
      // 除去页头
      int cellStart = row.getFirstCellNum();
      int cellEnd = row.getLastCellNum();

      ExcelItem item = new ExcelItem();
      for (int k = cellStart; k <= cellEnd; k++) {
        HSSFCell cell = row.getCell(k);
        if (null == cell) {
          item.addProperty(null);
          continue;
        } else {
          cell.setCellType(Cell.CELL_TYPE_STRING);
          item.addProperty(trim(cell.getStringCellValue()));
        }
      }
      item.setPictureList(pictureMap.get(i));
      item.setIntroPictureList(introPictureMap.get(i));
      result.add(item);
    }
    return result;
  }

  /**
   * 查询一个发布的前台商品对象。先查询对应的后台商品，获取属性设置前台商品属性，导入数据功能使用
   * 
   * @param productId
   * @return
   */
  private CommodityItemVO beforAddCommodityQueryProduct(long productCode) {
    Result<ItemProductDTO> productDTO =
        productService.queryItemProductByItemCode(productCode, true);
    if (null == productDTO || !productDTO.getSuccess()) {
      logger.error("get productDTO is null or not successs,productCode:" + productCode);
      return null;
    }

    List<ItemProductSkuDTO> skuList = productDTO.getModule().getItemProductSkuList();;
    ItemProductSkuDTO sku;
    if (CollectionUtils.isEmpty(skuList)) {
      logger.error("get skuList is empty,skuId:" + productCode);
      return null;
    }
    CommodityItemVO result = this.getCommodityItemVOByProductItemDTO(productDTO.getModule());

    sku = skuList.get(0);
    int catId = productDTO.getModule().getCategoryId();

    // 属性始终保持与采购商品同步
    List<PropertyVO> propertyVOList = getCommodityPropertyBySKUDTO(sku, catId, false);
    if (null != sku.getSku69code()) {
      result.setSku69Id(sku.getSku69code());
    }
    result.setPropertyVO(propertyVOList);
    return result;
  }

  /*
   * 导入数据时使用，更新历史数据
   */
  private ItemProductDTO getItemProductDTOByTitle(String title) {
    SkuQuery query = SkuQuery.querySkuByTitle(title, 100, 0);
    List<ItemProductDTO> queryList = Lists.newArrayList();
    Result<List<ItemProductSkuDTO>> result = skuService.queryProductSku(query);
    if (result.getSuccess() && CollectionUtils.isNotEmpty(result.getModule())) {
      List<ItemProductSkuDTO> tmpList = result.getModule();
      for (ItemProductSkuDTO sku : tmpList) {
        if (title.equals(sku.getTitle())) {
          Long itemId = sku.getItemId();
          Result<ItemProductDTO> productResult = productService.queryItemProduct(itemId, false);
          if (null != productResult && productResult.getSuccess()) {
            ItemProductDTO product = productResult.getModule();
            product.setItemProductSkuList(Arrays.asList(sku));
            queryList.add(product);
          }
        }
      }
    }
    if (queryList.size() > 1) {
      logger.error("Insert product 发现同名商品,title:" + title);
      return null;
    } else if (queryList.size() == 1) {
      return queryList.get(0);
    } else {
      return null;
    }
  }

  /*
   * 导入数据时使用，更新历史数据
   */
  private ItemProductDTO getItemProductDTOBy69Code(Long item69Code) {
    SkuQuery query = SkuQuery.querySkuBySku69Code(item69Code);
    List<ItemProductDTO> queryList = Lists.newArrayList();
    Result<List<ItemProductSkuDTO>> result = skuService.queryProductSku(query);
    if (result.getSuccess() && CollectionUtils.isNotEmpty(result.getModule())) {
      List<ItemProductSkuDTO> tmpList = result.getModule();
      for (ItemProductSkuDTO sku : tmpList) {
        if (item69Code.equals(sku.getSku69code())) {
          Long itemId = sku.getItemId();
          Result<ItemProductDTO> productResult = productService.queryItemProduct(itemId, false);
          if (null != productResult && productResult.getSuccess()) {
            ItemProductDTO product = productResult.getModule();
            product.setItemProductSkuList(Arrays.asList(sku));
            queryList.add(product);
          }
        }
      }
    }
    if (queryList.size() > 1) {
      logger.error("Insert product 发现同名商品,skuId:" + item69Code);
      return null;
    } else if (queryList.size() == 1) {
      return queryList.get(0);
    } else {
      return null;
    }
  }

  /*
   * 导入数据时使用，更新历史数据
   */
  private ItemCommoditySkuDTO getItemCommoditySkuDTOByTitle(String title) {
    SkuQuery query = SkuQuery.querySkuByTitle(title, 100, 0);
    List<ItemCommoditySkuDTO> queryList = Lists.newArrayList();
    Result<List<ItemCommoditySkuDTO>> result = skuService.queryItemSkuList(query);
    if (result.getSuccess() && CollectionUtils.isNotEmpty(result.getModule())) {
      List<ItemCommoditySkuDTO> tmpList = result.getModule();
      for (ItemCommoditySkuDTO sku : tmpList) {
        if (title.equals(sku.getTitle())) {
          queryList.add(sku);
        }
      }
    }

    if (queryList.size() > 1) {
      logger.error("update commondity, find name equal item,title:" + title);
      return null;
    } else if (queryList.size() == 1) {
      return queryList.get(0);
    } else {
      return null;
    }
  }

  /*
   * 导入数据时使用，更新历史数据
   */
  private ItemCommoditySkuDTO getItemCommoditySkuDTOBySku69Code(Long sku69Code) {
    SkuQuery query = SkuQuery.querySkuBySku69Code(sku69Code);
    List<ItemCommoditySkuDTO> queryList = Lists.newArrayList();
    Result<List<ItemCommoditySkuDTO>> result = skuService.queryItemSkuList(query);
    if (result.getSuccess() && CollectionUtils.isNotEmpty(result.getModule())) {
      List<ItemCommoditySkuDTO> tmpList = result.getModule();
      for (ItemCommoditySkuDTO sku : tmpList) {
        if (sku69Code.equals(sku.getSku69code())) {
          queryList.add(sku);
        }
      }
    }

    if (queryList.size() > 1) {
      logger.error("update commondity, find name equal item,sku69Code:" + sku69Code);
      return null;
    } else if (queryList.size() == 1) {
      return queryList.get(0);
    } else {
      return null;
    }
  }


  /**
   * 获取今晚凌晨
   * 
   * @return
   */
  private Date getTomorrowAMTime() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.DAY_OF_MONTH, 1);
    return cal.getTime();
  }

  private String trim(String input) {
    if (StringUtils.isEmpty(input)) {
      return "";
    } else {
      return input.trim();
    }
  }

  /**
   * 获取今晚凌晨
   * 
   * @return
   */
  private Date getTodayAMTime() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.DAY_OF_MONTH, 0);
    return cal.getTime();
  }

  public boolean isDecimal(String str) {
    if (StringUtils.isEmpty(str))
      return false;
    java.util.regex.Pattern pattern = Pattern.compile("[0-9]*(\\.?)[0-9]*");
    return pattern.matcher(str).matches();
  }

  private String getIntroStr(List<String> picList) {
    List<IntorductionDataVO> resultList = Lists.newArrayList();
    for (String tmp : picList) {
      IntorductionDataVO vo = new IntorductionDataVO();
      vo.setSource(tmp);
      vo.setType(2);
      resultList.add(vo);
    }
    if (CollectionUtils.isEmpty(resultList)) {
      return "";
    } else {
      return JackSonUtil.getJson(resultList);
    }
  }

  // /**
  // * 取图片目录下的文件名称列表
  // *
  // * @param imgFolder
  // * @return
  // */
  // private Set<String> getpicname(String imgFolder) {
  // File file = new File(imgFolder);
  // if (file != null && file.isDirectory()) {
  // String[] fileName = file.list();
  // if (null == fileName || fileName.length == 0) {
  // logger.error("Add all get picname, Folder empty,path:" + imgFolder);
  // return Sets.newHashSet();
  // } else {
  // Set<String> result = Sets.newHashSet();
  // result.addAll(Arrays.asList(fileName));
  // for (String tmp : result) {
  // tmp = tmp.toLowerCase();
  // }
  // return result;
  // }
  // } else {
  // logger.error("Add all get picname failed,path:" + imgFolder);
  // return null;
  // }
  // }

  /**
   * 取图片文件列表
   * 
   * @param title
   * @return
   */
  private List<File> getFileList(File folder) {
    List<File> result = Lists.newArrayList();
    File subList[] = folder.listFiles();
    if (subList == null || subList.length < 0) {
      return result;
    }
    Arrays.sort(subList, new CompratorByName());
    for (File file : subList) {
      if (!file.isDirectory() && file.isFile()) {
        result.add(file);
      }
    }
    return result;

    // List<File> result = Lists.newArrayList();
    // for (int i = 1; i < 6; i++) {
    // String fileName = title + i + ".jpg";
    // fileName = fileName.toLowerCase();
    // if (nameSet.contains(fileName)) {
    // result.add(new File(basePath + fileName));
    // continue;
    // }
    // fileName = title + i + ".jpeg";
    // fileName = fileName.toLowerCase();
    // if (nameSet.contains(fileName)) {
    // result.add(new File(basePath + fileName));
    // continue;
    // }
    // fileName = title + i + ".png";
    // fileName = fileName.toLowerCase();
    // if (nameSet.contains(fileName)) {
    // result.add(new File(basePath + fileName));
    // continue;
    // }
    // }

  }

  public static void main(String args[]) {
    ItemComponent item = new ItemComponent();
    String test = "8410069001450";
    System.out.println(item.get69Code(test));
    // System.out.println(Long.valueOf(test));
    // ItemComponent item = new ItemComponent();
    // List<File> testList =
    // item.getFileList(new File("D:/work/input/img/LAFITE 拉菲 传奇波尔多法定产区红葡萄酒 750mL瓶/introduction"));
    // for (File file : testList) {
    // System.out.println(file.getName());
    // }
    // System.out.println(item.get69Code("085000001886"));
  }

  public String getSkuInfo(ItemListVO data) {
    if (null == data || data.getCurrentPage() < 0 || data.getPageSize() < 0) {
      throw new BizException("参数异常");
    }

    if (null == data.getSkuId() || 0 == data.getSkuId() || StringUtils
        .isEmpty(data.getTitle())) {
      throw new BizException("商品条码和名称不能同时为空");
    }

    HashMap<String, Object> map = Maps.newHashMap();

    if (null != data.getSkuId() && data.getSkuId() != 0 && StringUtils.isEmpty(data.getTitle())) {
      Result<CommodityItemVO> commodityItemVOResult = getCommodityItemVO(data.getSkuId() + "");
      if (null != commodityItemVOResult && commodityItemVOResult.getSuccess()
          && null != commodityItemVOResult.getModule()) {
        map.put("items", commodityItemVOResult.getModule());
        map.put("currentPage", data.getCurrentPage());
        //TODO 曲洋
        return JackSonUtil
            .getJson(Result.getSuccDataResult(map));
      }
      throw new BizException("查询信息失败");
    }

    if (null != data.getTitle() && !StringUtils.isEmpty(data.getTitle())) {
      ItemDTO itemDTO = new ItemDTO();
      Result<List<ItemCommoditySkuDTO>> listResult = commodityService
          .queryCommodityLikeTitle(itemDTO, data.getPageSize(), data.getCurrentPage());

      if (null != listResult && listResult.getSuccess() && null != listResult.getModule()
          && CollectionUtils.isNotEmpty(listResult.getModule())) {
        List<ItemCommoditySkuDTO> listResultModule = listResult.getModule();
        map.put("items", listResultModule);
        map.put("currentPage", data.getCurrentPage());
        return JackSonUtil
            .getJson(Result.getSuccDataResult(map));
      }
      throw new BizException("查询商品信息失败！");
    }
    throw new BizException("查询商品信息失败");
  }

  /**
   * 查询商品信息
   */
  public String getItemsInfo(CommodityItemVO commodityItemVO) {
    logger.error("commodityItemVO="+JackSonUtil.getJson(commodityItemVO));
    if (null == commodityItemVO || null == commodityItemVO.getPageSize() || null == commodityItemVO.getCurrentPage()) {
      throw new BizException("参数异常");
    }
    ConcurrentMap<String, Object> map = Maps.newConcurrentMap();
    map.put("currentPage",commodityItemVO.getCurrentPage());
    map.put("totalPage",getTotalPage(getCommodityItemVOCount(commodityItemVO),commodityItemVO.getPageSize()));
    map.put("data", getCommodityItemVO(commodityItemVO));
    return JackSonUtil
        .getJson(Result.getSuccDataResult(map));
  }


  public Integer getCommodityItemVOCount(CommodityItemVO commodityItemVO) {
    if (null == commodityItemVO) {
      throw new BizException("商品参数对象为空");
    }
    SkuQuery skuQuery = new SkuQuery();
    skuQuery.setSkuIds(commodityItemVO.getSkuIds());
    skuQuery.setSkuTitle(commodityItemVO.getTitle());
    Result<Integer> integerResult = skuService.queryItemSkuCount(skuQuery);
    if (null == integerResult || !integerResult.getSuccess()) {
      throw new BizException("查询商品总数量失败");
    }
    return integerResult.getModule();
  }

  /**
   * 获取商品信息
   * skuIds title pageSizw pageNum
   *
   */
  public List<CommodityItemVO> getCommodityItemVO(CommodityItemVO commodityItemVO) {
    if (null == commodityItemVO) {
      throw new BizException("查询商品信息的参数对象为空");
    }

    SkuQuery skuQuery = new SkuQuery();
    skuQuery.setPageNum(commodityItemVO.getCurrentPage());
    skuQuery.setPageSize(commodityItemVO.getPageSize());
    skuQuery.setSkuIds(commodityItemVO.getSkuIds());
    skuQuery.setSkuTitle(commodityItemVO.getTitle());

    //通过商品名称模糊查询
    logger.error("查询条件=" + skuQuery.getPageSize() + ",pageNum=" + skuQuery.getPageNum() + ",skuIds="
        + skuQuery.getSkuIds() + ",title=" + skuQuery.getSkuTitle());
    Result<List<ItemCommoditySkuDTO>> listResult = skuService.queryItemSkuList(skuQuery);
    List<CommodityItemVO> list = new ArrayList<>();
    if (null != listResult && listResult.getSuccess() && null != listResult.getModule()
        && CollectionUtils.isNotEmpty(listResult.getModule())) {
      List<ItemCommoditySkuDTO> module = listResult.getModule();
      for (ItemCommoditySkuDTO itemCommoditySkuDTO : module) {
        list.add(getCommodityItemVOByItemCommoditySkuDTO(
            itemCommoditySkuDTO));
      }
      if (CollectionUtils.isEmpty(list)) {
        throw new BizException("查询商品信息结构为空");
      }
      return list;
    }

    throw new BizException("没有符合查询商品信息的查询条件");

  }

  /**
   * 关联商品和优惠券
   */
  public Result addCouponAndItem(CouponQueryVO data) {
    isRightParames(data);

    //获取优惠券
    if (hasCouponDTO(CouponsQuery.queryByTitle(data.getTitle()))) {
      throw new BizException("已从在同名的优惠券");
    }

    CouponDTO couponDTO = new CouponDTO();
    couponDTO.setTitle(data.getTitle());//优惠券名称
    couponDTO.setType(CouponType.ZHIDING_ITEM);//指定商品优惠
    couponDTO.setStatus(CouponStatus.UNUSED);//未使用
    couponDTO.setChannelType(data.getChannelType());//使用渠道
    couponDTO.setGmtStart(getRightDate(data.getStartTime()));//开始时间
    couponDTO.setGmtEnd(getRightDate(data.getEndTime()));//失效时间
    couponDTO.setRules(
        Double.parseDouble(data.getAmount() + "") + "_id_" + 1 + ">" + Double
            .parseDouble(data.getAmountLimit() + "") + "?" + data.getAmount()
            + ":0");//使用规则
    couponDTO.setAmount(Double.parseDouble(data.getAmount() + ""));//优惠金额
    couponDTO.setAmountLimit(Double.parseDouble(data.getAmountLimit() + ""));//最大优惠金额

    insertCouponDTO(couponDTO);//生成优惠券

    List<CouponDTO> couponDTO1 = getCouponDTO(CouponsQuery.queryByTitle(data.getTitle()));//获取优惠券

    if (CollectionUtils.isEmpty(couponDTO1)) {
      throw new BizException("插入优惠券信息失败");
    }

    CouponDTO targetCoupon = couponDTO1.get(0);
    if (CouponCondition.NO_CONDITION.equals(data.getUseConditon())) {//无门槛
      targetCoupon.setRules("金额_id_" + targetCoupon.getId() + ">" + data.getAmountLimit() + "?"
          + data.getAmount()
          + ":" + data.getAmount());
    } else if (CouponCondition.MANJIAN.equals(data.getUseConditon())) {//满减券
      targetCoupon.setRules("金额_id_" + targetCoupon.getId() + ">" + data.getAmountLimit() + "?"
          + data.getAmount()
          + ":0");
    }

    setSendRules(data, targetCoupon);//发放规则
    updateCouponDTO(targetCoupon);//更新优惠券

    //关联
    if ( data.getRelated() && CollectionUtils.isNotEmpty(data.getSkuIds())) {
      updateSkuTags(data.getSkuIds(), 512, data.getChannelType(), null, targetCoupon.getId() + "");
    }

    return Result.getSuccDataResult(true);
  }

  /**
   * 格式化时间
   */
  public Date getRightDate(String date) {
    if (StringUtils.isEmpty(date)) {
      return null;
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date parse = null;
    try {
      parse = sdf.parse(date);
    } catch (ParseException e) {
      logger.error("格式化时间失败");
      throw new BizException("格式化时间失败");
    }

    return parse;
  }

  /**
   * 设置发放规则
   */
  private void setSendRules(CouponQueryVO data, CouponDTO targetCoupon) {
    if (CouponSendRulesCondition.NO_CONDITION.equals(data.getSendRulesType())) {//无门槛
      targetCoupon.setSendRules(null);
    }
    if (CouponSendRulesCondition.ALL_COUPON_AMOUNT.equals(data.getSendRulesType())) {
      targetCoupon
          .setSendRules("coupon<=" + data.getLimitAmount() + "?1:0");
    }

    if (CouponSendRulesCondition.VALID_COUPON_AMOUNT.equals(data.getSendRulesType())) {
      targetCoupon
          .setSendRules("valid_coupon<=" + data.getLimitAmount() + "?1:0");
    }
  }

  private void isRightParames(CouponQueryVO data) {

    //channnelType(0通用渠道,1线上渠道,2线下) distributeAmount(发行数) endTime startTiem title useConditon(无门槛or满n元) limitAmount(限领数)
    //amount(优惠金额) amountLimit(最大金额)
    //related 是否关联优惠券，默认为true
    //指定商品满减	12
    //单品优惠	    4
    //无门槛			12
    //全场满减		11

    if (null == data || null == data.getChannelType()
        || null == data.getDistributeAmount() || null == data.getEndTime() || null == data
        .getStartTime() || null == data.getTitle() || StringUtils.isEmpty(data.getTitle())
        || null == data.getUseConditon() || null == data.getLimitAmount() || null == data
        .getAmount() || "0"
        .equals(data.getAmount() + "") || null == data.getAmountLimit() || "0"
        .equals(data.getAmountLimit() + "")) {

      throw new BizException("参数不正确");
    }

    if (null == data.getRelated()) {
      throw new BizException("关联参数为空");
    }

    if (data.getRelated() && CollectionUtils.isEmpty(data.getSkuIds())) {
      throw new BizException("商品参数信息为空");
    }

  }

  // 根据文件修改时间进行比较的内部类
  static class CompratorByName implements Comparator<File> {
    public int compare(File f1, File f2) {
      String f1name = f1.getName();
      String f2name = f2.getName();
      return f1name.compareTo(f2name);
    }
  }

  public Result<InventoryVO> queryItemInventory(Long skuId) {
    if (null == skuId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    SimpleResultVO<InventoryVO> simpleResultVO =
        inventory2CDomainClient.getInventoryBySKUID(1L, skuId);
    logger.info("请求商品库存是否成功：" + simpleResultVO.isSuccess());
    if (null != simpleResultVO && simpleResultVO.isSuccess() && null != simpleResultVO.getTarget()) {
      InventoryVO inventoryVO = simpleResultVO.getTarget();
      return Result.getSuccDataResult(inventoryVO);
    }
    return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "当前skuId：" + skuId + "下的商品不存在！");
  }

  /**
   * 获取标类型以及标渠道
   * 
   * @return
   */
  public Result<Map<String, Object>> queryTagsType() {
    Result<SystemConfigDTO> systemConfigResult =
        systemPropertyService.getSystemConfig(TAG_TYPE_CONFIG_ID);
    Map<String, Object> resultMap = Maps.newHashMap();
    List<ItemTagTypeVO> itemTagTypeVOs = null;
    if (null != systemConfigResult && systemConfigResult.getSuccess()
        && null != systemConfigResult.getModule()) {
      SystemConfigDTO systemConfigDTO = systemConfigResult.getModule();
      String configInfo = systemConfigDTO.getConfigInfo();
      if (StringUtils.isNotBlank(configInfo)) {
        itemTagTypeVOs = JackSonUtil.jsonToList(configInfo, ItemTagTypeVO.class);
      }
    }
    resultMap.put("tagsType", itemTagTypeVOs);
    List<Map<String, Object>> tagsChannelTypes = Lists.newArrayList();
    for (int i = 0; i < 3; i++) {
      Map<String, Object> map = Maps.newHashMap();
      map.put("channel", i);
      map.put("channelString", CHANNEL_STRINGS[i]);
      tagsChannelTypes.add(map);
    }
    resultMap.put("channelTypes", tagsChannelTypes);
    return Result.getSuccDataResult(resultMap);
  }

  public Result<Boolean> deleteSkuTags(ItemTagsDetaileVO itemTagsDetaileVO) {
    Long skuId = itemTagsDetaileVO.getSkuId();
    if (null == skuId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    SkuTagDetail skuTagDetail = new SkuTagDetail();
    skuTagDetail.setTagId(itemTagsDetaileVO.getTagId());
    skuTagDetail.setTagName(itemTagsDetaileVO.getTagContent());
    skuTagDetail.setChannel(itemTagsDetaileVO.getChannel());
    Result<ItemCommoditySkuDTO> itemResult = skuService.queryItemSku(SkuQuery.querySkuById(skuId));
    if (null == itemResult || !itemResult.getSuccess() || null == itemResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "该商品不存在！");
    }
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemResult.getModule();
    String tagContent = itemCommoditySkuDTO.queryTagContent();
    Integer tag = getTagsByTagId(itemTagsDetaileVO.getTagId(), itemTagsDetaileVO.getChannel());
    if (StringUtils.isNotBlank(tagContent)) {
      List<SkuTagDetail> skuTagDetails = JackSonUtil.jsonToList(tagContent, SkuTagDetail.class);
      final SkuTagDetail skuTagDetailFinal = skuTagDetail;
      final Integer tagFinal = tag;
      final String finalTagRule = itemTagsDetaileVO.getTagRule();
      CollectionUtils.filter(skuTagDetails, new Predicate() {
        @Override
        public boolean evaluate(Object object) {
          SkuTagDetail tagDetail = (SkuTagDetail) object;
          String tagDetailId = tagDetail.getTagId();
          if (StringUtils.isBlank(tagDetailId)) {
            return true;
          }
          if (!SkuTagDefinitionConstants.PROMOTION_TAG.equals(tagDetailId)
              && tagDetailId.equals(skuTagDetailFinal.getTagId())) {
            return false;
          } else if (SkuTagDefinitionConstants.PROMOTION_TAG.equals(tagDetailId)) {
            updateItemPromotion(tagFinal, tagDetail, finalTagRule);
            if (StringUtils.isBlank(tagDetail.getTagRule())) {
              return false;
            }
          }
          return true;
        }
      });
      itemCommoditySkuDTO.addTagContent(JackSonUtil.getJson(skuTagDetails));
      skuService.updateItemSku(itemCommoditySkuDTO);
    }

    Result<Boolean> result = skuService.updateItemSkuTags(skuId, tag, -1);
    return result;
  }

  /**
   * 更新商品优惠信息
   * 
   * @param tag
   * @param skuTagDetail
   * @param tagRule 
   */
  protected void updateItemPromotion(Integer tag, SkuTagDetail skuTagDetail, String tagRule) {
    if (null == tag || null == skuTagDetail || StringUtils.isBlank(skuTagDetail.getTagRule())) {
      return;
    }
    Result<SystemConfigDTO> tagConfigResult =
        systemPropertyService.getSystemConfig(TAG_TYPE_CONFIG_ID);
    if (null == tagConfigResult || !tagConfigResult.getSuccess()
        || null == tagConfigResult.getModule()) {
      return;
    }
    SystemConfigDTO systemConfigDTO = tagConfigResult.getModule();
    String configInfo = systemConfigDTO.getConfigInfo();
    if (StringUtils.isBlank(configInfo)) {
      return;
    }
    List<ItemTagTypeVO> itemTagTypeVOs = JackSonUtil.jsonToList(configInfo, ItemTagTypeVO.class);
    if (CollectionUtils.isEmpty(itemTagTypeVOs)) {
      return;
    }
    List<Integer> tags = Lists.newArrayList();
    for (ItemTagTypeVO itemTagTypeVO : itemTagTypeVOs) {
      tags.add(itemTagTypeVO.getTags());
    }
    // 获取tag所对应的tagRule，即优惠券id
    if(StringUtils.isEmpty(tagRule)){
      int index = tags.indexOf(tag);
      tagRule = itemTagTypeVOs.get(index).getTagRule();
    }
    final String promotionId = tagRule;
    // 删除已经存在的优惠券id
    List<String> tagRules =
        new ArrayList<String>(Arrays.asList(skuTagDetail.getTagRule().split(",")));
    CollectionUtils.filter(tagRules, new Predicate() {
      @Override
      public boolean evaluate(Object input) {
        String tagRuleElement = (String) input;
        if (tagRuleElement.equals(promotionId)) {
          return false;
        }
        return true;
      }
    });

    if (CollectionUtils.isNotEmpty(tagRules)) {
      StringBuilder sBuilder = new StringBuilder();
      for (String rule : tagRules) {
        sBuilder.append(rule);
        if (tagRules.indexOf(rule) != (tagRules.size() - 1)) {
          sBuilder.append(SPLIT_COMMA_KEY);
        }
      }
      skuTagDetail.setTagRule(sBuilder.toString());
    } else {
      skuTagDetail.setTagRule(null);
    }
  }

  /**
   * 判断tag id是否已经存在skuTagDetailList
   * 
   * @param desTag
   * @param skuTagDetailList
   * @return
   */
  private boolean tagIdExist(String tagId, List<SkuTagDetail> skuTagDetailList) {
    if (CollectionUtils.isEmpty(skuTagDetailList)) {
      return false;
    }
    List<String> tagIds = Lists.newArrayList();
    for (SkuTagDetail skuTagDetail : skuTagDetailList) {
      tagIds.add(skuTagDetail.getTagId());
    }
    if (CollectionUtils.isNotEmpty(tagIds) && tagIds.contains(tagId.toString())) {
      return true;
    }
    return false;
  }

  /**
   * 根据tagId和标渠道获取标位
   * 
   * @param tagId
   * @param channel
   * @return
   */
  private Integer getTagsByTagId(String tagId, Short channel) {
    if (StringUtils.isBlank(tagId)) {
      return null;
    }
    Integer tag = 0;
    switch (tagId) {
      case SkuTagDefinitionConstants.BUY_DISCOUNT_99_40:
        if (null != channel && channel.shortValue() == 1) {
          tag = 1;
        } else if (null != channel && channel.shortValue() == 2) {
          tag = 16;
        } else {
          tag = 17;
        }
        break;
      case SkuTagDefinitionConstants.BUY_DISCOUNT_40_20:
        if (null != channel && channel.shortValue() == 1) {
          tag = 32;
        } else if (null != channel && channel.shortValue() == 2) {
          tag = 2;
        } else {
          tag = 34;
        }
        break;
      case SkuTagDefinitionConstants.DES_TAG:
        tag = 4;
        break;
      case SkuTagDefinitionConstants.NOT_ALLOW_PROMOTION:
        tag = 64;
        break;
      case SkuTagDefinitionConstants.NOT_ALLOW_CHARGE:
        tag = 128;
        break;
      case SkuTagDefinitionConstants.NOT_ENOUGH_INVENTORY:
        tag = 256;
        break;
      case SkuTagDefinitionConstants.ONLINE_DISCOUNT:
        tag = 512;
        break;
      case SkuTagDefinitionConstants.LIMITED_TIME_PREFERENCE:
        tag = 8;
        break;
      case SkuTagDefinitionConstants.DISTRIBUTION_ALONE:
        tag = 2048;
        break;
      case SkuTagDefinitionConstants.BUY_LIMIT:
        tag = 4096;
        break;
      case SkuTagDefinitionConstants.DISTRIBUTION_FREE:
        tag = 8192;
        break;
      default:
        tag = 0;
        break;
    }
    return tag;
  }

  /**
   * 通过类目id查找商品id
   * 
   * @param CatIds
   * @return
   */
  public List<Long> querySkuIdsByCatId(List<Integer> catIds) {
    if (CollectionUtils.isEmpty(catIds)) {
      return null;
    }
    List<Integer> leavesCatIds = Lists.newArrayList();
    for (Integer catId : catIds) {
      leavesCatIds.addAll(queryLeavesCatById(catId));
    }
    ItemListQuery query = new ItemListQuery();
    query.setCategoryIds(leavesCatIds);
    query.setPageSize(1000);
    query.setPageNum(0);
    Result<Map<String, Object>> itemResult = commodityService.queryCommodies(query);
    if (null == itemResult || !itemResult.getSuccess() || MapUtils.isEmpty(itemResult.getModule())) {
      return null;
    }
    Map<String, Object> itemMap = itemResult.getModule();
    List<ItemDTO> itemDTOs = (List<ItemDTO>) itemMap.get(CommodityService.ITEM_LIST_KEY);
    if (CollectionUtils.isEmpty(itemDTOs)) {
      return null;
    }
    List<Long> skuIds = Lists.newArrayList();
    for (ItemDTO itemDTO : itemDTOs) {
      String[] skus = itemDTO.getSku().split(",");
      skuIds.add(Long.valueOf(skus[0]));
    }
    return skuIds;
  }

  /**
   * 查询叶子类目
   * 
   * @param catId
   * @return
   */
  private List<Integer> queryLeavesCatById(Integer catId) {
    if (null == catId) {
      return null;
    }
    Result<List<CategoryDTO>> subCategoryResult = stdCategoryService.querySubcategoryById(catId);
    if (null == subCategoryResult || !subCategoryResult.getSuccess()
        || CollectionUtils.isEmpty(subCategoryResult.getModule())) {
      Result<CategoryDTO> categoryResult = stdCategoryService.queryCategoryById(catId);
      if (null != categoryResult && categoryResult.getSuccess()
          && null != categoryResult.getModule()) {
        CategoryDTO subCategoryDTO = categoryResult.getModule();
        if (subCategoryDTO.getLeaf().shortValue() == 1) {
          return Arrays.asList(catId);
        }
      }
    }
    List<CategoryDTO> categoryDTOs = subCategoryResult.getModule();
    List<Integer> catIds = Lists.newArrayList();
    for (CategoryDTO categoryDTO : categoryDTOs) {
      catIds.add(categoryDTO.getCatId());
    }
    List<Integer> leavesCatId = Lists.newArrayList();
    for (Integer subCatId : catIds) {
      leavesCatId.addAll(queryLeavesCatById(subCatId));
    }
    return leavesCatId;
  }

  /**
   * 采购商品查询
   * 
   * @param purchaseItemQueryVO
   * @return
   */
  public PagedResult<List<SubPurchaseItemVO>> queryPurchaseItem(
      PurchaseItemQueryVO purchaseItemQueryVO) {
    if (null == purchaseItemQueryVO) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    Integer pageSize = purchaseItemQueryVO.getPageSize();
    Integer pageNum = purchaseItemQueryVO.getPageNum();
    Integer supplierId = purchaseItemQueryVO.getSupplierId();
    Long skuId = purchaseItemQueryVO.getSkuId();
    String title = purchaseItemQueryVO.getTitle();
    Short status = purchaseItemQueryVO.getStatus();
    logger.error(status);
    if(null == status){
      status = ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES.shortValue();
    }
    if (null != skuId) {
      return queryItemBySkuId(supplierId, skuId, status, pageSize, pageNum);
    } else if (StringUtils.isNotBlank(title)) {
      return queryPurchaseItemByTitle(supplierId, status, title, pageSize, pageNum);
    }
    return queryDefaultPurchaseItem(supplierId, status, pageSize, pageNum);
  }

  /**
   * 根据商品名查询采购商品数据
   * 
   * @param supplierId
   * @param status 
   * 
   * @param title
   * @param pageSize
   * @param pageNum
   * @return
   */
  private PagedResult<List<SubPurchaseItemVO>> queryPurchaseItemByTitle(Integer supplierId,
      Short status, String title, Integer pageSize, Integer pageNum) {
    if (StringUtils.isBlank(title)) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "商品名称为空");
    }
    if(null == status){
      status = ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES.shortValue();
    }
    SkuQuery commoditySkuQuery = SkuQuery.querySkuByTitle(title, pageSize, pageNum);
    commoditySkuQuery.setStatuses(Arrays.asList(status));
    Result<List<ItemCommoditySkuDTO>> itemCommodityResult = skuService.queryItemSkuList(commoditySkuQuery);
    if(null == itemCommodityResult || !itemCommodityResult.getSuccess() || CollectionUtils.isEmpty(itemCommodityResult.getModule())){
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    List<Long> cskuIds = Lists.newArrayList();
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommodityResult.getModule();
    for(ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs){
      cskuIds.add(itemCommoditySkuDTO.getSkuId());
    }
    
    Result<List<ItemSkuRelationDTO>> itemSkuRelationResult = skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(cskuIds, RelationTypeConstants.RELATION_TYPE_DISCOUNT));
    if(null == itemSkuRelationResult || !itemSkuRelationResult.getSuccess() || CollectionUtils.isEmpty(itemSkuRelationResult.getModule())){
      logger.error("服务内部错误或网络连接失败（cskuId）：" + JackSonUtil.getJson(cskuIds));
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "网络连接失败，请稍后再试");
    }
    List<ItemSkuRelationDTO> itemSkuRelationDTOs = itemSkuRelationResult.getModule();
    List<Long> pskuIds = Lists.newArrayList();
    for(ItemSkuRelationDTO itemSkuRelationDTO : itemSkuRelationDTOs){
      pskuIds.add(itemSkuRelationDTO.getPskuId());
    }
    SkuQuery productSkuQuery = SkuQuery.querySkuBySkuIds(pskuIds);
    productSkuQuery.setSupplier(supplierId);
    productSkuQuery.setPageNum(pageNum);
    productSkuQuery.setPageSize(pageSize);
    Result<List<ItemProductSkuDTO>> itemProductResult =
        skuService.queryProductSku(productSkuQuery);
    if (null == itemProductResult || !itemProductResult.getSuccess()
        || CollectionUtils.isEmpty(itemProductResult.getModule())) {
      logger.error("查找与" + title + "相关的商品失败：" + itemProductResult.getErrorMsg());
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    Result<Integer> itemProductCountResult =
        skuService.queryProductSkuCount(productSkuQuery);
    Integer totalNum = 0;
    if (null != itemProductCountResult && itemProductCountResult.getSuccess()
        && null != itemProductCountResult.getModule()) {
      totalNum = itemProductCountResult.getModule();
    }
    List<ItemProductSkuDTO> itemProductSkuDTOs = itemProductResult.getModule();
    List<SubPurchaseItemVO> purchaseItemVOs =
        itemProductSkuDTOs2PurchaseItemVOs(itemProductSkuDTOs);
    return PagedResult.getSuccDataResult(purchaseItemVOs, totalNum,
        getTotalPage(totalNum, pageSize), pageSize, pageNum);
  }

  /**
   * DTO列表转换为VO列表
   * 
   * @param itemProductSkuDTOs
   * @return
   */
  private List<SubPurchaseItemVO> itemProductSkuDTOs2PurchaseItemVOs(
      List<ItemProductSkuDTO> itemProductSkuDTOs) {
    if (CollectionUtils.isEmpty(itemProductSkuDTOs)) {
      return null;
    }
    List<Integer> catIds = Lists.newArrayList();
    List<Long> itemIds = Lists.newArrayList();
    for (ItemProductSkuDTO itemProductSkuDTO : itemProductSkuDTOs) {
      itemIds.add(itemProductSkuDTO.getItemId());
    }
    Result<List<ItemProductDTO>> itemResult = productService.queryItemProductByIdList(itemIds);
    Map<Long, String> itemCategoryNameMap = Maps.newHashMap();
    // 根据商品id获取类目id
    if (null != itemResult && itemResult.getSuccess()
        && CollectionUtils.isNotEmpty(itemResult.getModule())) {
      List<ItemProductDTO> itemProductDTOs = itemResult.getModule();
      Map<Long, Integer> categoryItemMap = Maps.newHashMap();
      for (ItemProductDTO itemProductDTO : itemProductDTOs) {
        catIds.add(itemProductDTO.getCategoryId());
        categoryItemMap.put(itemProductDTO.getProductId(), itemProductDTO.getCategoryId());
      }
      // 根据类目id获取类目名，设置商品id和类目名映射关系
      Map<Integer, String> categoryNameMap = Maps.newHashMap();
      Result<List<CategoryDTO>> categoryResult = stdCategoryService.queryCategoriesByIdList(catIds);
      logger.error("查询类目结果：" + JackSonUtil.getJson(categoryResult));
      if (null != categoryResult && categoryResult.getSuccess()
          && CollectionUtils.isNotEmpty(categoryResult.getModule())) {
        List<CategoryDTO> categoryDTOs = categoryResult.getModule();
        for (CategoryDTO categoryDTO : categoryDTOs) {
          categoryNameMap.put(categoryDTO.getCatId(), categoryDTO.getCatName());
        }
      }
      for (Long itemId : categoryItemMap.keySet()) {
        itemCategoryNameMap.put(itemId, categoryNameMap.get(categoryItemMap.get(itemId)));
      }
    }
    List<Long> skuIds = Lists.newArrayList();
    for (ItemProductSkuDTO itemProductSkuDTO : itemProductSkuDTOs) {
      skuIds.add(itemProductSkuDTO.getSkuId());
    }
    // 获取商品前台规格——一箱多少份或多少卷
    Map<Long, Integer> specificationMap = Maps.newHashMap();// 前台skuid和进售属性映射表
    Map<Long, Long> cPSkuIdMap = Maps.newHashMap();// 后台sku id和前台sku id映射表
    Map<Long, Long> pCSkuIdMap = Maps.newHashMap();// 前台sku id和后台sku id映射表
    Map<Long, Integer> skuBoxCostMap = Maps.newHashMap();// 后台sku id和进货成本映射表
    Result<List<ItemSkuRelationDTO>> skuRelationResult =
        skuService.querySkuRelationList(SkuRelationQuery.getQueryByPskuIds(skuIds, (short) 1));
    List<Long> cSkuIds = Lists.newArrayList();
    if (null != skuRelationResult && skuRelationResult.getSuccess()
        && null != skuRelationResult.getModule()) {
      List<ItemSkuRelationDTO> itemSkuRelationDTOs = skuRelationResult.getModule();
      for (ItemSkuRelationDTO itemSkuRelationDTO : itemSkuRelationDTOs) {
        skuBoxCostMap.put(itemSkuRelationDTO.getPskuId(), itemSkuRelationDTO.getBoxCost());
        if (null == itemSkuRelationDTO.getPcProportion()) {
          itemSkuRelationDTO.setPcProportion(1000);
        }
        specificationMap.put(itemSkuRelationDTO.getPskuId(),
            itemSkuRelationDTO.getPcProportion() / 1000);
        cPSkuIdMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getPskuId());
        pCSkuIdMap.put(itemSkuRelationDTO.getPskuId(), itemSkuRelationDTO.getCskuId());//TODO：优化
        cSkuIds.add(itemSkuRelationDTO.getCskuId());
      }
    }

    // 设置商品近7天销量
    Map<Long, ItemSaleCountDTO> itemSaleCountMapCache = Maps.newHashMap();
    Result<List<ItemSaleCountDTO>> itemSaleCountResult =
        itemSaleCountService.queryItemSaleCountLast7Days(cSkuIds);
    if (null != itemSaleCountResult && itemSaleCountResult.getSuccess()
        && CollectionUtils.isNotEmpty(itemSaleCountResult.getModule())) {
      List<ItemSaleCountDTO> itemSaleCountDTOs = itemSaleCountResult.getModule();
      for (ItemSaleCountDTO itemSaleCountDTO : itemSaleCountDTOs) {
        itemSaleCountMapCache.put(cPSkuIdMap.get(itemSaleCountDTO.getSkuId()), itemSaleCountDTO);
      }
    }
    // 获取后台商品规格属性
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(cSkuIds));
    Map<Long, ItemCommoditySkuDTO> itemCommoditySkuMap = Maps.newHashMap();
    if (null != itemCommoditySkuResult && itemCommoditySkuResult.getSuccess()
        && null != itemCommoditySkuResult.getModule()) {
      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
      for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
        itemCommoditySkuMap.put(cPSkuIdMap.get(itemCommoditySkuDTO.getSkuId()), itemCommoditySkuDTO);
      }
    }

    // 获取商品库存
    Map<Long, Integer> inventoryMap = batchQueryItemInventory(cSkuIds, cPSkuIdMap);
    // 查询供应商信息
    List<Integer> supplierIds = Lists.newArrayList();
    for (ItemProductSkuDTO itemProductSkuDTO : itemProductSkuDTOs) {
      supplierIds.add(itemProductSkuDTO.getSupplierId());
    }
    SupplierQuery supplierQuery = new SupplierQuery();
    supplierQuery.setSupplierIds(supplierIds);
    Result<List<SupplierDTO>> supplierResult = supplierService.querySupplier(supplierQuery);
    Map<Integer, String> supplierMap = Maps.newHashMap();
    if (null != supplierResult && supplierResult.getSuccess() && null != supplierResult.getModule()) {
      List<SupplierDTO> supplierDTOs = supplierResult.getModule();
      for (SupplierDTO supplierDTO : supplierDTOs) {
        supplierMap.put(supplierDTO.getUserId(), supplierDTO.getName());
      }
    }
    // 设置返回结果属性
    List<SubPurchaseItemVO> purchaseItemVOs = Lists.newArrayList();
    for (ItemProductSkuDTO itemProductSkuDTO : itemProductSkuDTOs) {
      SubPurchaseItemVO purchaseItemVO = new SubPurchaseItemVO();
      Long productSkuId = itemProductSkuDTO.getSkuId();
      Long commoditySkuId = 0L;
      if(null != pCSkuIdMap.get(productSkuId)){
        commoditySkuId = pCSkuIdMap.get(productSkuId);
      }
      ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuMap.get(productSkuId);
      if (null == itemCommoditySkuDTO) {
        logger.error("[itemProductSkuDTOs2PurchaseItemVOs] data error with product id is:"
            + productSkuId + ",commodity sku id is:" + commoditySkuId);
        continue;
      }
      purchaseItemVO.setSkuId(commoditySkuId);
      purchaseItemVO.setCatName(itemCategoryNameMap.get(itemProductSkuDTO.getItemId()));
      purchaseItemVO.setTitle(itemCommoditySkuDTO.getTitle());
      if (null != skuBoxCostMap.get(productSkuId)) {
        purchaseItemVO.setDiscountPrice(new BigDecimal(skuBoxCostMap.get(productSkuId)).divide(
            new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
      } else {
        purchaseItemVO.setDiscountPrice("0.00");
      }
      if (null != inventoryMap && null != inventoryMap.get(productSkuId)) {
        purchaseItemVO.setInventory(inventoryMap.get(productSkuId));
      } else {
        purchaseItemVO.setInventory(0);
      }
      purchaseItemVO.setPurchaseUnit("箱");
      purchaseItemVO.setSupplierId(itemProductSkuDTO.getSupplierId());
      purchaseItemVO.setSupplierName(supplierMap.get(itemProductSkuDTO.getSupplierId()));
      String saleUnitStr =
          (itemCommoditySkuDTO.getIsSteelyardSku() ? "公斤" : itemCommoditySkuDTO.getSaleUnitStr());
      if (StringUtils.isNotBlank(saleUnitStr)) {
        purchaseItemVO.setSaleUnit(saleUnitStr);
      }
      if (null != specificationMap.get(productSkuId)
          && StringUtils.isNotBlank(saleUnitStr)) {
        purchaseItemVO.setSpecification(new StringBuilder("1箱*")
            .append(specificationMap.get(productSkuId)).append("*")
            .append(saleUnitStr).toString());
      }
      ItemSaleCountDTO itemSaleCountDTO = itemSaleCountMapCache.get(productSkuId);
      if (null != itemSaleCountDTO) {
        purchaseItemVO.setSaleCountOffLine(new BigDecimal(itemSaleCountDTO.getSaleCountOffline())
            .setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        purchaseItemVO.setSaleCountOnLine(new BigDecimal(itemSaleCountDTO.getSaleCountOnline())
            .setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      } else {
        purchaseItemVO.setSaleCountOffLine("0.0");
        purchaseItemVO.setSaleCountOnLine("0.0");
      }
      purchaseItemVO.setSku69Code(itemCommoditySkuDTO.getSku69code());
      purchaseItemVOs.add(purchaseItemVO);
    }
    return purchaseItemVOs;
  }

  /**
   * 批量查询商品库存
   * 
   * @param cSkuIds
   * @param cPSkuIdMap
   * @return
   */
  public Map<Long, Integer> batchQueryItemInventory(List<Long> cSkuIds, Map<Long, Long> cPSkuIdMap) {
    SimpleResultVO<List<InventoryVO>> simpleResultVO = null;
    try {
      // 批量获取商品库存
      simpleResultVO = inventory2CDomainClient.batchGetInventoryBySKUID(1L, cSkuIds);
    } catch (Exception e) {
      logger.error("批量获取库存出错：" + e.getMessage());
      return Maps.newHashMap();
    }
    Map<Long, Integer> inventoryMap = Maps.newHashMap();// 前台sku id和后台库存映射关系表
    if (null != simpleResultVO && simpleResultVO.isSuccess()
        && CollectionUtils.isNotEmpty(simpleResultVO.getTarget())) {
      List<InventoryVO> inventoryVOs = simpleResultVO.getTarget();
      for (InventoryVO inventoryVO : inventoryVOs) {
        logger.error("商品sku id:" + inventoryVO.getSkuId() + "库存为：" + inventoryVO.getNumberTotal());
        if (null == inventoryVO.getNumberTotal()) {
          logger.error("商品sku id: " + inventoryVO.getSkuId() + "的库存为空");
        }
        inventoryMap.put(cPSkuIdMap.get(inventoryVO.getSkuId()), inventoryVO.getNumberTotal());
      }
    }
    return inventoryMap;
  }

  /**
   * 获取分页数量
   * 
   * @param number 数据总数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (null == pageSize) {
      return totalPage;
    }
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  /**
   * 查询默认采购商品数据
   * @param status 
   * 
   * @param pageSize
   * @param pageNum
   * @param pageNum2
   * @return
   */
  private PagedResult<List<SubPurchaseItemVO>> queryDefaultPurchaseItem(Integer supplierId,
      Short status, Integer pageSize, Integer pageNum) {
    if (null == pageSize) {
      pageSize = BaseQuery.DEFAULT_PAGE_SIZE;
    }
    if (null == pageNum) {
      pageNum = BaseQuery.DEFAULT_PAGE_NUM;
    }
    if (null == status) {
      status = ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES.shortValue();
    }
    SkuQuery skuQuery = SkuQuery.querySkuBySupplierAndTitle(supplierId, null, pageSize, pageNum);
    skuQuery.setStatuses(Arrays.asList(status));
    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(skuQuery);
    if (null == itemCommoditySkuResult || !itemCommoditySkuResult.getSuccess()
        || CollectionUtils.isEmpty(itemCommoditySkuResult.getModule())) {
      logger.error("查找采购商品失败：" + itemCommoditySkuResult.getErrorMsg());
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
    List<Long> cskuIds = Lists.newArrayList();
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      cskuIds.add(itemCommoditySkuDTO.getSkuId());
    }
    Result<List<ItemSkuRelationDTO>> itemSkuRelationResult =
        skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(cskuIds,
            RelationTypeConstants.RELATION_TYPE_DISCOUNT));
    if (null == itemSkuRelationResult || !itemSkuRelationResult.getSuccess()
        || CollectionUtils.isEmpty(itemSkuRelationResult.getModule())) {
      logger.error("服务内部错误或网络连接失败（cskuId）：" + JackSonUtil.getJson(cskuIds));
      return PagedResult
          .getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "网络连接失败，请稍后再试");
    }
    List<ItemSkuRelationDTO> itemSkuRelationDTOs = itemSkuRelationResult.getModule();
    List<Long> pskuIds = Lists.newArrayList();
    for (ItemSkuRelationDTO itemSkuRelationDTO : itemSkuRelationDTOs) {
      if (itemSkuRelationDTO.getPskuId() > 0) {
        pskuIds.add(itemSkuRelationDTO.getPskuId());
      }
    }
    Result<List<ItemProductSkuDTO>> itemProductSkuResult =
        skuService.queryProductSku(SkuQuery.querySkuBySkuIds(pskuIds));
    if (null == itemProductSkuResult || !itemProductSkuResult.getSuccess()
        || CollectionUtils.isEmpty(itemProductSkuResult.getModule())) {
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    List<ItemProductSkuDTO> itemProductSkuDTOs = itemProductSkuResult.getModule();
    Result<Integer> itemCommoditySkuCountResult = skuService.queryItemSkuCount(skuQuery);
    Integer totalNum = 0;
    if (null != itemCommoditySkuCountResult && itemCommoditySkuCountResult.getSuccess()
        && null != itemCommoditySkuCountResult.getModule()) {
      totalNum = itemCommoditySkuCountResult.getModule();
    }
    List<SubPurchaseItemVO> purchaseItemVOs =
        itemProductSkuDTOs2PurchaseItemVOs(itemProductSkuDTOs);
    return PagedResult.getSuccDataResult(purchaseItemVOs, totalNum,
        getTotalPage(totalNum, pageSize), pageSize, pageNum);
  }

//  private List<SubPurchaseItemVO> itemCommoditySkuDTOs2PurchaseItemVOs(
//      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs) {
//    for (ItemCommoditySkuDTO itemProductSkuDTO : itemCommoditySkuDTOs) {
//      SubPurchaseItemVO purchaseItemVO = new SubPurchaseItemVO();
//      Long productSkuId = itemProductSkuDTO.getSkuId();
//      Long commoditySkuId = 0L;
//      if(null != pCSkuIdMap.get(productSkuId)){
//        commoditySkuId = pCSkuIdMap.get(productSkuId);
//      }
//      purchaseItemVO.setSkuId(commoditySkuId);
//      purchaseItemVO.setCatName(itemCategoryNameMap.get(itemProductSkuDTO.getItemId()));
//      purchaseItemVO.setTitle(itemProductSkuDTO.getTitle());
//      if (null != skuBoxCostMap.get(productSkuId)) {
//        purchaseItemVO.setDiscountPrice(new BigDecimal(skuBoxCostMap.get(productSkuId)).divide(
//            new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
//      } else {
//        purchaseItemVO.setDiscountPrice("0.00");
//      }
//      if (null != inventoryMap && null != inventoryMap.get(productSkuId)) {
//        purchaseItemVO.setInventory(inventoryMap.get(productSkuId));
//      } else {
//        purchaseItemVO.setInventory(0);
//      }
//      purchaseItemVO.setPurchaseUnit("箱");
//      purchaseItemVO.setSupplierId(itemProductSkuDTO.getSupplierId());
//      purchaseItemVO.setSupplierName(supplierMap.get(itemProductSkuDTO.getSupplierId()));
//      if (StringUtils.isNotBlank(commoditySpecificationMap.get(productSkuId))) {
//        purchaseItemVO.setSaleUnit(commoditySpecificationMap.get(productSkuId));
//      }
//      if (null != specificationMap.get(productSkuId)
//          && StringUtils.isNotBlank(commoditySpecificationMap.get(productSkuId))) {
//        purchaseItemVO.setSpecification(new StringBuilder("1箱*")
//            .append(specificationMap.get(productSkuId)).append("*")
//            .append(commoditySpecificationMap.get(productSkuId)).toString());
//      }
//      ItemSaleCountDTO itemSaleCountDTO = itemSaleCountMapCache.get(productSkuId);
//      if (null != itemSaleCountDTO) {
//        purchaseItemVO.setSaleCountOffLine(Double.toString(itemSaleCountDTO.getSaleCountOffline()));
//        purchaseItemVO.setSaleCountOnLine(Double.toString(itemSaleCountDTO.getSaleCountOnline()));
//      } else {
//        purchaseItemVO.setSaleCountOffLine("0.0");
//        purchaseItemVO.setSaleCountOnLine("0.0");
//      }
//      purchaseItemVOs.add(purchaseItemVO);
//    }
//    return purchaseItemVOs;
//  }

  /**
   * 根据sku id查询采购商品数据
   * 
   * @param supplierId
   * 
   * @param skuId
   * @param status 
   * @param pageSize
   * @param pageNum
   * @return
   */
  private PagedResult<List<SubPurchaseItemVO>> queryItemBySkuId(Integer supplierId, Long skuId,
      Short status, Integer pageSize, Integer pageNum) {
    if (null == skuId) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "商品名称为空");
    }
    if(null == status){
      status = ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES.shortValue();
    }
    SkuQuery skuQuery = SkuQuery.querySkuById(skuId);
    skuQuery.setStatuses(Arrays.asList(status));
    Result<ItemCommoditySkuDTO> itemCommodityResult = skuService.queryItemSku(skuQuery);
    if(null == itemCommodityResult || !itemCommodityResult.getSuccess() || null == itemCommodityResult.getModule()){
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    Result<List<ItemSkuRelationDTO>> itemSkuRelationResult =
        skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(Arrays.asList(skuId),
            RelationTypeConstants.RELATION_TYPE_DISCOUNT));
    if (null == itemSkuRelationResult || !itemSkuRelationResult.getSuccess()
        || CollectionUtils.isEmpty(itemSkuRelationResult.getModule())) {
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    ItemSkuRelationDTO itemSkuRelationDTO = itemSkuRelationResult.getModule().get(0);
    Long pskuId = itemSkuRelationDTO.getPskuId();
    Result<List<ItemProductSkuDTO>> itemProductResult =
        skuService.queryProductSku(SkuQuery.querySkuBySkuIdAndSupplier(pskuId, supplierId));
    if (null == itemProductResult || !itemProductResult.getSuccess()
        || CollectionUtils.isEmpty(itemProductResult.getModule())) {
      logger.error("查找与" + skuId + "相关的商品失败：" + itemProductResult.getErrorMsg());
      return PagedResult.getSuccDataResult(null, 0, 1, pageSize, pageNum);
    }
    List<ItemProductSkuDTO> itemProductSkuDTOs = itemProductResult.getModule();
    List<SubPurchaseItemVO> purchaseItemVOs =
        itemProductSkuDTOs2PurchaseItemVOs(itemProductSkuDTOs);
    Integer totalNum = 1;
    return PagedResult.getSuccDataResult(purchaseItemVOs, totalNum, 1, pageSize, pageNum);
  }

  /**
   * 解析商品sku状态
   * @param status
   * @return
   */
  private String parseItemSkuStatus(Short status) {
    if(null == status){
      return null;
    }
    String statuString = null;
    switch (status) {
      case -1:
        statuString = "审核不通过";
        break;
      case 2:
        statuString = "审核通过并未上架";
        break;
      case 1:
        statuString = "审核通过并上架";
        break;
    }
    return statuString;
  }

  /**
   * 获取优惠券
   */
  public List<CouponDTO> getCouponDTO(CouponsQuery couponQuery) {
    if (null == couponQuery) {
      throw new BizException("优惠券参数对象为空");
    }
    Result<List<CouponDTO>> couponResult = couponService.query(couponQuery);
    if (null == couponResult || !couponResult.getSuccess()) {
      throw new BizException(couponResult.getErrorMsg());
    }
    return couponResult.getModule();
  }

  /**
   * 是否存在优惠券
   */
  public Boolean hasCouponDTO(CouponsQuery couponQuery) {
    if (null == couponQuery) {
      throw new BizException("优惠券参数对象为空");
    }
    Result<List<CouponDTO>> couponResult = couponService.query(couponQuery);
    if (null == couponResult || !couponResult.getSuccess()) {
      return false;
    }
    if (CollectionUtils.isEmpty(couponResult.getModule())) {
      return false;
    }
    return true;
  }

  /**
   * 更新优惠券
   */
  public void updateCouponDTO(CouponDTO couponDTO) {
    if (null == couponDTO) {
      throw new BizException("优惠券参数对象为空");
    }
    Result<Boolean> update = couponService.update(couponDTO);
    if (null == update || !update.getSuccess()) {
      throw new BizException("更新优惠券失败");
    }
  }

  /**
   * 更新优惠券
   */
  public void insertCouponDTO(CouponDTO couponDTO) {
    if (null == couponDTO) {
      throw new BizException("优惠券参数对象为空");
    }
    Result<Long> insertResult = couponService.insert(couponDTO);
    if (null == insertResult || !insertResult.getSuccess()) {
      throw new BizException("生成优惠券失败");
    }
  }
}
