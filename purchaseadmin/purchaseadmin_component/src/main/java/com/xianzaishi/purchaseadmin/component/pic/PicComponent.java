package com.xianzaishi.purchaseadmin.component.pic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.pic.PicConstants;
import com.xianzaishi.itemcenter.common.result.Result;

@Component("picComponent")
public class PicComponent {

  private static final String SERVER_URL = "http://static.xianzaishi.com:7009/pic/upload";

  private static final Logger logger = Logger.getLogger(PicComponent.class);

  private static final int PIC_WIDTH = 900;

  public Result<List<String>> updatePic(List<HSSFPicture> picList, String folderPath, int biztype) {

    DefaultHttpClient httpClient = new DefaultHttpClient();
    HttpPost request = new HttpPost(SERVER_URL);
    List<String> result = Lists.newArrayList();
    boolean presswidth = false;
    if(2== biztype){//商品图片
      presswidth = true;
    }
    
    try {
      List<byte[]> fileList = Lists.newArrayList();
      for (HSSFPicture pic : picList) {
        byte[] fileByte = pic.getPictureData().getData();
        File pressPic = null;
        if (StringUtils.isNotEmpty(folderPath)) {
          File folderP = new File(folderPath);
          if (!folderP.exists()) {
            folderP.mkdirs();
          }

          File file = new File(folderPath + "/" + new Date().getTime() + ".jpg");
          FileOutputStream fos = new FileOutputStream(file);
          fos.write(fileByte);
          fos.close();

          pressPic = new File(folderPath + "/" + new Date().getTime() + "press.jpg");
          pressPic = compressPic(file, pressPic, presswidth);
        }

        MultipartEntity multipartEntity =
            new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
                "----------ThIs_Is_tHe_bouNdaRY_$", Charset.defaultCharset());
        if (null != pressPic) {
          multipartEntity.addPart("img", new FileBody(pressPic, "image/png"));
        } else {
          multipartEntity.addPart("img", new ByteArrayBody(fileByte, "image/png"));
        }
        multipartEntity.addPart("token", new StringBody("BC7836FD25EDDBE3"));
        multipartEntity.addPart("bizid", new StringBody(String.valueOf(biztype)));

        request.setEntity(multipartEntity);
        request.addHeader("Content-Type",
            "multipart/form-data; boundary=----------ThIs_Is_tHe_bouNdaRY_$");

        if(null != pressPic){
          logger.info("Start update pic:"+pressPic.getName());
        }
        Thread.sleep(100);
        HttpResponse response = httpClient.execute(request);
        logger.info("end update pic");
        InputStream is = response.getEntity().getContent();
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        while ((line = in.readLine()) != null) {
          buffer.append(line);
        }
        in.close();
        String returnStr = buffer.toString();
        if (StringUtils.isEmpty(returnStr)) {
          return Result.getErrDataResult(-2, "update pic failed");
        }
        Result<String> resultObj =
            (Result<String>) JackSonUtil.jsonToObject(returnStr, Result.class);
        if (resultObj == null || !resultObj.getSuccess()) {
          return Result.getErrDataResult(-2, "update pic failed");
        } else {
          result.add(resultObj.getModule().replace(PicConstants.PIC_PREFIX, ""));
        }
      }
    } catch (Exception e) {
      httpClient.close();
    } finally {
      httpClient.close();
    }
    return Result.getSuccDataResult(result);
  }

  public Result<List<String>> updateFilePic(List<File> picList, String folderPath, int biztype) {

    DefaultHttpClient httpClient = new DefaultHttpClient();
    HttpPost request = new HttpPost(SERVER_URL);
    List<String> result = Lists.newArrayList();
    boolean presswidth = false;
    if(2== biztype){//商品图片
      presswidth = true;
    }
    try {

      for (File pic : picList) {

        String picName = pic.getName();
        if(picName.startsWith(".")){
          continue;
        }
        if (StringUtils.isNotEmpty(folderPath)) {
          File folderP = new File(folderPath);
          if (!folderP.exists()) {
            folderP.mkdirs();
          }
        }
        File pressPic = new File(folderPath + "/" + pic.getName() + "press.jpg");
        logger.error("zhancang test before press :"+folderPath + "/" + pic.getName() + "press.jpg");
        if (StringUtils.isNotEmpty(folderPath)) {
          pressPic = this.compressPic(pic, pressPic,presswidth);
          if (null == pressPic) {
            pressPic = pic;
          }
        }

        MultipartEntity multipartEntity =
            new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
                "----------ThIs_Is_tHe_bouNdaRY_$", Charset.defaultCharset());
        multipartEntity.addPart("img", new FileBody(pressPic, "image/png"));
        multipartEntity.addPart("token", new StringBody("BC7836FD25EDDBE3"));
        multipartEntity.addPart("bizid", new StringBody(String.valueOf(biztype)));

        request.setEntity(multipartEntity);
        request.addHeader("Content-Type",
            "multipart/form-data; boundary=----------ThIs_Is_tHe_bouNdaRY_$");

        logger.error("Start update pic:"+pressPic.getPath());
        HttpResponse response = httpClient.execute(request);
        Thread.sleep(100);
        logger.info("end update pic");
        InputStream is = response.getEntity().getContent();
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        while ((line = in.readLine()) != null) {
          buffer.append(line);
        }
        in.close();
        String returnStr = buffer.toString();
        if (StringUtils.isEmpty(returnStr)) {
          return Result.getErrDataResult(-2, "update pic failed");
        }
        Result<String> resultObj =
            (Result<String>) JackSonUtil.jsonToObject(returnStr, Result.class);
        if (resultObj == null || !resultObj.getSuccess()) {
          return Result.getErrDataResult(-2, "update pic failed");
        } else {
          logger.error("end update pic:"+pressPic.getPath()+" Result:"+resultObj.getModule().replace(PicConstants.PIC_PREFIX, ""));
          result.add(resultObj.getModule().replace(PicConstants.PIC_PREFIX, ""));
        }
      }
    } catch (Exception e) {
      httpClient.close();
    } finally {
      httpClient.close();
    }
    return Result.getSuccDataResult(result);
  }

  // 图片处理
  public File compressPic(File file, File outfile, boolean isPresswidth) {
    logger.info("Start press pic:"+file.getName());
    Image src = Toolkit.getDefaultToolkit().getImage(file.getPath());

    BufferedImage image = toBufferedImage(src);// Image to BufferedImage
    try {
      int w = image.getWidth(null);
      int h = image.getHeight(null);

      int newWidth;
      int newHeight;
      if (w > PIC_WIDTH && isPresswidth) {
        // 判断是否是等比缩放
        // 为等比缩放计算输出的图片宽度及高度
        double rate = ((double) w) / PIC_WIDTH - 0.1;
        // 根据缩放比率大的进行缩放控制
        newWidth = (int) (((double) w) / rate);
        newHeight = (int) (((double) h) / rate);

        BufferedImage pressImg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        pressImg.getGraphics().drawImage(
            image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH), 0, 0, Color.white, null);

        ImageIO.write(pressImg, "jpeg", outfile);
        pressImg.flush();
      } else {
        outfile = file;
//        ImageIO.write(image, "jpeg", outfile);
//        image.flush();
      }
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
    logger.info("end press pic");
    return outfile;
  }

  public BufferedImage toBufferedImage(Image image) {
    if (image instanceof BufferedImage) {
      return (BufferedImage) image;
    }
    // This code ensures that all the pixels in the image are loaded
    image = new ImageIcon(image).getImage();

    // Determine if the image has transparent pixels; for this method's
    // implementation, see e661 Determining If an Image Has Transparent Pixels
    // boolean hasAlpha = hasAlpha(image);
    // Create a buffered image with a format that's compatible with the screen
    BufferedImage bimage = null;
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    try {
      // Determine the type of transparency of the new buffered image
      int transparency = Transparency.OPAQUE;
      /*
       * if (hasAlpha) { transparency = Transparency.BITMASK; }
       */

      // Create the buffered image
      GraphicsDevice gs = ge.getDefaultScreenDevice();
      GraphicsConfiguration gc = gs.getDefaultConfiguration();
      bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);
    } catch (HeadlessException e) {
      // The system does not have a screen
    }

    if (bimage == null) {
      // Create a buffered image using the default color model
      int type = BufferedImage.TYPE_INT_RGB;
      // int type = BufferedImage.TYPE_3BYTE_BGR;//by wang
      /*
       * if (hasAlpha) { type = BufferedImage.TYPE_INT_ARGB; }
       */
      bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
    }

    // Copy image to buffered image
    Graphics g = bimage.createGraphics();

    // Paint the image onto the buffered image
    g.drawImage(image, 0, 0, Color.white,null);
    g.dispose();

    return bimage;
  }

  public static void main(String args[]) {
    PicComponent pc = new PicComponent();
    String inputPath = "D:/work/input/imgtest/";
    String outputPath = "D:/work/input/press/";
    File file = new File(inputPath);
    for (String tmpName : file.list()) {
      File input = new File(inputPath + tmpName);
      String name = input.getName();
      if(name.startsWith(".")){
        System.out.println("我晕");
        continue;
      }
      File outfile = new File(outputPath + name);
      pc.compressPic(input, outfile, true);
    }
    System.out.println("test end");
  }
}
