package com.xianzaishi.purchaseadmin.component.order;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.msg.domain.RpcResult;
import com.xianzaishi.purchaseadmin.client.item.vo.SubPurchaseItemVO;
import com.xianzaishi.purchaseadmin.client.order.vo.OrderCollectionVO;
import com.xianzaishi.purchaseadmin.client.order.vo.OrderListQueryVO;
import com.xianzaishi.purchaseadmin.client.order.vo.OrderListVO;
import com.xianzaishi.purchasecenter.client.mail.MailService;
import com.xianzaishi.purchasecenter.client.mail.dto.SheetDTO;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseDeliveryStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseOrderAuditingStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.EmployeeService;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;
import com.xianzaishi.purchasecenter.client.user.dto.EmployeeDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.service.MessageService;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IStorageDomainClient;
import com.xianzaishi.wms.track.vo.StorageDetailVO;

/**
 * 采购单列表
 * 
 * @author dongpo
 * 
 */
@Component("orderListComponent")
public class OrderListComponent {

  @Autowired
  private SkuService skuService;

  @Autowired
  private SupplierService supplierService;

  @Autowired
  private PurchaseService purchaseService;

  @Autowired
  private BackGroundUserService backGroundUserService;

  @Autowired
  private EmployeeService employeeService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private MailService mailService;

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private StdCategoryService stdCategoryService;

  @Autowired
  private IStorageDomainClient storageDomainClient;


  private static final Logger LOGGER = Logger.getLogger(OrderListComponent.class);

  /**
   * 默认收件人邮箱
   */
  private static String recipient = null;

  /**
   * 查询采购单列表
   * 
   * @param orderListQueryVO
   * @param userDTO
   * @return
   */
  public String queryOrderList(OrderListQueryVO orderListQueryVO, BackGroundUserDTO userDTO) {
    if (null == orderListQueryVO) {
      orderListQueryVO = new OrderListQueryVO();
    }
    if (null == orderListQueryVO.getPageSize()) {
      orderListQueryVO.setPageSize(8);
    }
    if (null == orderListQueryVO.getPageNum()) {
      orderListQueryVO.setPageNum(0);
    }
    String result = null;
    if (null != orderListQueryVO.getPurchaseId()) {
      result =
          JackSonUtil.getJson(createSubOrderByPurchaseId(orderListQueryVO.getPurchaseId(),
              orderListQueryVO.getAuditingStatus(), userDTO));
    } else {
      result = toJsonData(createSubOrderByOthers(orderListQueryVO, userDTO));
    }
    return result;
  }


  /**
   * 请求默认数据
   * 
   * @param userDTO
   * @param pageSize
   * @param pageNum
   * @param status
   * @return
   */
  private PagedResult<OrderCollectionVO> createDefaultData(BackGroundUserDTO userDTO,
      Integer pageSize, Integer pageNum, Short status) {
    if (null == pageNum) {
      pageNum = 0;// 默认当前页号
    }
    if (null == pageSize) {
      pageSize = 8;//
    }
    PurchaseQuery purchaseQuery = new PurchaseQuery();
    if (null != status) {
      if (PurchaseSubOrderStatusConstants.isCorrectParameter(status)) {
        purchaseQuery.setStatus(status);
      } else if (PurchaseSubOrderFlowStatusConstants.isCorrectParameter(status)) {
        purchaseQuery.setFlowStatus(status);
      }
    }
    if (null != userDTO && null != userDTO.getUserId()
        && userDTO.getUserType().equals(UserTypeConstants.USER_SUPPLIER)) {
      purchaseQuery.setSupplierId(userDTO.getUserId());
    }
    purchaseQuery.setPageSize(pageSize);
    purchaseQuery.setPageNum(pageNum);
    PagedResult<List<PurchaseOrderDTO>> purchaseResult =
        purchaseService.queryPurchase(purchaseQuery);// 查询子采购单数据
    if (null == purchaseResult || !purchaseResult.getSuccess()) {
      String errorMsg = (null == purchaseResult ? "请求采购单数据失败" : purchaseResult.getErrorMsg());
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    List<PurchaseOrderDTO> purchaseOrderDTOs = purchaseResult.getModule();
    if (CollectionUtils.isEmpty(purchaseOrderDTOs)) {
      return PagedResult.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT, "采购单数据为空");
    }
    List<Integer> supplierIds = Lists.newArrayList();
    for (PurchaseOrderDTO purchaseOrderDTO : purchaseOrderDTOs) {
      supplierIds.add(purchaseOrderDTO.getSupplierId());
    }

    Result<List<BackGroundUserDTO>> supplierResult =
        backGroundUserService.queryUserDTOByIdList(supplierIds);// 根据供应商id查询供应商数据
    if (null == supplierResult || !supplierResult.getSuccess()) {
      String errorMsg = (null == supplierResult ? "请求供应商数据失败" : supplierResult.getErrorMsg());
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    List<BackGroundUserDTO> backGroundUserDTOs = supplierResult.getModule();
    if (null == backGroundUserDTOs || backGroundUserDTOs.size() == 0) {
      return PagedResult.getErrDataResult(ServerResultCode.SUPPLIER_ERROR_CODE_NOT_EXIT, "供应商不存在");
    }
    Map<Integer, String> supplierMap = Maps.newHashMap();
    for (BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs) {
      supplierMap.put(backGroundUserDTO.getUserId(), backGroundUserDTO.getName());
    }

    List<OrderListVO> orderListVOs = Lists.newArrayList();
    for (PurchaseOrderDTO purchaseOrderDTO : purchaseOrderDTOs) {
      OrderListVO orderListVO = new OrderListVO();
      // 获取总采购单编号
      orderListVO.setPurchaseId(purchaseOrderDTO.getPurchaseId());
      // 获取采购供应商
      orderListVO.setSupplier(supplierMap.get(purchaseOrderDTO.getSupplierId()));
      Short auditingStatus = purchaseOrderDTO.getAuditingStatus();
      orderListVO.setAuditingStatus(auditingStatus);
      orderListVO.setAuditingStatusString(pareseAuditingStatus(auditingStatus));
      Short deliveryStatus = purchaseOrderDTO.getDeliveryStatus();
      // 获取采购金额
      if (null != purchaseOrderDTO.getActualAmount()) {
        orderListVO.setPurchaseAmount(BigDecimal.valueOf(purchaseOrderDTO.getActualAmount())
            .divide(new BigDecimal(100)).toString());
      }
      // 获取采购时间
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      orderListVO.setPurchaseData(sdf.format(purchaseOrderDTO.getGmtCreate()));
      // TODO:获取采购入库状态以及付款状态
      orderListVO.setStorageStatus("未入库");
      orderListVO.setStorageStatusCode((short) 2);
      orderListVO.setPayStatus("未付款");
      orderListVO.setPayStatusCode((short) 2);
      orderListVO.setRemarks(purchaseOrderDTO.getRemarks());
      orderListVOs.add(orderListVO);
    }
    OrderCollectionVO orderCollectionVO = new OrderCollectionVO();
    orderCollectionVO.setOrderListVOs(orderListVOs);
    return PagedResult.getSuccDataResult(orderCollectionVO, purchaseResult.getTotalNum(),
        purchaseResult.getTotalPageCount(), pageSize, pageNum);
  }

  /**
   * 根据订单id请求总采购单
   * 
   * @param purchaseSubId
   * @param status
   * @param userDTO
   * @return
   */
  private PagedResult<OrderCollectionVO> createSubOrderByPurchaseId(Integer parentPurchaseId,
      Short status, BackGroundUserDTO userDTO) {
    OrderListVO orderListVO = new OrderListVO();
    PurchaseQuery query = new PurchaseQuery();
    query.setParentPurchaseId(parentPurchaseId);

    // 添加权限过滤
    setPurchaseQueryByAuthority(userDTO, query, false);
    if (null != status) {
      query.setAuditingStatus(Arrays.asList(status));
    }
    PagedResult<List<PurchaseOrderDTO>> purchaseResult = purchaseService.queryPurchase(query);
    if (null == purchaseResult || !purchaseResult.getSuccess()) {
      String errorMsg = (null == purchaseResult ? "采购单数据请求失败" : "没有此采购单相关信息");
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    List<PurchaseOrderDTO> purchaseOrderDTOs = purchaseResult.getModule();
    if (CollectionUtils.isEmpty(purchaseOrderDTOs)) {
      return PagedResult.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "没有找到与查询条件相关的采购单");
    }
    PurchaseOrderDTO purchaseOrderDTO = purchaseOrderDTOs.get(0);

    Integer supplierId = purchaseOrderDTO.getSupplierId();
    Result<SupplierDTO> supplierResult = supplierService.querySupplierById(supplierId, false);
    if (null == supplierResult || !supplierResult.getSuccess()) {
      String errorMsg = (null == supplierResult ? "供应商数据请求失败" : "没有该供应商相关信息");
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    SupplierDTO supplierDTO = supplierResult.getModule();
    // 获取总采购单编号
    orderListVO.setPurchaseId(purchaseOrderDTO.getPurchaseId());
    // 获取采购供应商
    orderListVO.setSupplier(supplierDTO.getName());
    Short auditingStatus = purchaseOrderDTO.getAuditingStatus();
    orderListVO.setAuditingStatus(auditingStatus);
    orderListVO.setAuditingStatusString(pareseAuditingStatus(auditingStatus));
    Short deliveryStatus = purchaseOrderDTO.getDeliveryStatus();
    // 获取采购金额
    if (null != purchaseOrderDTO.getTotalAmount()) {
      orderListVO.setPurchaseAmount(new BigDecimal(purchaseOrderDTO.getTotalAmount()).divide(
          new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
    }
    // 获取采购时间
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    orderListVO.setPurchaseData(sdf.format(purchaseOrderDTO.getGmtCreate()));
    // TODO:获取采购入库状态以及付款状态
    if (null == deliveryStatus
        || PurchaseDeliveryStatusConstants.STATUS_NOT_DISTRIBUTION.equals(deliveryStatus)
        || PurchaseDeliveryStatusConstants.STATUS_IN_DISTRIBUTION.equals(deliveryStatus)) {
      orderListVO.setArriveDate("未到货");
      orderListVO.setStorageStatus("未入库");
    } else if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART.equals(deliveryStatus)) {
      orderListVO.setArriveDate("部分到货");
      orderListVO.setStorageStatus("部分入库");
    } else if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL.equals(deliveryStatus)
        || PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE
            .equals(auditingStatus)) {
      orderListVO.setArriveDate(sdf.format(purchaseOrderDTO.getGmtModified()));
      orderListVO.setStorageStatus("全部入库");
    }
    orderListVO.setStorageStatus("未入库");
    orderListVO.setStorageStatusCode((short) 2);
    if (null != purchaseOrderDTO.getActualAmount()) {
      orderListVO.setActualAmount(new BigDecimal(purchaseOrderDTO.getActualAmount()).divide(
          new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
      orderListVO.setPayStatus("已付款");
      orderListVO.setPayStatusCode((short) 2);
    }else{
      orderListVO.setPayStatus("未付款");
      orderListVO.setPayStatusCode((short) 0);
    }
    orderListVO.setPayStatusCode((short) 2);
    orderListVO.setRemarks(purchaseOrderDTO.getRemarks());
    List<OrderListVO> orderListVOs = Lists.newArrayList();
    orderListVOs.add(orderListVO);
    OrderCollectionVO orderCollectionVO = new OrderCollectionVO();
    orderCollectionVO.setOrderListVOs(orderListVOs);
    return PagedResult.getSuccDataResult(orderCollectionVO, 1, 1, 1, 0);
  }

  /**
   * 根据其他查询条件（采购人、供应商、商品名称）请求采购单列表数据
   * 
   * @param name
   * @param requestType
   * @param status
   * @param pageSize
   * @param pageNum
   * @param userDTO
   * @return
   */
  private PagedResult<OrderCollectionVO> createSubOrderByOthers(OrderListQueryVO orderListQueryVO,
      BackGroundUserDTO userDTO) {
    PurchaseQuery purchaseQuery = new PurchaseQuery();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    // 当该用户为店内员工并且有采购员权限的时候是否要查询自己采购的所有的订单
    boolean querySelfPurchaseOrder = true;
    try {
      if (null != orderListQueryVO.getBegin()) {
        purchaseQuery.setPurchaseBegin(sdf.parse(orderListQueryVO.getBegin()));
        querySelfPurchaseOrder = false;
      }
      if (null != orderListQueryVO.getEnd()) {
        purchaseQuery.setPurchaseEnd(sdf.parse(orderListQueryVO.getEnd()));
        querySelfPurchaseOrder = false;
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    String supplier = orderListQueryVO.getSupplier();
    if (StringUtils.isNotBlank(supplier)) {
      Result<SupplierDTO> supplierResult =
          supplierService.querySupplierByName(orderListQueryVO.getSupplier(), false);
      if (!supplierResult.getSuccess()) {
        String errorMsg = (null == supplierResult ? "请求供应商数据失败" : "没有该供应商相关信息");
        return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
      }
      SupplierDTO supplierDTO = supplierResult.getModule();
      if (null == supplierDTO) {
        return PagedResult.getErrDataResult(ServerResultCode.SUPPLIER_ERROR_CODE_NOT_EXIT, "没有找到与"
            + supplier + "相关的供应商");
      }
      purchaseQuery.setSupplierId(supplierDTO.getUserId());
      querySelfPurchaseOrder = false;
    }
    Integer pageSize = orderListQueryVO.getPageSize();
    Integer pageNum = orderListQueryVO.getPageNum();
    purchaseQuery.setPageSize(pageSize);
    purchaseQuery.setPageNum(pageNum);
    if (null != orderListQueryVO.getAuditingStatus()) {
      querySelfPurchaseOrder = false;
    }
    // 添加权限过滤
    setPurchaseQueryByAuthority(userDTO, purchaseQuery, querySelfPurchaseOrder);
    // 根据审核状态查询
    Short auditingStatus = orderListQueryVO.getAuditingStatus();
    if (null != auditingStatus) {
      purchaseQuery.setAuditingStatus(Arrays.asList(auditingStatus));
      if (auditingStatus == 0 && null == purchaseQuery.getPurchasingAgent()) {
        purchaseQuery.setPurchasingAgent(userDTO.getUserId());
      }
    }
    // 根据查询条件查询采购单
    PagedResult<List<PurchaseOrderDTO>> purchaseResult =
        purchaseService.queryPurchase(purchaseQuery);
    if (null == purchaseResult || !purchaseResult.getSuccess()) {
      String errorMsg = (null == purchaseResult ? "采购单数据请求失败" : "没有此采购单相关信息");
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    List<PurchaseOrderDTO> purchaseOrderDTOs = purchaseResult.getModule();
    if (CollectionUtils.isEmpty(purchaseOrderDTOs)) {
      return PagedResult.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "不存在与查询条件相关的采购单");
    }

    List<Integer> supplierIds = Lists.newArrayList();
    for (PurchaseOrderDTO purchaseOrderDTO : purchaseOrderDTOs) {
      supplierIds.add(purchaseOrderDTO.getSupplierId());
    }
    // 根据供应商id查询供应商数据
    Result<List<BackGroundUserDTO>> supplierResult =
        backGroundUserService.queryUserDTOByIdList(supplierIds);
    if (null == supplierResult || !supplierResult.getSuccess()) {
      String errorMsg = (null == supplierResult ? "请求供应商数据失败" : "没有该供应商相关信息");
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    List<BackGroundUserDTO> backGroundUserDTOs = supplierResult.getModule();
    if (null == backGroundUserDTOs || backGroundUserDTOs.size() == 0) {
      return PagedResult.getErrDataResult(ServerResultCode.SUPPLIER_ERROR_CODE_NOT_EXIT, "供应商不存在");
    }
    Map<Integer, String> supplierMap = Maps.newHashMap();
    for (BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs) {
      supplierMap.put(backGroundUserDTO.getUserId(), backGroundUserDTO.getName());
    }

    List<OrderListVO> orderListVOs = Lists.newArrayList();
    for (PurchaseOrderDTO purchaseOrderDTO : purchaseOrderDTOs) {
      OrderListVO orderListVO = new OrderListVO();
      // 获取总采购单编号
      orderListVO.setPurchaseId(purchaseOrderDTO.getPurchaseId());
      // 获取采购供应商
      orderListVO.setSupplier(supplierMap.get(purchaseOrderDTO.getSupplierId()));
      Short auditingStatusDb = purchaseOrderDTO.getAuditingStatus();
      orderListVO.setAuditingStatus(auditingStatusDb);
      orderListVO.setAuditingStatusString(pareseAuditingStatus(auditingStatusDb));
      Short deliveryStatus = purchaseOrderDTO.getDeliveryStatus();
      // 获取采购金额
      if (null != purchaseOrderDTO.getTotalAmount()) {
        orderListVO.setPurchaseAmount(new BigDecimal(purchaseOrderDTO.getTotalAmount()).divide(
            new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
      }
      // 获取采购时间
      orderListVO.setPurchaseData(sdf.format(purchaseOrderDTO.getGmtCreate()));
      if (null == deliveryStatus
          || PurchaseDeliveryStatusConstants.STATUS_NOT_DISTRIBUTION.equals(deliveryStatus)
          || PurchaseDeliveryStatusConstants.STATUS_IN_DISTRIBUTION.equals(deliveryStatus)) {
        orderListVO.setArriveDate("未到货");
        orderListVO.setStorageStatus("未入库");
      } else if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART.equals(deliveryStatus)) {
        orderListVO.setArriveDate("部分到货");
        orderListVO.setStorageStatus("部分入库");
      } else if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL.equals(deliveryStatus)
          || PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE
              .equals(auditingStatusDb)) {
        orderListVO.setArriveDate(sdf.format(purchaseOrderDTO.getGmtModified()));
        orderListVO.setStorageStatus("全部入库");
      }
      // TODO:获取采购入库状态以及付款状态
      orderListVO.setStorageStatusCode((short) 0);
      if (null != purchaseOrderDTO.getActualAmount()) {
        orderListVO.setActualAmount(new BigDecimal(purchaseOrderDTO.getActualAmount()).divide(
            new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
        orderListVO.setPayStatus("已付款");
        orderListVO.setPayStatusCode((short) 2);
      }else{
        orderListVO.setPayStatus("未付款");
        orderListVO.setPayStatusCode((short) 0);
      }
      orderListVO.setRemarks(purchaseOrderDTO.getRemarks());
      orderListVOs.add(orderListVO);
    }
    OrderCollectionVO orderCollectionVO = new OrderCollectionVO();
    orderCollectionVO.setOrderListVOs(orderListVOs);
    return PagedResult.getSuccDataResult(orderCollectionVO, purchaseResult.getTotalNum(),
        purchaseResult.getTotalPageCount(), pageSize, pageNum);
  }

  /**
   * 添加权限过滤
   * 
   * @param userDTO
   * @param purchaseQuery
   * @param querySelfPurchaseOrder
   */
  private void setPurchaseQueryByAuthority(BackGroundUserDTO userDTO, PurchaseQuery purchaseQuery,
      Boolean querySelfPurchaseOrder) {
    Short userType = null;
    List<Integer> role = Lists.newArrayList();
    if (null != userDTO && null != userDTO.getUserId()) {
      userType = userDTO.getUserType();
      role = userDTO.queryRoleList();
    }
    if (CollectionUtils.isEmpty(role)) {
      role = Lists.newArrayList();
    }
    if (null != userDTO && null != userDTO.getUserId()
        && UserTypeConstants.USER_EMPLOYEE.equals(userType) && !role.contains(81)
        && !role.contains(82)) {
      purchaseQuery.setPurchasingAgent(userDTO.getUserId());
    }
    // 如果仅仅是采购人员那么只能查询自己下的采购单
    if (UserTypeConstants.USER_EMPLOYEE.equals(userType)
        && (role.contains(81) || role.contains(82))) {
      List<Short> auditingStatusList = Lists.newArrayList();
      if (role.contains(81)) {
        auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_FIRST_AUDITING);
      }
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_FIRST_AUDITING_PASS);
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS);
      auditingStatusList
          .add(PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_DISTRIBUTION);
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_PASS);
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE);
      purchaseQuery.setAuditingStatus(auditingStatusList);
      // 如果即是采购主管又是采购人员，那么除了查询别人的待审核采购单外还可以查询自己下的采购单
      if (role.contains(80) && querySelfPurchaseOrder) {
        purchaseQuery.setOrPurchasingAgent(userDTO.getUserId());
      } else if (role.contains(80) && !querySelfPurchaseOrder) {
        purchaseQuery.setOrPurchasingAgent(userDTO.getUserId());
        purchaseQuery.setOrParentPurchaseId(purchaseQuery.getParentPurchaseId());
        purchaseQuery.setOrPurchaseBegin(purchaseQuery.getPurchaseBegin());
        purchaseQuery.setOrPurchaseEnd(purchaseQuery.getPurchaseEnd());
        purchaseQuery.setOrSupplierId(purchaseQuery.getSupplierId());
      }
    }
    if (null != userDTO && null != userDTO.getUserId()
        && UserTypeConstants.USER_SUPPLIER.equals(userType)) {
      purchaseQuery.setSupplierId(userDTO.getUserId());
      // 供应商只能看得到终审通过的采购单
      List<Short> auditingStatusList = Lists.newArrayList();
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS);
      auditingStatusList
          .add(PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_DISTRIBUTION);
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_PASS);
      auditingStatusList.add(PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE);
      purchaseQuery.setAuditingStatus(auditingStatusList);
    }
  }


  /**
   * 解析审核状态
   * 
   * @param auditingStatus
   * @return
   */
  private String pareseAuditingStatus(Short auditingStatus) {
    String statusName = null;
    switch (auditingStatus) {
      case 0:
        statusName = "草稿状态";
        break;
      case 1:
        statusName = "待审核状态";
        break;
      case 2:
        statusName = "初审完成，待提交供应商";
        break;
      case 3:
        statusName = "终审完成，待供应商审核";
        break;
      case 4:
        statusName = "待配送";
        break;
      case 5:
        statusName = "配送完成，待收货";
        break;
      case 8:
        statusName = "收货完成";
        break;
    }
    return statusName;
  }


  /**
   * 获取分页数量
   * 
   * @param number 商品数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (null == pageSize || null == number) {
      return totalPage;
    }
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }


  /**
   * 根据总订单编号查询订单详情
   * 
   * @param purchaseId
   * @return
   */
  public PagedResult<Map<String, Object>> queryOrderDetail(Integer purchaseId, Integer pageSize,
      Integer pageNum) {
    if (null == purchaseId) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    PurchaseQuery query = new PurchaseQuery();
    query.setParentPurchaseId(purchaseId);
    query.setPageNum(0);
    query.setPageSize(2000);
    Result<Map<String, Object>> subPurchaseResult = purchaseService.querySubPurchaseList(query);
    if (null == subPurchaseResult || !subPurchaseResult.getSuccess()
        || MapUtils.isEmpty(subPurchaseResult.getModule())) {
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "采购单数据不存在");
    }
    Map<String, Object> subPurchaseMap = subPurchaseResult.getModule();
    List<PurchaseSubOrderDTO> purchaseSubOrderDTOs =
        (List<PurchaseSubOrderDTO>) subPurchaseMap.get(PurchaseService.SUB_PURCHASE_LIST_KEY);
    if (CollectionUtils.isEmpty(purchaseSubOrderDTOs)) {
      LOGGER.error("总采购单：" + purchaseId + "下的子采购单数据不存在");
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "采购单数据不存在");
    }

    PagedResult<List<PurchaseOrderDTO>> purchaseOrderResult = purchaseService.queryPurchase(query);
    if (null == purchaseOrderResult || !purchaseOrderResult.getSuccess()
        || CollectionUtils.isEmpty(purchaseOrderResult.getModule())) {
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "采购单数据不存在");
    }
    List<PurchaseOrderDTO> purchaseOrderDTOs = purchaseOrderResult.getModule();
    PurchaseOrderDTO purchaseOrderDTO = purchaseOrderDTOs.get(0);
    Integer supplierId = purchaseOrderDTO.getSupplierId();
    String remarks = purchaseOrderDTO.getRemarks();
    // 获取子采购单相关数据
    List<SubPurchaseItemVO> purchaseItemVOs =
        purchaseSubOrderDTO2PurchaseItemVO(purchaseSubOrderDTOs);
    PurchaseSubOrderDTO purchaseSubOrderDTO = purchaseSubOrderDTOs.get(0);
    // 获取供应商数据
    Result<List<BackGroundUserDTO>> supplierResult =
        backGroundUserService.queryUserDTOByIdList(Arrays.asList(supplierId));
    String supplierName = null;
    if (null != supplierResult && supplierResult.getSuccess() && null != supplierResult.getModule()) {
      List<BackGroundUserDTO> backGroundUserDTOs = supplierResult.getModule();
      BackGroundUserDTO backGroundUserDTO = backGroundUserDTOs.get(0);
      supplierName = backGroundUserDTO.getName();
    }
    String totalAmount = "0.00";
    for (SubPurchaseItemVO purchaseItemVO : purchaseItemVOs) {
      totalAmount =
          new BigDecimal(purchaseItemVO.getSubPurchaseTotalAmount()).add(
              new BigDecimal(totalAmount)).toString();
    }
    Map<String, Object> resultMap = Maps.newHashMap();
    resultMap.put("purchaseItems", purchaseItemVOs);
    resultMap.put("supplier", supplierName);
    resultMap.put("userId", supplierId);
    resultMap.put("remarks", remarks);
    resultMap.put("totalAmount", totalAmount);
    // 设置采购时间以及到货时间
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String purchaseDate = null;
    String arriveDate = null;
    if (null != purchaseSubOrderDTO.getGmtCreate()) {
      purchaseDate = sdf.format(purchaseSubOrderDTO.getGmtCreate());
    }
    if (null != purchaseSubOrderDTO.getExpectArriveDate()) {
      arriveDate = sdf.format(purchaseSubOrderDTO.getExpectArriveDate());
    }
    resultMap.put("purchaseDate", purchaseDate);
    resultMap.put("arriveDate", arriveDate);
    Integer totalNum = (Integer) subPurchaseMap.get(PurchaseService.SUB_PURCHASE_COUNT_KEY);
    return PagedResult.getSuccDataResult(resultMap, totalNum, getTotalPage(totalNum, pageSize),
        pageSize, pageNum);
  }

  /**
   * DTO转换为VO
   * 
   * @param purchaseSubOrderDTOs
   * @return
   */
  private List<SubPurchaseItemVO> purchaseSubOrderDTO2PurchaseItemVO(
      List<PurchaseSubOrderDTO> purchaseSubOrderDTOs) {
    if (CollectionUtils.isEmpty(purchaseSubOrderDTOs)) {
      return null;
    }
    List<Long> skuIds = Lists.newArrayList();
    for (PurchaseSubOrderDTO purchaseSubOrderDTO : purchaseSubOrderDTOs) {
      skuIds.add(purchaseSubOrderDTO.getSkuId());
    }

    Result<List<ItemSkuRelationDTO>> skuRelationResult =
        skuService.querySkuRelationList(SkuRelationQuery.getQueryByCskuIds(skuIds, (short) 1));
    List<Long> pskuIds = Lists.newArrayList();
    Map<Long, Integer> specificationMap = Maps.newHashMap();
    // Map<Long, Long> cPSkuIdMap = Maps.newHashMap();// 后台sku id和前台sku id映射表
    Map<Long, Integer> skuBoxCostMap = Maps.newHashMap();// 后台sku id和进货成本映射表
    if (null != skuRelationResult && skuRelationResult.getSuccess()
        && null != skuRelationResult.getModule()) {
      List<ItemSkuRelationDTO> itemSkuRelationDTOs = skuRelationResult.getModule();
      for (ItemSkuRelationDTO itemSkuRelationDTO : itemSkuRelationDTOs) {
        skuBoxCostMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getBoxCost());
        if (null == itemSkuRelationDTO.getPcProportion()) {
          itemSkuRelationDTO.setPcProportion(1000);
        }
        // cPSkuIdMap.put(itemSkuRelationDTO.getCskuId(), itemSkuRelationDTO.getPskuId());
        pskuIds.add(itemSkuRelationDTO.getPskuId());
        specificationMap.put(itemSkuRelationDTO.getCskuId(),
            itemSkuRelationDTO.getPcProportion() / 1000);
      }
    }

    Result<List<ItemCommoditySkuDTO>> itemCommoditySkuResult =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
    Map<Long, ItemCommoditySkuDTO> itemCommoditySkuMap = Maps.newHashMap();
    List<Long> itemIds = Lists.newArrayList();
    Map<Long, Long> itemSkuMap = Maps.newHashMap();
    if (null != itemCommoditySkuResult && itemCommoditySkuResult.getSuccess()
        && null != itemCommoditySkuResult.getModule()) {
      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemCommoditySkuResult.getModule();
      for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
        itemCommoditySkuMap.put(itemCommoditySkuDTO.getSkuId(), itemCommoditySkuDTO);
        itemIds.add(itemCommoditySkuDTO.getItemId());
        itemSkuMap.put(itemCommoditySkuDTO.getItemId(), itemCommoditySkuDTO.getSkuId());
      }
    }
    List<Integer> catIds = Lists.newArrayList();
    ItemListQuery itemListQuery = new ItemListQuery();
    itemListQuery.setItemIds(itemIds);
    itemListQuery.setPageNum(0);
    itemListQuery.setPageSize(100);
    Result<Map<String, Object>> itemResult = commodityService.queryCommodies(itemListQuery);
    Map<Long, String> itemCategoryNameMap = Maps.newHashMap();
    // 根据商品id获取类目id
    if (null != itemResult && itemResult.getSuccess()
        && MapUtils.isNotEmpty(itemResult.getModule())) {
      Map<String, Object> itemMap = itemResult.getModule();
      List<ItemDTO> itemDTOs = (List<ItemDTO>) itemMap.get(CommodityService.ITEM_LIST_KEY);
      Map<Long, Integer> categoryItemMap = Maps.newHashMap();
      for (ItemDTO itemDTO : itemDTOs) {
        catIds.add(itemDTO.getCategoryId());
        categoryItemMap.put(itemSkuMap.get(itemDTO.getItemId()), itemDTO.getCategoryId());
      }
      // 根据类目id获取类目名，设置商品id和类目名映射关系
      Map<Integer, String> categoryNameMap = Maps.newHashMap();
      Result<List<CategoryDTO>> categoryResult = stdCategoryService.queryCategoriesByIdList(catIds);
      if (null != categoryResult && categoryResult.getSuccess()
          && CollectionUtils.isNotEmpty(categoryResult.getModule())) {
        List<CategoryDTO> categoryDTOs = categoryResult.getModule();
        for (CategoryDTO categoryDTO : categoryDTOs) {
          categoryNameMap.put(categoryDTO.getCatId(), categoryDTO.getCatName());
        }
      }
      for (Long skuIdCache : categoryItemMap.keySet()) {
        itemCategoryNameMap.put(skuIdCache, categoryNameMap.get(categoryItemMap.get(skuIdCache)));
      }
    }
    Long storageId = purchaseSubOrderDTOs.get(0).getStorageId();
    Map<Long, String> storageCountMap = Maps.newHashMap();
    if (null != storageId && storageId > 0) {
      SimpleResultVO<List<StorageDetailVO>> storageDetailResult =
          storageDomainClient.getStorageDetailVOListByStorageID(storageId);
      if (null != storageDetailResult
          && CollectionUtils.isNotEmpty(storageDetailResult.getTarget())
          && storageDetailResult.isSuccess()) {
        List<StorageDetailVO> storageDetailVOs = storageDetailResult.getTarget();
        for (StorageDetailVO storageDetailVO : storageDetailVOs) {
          storageCountMap.put(storageDetailVO.getSkuId(), storageDetailVO.getNumber());
        }
      }
    }
    List<SubPurchaseItemVO> purchaseItemVOs = Lists.newArrayList();
    for (PurchaseSubOrderDTO purchaseSubOrderDTO : purchaseSubOrderDTOs) {
      SubPurchaseItemVO purchaseItemVO = new SubPurchaseItemVO();
      Long skuId = purchaseSubOrderDTO.getSkuId();
      purchaseItemVO.setSkuId(skuId);
      purchaseItemVO.setTitle(purchaseSubOrderDTO.getTitle());
      purchaseItemVO.setCatName(itemCategoryNameMap.get(skuId));
      purchaseItemVO.setPurchaseUnit("箱");
      if (null != purchaseSubOrderDTO.getWareHouse()) {
        purchaseItemVO.setWareHouse(purchaseSubOrderDTO.getWareHouse().toString());
      }
      purchaseItemVO.setSubPurchaseId(purchaseSubOrderDTO.getPurchaseSubId());
      purchaseItemVO.setStatus(purchaseSubOrderDTO.getStatus());
      purchaseItemVO.setCount(purchaseSubOrderDTO.getCount());
      if (null != skuBoxCostMap.get(skuId)) {
        purchaseItemVO.setDiscountPrice(new BigDecimal(skuBoxCostMap.get(skuId)).divide(
            new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
      } else {
        purchaseItemVO.setDiscountPrice("0.00");
      }
      // 子采购单总金额
      purchaseItemVO.setSubPurchaseTotalAmount(new BigDecimal(purchaseItemVO.getDiscountPrice())
          .multiply(new BigDecimal(purchaseItemVO.getCount())).setScale(2, BigDecimal.ROUND_DOWN)
          .toString());
      purchaseItemVO.setCheckInfo(purchaseSubOrderDTO.getCheckinfo());
      ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuMap.get(skuId);
      if(null != itemCommoditySkuDTO){
        String saleUnitStr =
            (itemCommoditySkuDTO.getIsSteelyardSku() ? "公斤" : itemCommoditySkuDTO.getSaleUnitStr());
        purchaseItemVO.setSaleUnit(saleUnitStr);
        purchaseItemVO.setSpecification(new StringBuilder("1箱*").append(specificationMap.get(skuId))
            .append("*").append(saleUnitStr).toString());
        purchaseItemVO.setSku69Code(itemCommoditySkuDTO.getSku69code());
      }
      if (null != storageCountMap.get(skuId) && null != specificationMap.get(skuId)) {
        String actualCount =
            new BigDecimal(storageCountMap.get(skuId)).divide(
                new BigDecimal(specificationMap.get(skuId)), 3, BigDecimal.ROUND_HALF_DOWN)
                .toString();
        purchaseItemVO.setActualCount(actualCount);
      }
      if (null != purchaseSubOrderDTO.getSettlementPrice()) {
        purchaseItemVO
            .setSubPurchaseActualAmount(new BigDecimal(purchaseSubOrderDTO.getSettlementPrice())
                .divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString());
      }
      purchaseItemVOs.add(purchaseItemVO);
    }
    return purchaseItemVOs;
  }


  /**
   * 更新采购单审核状态
   * 
   * @param purchaseId
   * @param auditingStatus
   * @param remarks
   * @param userDTO
   * @return
   */
  public Result<Boolean> updatePurchaseStatus(Integer purchaseId, Short auditingStatus,
      String remarks, BackGroundUserDTO userDTO) {
    if (null == purchaseId || null == auditingStatus) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    PurchaseQuery query = new PurchaseQuery();
    query.setPageNum(0);
    query.setPageSize(1000);
    query.setParentPurchaseId(purchaseId);
    PagedResult<List<PurchaseOrderDTO>> purchaseOrderResult = purchaseService.queryPurchase(query);
    if (null == purchaseOrderResult || !purchaseOrderResult.getSuccess()
        || CollectionUtils.isEmpty(purchaseOrderResult.getModule())) {
      String errorMsg =
          (null == purchaseOrderResult ? "请求采购单数据失败" : purchaseOrderResult.getErrorMsg());
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, errorMsg);
    }

    List<PurchaseOrderDTO> purchaseOrderDTOs = purchaseOrderResult.getModule();
    PurchaseOrderDTO purchaseOrderDTO = purchaseOrderDTOs.get(0);

    LOGGER
        .error(">>>>>>>用户id:" + userDTO.getUserId() + "," + purchaseOrderDTO.getPurchasingAgent());
    Short auditingStatusDB = purchaseOrderDTO.getAuditingStatus();
    if (UserTypeConstants.USER_EMPLOYEE.equals(userDTO.getUserType())
        && auditingStatusDB.shortValue() >= PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS
            .shortValue() && auditingStatus == 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "已经提交供应商的采购订单不能再修改");
    }
    if (auditingStatusDB.shortValue() >= auditingStatus.shortValue()
        && auditingStatus.shortValue() != 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "您已完成了此操作，请勿重复进行");
    }
    if (PurchaseOrderAuditingStatusConstants.STATUS_FIRST_AUDITING_PASS.equals(auditingStatus)
        && (null == auditingStatusDB || auditingStatusDB.shortValue() == 0)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "该采购单尚未提交，您还不能进行此操作");
    }
    if (PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS.equals(auditingStatus)
        && (null == auditingStatusDB || auditingStatusDB.shortValue() <= 1)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "初审尚未完成，您还不能进行此操作");
    }
    if (PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_DISTRIBUTION
        .equals(auditingStatus) && (null == auditingStatusDB || auditingStatusDB.shortValue() <= 3)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "店内审核尚未完成，您还不能进行此操作");
    }
    if (PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE
        .equals(auditingStatus) && (null == auditingStatusDB || auditingStatusDB.shortValue() <= 4)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "您还未审核，不能进行配送操作");
    }
    if (null != auditingStatus && auditingStatus == 5) {
      purchaseOrderDTO.setDeliveryStatus(PurchaseDeliveryStatusConstants.STATUS_IN_DISTRIBUTION);
    }
    purchaseOrderDTO.setAuditingStatus(auditingStatus);
    if (StringUtils.isNotBlank(remarks)) {
      String purchaseRemarks = purchaseOrderDTO.getRemarks();
      if (StringUtils.isEmpty(purchaseRemarks)) {
        purchaseOrderDTO.setRemarks(new StringBuilder(remarks).toString());
      } else {
        purchaseOrderDTO.setRemarks(new StringBuilder(purchaseRemarks).append("<br>")
            .append(remarks).toString());
      }

    }
    Result<Boolean> updatePurchaseResult = purchaseService.updatePurchaseOrder(purchaseOrderDTO);
    if (null == updatePurchaseResult) {
      LOGGER.error("更新采购单失败，原因：" + JackSonUtil.getJson(updatePurchaseResult));
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "网络连接失败，请稍后再试");
    }
    // 提交供应商的时候给供应商发送短信并且发送采购详情邮件
    if (null != updatePurchaseResult && updatePurchaseResult.getSuccess()
        && null != updatePurchaseResult.getModule() && updatePurchaseResult.getModule()
        && PurchaseOrderAuditingStatusConstants.STATUS_LAST_AUDITING_PASS.equals(auditingStatus)) {
      Integer supplierId = purchaseOrderDTO.getSupplierId();
      Result<BackGroundUserDTO> backGroundUserResult =
          backGroundUserService.queryUserDTOById(supplierId);
      if (null != backGroundUserResult && backGroundUserResult.getSuccess()
          && null != backGroundUserResult.getModule()) {
        // 创建任务集合
        BackGroundUserDTO backGroundUserDTO = backGroundUserResult.getModule();
        Long phone = backGroundUserDTO.getPhone();
        final Long finalPhone = phone;
        final Integer finalPurchaseOrderId = purchaseId;
        final Integer finalSupplierId = supplierId;
        final Integer finalPurchaseAgent = purchaseOrderDTO.getPurchasingAgent();
        final Date finalPurchaseDate = purchaseOrderDTO.getGmtCreate();
        final String finalSupplierName = backGroundUserDTO.getName();
        final String finalRemarks = purchaseOrderDTO.getRemarks();
        // 创建线程池
        ExecutorService exec = Executors.newFixedThreadPool(10);
        FutureTask<Boolean> sendMessageTask = new FutureTask<>(new Callable<Boolean>() {
          @Override
          public Boolean call() throws Exception {
            return sendMessage(finalPhone, finalPurchaseOrderId);
          }
        });
        exec.submit(sendMessageTask);
        FutureTask<Boolean> sendMailTask = new FutureTask<>(new Callable<Boolean>() {
          @Override
          public Boolean call() throws Exception {
            return sendPurchaseEmail(finalPurchaseOrderId, finalSupplierId, finalPurchaseAgent,
                finalPurchaseDate, finalSupplierName, finalRemarks);
          }
        });
        exec.submit(sendMailTask);
        // 关闭线程池
        exec.shutdown();
      }
    }
    return updatePurchaseResult;
  }


  private Boolean sendMessage(Long phone, Integer purchaseId) {
    LOGGER.error("开始发送短信，手机号：" + phone);
    Map<String, String> context = Maps.newHashMap();
    context.put("proder", String.valueOf(purchaseId));
    RpcResult<Boolean> sendResult =
        messageService.sendSmsMessage(String.valueOf(phone), "SMS_63085169", context);
    if (null == sendResult || !sendResult.isSuccess()) {
      String msg = sendResult == null ? "网络连接失败" : sendResult.getMsg();
      LOGGER.error("供应商：" + phone + "短信发送失败，原因：" + msg);
    }
    LOGGER.error("send message result:" + JackSonUtil.getJson(sendResult));
    return null == sendResult ? false : sendResult.isSuccess();
  }


  private Boolean sendPurchaseEmail(Integer purchaseId, Integer supplierId, Integer purchaseAgent,
      Date purchaseDate, String supplierName, String remarks) {
    LOGGER.error("开始发送邮件，默认邮箱：" + recipient);
    Result<Boolean> createFileResult = null;
    try {
      createFileResult =
          createPurchaseDetailFile(purchaseId, purchaseAgent, purchaseDate, supplierName, remarks);
    } catch (Exception e) {
      LOGGER.error("[sendPurchaseEmail] create file error:" + e.getMessage());
    }
    LOGGER.error("[sendPurchaseEmail] create file result:" + JackSonUtil.getJson(createFileResult));
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String content = null;
    String subject = null;
    String to = null;
    String copyto = null;
    List<String> fileNames = Lists.newArrayList();
    if (null == createFileResult || !createFileResult.getSuccess()
        || null == createFileResult.getModule() || !createFileResult.getModule()) {
      String errorMsg = null == createFileResult ? "网络异常" : createFileResult.getErrorMsg();
      content =
          new StringBuilder("采购邮件发送供应商失败，采购单：").append(purchaseId).append("，原因：").append(errorMsg)
              .append(",请稍后发送").toString();
      subject = new StringBuilder("采购单异常(").append(sdf.format(new Date())).append(")").toString();
      to = "dongpo@xianzaishi.com";
    } else {
      Result<SupplierDTO> supplierResult = supplierService.querySupplierById(supplierId, true);
      if (null != supplierResult && supplierResult.getSuccess()
          && null != supplierResult.getModule()) {
        SupplierDTO supplierDTO = supplierResult.getModule();
        String supplierMail = null;
        if (null != supplierDTO.getSupplierCompanyDTO()) {
          supplierMail = supplierDTO.getSupplierCompanyDTO().getCompanyEmail();
        }
        subject =
            new StringBuilder("鲜在时采购单(").append(sdf.format(purchaseDate)).append(")").toString();
        content =
            new StringBuilder("你好：").append("附件是鲜在时采购员").append(sdf.format(purchaseDate))
                .append("日下的采购单，请查阅！").toString();
        if (StringUtils.isNotEmpty(supplierMail)) {
          to = new StringBuilder(recipient).append(",").append(supplierMail).toString();
        }
        copyto = "dongpo@xianzaishi.com";
        String path =
            new StringBuilder("/usr/works/purchasefile/采购单_").append(purchaseId).append(".xlsx")
                .toString();
        fileNames.add(path);
      }

    }
    LOGGER.error("默认邮箱：" + recipient + "收件人邮箱：" + to);
    Result<Boolean> sendMailResult = mailService.sendMail(to, copyto, subject, content, fileNames);
    if (null == sendMailResult || !sendMailResult.getSuccess()
        || null == sendMailResult.getModule() || !sendMailResult.getModule()) {
      String errorMsg = null == sendMailResult ? "网络连接失败" : sendMailResult.getErrorMsg();
      LOGGER.error("发送邮件失败，原因：" + errorMsg + ",附件：" + fileNames);
      sendMailResult = mailService.sendMail(to, copyto, subject, content, fileNames);
    }
    return sendMailResult == null ? false : sendMailResult.getModule();
  }

  /**
   * 创建采购单文档
   * 
   * @param purchaseId
   * @param purchaseAgent
   * @param purchaseDate
   * @param supplierName
   * @param remarks
   * @return
   */
  private Result<Boolean> createPurchaseDetailFile(Integer purchaseId, Integer purchaseAgent,
      Date purchaseDate, String supplierName, String remarks) {
    if (null == purchaseId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    Result<Map<String, Object>> purchaseDetailResult = queryOrderDetail(purchaseId, null, null);
    if (null == purchaseDetailResult || !purchaseDetailResult.getSuccess()
        || null == purchaseDetailResult.getModule()) {
      String errorMsg =
          null == purchaseDetailResult ? "网络链接失败，请稍后再发送供应商邮件" : purchaseDetailResult.getErrorMsg();
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    SheetDTO sheetDTO = new SheetDTO();
    List<String> columnNames = Lists.newArrayList();
    for (int i = 0; i < 6; i++) {
      String columnName = getSheetColumnName(i);
      columnNames.add(columnName);
    }
    sheetDTO.setColumnNames(columnNames);
    Map<String, Object> purchaseMap = purchaseDetailResult.getModule();
    List<SubPurchaseItemVO> subPurchaseItemVOs =
        (List<SubPurchaseItemVO>) purchaseMap.get("purchaseItems");
    List<Map<String, Object>> columnDataMaps = Lists.newArrayList();
    String totalPrice = "0";
    String totalCount = "0";
    if (CollectionUtils.isNotEmpty(subPurchaseItemVOs)) {
      for (SubPurchaseItemVO subPurchaseItemVO : subPurchaseItemVOs) {
        Map<String, Object> map = Maps.newHashMap();
        String specification = subPurchaseItemVO.getSpecification();
        map.put(getSheetColumnName(0), subPurchaseItemVO.getCatName());
        map.put(getSheetColumnName(1), subPurchaseItemVO.getTitle());
        map.put(getSheetColumnName(2), specification);
        map.put(getSheetColumnName(3), subPurchaseItemVO.getDiscountPrice());
        map.put(getSheetColumnName(4), subPurchaseItemVO.getSubPurchaseTotalAmount());
        String[] specificationArray = specification.split("\\*");
        String pcProption = "1";
        String pcProptionUnit = "盒";
        if (specificationArray.length > 2) {
          pcProption = specificationArray[1];
          pcProptionUnit = specificationArray[2];
        }
        String count =
            new BigDecimal(subPurchaseItemVO.getCount()).multiply(new BigDecimal(pcProption))
                .toString();
        String sb =
            new StringBuilder(String.valueOf(subPurchaseItemVO.getCount()))
                .append(subPurchaseItemVO.getPurchaseUnit()).append("共").append(count)
                .append(pcProptionUnit).toString();
        map.put(getSheetColumnName(5), sb);
        if (null == subPurchaseItemVO.getSubPurchaseTotalAmount()) {
          subPurchaseItemVO.setSubPurchaseTotalAmount("0");
        }
        totalCount =
            new BigDecimal(totalCount).add(new BigDecimal(subPurchaseItemVO.getCount())).toString();
        totalPrice =
            new BigDecimal(totalPrice).add(
                new BigDecimal(subPurchaseItemVO.getSubPurchaseTotalAmount())).toString();
        columnDataMaps.add(map);
      }
    }
    sheetDTO.setRows(columnDataMaps);
    Result<EmployeeDTO> employeeResult = employeeService.queryEmployeeById(purchaseAgent);
    if (null != employeeResult && employeeResult.getSuccess() && null != employeeResult.getModule()) {
      EmployeeDTO employeeDTO = employeeResult.getModule();
      Integer organizationId = employeeDTO.getOrganizationId();
      sheetDTO.setAddress(getShopAddress(organizationId));
    }
    sheetDTO.setDatePattern("yyyy-MM-dd");
    sheetDTO.setPurchaseDate(new SimpleDateFormat("yyyy-MM-dd").format(purchaseDate));
    sheetDTO.setSheetName("采购单");
    sheetDTO.setTitleActive("上海鲜在时精品生鲜会员店订单");
    sheetDTO.setOrderId(purchaseId);
    sheetDTO.setSupplierName(supplierName);
    sheetDTO.setTotalPrice(totalPrice);
    sheetDTO.setTotalCount(new StringBuilder(totalCount).append("箱").toString());
    sheetDTO.setRemarks(remarks);
    LOGGER.error("[createPurchaseDetailFile] sheet data :" + JackSonUtil.getJson(sheetDTO));
    // 新建固定格式的采购单文档
    return mailService.createAttachment(Arrays.asList(sheetDTO));
  }


  private String getShopAddress(Integer organizationId) {
    String address = null;
    switch (organizationId) {
      case 1:
        address = "上海浦东新区东方路1367号富都广场1F";
        break;
    }
    return address;
  }


  /**
   * 获取表格列名
   * 
   * @param i
   * @return
   */
  private String getSheetColumnName(int i) {
    String columnName = null;
    switch (i) {
      case 0:
        columnName = "所属类目";
        break;
      case 1:
        columnName = "商品名称";
        break;
      case 2:
        columnName = "采购规格";
        break;
      case 3:
        columnName = "采购单价";
        break;
      case 4:
        columnName = "订货金额";
        break;
      case 5:
        columnName = "订货数量";
        break;
    }
    return columnName;
  }

  public String printRecipientMail() {
    LOGGER.error("默认邮箱：" + recipient);
    return recipient;
  }


  public String getRecipient() {
    return recipient;
  }


  public void setRecipient(String recipient) {
    LOGGER.error("setter注入：" + recipient);
    this.recipient = recipient;
  }

}
