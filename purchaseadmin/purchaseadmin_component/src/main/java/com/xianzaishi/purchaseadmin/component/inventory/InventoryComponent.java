package com.xianzaishi.purchaseadmin.component.inventory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskStatusConstants;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.custom.vo.CustomTaskQueryVO;
import com.xianzaishi.purchaseadmin.client.custom.vo.TaskInfoVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.InventoryDetailVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.InventoryVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.OrderSkuInfo;
import com.xianzaishi.purchaseadmin.client.inventory.vo.OrderSkuInfo.RefundOrderSkuType;
import com.xianzaishi.purchaseadmin.client.inventory.vo.OrderUpdateInventoryVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.OrderUpdateInventoryVO.OrderProcessTypeConstant;
import com.xianzaishi.purchaseadmin.client.inventory.vo.OutgointDetailAuditVO;
import com.xianzaishi.purchaseadmin.client.inventory.vo.StorageInfoVO;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.vo.OrderExchangeVO;
import com.xianzaishi.trade.client.vo.OrderItemExchangeDetailVO;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import com.xianzaishi.trade.client.vo.OrderSkuExchangeVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.DemicalUtils;
import com.xianzaishi.wms.common.utils.ListUtils;
import com.xianzaishi.wms.common.utils.ObjectUtils;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.domain.client.itf.IOutgoingDomainClient;
import com.xianzaishi.wms.track.domain.client.itf.IStorageDomainClient;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageVO;
import com.xianzaishi.wms.track.vo.StorageVO.StorageReasonType;
import com.xianzaishi.wms.track.vo.StorageVO.StorageStatus;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component("inventoryComponent")
public class InventoryComponent {

  private static final Logger logger = Logger.getLogger(InventoryComponent.class);

  @Autowired
  private IInventory2CDomainClient iInventory2CDomainClient;

  @Autowired
  private IInventoryDomainClient iInventoryDomainClient;

  @Autowired
  private IInventoryConfigDomainClient inventoryConfigDomainClient;

  @Autowired
  private IPositionDomainClient positionDomainClient;

  @Autowired
  private IStorageDomainClient storageDomainClient;

  @Autowired
  private IOutgoingDomainClient outgoingDomainClient;

  @Autowired
  private CustomerService customerService;

  @Autowired
  private BackGroundUserService backgrounduserservice;

  @Autowired
  private OrderService orderService;

  @Autowired
  private SkuService skuService;


  /**
   * 获取sku库存
   */
  public Result<InventoryVO> getInventory(long skuId) {
    SimpleResultVO<com.xianzaishi.wms.hive.vo.InventoryVO> inventoryResult =
        iInventory2CDomainClient.getInventoryByGovAndSKU(1L, skuId);
    if (null == inventoryResult || !inventoryResult.isSuccess()
        || null == inventoryResult.getTarget()) {
      return Result.getErrDataResult(-1, "查询库存信息出错");
    }
    com.xianzaishi.wms.hive.vo.InventoryVO inventory = inventoryResult.getTarget();

    InventoryVO result = new InventoryVO();
    result.setNum(inventory.getNumberShow());
    result.setSafeNum(inventory.getGuardBit());

    /**
     * 查询有效库位，包括部分推荐库位和正常使用库位
     */
    SimpleResultVO<List<com.xianzaishi.wms.hive.vo.InventoryVO>> inventoryPositionResult =
        iInventoryDomainClient.getInventoryBySKUID(1L, skuId);
    if (null == inventoryPositionResult || !inventoryPositionResult.isSuccess()
        || null == inventoryPositionResult.getTarget()) {
      return Result.getErrDataResult(-1, "查询总库存库位信息出错");
    }
    List<com.xianzaishi.wms.hive.vo.InventoryVO> totalInventoryResult =
        inventoryPositionResult.getTarget();
    List<InventoryDetailVO> allList = Lists.newArrayList();
    Map<String, InventoryDetailVO> voM = Maps.newHashMap();
    for (com.xianzaishi.wms.hive.vo.InventoryVO tmpInventory : totalInventoryResult) {
      InventoryDetailVO detail = new InventoryDetailVO();
      detail.setPoisition(tmpInventory.getPositionCode());
      detail.setNum(new BigDecimal(tmpInventory.getNumber()).divide(new BigDecimal(1000))
          .toString());
      voM.put(tmpInventory.getPositionCode(), detail);
      allList.add(detail);
    }
    result.setAvailableInventory(allList);

    /**
     * 推荐库位，空的可供使用的库位
     */
    SimpleResultVO<List<PositionDetailVO>> positionDetailVOs =
        inventoryConfigDomainClient.getPositionDetailBySkuID(1L, skuId);
    if (null == positionDetailVOs || !positionDetailVOs.isSuccess()
        || null == positionDetailVOs.getTarget()) {
      return Result.getErrDataResult(-1, "查询推荐库存库位信息出错");
    }
    if (positionDetailVOs.getTarget() != null && positionDetailVOs.getTarget().size() > 0) {
      List<InventoryDetailVO> recommondList = Lists.newArrayList();
      for (PositionDetailVO positionDetailVO : positionDetailVOs.getTarget()) {
        SimpleResultVO<PositionVO> positionResult =
            positionDomainClient.getPositionVOByID(positionDetailVO.getPositionId());
        if (null == positionResult || !positionResult.isSuccess()
            || null == positionResult.getTarget()) {
          continue;
        }
        PositionVO po = positionResult.getTarget();
        String code = po.getCode();
        InventoryDetailVO vo = voM.get(code);
        if (null == vo) {
          InventoryDetailVO recommondDetail = new InventoryDetailVO();
          recommondDetail.setPoisition(code);
          recommondDetail.setNum("0");
          recommondList.add(recommondDetail);
        } else {
          recommondList.add(vo);
        }
      }
      result.setRecommondInventory(recommondList);
    }
    return Result.getSuccDataResult(result);
  }

  /**
   * 获取入库记录
   */
  public Result<StorageVO> getStorageVO(Long id) {
    SimpleResultVO<StorageVO> storageResult = storageDomainClient.getStorageDomainByID(id);
    if (null != storageResult && storageResult.isSuccess() && null != storageResult.getTarget()) {
      return Result.getSuccDataResult(storageResult.getTarget());
    }
    if (null == storageResult) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "网络连接失败，请稍后再试");
    }
    return Result.getSuccDataResult(null);
  }

  /**
   * 获取出库记录
   */
  public Result<OutgointDetailAuditVO> getOutgoingVO(Long id) {
    SimpleResultVO<OutgoingVO> outgoingResult = outgoingDomainClient.getOutgoingDomainByID(id);
    if (null != outgoingResult && outgoingResult.isSuccess() && null != outgoingResult
        .getTarget()) {
      OutgointDetailAuditVO outgointDetailAuditVO = new OutgointDetailAuditVO();
      BeanCopierUtils.copyProperties(outgoingResult.getTarget(), outgointDetailAuditVO);
      outgointDetailAuditVO
          .setOpReasonString(getOpReasonString(outgointDetailAuditVO.getOpReason()));
      outgointDetailAuditVO.setStatusString(getOutgoingStatus(outgointDetailAuditVO.getStatu()));
      return Result.getSuccDataResult(outgointDetailAuditVO);
    }
    if (null == outgoingResult) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "网络连接失败，请稍后再试");
    }
    return Result.getSuccDataResult(null);
  }

  /**
   * 解析出库状态
   */
  private String getOutgoingStatus(Integer statu) {
    String statusString = null;
    switch (statu) {
      case 1:
      case 2:
        statusString = "待出库";
        break;
      case 3:
        statusString = "出库完成";
        break;
      default:
        statusString = "其他状态";
        break;
    }
    return statusString;
  }

  /**
   * 解析出库原因
   */
  private String getOpReasonString(Integer opReason) {
    String opResonString = null;
    switch (opReason) {
      case 2:
        opResonString = "报损出库";
        break;
      case 3:
        opResonString = "领用出库";
        break;
      default:
        opResonString = "其他出库";
        break;
    }
    return opResonString;
  }

  /**
   * 审核出库任务
   */
  public Result<Boolean> processOutgoingTask(TaskInfoVO taskInfoVO, Integer userId) {
    if (null == taskInfoVO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }

    Result<CustomerServiceTaskDTO> customerServiceTaskResult =
        customerService.queryCustomerServiceTaskById(taskInfoVO.getTaskId());
    if (null == customerServiceTaskResult || !customerServiceTaskResult.getSuccess()
        || null == customerServiceTaskResult.getModule()) {
      String errorMsg = null == customerServiceTaskResult ? "网络连接失败，请稍后再试" : "客服任务不存在";
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    CustomerServiceTaskDTO customerServiceTaskDTO = customerServiceTaskResult.getModule();
    if (!StringUtils.isNumeric(customerServiceTaskDTO.getBizId())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "该客服任务不是出库审核任务");
    }
    if (taskInfoVO.getStatus().shortValue() == 0) {
      customerServiceTaskDTO.setStatus(CustomerServiceTaskStatusConstants.END);
    } else {
      customerServiceTaskDTO.setStatus(CustomerServiceTaskStatusConstants.AUDIT);
    }
    Result<Boolean> updateResult =
        customerService.updateCustomerServiceTask(customerServiceTaskDTO);
    if (null == updateResult || !updateResult.getSuccess() || null == updateResult.getModule()
        || !updateResult.getModule()) {
      String errorMsg = null == updateResult ? "网络连接失败" : updateResult.getErrorMsg();
      logger.error("更新客服任务状态失败，原因：" + errorMsg);
    }
    if (taskInfoVO.getStatus() == 0) {
      if (null == updateResult) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "网络连接失败，请稍后再试");
      }
      return Result.getSuccDataResult(updateResult.getModule());
    }
    try {
      SimpleResultVO<OutgoingVO> outgoingResult =
          outgoingDomainClient.getOutgoingVOByID(Long.valueOf(customerServiceTaskDTO.getBizId()));
      if (null == outgoingResult || !outgoingResult.isSuccess()
          || null == outgoingResult.getTarget()) {
        String errorMsg = null == outgoingResult ? "连接网络失败，请稍后再试" : "该出库业务数据异常或不存在";
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
      }
      OutgoingVO outgoingVO = outgoingResult.getTarget();
      if (outgoingVO.getStatu().shortValue() == 3) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST,
            "该出库任务已经审核通过，无需再次审核");
      }
      SimpleResultVO<Boolean> auditResult =
          outgoingDomainClient.auditOutgoing(Long.valueOf(customerServiceTaskDTO.getBizId()),
              userId.longValue());
      if (null == auditResult) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "网络连接失败，请稍后再试");
      }
      if (!auditResult.isSuccess() || null == auditResult.getTarget() || !auditResult.getTarget()) {
        logger.error("出库审核失败" + auditResult.getMessage());
      }
      return Result.getSuccDataResult(auditResult.getTarget());
    } catch (Exception e) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
  }


  /**
   * 审核入库任务
   */
  public String setAudited(CustomTaskQueryVO customTaskQueryVO) {
    ObjectUtils.isNull(customTaskQueryVO, "参数对象为空");
    ObjectUtils.isNull(customTaskQueryVO.getTaskId(), "任务id不能为空");
    ObjectUtils.isNull(customTaskQueryVO.getStatus(), "状态参数不能为空");
    ObjectUtils.isNull(customTaskQueryVO.getUserId(), "用户id不能为空");
    notTrue(CustomerServiceTaskStatusConstants.hasStatus(customTaskQueryVO.getStatus()),
        "不存在这样的参数状态");
    //获取客服任务
    CustomerServiceTaskDTO customerServiceTaskDTO = getCustomerServiceTaskDTO(
        customTaskQueryVO.getTaskId());
    //设置状态 1表示审核通过 0 表示审核不通过(即处理完成)
    customerServiceTaskDTO.setStatus(
        customTaskQueryVO.getStatus() == 1 ? CustomerServiceTaskStatusConstants.AUDIT
            : CustomerServiceTaskStatusConstants.END);
    //更新客服任务
    updateCustomerServiceTaskDTO(customerServiceTaskDTO);
    if (1 == customTaskQueryVO.getStatus()) {
      //入库审核
      auditStorge(Long.parseLong(customerServiceTaskDTO.getBizId()),
          customTaskQueryVO.getUserId());
    }
    return JackSonUtil.getJson(Result.getSuccDataResult(true));
  }

  public void notSuccess(Result result, String showMsg) {
    ObjectUtils.isNull(result, "结果对象为空");
    notTrue(result.getSuccess(), showMsg);
  }

  public void notTrue(Boolean a, String showMsg) {
    ObjectUtils.isNull(a, "布尔对象为空");
    if (!a) {
      throw new BizException(showMsg);
    }
  }
  /**
   * 退换货
   */
  public Result<Boolean> returnAndExchangeGoods(OrderUpdateInventoryVO data) {
    logger.error("data参数=" + JackSonUtil.getJson(data));
    //校验参数
    checkData(data);

    //退换货逻辑
    if (OrderProcessTypeConstant.RETURN_EXCHANGE_TYPE.equals(data.getOrderProcessType())) {
      if (OrderStatus.REFOUDED.getValue() <= getOrder(data.getOrderId()).getStatus()) {
        throw new BizException("订单已经是退款完成状态，不能进行退款了");
      }

      //入库
      intoStorage(data);
      //报损出库
      baosunOutgoing(data);
      //对退换货做记录
      recordReturn(data);

    }

    if (OrderProcessTypeConstant.REFOUND_TYPE.equals(data.getOrderProcessType())) {
      throw new BizException("当前不支持该操作，type=" + data.getOrderProcessType());
    }

    return Result.getSuccDataResult(true);
  }

  /**
   * 获取订单id已经入库的数量
   * @param relationId 订单id
   */
  public List<StorageDetailVO> getStorageDetailsByRelationId(Long relationId) {
    isNull(relationId, "数据对象不能为空");
    StorageDetailQueryVO queryVO = new StorageDetailQueryVO();
    queryVO.setPage(0);
    queryVO.setSize(10000);
    queryVO.setRelationId(relationId);
    logger.error("订单id==" + relationId);
    SimpleResultVO<List<StorageDetailVO>> listSimpleResultVO = storageDomainClient
        .queryStorageDetailVOList(queryVO);

    if (!listSimpleResultVO.isSuccess()) {
      throw new BizException(listSimpleResultVO.getMessage());
    }
    return listSimpleResultVO.getTarget();
  }

  /**
   * 订单商品退换货痕迹
   * @param data 客户端数据对象
   * @return
   */
  private OrderItemExchangeDetailVO getOrderItemExchangeDetailVO(OrderUpdateInventoryVO data) {
    isNull(data,"数据对象为空");
    isNull(data.getOrderItemTransformDetail(),"订单商品退换货痕迹对象为空");

    OrderItemExchangeDetailVO orderItemExchangeDetailVO = new OrderItemExchangeDetailVO();
    //支付记录
    orderItemExchangeDetailVO.setPayInfo(data.getOrderItemTransformDetail().getPayInfo());
    //支付类型
    orderItemExchangeDetailVO.setPayType(data.getOrderItemTransformDetail().getPayType());
    //总差价
    orderItemExchangeDetailVO.setPrice(new BigDecimal(data.getOrderItemTransformDetail().getPrice()+"").multiply(new BigDecimal("100")).intValue());
    return orderItemExchangeDetailVO;
  }

  /**
   * 对报损商品进行出库
   */
  private void baosunOutgoing(OrderUpdateInventoryVO data) {
    isNull(data, "数据源对象不能为空");

    List<OutgoingDetailVO> outgongingDetails = getOutgongingDetails(data);
    if (org.apache.commons.collections.CollectionUtils.isEmpty(outgongingDetails)) {
      logger.error("没有要报损的商品");
      return;
    }
    OutgoingVO outgoingVO = new OutgoingVO();
    outgoingVO.setAgencyId(1L);
    outgoingVO.setStatu(0);
    outgoingVO.setOperate(Long.parseLong(String.valueOf(data.getOperate())));
    outgoingVO.setOpReason(2);
    outgoingVO.setRelationId(data.getOrderId());//订单id
    //出库明细
    outgoingVO.setDetails(outgongingDetails);
    SimpleResultVO<Boolean> resultVO = outgoingDomainClient.createAndAccount(outgoingVO);
    logger.error("出库结果=" + JackSonUtil.getJson(resultVO));
    if (null == resultVO || !resultVO.isSuccess() || !resultVO.getTarget()) {
      throw new BizException("报损出库失败");
    }
  }

  /**
   * 根据入库id获取入库明细
   */
  public Object getStorageDetails(String storageId) {
    com.xianzaishi.wms.common.utils.StringUtils.isEmptyString(storageId, "入库id不能为空");
    //获取入库信息
    StorageVO target = getStorageVOByStorageId(Long.parseLong(storageId.trim()));
    //获取员工信息 设置name
    target.setOperateName(getBackGroundUserDTOByUserId(
        Integer.parseInt(String.valueOf(target.getOperate()))).getName());
    //拷贝数据
    StorageInfoVO storageInfoVO = new StorageInfoVO();
    BeanCopierUtils.copyProperties(target, storageInfoVO);
    //设置入库原因
    storageInfoVO.setOpReasonString(getStorageReasonString(storageInfoVO.getOpReason()));
    //设置入库状态，不是任务状态
    storageInfoVO.setStatusString(getStorageStatus(storageInfoVO.getStatu()));
    if ( null != storageInfoVO.getAuditor()) {
      //审核人
      storageInfoVO.setAuditorName(getBackGroundUserDTOByUserId(
          Integer.parseInt(storageInfoVO.getAuditor() + "")).getName());
    }
    return storageInfoVO;
  }

  /**
   * 根据入库状态获取对应的statustring
   * @param statu
   * @return
   */
  private String getStorageStatus(Integer statu) {
    ObjectUtils.isNull(statu,"状态码不能为空");

    if (!StorageStatus.hasStatus(statu)) {
      throw new BizException("不存在status：" + statu + "这种入库状态");
    }

    if (StorageStatus.INIT.equals(statu)) {
      return "入库初始状态";
    }

    if (StorageStatus.SUBMIT_STATUS.equals(statu)) {
      return "入库提交状态";
    }

    if (StorageStatus.AUDIT_STATUS.equals(statu)) {
      return "入库审核通过状态";
    }

    if (StorageStatus.COMPLETE_STATUS.equals(statu)) {
      return "入库完成状态";
    }
    return "未知状态";
  }

  /*
   * 初始化出库details
   *
   * @param data OrderUpdateInventoryVO 客户端数据
   */
  private List<OutgoingDetailVO> getOutgongingDetails(OrderUpdateInventoryVO data) {
    isNull(data, "数据不能为空");
    ListUtils.isEmpty(data.getSkuList(), "商品信息参数集合为空");

    List<OutgoingDetailVO> list = new ArrayList<>();
    for (OrderSkuInfo orderSkuInfo : data.getSkuList()) {
      if (null == orderSkuInfo) {
        continue;
      }
      if ("".equals(orderSkuInfo.getInventoryLostCount()) || null == orderSkuInfo
          .getInventoryLostCount() || "0.00".equals(orderSkuInfo.getInventoryLostCount()) || "0"
          .equals(orderSkuInfo.getInventoryLostCount())) {
        continue;
      }

      OutgoingDetailVO outgoingDetailVO = new OutgoingDetailVO();
      outgoingDetailVO.setSkuId(orderSkuInfo.getSkuId());
      outgoingDetailVO.setRelationId(data.getOrderId());//订单id

      logger.error("查询库存库位=" + orderSkuInfo.getSkuId());
      SimpleResultVO<List<com.xianzaishi.wms.hive.vo.InventoryVO>> inventoryBySKUID = iInventoryDomainClient
          .getInventoryBySKUID(1L, orderSkuInfo.getSkuId());
      logger.error("查询库存库位=" + JackSonUtil.getJson(inventoryBySKUID));

      if (null == inventoryBySKUID || !inventoryBySKUID.isSuccess() || null == inventoryBySKUID
          .getTarget() || org.apache.commons.collections.CollectionUtils
          .isEmpty(inventoryBySKUID.getTarget())) {
        throw new BizException("skuId:" + orderSkuInfo.getSkuId() + "的商品没有库存库位");
      }
      outgoingDetailVO.setPositionId(inventoryBySKUID.getTarget().get(0).getPositionId());

      outgoingDetailVO.setNumber(orderSkuInfo.getInventoryLostCount());//报损数就是要出库的数量
      list.add(outgoingDetailVO);
    }

    logger.error("报损出库=" + JackSonUtil.getJson(list));
    return list;
  }

  /**
   * 退换货入库
   */
  private void intoStorage(OrderUpdateInventoryVO data) {
    logger.error("data====" + JackSonUtil.getJson(data));
    StorageVO storageVO = initStorageVO(data);
    //入库
    SimpleResultVO<Boolean> booleanSimpleResultVO = storageDomainClient
        .createAndAccount(storageVO);
    logger.error("入库结果=" + JackSonUtil.getJson(booleanSimpleResultVO));
    if (!booleanSimpleResultVO.isSuccess()) {
      throw new BizException("退货商品入库失败");
    }
  }


  /**
   * 换货操作
   * @param orderSkuInfo
   * @param data
   */
  private void exchangeGoods(OrderSkuInfo orderSkuInfo, OrderUpdateInventoryVO data) {

    String refundCount = orderSkuInfo.getRefundCount();//退货数量
    String newTradeCount = orderSkuInfo.getNewTradeCount();//重新发货数量
    String inventoryCount = orderSkuInfo.getInventoryCount();//退货后可以入库数量
    String inventoryLostCount = orderSkuInfo.getInventoryLostCount();//退货后报损数量

    List<Long> exSkuId = orderSkuInfo.getExSkuId();//换货商品skuId集合
    ListUtils.isEmpty(exSkuId, "货物信息不能为空");

    //入库数不能大于退货数量
    DemicalUtils.greater(inventoryCount,refundCount,"入库数量不能大于退货数量");
    //报损数量不能大于退货数量
    DemicalUtils.greater(inventoryLostCount,refundCount,"报损数量不能大于入库数量");

    //查询订单信息
    OrderVO orderVO = getOrder(data.getOrderId());
    //退货数量不能大于购买数量
    DemicalUtils.greater(refundCount, String.valueOf(getItemOfOrderBySkuId(orderSkuInfo.getSkuId(),
        orderVO).getCount()),"退货数量不能大于购买数量");

  }

  /**
   * 记录退换货
   * @param data 客户端提交的参数对象s
   */
  private void recordReturn(OrderUpdateInventoryVO data) {
    isNull(data,"数据源对象不能为空");
    OrderExchangeVO orderExchangeVO = getOrderExchangeVO(data);
    com.xianzaishi.trade.client.Result<Boolean> booleanResult = orderService
        .orderExchangeRefund(orderExchangeVO);
    logger.error("退货结果=" + JackSonUtil.getJson(booleanResult)+",input is:"+JackSonUtil.getJson(data));
    if (null == booleanResult || !booleanResult.isSuccess() || !booleanResult.getModel()) {
      throw new BizException("记录退货信息失败");
    }
  }

  /**
   * 获取退货信息对象
   *
   * @param data OrderUpdateInventoryVO客户端传递的数据源
   * @return OrderExchangeVO
   */
  private OrderExchangeVO getOrderExchangeVO(OrderUpdateInventoryVO data) {
    ListUtils.isEmpty(data.getSkuList(), "退货信息为空");

    //对每次退货做记录
    OrderExchangeVO orderExchangeVO = new OrderExchangeVO();
    orderExchangeVO.setOrderId(data.getOrderId());
    orderExchangeVO.setOperator(Long.parseLong(String.valueOf(data.getOperate())));
    //订单商品退换货痕迹
    orderExchangeVO.setOrderItemTransformDetail(getOrderItemExchangeDetailVO(data));

    List<OrderSkuExchangeVO> objects = new ArrayList<>();

    //查询订单信息
    OrderVO orderVO = getOrder(data.getOrderId());

    for (OrderSkuInfo orderSkuInfo : data.getSkuList()) {
      //退货数量不能大于购买数量
      DemicalUtils.greater(orderSkuInfo.getRefundCount(),
          String.valueOf(getItemOfOrderBySkuId(orderSkuInfo.getSkuId(),
              orderVO).getCount()), "退货数量不能大于购买数量");
      OrderSkuExchangeVO exchangeVO = new OrderSkuExchangeVO();
      exchangeVO.setSkuId(orderSkuInfo.getSkuId());
      exchangeVO.setCount(Float.parseFloat(orderSkuInfo.getRefundCount()));
      if (RefundOrderSkuType.EXCHANGE.equals(orderSkuInfo.getRefundOrderSkuType())) {
        throw new BizException("暂时不支持换货");
      }
      exchangeVO.setProcessSkuType(orderSkuInfo.getRefundOrderSkuType());
      objects.add(exchangeVO);
    }
    orderExchangeVO.setSkuList(objects);
    return orderExchangeVO;
  }

  /**
   * 获取入库storageVO
   *
   * @param data 客户端提交的参数对象
   */
  private StorageVO initStorageVO(OrderUpdateInventoryVO data) {
    ListUtils.isEmpty(data.getSkuList(), "提交的退货信息为空");

    logger.error("initStorageVO,data=" + JackSonUtil.getJson(data));

    StorageVO storageVO = new StorageVO();
    storageVO.setAgencyId(1L);
    storageVO.setOperate(Long.parseLong(String.valueOf(data.getOperate())));
    storageVO.setStatu(0);
    storageVO.setOpReason(StorageReasonType.RETURN_GOODS);
    storageVO.setRelationId(data.getOrderId());

    //订单入库信息
    List<StorageDetailVO> detailsList = getStorageDetailsByRelationId(
        data.getOrderId());

    logger.error("订单入库信息=" + JackSonUtil.getJson(detailsList));

    //订单信息
    OrderVO order = getOrder(data.getOrderId());

    logger.error("订单信息=" + JackSonUtil.getJson(order));

    //skuId number positioinId（自己获取）
    List<StorageDetailVO> details = new ArrayList<>();

    for (OrderSkuInfo orderSkuInfo : data.getSkuList()) {

      logger.error("initStorageVO,orderSkuInfo=" + JackSonUtil.getJson(orderSkuInfo));

      StorageDetailVO storageDetailVO = new StorageDetailVO();
      storageDetailVO.setSkuId(orderSkuInfo.getSkuId());//skuId

      //已经入库数量
      String havedStorageCount = getStorageCountBySkuId(detailsList, orderSkuInfo.getSkuId());
      //订单中的商品信息
      OrderItemVO orderItemVO = getItemOfOrderBySkuId(orderSkuInfo.getSkuId(), order);

      logger.error("哈哈=skuId" + orderSkuInfo.getSkuId() + "已经入库：" + havedStorageCount + ",订单中的数量："
          + orderItemVO.getCount());

      DemicalUtils.greaterAndEqual(havedStorageCount, String.valueOf(orderItemVO.getCount()),
          "skuId：" + orderSkuInfo.getSkuId() + "已经入库数量不能大于或等于订单中的商品数量");
      //剩余可入库数量
      BigDecimal shengyu = DemicalUtils
          .subtract(String.valueOf(orderItemVO.getCount()), havedStorageCount);

      logger.error("入库数量：" + orderSkuInfo.getInventoryCount() + ", 剩余可入库：" + shengyu.toString());

      DemicalUtils.greater(orderSkuInfo.getInventoryCount(), shengyu.toString(),
          "商品skuId：" + orderSkuInfo.getSkuId() + "剩余可入库数量为：" + shengyu.toString());

      storageDetailVO.setNumber(orderSkuInfo.getInventoryCount());//入库数量

      storageDetailVO.setRelationId(data.getOrderId());//订单id

      SimpleResultVO<List<com.xianzaishi.wms.hive.vo.InventoryVO>> inventoryBySKUID = iInventoryDomainClient
          .getInventoryBySKUID(1L, orderSkuInfo.getSkuId());
      logger.error("initStorageVO,获取库位信息=" + JackSonUtil.getJson(inventoryBySKUID));
      notTrue(inventoryBySKUID.isSuccess(), "查询库位失败skuId:" + orderSkuInfo.getSkuId());
      storageDetailVO.setPositionId(inventoryBySKUID.getTarget().get(0).getPositionId());//库位id
      details.add(storageDetailVO);
    }
    storageVO.setDetails(details);
    logger.error("入库storageVO=" + JackSonUtil.getJson(storageVO));
    return storageVO;
  }


  public String getStorageCountBySkuId(List<StorageDetailVO> list, Long  skuId) {
    isNull(skuId,"skuid不能为空");
    if (org.apache.commons.collections.CollectionUtils.isEmpty(list)) {
      return "0";//表示已经入库数为0
    }

    String count = "0";
    for (StorageDetailVO storageDetailVO:list) {
      if (null == storageDetailVO) {
        continue;
      }

      logger.error("商品skuId："+storageDetailVO.getNumber());

      if (skuId.equals(storageDetailVO.getSkuId())) {
        logger.error("订单中的商品数量=" + storageDetailVO.getNumber());
        count = DemicalUtils.add(count, storageDetailVO.getNumber()).toString();
      }
    }
    logger.error("总数量===" + count);
    return count;
  }

  /**
   * 更具skuId获取订单中对应的商品信息
   *
   * @param order 订单对象
   * @return OrderItemVO 订单中的商品信息
   */
  private OrderItemVO getItemOfOrderBySkuId(Long skuId, OrderVO order) {
    isNull(skuId, "商品skuId不能为空");
    isNull(order, "订单对象不能为空");
    List<OrderItemVO> items = order.getItems();
    for (OrderItemVO itemVO : items) {
      if (null == itemVO) {
        continue;
      }
      Long itemVOSkuId = itemVO.getSkuId();
      if (skuId.equals(itemVOSkuId)) {
        return itemVO;
      }
    }
    throw new BizException("订单中不存在skuId：" + skuId + "的商品信息");
  }

  /**
   * 校验参数
   * @param data
   */
  private void checkData(OrderUpdateInventoryVO data) {
    isNull(data, "数据对象data不能为空");
    isNull(data.getOrderProcessType(), "订单处理类型参数不能为空");
    notTrue(OrderProcessTypeConstant.hasType(data.getOrderProcessType()),
        "不存在处理类型：" + data.getOrderProcessType() + "对应的类型");

    hasOrder(data.getOrderId());

    List<OrderSkuInfo> skuList = data.getSkuList();
    if (CollectionUtils.isEmpty(skuList)) {
      throw new BizException("回传商品数据为空");
    }

    for (OrderSkuInfo skuInf : skuList) {
      if (null == skuInf) {
        continue;
      }
      String inventoryCount = skuInf.getInventoryCount();
      String inventoryLostCount = skuInf.getInventoryLostCount();
      String refundCount = skuInf.getRefundCount();
      DemicalUtils
          .notEquls(inventoryCount, refundCount, "商品skuid：" + skuInf.getSkuId() + ", 退货数量必须和入库数量相等");
      DemicalUtils.greater(inventoryLostCount, refundCount,
          "商品skuid：" + skuInf.getSkuId() + ", 报损数量不能多于退货数量");
      DemicalUtils
          .greater(inventoryCount, refundCount, "商品skuid：" + skuInf.getSkuId() + ", 入库数量不能多于退货数量");

    }
  }


  public IInventory2CDomainClient getiInventory2CDomainClient() {
    return iInventory2CDomainClient;
  }

  public void setiInventory2CDomainClient(
      IInventory2CDomainClient iInventory2CDomainClient) {
    this.iInventory2CDomainClient = iInventory2CDomainClient;
  }

  public IInventoryDomainClient getiInventoryDomainClient() {
    return iInventoryDomainClient;
  }

  public void setiInventoryDomainClient(
      IInventoryDomainClient iInventoryDomainClient) {
    this.iInventoryDomainClient = iInventoryDomainClient;
  }

  public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
    return inventoryConfigDomainClient;
  }

  public void setInventoryConfigDomainClient(
      IInventoryConfigDomainClient inventoryConfigDomainClient) {
    this.inventoryConfigDomainClient = inventoryConfigDomainClient;
  }

  public IPositionDomainClient getPositionDomainClient() {
    return positionDomainClient;
  }

  public void setPositionDomainClient(
      IPositionDomainClient positionDomainClient) {
    this.positionDomainClient = positionDomainClient;
  }

  public IStorageDomainClient getStorageDomainClient() {
    return storageDomainClient;
  }

  public void setStorageDomainClient(
      IStorageDomainClient storageDomainClient) {
    this.storageDomainClient = storageDomainClient;
  }

  public IOutgoingDomainClient getOutgoingDomainClient() {
    return outgoingDomainClient;
  }

  public void setOutgoingDomainClient(
      IOutgoingDomainClient outgoingDomainClient) {
    this.outgoingDomainClient = outgoingDomainClient;
  }


  public String getStorageReasonString(Integer reason) {
    ObjectUtils.isNull(reason, "原因不能为空");

    if (StorageReasonType.PUCHASE.equals(reason)) {
      return "采购入库";
    }

    if (StorageReasonType.RETURN_GOODS.equals(reason)) {
      return "退换货入库";
    }

    if (StorageReasonType.PRODUCE.equals(reason)) {
      return "生产";
    }

    if (StorageReasonType.OTHER.equals(reason)) {
      return "其他原因入库";
    }

    return "未知原因";
  }


  /**
   * 获取订单信息
   */
  public OrderVO getOrder(Long orderId) {
    if (null == orderId || orderId <= 0) {
      throw new BizException("订单id错误");
    }
    com.xianzaishi.trade.client.Result<OrderVO> orderVOResult = orderService.getOrder(orderId);
    if (null == orderVOResult || !orderVOResult.isSuccess()) {
      throw new BizException("订单id：" + orderId + "不存在对应的信息");
    }
    if (null == orderVOResult.getModel()) {
      throw new BizException("查询到的订单信息为空");
    }
    return orderVOResult.getModel();
  }

  /**
   * 确定有这个订单
   *
   * @param orderId 订单id
   */
  public void hasOrder(Long orderId) {
    if (null == orderId || orderId <= 0) {
      throw new BizException("订单id错误");
    }
    com.xianzaishi.trade.client.Result<OrderVO> orderVOResult = orderService.getOrder(orderId);
    if (null == orderVOResult || !orderVOResult.isSuccess()) {
      throw new BizException("不存在订单id：" + orderId + "的订单");
    }
    if (null == orderVOResult.getModel()) {
      throw new BizException("不存在订单id：" + orderId + "的订单");
    }
  }


  /**
   * 根据入库id审核商品入库
   */
  public void auditStorge(Long storgeId, Long userId) {
    if (null == storgeId || storgeId <= 0 || null == userId || userId <= 0) {
      throw new BizException("入库id或用户id不能为空或小于等于0");
    }
    SimpleResultVO<StorageVO> simpleResultVO = storageDomainClient
        .getStorageVOByID(storgeId);
    if (!simpleResultVO.isSuccess()) {
      throw new BizException("查询入库信息失败");
    }

    SimpleResultVO<Boolean> audit = storageDomainClient
        .audit(simpleResultVO.getTarget().getId(), userId);
    if (!audit.isSuccess()) {
      throw new BizException("审核失败了");
    }
  }

  /**
   * 获取客服任务
   * @param taskId 任务id
   * @return CustomerServiceTaskDTO
   */
  public CustomerServiceTaskDTO getCustomerServiceTaskDTO(Long taskId) {
    ObjectUtils.isNull(taskId, "任务id不能为空");
    Result<CustomerServiceTaskDTO> taskDTOResult = customerService
        .queryCustomerServiceTaskById(taskId);
    notSuccess(taskDTOResult, "查询任务不存在");
    return taskDTOResult.getModule();
  }

  /**
   * 是否存在该客服任务
   * @param taskId 任务id
   */
  public void hasCustomerServiceTaskDTO(Long taskId) {
    ObjectUtils.isNull(taskId, "任务id不能为空");
    Result<CustomerServiceTaskDTO> taskDTOResult = customerService
        .queryCustomerServiceTaskById(taskId);
    notSuccess(taskDTOResult, "查询任务不存在");
  }

  /**
   * 更新CustomerServiceTaskDTO
   * @param customerServiceTaskDTO
   */
  public void updateCustomerServiceTaskDTO(CustomerServiceTaskDTO customerServiceTaskDTO) {
    ObjectUtils.isNull(customerServiceTaskDTO,"更新参数对象为空");
    Result<Boolean> booleanResult = customerService
        .updateCustomerServiceTask(customerServiceTaskDTO);
    notSuccess(booleanResult,"更新信息失败");
  }

  /**
   * 根据storageId获取入库信息
   * @param storageId 入库id
   * @return StorageVO
   */
  public StorageVO getStorageVOByStorageId(Long storageId) {
    ObjectUtils.isNull(storageId,"入库id不能为空");
    SimpleResultVO<StorageVO> detailVOList = storageDomainClient
        .getStorageDomainByID(storageId);
    if (!detailVOList.isSuccess()) {
      throw new BizException("查询入库信息不存在");
    }
    return detailVOList.getTarget();
  }

  /**
   * 通过用工id获取员工信息，例如，曲洋，工号是55 就是这个55
   * @return BackGroundUserDTO
   */
  public BackGroundUserDTO getBackGroundUserDTOByUserId(Integer userId) {
    ObjectUtils.isNull(userId,"用工号不能为空");
    Result<BackGroundUserDTO> backGroundUserDTOResult = backgrounduserservice
        .queryUserDTOById(userId);
    notSuccess(backGroundUserDTOResult, "获取员工信息失败");
    return backGroundUserDTOResult.getModule();
  }

  /**
   * 通过用工token获取员工信息
   * @return BackGroundUserDTO
   */
  public BackGroundUserDTO getBackGroundUserDTOByToken(String token) {
    isEmty(token,"token不能为空");
    Result<BackGroundUserDTO> backGroundUserDTOResult = backgrounduserservice
        .queryUserDTOByToken(token);
    notSuccess(backGroundUserDTOResult, "获取员工信息失败");
    return backGroundUserDTOResult.getModule();
  }

  /**
   * 是否有这个用工，通过用工id获取员工信息，例如，曲洋，工号是55 就是这个55
   * @return BackGroundUserDTO
   */
  public void isHasBackGroundUserDTOByUserId(Integer userId) {
    ObjectUtils.isNull(userId,"用工号不能为空");
    Result<BackGroundUserDTO> backGroundUserDTOResult = backgrounduserservice
        .queryUserDTOById(userId);
    if (null == backGroundUserDTOResult || !backGroundUserDTOResult.getSuccess()) {
      throw new BizException("不存在userId：" + userId + "这样的员工");
    }
  }


/**
   * 更新订单
   *
   * @param orderVO 订单对象
   */
  public void updateOrder(OrderVO orderVO) {
    if (null == orderVO) {
      throw new BizException("订单对象为空");
    }
    com.xianzaishi.trade.client.Result<Boolean> booleanResult = orderService.updateOrderVO(orderVO);
    if (null == booleanResult || !booleanResult.isSuccess()) {
      throw new BizException("更新订单id：" + orderVO.getId() + "失败");
    }
    if (null == booleanResult.getModel() || !booleanResult.getModel()) {
      throw new BizException("更新订单id：" + orderVO.getId() + "失败");
    }
  }



  /**
   * 根据skuId获取商品信息
   * @param skuId 商品skuId
   * @return ItemCommoditySkuDTO
   */
  public ItemCommoditySkuDTO getItemCommoditySkuDTO(Long skuId) {
    if (null == skuId || skuId <= 0) {
      throw new BizException("商品skuId异常");
    }
    Result<ItemCommoditySkuDTO> itemSku = skuService
        .queryItemSku(SkuQuery.querySkuById(skuId));
    notSuccess(itemSku, "不存在skuId：" + skuId + "对应的商品信息");
    return itemSku.getModule();
  }

  /**
   * 确定含有skuId对应的商品信息
   *
   * @param skuId 商品skuId
   */
  public void hasItemCommoditySkuDTO(Long skuId) {
    if (null == skuId || skuId <= 0) {
      throw new BizException("商品skuId异常");
    }
    Result<ItemCommoditySkuDTO> itemSku = skuService
        .queryItemSku(SkuQuery.querySkuById(skuId));
    if (null == itemSku || !itemSku.getSuccess()) {
      throw new BizException("不存在skuId：" + skuId + "对应的商品");
    }
    if (null == itemSku.getModule()) {
      throw new BizException("不存在skuId：" + skuId + "对应的商品");
    }
  }


  public void isNull(Object a, String showMsg) {
    if (null == a) {
      throw new BizException(showMsg);
    }
  }

  public void isEmty(String string,String showMsg) {
    if (null == string || "".equals(string) || "null".equals(string)) {
      throw new BizException(showMsg);
    }
  }

  /**
   * 查询差价接口
   */
  public Result getPrice(OrderUpdateInventoryVO queryObjData) {
    ObjectUtils.isNull(queryObjData, "参数对象不能为空");
    ObjectUtils.isNull(queryObjData.getOperate(),"操作人不能为空");
    ObjectUtils.isNull(queryObjData.getOrderId(),"订单id不能为空");

    OrderExchangeVO orderExchangeVO = new OrderExchangeVO();
    orderExchangeVO.setOrderId(queryObjData.getOrderId());
    orderExchangeVO.setOperator(Long.parseLong(String.valueOf(queryObjData.getOperate())));
    orderExchangeVO.setSkuList(getOrderSkuExchangeVOS(queryObjData));

    com.xianzaishi.trade.client.Result<Integer> integerResult = orderService
        .accountOrderExchangeMoney(orderExchangeVO);
    logger.error("获取差价结果=" + integerResult);
    notTrue(integerResult.isSuccess(), "查询差价失败");
    return Result.getSuccDataResult(
        new BigDecimal(integerResult.getModel()).divide(new BigDecimal("100")).toString());
  }



  /**
   * 获取订单商品退换货sku对象集合
   * @param queryObjData 数据源
   * @return
   */
  private List<OrderSkuExchangeVO> getOrderSkuExchangeVOS(OrderUpdateInventoryVO queryObjData) {
    ObjectUtils.isNull(queryObjData,"数据对象不能为空");
    ListUtils.isEmpty(queryObjData.getSkuList(), "商品信息不能为空");

    List<OrderSkuExchangeVO> objects = new ArrayList<>();
    for (OrderSkuInfo orderSkuInfo : queryObjData.getSkuList()) {
      if (RefundOrderSkuType.EXCHANGE.equals(orderSkuInfo.getRefundOrderSkuType())) {
        throw new BizException("暂时不支持换货skuId:" + orderSkuInfo.getSkuId());
      }
      OrderSkuExchangeVO exchangeVO = new OrderSkuExchangeVO();
      exchangeVO.setSkuId(orderSkuInfo.getSkuId());
      exchangeVO.setCount(Float.parseFloat(orderSkuInfo.getRefundCount()));
      exchangeVO.setProcessSkuType(orderSkuInfo.getRefundOrderSkuType());
      objects.add(exchangeVO);
    }
    ListUtils.isEmpty(objects, "订单商品退换货sku对象集合为空");
    return objects;
  }

}
