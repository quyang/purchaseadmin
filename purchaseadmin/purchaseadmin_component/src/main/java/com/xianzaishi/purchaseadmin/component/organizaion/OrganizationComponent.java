package com.xianzaishi.purchaseadmin.component.organizaion;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchaseadmin.client.cashier.vo.PayOrderQuery;
import com.xianzaishi.purchasecenter.client.organization.OrganizationService;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.RechargeService;
import com.xianzaishi.trade.client.vo.RechargeVO;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;

@Component("organizationComponent")
public class OrganizationComponent {

  @Autowired
  private OrganizationService organizationService;

  @Autowired
  private RechargeService rechargeService;

  @Autowired
  private UserService userService;
  
  @Autowired
  private OrderService orderService;

  /**
   * 按照店铺id取数据
   * 
   * @param shopId
   * @return
   */
  public OrganizationDTO getOrganizationDTOById(int shopId) {
    Result<OrganizationDTO> orgInfo = organizationService.queryOrganization(shopId);
    if (null != orgInfo && orgInfo.getSuccess()) {
      return orgInfo.getModule();
    }
    return null;
  }

  /**
   * 按照店员token取店员绑定的店铺数据
   * 
   * @param userId
   * @return
   */
  public OrganizationDTO getOrganizationDTOByUserToken(String token) {
    Result<OrganizationDTO> orgInfo = organizationService.queryOrganizationByEmployToken(token);
    if (null != orgInfo && orgInfo.getSuccess()) {
      return orgInfo.getModule();
    }
    return null;
  }

  /**
   * 获取用户余额
   * 
   * @param userId
   * @return
   */
  public Result<Map<String, Object>> getUserBalance(Long userId) {
    if (null == userId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
    if (null == userResult || !userResult.getSuccess() || null == userResult.getModule()
        || userResult.getModule().getUserId() <= 0) {
      String errorMsg = null == userResult ? "网络连接失败，请稍后再试" : "用户不存在";
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    com.xianzaishi.trade.client.Result<RechargeVO> rechargeResult =
        rechargeService.queryRechargeSnap(userId);
    Integer balance = 0;
    if (null != rechargeResult && null != rechargeResult.getModel()) {
      RechargeVO rechargeVO = rechargeResult.getModel();
      balance = rechargeVO.getSum();
    }
    Map<String, Object> result = Maps.newHashMap();
    result.put("balance", balance);
    result.put("userId", userId);
    return Result.getSuccDataResult(result);
  }

  /**
   * 余额付款
   * @param query
   * @return
   */
  public Result<Map<String, Object>> payWithUserBalance(PayOrderQuery query) {
    if (null == query || null == query.getOrderId() || StringUtils.isEmpty(query.getPayMoney())
        || StringUtils.isEmpty(query.getPaySign())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误");
    }
    Integer payMoney = new BigDecimal(query.getPayMoney()).setScale(0, BigDecimal.ROUND_DOWN).intValue();
    com.xianzaishi.trade.client.Result<Boolean> payResult = orderService.payOrder(query.getOrderId(), payMoney, (short)2048, query.getPaySign(), (short)2);
    if(null == payResult || null == payResult.getModel() || !payResult.getModel()){
      String errorMsg = payResult == null ? "网络连接失败，请稍后再试！" : payResult.getMessage();
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    
    return getUserBalance(query.getUserId());
  }
}
