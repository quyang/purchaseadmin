package com.xianzaishi.purchaseadmin.component.marketactivity;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.coupon.CouponService;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO.CouponType;
import com.xianzaishi.couponcenter.client.coupon.query.CouponsQuery;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.CouponReturnVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.CouponVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.EnoughSubstractActivityVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.LimitBuyActivityVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.MarketActivityQueryVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.MarketActivityReturnVO;
import com.xianzaishi.purchaseadmin.client.marketactivity.vo.MarketActivityVO;
import com.xianzaishi.purchaseadmin.component.item.ItemComponent;
import com.xianzaishi.purchasecenter.client.marketactivity.MarketActivityService;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO.MarketActivityBindType;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO.MarketActivityStatus;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO.MarketActivityType;
import com.xianzaishi.purchasecenter.client.marketactivity.query.MarketActivityQuery;
import com.xianzaishi.purchasecenter.client.role.RoleService;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.DateUtils;
import com.xianzaishi.wms.common.utils.DemicalUtils;
import com.xianzaishi.wms.common.utils.ObjectUtils;
import com.xianzaishi.wms.common.utils.PageUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * 优惠券
 *
 * @author quyang
 */
@Component("marketActivityComponent")
public class MarketActivityComponent {

  private static final Logger logger = Logger.getLogger(MarketActivityComponent.class);

  @Autowired
  private CustomerService customerService;

  @Autowired
  private UserService userService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private SkuService skuService;

  @Autowired
  private ValueService valueService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private BackGroundUserService backGroundUserService;

  @Autowired
  private CouponService couponService;

  @Autowired
  private MarketActivityService marketActivityService;

  @Autowired
  private ItemComponent itemComponent;


  /**
   * 获取活动列表
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return return the result of getting the activities wo want
   */
  public Result getActivityList(MarketActivityQueryVO data) {

    Assert.notNull(data, "数据对象为空");
    Assert.notNull(data.getPageSize(), "页面大小参数为空");
    Assert.notNull(data.getCurrentPage(), "当前页面参数为空");

    MarketActivityQuery marketActivityQuery = new MarketActivityQuery();
    marketActivityQuery.setId(data.getId());
    marketActivityQuery.setGmtStart(DateUtils.getRightDate(data.getGmtStartString()));
    marketActivityQuery.setGmtEnd(DateUtils.getRightDate(data.getGmtEndString()));
    marketActivityQuery.setType(data.getType());
    marketActivityQuery.setStatus(data.getStatus());
    marketActivityQuery.setName(data.getName());
    marketActivityQuery.setPageNum(data.getCurrentPage());
    marketActivityQuery.setPageSize(data.getPageSize());

    List<MarketActivityReturnVO> list = new ArrayList<>();
    for (MarketActivityDTO dto : getMarketActivity(marketActivityQuery)) {
      if (null == dto) {
        continue;
      }
      MarketActivityReturnVO marketActivityVO = new MarketActivityReturnVO();
      BeanCopierUtils.copyProperties(dto, marketActivityVO);
      marketActivityVO
          .setStatusString(
              getMarketActivityStatusString(dto.getGmtStart(), dto.getGmtEnd(), marketActivityVO));
      marketActivityVO.setTypeString(getMarketActivityTypeString(marketActivityVO.getType()));
      marketActivityVO
          .setOperatorName(getBackGroundUserDTOByUserId(dto.getOperator()).getName());
      ArrayList<String> objects = new ArrayList<>();
      objects.add("有效");
      objects.add("无效");
      marketActivityVO.setStatusList(objects);
      initCouponInfo(dto, marketActivityVO);//优惠内容
      list.add(marketActivityVO);
    }

    Assert.notEmpty(list, "查询活动结果为空");

    HashMap<String, Object> map = new HashMap<>();
    map.put("currentPage", data.getCurrentPage());
    map.put("totalPage", PageUtils
        .getTotalPage(getConditionMarketActivityCount(marketActivityQuery), data.getPageSize()));
    map.put("activities", list);

    return Result.getSuccDataResult(map);
  }

  private String getMarketActivityTypeString(Short type) {
    if (null == type) {
      return "未知类型";
    }
    if (MarketActivityType.LIMIT_BUY_TYPE.equals(type)) {
      return "限购活动";
    }
    if (MarketActivityType.ENOUGHT_SUBSTRACT_TYPE.equals(type)) {
      return "满件活动";
    }
    return "未知活动";
  }


  /**
   * 设置优惠内容信息
   */
  private void initCouponInfo(MarketActivityDTO dto, MarketActivityReturnVO marketActivityVO) {

    Assert.notNull(marketActivityVO, "数据源对象为空");
    Assert.notNull(dto, "优惠券对象为空");
    String coupon_info = dto.getCouponInfo();
    com.xianzaishi.wms.common.utils.StringUtils.isEmptyString(coupon_info, "优惠信息为空");

    Short type = dto.getType();
    if (MarketActivityType.ENOUGHT_SUBSTRACT_TYPE.equals(type)) {//满减活动
      EnoughSubstractActivityVO activity = (EnoughSubstractActivityVO) JackSonUtil
          .jsonToObject(coupon_info, EnoughSubstractActivityVO.class);
      if (null != activity) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("discount", activity.getDiscount());
        map.put("post", activity.getPost());
        map.put("discountCount", activity.getDiscountCount());
        if (CollectionUtils.isNotEmpty(activity.getCouponIds())) {
          List<CouponDTO> couponDTOSList = getCouponInfo(activity.getCouponIds());
          map.put("couponInfos", getBasicCouponList(couponDTOSList));
        }
        if (CollectionUtils.isNotEmpty(activity.getSkuIds())) {
          List<ItemCommoditySkuDTO> itemInfoList = getItemInfo(activity.getSkuIds());
          map.put("itemInfos", getBasicItemList(itemInfoList));
        }

        marketActivityVO.setActivityInfo(map);
      }
    }

    if (MarketActivityType.LIMIT_BUY_TYPE.equals(type)) {//限购活动
      LimitBuyActivityVO activity = (LimitBuyActivityVO) JackSonUtil
          .jsonToObject(coupon_info, LimitBuyActivityVO.class);
      if (null != activity) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("limitedTimeDiscount", activity.getLimitedTimeDiscount());
        map.put("limitCount", activity.getLimitCount());
        if (CollectionUtils.isNotEmpty(activity.getCouponIds())) {
          List<CouponDTO> couponDTOSList = getCouponInfo(activity.getCouponIds());
          map.put("couponInfos", getBasicCouponList(couponDTOSList));
        }
        if (CollectionUtils.isNotEmpty(activity.getSkuIds())) {
          List<ItemCommoditySkuDTO> itemInfoList = getItemInfo(activity.getSkuIds());
          map.put("itemInfos", getBasicItemList(itemInfoList));
        }

        marketActivityVO.setActivityInfo(map);
      }
    }
  }

  /**
   * 编辑活动
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return return the result of updating
   */
  public Result updateActivity(MarketActivityQueryVO data) {
    Assert.notNull(data, "更新数据源为空");
    Assert.notNull(data.getId(), "更新数据的活动id为空");
    //查询活动信息
    List<MarketActivityDTO> queryResult = getMarketActivity(
        MarketActivityQuery.queryById(data.getId()));
    MarketActivityDTO activityDTO = queryResult.get(0);
    //更新活动信息
    updateMarketActivity(getRightMarketActivityDTO(activityDTO, data));
    return Result.getSuccDataResult(true);
  }


  /**
   * 添加活动
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return return the result of adding
   */
  public Result<Boolean> addActivity(MarketActivityVO data) {
    isRightMarketParams(data);
    data.setGmtStart(DateUtils.getRightDate(data.getGmtStartString()));
    data.setGmtEnd(DateUtils.getRightDate(data.getGmtEndString()));
    DemicalUtils.greater(data.getGmtStart().getTime() + "", data.getGmtEnd().getTime() + "",
        "活动开始时间不能大于结束时间");
    Assert.isTrue(!hasMarketActivity(MarketActivityQuery.queryByName(data.getName().trim())),
        "已存在同名的活动");

    MarketActivityDTO marketActivityDTO = new MarketActivityDTO();
    BeanCopierUtils.copyProperties(data, marketActivityDTO);
    if (CollectionUtils.isEmpty(data.getSkuIds()) && CollectionUtils.isEmpty(data.getCouponIds())) {
      marketActivityDTO.setCouponInfo(null);
    }

    marketActivityDTO.setStatus(
        data.getStatus() == null ? MarketActivityStatus.EFFECTIVE_STATUS : data.getStatus());
    initCouponDTOInfo(marketActivityDTO, data);

    //插入活动信息
    insertMarketActivity(marketActivityDTO);

    //限购打标1
    if (MarketActivityType.LIMIT_BUY_TYPE.equals(data.getType())) {
      itemComponent
          .updateSkuTags(data.getSkuIds(), 4096, data.getChannelType(), data.getLimitCount(), null);
    }

    return Result.getSuccDataResult(true);
  }

  private void isRightMarketParams(MarketActivityVO data) {
    Assert.notNull(data.getName(), "名称参数不正确");
    Assert.notNull(data.getType(), "类型参数不正确");

    //限购活动
    if (MarketActivityType.LIMIT_BUY_TYPE.equals(data.getType())) {
      Assert.notEmpty(data.getSkuIds(), "限购活动必须关联商品");
      Assert.notNull(data.getChannelType(), "限购活动渠道类型为空");//打标需要
      Assert.notEmpty(data.getSkuIds(), "限购活动必须关联商品信息");
      if (StringUtils.isEmpty(data.getLimitCount()) && StringUtils
          .isEmpty(data.getLimitedTimeDiscount())) {
        throw new BizException("限购数和限时折扣不能同时为空");
      }
    }

    //满件活动
    if (MarketActivityType.ENOUGHT_SUBSTRACT_TYPE.equals(data.getType())) {
      Assert.notEmpty(data.getSkuIds(), "限购活动必须关联商品");
      com.xianzaishi.wms.common.utils.StringUtils
          .isEmptyString(data.getDiscountCount(), "优惠条件：满几件必须设置");
      if (StringUtils.isEmpty(data.getDiscount()) && null == data.getPost()) {
        throw new BizException("打折和包邮不能同时为空");
      }
      Assert.notEmpty(data.getSkuIds(), "满件活动必须关联商品信息");
    }
  }

  /**
   * 获取优惠券列表
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return return the details of coupons
   */
  public Result getCoupons(CouponVO data) {
    ObjectUtils.isNull(data, "查询优惠券时间为空");
    CouponDTO couponDTO = new CouponDTO();
    CouponsQuery couponQuery = new CouponsQuery();
    couponQuery.setGmtStart(DateUtils.getDate(data.getGmtStartString()));
    couponQuery.setGmtEnd(DateUtils.getDate(data.getGmtEndString()));
    couponQuery.setTitle(data.getTitle());
    couponQuery.setStatus(data.getStatus());
    couponQuery.setChannelType(data.getChannelType());
    couponQuery.setPageNum(data.getPageNum());
    couponQuery.setPageSize(data.getPageSize());
    couponDTO.setId(data.getId());

    List<CouponReturnVO> list = new ArrayList<>();
    for (CouponDTO cp : getCouponDTO(couponQuery)) {
      CouponReturnVO couponReturnVO = new CouponReturnVO();
      BeanCopierUtils.copyProperties(cp, couponReturnVO);
      initCouponReturnVO(couponReturnVO);
      list.add(couponReturnVO);
    }
    HashMap<String, Object> map = Maps.newHashMap();
    map.put("coupons", list);
    map.put("currentPage", data.getPageNum());
    map.put("totalPage", PageUtils.getTotalPage(getCouponCount(couponQuery), data.getPageSize()));
    return Result.getSuccDataResult(map);
  }


  /**
   * 更新优惠券
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters is not right
   * @return return the result of updating
   */
  public Result updateCoupons(CouponVO data) {
    Assert.notNull(data, "参数对象为空");
    Assert.notNull(data.getId(), "优惠券id不能为空");
    Result<List<CouponDTO>> listResult = couponService.query(CouponsQuery.queryById(data.getId()));
    //判断查询结果
    notSuccess(listResult, listResult.getErrorMsg());
    CouponDTO couponDTO = listResult.getModule().get(0);
    updateParams(couponDTO, data);
    Result<Boolean> update = couponService.update(couponDTO);
    //判断更新结果
    notSuccess(update, update.getErrorMsg());
    return Result.getSuccDataResult(true);
  }

  private void updateParams(CouponDTO couponDTO, CouponVO data) {
    Assert.notNull(couponDTO, "参数对象为空");
    Assert.notNull(data, "优惠券数据源为空");
    if (!StringUtils.isEmpty(data.getTitle())) {
      couponDTO.setTitle(data.getTitle());
    }

    if (data.getType() != null) {
      couponDTO.setType(data.getType());
    }

    if (data.getAmount() != null) {
      couponDTO.setAmount(data.getAmount());
    }

    if (null != data.getAmountLimit()) {
      couponDTO.setAmountLimit(data.getAmountLimit());
    }

    if (null != data.getChannelType()) {
      couponDTO.setChannelType(data.getChannelType());
    }

    if (null != data.getGmtDayDuration()) {
      couponDTO.setGmtDayDuration(data.getGmtDayDuration());
    }

    if (null != data.getGmtStartString() && !"".equals(data.getGmtStartString())) {
      couponDTO.setGmtStart(DateUtils.getDate(data.getGmtStartString()));
    }
    if (null != data.getGmtEndString() && !"".equals(data.getGmtEndString())) {
      couponDTO.setGmtEnd(DateUtils.getDate(data.getGmtEndString()));
    }

    if (null != data.getStatus()) {
      couponDTO.setStatus(data.getStatus());
    }

    if (!StringUtils.isEmpty(data.getRules())) {
      couponDTO.setRules(data.getRules());
    }
  }

  /**
   * 删除某个活动
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return return the result of deleting
   */
  public Result deleteActivity(MarketActivityVO data) {
    Assert.notNull(data, "活动数据源对象为空");
    if (null == data.getId() && StringUtils.isEmpty(data.getName())) {
      throw new BizException("至少需要活动id或者活动全名");
    }
    Assert.isTrue(
        hasMarketActivity(MarketActivityQuery.queryByIdAndName(data.getId(), data.getName())),
        "不存在这样的活动");
    //删除活动
    delMarketActivity(data.getId());
    return Result.getSuccDataResult(true);
  }

  private void initCouponDTOInfo(MarketActivityDTO marketActivityDTO, MarketActivityVO data) {
    Assert.notNull(marketActivityDTO, "优惠券dto对象为空");
    Assert.notNull(data, "优惠券data对象为空");
    Assert
        .isTrue(MarketActivityType.hasActivityType(data.getType()), "不存在对应的活动类型：" + data.getType());

    if (MarketActivityType.ENOUGHT_SUBSTRACT_TYPE.equals(data.getType())) {//满减活动
      EnoughSubstractActivityVO enoughSubstractActivityVO = new EnoughSubstractActivityVO();
      enoughSubstractActivityVO.setCouponIds(data.getCouponIds());
      enoughSubstractActivityVO.setSkuIds(data.getSkuIds());
      enoughSubstractActivityVO.setDiscount(data.getDiscount());//折扣
      enoughSubstractActivityVO.setPost(data.getPost());//是否包邮
      enoughSubstractActivityVO.setDiscountCount(data.getDiscountCount());//满n件
      enoughSubstractActivityVO.setCouponIds(data.getCouponIds());
      enoughSubstractActivityVO.setSkuIds(data.getSkuIds());
      marketActivityDTO.setCouponInfo(JackSonUtil.getJson(enoughSubstractActivityVO));
    }

    if (MarketActivityType.LIMIT_BUY_TYPE.equals(data.getType())) {//限购活动
      LimitBuyActivityVO limitBuyActivityVO = new LimitBuyActivityVO();
      limitBuyActivityVO.setCouponIds(data.getCouponIds());
      limitBuyActivityVO.setSkuIds(data.getSkuIds());
      limitBuyActivityVO.setLimitCount(data.getLimitCount());//限购数
      limitBuyActivityVO.setLimitedTimeDiscount(data.getLimitedTimeDiscount());//限时折扣
      limitBuyActivityVO.setCouponIds(data.getCouponIds());
      limitBuyActivityVO.setSkuIds(data.getSkuIds());
      marketActivityDTO.setCouponInfo(JackSonUtil.getJson(limitBuyActivityVO));
    }
  }

  private MarketActivityDTO getRightMarketActivityDTO(MarketActivityDTO activityDTO,
      MarketActivityQueryVO data) {
    ObjectUtils.isNull(activityDTO, "优惠券dto对象为空");
    ObjectUtils.isNull(data, "优惠券data对象为空");

    if (null != data.getType()) {
      activityDTO.setType(data.getType());
      Assert
          .isTrue(MarketActivityType.hasActivityType(data.getType()), "不存在该类型活动:" + data.getType());
    }

    if (null != data.getGmtStartString() && !"".equals(data.getGmtStartString())) {
      activityDTO.setGmtStart(DateUtils.getDate(data.getGmtStartString()));
    }
    if (null != data.getGmtEndString() && !"".equals(data.getGmtEndString())) {
      activityDTO.setGmtEnd(DateUtils.getDate(data.getGmtEndString()));
    }

    if (!StringUtils.isEmpty(data.getName())) {
      activityDTO.setName(data.getName());
    }
    if (null != data.getStatus()) {
      activityDTO.setStatus(data.getStatus());
    }

    //满件活动
    if (!StringUtils.isEmpty(activityDTO.getCouponInfo())) {
      if (MarketActivityType.ENOUGHT_SUBSTRACT_TYPE.equals(activityDTO.getType())) {
        EnoughSubstractActivityVO info = (EnoughSubstractActivityVO) JackSonUtil
            .jsonToObject(activityDTO.getCouponInfo(), EnoughSubstractActivityVO.class);
        if (null == info) {
          info = new EnoughSubstractActivityVO();
        }

        //是否增加或减去关联的商品或优惠券
        if (null != data.getBindType()) {
          Assert.isTrue(MarketActivityBindType.hasActivityBindType(data.getBindType()),
              "不存在对应的绑定类型:" + data.getBindType());
          Assert.notNull(data.getAdd(), "add参数不能为空");

          LinkedList<Long> couponIdList = info.getCouponIds();
          LinkedList<Long> skuIdList = info.getSkuIds();

          if (CollectionUtils.isEmpty(couponIdList)) {
            couponIdList = new LinkedList<>();
          }
          if (CollectionUtils.isEmpty(skuIdList)) {
            skuIdList = new LinkedList<>();
          }

          Boolean add = data.getAdd();

          if (MarketActivityBindType.BIND_GOODS_TYPE.equals(data.getBindType())) {//关联的商品
            Assert.notEmpty(data.getItemIds(), "skuId集合不能为空");
            List<Long> itemIds = data.getItemIds();
            //判断是否存在对应的skuId的商品
            for (Long skuId : itemIds) {
              if (null == skuId) {
                continue;
              }

              if (hasItemCommoditySkuDTO(skuId)) {//有该商品
                if (skuIdList.contains(skuId)) {
                  if (!add) {
                    skuIdList.remove(skuId);
                  }
                } else {
                  if (add) {
                    skuIdList.add(skuId);
                  } else {
                    throw new BizException("活动本身并不存在商品skuId：" + skuId + "的信息");
                  }
                }
              } else {
                throw new BizException("商品skuId：" + skuId + "对应的商品信息不存在");
              }
            }

            info.setSkuIds(skuIdList);
          }

          if (MarketActivityBindType.BIND_COUPON_TYPE.equals(data.getBindType())) {//关联的优惠券
            List<Long> couponIds = data.getCouponIds();
            Assert.notEmpty(couponIds, "优惠券集合不能为空");
            //判断是否存在对应的skuId的商品
            for (Long couponId : couponIds) {
              if (null == couponId) {
                continue;
              }

              if (hasCouponDTO(couponId)) {//有该商品
                if (couponIdList.contains(couponId)) {
                  if (!add) {
                    couponIdList.remove(couponId);
                  }
                } else {
                  if (add) {
                    couponIdList.add(couponId);
                  } else {
                    throw new BizException("活动本身并不存在优惠券id：" + couponId + "的信息");
                  }
                }
              } else {
                throw new BizException("优惠券couponId：" + couponId + "对应的优惠券信息信息不存在");
              }
            }

            info.setCouponIds(couponIdList);
          }

        } else {
          if (CollectionUtils.isNotEmpty(data.getCouponIds())) {
            info.setCouponIds(data.getCouponIds());
          }
          if (CollectionUtils.isNotEmpty(data.getItemIds())) {
            info.setSkuIds(data.getItemIds());
          }
          if (!StringUtils.isEmpty(data.getDiscount())) {
            info.setDiscount(data.getDiscount());
          }
          if (null != data.getPost()) {
            info.setPost(data.getPost());
          }
          if (!StringUtils.isEmpty(data.getDiscountCount())) {
            info.setDiscountCount(data.getDiscountCount());
          }
        }

        activityDTO.setCouponInfo(JackSonUtil.getJson(info));
      }

      //限购活动
      if (MarketActivityType.LIMIT_BUY_TYPE.equals(activityDTO.getType())) {
        LimitBuyActivityVO info = (LimitBuyActivityVO) JackSonUtil
            .jsonToObject(activityDTO.getCouponInfo(), LimitBuyActivityVO.class);
        if (null == info) {
          info = new LimitBuyActivityVO();
        }

        //是否增加或减去关联的商品或优惠券
        if (null != data.getBindType()) {

          if (!MarketActivityBindType.hasActivityBindType(data.getBindType())) {
            throw new BizException("不存在对应的绑定类型");
          }

          Assert.notNull(data.getAdd(), "add参数不能为空");

          List<Long> couponIdList = info.getCouponIds();
          List<Long> skuIdList = info.getSkuIds();

          if (CollectionUtils.isEmpty(couponIdList)) {
            couponIdList = new ArrayList<>();
          }
          if (CollectionUtils.isEmpty(skuIdList)) {
            skuIdList = new ArrayList<>();
          }

          Boolean add = data.getAdd();

          if (MarketActivityBindType.BIND_GOODS_TYPE.equals(data.getBindType())) {//关联的商品
            Assert.notEmpty(data.getItemIds(), "商品skuId集合不能为空");
            List<Long> itemIds = data.getItemIds();
            //判断是否存在对应的skuId的商品
            for (Long skuId : itemIds) {
              if (null == skuId) {
                continue;
              }

              if (hasItemCommoditySkuDTO(skuId)) {//有该商品
                if (skuIdList.contains(skuId)) {
                  if (!add) {
                    skuIdList.remove(skuId);
                  }
                } else {
                  if (add) {
                    skuIdList.add(skuId);
                  } else {
                    throw new BizException("活动本身并不存在商品skuId：" + skuId + "的信息");
                  }
                }
              } else {
                throw new BizException("商品skuId：" + skuId + "对应的商品信息不存在");
              }
            }

            info.setSkuIds(skuIdList);
          }

          if (MarketActivityBindType.BIND_COUPON_TYPE.equals(data.getBindType())) {//关联的优惠券
            List<Long> couponIds = data.getCouponIds();
            if (CollectionUtils.isEmpty(couponIds)) {
              throw new BizException("优惠券集合不能为空");
            }

            //判断是否存在对应的skuId的商品
            for (Long couponId : couponIds) {
              if (null == couponId) {
                continue;
              }

              if (hasCouponDTO(couponId)) {//有该商品
                if (couponIdList.contains(couponId)) {
                  if (!add) {
                    couponIdList.remove(couponId);
                  }
                } else {
                  if (add) {
                    couponIdList.add(couponId);
                  } else {
                    throw new BizException("活动本身并不存在优惠券id：" + couponId + "的信息");
                  }
                }
              } else {
                throw new BizException("优惠券couponId：" + couponId + "对应的优惠券信息信息不存在");
              }
            }

            info.setCouponIds(couponIdList);
          }

        } else {
          if (CollectionUtils.isNotEmpty(data.getCouponIds())) {
            info.setCouponIds(data.getCouponIds());
          }

          if (CollectionUtils.isNotEmpty(data.getItemIds())) {
            info.setSkuIds(data.getItemIds());
          }

          if (!StringUtils.isEmpty(data.getLimitCount())) {
            info.setLimitCount(data.getLimitCount());
          }

          if (!StringUtils.isEmpty(data.getLimitedTimeDiscount())) {
            info.setLimitedTimeDiscount(data.getLimitedTimeDiscount());
          }
        }

        activityDTO.setCouponInfo(JackSonUtil.getJson(info));
      }
    }

    return activityDTO;
  }

  /**
   * 通过优惠券id集合查询优惠券信息
   */
  public List<CouponDTO> getCouponInfo(List<Long> couponIds) {
    //判断集合
    Assert.notEmpty(couponIds, "优惠券id集合为空");
    //获取优惠券
    Result<List<CouponDTO>> couponByIdList = couponService.getCouponByIdList(couponIds);
    //判断查询结果
    notSuccess(couponByIdList, couponByIdList.getErrorMsg());
    //判断集合集合
    Assert.notEmpty(couponByIdList.getModule(), "查询优惠券信息失败");
    return couponByIdList.getModule();
  }

  /**
   * 是否存在对应的优惠券
   */
  public Boolean hasCouponDTO(Long couponId) {
    if (null == couponId) {
      return false;
    }

    Result<CouponDTO> couponDTOResult = couponService.getCouponById(couponId);
    if (null == couponDTOResult || !couponDTOResult.getSuccess()) {
      return false;
    }
    return true;
  }

  /**
   * 通过skuId集合查询商品信息
   */
  public List<ItemCommoditySkuDTO> getItemInfo(List<Long> skuIds) {
    Assert.notEmpty(skuIds, "商品skuId集合为空");
    Result<List<ItemCommoditySkuDTO>> listResult = skuService
        .queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
    notSuccess(listResult, listResult.getErrorMsg());
    Assert.notEmpty(listResult.getModule(), "查询商品信息失败");
    return listResult.getModule();
  }


  /**
   * 获取优惠券的信息集合
   */
  public List<Object> getBasicCouponList(List<CouponDTO> list) {
    if (CollectionUtils.isEmpty(list)) {
      return null;
    }
    List<Object> arrayList = Lists.newArrayList();
    for (CouponDTO couponDTO : list) {
      if (null == couponDTO) {
        continue;
      }
      HashMap<String, Object> map = new HashMap<>();
      map.put("couponId", couponDTO.getId());
      map.put("title", couponDTO.getTitle());
      arrayList.add(map);
    }
    return arrayList;
  }


  /**
   * 获取商品的信息集合
   */
  public List<Object> getBasicItemList(List<ItemCommoditySkuDTO> list) {
    if (CollectionUtils.isEmpty(list)) {
      return null;
    }
    List<Object> arrayList = Lists.newArrayList();
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : list) {
      if (null == itemCommoditySkuDTO) {
        continue;
      }
      HashMap<Object, Object> map = new HashMap<>();
      map.put("skuId", itemCommoditySkuDTO.getSkuId());
      map.put("title", itemCommoditySkuDTO.getTitle());
      arrayList.add(map);
    }
    return arrayList;
  }

  /**
   * 根据活动状态获取相应的statusString
   */
  private String getMarketActivityStatusString(Date start, Date end,
      MarketActivityReturnVO marketActivityVO) {
    Assert.notNull(start,"开始时间不能为空");
    Assert.notNull(end, "结束时间不能为空");
    Assert.notNull(marketActivityVO, "活动信息对象不能为空");
    Assert.notNull(marketActivityVO.getStatus(), "活动状态status不能为空");

    Short status = marketActivityVO.getStatus();
    String statusEffe = "";
    if (MarketActivityStatus.UN_EFFECTIVE_STATUS.equals(status)) {
      statusEffe = "无效";
    }
    if (MarketActivityStatus.EFFECTIVE_STATUS.equals(status)) {
      statusEffe = "无效";
    }


    Date now = new Date();
    logger
        .error("当前时间：" + now.getTime() + ",start=" + start.getTime() + ",end=" + end.getTime());
    if (now.getTime() < start.getTime()) {
      return statusEffe + "未开始";
    }

    if (now.getTime() > start.getTime() && now.getTime() < end.getTime()) {
      return statusEffe + "进行中";
    }

    if (end.getTime() < now.getTime()) {
      return statusEffe + "已结束";
    }

    return statusEffe + "未知";
  }


  /**
   * 获取营销活动
   */
  public List<MarketActivityDTO> getMarketActivity(MarketActivityQuery marketActivityQuery) {
    Assert.notNull(marketActivityQuery, "活动query对象为空");
    Result<List<MarketActivityDTO>> listResult = marketActivityService.query(marketActivityQuery);
    //判断是否查询成功
    notSuccess(listResult, listResult.getErrorMsg());
    List<MarketActivityDTO> module = listResult.getModule();
    //判断查询集合
    Assert.notEmpty(module, "查询活动信息为空");
    return module;
  }

  /**
   * 查询是否有某个营销活动
   */
  public Boolean hasMarketActivity(MarketActivityQuery marketActivityQuery) {
    Assert.notNull(marketActivityQuery, "参数对象为空");
    Result<List<MarketActivityDTO>> listResult = marketActivityService.query(marketActivityQuery);
    if (null == listResult || !listResult.getSuccess()) {
      return false;
    }

    List<MarketActivityDTO> module = listResult.getModule();
    if (CollectionUtils.isEmpty(module)) {
      return false;
    }
    return true;
  }


  /**
   * 删除营销活动
   */
  public void delMarketActivity(Integer id) {
    if (null == id || id < 0) {
      throw new BizException("活动id不能为空");
    }
    Result<Boolean> delete = marketActivityService.delete(id);
    notSuccess(delete, delete.getErrorMsg());
  }

  /**
   * 更新营销活动
   */
  public void updateMarketActivity(MarketActivityDTO marketActivityDTO) {
    Assert.notNull(marketActivityDTO, "更新数据源为空");
    Result<Boolean> update = marketActivityService.update(marketActivityDTO);
    notSuccess(update, update.getErrorMsg());
  }

  /**
   * 更新营销活动
   */
  public void insertMarketActivity(MarketActivityDTO marketActivityDTO) {
    Assert.notNull(marketActivityDTO, "参数对象为空");
    Result<Boolean> insert = marketActivityService.insert(marketActivityDTO);
    notSuccess(insert, insert.getErrorMsg());
  }

  /**
   * 获取符合条件的营销活动数量
   */
  public Integer getConditionMarketActivityCount(MarketActivityQuery marketActivityQuery) {
    Assert.notNull(marketActivityQuery, "参数对象为空");
    Result<Integer> count = marketActivityService.queryCount(marketActivityQuery);
    notSuccess(count, count.getErrorMsg());
    return count.getModule();
  }

  /**
   * 查询优惠券
   */
  public List<CouponDTO> getCouponDTO(CouponsQuery couponQuery) {
    Assert.notNull(couponQuery, "优惠券参数对象为空");
    Result<List<CouponDTO>> listResult = couponService.query(couponQuery);
    notSuccess(listResult, listResult.getErrorMsg());
    return listResult.getModule();
  }

  /**
   * 获取优惠券数量
   */
  public Integer getCouponCount(CouponsQuery couponQuery) {
    Assert.notNull(couponQuery, "优惠券参数对象为空");
    Result<Integer> integerResult = couponService.queryCount(couponQuery);
    notSuccess(integerResult, integerResult.getErrorMsg());
    return integerResult.getModule();
  }

  /**
   * 是否存在对应的skuId商品
   */
  public Boolean hasItemCommoditySkuDTO(Long skuId) {
    Assert.notNull(skuId, "商品skuId不能为空");
    Result<ItemCommoditySkuDTO> skuDTOResult = skuService
        .queryItemSku(SkuQuery.querySkuById(skuId));
    if (null == skuDTOResult || null == skuDTOResult.getModule() || !skuDTOResult.getSuccess()) {
      return false;
    }
    return true;
  }

  public void notSuccess(Result result, String showMsg) {
    Assert.notNull(result, "结果result为空");
    if (!result.getSuccess()) {
      throw new BizException(showMsg);
    }
  }

  /**
   * 定时轮询活动
   */
  public Result timer() {
    marketActivityService.time();
    return Result.getSuccDataResult(true);
  }

  /**
   * get the special activity detail
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return the special activity detail
   */
  public Result getActivityDetail(MarketActivityVO data) {
    Assert.notNull(data, "参数对象不能为空");
    Assert.notNull(data.getId(), "活动id不能为空");

    MarketActivityQuery marketActivityQuery = new MarketActivityQuery();
    marketActivityQuery.setId(data.getId());
    //获取营销活动
    List<MarketActivityDTO> listResult = getMarketActivity(marketActivityQuery);
    MarketActivityReturnVO marketActivityVO = new MarketActivityReturnVO();
    BeanCopierUtils.copyProperties(listResult.get(0), marketActivityVO);
    marketActivityVO
        .setStatusString(getMarketActivityStatusString(listResult.get(0).getGmtStart(),
            listResult.get(0).getGmtEnd(), marketActivityVO));
    marketActivityVO.setTypeString(getMarketActivityTypeString(marketActivityVO.getType()));
    ArrayList<String> objects = new ArrayList<>();
    objects.add("无效");
    objects.add("有效");//前端说框架需要
    marketActivityVO.setStatusList(objects);
    marketActivityVO
        .setOperatorName(getBackGroundUserDTOByUserId(listResult.get(0).getOperator()).getName());
    initCouponInfo(listResult.get(0), marketActivityVO);//优惠内容
    return Result.getSuccDataResult(marketActivityVO);
  }


  /**
   * get the detail of the special coupon
   * @param data the request object from client
   * @throws IllegalArgumentException when the parameters or the others is not right
   * @return the detail of the special coupon
   */
  public Result getCouponDetail(CouponVO data) {
    Assert.notNull(data, "参数对象不能为空");
    Assert.notNull(data.getId(), "优惠券id不能为空");
    CouponsQuery couponQuery = new CouponsQuery();
    couponQuery.setId(data.getId());
    List<CouponDTO> couponDTOList = getCouponDTO(couponQuery);
    Assert.notEmpty(couponDTOList, "优惠券id：" + data.getId() + "不存在");
    CouponReturnVO couponReturnVO = new CouponReturnVO();
    BeanCopierUtils.copyProperties(couponDTOList.get(0), couponReturnVO);
    initCouponReturnVO(couponReturnVO);
    return Result.getSuccDataResult(couponReturnVO);
  }

  private void initCouponReturnVO(CouponReturnVO couponReturnVO) {
    ObjectUtils.isNull(couponReturnVO, "couponReturnVO对象为空");
    couponReturnVO.setTypeString(getCouponTypeStrign(couponReturnVO.getType()));
    couponReturnVO.setChannelTypeString(getChannelTypeString(couponReturnVO.getChannelType()));
    couponReturnVO.setStatuString(getStatuString(couponReturnVO.getStatus()));
    if (null == couponReturnVO.getRules()) {
      couponReturnVO.setRules("无门槛");
    }
  }

  private String getStatuString(Short status) {
    if (null == status) {
      return "未知状态";
    }
    if (-1 == status) {
      return "无效状态";
    }
    if (0 == status) {
      return "初始状态";
    }
    if (1 == status) {
      return "有效状态";
    }
    return "未知状态";
  }

  private String getChannelTypeString(Short channelType) {
//    0 通用渠道 1 线上渠道 2 线下
    if (null == channelType) {
      return "未知渠道";
    }
    if (0 == channelType) {
      return "通用";
    }
    if (1 == channelType) {
      return "线上";
    }
    if (2 == channelType) {
      return "线下";
    }
    return "未知渠道";
  }


  public String getCouponTypeStrign(Short type) {
    isNull(type, "优惠券类型为空");

    if (CouponType.ZHIDING_ITEM.equals(type)) {
      return "指定商品券";
    }
    if (type == 4) {
      return "单品优惠券";
    }

    if (CouponType.CASH.equals(type)) {
      return "现金券";
    }
    if (CouponType.BREAKFAST_VOUCHER.equals(type)) {
      return "早餐券";
    }
    if (CouponType.NEW_PERSON.equals(type)) {
      return "新人礼包券";
    }

    if (11 == type) {
      return "全场满减券";
    }

    return "未知类型";

//    指定商品满减	12		1、造券；2、绑定商品（商品打标）；3、用户领取
//    单品优惠	    4		1、造券；2、绑定商品（设置价格）；
//    无门槛			12		1、造券；2、发放用户
//    全场满减		11		1、造券；2、用户领取
  }

  public void isNull(Object a, String showMsg) {
    if (null == a) {
      throw new BizException(showMsg);
    }
  }

  private BackGroundUserDTO getBackGroundUserDTOByUserId(Integer userId) {
    isNull(userId, "用户id不能为空");
    Result<BackGroundUserDTO> backGroundUserDTOResult = backGroundUserService
        .queryUserDTOById(userId);
    notSuccess(backGroundUserDTOResult, backGroundUserDTOResult.getErrorMsg());
    isNull(backGroundUserDTOResult.getModule(), "userId：" + userId + "查询结果为空");
    return backGroundUserDTOResult.getModule();
  }
}
