package com.xianzaishi.purchaseadmin.component.wms;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.trade.callback.client.itf.ITrackCallbackClient;

@Component("wmsCallBackComponent")
public class WmsCallBackComponent {

  @Autowired
  private ITrackCallbackClient trackCallbackOnlineClient;
  
  private static final Logger logger = Logger.getLogger(WmsCallBackComponent.class);
  
  /**
   * 更新订单物流状态
   * @param orderId
   */
  public SimpleResultVO<Boolean> updateOrderCallBack(Long orderId){
    SimpleResultVO<Boolean> result = trackCallbackOnlineClient.onOrderCreate(orderId);
    logger.error("[WmsCallBackComponent]order status update end,orderId:"+orderId+",result "+result.toString());
    return result;
  }
}
